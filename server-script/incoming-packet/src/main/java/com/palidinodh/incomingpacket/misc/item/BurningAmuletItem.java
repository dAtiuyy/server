package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.BURNING_AMULET_1,
  ItemId.BURNING_AMULET_2,
  ItemId.BURNING_AMULET_3,
  ItemId.BURNING_AMULET_4,
  ItemId.BURNING_AMULET_5
})
class BurningAmuletItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getController().canTeleport(30, true)) {
      return;
    }
    switch (option.getText()) {
      case "rub":
        player.openDialogue(
            new OptionsDialogue(
                "All the teleports are located in the wilderness. <br> Are you sure you want to teleport to the wilderness??",
                new DialogueOption(
                    "Yes, teleport me into the wilderness!",
                    (c, s) -> {
                      if (!player.getController().canTeleport(true)) {
                        return;
                      }
                      if (player.getCombat().inRecentCombat()) {
                        player.getGameEncoder().sendMessage("You can't do this while in combat.");
                        return;
                      }
                      player.openOldDialogue("burningamulet", 0);
                    }),
                new DialogueOption("No!")));

        break;
      case "chaos temple":
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you want to teleport into the wilderness?",
                new DialogueOption(
                    "Yes, teleport me into the wilderness!",
                    (c, s) -> {
                      if (!player.getController().canTeleport(true)) {
                        return;
                      }
                      if (player.getCombat().inRecentCombat()) {
                        player.getGameEncoder().sendMessage("You can't do this while in combat.");
                        return;
                      }
                      SpellTeleport.normalTeleport(player, new Tile(3233, 3637));
                      player.getController().stopWithTeleport();
                      player.getGameEncoder().sendMessage("You teleport to the chaos temple.");
                    }),
                new DialogueOption("No!")));
        break;
      case "bandit camp":
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you want to teleport into the wilderness?",
                new DialogueOption(
                    "Yes, teleport me into the wilderness!",
                    (c, s) -> {
                      if (!player.getController().canTeleport(true)) {
                        return;
                      }
                      if (player.getCombat().inRecentCombat()) {
                        player.getGameEncoder().sendMessage("You can't do this while in combat.");
                        return;
                      }
                      SpellTeleport.normalTeleport(player, new Tile(3039, 3651));
                      player.getController().stopWithTeleport();
                      player.getGameEncoder().sendMessage("You teleport to the bandit camp.");
                    }),
                new DialogueOption("No!")));

        break;
      case "lava maze":
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you want to teleport into the wilderness?",
                new DialogueOption(
                    "Yes, teleport me into the wilderness!",
                    (c, s) -> {
                      if (!player.getController().canTeleport(true)) {
                        return;
                      }
                      if (player.getCombat().inRecentCombat()) {
                        player.getGameEncoder().sendMessage("You can't do this while in combat.");
                        return;
                      }
                      SpellTeleport.normalTeleport(player, new Tile(3027, 3839));
                      player.getController().stopWithTeleport();
                      player.getGameEncoder().sendMessage("You teleport to the lava maze.");
                    }),
                new DialogueOption("No!")));

        break;
    }
  }
}
