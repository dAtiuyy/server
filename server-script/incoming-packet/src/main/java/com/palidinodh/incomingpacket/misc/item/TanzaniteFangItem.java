package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.TANZANITE_FANG,
  ItemId.MAGIC_FANG,
  ItemId.SERPENTINE_VISAGE,
  ItemId.SERPENTINE_HELM_UNCHARGED,
  ItemId.TOXIC_BLOWPIPE_EMPTY
})
class TanzaniteFangItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    player.openDialogue(
        new OptionsDialogue(
            "Are you sure you want to dismantle the " + item.getName() + "?",
            new DialogueOption(
                "Yes, dismantle the fang for 20000 zulrah's scales",
                (c, s) -> {
                  item.remove();
                  player.getInventory().addItem(12934, 20000);
                }),
            new DialogueOption("Cancel.")));
  }
}
