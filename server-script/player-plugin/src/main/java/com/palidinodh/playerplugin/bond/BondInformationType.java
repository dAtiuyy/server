package com.palidinodh.playerplugin.bond;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.DonatorRankType;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PString;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BondInformationType {
  DONATOR_RANKS(
      "Donator Ranks",
      -1,
      PString.getString(
          "<br>",
          s -> {
            s.append("These ranks are unlocked based on your combined total of bonds.");
            s.append("");
            s.append("<col=FF981F>Total Bond Costs</col>");
            for (var rank : DonatorRankType.values()) {
              if (rank == DonatorRankType.NONE) {
                continue;
              }
              s.append(
                  "<col=FFC184>"
                      + rank.getFormattedName()
                      + "</col>: <col=FFFFFF>"
                      + PNumber.formatNumber(rank.getBonds())
                      + "</col>");
            }
            return s;
          })),
  DONATOR_TOGGLES(
      "Donator Toggles",
      -1,
      PString.getString(
          "<br>",
          s -> {
            s.append("This determines the chance of the enabled toggles activating.");
            s.append("Item noting effects always activate.");
            s.append("");
            s.append("<col=FF981F>Activation Chance</col>");
            for (var rank : DonatorRankType.values()) {
              if (rank == DonatorRankType.NONE) {
                continue;
              }
              s.append(
                  "<col=FFC184>"
                      + rank.getFormattedName()
                      + "</col>: <col=FFFFFF>1 in "
                      + rank.getActivationChance()
                      + "</col>");
            }
            s.append("");
            s.append("<col=FF981F>Toggles</col>");
            for (var effect : DonatorEffectType.values()) {
              s.append(effect.getFormattedName());
            }
            return s;
          })),
  DONATOR_MULTIPLIERS(
      "Donator Multipliers",
      -1,
      PString.getString(
          "<br>",
          s -> {
            s.append("This determines how big of a boost you receive.");
            s.append("");
            s.append("<col=FF981F>Multipliers</col>");
            for (var rank : DonatorRankType.values()) {
              if (rank == DonatorRankType.NONE) {
                continue;
              }
              s.append(
                  "<col=FFC184>"
                      + rank.getFormattedName()
                      + "</col>: <col=FFFFFF>x"
                      + rank.getMultiplier()
                      + "</col>");
            }
            s.append("");
            s.append("<col=FF981F>Boosts</col>");
            s.append("Drops/Pets Rates (Halved)");
            s.append("Experience");
            s.append("PvM Coin Drops");
            s.append("Skilling Success Rate");
            s.append("Creepy Crawly Points");
            s.append("Reduced Slayer Task Cancel Fee");
            s.append("Reduced CoX Death Penalty");
            return s;
          })),
  YELL_COMMAND(
      "Yell Command",
      -1,
      PString.getString(
          "<br>",
          s -> {
            s.append("The ::yell command lets you send messages to everyone online.");
            s.append("");
            s.append("<col=FF981F>Cooldown</col>");
            for (var rank : DonatorRankType.values()) {
              if (rank == DonatorRankType.NONE) {
                continue;
              }
              s.append(
                  "<col=FFC184>"
                      + rank.getFormattedName()
                      + "</col>: <col=FFFFFF>"
                      + rank.getYellCooldown()
                      + " seconds</col>");
            }
            return s;
          })),
  OTHER_DONATOR_BENEFITS(
      "Other Benefits",
      -1,
      PString.getString(
          "<br>",
          s -> {
            s.append("<col=FF981F>Sapphire+ Rank</col>");
            s.append("Donator zone access");
            s.append("Free daily Slayer task skip");
            s.append("Skip the first 15 waves of the Fight Cave and the Inferno");
            s.append("The Inferno: practice 3 JalTok-Jad wave");
            s.append("The Inferno: practice TzKal-Zuk");
            s.append("Set your home location");
            s.append("3 options for daily skilling task");
            s.append("<col=FF981F>Emerald+ Rank</col>");
            s.append("Skip the first 30 waves of the Fight Cave and the Inferno");
            s.append("4 options for daily skilling task");
            s.append("<col=FF981F>Ruby+ Rank</col>");
            s.append("Skip the first 45 waves of the Fight Cave and the Inferno");
            s.append("5 options for daily skilling task");
            s.append("<col=FF981F>Diamond+ Rank</col>");
            s.append("Each God Wars kill counts as +2");
            s.append("6 options for daily skilling task");
            s.append("<col=FF981F>Dragonstone+ Rank</col>");
            s.append("7 options for daily skilling task");
            s.append("<col=FF981F>Onyx+ Rank</col>");
            s.append("8 options for daily skilling task");
            s.append("<col=FF981F>Zenyte+ Rank</col>");
            s.append("Each God Wars kill counts as +3");
            s.append("9 options for daily skilling task");
            return s;
          })),
  MYSTERY_BOX(
      "Mystery Box",
      ItemId.MYSTERY_BOX,
      PString.getString(
          "<br>",
          s -> {
            s.append(
                "Mystery boxes contain many different items! These items include clue scroll loot, Barrows gear, unique monster drops, unique boss drops, and holiday rares.");
            s.append("");
            s.append("This box is limited to normal mode only.");
            return s;
          })),
  SUPER_MYSTERY_BOX(
      "Super Mystery Box",
      ItemId.SUPER_MYSTERY_BOX_32286,
      PString.getString(
          "<br>",
          s -> {
            s.append(
                "Mystery boxes contain many different items! These items include clue scroll loot, Barrows gear, unique monster drops, unique boss drops, and holiday rares.");
            s.append("Unlike the regular mystery box, you have a higher chance at better loot.");
            s.append("");
            s.append("This box is limited to normal mode only.");
            return s;
          })),
  SKILLING_MYSTERY_BOX(
      "Skilling Mystery Box",
      ItemId.SKILLING_MYSTERY_BOX_32380,
      PString.getString(
          "<br>",
          s -> {
            s.append("Obtain skilling supplies to help you on your way to maxing.");
            s.append("");
            s.append("This mystery box can be purchased by ironman modes.");
            return s;
          })),
  PET_MYSTERY_BOX(
      "Pet Mystery Box",
      ItemId.PET_MYSTERY_BOX_32311,
      PString.getString(
          "<br>",
          s -> {
            s.append("Obtain a random pet!");
            s.append("");
            s.append("This box is limited to normal mode only.");
            return s;
          })),
  SKILLING_OUTFITS(
      "Skilling Outfits",
      -1,
      PString.getString(
          "<br>",
          s -> {
            s.append(
                "When wearing a complete skilling outfit, the outfit will provide 10% more experience and increases your skilling success rate by 10%. Clue Scroll chances are increased by 10% where applicable. Some outfits provide additional boosts, outlined below.");
            s.append("");
            s.append("<col=FFC184>Graceful outfit:</col> Run energy restored 30% faster");
            s.append("<col=FFC184>Rougue outfit:</col> 10% chance to pickpocket twice");
            s.append("<col=FFC184>Cooking gauntlets:</col> Lowers to burn level of some fish");
            s.append(
                "<col=FFC184>Goldsmith gauntlets:</col> 2.5x more experience when smelting gold bars");
            return s;
          })),
  HERB_SACK(
      "Herb Sack",
      -1,
      PString.getString(
          "<br>",
          s -> {
            s.append("Can store up to 30 of each herb.");
            s.append("Doubles the number of herbs and notes them from monster drops.");
            return s;
          })),
  SEED_BOX(
      "Seed Box",
      -1,
      PString.getString(
          "<br>",
          s -> {
            s.append("Can store 6 types of seeds at a time.");
            s.append("Doubles the number of seeds from monster drops.");
            return s;
          }));

  private final String name;
  private final int mysteryBoxItemId;
  private final String description;
}
