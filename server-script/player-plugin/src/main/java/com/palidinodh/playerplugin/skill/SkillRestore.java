package com.palidinodh.playerplugin.skill;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import lombok.Getter;
import lombok.Setter;

@Getter
public class SkillRestore {

  private static final int DELAY = 100;

  @Setter private transient Player player;

  private final int id;
  private int delay = DELAY;
  private int toLimitsResetDelay;
  private int lowerToLimit;

  public SkillRestore(int id) {
    this.id = id;
  }

  public void startToLimits(int delayLimit, int lowerLimit) {
    toLimitsResetDelay = delayLimit;
    lowerToLimit = lowerLimit;
  }

  public void stopToLimits() {
    toLimitsResetDelay = 0;
    lowerToLimit = 0;
  }

  public void tick() {
    checkToLimits();
    checkLevel();
  }

  private void checkToLimits() {
    var normalLevel = player.getController().getLevelForXP(id);
    if (normalLevel > lowerToLimit) {
      stopToLimits();
      return;
    }
    if (--toLimitsResetDelay != 0) {
      return;
    }
    stopToLimits();
    if (player.getSkills().getLevel(id) < normalLevel) {
      return;
    }
    player.getSkills().setLevel(id, normalLevel);
    player.getGameEncoder().sendSkillLevel(id);
  }

  private void checkLevel() {
    if (--delay > 0) {
      return;
    }
    delay = DELAY;
    var currentLevel = player.getSkills().getLevel(id);
    var normalLevel = player.getController().getLevelForXP(id);
    if (id == Skills.HITPOINTS) {
      currentLevel = player.getCombat().getHitpoints();
      normalLevel = player.getCombat().getMaxHitpoints();
    }
    var rtl = normalLevel;
    var ltl = normalLevel;
    if (lowerToLimit > 0) {
      ltl = lowerToLimit;
    }
    if (currentLevel < rtl) {
      if (id == Skills.HITPOINTS) {
        player.getCombat().setHitpoints(currentLevel + 1);
        if (player.getPrayer().hasActive("rapid heal")
            || player.getEquipment().wearingAccomplishmentCape(Skills.HITPOINTS)) {
          delay /= 2;
        }
        if (player.getEquipment().getHandId() == ItemId.REGEN_BRACELET) {
          delay /= 2;
        }
      } else {
        player.getSkills().setLevel(id, currentLevel + 1);
        if (id != Skills.HITPOINTS && player.getPrayer().hasActive("rapid restore")) {
          delay /= 2;
        }
      }
    } else if (currentLevel > ltl) {
      if (id == Skills.HITPOINTS) {
        player.getCombat().setHitpoints(currentLevel - 1);
      } else {
        player.getSkills().setLevel(id, currentLevel - 1);
        if (id != Skills.HITPOINTS && player.getPrayer().hasActive("preserve")) {
          delay *= 1.5;
        }
      }
    }
    if (currentLevel == player.getSkills().getLevel(id)) {
      return;
    }
    player.getGameEncoder().sendSkillLevel(id);
  }
}
