package com.palidinodh.playerplugin.vote;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.osrscore.util.RequestManager;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import com.palidinodh.playerplugin.playstyle.PlayStylePlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.communication.RequestServer;
import com.palidinodh.rs.communication.request.Request;
import com.palidinodh.rs.communication.request.VoteRequest;
import com.palidinodh.rs.communication.response.VoteResponse;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.SqlUserField;
import com.palidinodh.util.PTime;
import java.util.Calendar;

public class VotePlugin implements PlayerPlugin {

  private static final int VOTED_HOURS_TIMEFRAME = 11;
  private static final int POINTS_FOR_BONUS = 8;

  @Inject private transient Player player;
  private transient VoteRequest voteData;

  private String votedDate;
  private int votedPoints;

  @Override
  public void login() {
    if (Settings.getInstance().isBeta()) {
      return;
    }
    if (player.getPlugin(PlayStylePlugin.class).getGameMode().isUnset()) {
      return;
    }
    updateVote(
        Integer.parseInt(player.getSql().getOrDefault(SqlUserField.PENDING_VOTE_POINTS, "0")));
  }

  @Override
  public void tick() {
    checkVoteData();
  }

  public void claimVotes() {
    var timeSince = player.getAttributeLong("claim_votes");
    if (timeSince > 0 && PTime.betweenMilliToSec(timeSince) < 30) {
      player.getGameEncoder().sendMessage("You can't check this again yet.");
      return;
    }
    if (voteData != null) {
      player.getGameEncoder().sendMessage("You have vote data currently pending.");
      return;
    }
    voteData = RequestServer.getInstance().addVote(VoteRequest.Type.GET, player.getId());
    player.getGameEncoder().sendMessage("Checking for votes...");
    player.putAttribute("claim_votes", PTime.currentTimeMillis());
  }

  private void updateVote(int count) {
    if (count == 0) {
      return;
    }
    voteData = RequestManager.addRemovePendingVotePoints(player);
    var calendar = PTime.getCalendar();
    var votedCalendar = votedDate == null ? null : PTime.getExactDateCalendar(votedDate);
    if (votedCalendar == null
        || calendar.get(Calendar.DAY_OF_YEAR) != votedCalendar.get(Calendar.DAY_OF_YEAR)
        || calendar.get(Calendar.HOUR_OF_DAY) - votedCalendar.get(Calendar.HOUR_OF_DAY)
            >= VOTED_HOURS_TIMEFRAME) {
      votedPoints = 0;
    }
    var votedBonus = votedPoints >= POINTS_FOR_BONUS;
    votedDate = PTime.getExactDate();
    votedPoints += count;
    player.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.VOTES, count);
    player.getBank().add(new Item(ItemId.VOTE_TICKET, count));
    if (votedPoints < POINTS_FOR_BONUS || votedBonus) {
      player
          .getGameEncoder()
          .sendMessage(
              "<col=ff0000>" + count + " vote tickets have been added to your bank.</col>");
      return;
    }
    player
        .getGameEncoder()
        .sendMessage(
            "<col=ff0000>"
                + count
                + " vote tickets and a skilling mystery box have been added to your bank, and 1 bond has been added to your pouch.</col>");
    player.getBank().add(new Item(ItemId.SKILLING_MYSTERY_BOX_32380));
    player.changeVariableCurrency("bonds", 1);
    if (PRandom.randomE(4_000) == 0) {
      var crackerItem = new Item(ItemId.PARTYHAT_CRACKER_32391);
      player.getWorld().sendItemDropNews(player, crackerItem, "from voting");
      player.getBank().add(crackerItem);
    }
    if (PRandom.randomE(2_000) == 0) {
      var crackerItem = new Item(ItemId.HALLOWEEN_CRACKER_32392);
      player.getWorld().sendItemDropNews(player, crackerItem, "from voting");
      player.getBank().add(crackerItem);
    }
    if (Main.getHolidayToken() != -1) {
      player.getBank().add(new Item(Main.getHolidayToken(), 50));
      player
          .getGameEncoder()
          .sendMessage(
              "<col=ff0000>"
                  + ItemDef.getName(Main.getHolidayToken())
                  + "s have been added to your bank.");
    }
  }

  private void checkVoteData() {
    if (voteData == null) {
      return;
    }
    if (voteData.getState() != Request.State.COMPLETE) {
      return;
    }
    if (voteData.getResponse() == null) {
      voteData = null;
      return;
    }
    var voteType = voteData.getType();
    var voteResponse = (VoteResponse) voteData.getResponse();
    var voteText = voteResponse.getText();
    voteData = null;
    if (voteType == VoteRequest.Type.GET && voteText != null && !voteText.isEmpty()) {
      updateVote(Integer.parseInt(voteText));
    }
  }
}
