package com.palidinodh.playerplugin.skill;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.util.PArrayList;

public class SkillPlugin implements PlayerPlugin {

  private static final int SKILL_RESTORE_DELAY = 100;

  @Inject private transient Player player;

  private PArrayList<SkillRestore> restores = new PArrayList<>();

  @Override
  public void login() {
    for (var i = 0; i < Skills.SKILL_COUNT; i++) {
      var id = i;
      if (id == Skills.PRAYER) {
        continue;
      }
      if (restores.containsIf(r -> id == r.getId())) {
        continue;
      }
      restores.add(new SkillRestore(id));
    }
    restores.sort(SkillRestore::getId);
    restores.forEach(r -> r.setPlayer(player));
  }

  @Override
  public void restore() {
    restores.forEach(SkillRestore::stopToLimits);
  }

  @Override
  public void tick() {
    if (!player.getCombat().isDead()) {
      restores.forEach(SkillRestore::tick);
    }
  }

  public SkillRestore getRestore(int id) {
    return restores.getIf(r -> id == r.getId());
  }
}
