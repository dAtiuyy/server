package com.palidinodh.maparea.wilderness.godwarsdungeon.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.BOULDER_6621)
class BoulderNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    if (player.getController().isMagicBound()) {
      player
          .getGameEncoder()
          .sendMessage(
              "A magical force stops you from moving for "
                  + player.getMovement().getMagicBindSeconds()
                  + " more seconds.");
      return;
    }
    if (npc.getX() == 3053 && npc.getY() == 10165) {
      if (player.getX() >= 3055) {
        player.getMovement().teleport(3052, 10165, 3);
      } else {
        player.getMovement().teleport(3055, 10165, 3);
      }
    }
  }
}
