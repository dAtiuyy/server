package com.palidinodh.maparea.dragonkinislands.fossilisland.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class FossilIslandNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3737, 3805), NpcId.FOSSIL_COLLECTOR));
    spawns.add(new NpcSpawn(new Tile(3735, 3804), NpcId.DOG_7771));
    spawns.add(new NpcSpawn(new Tile(3735, 3815), NpcId.DAVID_7766));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3732, 3812), NpcId.JOHN_7765));
    spawns.add(new NpcSpawn(4, new Tile(3737, 3820), NpcId.CHARLES_7773));
    spawns.add(new NpcSpawn(2, new Tile(3726, 3817), NpcId.SHOP_KEEPER_7769));
    spawns.add(new NpcSpawn(4, new Tile(3754, 3807), NpcId.JOHN_7774));
    spawns.add(new NpcSpawn(4, new Tile(3759, 3824), NpcId.MATTIMEO));
    spawns.add(new NpcSpawn(4, new Tile(3818, 3808), NpcId.PETRIFIED_PETE));
    spawns.add(new NpcSpawn(4, new Tile(3720, 3812), NpcId.JARDRIC));
    spawns.add(new NpcSpawn(4, new Tile(3680, 3722), NpcId.DERANGED_ARCHAEOLOGIST_276));
    spawns.add(new NpcSpawn(4, new Tile(3682, 3755), NpcId.DERANGED_ARCHAEOLOGIST_276));
    spawns.add(new NpcSpawn(4, new Tile(3685, 3767), NpcId.DERANGED_ARCHAEOLOGIST_276));

    return spawns;
  }
}
