package com.palidinodh.playerplugin.bond.handler.widget;

import com.palidinodh.cache.clientscript2.ScrollbarSizeCs2;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.bond.BondRelicCategoryType;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;

@ReferenceId(WidgetId.BOND_RELICS_1023)
class BondRelicWidget implements WidgetHandler {

  private static final int CATEGORY_LENGTH = 40;
  private static final int CATEGORY_ENTRY_CHILDREN = 2;
  private static final int CATEGORY_FIRST_CHILD = 29;

  private static final int SUB_CATEGORY_LENGTH = 40;
  private static final int SUB_CATEGORY_ENTRY_CHILDREN = 2;
  private static final int SUB_CATEGORY_FIRST_CHILD = 156;

  private static final int DESCRIPTION_OPTION_CHILD_ID = 284;

  private static void loadCategories(Player player) {
    var selectedCategory = (BondRelicCategoryType) player.getAttribute("relic_category");
    var categories = BondRelicCategoryType.values();
    var plugin = player.getPlugin(BondPlugin.class);

    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.BOND_RELICS_1023, 17, "Bonds: " + PNumber.formatNumber(plugin.getPouch()));

    player
        .getGameEncoder()
        .sendClientScriptData(
            ScrollbarSizeCs2.builder()
                .root(WidgetId.BOND_RELICS_1023)
                .container(28)
                .scrollbar(27)
                .height(20 * categories.length)
                .build());

    for (var i = 0; i < 12; i++) {
      var childOffset = i * (CATEGORY_ENTRY_CHILDREN + 1);
      player
          .getGameEncoder()
          .sendHideWidget(
              WidgetId.BOND_RELICS_1023,
              CATEGORY_FIRST_CHILD + childOffset,
              i >= categories.length);
    }

    for (var i = 0; i < categories.length; i++) {
      var category = categories[i];
      var childOffset = i * (CATEGORY_ENTRY_CHILDREN + 1) + CATEGORY_ENTRY_CHILDREN;
      var color = "";
      if (isCategoryUnlocked(player, category)) {
        color = "<col=33CC00>";
      }
      if (category == selectedCategory) {
        color = "<col=FFFFFF>";
        loadRelics(player, category);
      }
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.BOND_RELICS_1023,
              CATEGORY_FIRST_CHILD + childOffset,
              color + category.getName());
    }
  }

  private static void loadRelics(Player player, BondRelicCategoryType category) {
    var selectedRelic = (BondRelicType) player.getAttribute("relic_sub_category");
    var relics = category.getRelics();
    var plugin = player.getPlugin(BondPlugin.class);

    player
        .getGameEncoder()
        .sendClientScriptData(
            ScrollbarSizeCs2.builder()
                .root(WidgetId.BOND_RELICS_1023)
                .container(155)
                .scrollbar(154)
                .height(20 * relics.length)
                .build());

    for (var i = 0; i < 12; i++) {
      var childOffset = i * (SUB_CATEGORY_ENTRY_CHILDREN + 1);
      player
          .getGameEncoder()
          .sendHideWidget(
              WidgetId.BOND_RELICS_1023,
              SUB_CATEGORY_FIRST_CHILD + childOffset,
              relics.length == 1 || i >= relics.length);
    }

    for (var i = 0; i < relics.length; i++) {
      var relic = relics[i];
      var childOffset = i * (SUB_CATEGORY_ENTRY_CHILDREN + 1) + SUB_CATEGORY_ENTRY_CHILDREN;
      var color = "";
      if (plugin.getRelicsUnlocked().contains(relic)) {
        color = "<col=33CC00>";
      }
      if (relic == selectedRelic) {
        color = "<col=FFFFFF>";
        loadRelic(player, category, relic);
      }
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.BOND_RELICS_1023,
              SUB_CATEGORY_FIRST_CHILD + childOffset,
              color + relic.getName());
    }
  }

  private static void loadRelic(
      Player player, BondRelicCategoryType category, BondRelicType relic) {
    var plugin = player.getPlugin(BondPlugin.class);
    var name = relic.getName();
    if (!category.getName().equals(relic.getName())) {
      name = category.getName() + ":<br>" + relic.getName();
    }
    player.getGameEncoder().sendWidgetText(WidgetId.BOND_RELICS_1023, 282, name);
    player.getGameEncoder().sendWidgetText(WidgetId.BOND_RELICS_1023, 283, relic.getDescription());
    player
        .getGameEncoder()
        .sendHideWidget(
            WidgetId.BOND_RELICS_1023,
            DESCRIPTION_OPTION_CHILD_ID,
            plugin.getRelicsUnlocked().contains(relic));
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.BOND_RELICS_1023,
            285,
            "Unlock for<br>" + PNumber.formatNumber(relic.getCost()) + " Bonds");
  }

  private static boolean isCategoryUnlocked(Player player, BondRelicCategoryType category) {
    var plugin = player.getPlugin(BondPlugin.class);
    for (var relic : category.getRelics()) {
      if (plugin.getRelicsUnlocked().contains(relic)) {
        continue;
      }
      return false;
    }
    return true;
  }

  @Override
  public void onOpen(Player player) {
    var category = BondRelicCategoryType.values()[0];
    var subCategory = category.getRelics()[0];
    player.putAttribute("relic_category", category);
    player.putAttribute("relic_sub_category", subCategory);
    loadCategories(player);
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var plugin = player.getPlugin(BondPlugin.class);
    if (childId >= CATEGORY_FIRST_CHILD
        && childId <= CATEGORY_FIRST_CHILD + CATEGORY_LENGTH * (CATEGORY_ENTRY_CHILDREN + 1)) {
      var index = (childId - CATEGORY_FIRST_CHILD) / (CATEGORY_ENTRY_CHILDREN + 1);
      var categories = BondRelicCategoryType.values();
      if (index >= categories.length) {
        return;
      }
      var category = categories[index];
      var subCategory = category.getRelics()[0];
      player.putAttribute("relic_category", category);
      player.putAttribute("relic_sub_category", subCategory);
      loadCategories(player);
    }
    if (childId >= SUB_CATEGORY_FIRST_CHILD
        && childId
            <= SUB_CATEGORY_FIRST_CHILD + SUB_CATEGORY_LENGTH * (SUB_CATEGORY_ENTRY_CHILDREN + 1)) {
      var category = (BondRelicCategoryType) player.getAttribute("relic_category");
      var index = (childId - SUB_CATEGORY_FIRST_CHILD) / (SUB_CATEGORY_ENTRY_CHILDREN + 1);
      var relics = category.getRelics();
      if (index >= relics.length) {
        return;
      }
      var subCategory = relics[index];
      player.putAttribute("relic_sub_category", subCategory);
      loadRelics(player, category);
    }
    if (childId == DESCRIPTION_OPTION_CHILD_ID) {
      var category = (BondRelicCategoryType) player.getAttribute("relic_category");
      var relic = (BondRelicType) player.getAttribute("relic_sub_category");
      if (plugin.getRelicsUnlocked().contains(relic)) {
        player.getGameEncoder().sendMessage("You have already unlocked this relic.");
        return;
      }
      if (plugin.getPouch() < relic.getCost()) {
        player.getGameEncoder().sendMessage("You don't have enough bonds to unlock this.");
        return;
      }
      plugin.setPouch(plugin.getPouch() - relic.getCost());
      plugin.setTotalRedeemed(plugin.getTotalRedeemed() + relic.getCost());
      plugin.getRelicsUnlocked().add(relic);
      var name = relic.getName();
      if (!category.getName().equals(relic.getName())) {
        name = category.getName() + ": " + relic.getName();
      }
      player.getGameEncoder().sendMessage("Relic unlocked: " + name);
      loadCategories(player);
    }
  }
}
