package com.palidinodh.incomingpacket.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Farming;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({NpcId.TOOL_LEPRECHAUN, NpcId.TOOL_LEPRECHAUN_757, NpcId.TOOL_LEPRECHAUN_7757})
class ToolLeprechaunNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    player.getGameEncoder().sendMessage("The leprechaun will note items for you.");
  }

  @Override
  public void itemOnNpc(Player player, int slot, int itemId, Npc npc) {
    var item = player.getInventory().getItem(slot);
    if (item.getDef().getNotedId() == -1) {
      return;
    }
    var notedId = item.getDef().getNotedId();
    if (!Farming.isCollectable(item.getId()) || notedId == -1) {
      return;
    }
    var exchangeCount2 = player.getInventory().getCount(item.getId());
    player.getInventory().deleteItem(item.getId(), exchangeCount2);
    player.getInventory().addItem(notedId, exchangeCount2);
  }
}
