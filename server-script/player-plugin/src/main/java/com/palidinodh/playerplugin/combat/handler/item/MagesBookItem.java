package com.palidinodh.playerplugin.combat.handler.item;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.MAGES_BOOK)
class MagesBookItem implements ItemHandler {

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    if (ItemHandler.used(useItem, onItem, ItemId.MAGES_BOOK, ItemId.MTA_ORNAMENT_KIT_60061)) {
      onItem.replace(new Item(ItemId.MAGES_BOOK_OR_60051));
      useItem.remove();
      return true;
    }
    return false;
  }
}
