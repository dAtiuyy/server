package com.palidinodh.worldevent.groupboss;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Setter;

public class GroupBossEvent extends WorldEvent {

  private static final int SOON_MINUTES = 15;
  private static final String[] TIME = {"2:00", "5:00", "8:00", "14:00", "17:00", "20:00"};
  private static final int[] HOURS;
  private static final int[] MINUTES;
  private static final NpcSpawn JAD_SPAWN =
      new NpcSpawn(
          8, new Tile(2399, 5086), NpcId.JAD_900_16065, NpcId.JAD_900_16066, NpcId.JAD_900_16068);
  private static final Tile JAD_COMMAND_TILE = new Tile(2438, 5169);
  private static final Tile JAD_ENTER_TILE = new Tile(2413, 5115);

  @Setter private static boolean enabled = true;

  static {
    if (TIME == null || TIME.length == 0) {
      HOURS = null;
      MINUTES = null;
    } else {
      HOURS = new int[TIME.length];
      MINUTES = new int[TIME.length];
      for (int i = 0; i < TIME.length; i++) {
        String[] data = TIME[i].split(":");
        HOURS[i] = Integer.parseInt(data[0]);
        MINUTES[i] = Integer.parseInt(data[1]);
      }
    }
  }

  @Inject private transient World world;
  private transient Npc boss;
  private transient List<Integer> players = new ArrayList<>();

  public GroupBossEvent() {
    super(4);
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("group_boss_message")) {
      return getNextTimeText();
    }
    return null;
  }

  @Override
  public void execute() {
    setTick(4);
    if (boss != null && !boss.isVisible()) {
      boss = null;
    }
    if (!canRun()) {
      return;
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    int dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    int[] nextTime = getNextTime();
    if (nextTime == null) {
      return;
    }
    int remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    if (remainingMinutes == SOON_MINUTES) {
      var message = "Jad will spawn in " + SOON_MINUTES + " minutes!";
      world.sendNews(message + " Use ::jadboss to teleport there!");
      DiscordBot.sendMessage(DiscordChannel.EVENTS, message);
      setTick(105);
    } else if (remainingMinutes == 0) {
      spawn();
      setTick(105);
    }
  }

  @Override
  public boolean useHandlers(Player player) {
    return boss != null;
  }

  public void commandTeleport(Player player) {
    if (!canRun() || boss == null || boss.getCombat().isDead()) {
      player.getGameEncoder().sendMessage("Jad is currently unavailable.");
      return;
    }
    if (PRandom.getPercent(boss.getCombat().getHitpoints(), boss.getCombat().getMaxHitpoints())
        < 75) {
      player.getGameEncoder().sendMessage("The event is no longer taking new entrants.");
      return;
    }
    if (players.contains(player.getId())) {
      player.getGameEncoder().sendMessage("This event can't be re-entered.");
      return;
    }
    player.getMovement().teleport(JAD_COMMAND_TILE);
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
  }

  public void enterTeleport(Player player) {
    if (!canRun() || boss == null || boss.getCombat().isDead()) {
      player.getGameEncoder().sendMessage("Jad is currently unavailable.");
      return;
    }
    if (PRandom.getPercent(boss.getCombat().getHitpoints(), boss.getCombat().getMaxHitpoints())
        < 75) {
      player.getGameEncoder().sendMessage("The event is no longer taking new entrants.");
      return;
    }
    if (players.contains(player.getId())) {
      player.getGameEncoder().sendMessage("This event can't be re-entered.");
      return;
    }
    players.add(player.getId());
    player.setController(new GroupBossController(boss));
    player.getController().joinInstance(boss.getController());
    player.getMovement().teleport(JAD_ENTER_TILE);
  }

  public void spawn() {
    if (!canRun()) {
      return;
    }
    players.clear();
    world.removeNpc(boss);
    boss = world.addNpc(JAD_SPAWN);
    boss.getController().startInstance();
    boss.setLock(50);
    boss.getCombat()
        .addContinuousEvent(
            1,
            e -> {
              if (!boss.isLocked()) {
                var playerCount = Math.min(boss.getController().getPlayers().size(), 10);
                var hitpoints = boss.getCombat().getMaxHitpoints() + (playerCount * 1000);
                boss.getCombat().setHitpoints(hitpoints);
                boss.getCombat().setMaxHitpoints(hitpoints);
                e.stop();
                return;
              }
              boss.setForceMessage(Long.toString(PTime.tickToSec(boss.getLock())));
            });
    var message = "Jad has spawned!";
    world.sendBroadcast(message + " Use ::jadboss to teleport there!");
    world.sendNews("Event: deaths are safe.");
    DiscordBot.sendMessage(DiscordChannel.EVENTS, message);
    DiscordBot.sendMessage(DiscordChannel.GENERAL_CHAT, message);
  }

  private String getNextTimeText() {
    if (boss != null) {
      return "Spawned";
    }
    if (!canRun()) {
      return "N/A";
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    int dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    int[] nextTime = getNextTime();
    int remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    return PTime.ticksToLongDuration(PTime.minToTick(remainingMinutes));
  }

  private int[] getNextTime() {
    if (!canRun()) {
      return null;
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    for (int i = 0; i < HOURS.length; i++) {
      int hour = HOURS[i];
      int minute = MINUTES[i];
      if (currentHour > hour || currentHour == hour && currentMinute > minute) {
        continue;
      }
      return new int[] {hour, minute};
    }
    return new int[] {HOURS[0], MINUTES[0]};
  }

  private boolean canRun() {
    if (!enabled) {
      return false;
    }
    if (TIME == null) {
      return false;
    }
    if (TIME.length == 0) {
      return false;
    }
    return world.isPrimary();
  }
}
