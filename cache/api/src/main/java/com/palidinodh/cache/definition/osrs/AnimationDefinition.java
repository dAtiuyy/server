package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.definition.Definition;
import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Stream;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public class AnimationDefinition implements Definition {

  private static final AnimationDefinition DEFAULT = new AnimationDefinition(-1);

  @Getter private static AnimationDefinition[] definitions;

  private int id;
  private int[] frameLengths = {};
  private int[] frameIds = {};
  private int[] chatFrameIds = {};
  private int[] frameSounds = {};
  private int frameStep = -1;
  private int[] interleaveLeave = {};
  private boolean stretches;
  private int forcedPriority = 5;
  private int leftHandItem = -1;
  private int rightHandItem = -1;
  private int maxLoops = 99;
  private int precedenceAnimating = -1;
  private int priority = -1;
  private int replyMode = 2;
  private transient List<String> frames = new ArrayList<>();
  private transient List<Integer> skeletons = new ArrayList<>();

  public AnimationDefinition(int id) {
    this.id = id;
  }

  public static AnimationDefinition getDefinition(int id) {
    if (definitions == null) {
      return DEFAULT;
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id]
        : DEFAULT;
  }

  public static int size() {
    return definitions.length;
  }

  public static synchronized void load(boolean force, Store store) {
    AnimationFrameDefinition.load(force, store);
    if (!force && definitions != null) {
      return;
    }
    try {
      var files = store.getFiles(IndexType.CONFIG, ConfigType.ANIMATION);
      definitions = new AnimationDefinition[files.get(files.size() - 1).getFileId() + 1];
      for (var file : files) {
        var def = definitions[file.getFileId()] = new AnimationDefinition(file.getFileId());
        if (file.getContents() != null) {
          def.load(new Stream(file.getContents()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    return Integer.toString(id);
  }

  @Override
  public void load(Stream stream) {
    while (true) {
      var opcode = stream != null ? stream.readUnsignedByte() : 0;
      if (opcode == 0) {
        break;
      }
      switch (opcode) {
        case 1:
          {
            var length = stream.readUnsignedShort();
            frameLengths = new int[length];
            for (var i = 0; i < length; i++) {
              frameLengths[i] = stream.readUnsignedShort();
            }
            frameIds = new int[length];
            for (var i = 0; i < length; i++) {
              frameIds[i] = stream.readUnsignedShort();
            }
            for (var i = 0; i < length; i++) {
              frameIds[i] += stream.readUnsignedShort() << 16;
            }
            break;
          }
        case 2:
          frameStep = stream.readUnsignedShort();
          break;
        case 3:
          {
            var length = stream.readUnsignedByte();
            interleaveLeave = new int[length + 1];
            for (var i = 0; i < length; i++) {
              interleaveLeave[i] = stream.readUnsignedByte();
            }
            interleaveLeave[length] = 9999999;
            break;
          }
        case 4:
          stretches = true;
          break;
        case 5:
          forcedPriority = stream.readUnsignedByte();
          break;
        case 6:
          leftHandItem = stream.readUnsignedShort();
          break;
        case 7:
          rightHandItem = stream.readUnsignedShort();
          break;
        case 8:
          maxLoops = stream.readUnsignedByte();
          break;
        case 9:
          precedenceAnimating = stream.readUnsignedByte();
          break;
        case 10:
          priority = stream.readUnsignedByte();
          break;
        case 11:
          replyMode = stream.readUnsignedByte();
          break;
        case 12:
          {
            var length = stream.readUnsignedByte();
            chatFrameIds = new int[length];
            for (var i = 0; i < length; i++) {
              chatFrameIds[i] = stream.readUnsignedShort();
            }
            for (var i = 0; i < length; i++) {
              chatFrameIds[i] += stream.readUnsignedShort() << 16;
            }
            break;
          }
        case 13:
          {
            var length = stream.readUnsignedByte();
            frameSounds = new int[length];
            for (var i = 0; i < length; i++) {
              frameSounds[i] = stream.readTriByte();
            }
            break;
          }
        default:
          System.out.println("Animation Definitions Unknown Opcode: " + opcode);
          break;
      }
    }
    postLoad();
  }

  @Override
  public void postLoad() {
    frames.clear();
    skeletons.clear();
    for (int frameId : frameIds) {
      var archiveId = frameId >> 16;
      var fileId = frameId & 0xFFFF;
      frames.add("{" + archiveId + ", " + fileId + "}");
      var frameDefinition = AnimationFrameDefinition.getDefinition(archiveId);
      if (!skeletons.contains(frameDefinition.getSkeletonId())) {
        skeletons.add(frameDefinition.getSkeletonId());
      }
    }
  }

  @Override
  public Stream save(Stream stream) {
    if (frameLengths != null && frameLengths.length != 0) {
      if (frameLengths.length > 0xFFFF) {
        throw new IllegalArgumentException(id + ": frameLengths.length");
      }
      if (frameLengths.length != frameIds.length) {
        throw new IllegalArgumentException(id + ": frameLengths.length != frameIds.length");
      }
      stream.writeByte(1);
      stream.writeShort(frameLengths.length);
      for (int i : frameLengths) {
        if (i < 0 || i > 0xFFFF) {
          throw new IllegalArgumentException(id + ": frameLengths");
        }
        stream.writeShort(i);
      }
      for (int i : frameIds) {
        var archiveId = i >> 16;
        stream.writeShort(archiveId);
      }
      for (int i : frameIds) {
        var fileId = i & 0xFFFF;
        stream.writeShort(fileId);
      }
    }
    if (frameStep != -1) {
      if (frameStep < 0 || frameStep > 0xFFFF) {
        throw new IllegalArgumentException(id + ": frameStep");
      }
      stream.writeByte(2);
      stream.writeShort(frameStep);
    }
    if (interleaveLeave != null && interleaveLeave.length != 0) {
      if (interleaveLeave.length - 1 > 0xFF) {
        throw new IllegalArgumentException(id + ": frameLengths.length");
      }
      stream.writeByte(3);
      stream.writeByte(interleaveLeave.length - 1);
      for (var i = 0; i < interleaveLeave.length - 1; i++) {
        if (i < 0 || i > 0xFF) {
          throw new IllegalArgumentException(id + ": interleaveLeave");
        }
        stream.writeByte(interleaveLeave[i]);
      }
    }
    if (stretches) {
      stream.writeByte(4);
    }
    if (forcedPriority != 5) {
      if (forcedPriority < 0 || forcedPriority > 0xFF) {
        throw new IllegalArgumentException(id + ": forcedPriority");
      }
      stream.writeByte(5);
      stream.writeByte(forcedPriority);
    }
    if (leftHandItem != -1) {
      if (leftHandItem < 0 || leftHandItem > 0xFFFF) {
        throw new IllegalArgumentException(id + ": leftHandItem");
      }
      stream.writeByte(6);
      stream.writeShort(leftHandItem);
    }
    if (rightHandItem != -1) {
      if (rightHandItem < 0 || rightHandItem > 0xFFFF) {
        throw new IllegalArgumentException(id + ": rightHandItem");
      }
      stream.writeByte(7);
      stream.writeShort(rightHandItem);
    }
    if (maxLoops != 99) {
      if (maxLoops < 0 || maxLoops > 0xFF) {
        throw new IllegalArgumentException(id + ": maxLoops");
      }
      stream.writeByte(8);
      stream.writeByte(maxLoops);
    }
    if (precedenceAnimating != -1) {
      if (precedenceAnimating < 0 || precedenceAnimating > 0xFF) {
        throw new IllegalArgumentException(id + ": precedenceAnimating");
      }
      stream.writeByte(9);
      stream.writeByte(precedenceAnimating);
    }
    if (priority != -1) {
      if (priority < 0 || priority > 0xFF) {
        throw new IllegalArgumentException(id + ": priority");
      }
      stream.writeByte(10);
      stream.writeByte(priority);
    }
    if (replyMode != 2) {
      if (replyMode < 0 || replyMode > 0xFF) {
        throw new IllegalArgumentException(id + ": replyMode");
      }
      stream.writeByte(11);
      stream.writeByte(replyMode);
    }
    if (chatFrameIds != null && chatFrameIds.length != 0) {
      if (chatFrameIds.length > 0xFF) {
        throw new IllegalArgumentException(id + ": chatFrameIds.length");
      }
      stream.writeByte(12);
      stream.writeByte(chatFrameIds.length);
      for (int i : chatFrameIds) {
        i &= 0xFFFF;
        if (i < 0 || i > 0xFFFF) {
          throw new IllegalArgumentException(id + ": chatFrameIds");
        }
        stream.writeShort(i);
      }
      for (int i : chatFrameIds) {
        i >>= 16;
        if (i < 0 || i > 0xFFFF) {
          throw new IllegalArgumentException(id + ": chatFrameIds");
        }
        stream.writeShort(i);
      }
    }
    if (frameSounds != null && frameSounds.length != 0) {
      if (frameSounds.length > 0xFF) {
        throw new IllegalArgumentException(id + ": frameSounds.length");
      }
      stream.writeByte(13);
      for (var i : frameSounds) {
        stream.writeTriByte(i);
      }
    }
    stream.writeByte(0);
    return stream;
  }

  @Override
  public boolean isMatch(String s) {
    s = s.toLowerCase();
    var hypenIndex = s.indexOf(':');
    if (hypenIndex == -1) {
      return false;
    }
    var fieldNameSearch = s.substring(0, hypenIndex).strip();
    var fieldValueSearch = s.substring(hypenIndex + 1).strip();
    if ("frame".contains(fieldNameSearch)) {
      for (var frame : frames) {
        if (frame.startsWith("{" + fieldValueSearch + ",")) {
          return true;
        }
      }
      return false;
    }
    if ("skeleton".contains(fieldNameSearch)) {
      for (var skeleton : skeletons) {
        if (Integer.toString(skeleton).equals(fieldValueSearch)) {
          return true;
        }
      }
      return false;
    }
    return Definition.isFieldMatch(s, this);
  }

  @Override
  public Definition getDefaultDefinition() {
    return DEFAULT;
  }

  @Override
  public AnimationDefinition[] allDefinitions() {
    return definitions;
  }
}
