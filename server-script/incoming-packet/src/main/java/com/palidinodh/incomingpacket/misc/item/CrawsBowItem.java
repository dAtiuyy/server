package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.CRAWS_BOW)
class CrawsBowItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "uncharge":
        if (player.getInventory().getRemainingSlots() < 1) {
          player.getInventory().notEnoughSpace();
          return;
        }
        player.getInventory().addItem(21820, item.getCharges());
        item.replace(new Item(ItemId.CRAWS_BOW_U));
        break;
    }
  }
}
