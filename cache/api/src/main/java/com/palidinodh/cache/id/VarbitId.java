package com.palidinodh.cache.id;

public final class VarbitId {

  public static final int COMBAT_WEAPON_TYPE = 357;
  public static final int IN_WILDERNESS = 5963;
  public static final int WILDERNESS_KDR_1 = 4143;
  public static final int WILDERNESS_KDR_2 = 5963;
  public static final int DUEL_DISABLE_EQUIP_SLOTS = 642;
  public static final int DUEL_DISABLE_HEAD_SLOT = 643;
  public static final int DUEL_DISABLE_CAPE_SLOT = 644;
  public static final int DUEL_DISABLE_NECK_SLOT = 645;
  public static final int DUEL_DISABLE_WEAPON_SLOT = 646;
  public static final int DUEL_DISABLE_CHEST_SLOT = 647;
  public static final int DUEL_DISABLE_SHIELD_SLOT = 648;
  public static final int DUEL_DISABLE_EQUIP_SLOT_6 = 649;
  public static final int DUEL_DISABLE_LEG_SLOT = 650;
  public static final int DUEL_DISABLE_EQUIP_SLOT_8 = 651;
  public static final int DUEL_DISABLE_HAND_SLOT = 652;
  public static final int DUEL_DISABLE_FEET_SLOT = 653;
  public static final int DUEL_DISABLE_EQUIP_SLOT_11 = 654;
  public static final int DUEL_DISABLE_RING_SLOT = 655;
  public static final int DUEL_DISABLE_AMMO_SLOT = 656;
  public static final int HOT_AIR_BALLOON_ENTRANA = 2867;
  public static final int HOT_AIR_BALLOON_TAVERLEY = 2868;
  public static final int HOT_AIR_BALLOON_CASTLE_WARS = 2869;
  public static final int HOT_AIR_BALLOON_GRAND_TREE = 2870;
  public static final int HOT_AIR_BALLOON_CRAFTING_GUILD = 2871;
  public static final int HOT_AIR_BALLOON_VARROCK = 2872;
  public static final int GOD_WARS_ENTRANCE_ROPE = 3966;
  public static final int GOD_WARS_SARADOMIN_FIRST_ROPE = 3967;
  public static final int GOD_WARS_SARADOMIN_SECOND_ROPE = 3968;
  public static final int GOD_WARS_SARADOMIN_KILLS = 3972;
  public static final int GOD_WARS_ARMADYL_KILLS = 3973;
  public static final int GOD_WARS_BANDOS_KILLS = 3975;
  public static final int GOD_WARS_ZAMORAK_KILLS = 3976;
  public static final int GOD_WARS_SARADOMINS_LIGHT = 4733;
  public static final int DIARY_FALADOR_STARTED = 4449;
  public static final int DIARY_ARDOUGNE_EASY_COMPLETE_1 = 4458;
  public static final int DIARY_ARDOUGNE_MEDIUM_COMPLETE_1 = 4459;
  public static final int DIARY_ARDOUGNE_HARD_COMPLETE_1 = 4460;
  public static final int DIARY_ARDOUGNE_ELITE_COMPLETE_1 = 4461;
  public static final int DIARY_DESERT_EASY_COMPLETE_1 = 4483;
  public static final int DIARY_DESERT_MEDIUM_COMPLETE_1 = 4484;
  public static final int DIARY_DESERT_HARD_COMPLETE_1 = 4485;
  public static final int DIARY_DESERT_ELITE_COMPLETE_1 = 4486;
  public static final int DIARY_FALADOR_EASY_COMPLETE_1 = 4462;
  public static final int DIARY_FALADOR_MEDIUM_COMPLETE_1 = 4463;
  public static final int DIARY_FALADOR_HARD_COMPLETE_1 = 4464;
  public static final int DIARY_FALADOR_ELITE_COMPLETE_1 = 4465;
  public static final int DIARY_FALADOR_EASY_COMPLETE_2 = 4503;
  public static final int DIARY_FALADOR_MEDIUM_COMPLETE_2 = 4504;
  public static final int DIARY_FALADOR_HARD_COMPLETE_2 = 4505;
  public static final int DIARY_FALADOR_ELITE_COMPLETE_2 = 4506;
  public static final int DIARY_FREMENNIK_EASY_COMPLETE_1 = 4491;
  public static final int DIARY_FREMENNIK_MEDIUM_COMPLETE_1 = 4492;
  public static final int DIARY_FREMENNIK_HARD_COMPLETE_1 = 4493;
  public static final int DIARY_FREMENNIK_ELITE_COMPLETE_1 = 4494;
  public static final int DIARY_KANDARIN_EASY_COMPLETE_1 = 4475;
  public static final int DIARY_KANDARIN_MEDIUM_COMPLETE_1 = 4476;
  public static final int DIARY_KANDARIN_HARD_COMPLETE_1 = 4477;
  public static final int DIARY_KANDARIN_ELITE_COMPLETE_1 = 4478;
  public static final int DIARY_KARAMJA_EASY_COMPLETE_1 = 3578;
  public static final int DIARY_KARAMJA_MEDIUM_COMPLETE_1 = 3599;
  public static final int DIARY_KARAMJA_HARD_COMPLETE_1 = 3611;
  public static final int DIARY_KARAMJA_ELITE_COMPLETE_1 = 4566;
  public static final int DIARY_LUMBRIDGE_EASY_COMPLETE_1 = 4495;
  public static final int DIARY_LUMBRIDGE_MEDIUM_COMPLETE_1 = 4496;
  public static final int DIARY_LUMBRIDGE_HARD_COMPLETE_1 = 4497;
  public static final int DIARY_LUMBRIDGE_ELITE_COMPLETE_1 = 4498;
  public static final int DIARY_MORYTANIA_EASY_COMPLETE_1 = 4487;
  public static final int DIARY_MORYTANIA_MEDIUM_COMPLETE_1 = 4488;
  public static final int DIARY_MORYTANIA_HARD_COMPLETE_1 = 4489;
  public static final int DIARY_MORYTANIA_ELITE_COMPLETE_1 = 4490;
  public static final int DIARY_VARROCK_EASY_COMPLETE_1 = 4479;
  public static final int DIARY_VARROCK_MEDIUM_COMPLETE_1 = 4480;
  public static final int DIARY_VARROCK_HARD_COMPLETE_1 = 4481;
  public static final int DIARY_VARROCK_ELITE_COMPLETE_1 = 4482;
  public static final int DIARY_WESTERN_EASY_COMPLETE_1 = 4471;
  public static final int DIARY_WESTERN_MEDIUM_COMPLETE_1 = 4472;
  public static final int DIARY_WESTERN_HARD_COMPLETE_1 = 4473;
  public static final int DIARY_WESTERN_ELITE_COMPLETE_1 = 4474;
  public static final int DIARY_WILDERNESS_EASY_COMPLETE_1 = 4466;
  public static final int DIARY_WILDERNESS_MEDIUM_COMPLETE_1 = 4467;
  public static final int DIARY_WILDERNESS_HARD_COMPLETE_1 = 4468;
  public static final int DIARY_WILDERNESS_ELITE_COMPLETE_1 = 4469;
  public static final int DIARY_FALADOR_EASY_COUNT = 6299;
  public static final int DIARY_FALADOR_MEDIUM_COUNT = 6300;
  public static final int DIARY_FALADOR_HARD_COUNT = 6301;
  public static final int DIARY_FALADOR_ELITE_COUNT = 6302;
  public static final int SPELLBOOK_SHOW_COMBAT = 6605;
  public static final int SPELLBOOK_SHOW_UTILITY = 6606;
  public static final int SPELLBOOK_SHOW_LACK_LEVEL = 6607;
  public static final int SPELLBOOK_SHOW_LACK_RUNES = 6608;
  public static final int SPELLBOOK_SHOW_TELEPORT = 6609;
  public static final int SPELLBOOK_FILTERING = 6718;
  public static final int MAGIC_VENGEANCE_ACIVE = 2450;
  public static final int MAGIC_VENGEANCE_COOLDOWN = 2451;
  public static final int MAGIC_DEFENSIVE = 2668;
  public static final int MAGIC_SPELLBOOK = 4070;
  public static final int MAGIC_BOUNTY_TELEPORT_UNLOCKED = 2010;
  public static final int BANK_QUANTITY = 6590;
  public static final int SLAYER_GROTESQUE_GUARDIANS_DOOR = 2468;
  public static final int SLAYER_STOP_THE_WYVERN_UNLOCK = 240;
  public static final int SLAYER_SEEING_RED_UNLOCK = 2462;
  public static final int SLAYER_MALEVOLENT_MASQUERADE_UNLOCK = 3202;
  public static final int SLAYER_RING_BLING_UNLOCK = 3207;
  public static final int SLAYER_BROADER_FLETCHING_UNLOCK = 3208;
  public static final int SLAYER_GARGOYLE_SMASHER_UNLOCK = 4027;
  public static final int SLAYER_SLUG_SALTER_UNLOCK = 4028;
  public static final int SLAYER_REPTILE_FREEZER_UNLOCK = 4029;
  public static final int SLAYER_SHROOM_SPRAYER_UNLOCK = 4030;
  public static final int SLAYER_NEED_MORE_DARKNESS_UNLOCK = 4031;
  public static final int SLAYER_ANKOU_VERY_MUCH_UNLOCK = 4085;
  public static final int SLAYER_SUQ_A_NOTHER_ONE_UNLOCK = 4086;
  public static final int SLAYER_FIRE_AND_DARKNESS_UNLOCK = 4087;
  public static final int SLAYER_PEDAL_TO_THE_METALS_UNLOCK = 4088;
  public static final int SLAYER_AUGMENT_MY_ABBIES_UNLOCK = 4090;
  public static final int SLAYER_ITS_DARK_IN_HERE_UNLOCK = 4091;
  public static final int SLAYER_GREATER_CHALLENGE_UNLOCK = 4092;
  public static final int SLAYER_I_HOPE_YOU_MITH_ME_UNLOCK = 4094;
  public static final int SLAYER_WATCH_THE_BIRDIE_UNLOCK = 4095;
  public static final int SLAYER_DULY_NOTED_UNLOCK = 4589;
  public static final int SLAYER_HOT_STUFF_UNLOCK = 4691;
  public static final int SLAYER_LIKE_A_BOSS_UNLOCK = 4724;
  public static final int SLAYER_BLEED_ME_DRY_UNLOCK = 4746;
  public static final int SLAYER_SMELL_YA_LATER_UNLOCK = 4747;
  public static final int SLAYER_BIRDS_OF_A_FEATHER_UNLOCK = 4748;
  public static final int SLAYER_I_REALLY_MITH_YOU_UNLOCK = 4749;
  public static final int SLAYER_HORRORIFIC_UNLOCK = 4750;
  public static final int SLAYER_TO_DUST_YOU_SHALL_RETURN_UNLOCK = 4751;
  public static final int SLAYER_WYVER_NOTHER_ONE_UNLOCK = 4752;
  public static final int SLAYER_GET_SMASHED_UNLOCK = 4753;
  public static final int SLAYER_NECHS_PLEASE_UNLOCK = 4754;
  public static final int SLAYER_KRACK_ON_UNLOCK = 4755;
  public static final int SLAYER_SPIRITUAL_FERVOUR_UNLOCK = 4757;
  public static final int SLAYER_REPTILE_GOT_RIPPED_UNLOCK = 4996;
  public static final int SLAYER_KING_BLACK_BONNET_UNLOCK = 5080;
  public static final int SLAYER_KALPHITE_KHAT_UNLOCK = 5081;
  public static final int SLAYER_UNHOLY_HELMET_UNLOCK = 5082;
  public static final int SLAYER_BIGGER_AND_BADDER_UNLOCK = 5358;
  public static final int SLAYER_GET_SCABARIGHT_ON_IT_UNLOCK = 5359;
  public static final int SLAYER_DARK_MANTLE_UNLOCK = 5631;
  public static final int SLAYER_WYVER_NOTHER_TWO_UNLOCK = 5733;
  public static final int SLAYER_ADAMIND_SOME_MORE_UNLOCK = 6094;
  public static final int SLAYER_RUUUUUNE_UNLOCK = 6095;
  public static final int SLAYER_UNDEAD_HEAD_UNLOCK = 6096;
  public static final int SLAYER_DOUBLE_TROUBLE_UNLOCK = 6485;
  public static final int SLAYER_USE_MORE_HEAD_UNLOCK = 6570;
  public static final int SLAYER_BLOCKED_TASK_1 = 3209;
  public static final int SLAYER_BLOCKED_TASK_2 = 3210;
  public static final int SLAYER_BLOCKED_TASK_3 = 3211;
  public static final int SLAYER_BLOCKED_TASK_4 = 3212;
  public static final int SLAYER_BLOCKED_TASK_5 = 4441;
  public static final int SLAYER_BLOCKED_TASK_6 = 5023;
  public static final int SLAYER_POINTS = 4068;
  public static final int OPTIONS_QUEST_ICON = 618;
  public static final int OPTIONS_LOG_NOTIFICATION_TIMEOUT = 1627;
  public static final int OPTIONS_TRANSPARENT_CHATBOX_CLICKED_THROUGH = 2570;
  public static final int OPTIONS_DATA_ORBS = 4084;
  public static final int OPTIONS_HIDE_PRIVATE_CHAT = 4089;
  public static final int OPTIONS_MIDDLE_MOUSE_CAMERA = 4134;
  public static final int OPTIONS_SIDE_PANELS = 4607;
  public static final int OPTIONS_TRANSPARENT_CHATBOX = 4608;
  public static final int OPTIONS_OPAQUE_SIDE_PANEL = 4609;
  public static final int OPTIONS_SIDE_PANELS_CLOSED_BY_HOTKEYS = 4611;
  public static final int OPTIONS_SHIFT_CLICK = 5542;
  public static final int OPTIONS_FOLLOWER_OPTIONS_PRIORITY = 5599;
  public static final int OPTIONS_MOUSE_WHEEL_ZOOM = 6357;
  public static final int OPTIONS_CHATBOX_SCROLLBAR = 6374;
  public static final int OPTIONS_FRAMES_PER_SECOND = 6364;
  public static final int DISPLAY_NAME_SET = 8119;
  public static final int DATA_ORBS_RUN_STAMINA = 25;
  public static final int MULTI_COMBAT = 4605;
  public static final int CENTER_CHATBOX_WIDGET = 5983;
  public static final int CLAN_WARS_GAME_END = 4270;
  public static final int CLAN_WARS_MELEE = 4271;
  public static final int CLAN_WARS_RANGING = 4272;
  public static final int CLAN_WARS_MAGIC = 4273;
  public static final int CLAN_WARS_PRAYER = 4274;
  public static final int CLAN_WARS_FOOD = 4275;
  public static final int CLAN_WARS_DRINKS = 4276;
  public static final int CLAN_WARS_SPECIAL_ATTACKS = 4277;
  public static final int CLAN_WARS_STRAGGLERS = 4278;
  public static final int CLAN_WARS_IGNORE_FREEZING = 4279;
  public static final int CLAN_WARS_PJ_TIMER = 4280;
  public static final int CLAN_WARS_ALLOW_TRIDENT_IN_PVP = 4281;
  public static final int CLAN_WARS_SINGLE_SPELLS = 4282;
  public static final int CLAN_WARS_ARENA = 4283;
  public static final int CLAN_WARS_COUNTDOWN = 4286;
  public static final int CLAN_WARS_TEAMMATES = 4287;
  public static final int CLAN_WARS_OPPONENTS = 4288;
  public static final int CLAN_WARS_TEAM_KILLS = 4290;
  public static final int CLAN_WARS_OPPONENT_KILLS = 4291;
  public static final int RUNE_POUCH_RUNE_1 = 29;
  public static final int RUNE_POUCH_RUNE_2 = 1622;
  public static final int RUNE_POUCH_RUNE_3 = 1623;
  public static final int RUNE_POUCH_AMOUNT_1 = 1624;
  public static final int RUNE_POUCH_AMOUNT_2 = 1625;
  public static final int RUNE_POUCH_AMOUNT_3 = 1626;
  public static final int TZKAL_ZULK_HEALTH_OVERLAY_CURRENT = 5653;
  public static final int TZKAL_ZULK_HEALTH_OVERLAY_TOTAL = 5654;
  public static final int HEALTH_OVERLAY_CURRENT = 6099;
  public static final int HEALTH_OVERLAY_TOTAL = 6100;
  public static final int BOUNTY_HUNTER_WEALTH = 1538;
  public static final int BOUNTY_HUNTER_EMBLEM = 4162;
  public static final int BOUNTY_HUNTER_MINIMISED = 6704;
  public static final int BOUNTY_HUNTER_ROGUE_HUNTER_COUNTS = 1621;
  public static final int IS_MOBILE = 6352;
  public static final int GRAVESTONE_FREE_INDEX = 10472;
  public static final int GRAVESTONE_1K_INDEX = 10473;
  public static final int GRAVESTONE_10K_INDEX = 10474;
  public static final int INSURED_PET_IDS_2 = 2168;
  public static final int THEATRE_OF_BLOOD = 6440;
  public static final int THEATRE_OF_BLOOD_PARTY_DISPLAY = 6441;
  public static final int THEATRE_OF_BLOOD_PARTY_HP_1 = 6442;
  public static final int THEATRE_OF_BLOOD_HP_TYPE = 6447;
  public static final int THEATRE_OF_BLOOD_CURRENT_HP = 6448;
  public static final int THEATRE_OF_BLOOD_MAX_HP = 6449;
  public static final int THE_GAUNTLET_CHEST = 9179;
  private static final NameIdLookup LOOKUP = new NameIdLookup(VarbitId.class);

  public static int valueOf(String name) {
    return LOOKUP.nameToId(name);
  }

  public static String valueOf(int id) {
    return LOOKUP.idToName(id);
  }
}
