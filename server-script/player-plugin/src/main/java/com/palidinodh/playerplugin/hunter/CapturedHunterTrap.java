package com.palidinodh.playerplugin.hunter;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.item.RandomItem;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
class CapturedHunterTrap {

  @Getter private static List<CapturedHunterTrap> capturedTrapEntries = loadCapturedTrapEntries();

  private int id;
  private int level;
  private int experience;
  private RandomItem[] items;

  private static List<CapturedHunterTrap> loadCapturedTrapEntries() {
    var entries = new ArrayList<CapturedHunterTrap>();

    entries.add(
        new CapturedHunterTrap(
            ObjectId.BIRD_SNARE_9373,
            1,
            34,
            new RandomItem[] {
              new RandomItem(ItemId.BONES),
              new RandomItem(ItemId.FEATHER, 6, 15),
              new RandomItem(ItemId.RAW_BIRD_MEAT)
            }));

    entries.add(
        new CapturedHunterTrap(
            ObjectId.BIRD_SNARE_9377,
            5,
            47,
            new RandomItem[] {
              new RandomItem(ItemId.BONES),
              new RandomItem(ItemId.FEATHER, 6, 15),
              new RandomItem(ItemId.RAW_BIRD_MEAT)
            }));

    entries.add(
        new CapturedHunterTrap(
            ObjectId.BIRD_SNARE_9379,
            9,
            61,
            new RandomItem[] {
              new RandomItem(ItemId.BONES),
              new RandomItem(ItemId.FEATHER, 6, 15),
              new RandomItem(ItemId.RAW_BIRD_MEAT)
            }));

    entries.add(
        new CapturedHunterTrap(
            ObjectId.BIRD_SNARE_9375,
            11,
            65,
            new RandomItem[] {
              new RandomItem(ItemId.BONES),
              new RandomItem(ItemId.FEATHER, 6, 15),
              new RandomItem(ItemId.RAW_BIRD_MEAT)
            }));

    entries.add(
        new CapturedHunterTrap(
            ObjectId.BIRD_SNARE_9348,
            19,
            95,
            new RandomItem[] {
              new RandomItem(ItemId.BONES),
              new RandomItem(ItemId.FEATHER, 6, 15),
              new RandomItem(ItemId.RAW_BIRD_MEAT)
            }));

    entries.add(
        new CapturedHunterTrap(
            ObjectId.NET_TRAP_9004,
            29,
            152,
            new RandomItem[] {new RandomItem(ItemId.SWAMP_LIZARD)}));

    entries.add(
        new CapturedHunterTrap(
            ObjectId.NET_TRAP_8734,
            47,
            224,
            new RandomItem[] {new RandomItem(ItemId.ORANGE_SALAMANDER)}));

    entries.add(
        new CapturedHunterTrap(
            ObjectId.NET_TRAP_8986,
            59,
            272,
            new RandomItem[] {new RandomItem(ItemId.RED_SALAMANDER)}));

    entries.add(
        new CapturedHunterTrap(
            ObjectId.NET_TRAP_8996,
            67,
            319,
            new RandomItem[] {new RandomItem(ItemId.BLACK_SALAMANDER)}));

    entries.add(
        new CapturedHunterTrap(
            ObjectId.SHAKING_BOX_9384, 27, 115, new RandomItem[] {new RandomItem(ItemId.FERRET)}));

    entries.add(
        new CapturedHunterTrap(
            ObjectId.SHAKING_BOX_9382,
            53,
            198,
            new RandomItem[] {new RandomItem(ItemId.CHINCHOMPA, 5)}));

    entries.add(
        new CapturedHunterTrap(
            ObjectId.SHAKING_BOX_9383,
            63,
            265,
            new RandomItem[] {new RandomItem(ItemId.RED_CHINCHOMPA, 5)}));

    entries.add(
        new CapturedHunterTrap(
            ObjectId.SHAKING_BOX,
            73,
            315,
            new RandomItem[] {new RandomItem(ItemId.BLACK_CHINCHOMPA, 5)}));

    return entries;
  }
}
