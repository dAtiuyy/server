package com.palidinodh.maparea.asgarnia.dwarvenmine;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12085)
public class DwarvenMineArea extends Area {}
