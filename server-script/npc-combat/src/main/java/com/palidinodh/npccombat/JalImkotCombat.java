package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class JalImkotCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.JAL_IMKOT_240);
    combat.hitpoints(NpcCombatHitpoints.total(75));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(210)
            .magicLevel(120)
            .rangedLevel(220)
            .defenceLevel(120)
            .bonus(BonusType.ATTACK_STAB, 40)
            .bonus(BonusType.ATTACK_SLASH, 40)
            .bonus(BonusType.ATTACK_CRUSH, 40)
            .bonus(BonusType.MELEE_DEFENCE, 65)
            .bonus(BonusType.DEFENCE_MAGIC, 30)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(8).always(true).forceable(false).build());
    combat.immunity(NpcCombatImmunity.builder().venom(true).build());
    combat.deathAnimation(7599).blockAnimation(7598);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(49));
    style.animation(7597).attackSpeed(4);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void tickStartHook() {
    var opponent = getAttackingEntity();
    if (npc.isLocked() || getHitDelay() > -34 || opponent == null || PRandom.randomE(4) != 0) {
      return;
    }
    Tile teleportTile = null;
    if (PRandom.randomI(1) == 0
        && npc.getController().routeAllow(opponent.getX(), opponent.getY() - npc.getSizeY())) {
      teleportTile = new Tile(getAttackingEntity()).moveY(-npc.getSizeY());
    } else if (PRandom.randomI(1) == 0
        && npc.getController().routeAllow(opponent.getX() - npc.getSizeX(), opponent.getY())) {
      teleportTile = new Tile(getAttackingEntity()).moveX(-npc.getSizeX());
    } else if (PRandom.randomI(1) == 0
        && npc.getController().routeAllow(opponent.getX(), opponent.getY() + 1)) {
      teleportTile = new Tile(getAttackingEntity()).moveY(1);
    } else if (PRandom.randomI(1) == 0
        && npc.getController().routeAllow(opponent.getX() + 1, opponent.getY())) {
      teleportTile = new Tile(getAttackingEntity()).moveX(1);
    }
    setHitDelay(2);
    if (teleportTile == null) {
      return;
    }
    npc.setLock(8);
    npc.getMovement().animatedTeleport(teleportTile, 7600, 7601, null, null, 4);
  }
}
