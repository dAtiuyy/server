package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.PMovement;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class BarrowsCombat extends NpcCombat {

  private static final NpcCombatStyle AHRIM_STUN =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MAGIC)
          .damage(
              NpcCombatDamage.builder().maximum(0).ignorePrayer(true).splashOnMiss(true).build())
          .animation(729)
          .attackSpeed(5)
          .castGraphic(new Graphic(173, 92))
          .targetGraphic(new Graphic(254, 124))
          .projectile(NpcCombatProjectile.id(174))
          .build();
  private static final NpcCombatStyle AHRIM_ENFEEBLE =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MAGIC)
          .damage(
              NpcCombatDamage.builder().maximum(0).ignorePrayer(true).splashOnMiss(true).build())
          .animation(729)
          .attackSpeed(5)
          .castGraphic(new Graphic(170, 92))
          .targetGraphic(new Graphic(172, 124))
          .projectile(NpcCombatProjectile.id(171))
          .build();
  private static final NpcCombatStyle AHRIM_VULNERABILITY =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MAGIC)
          .damage(
              NpcCombatDamage.builder().maximum(0).ignorePrayer(true).splashOnMiss(true).build())
          .animation(729)
          .attackSpeed(5)
          .castGraphic(new Graphic(167, 92))
          .targetGraphic(new Graphic(169, 124))
          .projectile(NpcCombatProjectile.id(168))
          .build();

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().rareDropTableDenominator(42);
    var dropTable =
        NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.EARTH_BATTLESTAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_AXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_WARHAMMER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_KITESHIELD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_FULL_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 250)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 80)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 45)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 40)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BOLTS, 5, 44)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_ARROW, 36)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RANARR_SEED, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPDRAGON_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_IRIT_LEAF, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_LOGS_NOTED, 35)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_BAR_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNPOWERED_ORB_NOTED, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.IRON_ORE_NOTED, 200)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_RUBY_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DIAMOND_NOTED, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BATTLESTAFF_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_POTION_4, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LOBSTER_NOTED, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_CROSSBOW_U)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_RUNE, 225)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATER_RUNE, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_BOLTS, 7, 99)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_GUAM_LEAF, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_MARRENTILL, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TARROMIN, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_HARRALANDER, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 300)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BARROWS_AMULET_60038)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BARROWS_AMULET_60038)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COMMON_BARROWS_KEY_60069)));
    drop.table(dropTable.build());

    var ahrim = NpcCombatDefinition.builder();
    ahrim.id(NpcId.AHRIM_THE_BLIGHTED_98);
    ahrim.spawn(NpcCombatSpawn.builder().respawnDelay(100).build());
    ahrim.hitpoints(NpcCombatHitpoints.total(100));
    ahrim.stats(
        NpcCombatStats.builder()
            .magicLevel(100)
            .defenceLevel(100)
            .bonus(BonusType.ATTACK_STAB, 12)
            .bonus(BonusType.ATTACK_CRUSH, 65)
            .bonus(BonusType.ATTACK_MAGIC, 73)
            .bonus(BonusType.DEFENCE_STAB, 103)
            .bonus(BonusType.DEFENCE_SLASH, 85)
            .bonus(BonusType.DEFENCE_CRUSH, 117)
            .bonus(BonusType.DEFENCE_MAGIC, 73)
            .build());
    ahrim.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    ahrim.killCount(NpcCombatKillCount.builder().asName("Barrows brother").build());
    ahrim.type(NpcCombatType.SPECTRAL).deathAnimation(836).blockAnimation(415);
    ahrim.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(25).splashOnMiss(true).build());
    style.animation(727).attackSpeed(5);
    style.castGraphic(new Graphic(155, 92)).targetGraphic(new Graphic(157, 124));
    style.projectile(NpcCombatProjectile.id(156));
    ahrim.style(style.build());

    var dharok = NpcCombatDefinition.builder();
    dharok.id(NpcId.DHAROK_THE_WRETCHED_115);
    dharok.spawn(NpcCombatSpawn.builder().respawnDelay(100).build());
    dharok.hitpoints(NpcCombatHitpoints.total(100));
    dharok.stats(
        NpcCombatStats.builder()
            .attackLevel(100)
            .defenceLevel(100)
            .bonus(BonusType.ATTACK_SLASH, 103)
            .bonus(BonusType.ATTACK_CRUSH, 95)
            .bonus(BonusType.DEFENCE_STAB, 252)
            .bonus(BonusType.DEFENCE_SLASH, 250)
            .bonus(BonusType.DEFENCE_CRUSH, 244)
            .bonus(BonusType.DEFENCE_MAGIC, -11)
            .bonus(BonusType.DEFENCE_RANGED, 249)
            .build());
    dharok.killCount(NpcCombatKillCount.builder().asName("Barrows brother").build());
    dharok.type(NpcCombatType.SPECTRAL).deathAnimation(836).blockAnimation(424);
    dharok.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(29));
    style.animation(2066).attackSpeed(6);
    dharok.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(29));
    style.animation(2067).attackSpeed(6);
    dharok.style(style.build());

    var guthan = NpcCombatDefinition.builder();
    guthan.id(NpcId.GUTHAN_THE_INFESTED_115);
    guthan.spawn(NpcCombatSpawn.builder().respawnDelay(100).build());
    guthan.hitpoints(NpcCombatHitpoints.total(100));
    guthan.stats(
        NpcCombatStats.builder()
            .attackLevel(100)
            .defenceLevel(100)
            .bonus(BonusType.ATTACK_STAB, 75)
            .bonus(BonusType.ATTACK_SLASH, 75)
            .bonus(BonusType.ATTACK_CRUSH, 75)
            .bonus(BonusType.DEFENCE_STAB, 259)
            .bonus(BonusType.DEFENCE_SLASH, 257)
            .bonus(BonusType.DEFENCE_CRUSH, 241)
            .bonus(BonusType.DEFENCE_MAGIC, -11)
            .bonus(BonusType.DEFENCE_RANGED, 250)
            .build());
    guthan.killCount(NpcCombatKillCount.builder().asName("Barrows brother").build());
    guthan.type(NpcCombatType.SPECTRAL).deathAnimation(836).blockAnimation(435);
    guthan.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(24));
    style.animation(2080).attackSpeed(5);
    guthan.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(24));
    style.animation(2081).attackSpeed(5);
    guthan.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(24));
    style.animation(2082).attackSpeed(5);
    guthan.style(style.build());

    var karil = NpcCombatDefinition.builder();
    karil.id(NpcId.KARIL_THE_TAINTED_98);
    karil.spawn(NpcCombatSpawn.builder().respawnDelay(100).build());
    karil.hitpoints(NpcCombatHitpoints.total(100));
    karil.stats(
        NpcCombatStats.builder()
            .rangedLevel(100)
            .defenceLevel(100)
            .bonus(BonusType.ATTACK_RANGED, 134)
            .bonus(BonusType.DEFENCE_STAB, 79)
            .bonus(BonusType.DEFENCE_SLASH, 71)
            .bonus(BonusType.DEFENCE_CRUSH, 90)
            .bonus(BonusType.DEFENCE_MAGIC, 106)
            .bonus(BonusType.DEFENCE_RANGED, 100)
            .build());
    karil.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    karil.killCount(NpcCombatKillCount.builder().asName("Barrows brother").build());
    karil.type(NpcCombatType.SPECTRAL).deathAnimation(836).blockAnimation(424);
    karil.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(20));
    style.animation(2075).attackSpeed(4);
    karil.style(style.build());

    var torag = NpcCombatDefinition.builder();
    torag.id(NpcId.TORAG_THE_CORRUPTED_115);
    torag.spawn(NpcCombatSpawn.builder().respawnDelay(100).build());
    torag.hitpoints(NpcCombatHitpoints.total(100));
    torag.stats(
        NpcCombatStats.builder()
            .attackLevel(100)
            .defenceLevel(100)
            .bonus(BonusType.ATTACK_STAB, 68)
            .bonus(BonusType.ATTACK_CRUSH, 82)
            .bonus(BonusType.DEFENCE_STAB, 221)
            .bonus(BonusType.DEFENCE_SLASH, 235)
            .bonus(BonusType.DEFENCE_CRUSH, 222)
            .bonus(BonusType.DEFENCE_RANGED, 221)
            .build());
    torag.killCount(NpcCombatKillCount.builder().asName("Barrows brother").build());
    torag.type(NpcCombatType.SPECTRAL).deathAnimation(836).blockAnimation(424);
    torag.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(23));
    style.animation(2068).attackSpeed(5);
    torag.style(style.build());

    var verac = NpcCombatDefinition.builder();
    verac.id(NpcId.VERAC_THE_DEFILED_115);
    verac.spawn(NpcCombatSpawn.builder().respawnDelay(100).build());
    verac.hitpoints(NpcCombatHitpoints.total(100));
    verac.stats(
        NpcCombatStats.builder()
            .attackLevel(100)
            .defenceLevel(100)
            .bonus(BonusType.ATTACK_STAB, 68)
            .bonus(BonusType.ATTACK_CRUSH, 82)
            .bonus(BonusType.DEFENCE_STAB, 227)
            .bonus(BonusType.DEFENCE_SLASH, 230)
            .bonus(BonusType.DEFENCE_CRUSH, 221)
            .bonus(BonusType.DEFENCE_RANGED, 225)
            .build());
    verac.killCount(NpcCombatKillCount.builder().asName("Barrows brother").build());
    verac.type(NpcCombatType.SPECTRAL).deathAnimation(836).blockAnimation(2063);
    verac.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(23));
    style.animation(2062).attackSpeed(5);
    verac.style(style.build());

    return Arrays.asList(
        ahrim.build(), dharok.build(), guthan.build(), karil.build(), torag.build(), verac.build());
  }

  @Override
  public NpcCombatStyle attackTickCombatStyleHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (combatStyle == NpcCombatStyle.NONE || !opponent.isPlayer()) {
      return combatStyle;
    }
    var player = opponent.asPlayer();
    if (npc.getId() == NpcId.AHRIM_THE_BLIGHTED_98 && PRandom.randomE(5) == 0) {
      var type = PRandom.randomE(3);
      NpcCombatStyle attemptedStyle = null;
      var skillId = 0;
      if (type == 0) {
        attemptedStyle = AHRIM_STUN;
        skillId = Skills.ATTACK;
      } else if (type == 1) {
        attemptedStyle = AHRIM_ENFEEBLE;
        skillId = Skills.STRENGTH;
      } else if (type == 2) {
        attemptedStyle = AHRIM_VULNERABILITY;
        skillId = Skills.DEFENCE;
      }
      var currentLevel = player.getSkills().getLevel(skillId);
      var maxLevel = player.getSkills().getLevel(skillId);
      var maxReducedLevel = (int) (maxLevel * 0.9);
      if (currentLevel > maxReducedLevel) {
        if (attemptedStyle != null) {
          return attemptedStyle;
        }
      }
    }
    return combatStyle;
  }

  @Override
  public void applyAttackEndHook(
      NpcCombatStyle combatStyle, Entity opponent, int applyAttackLoopCount, HitEvent hitEvent) {
    if (!opponent.isPlayer()) {
      return;
    }
    var player = opponent.asPlayer();
    if (npc.getId() == NpcId.AHRIM_THE_BLIGHTED_98) {
      if (PRandom.randomE(4) == 0) {
        player.getSkills().changeStat(Skills.STRENGTH, -5);
        player.setGraphic(400, 100);
      }
      if (combatStyle != AHRIM_STUN
              && combatStyle != AHRIM_ENFEEBLE
              && combatStyle != AHRIM_VULNERABILITY
          || hitEvent.isDefenceBlocked()) {
        return;
      }
      var skillId = 0;
      if (combatStyle == AHRIM_STUN) {
        skillId = Skills.ATTACK;
      } else if (combatStyle == AHRIM_ENFEEBLE) {
        skillId = Skills.STRENGTH;
      } else if (combatStyle == AHRIM_VULNERABILITY) {
        skillId = Skills.DEFENCE;
      }
      var currentLevel = player.getSkills().getLevel(skillId);
      var maxLevel = player.getSkills().getLevel(skillId);
      var reducedLevel = (int) (currentLevel * 0.9);
      var maxReducedLevel = (int) (maxLevel * 0.9);
      if (reducedLevel < maxReducedLevel) {
        reducedLevel = maxReducedLevel;
      }
      if (currentLevel != reducedLevel) {
        player.getSkills().changeStat(skillId, -(currentLevel - reducedLevel));
      }
    }
  }

  @Override
  public void attackedActionsOpponentHook(Entity opponent) {
    if (!npc.getArea().inWilderness() || !opponent.isPlayer()) {
      return;
    }
    var player = opponent.asPlayer();
    player.getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (npc.getArea().inWilderness() && PRandom.inRange(1, 128)) {
      player.getController().addMapItem(new Item(ItemId.RARE_BARROWS_KEY_60068), player, player);
    }
  }

  @Override
  public boolean dropTableCanDropHook(
      Player player, int dropRateDivider, int roll, NpcCombatDropTable table) {
    return npc.getArea().inWilderness();
  }

  @Override
  public double damageInflictedHook(NpcCombatStyle combatStyle, Entity opponent, double damage) {
    if (!opponent.isPlayer()) {
      return damage;
    }
    var player = opponent.asPlayer();
    if (npc.getId() == NpcId.DHAROK_THE_WRETCHED_115) {
      damage *= 1.0 + (getMaxHitpoints() - getHitpoints()) / 100.0 * (getMaxHitpoints() / 100.0);
    } else if (npc.getId() == NpcId.GUTHAN_THE_INFESTED_115 && PRandom.randomE(4) == 0) {
      opponent.setGraphic(398);
      if (damage > 0) {
        changeHitpoints((int) damage, 0);
      }
    } else if (npc.getId() == NpcId.KARIL_THE_TAINTED_98 && PRandom.randomE(4) == 0) {
      player
          .getSkills()
          .changeStat(Skills.AGILITY, (int) -(player.getSkills().getLevel(Skills.AGILITY) * 0.2));
      opponent.setGraphic(401, 100);
    } else if (npc.getId() == NpcId.TORAG_THE_CORRUPTED_115
        && damage > 0
        && PRandom.randomE(4) == 0) {
      player
          .getMovement()
          .setEnergy((int) (player.getMovement().getEnergy() - PMovement.MAX_ENERGY * 0.2));
      opponent.setGraphic(399, 100);
    } else if (npc.getId() == NpcId.VERAC_THE_DEFILED_115 && PRandom.randomE(4) == 0) {
      damage = PRandom.randomI(23) + 1;
      opponent.setGraphic(1041);
    }
    return damage;
  }
}
