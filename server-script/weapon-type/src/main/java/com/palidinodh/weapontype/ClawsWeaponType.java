package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.BRONZE_CLAWS,
  ItemId.IRON_CLAWS,
  ItemId.STEEL_CLAWS,
  ItemId.BLACK_CLAWS,
  ItemId.MITHRIL_CLAWS,
  ItemId.ADAMANT_CLAWS,
  ItemId.RUNE_CLAWS,
  ItemId.WHITE_CLAWS,
  ItemId.DRAGON_CLAWS,
  ItemId.DRAGON_CLAWS_BEGINNER_32327
})
class ClawsWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_4);
    type.equipSound(new Sound(2238));
    type.twoHanded(true);
    type.attackSpeed(4);
    type.defendAnimation(397);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(393).attackSound(new Sound(2517)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.CONTROLLED_STAB)
            .attackAnimation(1067)
            .build());
    return type;
  }
}
