package com.palidinodh.npccombat;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.Arrays;
import java.util.List;

class TreeSpiritCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.TREE_SPIRIT_101);
    combat.hitpoints(NpcCombatHitpoints.total(85));
    combat.stats(NpcCombatStats.builder().attackLevel(90).defenceLevel(80).build());
    combat.type(NpcCombatType.SPECTRAL).deathAnimation(5534).blockAnimation(5533);

    var style = NpcCombatStyle.builder();
    style.damage(NpcCombatDamage.maximum(10));
    style.animation(5532).attackSpeed(4);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    player.getCombat().setLostCity(true);
    player.getMovement().teleport(3093, 3496);
    player.getGameEncoder().sendMessage("<col=ff0000>You have completed Lost City!");
    player.getInventory().addOrDropItem(ItemId.COINS, 25_000);
  }
}
