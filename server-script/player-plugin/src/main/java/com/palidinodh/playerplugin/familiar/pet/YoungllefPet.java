package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;

class YoungllefPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.YOUNGLLEF, NpcId.YOUNGLLEF, NpcId.YOUNGLLEF_8737));
    builder.entry(
        new Pet.Entry(
            ItemId.CORRUPTED_YOUNGLLEF, NpcId.CORRUPTED_YOUNGLLEF, NpcId.CORRUPTED_YOUNGLLEF_8738));
    builder.optionVariation(
        (p, n) -> {
          switch (n.getId()) {
            case NpcId.CORRUPTED_YOUNGLLEF:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.YOUNGLLEF);
              break;
            case NpcId.YOUNGLLEF:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.CORRUPTED_YOUNGLLEF);
              break;
          }
        });
    return builder;
  }
}
