package com.palidinodh.maparea.dragonkinislands.wyverncave;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({14495, 14496})
public class WyvernCaveArea extends Area {}
