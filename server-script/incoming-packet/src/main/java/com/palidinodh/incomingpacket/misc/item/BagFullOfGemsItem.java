package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.BAG_FULL_OF_GEMS)
class BagFullOfGemsItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var randomItems =
        RandomItem.buildList(
            new RandomItem(ItemId.UNCUT_SAPPHIRE, 1, 4).weight(1024),
            new RandomItem(ItemId.UNCUT_EMERALD, 1, 4).weight(1024),
            new RandomItem(ItemId.UNCUT_RUBY, 1, 4).weight(1024),
            new RandomItem(ItemId.UNCUT_DIAMOND, 1, 4).weight(1024),
            new RandomItem(ItemId.UNCUT_DRAGONSTONE, 1).weight(32),
            new RandomItem(ItemId.UNCUT_ONYX, 1).weight(8),
            new RandomItem(ItemId.ZENYTE_SHARD, 1).weight(1));
    for (var i = 0; i < 30; i++) {
      var gemItem = RandomItem.getItem(randomItems);
      player.getInventory().addOrDropItem(gemItem.getNotedId(), gemItem.getAmount());
    }
    var plugin = player.getPlugin(SlayerPlugin.class);
    plugin.incrimentGemBags();
    player.getGameEncoder().sendMessage("You have opened " + plugin.getGemBags() + " gem bags!");
  }
}
