package com.palidinodh.playerplugin.magic.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.ANCESTRAL_HAT,
  ItemId.ANCESTRAL_ROBE_TOP,
  ItemId.ANCESTRAL_ROBE_BOTTOM,
  ItemId.TWISTED_ANCESTRAL_HAT,
  ItemId.TWISTED_ANCESTRAL_ROBE_TOP,
  ItemId.TWISTED_ANCESTRAL_ROBE_BOTTOM
})
class AncestralItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "dismantle":
        {
          if (item.getInfoDef().getExchangeIds() == null) {
            break;
          }
          if (player.getInventory().getRemainingSlots()
              < item.getInfoDef().getExchangeIds().length - 1) {
            player.getInventory().notEnoughSpace();
            break;
          }
          item.remove();
          for (var exchangeId : item.getInfoDef().getExchangeIds()) {
            player.getInventory().addItem(exchangeId);
          }
          break;
        }
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var onSlot = onItem.getSlot();
    if (ItemHandler.used(
        useItem, onItem, ItemId.ANCESTRAL_HAT, ItemId.TWISTED_ANCESTRAL_COLOUR_KIT)) {
      player.getInventory().deleteItem(ItemId.ANCESTRAL_HAT);
      player.getInventory().deleteItem(ItemId.TWISTED_ANCESTRAL_COLOUR_KIT);
      player.getInventory().addItem(ItemId.TWISTED_ANCESTRAL_HAT, 1, onSlot);
      return true;
    } else if (ItemHandler.used(
        useItem, onItem, ItemId.ANCESTRAL_ROBE_TOP, ItemId.TWISTED_ANCESTRAL_COLOUR_KIT)) {
      player.getInventory().deleteItem(ItemId.ANCESTRAL_ROBE_TOP);
      player.getInventory().deleteItem(ItemId.TWISTED_ANCESTRAL_COLOUR_KIT);
      player.getInventory().addItem(ItemId.TWISTED_ANCESTRAL_ROBE_TOP, 1, onSlot);
      return true;
    } else if (ItemHandler.used(
        useItem, onItem, ItemId.ANCESTRAL_ROBE_BOTTOM, ItemId.TWISTED_ANCESTRAL_COLOUR_KIT)) {
      player.getInventory().deleteItem(ItemId.ANCESTRAL_ROBE_BOTTOM);
      player.getInventory().deleteItem(ItemId.TWISTED_ANCESTRAL_COLOUR_KIT);
      player.getInventory().addItem(ItemId.TWISTED_ANCESTRAL_ROBE_BOTTOM, 1, onSlot);
      return true;
    }
    return false;
  }
}
