package com.palidinodh.maparea.quest;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(8534)
public class GrimTalesArea extends Area {

  @Override
  public boolean inMultiCombat() {
    return true;
  }
}
