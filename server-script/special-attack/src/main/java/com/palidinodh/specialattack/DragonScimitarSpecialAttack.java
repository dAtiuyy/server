package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.DRAGON_SCIMITAR, ItemId.DRAGON_SCIMITAR_OR, ItemId.DRAGON_SCIMITAR_20406})
class DragonScimitarSpecialAttack extends SpecialAttack {

  DragonScimitarSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(55);
    entry.animation(1872);
    entry.castGraphic(new Graphic(347, 100));
    entry.accuracyModifier(1.25);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    var opponent = hooks.getOpponent();
    if (!opponent.isPlayer()) {
      return;
    }
    if (hooks.getDamage() == 0) {
      return;
    }
    opponent.asPlayer().getPrayer().deactivate("protect from magic");
    opponent.asPlayer().getPrayer().deactivate("protect from missiles");
    opponent.asPlayer().getPrayer().deactivate("protect from melee");
    opponent.asPlayer().getPrayer().setDamageProtectionPrayerBlock(8);
  }
}
