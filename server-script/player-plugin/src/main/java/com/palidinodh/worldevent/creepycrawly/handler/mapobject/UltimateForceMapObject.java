package com.palidinodh.worldevent.creepycrawly.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.worldevent.creepycrawly.CreepyCrawlyController;

@ReferenceId(ObjectId.ULTIMATE_FORCE)
class UltimateForceMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (!player.getController().is(CreepyCrawlyController.class)) {
      return;
    }
    var controller = player.getController().as(CreepyCrawlyController.class);
    var minigame = controller.getMinigame();
    minigame.ultimateForcePowerUp();
  }
}
