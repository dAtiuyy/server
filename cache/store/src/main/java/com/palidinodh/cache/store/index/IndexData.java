package com.palidinodh.cache.store.index;

import com.palidinodh.cache.store.fs.Archive;
import com.palidinodh.cache.store.fs.Index;
import com.palidinodh.cache.store.util.Stream;
import java.util.Comparator;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IndexData {

  private int id;
  private int protocol;
  private int revision;
  private boolean named;
  private ArchiveData[] archives;

  public IndexData(int id) {
    this.id = id;
  }

  public static IndexData fromIndex(Index index) {
    index.getArchives().sort(Comparator.comparingInt(Archive::getId));
    var data = new IndexData(index.getId());
    data.setProtocol(index.getProtocol());
    data.setRevision(index.getRevision());
    data.setNamed(index.isNamed());
    var archiveDatas = new ArchiveData[index.getArchives().size()];
    data.setArchives(archiveDatas);
    var idx = 0;
    for (var archive : index.getArchives()) {
      archiveDatas[idx++] =
          new ArchiveData(
              index.getId(),
              archive.getId(),
              archive.getNameHash(),
              archive.getCompression(),
              archive.getCrc(),
              archive.getRevision(),
              archive.getFiles());
    }
    return data;
  }

  public void load(byte[] data) {
    var stream = new Stream(data);
    protocol = stream.readUnsignedByte();
    if (protocol < 5 || protocol > 7) {
      throw new IllegalArgumentException("Unsupported protocol");
    }
    if (protocol >= 6) {
      revision = stream.readInt();
    }
    var flags = stream.readUnsignedByte();
    named = (1 & flags) != 0;
    if ((flags & ~1) != 0) {
      throw new IllegalArgumentException("Unknown flags");
    }
    var validArchivesCount = protocol >= 7 ? stream.readBigSmart() : stream.readUnsignedShort();
    var lastArchiveId = 0;
    archives = new ArchiveData[validArchivesCount];
    for (var i = 0; i < validArchivesCount; i++) {
      var archiveId =
          lastArchiveId += protocol >= 7 ? stream.readBigSmart() : stream.readUnsignedShort();
      var ad = new ArchiveData(id, archiveId, 0, 0);
      archives[i] = ad;
    }
    if (named) {
      for (var i = 0; i < validArchivesCount; i++) {
        var hash = stream.readInt();
        archives[i].setNameHash(hash);
      }
    }
    for (var i = 0; i < validArchivesCount; i++) {
      var crc = stream.readInt();
      archives[i].setCrc(crc);
    }
    for (var i = 0; i < validArchivesCount; i++) {
      var adRevision = stream.readInt();
      archives[i].setRevision(adRevision);
    }
    var numberOfFiles = new int[validArchivesCount];
    for (var i = 0; i < validArchivesCount; i++) {
      var num = protocol >= 7 ? stream.readBigSmart() : stream.readUnsignedShort();
      numberOfFiles[i] = num;
    }
    List<FileData> files;
    for (var i = 0; i < validArchivesCount; i++) {
      var ad = archives[i];
      var num = numberOfFiles[i];
      var last = 0;
      for (var j = 0; j < num; j++) {
        var fileId = last += protocol >= 7 ? stream.readBigSmart() : stream.readUnsignedShort();
        var fd = new FileData(ad.getIndex(), ad.getId(), fileId, 0);
        ad.getFiles().add(fd);
      }
    }
    if (named) {
      for (var i = 0; i < validArchivesCount; i++) {
        var ad = archives[i];
        var num = numberOfFiles[i];
        files = ad.getFiles();
        for (var j = 0; j < num; j++) {
          var fd = files.get(j);
          var hash = stream.readInt();
          files.set(j, new FileData(fd.getIndex(), fd.getArchiveId(), fd.getId(), hash));
        }
      }
    }
  }

  public byte[] writeIndexData() {
    var stream = new Stream();
    stream.writeByte(protocol);
    if (protocol >= 6) {
      stream.writeInt(revision);
    }
    stream.writeByte(named ? 1 : 0);
    if (protocol >= 7) {
      stream.writeBigSmart(archives.length);
    } else {
      stream.writeShort(archives.length);
    }
    for (var i = 0; i < archives.length; i++) {
      var ad = archives[i];
      var archiveId = ad.getId();
      if (i != 0) {
        var prevAd = archives[i - 1];
        archiveId -= prevAd.getId();
      }
      if (protocol >= 7) {
        stream.writeBigSmart(archiveId);
      } else {
        stream.writeShort(archiveId);
      }
    }
    if (named) {
      for (var ad : archives) {
        stream.writeInt(ad.getNameHash());
      }
    }
    for (var ad : archives) {
      stream.writeInt(ad.getCrc());
    }
    for (var ad : archives) {
      stream.writeInt(ad.getRevision());
    }
    for (var ad : archives) {
      var len = ad.getFiles().size();
      if (protocol >= 7) {
        stream.writeBigSmart(len);
      } else {
        stream.writeShort(len);
      }
    }
    for (var ad : archives) {
      for (var i = 0; i < ad.getFiles().size(); i++) {
        var fd = ad.getFiles().get(i);
        var offset = fd.getId();
        if (i != 0) {
          var prevFd = ad.getFiles().get(i - 1);
          offset -= prevFd.getId();
        }
        if (protocol >= 7) {
          stream.writeBigSmart(offset);
        } else {
          stream.writeShort(offset);
        }
      }
    }
    if (named) {
      for (var ad : archives) {
        for (var i = 0; i < ad.getFiles().size(); i++) {
          var fd = ad.getFiles().get(i);
          stream.writeInt(fd.getNameHash());
        }
      }
    }
    return stream.toByteArray();
  }
}
