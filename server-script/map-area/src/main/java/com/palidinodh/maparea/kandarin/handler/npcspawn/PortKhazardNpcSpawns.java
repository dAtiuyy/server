package com.palidinodh.maparea.kandarin.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class PortKhazardNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2667, 3163), NpcId.MURPHY));
    spawns.add(new NpcSpawn(4, new Tile(2678, 3154), NpcId.TINDEL_MARCHANT));
    spawns.add(new NpcSpawn(4, new Tile(2674, 3146), NpcId.TRADER_CREWMEMBER_9324));
    spawns.add(new NpcSpawn(4, new Tile(2673, 3144), NpcId.TRADER_CREWMEMBER_9348));

    return spawns;
  }
}
