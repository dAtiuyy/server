package com.palidinodh.playerplugin.tirannwn.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.CRYSTAL_BOW,
  ItemId.CRYSTAL_HALBERD,
  ItemId.CRYSTAL_SHIELD,
  ItemId.NEW_CRYSTAL_SHIELD,
  ItemId.NEW_CRYSTAL_HALBERD_FULL,
  ItemId.NEW_CRYSTAL_BOW
})
class CrystalEquipmentItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (item.getId() == ItemId.NEW_CRYSTAL_SHIELD) {
      item.replace(new Item(ItemId.CRYSTAL_SHIELD));
    } else if (item.getId() == ItemId.NEW_CRYSTAL_HALBERD_FULL) {
      item.replace(new Item(ItemId.CRYSTAL_HALBERD));
    } else if (item.getId() == ItemId.NEW_CRYSTAL_BOW) {
      item.replace(new Item(ItemId.CRYSTAL_BOW));
    } else {
      if (option.equals("revert")) {
        item.replace(new Item(ItemId.CRYSTAL_WEAPON_SEED));
      }
    }
  }
}
