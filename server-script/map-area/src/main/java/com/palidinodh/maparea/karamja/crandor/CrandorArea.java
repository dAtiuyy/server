package com.palidinodh.maparea.karamja.crandor;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId({11315, 11314})
@ReferenceIdSet(
    primary = 11059,
    secondary = {112, 113, 114, 115})
public class CrandorArea extends Area {}
