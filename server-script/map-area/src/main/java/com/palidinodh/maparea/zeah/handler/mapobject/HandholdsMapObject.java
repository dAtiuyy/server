package com.palidinodh.maparea.zeah.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.HANDHOLDS, ObjectId.HANDHOLDS_42009})
public class HandholdsMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (mapObject.getX() == 1459 && mapObject.getY() == 3690) {
      player.getMovement().ladderDownTeleport(new Tile(1456, 3690, 0));
      return;
    }
    if (mapObject.getX() == 1457 && mapObject.getY() == 3690) {
      player.getMovement().ladderUpTeleport(new Tile(1460, 3690, 0));
      return;
    }
    if (mapObject.getX() == 1471 && mapObject.getY() == 3687) {
      player.getMovement().ladderDownTeleport(new Tile(1475, 3687, 0));
      return;
    }
    if (mapObject.getX() == 1474 && mapObject.getY() == 3687) {
      player.getMovement().ladderUpTeleport(new Tile(1470, 3687, 0));
    }
  }
}
