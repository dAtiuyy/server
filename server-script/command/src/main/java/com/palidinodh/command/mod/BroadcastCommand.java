package com.palidinodh.command.mod;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("broadcast")
class BroadcastCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Override
  public String getExample(String name) {
    return "message";
  }

  @Override
  public void execute(Player player, String name, String message) {
    player
        .getWorld()
        .sendBroadcast(
            player.getMessaging().getIconImage() + player.getUsername() + ": " + message);
    DiscordBot.sendMessage(
        DiscordChannel.MODERATION_LOG, player.getUsername() + " broadcasted: " + message);
  }
}
