package com.palidinodh.rs.communicationv2.event;

import com.palidinodh.rs.communicationv2.CommunicationEvent;
import lombok.Getter;

@Getter
public class LoadIgnoreEvent extends CommunicationEvent {

  private int userId;
  private String ignoredUsername;

  public LoadIgnoreEvent(int userId, String ignoredUsername) {
    this.userId = userId;
    this.ignoredUsername = ignoredUsername;
  }

  @Override
  public void mainAction() {
    var player = getMainServer().getPlayersById().get(userId);
    if (player == null) {
      return;
    }
    player.loadIgnore(ignoredUsername);
    getMainServer().getClan(player);
  }
}
