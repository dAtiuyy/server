package com.palidinodh.maparea.kandarin.smokedevildungeon.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.boss.BossPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;

@ReferenceId({ObjectId.CREVICE_535, ObjectId.CREVICE_536})
class BossCreviceMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.CREVICE_535:
        if (!player.getSkills().isAnySlayerTask(NpcId.THERMONUCLEAR_SMOKE_DEVIL_301)
            && !Settings.getInstance().isLocal()
            && !Settings.getInstance().isBeta()
            && !Settings.getInstance().isSpawn()) {
          player.getGameEncoder().sendMessage("You need an appropriate task to enter.");
          break;
        }
        player
            .getPlugin(BossPlugin.class)
            .openBossInstanceDialogue(NpcId.THERMONUCLEAR_SMOKE_DEVIL_301);
        break;
      case ObjectId.CREVICE_536:
        player.getMovement().ladderDownTeleport(new Tile(2379, 9452));
        player.getController().stopWithTeleport();
        break;
    }
  }
}
