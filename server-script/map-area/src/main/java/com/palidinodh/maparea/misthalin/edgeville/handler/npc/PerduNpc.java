package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.PERDU)
class PerduNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    for (var i = 0; i < player.getInventory().size(); i++) {
      var item = player.getInventory().getItem(i);
      if (item == null || item.getInfoDef().getRepairedId() == -1) {
        continue;
      }
      if (item.getInfoDef().getRepairCost() == null) {
        player.getGameEncoder().sendMessage("No repair cost set for " + item.getDef().getName());
        continue;
      }
      var hasRepairCost = true;
      for (var i2 = 0; i2 < item.getInfoDef().getRepairCost().length; i2++) {
        var repair = item.getInfoDef().getRepairCost()[i2];
        if (player.getInventory().getCount(repair.getId()) < repair.getAmount()) {
          player
              .getGameEncoder()
              .sendMessage("You need " + repair.getDef().getName() + " x " + repair.getAmount());
          hasRepairCost = false;
        }
      }
      if (!hasRepairCost) {
        continue;
      }
      for (var i2 = 0; i2 < item.getInfoDef().getRepairCost().length; i2++) {
        var repair = item.getInfoDef().getRepairCost()[i2];
        player.getInventory().deleteItem(repair.getId(), repair.getAmount());
      }
      var itemAmount = item.getAmount();
      player.getInventory().deleteItem(item, i);
      player.getInventory().addItem(item.getInfoDef().getRepairedId(), itemAmount, i);
    }
  }
}
