package com.palidinodh.util;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

public abstract class PEvent {

  @Getter @Setter private transient boolean running;
  @Getter private transient int tick;
  private transient int currentTick;
  @Getter @Setter private transient int executions;
  private transient Map<Integer, Object> attachments;

  public PEvent(int tick) {
    setTick(tick);
    setRunning(true);
  }

  public PEvent() {
    setTick(0);
    setRunning(true);
  }

  public static PEvent singleEvent(long delay, Execution eventExecution) {
    return new PEvent(Math.max(0, (int) delay - 1)) {
      @Override
      public void execute() {
        stop();
        eventExecution.execute(this);
      }
    };
  }

  public static PEvent continuousEvent(long delay, Execution eventExecution) {
    return new PEvent(Math.max(0, (int) delay - 1)) {
      @Override
      public void execute() {
        eventExecution.execute(this);
      }
    };
  }

  public Object script(String name, Object... args) {
    return null;
  }

  public abstract void execute();

  public void stopHook() {}

  public final boolean isReady() {
    if (!isRunning()) {
      return false;
    }
    return currentTick == -1 || currentTick-- == 0;
  }

  public final boolean complete() {
    if (!isReady()) {
      return false;
    }
    currentTick = tick;
    execute();
    executions++;
    if (currentTick == -1) {
      stop();
    }
    return !running;
  }

  public final void stop() {
    setRunning(false);
    stopHook();
  }

  public final void setTick(int tick) {
    this.tick = currentTick = tick;
  }

  public Object getAttachment() {
    return getAttachment(0);
  }

  public void setAttachment(Object attachment) {
    setAttachment(0, attachment);
  }

  public Object getAttachment(int index) {
    return attachments != null ? attachments.get(index) : null;
  }

  public void setAttachment(int index, Object attachment) {
    if (attachments == null) {
      attachments = new HashMap<>();
    }
    attachments.put(index, attachment);
  }

  public interface Execution {

    void execute(PEvent event);
  }
}
