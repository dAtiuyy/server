package com.palidinodh.npccombat;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.event.wellofgoodwill.WellOfGoodwillBoostType;
import com.palidinodh.osrscore.world.event.wellofgoodwill.WellOfGoodwillEvent;
import java.util.Arrays;
import java.util.List;

class AnimatedSteelArmourCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder();
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WARRIOR_GUILD_TOKEN, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_FULL_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_PLATEBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_PLATELEGS)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.ANIMATED_STEEL_ARMOUR_46);
    combat.hitpoints(NpcCombatHitpoints.total(40));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(40)
            .defenceLevel(40)
            .bonus(BonusType.MELEE_ATTACK, 4)
            .bonus(BonusType.DEFENCE_STAB, 50)
            .bonus(BonusType.DEFENCE_SLASH, 25)
            .bonus(BonusType.DEFENCE_CRUSH, 19)
            .bonus(BonusType.DEFENCE_MAGIC, 400)
            .bonus(BonusType.DEFENCE_RANGED, 400)
            .build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.deathAnimation(836).blockAnimation(404);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(5));
    style.animation(390).attackSpeed(4);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public Item dropTableDropGetItemHook(
      Player player,
      Tile tile,
      int dropRateDivider,
      int roll,
      NpcCombatDropTable table,
      NpcCombatDropTableDrop drop,
      Item item) {
    if (player
            .getWorld()
            .getWorldEvent(WellOfGoodwillEvent.class)
            .isBoost(WellOfGoodwillBoostType.MINIGAMES)
        && item.getId() == ItemId.WARRIOR_GUILD_TOKEN) {
      return new Item(
          ItemId.WARRIOR_GUILD_TOKEN,
          (int) (item.getAmount() * WellOfGoodwillBoostType.MINIGAMES.getMultiplier()));
    }
    return item;
  }
}
