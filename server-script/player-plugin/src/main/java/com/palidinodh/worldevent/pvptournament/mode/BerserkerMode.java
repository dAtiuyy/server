package com.palidinodh.worldevent.pvptournament.mode;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Loadout;
import com.palidinodh.osrscore.model.entity.player.magic.SpellbookType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.clanwars.ClanWarsRule;
import com.palidinodh.playerplugin.clanwars.ClanWarsRuleOption;
import com.palidinodh.worldevent.pvptournament.ModeMap;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
class BerserkerMode implements Mode {

  private List<SubMode> subModes = new ArrayList<>();

  BerserkerMode() {
    var mode =
        SubMode.builder()
            .name("Berserker")
            .map(ModeMap.FIVE_BY_FIVE_ROOMS)
            .spellbook(SpellbookType.LUNAR)
            .brewCap(3);
    mode.rules(
        Mode.buildRules(
            ClanWarsRule.PRAYER,
            ClanWarsRuleOption.NO_OVERHEADS,
            ClanWarsRule.SPECIAL_ATTACKS,
            ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD));
    mode.attackLevel(60)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(45)
        .hitpointsLevel(99)
        .prayerLevel(99);
    mode.rune(ItemId.ASTRAL_RUNE).rune(ItemId.DEATH_RUNE).rune(ItemId.EARTH_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out",
            new Item[] {
              new Item(ItemId.BERSERKER_HELM),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.AMULET_OF_TORTURE),
              new Item(ItemId.DRAGON_SCIMITAR),
              new Item(ItemId.FIGHTER_TORSO),
              new Item(ItemId.RUNE_DEFENDER),
              null,
              new Item(ItemId.RUNE_PLATELEGS),
              null,
              new Item(ItemId.BARROWS_GLOVES),
              new Item(ItemId.RUNE_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              null
            },
            new Item[] {
              new Item(ItemId.BARRELCHEST_ANCHOR),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              null,
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(
        new Loadout.Entry("+ Dragon Dagger (P++)", new Item(ItemId.DRAGON_DAGGER_P_PLUS_PLUS)));
    mode.loadout(new Loadout.Entry("+ Dragon Mace", new Item(ItemId.DRAGON_MACE)));
    mode.loadout(new Loadout.Entry("+ Granite Maul", new Item(ItemId.GRANITE_MAUL)));
    subModes.add(mode.build());

    mode =
        SubMode.builder()
            .name("Berserker Ranged")
            .map(ModeMap.ONE_ROOM)
            .spellbook(SpellbookType.LUNAR)
            .brewCap(3);
    mode.rules(
        Mode.buildRules(
            ClanWarsRule.PRAYER,
            ClanWarsRuleOption.NO_OVERHEADS,
            ClanWarsRule.SPECIAL_ATTACKS,
            ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD));
    mode.attackLevel(60)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(45)
        .hitpointsLevel(99)
        .prayerLevel(99);
    mode.rune(ItemId.ASTRAL_RUNE).rune(ItemId.DEATH_RUNE).rune(ItemId.EARTH_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out #1",
            new Item[] {
              new Item(ItemId.BERSERKER_HELM),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.AMULET_OF_TORTURE),
              new Item(ItemId.MAGIC_SHORTBOW_I),
              new Item(ItemId.FIGHTER_TORSO),
              null,
              null,
              new Item(ItemId.ZAMORAK_CHAPS),
              null,
              new Item(ItemId.BARROWS_GLOVES),
              new Item(ItemId.RUNE_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              new Item(ItemId.AMETHYST_ARROW_P_PLUS_PLUS, 8000)
            },
            new Item[] {
              null,
              new Item(ItemId.RUNE_DEFENDER),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.RANGING_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "Load-out #2",
            new Item[] {
              new Item(ItemId.BERSERKER_HELM),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.AMULET_OF_TORTURE),
              new Item(ItemId.RUNE_KNIFE_P_PLUS_PLUS, 8000),
              new Item(ItemId.FIGHTER_TORSO),
              new Item(ItemId.RUNE_DEFENDER),
              null,
              new Item(ItemId.ZAMORAK_CHAPS),
              null,
              new Item(ItemId.BARROWS_GLOVES),
              new Item(ItemId.RUNE_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              null
            },
            new Item[] {
              null,
              new Item(ItemId.RANGING_POTION_4),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(
        new Loadout.Entry("+ Dragon Dagger (P++)", new Item(ItemId.DRAGON_DAGGER_P_PLUS_PLUS)));
    mode.loadout(new Loadout.Entry("+ Dragon Mace", new Item(ItemId.DRAGON_MACE)));
    mode.loadout(
        new Loadout.Entry(
            "+ Dark Bow",
            new Item(ItemId.DARK_BOW),
            new Item(ItemId.DRAGON_ARROW_P_PLUS_PLUS, 8000)));
    mode.loadout(new Loadout.Entry("+ Barrelchest Anchor", new Item(ItemId.BARRELCHEST_ANCHOR)));
    mode.loadout(
        new Loadout.Entry(
            "+ Heavy Ballista",
            new Item(ItemId.HEAVY_BALLISTA),
            new Item(ItemId.DRAGON_JAVELIN_P_PLUS_PLUS, 8000)));
    mode.loadout(new Loadout.Entry("+ Granite Maul", new Item(ItemId.GRANITE_MAUL)));
    subModes.add(mode.build());
  }
}
