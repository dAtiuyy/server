package com.palidinodh.worldevent.pvptournament.mode;

import com.palidinodh.osrscore.model.entity.player.Loadout;
import com.palidinodh.osrscore.model.entity.player.magic.SpellbookType;
import com.palidinodh.worldevent.pvptournament.ModeMap;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;

@Builder
@Getter
public class SubMode {

  private String name;
  @Builder.Default private ModeMap map = ModeMap.ONE_ROOM;
  private SpellbookType spellbook;
  private int brewCap;
  private int[] rules;
  @Builder.Default private int teamSize = 1;
  @Builder.Default private int attackLevel = 1;
  @Builder.Default private int defenceLevel = 1;
  @Builder.Default private int strengthLevel = 1;
  @Builder.Default private int hitpointsLevel = 10;
  @Builder.Default private int rangedLevel = 1;
  @Builder.Default private int prayerLevel = 1;
  @Builder.Default private int magicLevel = 1;
  @Singular private List<Integer> runes;
  @Singular private List<Loadout.Entry> loadouts;

  public int[] getSkillLevels() {
    return new int[] {
      attackLevel, defenceLevel, strengthLevel, hitpointsLevel, rangedLevel, prayerLevel, magicLevel
    };
  }
}
