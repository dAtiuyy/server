package com.palidinodh.command.beta;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler.Beta;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("noclip")
class NoclipCommand implements CommandHandler, Beta {

  @Override
  public void execute(Player player, String name, String message) {
    player.getMovement().setNoclip(!player.getMovement().isNoclip());
  }
}
