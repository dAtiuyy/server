package com.palidinodh.command.all;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("hideyell")
class HideYellCommand implements CommandHandler {

  @Override
  public String getExample(String name) {
    return "- Toggle to hide yell messages";
  }

  @Override
  public void execute(Player player, String name, String message) {
    if (player.getMessaging().isHiddenYell()) {
      player.getMessaging().setHiddenYell(false);
      player.getGameEncoder().sendMessage("You unhide all yell messages.");
    } else if (!player.getMessaging().isHiddenYell()) {
      player.getMessaging().setHiddenYell(true);
      player.getGameEncoder().sendMessage("You hide all yell messages.");
    }
  }
}
