package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropLocationType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class EventChaosElementalCombat extends NpcCombat {

  @Inject private Npc npc;
  private List<Npc> fanatics = new ArrayList<>();
  private boolean summonedFanatics;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .rareDropTableDenominator(16)
            .gemDropTableDenominator(42)
            .locationType(NpcCombatDropLocationType.UNDER_OPPONENT)
            .additionalPlayers(2);

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.CHAOS_ELEMENTAL_610_16016);
    combat.spawn(NpcCombatSpawn.builder().respawnDelay(36000).build());
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(2000).barType(HitpointsBarType.GREEN_RED_120).build());
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(250)
            .magicLevel(250)
            .rangedLevel(250)
            .defenceLevel(200)
            .bonus(BonusType.MELEE_DEFENCE, 50)
            .bonus(BonusType.DEFENCE_MAGIC, 50)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(8).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.deathAnimation(3147);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE);
    style.damage(NpcCombatDamage.maximum(28));
    style.animation(3146).attackSpeed(5);
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(28));
    style.animation(3146).attackSpeed(5);
    style.castGraphic(new Graphic(556, 100)).targetGraphic(new Graphic(558, 100));
    style.projectile(NpcCombatProjectile.id(557));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(28));
    style.animation(3146).attackSpeed(5);
    style.castGraphic(new Graphic(556, 100)).targetGraphic(new Graphic(558, 100));
    style.projectile(NpcCombatProjectile.id(557));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void npcApplyHitEndHook(Hit hit) {
    if (isDead() || getHitpoints() > getMaxHitpoints() / 2) {
      return;
    }
    if (summonedFanatics) {
      return;
    }
    summonedFanatics = true;
    fanatics.add(
        npc.getController()
            .addNpc(
                new NpcSpawn(
                    new Tile(npc.getX() - 1, npc.getY() + 2, npc.getHeight()),
                    NpcId.CHAOS_FANATIC_202)));
    fanatics.add(
        npc.getController()
            .addNpc(
                new NpcSpawn(
                    new Tile(npc.getX() + 1, npc.getY() - 2, npc.getHeight()),
                    NpcId.CHAOS_FANATIC_202)));
    for (var npc2 : fanatics) {
      npc2.getSpawn().moveDistance(4);
    }
  }

  @Override
  public void spawnHook() {
    summonedFanatics = false;
  }

  @Override
  public void despawnHook() {
    npc.getWorld().removeNpcs(fanatics);
    fanatics.clear();
  }

  @Override
  public void tickStartHook() {
    if (fanatics.isEmpty()) {
      return;
    }
    fanatics.removeIf(npc2 -> npc2.getCombat().isDead());
  }

  @Override
  public void attackedActionsOpponentHook(Entity opponent) {
    if (!opponent.isPlayer()) {
      return;
    }
    var player = opponent.asPlayer();
    player.getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
  }

  @Override
  public boolean canBeAttackedHook(
      Entity opponent, boolean sendMessage, HitStyleType hitStyleType) {
    if (!opponent.isPlayer()) {
      return false;
    }
    var player = opponent.asPlayer();
    if (!fanatics.isEmpty()) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("Chaos Elemental is currently immune to attacks.");
      }
      return false;
    }
    if (player.getEquipment().wearingFullVoid(HitStyleType.MELEE)
        || player.getEquipment().wearingFullVoid(HitStyleType.RANGED)
        || player.getEquipment().wearingFullVoid(HitStyleType.MELEE)) {
      player
          .getGameEncoder()
          .sendMessage("Your armour doesn't seem to be effective against the Chaos Elemental.");
    }
    return true;
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (additionalPlayerLoopCount == 0) {
      player
          .getController()
          .addMapItem(
              new Item(ItemId.BLOODIER_KEY), dropTile, MapItem.NORMAL_TIME, MapItem.NORMAL_APPEAR);
      npc.getController()
          .sendMessage(
              "<col=ff0000>A bloodier key has been dropped for " + player.getUsername() + "!", 16);
    } else {
      player
          .getController()
          .addMapItem(
              new Item(ItemId.BLOODY_KEY), dropTile, MapItem.NORMAL_TIME, MapItem.NORMAL_APPEAR);
      npc.getController()
          .sendMessage(
              "<col=ff0000>A bloody key has been dropped for " + player.getUsername() + "!", 16);
    }
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (!opponent.isPlayer()) {
      return damage;
    }
    var player = opponent.asPlayer();
    if ((!npc.getController().inMultiCombat() || !opponent.getController().inMultiCombat())
        && damage > 6) {
      damage = 6;
    }
    if (player.getEquipment().wearingFullVoid(HitStyleType.MELEE)
        || player.getEquipment().wearingFullVoid(HitStyleType.RANGED)
        || player.getEquipment().wearingFullVoid(HitStyleType.MELEE)) {
      damage = 0;
    }
    return damage;
  }
}
