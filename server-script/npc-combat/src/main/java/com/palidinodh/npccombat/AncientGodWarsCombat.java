package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.Arrays;
import java.util.List;

class AncientGodWarsCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var spiritualMageDrop =
        NpcCombatDrop.builder().gemDropTableDenominator(128).clue(ClueScrollType.HARD, 128);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(128).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_MASK)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_TOP)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_LEGS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_GLOVES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_BOOTS)));
    spiritualMageDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_ESSENCE)));
    spiritualMageDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NIHIL_SHARD, 5, 9)));
    spiritualMageDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_BOOTS)));
    spiritualMageDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SMOKE_RUNE, 100, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_SNAPDRAGON)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 6900, 6942)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_BREW_3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPER_DEFENCE_3)));
    spiritualMageDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ASTRAL_RUNE, 38, 98)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAVA_RUNE, 30, 60)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_RUNE, 250)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MUD_RUNE, 40, 70)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 1300, 1337)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_BREW_2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPER_RESTORE_3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_BAR_NOTED, 1, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COAL_NOTED, 1, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 46)));
    spiritualMageDrop.table(dropTable.build());

    var spiritualMageCombat = NpcCombatDefinition.builder();
    spiritualMageCombat.id(NpcId.SPIRITUAL_MAGE_182);
    spiritualMageCombat.hitpoints(NpcCombatHitpoints.total(125));
    spiritualMageCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(200)
            .defenceLevel(100)
            .bonus(BonusType.ATTACK_MAGIC, 52)
            .bonus(BonusType.DEFENCE_STAB, 420)
            .bonus(BonusType.DEFENCE_SLASH, 400)
            .bonus(BonusType.DEFENCE_CRUSH, 420)
            .bonus(BonusType.DEFENCE_MAGIC, 200)
            .build());
    spiritualMageCombat.slayer(NpcCombatSlayer.builder().level(83).build());
    spiritualMageCombat.aggression(
        NpcCombatAggression.builder().type(NpcCombatAggression.Type.PLAYERS).always(true).build());
    spiritualMageCombat.deathAnimation(836).blockAnimation(404);
    spiritualMageCombat.drop(spiritualMageDrop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(38));
    style.effect(NpcCombatEffect.builder().poison(4).build());
    style.animation(1978).attackSpeed(5);
    style.targetGraphic(new Graphic(387, 100));
    spiritualMageCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(38));
    style.effect(NpcCombatEffect.builder().statDrain(Skills.ATTACK, 1).build());
    style.animation(1978).attackSpeed(5);
    style.targetGraphic(new Graphic(381));
    spiritualMageCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(38));
    style.effect(NpcCombatEffect.builder().healByDamage(0.25).build());
    style.animation(1978).attackSpeed(5);
    style.targetGraphic(new Graphic(375));
    spiritualMageCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(38));
    style.effect(NpcCombatEffect.builder().bind(25).build());
    style.animation(1978).attackSpeed(5);
    style.targetGraphic(new Graphic(367));
    spiritualMageCombat.style(style.build());

    var bloodReacerDrop =
        NpcCombatDrop.builder().gemDropTableDenominator(128).clue(ClueScrollType.HARD, 128);
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_MASK)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_TOP)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_LEGS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_GLOVES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_BOOTS)));
    bloodReacerDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_ESSENCE)));
    bloodReacerDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NIHIL_SHARD, 2, 7)));
    bloodReacerDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MIND_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NATURE_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_RUNE, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_SNAPDRAGON)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPER_DEFENCE_1)));
    bloodReacerDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ASTRAL_RUNE, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_RUNE, 250)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MUD_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 500)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 1300, 1337)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_POTION_1)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_POTION_2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_BAR_NOTED, 1, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COAL_NOTED, 1, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 23)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.POTATO_CACTUS)));
    bloodReacerDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MALICIOUS_ASHES)));
    bloodReacerDrop.table(dropTable.build());

    var bloodReaverCombat = NpcCombatDefinition.builder();
    bloodReaverCombat.id(NpcId.BLOOD_REAVER_174);
    bloodReaverCombat.hitpoints(NpcCombatHitpoints.total(125));
    bloodReaverCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(190)
            .defenceLevel(100)
            .bonus(BonusType.DEFENCE_STAB, 20)
            .bonus(BonusType.DEFENCE_SLASH, 80)
            .bonus(BonusType.DEFENCE_CRUSH, 120)
            .bonus(BonusType.DEFENCE_MAGIC, 300)
            .bonus(BonusType.DEFENCE_RANGED, 55)
            .build());
    bloodReaverCombat.aggression(
        NpcCombatAggression.builder().type(NpcCombatAggression.Type.PLAYERS).always(true).build());
    bloodReaverCombat.deathAnimation(9192).blockAnimation(9196);
    bloodReaverCombat.drop(bloodReacerDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(20).splashOnMiss(true).build());
    style.animation(9194).attackSpeed(5);
    style.targetGraphic(new Graphic(373));
    style.projectile(NpcCombatProjectile.id(374));
    style.effect(NpcCombatEffect.builder().healByDamage(0.25).build());
    bloodReaverCombat.style(style.build());

    var spiritualWarriorDrop =
        NpcCombatDrop.builder().gemDropTableDenominator(128).clue(ClueScrollType.HARD, 128);
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_MASK)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_TOP)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_LEGS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_GLOVES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_BOOTS)));
    spiritualWarriorDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NIHIL_SHARD, 2, 5)));
    spiritualWarriorDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LOBSTER)));
    spiritualWarriorDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NATURE_RUNE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_RUNE, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_SNAPDRAGON)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPER_DEFENCE_1)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPER_STRENGTH_1)));
    spiritualWarriorDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MIND_RUNE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MUD_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 400, 499)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 1300, 1337)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPER_ATTACK_1)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_POTION_2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_LONGSWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_BAR_NOTED, 1, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COAL_NOTED, 1, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 23)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_CHAINBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.POTATO_CACTUS)));
    spiritualWarriorDrop.table(dropTable.build());

    var spiritualWarriorCombat = NpcCombatDefinition.builder();
    spiritualWarriorCombat.id(NpcId.SPIRITUAL_WARRIOR_158);
    spiritualWarriorCombat.hitpoints(NpcCombatHitpoints.total(100));
    spiritualWarriorCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(190)
            .magicLevel(50)
            .defenceLevel(100)
            .bonus(BonusType.DEFENCE_STAB, 100)
            .bonus(BonusType.DEFENCE_SLASH, 100)
            .bonus(BonusType.DEFENCE_CRUSH, 80)
            .bonus(BonusType.DEFENCE_RANGED, 250)
            .build());
    spiritualWarriorCombat.slayer(NpcCombatSlayer.builder().level(68).build());
    spiritualWarriorCombat.aggression(
        NpcCombatAggression.builder().type(NpcCombatAggression.Type.PLAYERS).always(true).build());
    spiritualWarriorCombat.deathAnimation(836).blockAnimation(7056);
    spiritualWarriorCombat.drop(spiritualWarriorDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE);
    style.damage(NpcCombatDamage.maximum(20));
    style.animation(7045).attackSpeed(5);
    spiritualWarriorCombat.style(style.build());

    var spiritualRangerDrop =
        NpcCombatDrop.builder().gemDropTableDenominator(128).clue(ClueScrollType.HARD, 128);
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_MASK)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_TOP)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_LEGS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_GLOVES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_CEREMONIAL_BOOTS)));
    spiritualRangerDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NIHIL_SHARD, 2, 5)));
    spiritualRangerDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHARK)));
    spiritualRangerDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NATURE_RUNE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_ARROW, 12)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_ARROW, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_SNAPDRAGON)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPER_DEFENCE_1)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DIAMOND)));
    spiritualRangerDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_BOLTS, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 500)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 1300, 1337)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RANGING_POTION_2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_POTION_2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_LONGSWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_BAR_NOTED, 1, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COAL_NOTED, 1, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 23)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLUE_DRAGON_SCALE_NOTED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GREEN_DHIDE_BODY)));
    spiritualRangerDrop.table(dropTable.build());

    var spiritualRangerCombat = NpcCombatDefinition.builder();
    spiritualRangerCombat.id(NpcId.SPIRITUAL_RANGER_158);
    spiritualRangerCombat.hitpoints(NpcCombatHitpoints.total(110));
    spiritualRangerCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(100)
            .rangedLevel(190)
            .defenceLevel(100)
            .bonus(BonusType.DEFENCE_STAB, 20)
            .bonus(BonusType.DEFENCE_CRUSH, 20)
            .bonus(BonusType.DEFENCE_MAGIC, 300)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    spiritualRangerCombat.slayer(NpcCombatSlayer.builder().level(63).build());
    spiritualRangerCombat.aggression(
        NpcCombatAggression.builder().type(NpcCombatAggression.Type.PLAYERS).always(true).build());
    spiritualRangerCombat.deathAnimation(836).blockAnimation(424);
    spiritualRangerCombat.drop(spiritualRangerDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(20));
    style.animation(426).attackSpeed(5);
    style.castGraphic(new Graphic(1385, 96));
    style.projectile(NpcCombatProjectile.id(1384));
    spiritualRangerCombat.style(style.build());

    return Arrays.asList(
        spiritualMageCombat.build(),
        bloodReaverCombat.build(),
        spiritualWarriorCombat.build(),
        spiritualRangerCombat.build());
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    switch (npc.getId()) {
      case NpcId.SPIRITUAL_WARRIOR_158:
      case NpcId.SPIRITUAL_RANGER_158:
        player.getArea().script("increase_ancient_killcount", 2);
        break;
      case NpcId.BLOOD_REAVER_174:
        player.getArea().script("increase_ancient_killcount", 3);
        break;
      case NpcId.SPIRITUAL_MAGE_182:
        player.getArea().script("increase_ancient_killcount", 5);
        break;
    }
  }

  @Override
  public boolean canBeAggressiveHook(Entity opponent) {
    if (!opponent.isPlayer()) {
      return true;
    }
    return hasGodItem(opponent.asPlayer());
  }

  private static boolean hasGodItem(Player player) {
    if (player.getEquipment().getWeaponId() == ItemId.ZARYTE_CROSSBOW) {
      return true;
    }
    if (player.getEquipment().getShieldId() == ItemId.BOOK_OF_DARKNESS) {
      return true;
    }
    if (player.getEquipment().getHeadId() == ItemId.TORVA_FULL_HELM) {
      return true;
    }
    if (player.getEquipment().getHeadId() == ItemId.HOOD_OF_DARKNESS) {
      return true;
    }
    if (player.getEquipment().getChestId() == ItemId.TORVA_PLATEBODY) {
      return true;
    }
    if (player.getEquipment().getChestId() == ItemId.ROBE_TOP_OF_DARKNESS) {
      return true;
    }
    if (player.getEquipment().getLegId() == ItemId.TORVA_PLATELEGS) {
      return true;
    }
    if (player.getEquipment().getLegId() == ItemId.ROBE_BOTTOM_OF_DARKNESS) {
      return true;
    }
    if (player.getEquipment().getHandId() == ItemId.ZARYTE_VAMBRACES) {
      return true;
    }
    if (player.getEquipment().getHandId() == ItemId.GLOVES_OF_DARKNESS) {
      return true;
    }
    if (player.getEquipment().getFootId() == ItemId.BOOTS_OF_DARKNESS) {
      return true;
    }
    if (player.getEquipment().getAmmoId() == ItemId.ANCIENT_BLESSING) {
      return true;
    }
    return player.getEquipment().hasItemIgnoreCase("Ancient");
  }
}
