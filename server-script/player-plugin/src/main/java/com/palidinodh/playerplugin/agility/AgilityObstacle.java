package com.palidinodh.playerplugin.agility;

import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PEvent;
import java.util.ArrayDeque;
import java.util.Deque;

public class AgilityObstacle extends PEvent {

  private Player player;
  private MapObject mapObject;
  private int level;
  private int experience;
  private Deque<AgilityEvent> events = new ArrayDeque<>();

  public AgilityObstacle(Player player) {
    this(player, null, 1, 0);
  }

  public AgilityObstacle(Player player, MapObject mapObject, int level, int experience) {
    this.player = player;
    this.mapObject = mapObject;
    this.level = level;
    this.experience = experience;
  }

  @Override
  public void execute() {
    if (getExecutions() == 0) {
      if (player.getSkills().getLevel(Skills.AGILITY) < level) {
        player
            .getGameEncoder()
            .sendMessage("You need an Agility level of " + level + " to do this.");
        stop();
        return;
      }
    }
    if (events.isEmpty()) {
      stop();
      return;
    }
    var event = events.peek();
    if (event.getDelay() > 0) {
      event.setDelay(event.getDelay() - 1);
      return;
    }
    if (!event.complete(player)) {
      return;
    }
    events.remove();
  }

  @Override
  public void stopHook() {
    player.unlock();
    if (!events.isEmpty()) {
      return;
    }
    if (experience > 0) {
      var xp = experience;
      if (player.getEquipment().wearingMinimumGraceful()) {
        xp *= 1.1;
      }
      player.getSkills().addXp(Skills.AGILITY, xp);
    }
    Diary.getDiaries(player)
        .forEach(d -> d.makeItem(player, Skills.AGILITY, new Item(-1), null, mapObject));
    player
        .getPluginList()
        .forEach(
            p ->
                p.create(
                    Skills.AGILITY, null, mapObject, PArrayList.asList(), PArrayList.asList()));
  }

  public AgilityObstacle add(AgilityEvent event) {
    return add(0, event);
  }

  public AgilityObstacle add(int delay, AgilityEvent event) {
    event.setDelay(delay);
    events.add(event);
    return this;
  }

  public void start() {
    start(null);
  }

  public void start(MapObject mapObject) {
    player.lock();
    if (mapObject == null || mapObject.getType() != 0) {
      execute();
    }
    player.getWorld().addEvent(this);
  }
}
