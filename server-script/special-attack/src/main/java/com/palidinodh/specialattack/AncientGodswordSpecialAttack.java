package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.ANCIENT_GODSWORD})
class AncientGodswordSpecialAttack extends SpecialAttack {

  AncientGodswordSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(9171);
    entry.castGraphic(new Graphic(1996));
    entry.castSound(new Sound(2850));
    entry.accuracyModifier(2);
    entry.damageModifier(1.1);
    addEntry(entry);
  }
}
