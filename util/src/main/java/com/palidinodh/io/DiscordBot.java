package com.palidinodh.io;

import com.palidinodh.rs.communication.ResponseServer;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PLogger;
import com.palidinodh.util.PString;
import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.channel.AutoArchiveDuration;
import org.javacord.api.entity.channel.ServerTextChannel;
import org.javacord.api.entity.channel.ServerThreadChannel;
import org.javacord.api.entity.message.MessageBuilder;
import org.javacord.api.event.message.MessageCreateEvent;

public class DiscordBot {

  private static final int MAX_ANNOUNCEMENTS = 8;

  @Getter private static DiscordApi api;
  @Getter private static PArrayList<String> announcements = new PArrayList<>();
  private static Map<String, Command> commands = new HashMap<>();
  private static int worldId;

  public static void init(String token, int _worldId) {
    if (token == null) {
      return;
    }
    if (api != null) {
      disconnect();
    }
    worldId = _worldId;
    var builder = new DiscordApiBuilder();
    builder.setToken(token);
    builder.setAllIntents();
    builder.addMessageCreateListener(DiscordBot::checkMessage);
    var login = builder.login();
    login.thenAccept(
        a -> {
          api = a;
          join();
        });
  }

  public static void sendMessage(DiscordChannel channelType, String message) {
    if (api == null) {
      return;
    }
    if (Settings.getInstance().isLocal()) {
      return;
    }
    if (Settings.getInstance().isBeta()) {
      return;
    }
    try {
      var channelId = Settings.getInstance().getDiscordChannel(channelType);
      if (channelId == 0L) {
        return;
      }
      message = message.replaceAll("<.*>", "");
      if (message.endsWith(" @everyone")) {
        int everyoneIndex = message.lastIndexOf(" @everyone");
        message = "```" + message.substring(0, everyoneIndex) + "``` @everyone";
      } else {
        message = "```" + message + "```";
      }
      var channel = getTextChannelById(channelId);
      if (channel == null) {
        return;
      }
      channel.sendMessage(message);
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  public static void sendMessage(DiscordChannel channelType, String threadName, String message) {
    if (api == null) {
      return;
    }
    if (Settings.getInstance().isLocal()) {
      return;
    }
    if (Settings.getInstance().isBeta()) {
      return;
    }
    try {
      var channelId = Settings.getInstance().getDiscordChannel(channelType);
      if (channelId == 0L) {
        return;
      }
      message = message.replaceAll("<.*>", "");
      if (message.endsWith(" @everyone")) {
        int everyoneIndex = message.lastIndexOf(" @everyone");
        message = "```" + message.substring(0, everyoneIndex) + "``` @everyone";
      } else {
        message = "```" + message + "```";
      }
      var thread = getThreadChannelByName(channelId, threadName);
      if (thread == null) {
        var channel = getTextChannelById(channelId);
        if (channel == null) {
          return;
        }
        new MessageBuilder()
            .append(message)
            .send(channel)
            .whenComplete((m, t) -> m.createThread(threadName, AutoArchiveDuration.ONE_DAY));
      } else {
        thread.sendMessage(message);
      }
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  public static void disconnect() {
    if (api == null) {
      return;
    }
    api.disconnect();
    api = null;
  }

  public static void loadAnnouncements() {
    var channelId = Settings.getInstance().getDiscordChannel(DiscordChannel.ANNOUNCEMENTS);
    if (channelId == 0L) {
      return;
    }
    var channel = getTextChannelById(channelId);
    if (channel == null) {
      return;
    }
    try {
      var messageSet = channel.getMessages(MAX_ANNOUNCEMENTS * 2).get();
      announcements.clear();
      messageSet.forEach(m -> addAnnouncement(m.getContent()));
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  public static void addCommand(String name, Command command) {
    commands.put(name, command);
  }

  private static void join() {
    loadAnnouncements();
    if (worldId == -1) {
      return;
    }
    if (Settings.getInstance().isLocal()) {
      return;
    }
    if (Settings.getInstance().isBeta()) {
      return;
    }
    sendMessage(DiscordChannel.ANNOUNCEMENTS, Settings.getInstance().getName() + " is online!");
  }

  private static void checkMessage(MessageCreateEvent event) {
    var author = event.getMessageAuthor();
    if (event.getMessageContent().startsWith("::")) {
      checkCommandMessage(event);
      return;
    }
    if (event.getChannel().getId()
        == Settings.getInstance().getDiscordChannel(DiscordChannel.ANNOUNCEMENTS)) {
      addAnnouncement(event.getMessageContent());
      return;
    }
    if (event.getChannel().getId()
        == Settings.getInstance().getDiscordChannel(DiscordChannel.CLAN_CHAT)) {
      checkClanChatMessage(event);
    }
  }

  private static void checkCommandMessage(MessageCreateEvent event) {
    if (!event.getMessageAuthor().canKickUsersFromServer()) {
      return;
    }
    var message = event.getMessageContent();
    var commandName = message.substring(2).split(" ")[0].toLowerCase();
    var command = commands.get(commandName);
    if (command == null) {
      return;
    }
    try {
      var commandNameLength = 2 + commandName.length() + 1;
      message = commandNameLength < message.length() ? message.substring(commandNameLength) : "";
      command.execute(event, message);
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  private static void checkClanChatMessage(MessageCreateEvent event) {
    if (worldId != -1) {
      return;
    }
    if (event.getMessageAuthor().isBotUser()) {
      return;
    }
    var chatIcon = "[D]";
    if (Settings.getInstance().getDiscordChatIcon() != -1) {
      chatIcon = "<img=" + Settings.getInstance().getDiscordChatIcon() + ">";
    }
    if (event.getMessageAuthor().isServerAdmin()) {
      chatIcon += "<img=1>";
    } else if (event.getMessageAuthor().canKickUsersFromServer()) {
      chatIcon += "<img=0>";
    }
    var username = PString.cleanName(event.getMessageAuthor().getDisplayName());
    if (username.isEmpty()) {
      username = PString.cleanName(event.getMessageAuthor().getName());
    }
    if (!username.isEmpty()) {
      var message = PString.removeHtml(event.getMessageContent());
      message = PString.cleanString(message);
      ResponseServer.getInstance().sendGlobalClanMessage(chatIcon + username, message);
    }
  }

  private static ServerTextChannel getTextChannelById(long channelId) {
    if (channelId == 0L) {
      return null;
    }
    var opChannel = api.getServerTextChannelById(channelId);
    if (opChannel.isEmpty()) {
      return null;
    }
    return opChannel.get();
  }

  private static ServerThreadChannel getThreadChannelByName(long channelId, String threadName) {
    if (channelId == 0L || threadName == null) {
      return null;
    }
    var threads =
        api.getServerThreadChannelsByNameIgnoreCase(threadName).toArray(new ServerThreadChannel[0]);
    for (var thread : threads) {
      if (thread.asServerChannel().isEmpty()) {
        continue;
      }
      if (thread.isLocked()) {
        continue;
      }
      var channel = thread.getParent();
      if (channelId != channel.getId()) {
        continue;
      }
      return thread;
    }
    return null;
  }

  private static void addAnnouncement(String s) {
    if (ignoreAnnouncement(s)) {
      return;
    }
    announcements.add(s);
    if (announcements.size() > MAX_ANNOUNCEMENTS) {
      announcements.removeFirst();
    }
  }

  private static boolean ignoreAnnouncement(String s) {
    if (s.contains("is online!")) {
      return true;
    }
    return s.equalsIgnoreCase("@everyone");
  }

  public interface Command {

    void execute(MessageCreateEvent event, String message);
  }
}
