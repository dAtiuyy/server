package com.palidinodh.maparea.wilderness.slayercave;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({13469, 13470, 13725, 13726})
public class SlayerCaveArea extends Area {

  @Override
  public boolean inMultiCombat() {
    return true;
  }

  @Override
  public boolean inWilderness() {
    return true;
  }

  public int getWildernessLevel() {
    return (getTile().getY() - 9920) / 8 + 1;
  }
}
