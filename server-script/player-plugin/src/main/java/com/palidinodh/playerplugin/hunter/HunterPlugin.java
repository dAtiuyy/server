package com.palidinodh.playerplugin.hunter;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.hunter.birdhouse.BirdHouseLocationType;
import com.palidinodh.playerplugin.hunter.birdhouse.BirdHouseTrap;
import com.palidinodh.playerplugin.hunter.birdhouse.BirdHouseType;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PEvent;
import java.util.ArrayDeque;
import java.util.Deque;
import lombok.Getter;

public class HunterPlugin implements PlayerPlugin {

  public static final int TRAP_EXPIRIY = 100;

  @Inject private transient Player player;
  @Getter private transient PArrayList<MapObject> mapObjects = new PArrayList<>();
  @Getter private transient Deque<TempMapObject> traps = new ArrayDeque<>();

  private PArrayList<BirdHouseTrap> birdHouseTraps = new PArrayList<>();

  public static CapturedHunterTrap getCapturedTrap(int objectId) {
    for (var i = 0; i < CapturedHunterTrap.getCapturedTrapEntries().size(); i++) {
      var trap = CapturedHunterTrap.getCapturedTrapEntries().get(i);
      if (trap.getId() != objectId) {
        continue;
      }
      return trap;
    }
    return null;
  }

  public static int getCapturedTrapLevelRequirement(int objectId) {
    var trap = getCapturedTrap(objectId);
    return trap != null ? trap.getLevel() : 1;
  }

  @Override
  public void login() {
    if (!birdHouseTraps.isEmpty()) {
      mapObjects.addAll(birdHouseTraps, BirdHouseTrap::getDisplayMapObject);
      birdHouseTraps.forEach(
          t -> {
            if (t.getLocationType() == null || t.getLocationType().getVarpId() == -1) {
              return;
            }
            player.getGameEncoder().setVarp(t.getLocationType().getVarpId(), t.getVarpValue());
          });
    }
  }

  public void removeLostTraps() {
    traps.removeIf(tempTrap -> !tempTrap.isRunning());
  }

  public boolean layTrap(int itemId, MapObject fromMapObject) {
    var maxTraps = 1;
    var nextLevel = 20;
    if (player.getSkills().getLevel(Skills.HUNTER) >= 80) {
      maxTraps = 5;
    } else if (player.getSkills().getLevel(Skills.HUNTER) >= 60) {
      maxTraps = 4;
      nextLevel = 80;
    } else if (player.getSkills().getLevel(Skills.HUNTER) >= 40) {
      maxTraps = 3;
      nextLevel = 60;
    } else if (player.getSkills().getLevel(Skills.HUNTER) >= 20) {
      maxTraps = 2;
      nextLevel = 40;
    }
    if (player.getArea().inWilderness()) {
      maxTraps++;
    }
    Item[] items;
    MapObject[] trap;
    if (itemId == ItemId.BIRD_SNARE) {
      items = new Item[] {new Item(itemId)};
      trap = new MapObject[] {new MapObject(ObjectId.BIRD_SNARE_9345, 10, 0, player)};
    } else if (fromMapObject != null
        && (fromMapObject.getId() == ObjectId.YOUNG_TREE_8732
            || fromMapObject.getId() == ObjectId.YOUNG_TREE_8990
            || fromMapObject.getId() == ObjectId.YOUNG_TREE_8999
            || fromMapObject.getId() == ObjectId.YOUNG_TREE_9000
            || fromMapObject.getId() == ObjectId.YOUNG_TREE_9341)) {
      if (player.getX() != fromMapObject.getX() && player.getY() != fromMapObject.getY()) {
        return false;
      }
      items = new Item[] {new Item(ItemId.SMALL_FISHING_NET), new Item(ItemId.ROPE)};
      var direction = 0;
      if (player.getX() == fromMapObject.getX() && player.getY() < fromMapObject.getY()) {
        direction = 2;
      } else if (player.getX() > fromMapObject.getX() && player.getY() == fromMapObject.getY()) {
        direction = 1;
      } else if (player.getX() < fromMapObject.getX() && player.getY() == fromMapObject.getY()) {
        direction = 3;
      }
      trap =
          new MapObject[] {
            new MapObject(ObjectId.NET_TRAP_9343, 10, direction, player),
            new MapObject(ObjectId.YOUNG_TREE_8989, 10, direction, fromMapObject)
                .setOriginal(fromMapObject)
          };
    } else if (itemId == ItemId.BOX_TRAP) {
      items = new Item[] {new Item(ItemId.BOX_TRAP)};
      trap = new MapObject[] {new MapObject(ObjectId.BOX_TRAP_9380, 10, 0, player)};
    } else {
      return false;
    }
    var plugin = player.getPlugin(HunterPlugin.class);
    plugin.removeLostTraps();
    for (var item : items) {
      if (player.getInventory().getCount(item.getId()) < item.getAmount()) {
        player
            .getGameEncoder()
            .sendMessage("You need " + item.getAmount() + " " + item.getName() + " to do this.");
        return false;
      }
    }
    if (player.getController().hasSolidMapObject(trap[0])) {
      player.getGameEncoder().sendMessage("You can't set a trap here.");
      return false;
    }
    if (plugin.getTraps().size() >= maxTraps) {
      if (nextLevel > 0) {
        player
            .getGameEncoder()
            .sendMessage("You can't set any more traps until level " + nextLevel + ".");
      } else {
        player.getGameEncoder().sendMessage("You can't set any more traps.");
      }
      return false;
    }
    for (var i = 1; i < trap.length; i++) {
      var existing = player.getController().getSolidMapObject(trap[i]);
      if (existing != null && existing.getAttachment() instanceof TempMapObject) {
        player.getGameEncoder().sendMessage("You can't set a trap here.");
        return false;
      }
    }
    var tempTrap = new HunterTrap(player, trap, items);
    if (!tempTrap.isRunning()) {
      return false;
    }
    player.getWorld().addEvent(tempTrap);
    plugin.getTraps().add(tempTrap);
    for (var item : items) {
      player.getInventory().deleteItem(item.getId(), item.getAmount());
    }
    player.lock();
    player.setAnimation(5208);
    player
        .getWorld()
        .addEvent(
            PEvent.singleEvent(
                3,
                e -> {
                  if (player.getController().routeAllow(player.getX() - 1, player.getY())) {
                    player.getMovement().quickRoute(player.getX() - 1, player.getY());
                  } else if (player.getController().routeAllow(player.getX() + 1, player.getY())) {
                    player.getMovement().quickRoute(player.getX() + 1, player.getY());
                  } else if (player.getController().routeAllow(player.getX(), player.getY() - 1)) {
                    player.getMovement().quickRoute(player.getX(), player.getY() - 1);
                  } else if (player.getController().routeAllow(player.getX(), player.getY() + 1)) {
                    player.getMovement().quickRoute(player.getX(), player.getY() + 1);
                  }
                  player.unlock();
                }));
    return true;
  }

  public boolean pickupTrap(MapObject trap) {
    if (!(trap.getAttachment() instanceof TempMapObject)) {
      return false;
    }
    var tempTrap = (TempMapObject) trap.getAttachment();
    if (!(tempTrap.getAttachment() instanceof Integer)) {
      return false;
    }
    var userId = (Integer) tempTrap.getAttachment();
    if (player.getId() != userId) {
      player.getGameEncoder().sendMessage("This trap isn't yours.");
      return false;
    }
    switch (trap.getName().toLowerCase()) {
      case "bird snare":
        {
          player.getInventory().addOrDropItem(ItemId.BIRD_SNARE);
          break;
        }
      case "net trap":
        {
          player.getInventory().addOrDropItem(ItemId.SMALL_FISHING_NET);
          player.getInventory().addOrDropItem(ItemId.ROPE);
          break;
        }
      case "shaking box":
      case "box trap":
        {
          player.getInventory().addOrDropItem(ItemId.BOX_TRAP);
          break;
        }
      default:
        return false;
    }
    tempTrap.setDisableScript(true);
    tempTrap.execute();
    var capturedTrap = getCapturedTrap(trap.getId());
    if (capturedTrap == null) {
      return true;
    }
    var xp = capturedTrap.getExperience();
    if (player.getEquipment().wearingLarupiaOutfit()) {
      xp *= 1.1;
    }
    player.getSkills().addXp(Skills.HUNTER, xp);
    var created = new PArrayList<Item>();
    if (capturedTrap.getItems() != null) {
      for (var item : capturedTrap.getItems()) {
        var quantity = item.getRandomAmount();
        var capturedItem = new Item(item.getId(), item.getRandomAmount());
        if (item.getId() == ItemId.CHINCHOMPA
            || item.getId() == ItemId.RED_CHINCHOMPA
            || item.getId() == ItemId.BLACK_CHINCHOMPA) {
          if (player
                  .getPlugin(BondPlugin.class)
                  .isRelicUnlocked(BondRelicType.BIGGER_HARVEST_HUNTER)
              && PRandom.randomE(2) == 0) {
            capturedItem = new Item(capturedItem.getId(), capturedItem.getAmount() * 2);
          }
        }
        created.add(new Item(capturedItem));
        player.getInventory().addOrDropItem(capturedItem);
        Item finalCapturedItem = capturedItem;
        Diary.getDiaries(player)
            .forEach(d -> d.makeItem(player, Skills.HUNTER, finalCapturedItem, null, trap));
      }
    }
    player
        .getPluginList()
        .forEach(p -> p.create(Skills.HUNTER, null, trap, PArrayList.asList(), created));
    if (trap.getId() == ObjectId.SHAKING_BOX_9382
        || trap.getId() == ObjectId.SHAKING_BOX_9383
        || trap.getId() == ObjectId.SHAKING_BOX) {
      player
          .getPlugin(FamiliarPlugin.class)
          .rollSkillPet(Skills.HUNTER, 131395, ItemId.BABY_CHINCHOMPA_13324);
    }
    return true;
  }

  public void buildBirdHouse(MapObject mapObject, Item item) {
    var type = BirdHouseType.getByItemId(item.getId());
    if (type == null) {
      player.getGameEncoder().sendMessage("Nothing interesting happens.");
      return;
    }
    if (player.getSkills().getLevel(Skills.HUNTER) < type.getLevel()) {
      player
          .getGameEncoder()
          .sendMessage("You need a Hunter level of " + type.getLevel() + " to do this.");
      return;
    }
    if (getBirdHouseTrap(mapObject) != null) {
      player.getGameEncoder().sendMessage("You already have a birdhouse trap built.");
      return;
    }
    var locationType = BirdHouseLocationType.getByTile(mapObject);
    if (locationType == null) {
      player.getGameEncoder().sendMessage("Unknown bird house location.");
      return;
    }
    player.setAnimation(827);
    var birdHouseMapObject = new MapObject(type.getEmptyObjectId(), mapObject);
    var trap = new BirdHouseTrap(birdHouseMapObject, locationType, type);
    birdHouseTraps.add(trap);
    mapObjects.add(birdHouseMapObject);
    if (locationType.getVarpId() != -1) {
      player.getGameEncoder().setVarp(locationType.getVarpId(), trap.getVarpValue());
    } else {
      player.getGameEncoder().sendMapObject(trap.getMapObject());
    }
    player.getInventory().deleteItem(item);
  }

  public void addSeedsBirdHouse(BirdHouseTrap trap, Item item) {
    if (trap == null) {
      return;
    }
    if (player.getInventory().getCount(item.getId()) < item.getAmount()) {
      return;
    }
    if (trap.isStarted()) {
      player.getGameEncoder().sendMessage("You can't add any more seeds to this.");
      return;
    }
    for (var i = 0; i < item.getAmount(); i++) {
      switch (item.getId()) {
        case ItemId.BARLEY_SEED:
        case ItemId.HAMMERSTONE_SEED:
        case ItemId.ASGARNIAN_SEED:
        case ItemId.JUTE_SEED:
        case ItemId.YANILLIAN_SEED:
        case ItemId.KRANDORIAN_SEED:
        case ItemId.GUAM_SEED:
        case ItemId.MARRENTILL_SEED:
        case ItemId.TARROMIN_SEED:
        case ItemId.HARRALANDER_SEED:
          trap.increaseSeededPercent(10);
          break;
        case ItemId.WILDBLOOD_SEED:
        case ItemId.RANARR_SEED:
        case ItemId.TOADFLAX_SEED:
        case ItemId.IRIT_SEED:
        case ItemId.AVANTOE_SEED:
        case ItemId.KWUARM_SEED:
        case ItemId.SNAPDRAGON_SEED:
        case ItemId.CADANTINE_SEED:
        case ItemId.LANTADYME_SEED:
        case ItemId.DWARF_WEED_SEED:
        case ItemId.TORSTOL_SEED:
          trap.increaseSeededPercent(20);
          break;
        default:
          player.getGameEncoder().sendMessage("Nothing interesting happens.");
          return;
      }
      if (!trap.isStarted()) {
        continue;
      }
      player.getInventory().deleteItem(item.getId(), i + 1);
      break;
    }
    player.setAnimation(827);
    if (trap.getLocationType() != null && trap.getLocationType().getVarpId() != -1) {
      player.getGameEncoder().setVarp(trap.getLocationType().getVarpId(), trap.getVarpValue());
    } else {
      player.getGameEncoder().sendMapObject(trap.getMapObject());
    }
    if (trap.isStarted()) {
      player.openDialogue(
          new MessageDialogue(
              "Your birdhouse trap is now full of seed and will start to catch birds."));
    }
  }

  public void dismantleBirdHouse(BirdHouseTrap trap) {
    if (trap == null) {
      return;
    }
    player.setAnimation(827);
    if (trap.getLocationType() != null && trap.getLocationType().getVarpId() != -1) {
      player.getGameEncoder().setVarp(trap.getLocationType().getVarpId(), 0);
    } else {
      player
          .getGameEncoder()
          .sendMapObject(player.getController().getSolidMapObject(trap.getMapObject()));
    }
    birdHouseTraps.remove(trap);
    mapObjects.remove(trap.getMapObject());
    player.getInventory().addOrDropItem(ItemId.CLOCKWORK);
    if (!trap.isStarted()) {
      return;
    }
    var caught = trap.getBirdsCaught();
    var xp = trap.getType().getExperience() * caught;
    if (player.getEquipment().wearingLarupiaOutfit()) {
      xp *= 1.1;
    }
    player.getSkills().addXp(Skills.HUNTER, xp);
    for (var i = 0; i < trap.getType().getNestRolls() * caught; i++) {
      if (!PRandom.inRange(1, 32)) {
        continue;
      }
      player.getInventory().addItem(ItemId.CRUSHED_NEST);
    }
    player.getInventory().addOrDropItem(ItemId.FEATHER, 10 * caught);
    player.getInventory().addOrDropItem(ItemId.RAW_BIRD_MEAT, caught);
  }

  public BirdHouseTrap getBirdHouseTrap(Tile tile) {
    return birdHouseTraps.getIf(t -> t.getMapObject().matchesTile(tile));
  }

  public boolean canCaptureTrap(int objectId) {
    return player.getSkills().getLevel(Skills.HUNTER) >= getCapturedTrapLevelRequirement(objectId);
  }

  public boolean trapSuccess(Npc npc, int level) {
    var myLevel = (int) (player.getSkills().getLevel(Skills.HUNTER) * 1.25);
    myLevel *= 1 + SkillContainer.getMembershipBoost(player);
    if (player.getEquipment().wearingLarupiaOutfit()) {
      myLevel *= 1.1;
    }
    return PRandom.randomE(myLevel + 8) >= PRandom.randomE(level + 2);
  }
}
