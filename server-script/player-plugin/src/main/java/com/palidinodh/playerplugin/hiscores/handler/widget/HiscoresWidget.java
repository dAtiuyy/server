package com.palidinodh.playerplugin.hiscores.handler.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.worldevent.competitivehiscores.CompetitiveHiscoresEvent;

@ReferenceId(WidgetId.HISCORES_1009)
class HiscoresWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    player
        .getWorld()
        .getWorldEvent(CompetitiveHiscoresEvent.class)
        .hiscoresWidgetHandler(player, option, childId, slot, itemId);
  }
}
