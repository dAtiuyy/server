package com.palidinodh.maparea.trollcountry;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId(11577)
@ReferenceIdSet(
    primary = 11321,
    secondary = {112, 113, 114, 115, 116, 117, 118, 119})
public class TrollheimArea extends Area {}
