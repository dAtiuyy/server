package com.palidinodh.npcplugin;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NullNpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.NpcPlugin;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NullNpcId.NULL_8032)
class RuneDragonSpawnPlugin implements NpcPlugin {

  @Inject private Npc npc;
  private int countdown;

  @Override
  public void spawn() {
    countdown = 8;
  }

  @Override
  public void tick() {
    if (--countdown == 0) {
      npc.getCombat().startDeath();
    }
    if (npc.isLocked()) {
      return;
    }
    var following = npc.getMovement().getFollowing();
    if (npc.withinDistance(following, 1) && following instanceof Player) {
      var player = (Player) following;
      player
          .getCombat()
          .addHit(
              new Hit(
                  PRandom.randomI(
                      player.getEquipment().getFootId() == ItemId.INSULATED_BOOTS ? 2 : 8)));
    }
  }
}
