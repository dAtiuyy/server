package com.palidinodh.rs.communicationv2.event;

import com.palidinodh.rs.adaptive.RsFriend;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communicationv2.CommunicationEvent;
import com.palidinodh.rs.communicationv2.notification.PrivateMessageNotification;
import lombok.Getter;

@Getter
public class PrivateMessageEvent extends CommunicationEvent {

  private int userId;
  private int chatIcon;
  private String receiverUsername;
  private String message;

  public PrivateMessageEvent(int userId, int chatIcon, String receiverUsername, String message) {
    this.userId = userId;
    this.chatIcon = chatIcon;
    this.receiverUsername = receiverUsername;
    this.message = message;
  }

  @Override
  public void mainAction() {
    var player = getMainServer().getPlayersById().get(userId);
    if (player == null) {
      return;
    }
    var receiverPlayer = getMainServer().getPlayersByUsername().get(receiverUsername.toLowerCase());
    if (receiverPlayer == null) {
      return;
    }
    if (player.getRights() == RsPlayer.RIGHTS_NONE
        && !RsFriend.canRegister(player, receiverPlayer)) {
      return;
    }
    getMainServer()
        .addNotification(
            new PrivateMessageNotification(
                receiverPlayer.getWorldId(),
                player.getUsername(),
                chatIcon,
                receiverPlayer.getId(),
                message));
  }
}
