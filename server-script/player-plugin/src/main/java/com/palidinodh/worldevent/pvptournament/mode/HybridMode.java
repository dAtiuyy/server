package com.palidinodh.worldevent.pvptournament.mode;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Loadout;
import com.palidinodh.osrscore.model.entity.player.magic.SpellbookType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.clanwars.ClanWarsRule;
import com.palidinodh.playerplugin.clanwars.ClanWarsRuleOption;
import com.palidinodh.worldevent.pvptournament.ModeMap;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
class HybridMode implements Mode {

  private List<SubMode> subModes = new ArrayList<>();

  HybridMode() {
    var mode =
        SubMode.builder()
            .name("Main Brid")
            .map(ModeMap.ONE_ROOM)
            .spellbook(SpellbookType.ANCIENT)
            .brewCap(3);
    mode.rules(
        Mode.buildRules(
            ClanWarsRule.PRAYER,
            ClanWarsRuleOption.NO_OVERHEADS,
            ClanWarsRule.SPECIAL_ATTACKS,
            ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD));
    mode.attackLevel(99)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(99)
        .hitpointsLevel(99)
        .prayerLevel(99);
    mode.rune(ItemId.DEATH_RUNE).rune(ItemId.BLOOD_RUNE).rune(ItemId.WATER_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out",
            new Item[] {
              new Item(ItemId.HELM_OF_NEITIZNOT),
              new Item(ItemId.IMBUED_SARADOMIN_CAPE),
              new Item(ItemId.AMULET_OF_BLOOD_FURY),
              new Item(ItemId.ANCIENT_STAFF),
              new Item(ItemId.MYSTIC_ROBE_TOP),
              new Item(ItemId.BLESSED_SPIRIT_SHIELD),
              null,
              new Item(ItemId.MYSTIC_ROBE_BOTTOM),
              null,
              new Item(ItemId.BARROWS_GLOVES),
              new Item(ItemId.DRAGON_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              null
            },
            new Item[] {
              new Item(ItemId.TORAGS_PLATEBODY),
              new Item(ItemId.ABYSSAL_TENTACLE),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.TORAGS_PLATELEGS),
              new Item(ItemId.DRAGON_DEFENDER),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.KARILS_LEATHERTOP),
              new Item(ItemId.ARMADYL_GODSWORD),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SOUL_RUNE, 5000),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH, 5000)
            },
            -1));
    mode.loadout(
        new Loadout.Entry("+ Dragon Dagger (P++)", new Item(ItemId.DRAGON_DAGGER_P_PLUS_PLUS)));
    mode.loadout(new Loadout.Entry("+ Granite Maul", new Item(ItemId.GRANITE_MAUL)));
    subModes.add(mode.build());

    mode =
        SubMode.builder()
            .name("DH Brid")
            .map(ModeMap.ONE_ROOM)
            .spellbook(SpellbookType.ANCIENT)
            .brewCap(3);
    mode.rules(
        Mode.buildRules(
            ClanWarsRule.PRAYER,
            ClanWarsRuleOption.NO_OVERHEADS,
            ClanWarsRule.SPECIAL_ATTACKS,
            ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD));
    mode.attackLevel(99)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(99)
        .hitpointsLevel(99)
        .prayerLevel(99);
    mode.rune(ItemId.DEATH_RUNE).rune(ItemId.BLOOD_RUNE).rune(ItemId.WATER_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Load-out",
            new Item[] {
              new Item(ItemId.DHAROKS_HELM),
              new Item(ItemId.IMBUED_ZAMORAK_CAPE),
              new Item(ItemId.AMULET_OF_BLOOD_FURY),
              new Item(ItemId.STAFF_OF_LIGHT),
              new Item(ItemId.MYSTIC_ROBE_TOP),
              new Item(ItemId.BLESSED_SPIRIT_SHIELD),
              null,
              new Item(ItemId.MYSTIC_ROBE_BOTTOM),
              null,
              new Item(ItemId.BARROWS_GLOVES),
              new Item(ItemId.DRAGON_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              new Item(ItemId.DRAGONSTONE_DRAGON_BOLTS_E, 8000)
            },
            new Item[] {
              new Item(ItemId.ABYSSAL_TENTACLE),
              new Item(ItemId.DHAROKS_PLATEBODY),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.DRAGON_DEFENDER),
              new Item(ItemId.DHAROKS_PLATELEGS),
              new Item(ItemId.DHAROKS_GREATAXE),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.KARILS_LEATHERTOP),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_RESTORE_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.SUPER_RESTORE_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.GRANITE_MAUL),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH),
              new Item(ItemId.ANGLERFISH)
            },
            -1));
    subModes.add(mode.build());
  }
}
