package com.palidinodh.command.beta;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler.Beta;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("tele")
class TeleportCommand implements CommandHandler, Beta {

  @Override
  public String getExample(String name) {
    return "region_id/x y (z)";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var values = CommandHandler.splitInt(message);
    if (values.length == 1) {
      var id = values[0];
      player.getMovement().teleport(Tile.getAbsRegionX(id) + 32, Tile.getAbsRegionY(id) + 32);
      return;
    }
    var x = values[0];
    var y = values[1];
    var z = 0;
    if (values.length == 3) {
      z = values[2];
    }
    player.getMovement().teleport(x, y, z);
  }
}
