package com.palidinodh.rs.setting;

import com.palidinodh.util.PString;

public enum UserRank {
  UNKNOWN,
  BANNED,
  OVERSEER,
  FORUM_MODERATOR,
  MODERATOR,
  SENIOR_MODERATOR,
  ADVERTISEMENT_MANAGER,
  COMMUNITY_MANAGER,
  ADMINISTRATOR,
  YOUTUBER,
  BETA_TESTER,
  PREMIUM_MEMBER,
  OPAL_MEMBER,
  JADE_MEMBER,
  TOPAZ_MEMBER,
  SAPPHIRE_MEMBER,
  EMERALD_MEMBER,
  RUBY_MEMBER,
  DIAMOND_MEMBER,
  DRAGONSTONE_MEMBER,
  ONYX_MEMBER,
  ZENYTE_MEMBER,
  SPECIAL_MEMBER;

  public String getFormattedName() {
    return PString.formatName(name().toLowerCase().replace('_', ' '));
  }
}
