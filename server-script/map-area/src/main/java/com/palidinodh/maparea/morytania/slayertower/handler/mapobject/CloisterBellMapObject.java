package com.palidinodh.maparea.morytania.slayertower.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;

@ReferenceId(ObjectId.THE_CLOISTER_BELL)
class CloisterBellMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (!player.getSkills().isAnySlayerTask(NpcId.DUSK_248)
        && !Settings.getInstance().isLocal()
        && !Settings.getInstance().isBeta()
        && !Settings.getInstance().isSpawn()) {
      player.getGameEncoder().sendMessage("You need an appropriate task to do this.");
      return;
    }
    if (player.getController().getNpc(NpcId.DUSK_248) != null
        || player.getController().getNpc(NpcId.DUSK_248_7882) != null
        || player.getController().getNpc(NpcId.DUSK_328_7888) != null
        || player.getController().getNpc(NpcId.DAWN_228) != null
        || player.getController().getNpc(NpcId.DAWN_228_7885) != null
        || player.getController().getNpc(NpcId.DUSK_328_7889) != null) {
      player.getGameEncoder().sendMessage("Nothing interesting happens.");
      return;
    }
    player.getGameEncoder().setVarp(1667, 3);
    player.getGameEncoder().sendMapObjectAnimation(mapObject, 7748);
    var dusk =
        player
            .getController()
            .addNpc(new NpcSpawn(Tile.Direction.EAST, new Tile(1685, 4573), NpcId.DUSK_248));
    dusk.setLargeVisibility();
    var dawn =
        player
            .getController()
            .addNpc(new NpcSpawn(Tile.Direction.WEST, new Tile(1705, 4573), NpcId.DAWN_228));
    dawn.setLargeVisibility();
  }
}
