package com.palidinodh.playerplugin.sisterhoodsanctuary;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.PrayerChild;
import com.palidinodh.osrscore.model.Controller;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEventTasks;
import lombok.Getter;
import lombok.Setter;

public class SisterhoodSanctuaryPlugin implements PlayerPlugin {

  public static final NpcSpawn NIGHTMARE_SPAWN =
      new NpcSpawn(new Tile(3870, 9949, 3), NpcId.THE_NIGHTMARE_814_9433);

  @Inject private transient Player player;
  @Getter @Setter private transient int curse;
  @Getter @Setter private transient int parasite;
  @Setter private transient int spore;
  private transient boolean parasitePotion;

  public static Npc getIdleNightmare(Controller controller) {
    return controller.getNpc(
        NpcId.THE_NIGHTMARE_814,
        NpcId.THE_NIGHTMARE_814_9426,
        NpcId.THE_NIGHTMARE_814_9427,
        NpcId.THE_NIGHTMARE_814_9428,
        NpcId.THE_NIGHTMARE_814_9429,
        NpcId.THE_NIGHTMARE_814_9430,
        NpcId.THE_NIGHTMARE_814_9431);
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("sisterhood_sanctuary_plugin_spore")) {
      return spore > 0 ? Boolean.TRUE : Boolean.FALSE;
    }
    return null;
  }

  @Override
  public void restore() {
    curse = 0;
    parasite = 0;
    parasitePotion = false;
  }

  @Override
  public void tick() {
    if (!player.getArea().is("SisterhoodSanctuary")) {
      return;
    }
    var theNightmare = getIdleNightmare(player.getController());
    if (theNightmare == null || theNightmare.getCombat().isDead()) {
      restore();
      return;
    }
    if (curse > 0) {
      curse--;
      if (curse == 0) {
        player
            .getGameEncoder()
            .sendMessage(
                "<col=005500>You feel the effects of the Nightmare's curse wear off.</col>");
      }
    }
    if (parasite > 0) {
      parasite--;
      if (parasite == 0) {
        player
            .getGameEncoder()
            .sendMessage("<col=ff0000>The parasite bursts out of you, fully grown!</col>");
        if (parasitePotion) {
          theNightmare
              .getCombat()
              .addNpc(new NpcSpawn(player, NpcId.PARASITE_57))
              .getCombat()
              .setTarget(theNightmare);
          player.getCombat().addHit(new Hit(5));
        } else {
          var additionalDamage = PRandom.randomI(60);
          var parasiteNpc =
              theNightmare.getCombat().addNpc(new NpcSpawn(player, NpcId.PARASITE_86));
          parasiteNpc.getCombat().setTarget(theNightmare);
          parasiteNpc
              .getCombat()
              .setHitpoints(parasiteNpc.getCombat().getHitpoints() + additionalDamage);
          parasiteNpc.getCombat().setMaxHitpoints(parasiteNpc.getCombat().getHitpoints());
          player.getCombat().addHit(new Hit(10));
          player.getCombat().addHit(new Hit(additionalDamage));
        }
        parasitePotion = false;
      }
    }
    if (spore > 0) {
      spore--;
      if (spore == 0) {
        player
            .getGameEncoder()
            .sendMessage("<col=005500>The Nightmare's infection has worn off.</col>");
      }
    }
  }

  @Override
  public boolean widgetHook(int option, int widgetId, int childId, int slot, int itemId) {
    if (widgetId == WidgetId.PRAYER && curse > 0 && player.getArea().is("SisterhoodSanctuary")) {
      var child = PrayerChild.get(childId);
      switch (child) {
        case PROTECT_FROM_MAGIC:
          player.getPrayer().deactivate(child);
          player.getPrayer().activate(PrayerChild.PROTECT_FROM_MISSILES);
          return true;
        case PROTECT_FROM_MISSILES:
          player.getPrayer().deactivate(child);
          player.getPrayer().activate(PrayerChild.PROTECT_FROM_MELEE);
          return true;
        case PROTECT_FROM_MELEE:
          player.getPrayer().deactivate(child);
          player.getPrayer().activate(PrayerChild.PROTECT_FROM_MAGIC);
          return true;
        default:
          return false;
      }
    }
    if (widgetId == WidgetId.INVENTORY) {
      if (parasite > 0 && !parasitePotion && player.getArea().is("SisterhoodSanctuary")) {
        var itemName = ItemDefinition.getLowerCaseName(itemId);
        if (itemName.startsWith("sanfew serum") || itemName.startsWith("relicym's balm")) {
          parasitePotion = true;
          player
              .getGameEncoder()
              .sendMessage("<col=005500>The parasite within you has been weakened.</col>");
        }
      }
    }
    return false;
  }

  public void enterTheNightmare() {
    player.openDialogue(
        new MessageDialogue("The Nightmare pulls you into her dream as you approach her.", true));
    player.setAnimation(8584);
    player.getGameEncoder().sendFadeOut();
    player.lock();
    var tasks = new PEventTasks();
    tasks.execute(
        2, t -> player.getMovement().teleport(new Tile(3870 + PRandom.randomI(4), 9948, 3)));
    tasks.execute(
        2,
        t -> {
          player.setAnimation(8583);
          player.getGameEncoder().sendFadeIn();
        });
    tasks.execute(
        2,
        t -> {
          player.unlock();
          player.openDialogue(
              new MessageDialogue("The Nightmare pulls you into her dream as you approach her."));
        });
    player.getController().addEvent(tasks);
  }
}
