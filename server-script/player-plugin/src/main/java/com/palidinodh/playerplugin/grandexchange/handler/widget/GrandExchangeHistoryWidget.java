package com.palidinodh.playerplugin.grandexchange.handler.widget;

import com.palidinodh.cache.clientscript2.GrandExchangeHistoryItemCs2;
import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.WidgetSetting;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.grandexchange.GrandExchangePlugin;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeHistoryItem;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeHistoryType;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.Comparator;

@ReferenceId(WidgetId.GRAND_EXCHANGE_HISTORY_1027)
class GrandExchangeHistoryWidget implements WidgetHandler {

  @Override
  public void onOpen(Player player) {
    var plugin = player.getPlugin(GrandExchangePlugin.class);
    player.getWidgetManager().closeKeyboardScript();
    for (var i = 0; i < plugin.getSearchItems().size(); i++) {
      var item = plugin.getSearchItems().get(i);
      player
          .getGameEncoder()
          .sendClientScriptData(
              GrandExchangeHistoryItemCs2.builder()
                  .itemSlot(i)
                  .type(item.getType().ordinal())
                  .itemId(item.getId())
                  .itemQuantity(item.getQuantity())
                  .username(item.getUsername())
                  .itemPrice(item.getPrice())
                  .build());
    }
    player
        .getGameEncoder()
        .sendWidgetSettings(
            WidgetId.GRAND_EXCHANGE_HISTORY_1027,
            41,
            0,
            plugin.getSearchItems().size() * 10,
            WidgetSetting.OPTION_0);
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var plugin = player.getPlugin(GrandExchangePlugin.class);
    if (childId == 17) {
      player
          .getBank()
          .pinRequiredAction(
              () -> player.getWidgetManager().sendInteractiveOverlay(WidgetId.GRAND_EXCHANGE_1024));
      return;
    }
    var items = plugin.getSearchItems();
    if (childId == 20) {
      items.sort(
          (i1, i2) -> {
            if (i1.getType() != i2.getType()) {
              return (i1.getType() == GrandExchangeHistoryType.BUYING
                      || i1.getType() == GrandExchangeHistoryType.BOUGHT)
                  ? -1
                  : 1;
            }
            return 0;
          });
      onOpen(player);
      return;
    }
    if (childId == 23) {
      items.sort(Comparator.comparing(i -> ItemDefinition.getName(i.getId())));
      onOpen(player);
      return;
    }
    if (childId == 26) {
      items.sort(Comparator.comparingInt(GrandExchangeHistoryItem::getQuantity));
      onOpen(player);
      return;
    }
    if (childId == 29) {
      items.sort(Comparator.comparing(GrandExchangeHistoryItem::getUsername));
      onOpen(player);
      return;
    }
    if (childId == 32) {
      items.sort(Comparator.comparingInt(GrandExchangeHistoryItem::getPrice));
      onOpen(player);
      return;
    }
    if (childId == 41) {
      slot /= 7;
      var item = slot < items.size() ? items.get(slot) : null;
      if (item == null) {
        return;
      }
      var quantity = item.getQuantity();
      if (!ItemDefinition.getDefinition(item.getId()).isStackable()) {
        quantity = 1;
      }
      plugin
          .getOffers()
          .create(
              item.getType() == GrandExchangeHistoryType.BUYING,
              item.getId(),
              quantity,
              item.getPrice());
    }
  }
}
