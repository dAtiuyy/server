package com.palidinodh.cache.definition.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DefinitionOption {

  private int index;
  private String text;

  public boolean equals(String s) {
    return text.equalsIgnoreCase(s);
  }

  public boolean equals(int i) {
    return index == i;
  }
}
