package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.DORGESHUUN_CROSSBOW)
class DorgeshuunCrossbowSpecialAttack extends SpecialAttack {

  DorgeshuunCrossbowSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(75);
    entry.animation(4230);
    entry.projectileId(696);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    hooks.getOpponent().getCombat().changeDefenceLevel(-hooks.getDamage());
  }
}
