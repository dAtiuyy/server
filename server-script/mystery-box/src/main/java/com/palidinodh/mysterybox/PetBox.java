package com.palidinodh.mysterybox;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.List;

@ReferenceId(ItemId.PET_MYSTERY_BOX_32311)
class PetBox extends MysteryBox {

  private static List<RandomItem> weightless = RandomItem.minWeight(ItemTables.PETS);

  @Override
  public boolean shouldSendItemDropNews(Item item) {
    return true;
  }

  @Override
  public Item getRandomItem(Player player) {
    return RandomItem.getItem(ItemTables.PETS);
  }

  @Override
  public List<RandomItem> getAllItems(Player player) {
    return weightless;
  }
}
