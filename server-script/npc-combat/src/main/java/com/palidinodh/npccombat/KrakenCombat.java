package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropLocationType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.Arrays;
import java.util.List;

class KrakenCombat extends NpcCombat {

  private static final Tile[] TENTACLE_TILES = {
    new Tile(2275, 10034), new Tile(2284, 10034), new Tile(2275, 10038), new Tile(2284, 10038)
  };

  @Inject private Npc npc;
  private Npc[] tentacles;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .locationType(NpcCombatDropLocationType.UNDER_OPPONENT)
            .gemDropTableDenominator(64)
            .clue(ClueScrollType.ELITE, 500)
            .pet(ItemId.PET_KRAKEN, 3000);
    var dropTable =
        NpcCombatDropTable.builder().probabilityDenominator(1000).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JAR_OF_DIRT)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(512).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TRIDENT_OF_THE_SEAS_FULL)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(400).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.KRAKEN_TENTACLE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(300).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOME_OF_WATER_EMPTY)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(64);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGONSTONE_RING)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PIRATE_BOOTS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_WATER_STAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_WARHAMMER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_LONGSWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_ROBE_TOP)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_ROBE_BOTTOM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 60)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORSTOL_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNPOWERED_ORB_NOTED, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DIAMOND_NOTED, 8)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OAK_PLANK_NOTED, 60)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BAR, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_SHARK_NOTED, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_MONKFISH_NOTED, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_SNAPDRAGON_NOTED, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.HARPOON)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BUCKET)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SANFEW_SERUM_4, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CRYSTAL_KEY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUSTY_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOAKED_PAGE, 7, 43)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIDOTE_PLUS_PLUS_4_NOTED, 2)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATER_RUNE, 400)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MIST_RUNE, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 250)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATERMELON_SEED, 24)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SEAWEED_NOTED, 125)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BATTLESTAFF_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 10000, 19999)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHARK, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.EDIBLE_SEAWEED, 5)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.KRAKEN_291).id(NpcId.WHIRLPOOL);
    combat.spawn(NpcCombatSpawn.builder().respawnDelay(14).build());
    combat.hitpoints(NpcCombatHitpoints.total(255));
    combat.stats(
        NpcCombatStats.builder()
            .bonus(BonusType.DEFENCE_MAGIC, 130)
            .bonus(BonusType.DEFENCE_RANGED, 300)
            .build());
    combat.slayer(NpcCombatSlayer.builder().level(87).taskOnly(true).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.focus(
        NpcCombatFocus.builder().disableFollowingOpponent(true).bypassMapObjects(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.deathAnimation(3993).blockAnimation(3990);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(28).splashOnMiss(true).build());
    style.animation(3991).attackSpeed(4);
    style.targetGraphic(new Graphic(163, 124));
    style.projectile(NpcCombatProjectile.id(162));
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("kraken_combat_emerge")) {
      emerge((Player) args[0]);
    }
    return null;
  }

  @Override
  public void restoreHook() {
    npc.getWorld().removeNpcs(tentacles);
    tentacles = null;
  }

  @Override
  public void tickStartHook() {
    if (npc.isLocked()) {
      return;
    }
    if (tentacles == null) {
      loadTentacles();
    }
    if (!inRecentCombat() && !isAttacking() && npc.getId() != NpcId.WHIRLPOOL) {
      npc.setId(NpcId.WHIRLPOOL);
    } else if (inRecentCombat() || isAttacking()) {
      var attackingEntity = getAttackingEntity();
      emerge(attackingEntity instanceof Player ? attackingEntity.asPlayer() : null);
    }
    if (isDead()) {
      for (var tentacle : tentacles) {
        if (tentacle.isVisible() && !tentacle.getCombat().isDead()) {
          tentacle.getCombat().startDeath();
        }
      }
    }
  }

  @Override
  public boolean canBeAttackedHook(
      Entity opponent, boolean sendMessage, HitStyleType hitStyleType) {
    if (!opponent.isPlayer()) {
      return false;
    }
    var player = opponent.asPlayer();
    if (hitStyleType == HitStyleType.MELEE || hitStyleType == HitStyleType.RANGED) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("Only magic seems effective against these...");
      }
      return false;
    }
    for (var tentacle : tentacles) {
      if (tentacle.isVisible() && tentacle.getId() != NpcId.ENORMOUS_TENTACLE_112) {
        player.getGameEncoder().sendMessage("Nothing interesting happens.");
        return false;
      }
    }
    if (isAttacking() && getLastAttackedByEntity() != null && player != getLastAttackedByEntity()) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("The Kraken is busy attacking someone else.");
      }
      return false;
    }
    for (var tentacle : tentacles) {
      if (tentacle.getCombat().isAttacking()
          && tentacle.getCombat().getLastAttackedByEntity() != null
          && player != tentacle.getCombat().getLastAttackedByEntity()) {
        if (sendMessage) {
          player.getGameEncoder().sendMessage("The Kraken is busy attacking someone else.");
        }
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean canBeAggressiveHook(Entity opponent) {
    return !isAttacking() || !isHitDelayed();
  }

  public void loadTentacles() {
    tentacles = new Npc[4];
    for (var i = 0; i < tentacles.length; i++) {
      tentacles[i] =
          npc.getController().addNpc(new NpcSpawn(TENTACLE_TILES[i], NpcId.WHIRLPOOL_5534));
    }
    for (var tentacle : tentacles) {
      tentacle.getCombat().script("kraken", npc);
      tentacle.getCombat().script("tentacle0", tentacles[0]);
      tentacle.getCombat().script("tentacle1", tentacles[1]);
      tentacle.getCombat().script("tentacle2", tentacles[2]);
      tentacle.getCombat().script("tentacle3", tentacles[3]);
    }
  }

  private void emerge(Player player) {
    if (npc.getId() == NpcId.KRAKEN_291) {
      return;
    }
    if (tentacles == null) {
      return;
    }
    npc.setId(NpcId.KRAKEN_291);
    npc.setAnimation(7135);
    setHitDelay(4);
    setAttackingEntity(player);
    for (var tentacle : tentacles) {
      tentacle.getCombat().as(KrakenTentacleCombat.class).emerge(player);
    }
  }
}
