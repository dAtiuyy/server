package com.palidinodh.rs.communication.response;

import com.palidinodh.rs.communication.request.Request;

public class SQLUpdateResponse extends Response {

  private String text;

  public SQLUpdateResponse(Request request, String text) {
    super(request);
    this.text = text;
  }

  public String getText() {
    return text;
  }
}
