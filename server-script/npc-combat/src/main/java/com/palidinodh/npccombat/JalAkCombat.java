package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.tzhaar.TzhaarPlugin;
import java.util.Arrays;
import java.util.List;

class JalAkCombat extends NpcCombat {

  @Inject private Npc npc;
  private HitStyleType attackStyle;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.JAL_AK_165);
    combat.hitpoints(NpcCombatHitpoints.total(40));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(160)
            .magicLevel(160)
            .rangedLevel(160)
            .defenceLevel(95)
            .bonus(BonusType.ATTACK_STAB, 45)
            .bonus(BonusType.ATTACK_SLASH, 45)
            .bonus(BonusType.ATTACK_CRUSH, 45)
            .bonus(BonusType.ATTACK_MAGIC, 45)
            .bonus(BonusType.ATTACK_RANGED, 45)
            .bonus(BonusType.MELEE_DEFENCE, 25)
            .bonus(BonusType.DEFENCE_MAGIC, 25)
            .bonus(BonusType.DEFENCE_RANGED, 25)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(8).always(true).forceable(false).build());
    combat.immunity(NpcCombatImmunity.builder().venom(true).build());
    combat.deathAnimation(7584).blockAnimation(7585);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(29));
    style.animation(7587).attackSpeed(6);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(29));
    style.animation(7587).attackSpeed(6);
    style.projectile(NpcCombatProjectile.id(1378));
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(29));
    style.animation(7587).attackSpeed(6);
    style.projectile(NpcCombatProjectile.id(1380));
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    attackStyle = HitStyleType.NONE;
  }

  @Override
  public void tickStartHook() {
    if (getHitDelay() == 3) {
      var withinOneTile = npc.withinDistance(getAttackingEntity(), 1);
      attackStyle =
          withinOneTile
              ? HitStyleType.getRandomType(
                  HitStyleType.MELEE, HitStyleType.RANGED, HitStyleType.MAGIC)
              : HitStyleType.getRandomType(HitStyleType.RANGED, HitStyleType.MAGIC);
      if (!withinOneTile && getAttackingEntity() instanceof Player) {
        var player = (Player) getAttackingEntity();
        if (player.getPrayer().hasActive("protect from missiles")) {
          attackStyle = HitStyleType.MAGIC;
        } else if (player.getPrayer().hasActive("protect from magic")) {
          attackStyle = HitStyleType.RANGED;
        }
      }
    }
  }

  @Override
  public HitStyleType attackTickHitStyleTypeHook(HitStyleType hitStyleType, Entity opponent) {
    return attackStyle == HitStyleType.NONE ? hitStyleType : attackStyle;
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    var mej = npc.getController().addNpc(new NpcSpawn(npc, NpcId.JAL_AKREK_KET_70));
    mej.getMovement().setClipNpcs(true);
    mej.getCombat().setTarget(player);
    player.getPlugin(TzhaarPlugin.class).addMinigameNpc(mej);
    var xil =
        npc.getController()
            .addNpc(
                new NpcSpawn(
                    new Tile(npc.getX() + 1, npc.getY(), npc.getHeight()), NpcId.JAL_AKREK_XIL_70));
    xil.getMovement().setClipNpcs(true);
    xil.getCombat().setTarget(player);
    player.getPlugin(TzhaarPlugin.class).addMinigameNpc(xil);
    var ket =
        npc.getController()
            .addNpc(
                new NpcSpawn(
                    new Tile(npc.getX() + 2, npc.getY() + 1, npc.getHeight()),
                    NpcId.JAL_AKREK_MEJ_70));
    ket.getMovement().setClipNpcs(true);
    ket.getCombat().setTarget(player);
    player.getPlugin(TzhaarPlugin.class).addMinigameNpc(ket);
  }
}
