package com.palidinodh.maparea.kandarin.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class FishingPlatformNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2790, 3273), NpcId.FISHING_SPOT_1514));
    spawns.add(new NpcSpawn(new Tile(2790, 3276), NpcId.FISHING_SPOT_1514));
    spawns.add(new NpcSpawn(new Tile(2791, 3279), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(2791, 3280), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(2794, 3283), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(new Tile(2795, 3279), NpcId.ROD_FISHING_SPOT));
    spawns.add(new NpcSpawn(4, new Tile(2783, 3276), NpcId.FISHERMAN_5075));
    spawns.add(new NpcSpawn(4, new Tile(2769, 3276), NpcId.FISHERMAN_5076));
    spawns.add(new NpcSpawn(4, new Tile(2769, 3287), NpcId.FISHERMAN_5077));
    spawns.add(new NpcSpawn(4, new Tile(2782, 3287), NpcId.FISHERMAN_5078));

    return spawns;
  }
}
