package com.palidinodh.maparea.fremennikprovince;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(8763)
public class PiratesCoveArea extends Area {}
