package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;
import com.palidinodh.util.PCollection;
import java.util.Map;

class RockGolemPet implements Pet.BuildType {

  public static final Map<Integer, Integer> RECOLORS =
      PCollection.toMap(
          ItemId.RUNITE_ORE,
          NpcId.ROCK_GOLEM_7450,
          ItemId.DAEYALT_ORE,
          NpcId.ROCK_GOLEM_7738,
          ItemId.ROCK_1480,
          NpcId.ROCK_GOLEM_7439,
          ItemId.COPPER_ORE,
          NpcId.ROCK_GOLEM_7441,
          ItemId.TIN_ORE,
          NpcId.ROCK_GOLEM_7440,
          ItemId.IRON_ORE,
          NpcId.ROCK_GOLEM_7442,
          ItemId.SILVER_ORE,
          NpcId.ROCK_GOLEM_7444,
          ItemId.GOLD_ORE,
          NpcId.ROCK_GOLEM_7446,
          ItemId.MITHRIL_ORE,
          NpcId.ROCK_GOLEM_7447,
          ItemId.ADAMANTITE_ORE,
          NpcId.ROCK_GOLEM_7449,
          ItemId.BLURITE_ORE,
          NpcId.ROCK_GOLEM_7443,
          ItemId.COAL,
          NpcId.ROCK_GOLEM_7445,
          ItemId.GRANITE_500G,
          NpcId.ROCK_GOLEM_7448,
          ItemId.GRANITE_2KG,
          NpcId.ROCK_GOLEM_7448,
          ItemId.GRANITE_5KG,
          NpcId.ROCK_GOLEM_7448,
          ItemId.AMETHYST,
          NpcId.ROCK_GOLEM,
          ItemId.LOVAKITE_ORE,
          NpcId.ROCK_GOLEM_7736,
          ItemId.ELEMENTAL_ORE,
          NpcId.ROCK_GOLEM_7737);

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.ROCK_GOLEM, NpcId.ROCK_GOLEM_7439, NpcId.ROCK_GOLEM_7451));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21187, NpcId.ROCK_GOLEM_7440, NpcId.ROCK_GOLEM_7452));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21188, NpcId.ROCK_GOLEM_7441, NpcId.ROCK_GOLEM_7453));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21189, NpcId.ROCK_GOLEM_7442, NpcId.ROCK_GOLEM_7454));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21190, NpcId.ROCK_GOLEM_7443, NpcId.ROCK_GOLEM_7455));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21191, NpcId.ROCK_GOLEM_7444, NpcId.ROCK_GOLEM_7642));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21192, NpcId.ROCK_GOLEM_7445, NpcId.ROCK_GOLEM_7643));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21193, NpcId.ROCK_GOLEM_7446, NpcId.ROCK_GOLEM_7644));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21194, NpcId.ROCK_GOLEM_7447, NpcId.ROCK_GOLEM_7645));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21195, NpcId.ROCK_GOLEM_7448, NpcId.ROCK_GOLEM_7646));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21196, NpcId.ROCK_GOLEM_7449, NpcId.ROCK_GOLEM_7647));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21197, NpcId.ROCK_GOLEM_7450, NpcId.ROCK_GOLEM_7648));
    builder.entry(new Pet.Entry(ItemId.ROCK_GOLEM_21340, NpcId.ROCK_GOLEM, NpcId.ROCK_GOLEM_7711));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21358, NpcId.ROCK_GOLEM_7736, NpcId.ROCK_GOLEM_7739));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21359, NpcId.ROCK_GOLEM_7737, NpcId.ROCK_GOLEM_7740));
    builder.entry(
        new Pet.Entry(ItemId.ROCK_GOLEM_21360, NpcId.ROCK_GOLEM_7738, NpcId.ROCK_GOLEM_7741));
    builder.itemVariation(
        (p, n, i) -> {
          var recolor = RECOLORS.get(i.getId());
          if (recolor == null) {
            return;
          }
          p.getPlugin(FamiliarPlugin.class).transformPet(recolor);
          i.remove();
        });
    return builder;
  }
}
