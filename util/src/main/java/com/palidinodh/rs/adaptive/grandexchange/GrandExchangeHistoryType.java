package com.palidinodh.rs.adaptive.grandexchange;

public enum GrandExchangeHistoryType {
  BOUGHT,
  SOLD,
  BUYING,
  SELLING
}
