package com.palidinodh.maparea.kandarin.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class WitchavenNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2721, 3304), NpcId.HOLGART_5069));
    spawns.add(new NpcSpawn(4, new Tile(2691, 3272), NpcId.CHICKEN_1));
    spawns.add(new NpcSpawn(4, new Tile(2692, 3274), NpcId.CHICKEN_1));
    spawns.add(new NpcSpawn(4, new Tile(2691, 3277), NpcId.CHICKEN_1));
    spawns.add(new NpcSpawn(4, new Tile(2693, 3276), NpcId.CHICKEN_1));
    spawns.add(new NpcSpawn(4, new Tile(2715, 3282), NpcId.WITCHAVEN_VILLAGER));
    spawns.add(new NpcSpawn(4, new Tile(2708, 3291), NpcId.MAYOR_HOBB));
    spawns.add(new NpcSpawn(4, new Tile(2727, 3276), NpcId.WITCHAVEN_VILLAGER_4794));
    spawns.add(new NpcSpawn(4, new Tile(2732, 3284), NpcId.WITCHAVEN_VILLAGER_4792));
    spawns.add(new NpcSpawn(4, new Tile(2718, 3291), NpcId.WITCHAVEN_VILLAGER));
    spawns.add(new NpcSpawn(4, new Tile(2723, 3291), NpcId.WITCHAVEN_VILLAGER_4792));
    spawns.add(new NpcSpawn(4, new Tile(2721, 3283), NpcId.BROTHER_MALEDICT));
    spawns.add(new NpcSpawn(4, new Tile(2734, 3293), NpcId.EZEKIAL_LOVECRAFT));
    spawns.add(new NpcSpawn(4, new Tile(2740, 3302), NpcId.WITCHAVEN_VILLAGER_4794));
    spawns.add(new NpcSpawn(new Tile(2740, 3310), NpcId.COL_ONIALL));
    spawns.add(new NpcSpawn(4, new Tile(2716, 3302), NpcId.CAROLINE));
    spawns.add(new NpcSpawn(4, new Tile(2717, 3306), NpcId.JEB));
    spawns.add(new NpcSpawn(4, new Tile(2711, 3325), NpcId.GRIZZLY_BEAR_21));

    return spawns;
  }
}
