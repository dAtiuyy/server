package com.palidinodh.maparea.trollcountry;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({11322, 11323})
public class IcePathArea extends Area {}
