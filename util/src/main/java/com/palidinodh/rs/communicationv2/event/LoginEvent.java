package com.palidinodh.rs.communicationv2.event;

import com.palidinodh.io.FileManager;
import com.palidinodh.io.Geolocation;
import com.palidinodh.io.Readers;
import com.palidinodh.nio.Session;
import com.palidinodh.nio.WriteEventHandler;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communicationv2.CommunicationEvent;
import com.palidinodh.rs.communicationv2.MainCommunicationServer;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.SqlUserField;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PCollection;
import com.palidinodh.util.PLogger;
import com.palidinodh.util.PString;
import java.io.File;
import java.sql.Connection;
import java.util.EnumMap;
import java.util.Map;
import lombok.Getter;

@Getter
public class LoginEvent extends CommunicationEvent {

  private static int uniqueId = 1;

  private String username;
  private String password;
  private String ip;
  private long randomHash;
  private String mac;
  private String uuid;
  private int worldId;
  private LoginState loginState;
  private int userId;
  private byte[] playerFile;
  private Map<SqlUserField, String> sqlQuery;

  public LoginEvent(
      String username, String password, String ip, String mac, String uuid, int worldId) {
    this.username = username;
    this.password = password;
    this.ip = ip;
    this.mac = mac;
    this.uuid = uuid;
    this.worldId = worldId;
  }

  private static synchronized int getUniqueId() {
    return uniqueId++;
  }

  @Override
  public void mainAction() {
    Connection sql = null;
    if (!Settings.getInstance().isLocal()) {
      sql = FileManager.getSqlConnection();
    }
    if (!Settings.getInstance().isLocal() && sql == null) {
      loginState = LoginState.ERROR;
      return;
    }
    var forum = Settings.getSecure().getForum();
    sqlQuery = forum != null ? forum.executeLoginQuery(sql, username) : null;
    if (sqlQuery == null) {
      sqlQuery = new EnumMap<>(SqlUserField.class);
    }
    if (sqlQuery.isEmpty()) {
      if (Settings.getInstance().isLocal()) {
        sqlQuery.put(SqlUserField.ID, Integer.toString(getUniqueId()));
        sqlQuery.put(SqlUserField.NAME, username);
        sqlQuery.put(SqlUserField.MAIN_GROUP, UserRank.ADMINISTRATOR.name());
        sqlQuery.put(SqlUserField.IP_ADDRESS, "127.0.0.1");
      } else {
        loginState = LoginState.NO_PROFILE;
        return;
      }
    }
    userId = Integer.parseInt(sqlQuery.get(SqlUserField.ID));
    if (username.contains("@")) {
      username = sqlQuery.get(SqlUserField.NAME);
    }
    username = username.replaceAll("[^\\dA-Za-z_\\- ]", "");
    if (username.matches("[\\da-z_\\- ]+")) {
      username = PString.formatName(username);
    }
    username = username.toLowerCase();
    var mainGroup = UserRank.valueOf(sqlQuery.get(SqlUserField.MAIN_GROUP));
    if (mainGroup == UserRank.BANNED) {
      loginState = LoginState.BANNED;
      return;
    }
    if (getMainServer().getPlayersById().get(userId) != null
        || getMainServer().getPlayersByUsername().get(username) != null) {
      loginState = LoginState.LOGGED_IN;
      return;
    }
    if (!Settings.getInstance().isLocal() && !Settings.getSecure().getOwnerIps().contains(ip)) {
      if (!forum.verifyPassword(
          password,
          sqlQuery.get(SqlUserField.PASSWORD),
          sqlQuery.get(SqlUserField.PASSWORD_SALT))) {
        loginState = LoginState.INVALID_PASSWORD;
        return;
      }
    }
    var loginSecurity = false;
    String lastIp = null;
    long lastRandomHash = 0;
    String lastMac = null;
    String lastUuid = null;
    playerFile = readPlayerFile();
    try {
      if (playerFile != null) {
        var fileMap =
            PCollection.castMap((Map) Readers.deserialize(playerFile), String.class, Object.class);
        if (fileMap != null) {
          loginSecurity =
              (Boolean) fileMap.getOrDefault(RsPlayer.SAVE_MAP_LOGIN_SECURITY, Boolean.FALSE);
          lastIp = (String) fileMap.getOrDefault(RsPlayer.SAVE_MAP_IP, "");
          lastRandomHash = (long) fileMap.getOrDefault(RsPlayer.SAVE_MAP_RANDOM_HASH, 0L);
          lastMac = (String) fileMap.getOrDefault(RsPlayer.SAVE_MAP_MAC, "");
          lastUuid = (String) fileMap.getOrDefault(RsPlayer.SAVE_MAP_UUID, "");
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    }
    if (playerFile != null && loginSecurity) {
      var ipMatch = Geolocation.isSimiliar(ip, lastIp, sqlQuery.get(SqlUserField.IP_ADDRESS));
      var randomHashMatch = randomHash == lastRandomHash;
      var macMatch = !mac.isEmpty() && mac.equals(lastMac);
      var uuidMatch = !uuid.isEmpty() && uuid.equals(lastUuid);
      if (!ipMatch && !randomHashMatch && !macMatch && !uuidMatch) {
        loginState = LoginState.HACKED_RISK;
        return;
      }
    }
    var player = new RsPlayer(userId, username, password, ip, worldId);
    player.setRights(RsPlayer.getRights(mainGroup));
    loginState = LoginState.SUCCESS;
    synchronized (MainCommunicationServer.getInstance()) {
      getMainServer().getPlayersById().put(player.getId(), player);
      getMainServer().getPlayersByUsername().put(username, player);
    }
    setWriteHandler(new WriteHandler());
  }

  public byte[] getPlayerFile() {
    var data = playerFile;
    playerFile = null;
    return data;
  }

  public Map<SqlUserField, String> getSqlQuery() {
    Map<SqlUserField, String> map = sqlQuery;
    sqlQuery = null;
    return map == null ? new EnumMap<>(SqlUserField.class) : map;
  }

  private byte[] readPlayerFile() {
    var playerDirectory = Settings.getInstance().getPlayerMapDirectory();
    var fileName = Integer.toString(userId);
    if (Settings.getInstance().isLocal()) {
      fileName = username;
    }
    return Readers.readFile(new File(playerDirectory, fileName + ".dat"));
  }

  public enum LoginState {
    SUCCESS,
    NO_PROFILE,
    INVALID_PASSWORD,
    LOGGED_IN,
    HACKED_RISK,
    BANNED,
    ERROR
  }

  private class WriteHandler extends WriteEventHandler {

    @Override
    public void complete(Session session, boolean success) {
      if (success) {
        return;
      }
      synchronized (MainCommunicationServer.getInstance()) {
        getMainServer().getPlayersById().remove(userId);
        getMainServer().getPlayersByUsername().remove(username);
      }
    }
  }
}
