package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class CorporealBeastCombat extends NpcCombat {

  @Inject private Npc npc;
  private Npc darkCore;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .gemDropTableDenominator(42)
            .clue(ClueScrollType.ELITE, 200)
            .pet(ItemId.PET_DARK_CORE, 5000)
            .additionalPlayers(3);
    var dropTable =
        NpcCombatDropTable.builder().probabilityDenominator(585).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ELYSIAN_SIGIL).weight(1)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SPECTRAL_SIGIL).weight(3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ARCANE_SIGIL).weight(3)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(170).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.HOLY_ELIXIR)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(64);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SPIRIT_SHIELD)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(25);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ONYX_BOLTS_E, 175)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CANNONBALL, 2000)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_SAPPHIRE_NOTED, 1, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_EMERALD_NOTED, 1, 8)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_ARROW, 750)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BOLTS, 250)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_AIR_STAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_WATER_STAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_EARTH_STAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_FIRE_STAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_ROBE_TOP)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_ROBE_BOTTOM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 2500)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 250)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COSMIC_RUNE, 500)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 300)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 250)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_ORE_NOTED, 125)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_BAR_NOTED, 35)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_LOGS_NOTED, 75)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GREEN_DRAGONHIDE_NOTED, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_SHARK_NOTED, 70)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.DESERT_GOAT_HORN_NOTED, 120)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATERMELON_SEED, 24)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TUNA_POTATO_NOTED, 30)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_RUBY_NOTED, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DIAMOND_NOTED, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 20000, 50000)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.NATURE_TALISMAN_NOTED, 1, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_ORE_NOTED, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TEAK_PLANK_NOTED, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WHITE_BERRIES_NOTED, 120)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIDOTE_PLUS_PLUS_4_NOTED, 40)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RANARR_SEED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHIELD_LEFT_HALF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_SPEAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL_NOTED, 1, 5)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.CORPOREAL_BEAST_785);
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(2000).barType(HitpointsBarType.GREEN_RED_160).build());
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(320)
            .magicLevel(350)
            .rangedLevel(150)
            .defenceLevel(310)
            .bonus(BonusType.MELEE_ATTACK, 50)
            .bonus(BonusType.DEFENCE_STAB, 25)
            .bonus(BonusType.DEFENCE_SLASH, 200)
            .bonus(BonusType.DEFENCE_CRUSH, 100)
            .bonus(BonusType.DEFENCE_MAGIC, 150)
            .bonus(BonusType.DEFENCE_RANGED, 230)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(16).checkWhileAttacking(true).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.deathAnimation(1676).blockAnimation(1677);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(51));
    style.animation(1682).attackSpeed(6);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(51));
    style.animation(1683).attackSpeed(6);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(65).prayerEffectiveness(0.66).build());
    style.animation(1679).attackSpeed(6);
    style.projectile(NpcCombatProjectile.id(316));
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(51).prayerEffectiveness(0.66).build());
    style.animation(1679).attackSpeed(6);
    style.projectile(NpcCombatProjectile.id(314));
    style.effect(
        NpcCombatEffect.builder().statDrain(Skills.PRAYER, 1).statDrain(Skills.MAGIC, 1).build());
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(27).ignorePrayer(true).build());
    style.animation(1679).attackSpeed(6);
    // style.targetTileGraphic(new Graphic(317));
    style.projectile(NpcCombatProjectile.builder().id(315).speedMinimumDistance(10).build());
    var targetTile = NpcCombatTargetTile.builder();
    targetTile.breakOff(
        NpcCombatTargetTile.BreakOff.builder()
            .count(4)
            .distance(3)
            .damage(NpcCombatDamage.builder().maximum(14).ignorePrayer(true).build())
            .afterTargettedTile(true)
            .build());
    style.specialAttack(targetTile.build());
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.UNDERNEATH).build());
    style.damage(NpcCombatDamage.maximum(50));
    style.animation(1686).attackSpeed(8);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void npcApplyHitStartHook(Hit hit) {
    if (hit.getDamage() > 100) {
      hit.setDamage(100);
    }
  }

  @Override
  public void despawnHook() {
    reset();
  }

  @Override
  public void restoreHook() {
    reset();
  }

  @Override
  public void tickStartHook() {
    if (!npc.isVisible() || isDead()) {
      return;
    }
    if (getHitpoints() < getMaxHitpoints() && npc.getController().getPlayers().isEmpty()) {
      setHitpoints(getMaxHitpoints());
    }
    if (darkCore != null && darkCore.getCombat().isDead()) {
      npc.getWorld().removeNpc(darkCore);
      darkCore = null;
    }
    if (!npc.isLocked() && npc.getX() > 2998) { // Why does this happen?
      npc.getMovement().teleport(npc.getSpawn().getTile());
    }
  }

  @Override
  public void applyDeadHook() {
    if (darkCore != null) {
      npc.getWorld().removeNpc(darkCore);
      darkCore = null;
    }
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (opponent.isPlayer()) {
      var player = opponent.asPlayer();
      if (!player.getEquipment().getWeaponDef().getName().contains("spear")
              && !player.getEquipment().getWeaponDef().getName().contains("halberd")
          || player.getMagic().getActiveSpell() != null) {
        damage *= 0.5;
      }
    }
    if (damage > 32) {
      if (getPlayerAggressionDelay() == 0) {
        npc.setFaceEntity(opponent);
        setAttackingEntity(opponent);
        setFollowing(opponent);
        setPlayerAggressionDelay(2);
      }
    }
    spawnDarkCore((int) damage);
    return damage;
  }

  private void reset() {
    npc.getWorld().removeNpc(darkCore);
    darkCore = null;
    for (var player : npc.getController().getPlayers()) {
      if (player.getHeight() == npc.getHeight()) {
        player.getCombat().setDamageInflicted(0);
      }
    }
  }

  private void spawnDarkCore(int damage) {
    if (darkCore != null) {
      return;
    }
    if (damage < 32) {
      return;
    }
    if (getHitpoints() >= getMaxHitpoints() / 2) {
      return;
    }
    if (!PRandom.inRange(1, 8)) {
      return;
    }
    if (npc.getController().routeAllow(npc.getX() - 1, npc.getY())) {
      darkCore =
          npc.getController()
              .addNpc(
                  new NpcSpawn(
                      new Tile(npc.getX() - 1, npc.getY(), npc.getHeight()),
                      NpcId.DARK_ENERGY_CORE_75));
    } else {
      darkCore =
          npc.getController()
              .addNpc(
                  new NpcSpawn(
                      new Tile(npc.getX() + npc.getSizeX(), npc.getY(), npc.getHeight()),
                      NpcId.DARK_ENERGY_CORE_75));
    }
  }
}
