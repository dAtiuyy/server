package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.NullNpcId;
import com.palidinodh.cache.id.NullObjectId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Bounds;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.tirannwn.TheGauntletSubPlugin.Reward;
import com.palidinodh.playerplugin.tirannwn.TirannwnPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEventTasks;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

class HunllefCombat extends NpcCombat {

  private static final List<Integer> CRYSTALLINE_IDS =
      Arrays.asList(
          NpcId.CRYSTALLINE_HUNLLEF_674,
          NpcId.CRYSTALLINE_HUNLLEF_674_9022,
          NpcId.CRYSTALLINE_HUNLLEF_674_9023);
  private static final List<Integer> CORRUPTED_IDS =
      Arrays.asList(
          NpcId.CORRUPTED_HUNLLEF_894,
          NpcId.CORRUPTED_HUNLLEF_894_9036,
          NpcId.CORRUPTED_HUNLLEF_894_9037);
  private static final int MELEE = 0;
  private static final int RANGED = 1;
  private static final int MAGIC = 2;
  private static final Bounds CRYSTALLINE_BOUNDS = new Bounds(1361, 9380, 1372, 9391, 1);
  private static final Bounds CORRUPTED_BOUNDS = new Bounds(1377, 9380, 1388, 9391, 1);
  private static final Tile CRYSTALLINE_BASE_TILE = new Tile(1361, 9380, 1);
  private static final Tile CORRUPT_BASE_TILE = new Tile(1377, 9380, 1);
  private static final List<NpcCombatDropTable> GAUNTLET_DROP_TABLES;
  private static final List<NpcCombatDropTable> CORRUPTED_GAUNTLET_DROP_TABLES;

  @Inject private Npc npc;
  private List<Integer> ids;
  private HitStyleType hitStyle = HitStyleType.RANGED;
  private int hitCount;
  private int correctHits;
  private int tornadoDelay = PRandom.randomI(4, 10);
  private PEventTasks tileTask;

  static {
    var youngllefTable =
        NpcCombatDropTable.builder()
            .probabilityDenominator(2000)
            .log(true)
            .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YOUNGLLEF)))
            .build();
    var enhancedCrystalWeaponSeedTable =
        NpcCombatDropTable.builder()
            .probabilityDenominator(2000)
            .log(true)
            .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ENHANCED_CRYSTAL_WEAPON_SEED)))
            .build();
    var cystalWeaponSeedTable =
        NpcCombatDropTable.builder()
            .probabilityDenominator(120)
            .log(true)
            .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CRYSTAL_WEAPON_SEED)))
            .build();
    var cystalArmourSeedTable =
        NpcCombatDropTable.builder()
            .probabilityDenominator(120)
            .log(true)
            .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CRYSTAL_ARMOUR_SEED)))
            .build();
    GAUNTLET_DROP_TABLES =
        Arrays.asList(
            youngllefTable,
            enhancedCrystalWeaponSeedTable,
            cystalWeaponSeedTable,
            cystalArmourSeedTable);

    youngllefTable =
        NpcCombatDropTable.builder()
            .probabilityDenominator(800)
            .log(true)
            .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YOUNGLLEF)))
            .build();
    enhancedCrystalWeaponSeedTable =
        NpcCombatDropTable.builder()
            .probabilityDenominator(400)
            .log(true)
            .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ENHANCED_CRYSTAL_WEAPON_SEED)))
            .build();
    cystalWeaponSeedTable =
        NpcCombatDropTable.builder()
            .probabilityDenominator(50)
            .log(true)
            .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CRYSTAL_WEAPON_SEED)))
            .build();
    cystalArmourSeedTable =
        NpcCombatDropTable.builder()
            .probabilityDenominator(50)
            .log(true)
            .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CRYSTAL_ARMOUR_SEED)))
            .build();
    CORRUPTED_GAUNTLET_DROP_TABLES =
        Arrays.asList(
            youngllefTable,
            enhancedCrystalWeaponSeedTable,
            cystalWeaponSeedTable,
            cystalArmourSeedTable);
  }

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var aggression = NpcCombatAggression.PLAYERS;

    var crystallineHitpoints =
        NpcCombatHitpoints.builder().total(600).barType(HitpointsBarType.GREEN_RED_120).build();
    var crystallineStats =
        NpcCombatStats.builder()
            .attackLevel(240)
            .magicLevel(240)
            .rangedLevel(240)
            .defenceLevel(240)
            .bonus(BonusType.MELEE_ATTACK, 76)
            .bonus(BonusType.ATTACK_MAGIC, 76)
            .bonus(BonusType.ATTACK_RANGED, 76)
            .bonus(BonusType.MELEE_DEFENCE, 20)
            .bonus(BonusType.DEFENCE_MAGIC, 20)
            .bonus(BonusType.DEFENCE_RANGED, 20)
            .build();
    var crystallineKillCount =
        NpcCombatKillCount.builder().asName("Gauntlet").sendMessage(true).build();

    var crystallineRangedStyle = NpcCombatStyle.builder();
    crystallineRangedStyle.type(NpcCombatStyleType.RANGED);
    crystallineRangedStyle.damage(
        NpcCombatDamage.builder().maximum(12).prayerEffectiveness(1).build());
    crystallineRangedStyle.animation(8419).attackSpeed(5).attackRange(6);
    crystallineRangedStyle.projectile(NpcCombatProjectile.builder().id(1711).delay(11).build());

    var crystallineMagicStyle = NpcCombatStyle.builder();
    crystallineMagicStyle.type(
        NpcCombatStyleType.builder().hitStyleType(HitStyleType.MAGIC).weight(4).build());
    crystallineMagicStyle.damage(
        NpcCombatDamage.builder().maximum(12).prayerEffectiveness(1).build());
    crystallineMagicStyle.animation(8419).attackSpeed(5).attackRange(6);
    crystallineMagicStyle.targetGraphic(new Graphic(1709, 124));
    crystallineMagicStyle.projectile(NpcCombatProjectile.builder().id(1707).delay(11).build());

    var crystallineMagicPrayerStyle = NpcCombatStyle.builder();
    crystallineMagicPrayerStyle.type(NpcCombatStyleType.MAGIC);
    crystallineMagicPrayerStyle.damage(
        NpcCombatDamage.builder().maximum(12).prayerEffectiveness(1).build());
    crystallineMagicPrayerStyle.effect(
        NpcCombatEffect.builder().includeMiss(true).deactivatePrayers(true).build());
    crystallineMagicPrayerStyle.animation(8419).attackSpeed(5).attackRange(6);
    crystallineMagicPrayerStyle.targetGraphic(new Graphic(1703, 124));
    crystallineMagicPrayerStyle.projectile(
        NpcCombatProjectile.builder().id(1701).delay(11).build());

    var crystallineUnderneathStyle = NpcCombatStyle.builder();
    crystallineUnderneathStyle.type(NpcCombatStyleType.UNDERNEATH);
    crystallineUnderneathStyle.damage(NpcCombatDamage.builder().minimum(8).maximum(45).build());
    crystallineUnderneathStyle.animation(8420).attackSpeed(5);

    var crystallineMeleeProtectionCombat = NpcCombatDefinition.builder();
    crystallineMeleeProtectionCombat.id(CRYSTALLINE_IDS.get(MELEE));
    crystallineMeleeProtectionCombat.hitpoints(crystallineHitpoints);
    crystallineMeleeProtectionCombat.stats(crystallineStats);
    crystallineMeleeProtectionCombat.aggression(aggression);
    crystallineMeleeProtectionCombat.immunity(
        NpcCombatImmunity.builder().poison(true).venom(true).melee(true).build());
    crystallineMeleeProtectionCombat.killCount(crystallineKillCount);
    crystallineMeleeProtectionCombat.deathAnimation(8421);
    crystallineMeleeProtectionCombat.style(crystallineRangedStyle.build());
    crystallineMeleeProtectionCombat.style(crystallineMagicStyle.build());
    crystallineMeleeProtectionCombat.style(crystallineMagicPrayerStyle.build());
    crystallineMeleeProtectionCombat.style(crystallineUnderneathStyle.build());

    var crystallineRangedProtectionCombat = NpcCombatDefinition.builder();
    crystallineRangedProtectionCombat.id(CRYSTALLINE_IDS.get(RANGED));
    crystallineRangedProtectionCombat.hitpoints(crystallineHitpoints);
    crystallineRangedProtectionCombat.stats(crystallineStats);
    crystallineRangedProtectionCombat.aggression(aggression);
    crystallineRangedProtectionCombat.immunity(
        NpcCombatImmunity.builder().poison(true).venom(true).ranged(true).build());
    crystallineRangedProtectionCombat.killCount(crystallineKillCount);
    crystallineRangedProtectionCombat.deathAnimation(8421);
    crystallineRangedProtectionCombat.style(crystallineRangedStyle.build());
    crystallineRangedProtectionCombat.style(crystallineMagicStyle.build());
    crystallineRangedProtectionCombat.style(crystallineMagicPrayerStyle.build());
    crystallineRangedProtectionCombat.style(crystallineUnderneathStyle.build());

    var crystallineMagicProtectionCombat = NpcCombatDefinition.builder();
    crystallineMagicProtectionCombat.id(CRYSTALLINE_IDS.get(MAGIC));
    crystallineMagicProtectionCombat.hitpoints(crystallineHitpoints);
    crystallineMagicProtectionCombat.stats(crystallineStats);
    crystallineMagicProtectionCombat.aggression(aggression);
    crystallineMagicProtectionCombat.immunity(
        NpcCombatImmunity.builder().poison(true).venom(true).magic(true).build());
    crystallineMagicProtectionCombat.killCount(crystallineKillCount);
    crystallineMagicProtectionCombat.deathAnimation(8421);
    crystallineMagicProtectionCombat.style(crystallineRangedStyle.build());
    crystallineMagicProtectionCombat.style(crystallineMagicStyle.build());
    crystallineMagicProtectionCombat.style(crystallineMagicPrayerStyle.build());
    crystallineMagicProtectionCombat.style(crystallineUnderneathStyle.build());

    var corruptedHitpoints =
        NpcCombatHitpoints.builder().total(1000).barType(HitpointsBarType.GREEN_RED_120).build();
    var corruptedStats =
        NpcCombatStats.builder()
            .attackLevel(240)
            .magicLevel(240)
            .rangedLevel(240)
            .defenceLevel(240)
            .bonus(BonusType.MELEE_ATTACK, 90)
            .bonus(BonusType.ATTACK_MAGIC, 90)
            .bonus(BonusType.ATTACK_RANGED, 90)
            .bonus(BonusType.MELEE_DEFENCE, 20)
            .bonus(BonusType.DEFENCE_MAGIC, 20)
            .bonus(BonusType.DEFENCE_RANGED, 20)
            .build();
    var corruptedKillCount =
        NpcCombatKillCount.builder().asName("Corrupted Gauntlet").sendMessage(true).build();

    var corruptedRangedStyle = NpcCombatStyle.builder();
    corruptedRangedStyle.type(NpcCombatStyleType.RANGED);
    corruptedRangedStyle.damage(
        NpcCombatDamage.builder().maximum(16).prayerEffectiveness(1).build());
    corruptedRangedStyle.animation(8419).attackSpeed(5).attackRange(6);
    corruptedRangedStyle.projectile(NpcCombatProjectile.builder().id(1712).delay(11).build());

    var corruptedMagicStyle = NpcCombatStyle.builder();
    corruptedMagicStyle.type(
        NpcCombatStyleType.builder().hitStyleType(HitStyleType.MAGIC).weight(4).build());
    corruptedMagicStyle.damage(
        NpcCombatDamage.builder().maximum(16).prayerEffectiveness(1).build());
    corruptedMagicStyle.animation(8419).attackSpeed(5).attackRange(6);
    corruptedMagicStyle.targetGraphic(new Graphic(1710, 124));
    corruptedMagicStyle.projectile(NpcCombatProjectile.builder().id(1708).delay(11).build());

    var corruptedMagicPrayerStyle = NpcCombatStyle.builder();
    corruptedMagicPrayerStyle.type(NpcCombatStyleType.MAGIC);
    corruptedMagicPrayerStyle.damage(
        NpcCombatDamage.builder().maximum(16).prayerEffectiveness(1).build());
    corruptedMagicPrayerStyle.effect(
        NpcCombatEffect.builder().includeMiss(true).deactivatePrayers(true).build());
    corruptedMagicPrayerStyle.animation(8419).attackSpeed(5).attackRange(6);
    corruptedMagicPrayerStyle.targetGraphic(new Graphic(1704, 124));
    corruptedMagicPrayerStyle.projectile(NpcCombatProjectile.builder().id(1702).delay(11).build());

    var corruptedUnderneathStyle = NpcCombatStyle.builder();
    corruptedUnderneathStyle.type(NpcCombatStyleType.UNDERNEATH);
    corruptedUnderneathStyle.damage(NpcCombatDamage.builder().minimum(8).maximum(60).build());
    corruptedUnderneathStyle.animation(8420).attackSpeed(5);

    var corruptedMeleeProtectionCombat = NpcCombatDefinition.builder();
    corruptedMeleeProtectionCombat.id(CORRUPTED_IDS.get(MELEE));
    corruptedMeleeProtectionCombat.hitpoints(corruptedHitpoints);
    corruptedMeleeProtectionCombat.stats(corruptedStats);
    corruptedMeleeProtectionCombat.aggression(aggression);
    corruptedMeleeProtectionCombat.immunity(
        NpcCombatImmunity.builder().poison(true).venom(true).melee(true).build());
    corruptedMeleeProtectionCombat.killCount(corruptedKillCount);
    corruptedMeleeProtectionCombat.deathAnimation(8421);
    corruptedMeleeProtectionCombat.style(corruptedRangedStyle.build());
    corruptedMeleeProtectionCombat.style(corruptedMagicStyle.build());
    corruptedMeleeProtectionCombat.style(corruptedMagicPrayerStyle.build());
    corruptedMeleeProtectionCombat.style(corruptedUnderneathStyle.build());

    var corruptedRangedProtectionCombat = NpcCombatDefinition.builder();
    corruptedRangedProtectionCombat.id(CORRUPTED_IDS.get(RANGED));
    corruptedRangedProtectionCombat.hitpoints(corruptedHitpoints);
    corruptedRangedProtectionCombat.stats(corruptedStats);
    corruptedRangedProtectionCombat.aggression(aggression);
    corruptedRangedProtectionCombat.immunity(
        NpcCombatImmunity.builder().poison(true).venom(true).ranged(true).build());
    corruptedRangedProtectionCombat.killCount(corruptedKillCount);
    corruptedRangedProtectionCombat.deathAnimation(8421);
    corruptedRangedProtectionCombat.style(corruptedRangedStyle.build());
    corruptedRangedProtectionCombat.style(corruptedMagicStyle.build());
    corruptedRangedProtectionCombat.style(corruptedMagicPrayerStyle.build());
    corruptedRangedProtectionCombat.style(corruptedUnderneathStyle.build());

    var corruptedMagicProtectionCombat = NpcCombatDefinition.builder();
    corruptedMagicProtectionCombat.id(CORRUPTED_IDS.get(MAGIC));
    corruptedMagicProtectionCombat.hitpoints(corruptedHitpoints);
    corruptedMagicProtectionCombat.stats(corruptedStats);
    corruptedMagicProtectionCombat.aggression(aggression);
    corruptedMagicProtectionCombat.immunity(
        NpcCombatImmunity.builder().poison(true).venom(true).magic(true).build());
    corruptedMagicProtectionCombat.killCount(corruptedKillCount);
    corruptedMagicProtectionCombat.deathAnimation(8421);
    corruptedMagicProtectionCombat.style(corruptedRangedStyle.build());
    corruptedMagicProtectionCombat.style(corruptedMagicStyle.build());
    corruptedMagicProtectionCombat.style(corruptedMagicPrayerStyle.build());
    corruptedMagicProtectionCombat.style(corruptedUnderneathStyle.build());

    return Arrays.asList(
        crystallineMeleeProtectionCombat.build(),
        crystallineRangedProtectionCombat.build(),
        crystallineMagicProtectionCombat.build(),
        corruptedMeleeProtectionCombat.build(),
        corruptedRangedProtectionCombat.build(),
        corruptedMagicProtectionCombat.build());
  }

  @Override
  public List<NpcCombatDropTable> getAdditionalDropTables(int npcId) {
    return CORRUPTED_IDS.contains(npcId) ? CORRUPTED_GAUNTLET_DROP_TABLES : GAUNTLET_DROP_TABLES;
  }

  @Override
  public void spawnHook() {
    if (isCrystal()) {
      ids = CRYSTALLINE_IDS;
    } else {
      ids = CORRUPTED_IDS;
    }
  }

  @Override
  public void tickStartHook() {
    tornadoAttack();
    tileAttack();
  }

  @Override
  public HitStyleType attackTickHitStyleTypeHook(HitStyleType hitStyleType, Entity opponent) {
    return hitStyle;
  }

  @Override
  public int applyAttackMaximumDamageHook(NpcCombatStyle combatStyle, Entity opponent) {
    var damage = combatStyle.getDamage().getMaximum();
    if (!opponent.isPlayer()) {
      return damage;
    }
    var player = opponent.asPlayer();
    var correctPrayer = false;
    switch (combatStyle.getType().getHitStyleType()) {
      case MELEE:
        correctPrayer = player.getPrayer().hasActive("protect from melee");
        break;
      case RANGED:
        correctPrayer = player.getPrayer().hasActive("protect from missiles");
        break;
      case MAGIC:
        correctPrayer = player.getPrayer().hasActive("protect from magic");
        break;
    }
    var plugin = player.getPlugin(TirannwnPlugin.class);
    if (isCrystal()) {
      switch (plugin.getTheGauntlet().getArmourTier()) {
        case 1:
          damage -= correctPrayer ? 2 : 1;
          break;
        case 2:
          damage -= correctPrayer ? 4 : 2;
          break;
        case 3:
          damage -= correctPrayer ? 6 : 3;
          break;
        default:
          if (correctPrayer) {
            damage -= 1;
          }
          break;
      }
    } else {
      switch (plugin.getTheGauntlet().getArmourTier()) {
        case 1:
          damage -= correctPrayer ? 3 : 1;
          break;
        case 2:
          damage -= correctPrayer ? 6 : 3;
          break;
        case 3:
          damage -= correctPrayer ? 8 : 4;
          break;
        default:
          if (correctPrayer) {
            damage -= 1;
          }
          break;
      }
    }
    return damage;
  }

  @Override
  public void applyAttackEndHook(
      NpcCombatStyle combatStyle, Entity opponent, int applyAttackLoopCount, HitEvent hitEvent) {
    if (combatStyle.getType().getHitStyleType() == HitStyleType.UNDERNEATH) {
      return;
    }
    if (++hitCount == 4) {
      hitCount = 0;
      hitStyle = hitStyle == HitStyleType.RANGED ? HitStyleType.MAGIC : HitStyleType.RANGED;
      addSingleEvent(2, e -> npc.setAnimation(hitStyle == HitStyleType.RANGED ? 8755 : 8754));
    }
  }

  @Override
  public void attackTickEndHook(NpcCombatStyle combatStyle, Entity opponent) {
    tornadoDelay--;
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    var gauntlet = player.getPlugin(TirannwnPlugin.class).getTheGauntlet();
    if (isCrystal()) {
      gauntlet.setReward(Reward.CRYSTALLINE);
    } else {
      gauntlet.setReward(Reward.CORRUPTED);
    }
    gauntlet.setVarps();
    player.getGameEncoder().sendMessage("Your reward awaits you in the nearby chest.");
    player.getController().stop();
    player.rejuvenate();
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (isDead() || !opponent.isPlayer()) {
      return damage;
    }
    if (hitStyleType != HitStyleType.MELEE
        && hitStyleType != HitStyleType.RANGED
        && hitStyleType != HitStyleType.MAGIC) {
      return damage;
    }
    if (npc.getId() == ids.get(MELEE)) {
      correctHits += hitStyleType == HitStyleType.MELEE ? 0 : 1;
    } else if (npc.getId() == ids.get(RANGED)) {
      correctHits += hitStyleType == HitStyleType.RANGED ? 0 : 1;
    } else if (npc.getId() == ids.get(MAGIC)) {
      correctHits += hitStyleType == HitStyleType.MAGIC ? 0 : 1;
    }
    if (correctHits == 6) {
      correctHits = 0;
      switch (hitStyleType) {
        case MELEE:
          npc.setId(ids.get(MELEE));
          break;
        case RANGED:
          npc.setId(ids.get(RANGED));
          break;
        case MAGIC:
          npc.setId(ids.get(MAGIC));
          break;
      }
    }
    return damage;
  }

  private void tornadoAttack() {
    if (!isAttacking()) {
      return;
    }
    if (isHitDelayed()) {
      return;
    }
    if (tornadoDelay > 0) {
      return;
    }
    tornadoDelay = 4 + PRandom.randomI(4, 10);
    npc.setAnimation(8418);
    setHitDelay(6);
    var hitpointsPercent = PRandom.getPercent(getHitpoints(), getMaxHitpoints());
    addSingleEvent(
        4,
        e -> {
          var count = 1;
          if (hitpointsPercent <= 66) {
            count++;
          }
          if (hitpointsPercent <= 33) {
            count++;
          }
          for (var i = 0; i < count; i++) {
            var tornadoNpc =
                addNpc(
                    new NpcSpawn(
                        getBounds().getRandomTile(),
                        isCrystal() ? NullNpcId.NULL_9025 : NullNpcId.NULL_9039));
            tornadoNpc.getCombat().setTarget(getAttackingEntity());
          }
        });
  }

  private void tileAttack() {
    if (!isAttacking()) {
      return;
    }
    if (tileTask != null && tileTask.isRunning()) {
      return;
    }
    var attackingEntity = getAttackingEntity();
    if (attackingEntity == null || !attackingEntity.isPlayer()) {
      return;
    }
    var hitpointsPercent = PRandom.getPercent(getHitpoints(), getMaxHitpoints());
    var baseTile = getBaseTile();
    var baseTiles = PRandom.arrayRandom(EasyTileType.values()).getTiles(baseTile);
    var tileChangeDelay = 8;
    var tileMoveDelay = 16;
    if (hitpointsPercent <= 66) {
      if (PRandom.randomE(2) == 0) {
        baseTiles = PRandom.arrayRandom(MediumTileType.values()).getTiles(baseTile);
      }
      tileChangeDelay -= 2;
      tileMoveDelay -= 4;
    }
    if (hitpointsPercent <= 33) {
      if (PRandom.randomE(8) != 0) {
        baseTiles = PRandom.arrayRandom(HardTileType.values()).getTiles(baseTile);
      }
      tileChangeDelay -= 2;
      tileMoveDelay -= 4;
    }
    var selectedTiles = baseTiles;
    var isCrystal = isCrystal();
    var mapObjects = new ArrayList<MapObject>();
    selectedTiles.forEach(
        tile -> {
          var mapObject =
              new MapObject(
                  isCrystal ? NullObjectId.NULL_36150 : NullObjectId.NULL_36047, 22, 0, tile);
          mapObjects.add(mapObject);
          npc.getController().addMapObject(mapObject);
        });
    tileTask = new PEventTasks();
    tileTask.constant(
        tileChangeDelay + 1,
        t -> {
          selectedTiles.forEach(
              tile -> {
                if (attackingEntity.isLocked()) {
                  return;
                }
                if (!attackingEntity.withinDistance(tile, 0)) {
                  return;
                }
                attackingEntity
                    .getCombat()
                    .addHitEvent(new HitEvent(new Hit(PRandom.randomI(8, 20))));
              });
          t.delay();
        });
    tileTask.execute(
        tileChangeDelay,
        t -> {
          mapObjects.forEach(
              m -> {
                npc.getController()
                    .addMapObject(
                        new MapObject(
                            isCrystal ? NullObjectId.NULL_36151 : NullObjectId.NULL_36048, m));
              });
        });
    tileTask.delay(tileMoveDelay);
    tileTask.stop(
        t -> {
          mapObjects.forEach(
              m -> {
                npc.getController()
                    .addMapObject(
                        new MapObject(
                            isCrystal ? NullObjectId.NULL_36149 : NullObjectId.NULL_36046, m));
              });
        });
    addEvent(tileTask);
  }

  private boolean isCrystal() {
    return CRYSTALLINE_IDS.contains(npc.getId());
  }

  public Bounds getBounds() {
    return isCrystal() ? CRYSTALLINE_BOUNDS : CORRUPTED_BOUNDS;
  }

  private Tile getBaseTile() {
    return isCrystal() ? CRYSTALLINE_BASE_TILE : CORRUPT_BASE_TILE;
  }

  @AllArgsConstructor
  @Getter
  private enum EasyTileType implements TileAttack {
    ONE(new Bounds(6, 6, 11, 11, 1)),
    TWO(new Bounds(4, 2, 9, 7, 1)),
    THREE(new Bounds(0, 6, 5, 11, 1)),
    FOUR(new Bounds(0, 0, 5, 5, 1)),
    FIVE(new Bounds(4, 4, 9, 9, 1)),
    SIX(new Bounds(2, 4, 7, 9, 1)),
    SEVEN(new Bounds(6, 0, 11, 5, 1)),
    EIGHT(new Bounds(2, 2, 7, 7, 1)),
    NINE(new Bounds(0, 0, 3, 11, 1)),
    TEN(new Bounds(8, 0, 11, 11, 1)),
    ELEVEN(new Bounds(0, 8, 11, 11, 1)),
    TWELVE(new Bounds(0, 0, 11, 3, 1));

    private final Bounds bounds;

    @Override
    public List<Tile> getTiles(Tile start) {
      var tiles = bounds.getTiles();
      for (var tile : tiles) {
        tile.moveTile(start.getX(), start.getY());
      }
      return tiles;
    }
  }

  @AllArgsConstructor
  @Getter
  private enum MediumTileType implements TileAttack {
    ONE(new Bounds(0, 0, 11, 2, 1), new Bounds(0, 9, 11, 11, 1)),
    TWO(new Bounds(0, 0, 2, 11, 1), new Bounds(9, 0, 11, 11, 1));

    private final Bounds bounds1;
    private final Bounds bounds2;

    @Override
    public List<Tile> getTiles(Tile start) {
      var tiles = bounds1.getTiles();
      for (var tile : tiles) {
        tile.setHeight(start.getHeight());
        tile.moveTile(start.getX(), start.getY());
      }
      var tiles2 = bounds2.getTiles();
      for (var tile : tiles2) {
        tile.setHeight(start.getHeight());
        tile.moveTile(start.getX(), start.getY());
      }
      tiles.addAll(tiles2);
      return tiles;
    }
  }

  @AllArgsConstructor
  @Getter
  private enum HardTileType implements TileAttack {
    ONE(
        new Bounds(0, 0, 2, 2, 1),
        new Bounds(9, 0, 11, 2, 1),
        new Bounds(0, 9, 2, 11, 1),
        new Bounds(9, 9, 11, 11, 1),
        new Bounds(4, 4, 7, 7, 1)),
    TWO(
        new Bounds(0, 0, 11, 1, 1),
        new Bounds(0, 10, 11, 11, 1),
        new Bounds(0, 2, 1, 9, 1),
        new Bounds(10, 2, 11, 9, 1)),
    THREE(
        new Bounds(1, 1, 4, 4, 1),
        new Bounds(7, 1, 10, 4, 1),
        new Bounds(1, 7, 4, 10, 1),
        new Bounds(7, 7, 10, 10, 1)),
    FOUR(
        new Bounds(0, 0, 3, 3, 1),
        new Bounds(8, 0, 11, 3, 1),
        new Bounds(0, 8, 3, 11, 1),
        new Bounds(8, 8, 11, 11, 1));

    private final List<Bounds> bounds;

    HardTileType(Bounds... bounds) {
      this.bounds = Arrays.asList(bounds);
    }

    @Override
    public List<Tile> getTiles(Tile start) {
      var tiles = new ArrayList<Tile>();
      for (var bound : bounds) {
        var ts = bound.getTiles();
        for (var t : ts) {
          t.setHeight(start.getHeight());
          t.moveTile(start.getX(), start.getY());
        }
        tiles.addAll(ts);
      }
      return tiles;
    }
  }

  private interface TileAttack {
    List<Tile> getTiles(Tile start);
  }
}
