package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Farming;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.SEED_BOX)
class SeedBoxItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "fill":
        {
          for (var i = player.getInventory().size() - 1; i >= 0; i--) {
            var addingId = player.getInventory().getId(i);
            if (!Farming.isSeed(addingId)) {
              continue;
            }
            var addingAmount = player.getInventory().getAmount(i);
            addingAmount =
                player.getWidgetManager().getSeedBox().canAddAmount(addingId, addingAmount);
            addingAmount =
                player
                    .getWidgetManager()
                    .getSeedBox()
                    .addItem(addingId, addingAmount)
                    .getSuccessAmount();
            player.getInventory().deleteItem(addingId, addingAmount, i);
          }
          break;
        }
      case "empty":
        {
          for (var i = player.getWidgetManager().getSeedBox().size() - 1; i >= 0; i--) {
            var addingId = player.getWidgetManager().getSeedBox().getId(i);
            var addingAmount = player.getWidgetManager().getSeedBox().getAmount(i);
            addingAmount = player.getInventory().canAddAmount(addingId, addingAmount);
            addingAmount = player.getInventory().addItem(addingId, addingAmount).getSuccessAmount();
            player.getWidgetManager().getSeedBox().deleteItem(addingId, addingAmount);
          }
          break;
        }
      case "check":
        player.getWidgetManager().getSeedBox().displayItemList();
        break;
    }
  }
}
