package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.cache.widget.PrayerChild;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropLocationType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatMovement;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.graphic.Graphic.Projectile;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.godwars.GodWarsPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PEventTasks;
import com.palidinodh.util.PNumber;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

class NexCombat extends NpcCombat {

  private static final NpcSpawn FUMUS_SPAWN =
      new NpcSpawn(Tile.Direction.SOUTH_EAST, new Tile(2913, 5215), NpcId.FUMUS_285);
  private static final NpcSpawn UMBRA_SPAWN =
      new NpcSpawn(Tile.Direction.SOUTH_WEST, new Tile(2937, 5215), NpcId.UMBRA_285);
  private static final NpcSpawn CRUOR_SPAWN =
      new NpcSpawn(Tile.Direction.NORTH_WEST, new Tile(2937, 5191), NpcId.CRUOR_285);
  private static final NpcSpawn GLACIES_SPAWN =
      new NpcSpawn(Tile.Direction.NORTH_EAST, new Tile(2913, 5191), NpcId.GLACIES_285);

  private static final NpcCombatStyle MELEE_ATTACK;
  private static final NpcCombatStyle SMOKE_MAGIC_ATTACK;
  private static final NpcCombatStyle SHADOW_RANGED_ATTACK;
  private static final NpcCombatStyle BLOOD_MAGIC_ATTACK;
  private static final NpcCombatStyle ICE_MAGIC_ATTACK;
  private static final NpcCombatStyle ZAROS_MAGIC_ATTACK;
  private static final NpcCombatStyle ZAROS_MELEE_ATTACK;

  private static final NpcCombatStyle SHADOW_SMASH_ATTACK;
  private static final Tile[] CONTAINMENT_OFFSETS = {
    new Tile(-1, -1),
    new Tile(0, -1),
    new Tile(1, -1),
    new Tile(2, -1),
    new Tile(3, -1),
    new Tile(-1, 3),
    new Tile(0, 3),
    new Tile(1, 3),
    new Tile(2, 3),
    new Tile(3, 3),
    new Tile(-1, 0),
    new Tile(-1, 1),
    new Tile(-1, 2),
    new Tile(3, 0),
    new Tile(3, 1),
    new Tile(3, 2)
  };

  private static final int UNIQUE_DROP_TABLE_DENOMINATOR = 53;

  static {
    var style = NpcCombatStyle.builder();
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(9180).attackSpeed(4);
    MELEE_ATTACK = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(
        NpcCombatDamage.builder().maximum(32).splashOnMiss(true).prayerEffectiveness(0.8).build());
    style.animation(9181).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(1997));
    style.effect(NpcCombatEffect.builder().poison(8).build());
    style.multiCombat(
        NpcCombatMulti.builder().type(NpcCombatMulti.Type.NEAR_OPPONENT).chance(50).build());
    SMOKE_MAGIC_ATTACK = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(
        NpcCombatDamage.builder()
            .maximum(60)
            .prayerEffectiveness(0.5)
            .distanceReduction(0.1)
            .build());
    style.animation(9181).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(1999));
    style.effect(
        NpcCombatEffect.builder().randomizeStatDrains(true).statDrain(Skills.PRAYER, 15).build());
    style.multiCombat(
        NpcCombatMulti.builder().type(NpcCombatMulti.Type.NEAR_OPPONENT).chance(60).build());
    SHADOW_RANGED_ATTACK = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(
        NpcCombatDamage.builder().maximum(32).splashOnMiss(true).prayerEffectiveness(0.8).build());
    style.animation(9181).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(2000));
    style.effect(
        NpcCombatEffect.builder()
            .statDrainsByDamageMultiplier(0.25)
            .statDrainByDamage(Skills.PRAYER)
            .healByDamage(0.25)
            .build());
    style.multiCombat(NpcCombatMulti.nearOpponent(1));
    BLOOD_MAGIC_ATTACK = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(
        NpcCombatDamage.builder().maximum(32).splashOnMiss(true).prayerEffectiveness(0.8).build());
    style.animation(9181).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(2004));
    style.effect(
        NpcCombatEffect.builder()
            .statDrainsByDamageMultiplier(0.5)
            .statDrainByDamage(Skills.PRAYER)
            .bindType(NpcCombatEffect.BindType.PRAYER_BLOCKS)
            .bind(34)
            .build());
    style.multiCombat(
        NpcCombatMulti.builder().type(NpcCombatMulti.Type.NEAR_OPPONENT).chance(50).build());
    ICE_MAGIC_ATTACK = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(
        NpcCombatDamage.builder().maximum(32).splashOnMiss(true).prayerEffectiveness(0.8).build());
    style.animation(9181).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(2007));
    style.effect(NpcCombatEffect.builder().statDrain(Skills.PRAYER, 5).build());
    style.multiCombat(
        NpcCombatMulti.builder().type(NpcCombatMulti.Type.NEAR_OPPONENT).chance(50).build());
    ZAROS_MAGIC_ATTACK = style.build();

    style = NpcCombatStyle.builder();
    style.damage(NpcCombatDamage.builder().maximum(30).prayerEffectiveness(0.8).build());
    style.animation(9180).attackSpeed(4);
    style.multiCombat(NpcCombatMulti.nearOpponent(1));
    ZAROS_MELEE_ATTACK = style.build();

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(
        NpcCombatDamage.builder().maximum(50).ignoreDefence(true).ignorePrayer(true).build());
    style.animation(9186).attackSpeed(4);
    style.targetTileGraphic(new Graphic(383));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.specialAttack(NpcCombatTargetTile.builder().eventDelay(3).build());
    SHADOW_SMASH_ATTACK = style.build();
  }

  @Inject private Npc npc;
  private Phase phase = Phase.SPAWN;
  private PArrayList<Player> players;
  private double scaleMultiplier;
  private NpcCombatStyle pendingAttack;
  private int damageHealsDelay;
  private List<Npc> bloodReavers = new ArrayList<>();
  private List<MapObject> icicles = new ArrayList<>();
  private int soulSplitDamage;
  private Player mvpPlayer;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .locationType(NpcCombatDropLocationType.UNDER_OPPONENT)
            .clue(ClueScrollType.ELITE, 128)
            .pet(ItemId.NEXLING, 500)
            .additionalPlayers(255)
            .additionalDropRateDivider(0);
    var dropTable =
        NpcCombatDropTable.builder()
            .probabilityDenominator(UNIQUE_DROP_TABLE_DENOMINATOR)
            .broadcast(true)
            .log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_HILT).weight(1)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NIHIL_HORN).weight(2)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORVA_FULL_HELM_DAMAGED).weight(2)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORVA_PLATEBODY_DAMAGED).weight(2)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORVA_PLATELEGS_DAMAGED).weight(2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ZARYTE_VAMBRACES).weight(3)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_RUNE, 123, 1365)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_RUNE, 210, 1655)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATER_RUNE, 193, 1599)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ONYX_BOLTS_E, 11, 29)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COAL_NOTED, 23, 95)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_ORE_NOTED, 2, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_ESSENCE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 8539, 26748)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_SWORD)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 84, 325)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 85, 170)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 86, 227)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_BOLTS_UNF, 12, 90)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CANNONBALL, 42, 298)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_ORB_NOTED, 6, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_RUBY_NOTED, 3, 26)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DIAMOND, 3, 17)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.WINE_OF_ZAMORAK_NOTED, 4, 14)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHARK, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_POTION_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SARADOMIN_BREW_4, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPER_RESTORE_4)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ECUMENICAL_KEY_SHARD, 6, 39)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NIHIL_SHARD, 1, 20)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BIG_BONES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NIHIL_SHARD, 1, 10)));
    drop.table(dropTable.build());

    var idleCombat = NpcCombatDefinition.builder();
    idleCombat.id(NpcId.NEX_1001_11279);

    var combat = NpcCombatDefinition.builder();
    combat
        .id(NpcId.NEX_1001)
        .id(NpcId.NEX_1001_11280)
        .id(NpcId.NEX_1001_11281)
        .id(NpcId.NEX_1001_11282);
    combat.spawn(NpcCombatSpawn.builder().respawnDelay(35).deathDelay(8).build());
    combat.movement(NpcCombatMovement.builder().run(true).build());
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(3400).barType(HitpointsBarType.GREEN_RED_120).build());
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(315)
            .magicLevel(230)
            .rangedLevel(350)
            .defenceLevel(260)
            .bonus(BonusType.MELEE_ATTACK, 200)
            .bonus(BonusType.ATTACK_MAGIC, 100)
            .bonus(BonusType.ATTACK_RANGED, 150)
            .bonus(BonusType.DEFENCE_STAB, 40)
            .bonus(BonusType.DEFENCE_SLASH, 140)
            .bonus(BonusType.DEFENCE_CRUSH, 60)
            .bonus(BonusType.DEFENCE_MAGIC, 300)
            .bonus(BonusType.DEFENCE_RANGED, 190)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(32).checkWhileAttacking(true).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat.focus(NpcCombatFocus.builder().keepWithinDistance(5).routedFollowing(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.deathAnimation(9184).blockAnimation(9185);
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.drop(drop.build());

    combat.style(MELEE_ATTACK);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(
        NpcCombatDamage.builder().maximum(32).splashOnMiss(true).prayerEffectiveness(0.8).build());
    style.animation(9181).attackSpeed(4);
    style.multiCombat(
        NpcCombatMulti.builder().type(NpcCombatMulti.Type.NEAR_OPPONENT).chance(50).build());
    combat.style(style.build());

    return Arrays.asList(idleCombat.build(), combat.build());
  }

  @Override
  public void npcApplyHitStartHook(Hit hit) {
    if (damageHealsDelay > 0 && hit.getHitStyleType() != HitStyleType.HEAL) {
      switch (hit.getHitStyleType()) {
        case MELEE:
        case MAGIC:
        case RANGED:
          {
            hit.setHitMarkType(HitMarkType.HEAL);
            hit.setHitStyleType(HitStyleType.HEAL);
            break;
          }
      }
    }
    if (hit.getHitStyleType() == HitStyleType.HEAL) {
      return;
    }
    switch (phase) {
      case SPAWN:
      case SMOKE_LOCKED:
      case SHADOW_LOCKED:
      case BLOOD_LOCKED:
      case ICE_LOCKED:
        hit.setDamage(0);
        break;
    }
    if (npc.getId() == NpcId.NEX_1001_11281 && hit.getHitStyleType() == HitStyleType.MELEE) {
      var opponent = hit.getSource();
      var damage = hit.getDamage();
      hit.setDamage(-1);
      if (opponent == null) {
        return;
      }
      opponent.getCombat().addHit(new Hit(damage));
    }
  }

  @Override
  public void restoreHook() {
    phase = Phase.SPAWN;
    players = null;
    scaleMultiplier = 1;
    pendingAttack = null;
    damageHealsDelay = 0;
    removeNpcs(bloodReavers);
    bloodReavers.clear();
    npc.getController().removeMapObjects(icicles);
    icicles.clear();
    mvpPlayer = null;
  }

  @Override
  public void spawnHook() {
    players = npc.getController().getNearbyPlayers();
    npc.setLargeVisibility();
    npc.setForceMessage("AT LAST!");
    npc.setAnimation(9182);
    npc.lock();
    var projectileSpeed = getProjectileSpeed(7);
    scaleMultiplier = Math.min(PNumber.addDoubles(0.375, players.size() * 0.125), 1);
    setMaxHitpoints((int) (getMaxHitpoints() * scaleMultiplier));
    setHitpoints((int) (getMaxHitpoints() * scaleMultiplier));
    var event = new PEventTasks();
    event.execute(
        5,
        t -> {
          npc.setForceMessage("Fumus!");
          npc.setAnimation(9188);
          npc.setFaceTile(FUMUS_SPAWN.getTile());
          var minion = addNpc(FUMUS_SPAWN);
          npc.getCombat()
              .sendMapProjectile(
                  Projectile.builder()
                      .id(2010)
                      .entity(npc)
                      .startTile(minion)
                      .speed(projectileSpeed)
                      .build());
          minion
              .getCombat()
              .setMaxHitpoints((int) (minion.getCombat().getMaxHitpoints() * scaleMultiplier));
          minion
              .getCombat()
              .setHitpoints((int) (minion.getCombat().getMaxHitpoints() * scaleMultiplier));
        });
    event.execute(
        4,
        t -> {
          npc.setForceMessage("Umbra!");
          npc.setAnimation(9188);
          npc.setFaceTile(UMBRA_SPAWN.getTile());
          var minion = addNpc(UMBRA_SPAWN);
          npc.getCombat()
              .sendMapProjectile(
                  Projectile.builder()
                      .id(2010)
                      .entity(npc)
                      .startTile(minion)
                      .speed(projectileSpeed)
                      .build());
          minion
              .getCombat()
              .setMaxHitpoints((int) (minion.getCombat().getMaxHitpoints() * scaleMultiplier));
          minion
              .getCombat()
              .setHitpoints((int) (minion.getCombat().getMaxHitpoints() * scaleMultiplier));
        });
    event.execute(
        4,
        t -> {
          npc.setForceMessage("Cruor!");
          npc.setAnimation(9188);
          npc.setFaceTile(CRUOR_SPAWN.getTile());
          var minion = addNpc(CRUOR_SPAWN);
          npc.getCombat()
              .sendMapProjectile(
                  Projectile.builder()
                      .id(2010)
                      .entity(npc)
                      .startTile(minion)
                      .speed(projectileSpeed)
                      .build());
          minion
              .getCombat()
              .setMaxHitpoints((int) (minion.getCombat().getMaxHitpoints() * scaleMultiplier));
          minion
              .getCombat()
              .setHitpoints((int) (minion.getCombat().getMaxHitpoints() * scaleMultiplier));
        });
    event.execute(
        4,
        t -> {
          npc.setForceMessage("Glacies!");
          npc.setAnimation(9188);
          npc.setFaceTile(GLACIES_SPAWN.getTile());
          var minion = addNpc(GLACIES_SPAWN);
          npc.getCombat()
              .sendMapProjectile(
                  Projectile.builder()
                      .id(2010)
                      .entity(npc)
                      .startTile(minion)
                      .speed(projectileSpeed)
                      .build());
          minion
              .getCombat()
              .setMaxHitpoints((int) (minion.getCombat().getMaxHitpoints() * scaleMultiplier));
          minion
              .getCombat()
              .setHitpoints((int) (minion.getCombat().getMaxHitpoints() * scaleMultiplier));
        });
    event.execute(
        4,
        t -> {
          phase = Phase.SMOKE;
          npc.setForceMessage(phase.getPhrase());
          npc.getCombat()
              .sendMapProjectile(
                  Projectile.builder()
                      .id(2010)
                      .entity(npc)
                      .startTile(npc.getController().getNpc(phase.getLockedMinionId()))
                      .speed(projectileSpeed)
                      .build());
          npc.setId(NpcId.NEX_1001);
        });
    event.execute(
        4,
        t -> {
          npc.unlock();
          chokeAttack();
        });
    addEvent(event);
  }

  @Override
  public void tickStartHook() {
    if (!npc.isVisible()) {
      return;
    }
    players = npc.getController().getNearbyPlayers();
    if (players.isEmpty()) {
      var controller = npc.getController();
      controller.removeNpc(npc);
      controller.tick();
      controller.clearUpdates();
      return;
    }
    if (npc.isLocked()) {
      return;
    }
    if (damageHealsDelay > 0) {
      damageHealsDelay--;
    }
    checkPhase();
    checkMeleeAttack();
  }

  @Override
  public NpcCombatStyle attackTickCombatStyleHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (pendingAttack != null) {
      return pendingAttack;
    }
    switch (combatStyle.getType().getHitStyleType()) {
      case MELEE:
        {
          if (phase == Phase.ZAROS) {
            return ZAROS_MELEE_ATTACK;
          }
          break;
        }
      case RANGED:
      case MAGIC:
        {
          switch (phase) {
            case SMOKE:
            case SMOKE_LOCKED:
              return SMOKE_MAGIC_ATTACK;
            case SHADOW:
            case SHADOW_LOCKED:
              return SHADOW_RANGED_ATTACK;
            case BLOOD:
            case BLOOD_LOCKED:
              return BLOOD_MAGIC_ATTACK;
            case ICE:
            case ICE_LOCKED:
              return ICE_MAGIC_ATTACK;
            case ZAROS:
              return ZAROS_MAGIC_ATTACK;
          }
          break;
        }
    }
    return combatStyle;
  }

  @Override
  public void applyAttackEndHook(
      NpcCombatStyle combatStyle, Entity opponent, int applyAttackLoopCount, HitEvent hitEvent) {
    pendingAttack = null;
    if (npc.getId() == NpcId.NEX_1001_11280 && hitEvent.getDamage() > 0) {
      soulSplitDamage += hitEvent.getDamage();
      addSingleEvent(
          hitEvent.getTick() + 1,
          e -> {
            var projectile =
                Graphic.Projectile.builder()
                    .id(2009)
                    .entity(npc)
                    .startTile(opponent)
                    .speed(getProjectileSpeed(opponent).moveDelay())
                    .startHeight(0)
                    .build();
            sendMapProjectile(projectile);
          });
    }
  }

  public int applyAttackMaximumDamageHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (combatStyle == SHADOW_SMASH_ATTACK) {
      return combatStyle.getDamage().getMaximum();
    }
    return (int) (combatStyle.getDamage().getMaximum() * scaleMultiplier);
  }

  @Override
  public void attackTickEndHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (soulSplitDamage > 0) {
      var damage = soulSplitDamage;
      addSingleEvent(
          getProjectileSpeed(5).getEventDelay() * 2L + 1,
          e2 -> applyHit(new Hit(damage, HitMarkType.HEAL, HitStyleType.HEAL)));
      soulSplitDamage = 0;
    }
  }

  @Override
  public void applyDeadStartHook(int deathDelay) {
    wrathAttack();
    findMvpPlayer();
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    player.getArea().script("increase_ancient_killcount", 5);
  }

  @Override
  public int dropTableDenominatorHook(
      Player player, int dropRateDivider, int roll, NpcCombatDropTable table, int denominator) {
    if (table.getProbabilityDenominator() == UNIQUE_DROP_TABLE_DENOMINATOR) {
      if (player == mvpPlayer) {
        denominator *= 0.95;
      }
      denominator *= players.size();
    }
    return denominator;
  }

  private void checkPhase() {
    var locked = true;
    if (phase.getMinionId() != -1) {
      var minion = npc.getController().getNpc(phase.getMinionId());
      if (minion == null || minion.isLocked()) {
        locked = false;
      }
    }
    var changedPhase = Phase.getPhase(phase, getHitpoints() / (double) getMaxHitpoints(), locked);
    if (phase == changedPhase || phase.ordinal() >= changedPhase.ordinal()) {
      return;
    }
    phase = changedPhase;
    if (phase.getMinionId() != -1) {
      var minion = npc.getController().getNpc(phase.getMinionId());
      if (minion != null) {
        minion.getCombat().as(NexMinionCombat.class).setBlockDamage(false);
      }
    }
    npc.setForceMessage(phase.getPhrase());
    npc.getCombat()
        .sendMapProjectile(
            Projectile.builder()
                .id(2010)
                .entity(npc)
                .startTile(npc.getController().getNpc(phase.getLockedMinionId()))
                .speed(getProjectileSpeed(7))
                .build());
    if (!locked) {
      setHitDelay(4);
      switch (phase) {
        case SHADOW:
          pendingAttack = SHADOW_SMASH_ATTACK;
          break;
        case BLOOD:
          addSingleEvent(4, e -> bloodSiphonAttack());
          break;
        case ICE:
          bloodSiphonHeal();
          addSingleEvent(4, e -> containmentAttack());
          break;
        case ZAROS:
          {
            applyHit(new Hit((int) (500 * scaleMultiplier), HitMarkType.HEAL, HitStyleType.HEAL));
            npc.setAnimation(9179);
            npc.setGraphic(2016);
            addSingleEvent(4, e -> soulSplitAttack(true));
            break;
          }
      }
    }
  }

  private void checkMeleeAttack() {
    if (isLocked()) {
      return;
    }
    if (!isAttacking()) {
      return;
    }
    if (getHitDelay() < -4 && pendingAttack == MELEE_ATTACK) {
      pendingAttack = null;
    }
    if (pendingAttack != null) {
      return;
    }
    if (getHitDelay() <= 0) {
      return;
    }
    if (PRandom.randomE(32) != 0) {
      return;
    }
    pendingAttack = MELEE_ATTACK;
    setFollowing(getAttackingEntity(), 1, null);
  }

  private void chokeAttack() {
    Player infectedPlayer = null;
    var lowestMagicDefence = Integer.MAX_VALUE;
    for (Player p : players) {
      var bonus = PCombat.getHitChanceHook().getEquipmentDefence(p, HitStyleType.MAGIC, null);
      if (bonus >= lowestMagicDefence) {
        continue;
      }
      lowestMagicDefence = bonus;
      infectedPlayer = p;
    }
    if (infectedPlayer == null) {
      return;
    }
    setHitDelay(4);
    npc.setForceMessage("Let the virus flow through you!");
    npc.setFaceEntity(infectedPlayer);
    npc.setAnimation(9186);
    infectedPlayer.getPlugin(GodWarsPlugin.class).startNexCough();
  }

  private void bloodSiphonAttack() {
    bloodSiphonHeal();
    npc.setForceMessage("A siphon will solve this!");
    npc.setAnimation(9183);
    damageHealsDelay = 8;
    setLock(8);
    npc.getController().setMagicBind(damageHealsDelay);
    var reaverCount = Math.max(2, players.size() / 10);
    for (var i = 0; i < reaverCount; i++) {
      Tile tile = new Tile();
      for (var tries = 0; tries < 64; tries++) {
        tile.setTile(npc);
        tile.randomize(reaverCount * 2);
        if (tile.getX() < 2910) {
          tile.setX(2910);
        }
        if (!npc.getController().isMapClipEmpty(tile, 2)) {
          continue;
        }
        var spawn = new NpcSpawn(32, tile, NpcId.BLOOD_REAVER_161);
        var reaverNpc = addNpc(spawn);
        reaverNpc
            .getCombat()
            .setMaxHitpoints((int) (reaverNpc.getCombat().getMaxHitpoints() * scaleMultiplier));
        reaverNpc
            .getCombat()
            .setHitpoints((int) (reaverNpc.getCombat().getMaxHitpoints() * scaleMultiplier));
        bloodReavers.add(reaverNpc);
        break;
      }
    }
  }

  private void bloodSiphonHeal() {
    if (bloodReavers.isEmpty()) {
      return;
    }
    var remainingHitpoints = 0;
    for (var bloodReaver : bloodReavers) {
      if (bloodReaver.isLocked()) {
        continue;
      }
      remainingHitpoints += bloodReaver.getCombat().getHitpoints();
    }
    removeNpcs(bloodReavers);
    bloodReavers.clear();
    if (remainingHitpoints == 0) {
      return;
    }
    npc.getCombat().applyHit(new Hit(remainingHitpoints, HitMarkType.HEAL, HitStyleType.HEAL));
  }

  private void containmentAttack() {
    npc.setForceMessage("Contain this!");
    npc.setAnimation(9186);
    setHitDelay(4);
    addSingleEvent(
        4,
        e -> {
          for (var offsetTile : CONTAINMENT_OFFSETS) {
            var tile =
                new Tile(
                    npc.getX() + offsetTile.getX(),
                    npc.getY() + offsetTile.getY(),
                    npc.getHeight());
            if (!npc.getController().isMapClipEmpty(tile, 1)) {
              continue;
            }
            var mapObject = new MapObject(ObjectId.STALAGMITE_42943, 10, 0, tile);
            icicles.add(mapObject);
            npc.getController().addMapObject(mapObject);
            players.forEach(
                p -> {
                  if (!p.withinDistance(tile, 0)) {
                    return;
                  }
                  p.getCombat().addHitEvent(new HitEvent(new Hit(PRandom.randomI(1, 60))));
                  p.getPrayer().deactivate(PrayerChild.PROTECT_FROM_MISSILES);
                  p.getPrayer().deactivate(PrayerChild.PROTECT_FROM_MAGIC);
                  p.getPrayer().deactivate(PrayerChild.PROTECT_FROM_MELEE);
                });
          }
        });
    addSingleEvent(
        14,
        e -> {
          npc.getController().removeMapObjects(icicles);
          icicles.clear();
        });
  }

  private void soulSplitAttack(boolean deflectAfter) {
    if (npc.getId() != NpcId.NEX_1001) {
      return;
    }
    npc.setId(NpcId.NEX_1001_11280);
    addSingleEvent(
        18,
        e -> {
          npc.setId(NpcId.NEX_1001);
          if (deflectAfter) {
            deflectMeleeAttack();
          }
        });
  }

  private void deflectMeleeAttack() {
    if (npc.getId() != NpcId.NEX_1001) {
      return;
    }
    npc.setId(NpcId.NEX_1001_11281);
    addSingleEvent(18, e -> npc.setId(NpcId.NEX_1001));
  }

  private void wrathAttack() {
    var deathPlayers = new ArrayList<>(players);
    npc.setId(NpcId.NEX_1001_11282);
    npc.setForceMessage("Taste my wrath!");
    var tasks = new PEventTasks();
    tasks.execute(
        4,
        t -> {
          var tiles =
              Arrays.asList(
                  new Tile(npc.getX() + 1, npc.getY() - 2, npc.getHeight()),
                  new Tile(npc.getX() - 1, npc.getY() - 1, npc.getHeight()),
                  new Tile(npc.getX() + 3, npc.getY() - 1, npc.getHeight()),
                  new Tile(npc.getX() + 3, npc.getY() - 1, npc.getHeight()),
                  new Tile(npc.getX() - 2, npc.getY() + 1, npc.getHeight()),
                  new Tile(npc.getX() + 4, npc.getY() + 1, npc.getHeight()),
                  new Tile(npc.getX() - 1, npc.getY() + 3, npc.getHeight()),
                  new Tile(npc.getX() + 3, npc.getY() + 3, npc.getHeight()),
                  new Tile(npc.getX() + 1, npc.getY() + 4, npc.getHeight()));
          var projectileSpeed = getProjectileSpeed(2).moveDelay();
          npc.getController()
              .sendMapGraphic(
                  new Tile(npc.getX() + 1, npc.getY() + 1, npc.getHeight()),
                  new Graphic(2013, 0, projectileSpeed.getContactDelay()));
          tiles.forEach(
              tile -> {
                if (npc.getController().getMapClip(tile) != 0) {
                  return;
                }
                var projectile =
                    Graphic.Projectile.builder()
                        .id(2012)
                        .startTile(npc)
                        .endTile(tile)
                        .speed(projectileSpeed)
                        .endHeight(0)
                        .build();
                sendMapProjectile(projectile);
                npc.getController()
                    .sendMapGraphic(tile, new Graphic(2014, 0, projectileSpeed.getContactDelay()));
              });
        });
    tasks.execute(
        2,
        t -> {
          deathPlayers.forEach(
              p -> {
                if (!npc.withinDistance(p, 2)) {
                  return;
                }
                p.getCombat().addHit(PRandom.randomI(40));
              });
        });
    npc.getController().addEvent(tasks);
  }

  private void findMvpPlayer() {
    var total = 0;
    for (var player : players) {
      if (player.getCombat().getDamageInflicted() <= total) {
        continue;
      }
      mvpPlayer = player;
      total = player.getCombat().getDamageInflicted();
      player.getCombat().setDamageInflicted(0);
    }
  }

  @AllArgsConstructor
  @Getter
  private enum Phase {
    SPAWN(1, -1, ""),
    SMOKE(1, -1, "Fill my soul with smoke!"),
    SMOKE_LOCKED(0.8, NpcId.FUMUS_285, "Fumus, don't fail me!"),
    SHADOW(0.8, -1, "Darken my shadow!"),
    SHADOW_LOCKED(0.6, NpcId.UMBRA_285, "Umbra, don't fail me!"),
    BLOOD(0.6, -1, "Flood my lungs with blood!"),
    BLOOD_LOCKED(0.4, NpcId.CRUOR_285, "Cruor, don't fail me!"),
    ICE(0.4, -1, "Infuse me with the power of ice!"),
    ICE_LOCKED(0.2, NpcId.GLACIES_285, "Glacies, don't fail me!"),
    ZAROS(0.2, -1, "NOW, THE POWER OF ZAROS!");

    private final double hitpointsPercent;
    private final int minionId;
    private final String phrase;

    public int getLockedMinionId() {
      for (var value : values()) {
        if (this == value) {
          continue;
        }
        if (!value.name().startsWith(name())) {
          continue;
        }
        return value.getMinionId();
      }
      return -1;
    }

    public static Phase getPhase(Phase currentPhase, double hpPercent, boolean toLocked) {
      var values = values();
      for (var value : values) {
        if (currentPhase.ordinal() >= value.ordinal()) {
          continue;
        }
        if (toLocked && hpPercent > value.hitpointsPercent) {
          continue;
        }
        if (toLocked && !value.name().endsWith("LOCKED")) {
          continue;
        }
        if (!toLocked && value.name().endsWith("LOCKED")) {
          continue;
        }
        return value;
      }
      return SMOKE;
    }
  }
}
