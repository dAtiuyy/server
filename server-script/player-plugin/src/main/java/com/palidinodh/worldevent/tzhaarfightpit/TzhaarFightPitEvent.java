package com.palidinodh.worldevent.tzhaarfightpit;

import com.google.inject.Inject;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import com.palidinodh.playerplugin.tzhaar.TzhaarFightPitMinigame;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.util.PTime;

public class TzhaarFightPitEvent extends WorldEvent {

  private static final boolean ENABLED = true;
  private static final int SOON_MINUTES = 15;
  private static final String[] TIME = {};
  private static final int[] HOURS;
  private static final int[] MINUTES;

  static {
    if (TIME == null || TIME.length == 0) {
      HOURS = null;
      MINUTES = null;
    } else {
      HOURS = new int[TIME.length];
      MINUTES = new int[TIME.length];
      for (var i = 0; i < TIME.length; i++) {
        var data = TIME[i].split(":");
        HOURS[i] = Integer.parseInt(data[0]);
        MINUTES[i] = Integer.parseInt(data[1]);
      }
    }
  }

  @Inject private transient World world;
  private transient boolean announcedStartingSoon;

  public TzhaarFightPitEvent() {
    super(4);
  }

  @Override
  public Object script(String name, Object... args) {
    boolean isPrimary = isPrimary();
    if (name.equals("world_event_name")) {
      return isPrimary ? "TzHaar Fight Pit" : null;
    }
    if (name.equals("world_event_tile")) {
      return isPrimary ? new Tile(2400, 5179) : null;
    }
    if (name.equals("fight_pit_message")) {
      return getNextTimeText();
    }
    return null;
  }

  @Override
  public void execute() {
    setTick(4);
    if (!canRun()) {
      return;
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    var dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    var nextTime = getNextTime();
    if (nextTime == null) {
      return;
    }
    var remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    if (remainingMinutes == SOON_MINUTES) {
      var message = "The TzHaar Fight Pit lobby will open in " + SOON_MINUTES + " minutes!";
      world.sendNews(message);
      DiscordBot.sendMessage(DiscordChannel.EVENTS, message);
      DiscordBot.sendMessage(DiscordChannel.GENERAL_CHAT, message);
      world.sendNews(
          "The TzHaar Fight Pit is a 'bring your own gear' minigame, and is safe for everyone.");
      announcedStartingSoon = true;
      setTick(105);
    } else if (remainingMinutes == 0) {
      startEvent();
      setTick(105);
    }
  }

  private boolean isPrimary() {
    if (!canRun()) {
      return false;
    }
    return announcedStartingSoon
        || TzhaarFightPitMinigame.isOpen() && TzhaarFightPitMinigame.getInstance().isGivePrizes();
  }

  private String getNextTimeText() {
    if (!canRun()) {
      return "N/A";
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    int dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    int[] nextTime = getNextTime();
    int remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    return PTime.ticksToLongDuration(PTime.minToTick(remainingMinutes));
  }

  private void startEvent() {
    announcedStartingSoon = false;
    if (!canRun()) {
      return;
    }
    var giveBonds = PTime.getHour24() == 17 && world.getPlayerCount() >= 50;
    TzhaarFightPitMinigame.start(true, giveBonds);
  }

  private int[] getNextTime() {
    if (!canRun()) {
      return null;
    }
    var currentHour = PTime.getHour24();
    var currentMinute = PTime.getMinute();
    for (var i = 0; i < HOURS.length; i++) {
      var hour = HOURS[i];
      var minute = MINUTES[i];
      if (currentHour > hour || currentHour == hour && currentMinute > minute) {
        continue;
      }
      return new int[] {hour, minute};
    }
    return new int[] {HOURS[0], MINUTES[0]};
  }

  public boolean canRun() {
    if (!ENABLED) {
      return false;
    }
    if (TIME == null) {
      return false;
    }
    if (TIME.length == 0) {
      return false;
    }
    return world.isPrimary();
  }
}
