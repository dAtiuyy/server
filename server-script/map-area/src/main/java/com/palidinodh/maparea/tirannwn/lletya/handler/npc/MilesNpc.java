package com.palidinodh.maparea.tirannwn.lletya.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.MILES)
class MilesNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    player.getGameEncoder().sendMessage("Miles will note items for you.");
  }

  @Override
  public void itemOnNpc(Player player, int slot, int itemId, Npc npc) {
    player.getInventory().noteItems(slot);
  }
}
