package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.STARTER_PACK_32288)
class StarterPackItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.isVoted()) {
      player.getGameEncoder().sendMessage("To open this, you first need to vote.");
      player.getGameEncoder().sendMessage("Make sure to relog after voting!");
      return;
    }
    if (player.isAltVoted()) {
      player.getGameEncoder().sendMessage("You must vote on this account to open this pack.");
      return;
    }
    item.remove();
    if (!player.getGameMode().isIronType()) {
      player.getInventory().addOrDropItem(ItemId.COINS, 400_000);
    }
    player.getInventory().addItem(ItemId.MONKFISH_NOTED, 400);
    player.getInventory().addItem(ItemId.SUPER_ATTACK_4_NOTED, 10);
    player.getInventory().addItem(ItemId.SUPER_STRENGTH_4_NOTED, 10);
    player.getInventory().addItem(ItemId.SUPER_DEFENCE_4_NOTED, 10);
    player.getInventory().addItem(ItemId.PRAYER_POTION_4_NOTED, 50);
  }
}
