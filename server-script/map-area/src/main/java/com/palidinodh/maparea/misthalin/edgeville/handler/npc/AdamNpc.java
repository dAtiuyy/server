package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.playerplugin.playstyle.PlayStylePlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.ADAM)
class AdamNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    if (option.getIndex() == 0) {
      player.openDialogue(
          new OptionsDialogue(
              new DialogueOption(
                  "Manage play style.",
                  (c, s) -> {
                    player.getWidgetManager().sendInteractiveOverlay(WidgetId.PLAY_STYLE_1011);
                  }),
              new DialogueOption(
                  "Manage group.",
                  (c, s) -> {
                    player.getPlugin(PlayStylePlugin.class).groupOptionsDialogue();
                  })));
    } else if (option.getIndex() == 2) {
      if (player.getGameMode().isIronman()) {
        player.openShop("ironman");
      } else if (player.getGameMode().isHardcoreIronman()) {
        player.openShop("hardcore_ironman");
      } else {
        player.getGameEncoder().sendMessage("Adam has no reason to trade you.");
      }
    }
  }
}
