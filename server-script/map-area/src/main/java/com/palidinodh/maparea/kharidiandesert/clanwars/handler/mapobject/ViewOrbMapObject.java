package com.palidinodh.maparea.kharidiandesert.clanwars.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.clanwars.ClanWarsPlayerState;
import com.palidinodh.playerplugin.clanwars.ClanWarsPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ObjectId.VIEWING_ORB_26741,
  ObjectId.VIEWING_ORB_26743,
  ObjectId.VIEWING_ORB_26745,
  ObjectId.VIEWING_ORB_26747,
  ObjectId.VIEWING_ORB_26749
})
class ViewOrbMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.isLocked()
        || player.getMovement().isTeleportStateStarted()
        || player.getMovement().isTeleportStateFinished()) {
      return;
    }
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    if (plugin.getState() == ClanWarsPlayerState.NONE
        || plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
        return;
      }
      player.getMovement().teleport(3105, 3492);
    } else {
      plugin.teleportViewing(2);
    }
  }
}
