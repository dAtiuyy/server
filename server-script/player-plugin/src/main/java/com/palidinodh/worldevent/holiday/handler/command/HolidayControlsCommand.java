package com.palidinodh.worldevent.holiday.handler.command;

import com.google.inject.Inject;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.worldevent.holiday.HolidayEvent;
import com.palidinodh.worldevent.holiday.HolidayType;

@ReferenceName("holidaycontrols")
class HolidayControlsCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Inject private HolidayEvent event;

  @Override
  public String getExample(String name) {
    return "(name NAME)(stop)(endshop)(spawnboss)";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    switch (messages[0]) {
      case "name":
        {
          var holidayName = messages[1];
          if (holidayName.equalsIgnoreCase("null")) {
            event.setType(null);
            player.getGameEncoder().sendMessage("Holiday event stopped.");
          } else {
            event.setType(HolidayType.valueOf(holidayName.replace(" ", "_").toUpperCase()));
            player.getGameEncoder().sendMessage("Holiday set to: " + event.getType() + ".");
          }
          break;
        }
      case "stop":
        {
          event.setType(null);
          player.getGameEncoder().sendMessage("Holiday event stopped.");
          break;
        }
      case "endshop":
        {
          event.endShop();
          break;
        }
      case "spawnboss":
        {
          event.spawnBoss();
          break;
        }
    }
    DiscordBot.sendMessage(
        DiscordChannel.MODERATION_LOG, player.getUsername() + " used ::holidaycontrols " + message);
  }
}
