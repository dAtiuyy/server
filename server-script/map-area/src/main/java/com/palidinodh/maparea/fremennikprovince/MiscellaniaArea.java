package com.palidinodh.maparea.fremennikprovince;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({9788, 10043, 10044, 10300, 10301, 10556})
public class MiscellaniaArea extends Area {}
