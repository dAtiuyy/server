package com.palidinodh.maparea.misthalin.wizardstower.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class WizardsTowerNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3113, 3169), NpcId.WIZARD_9));
    spawns.add(new NpcSpawn(4, new Tile(3114, 3162), NpcId.WIZARD_9));
    spawns.add(new NpcSpawn(4, new Tile(3106, 3156), NpcId.WIZARD_9));
    spawns.add(new NpcSpawn(4, new Tile(3112, 3157), NpcId.WIZARD_9));
    spawns.add(new NpcSpawn(4, new Tile(3109, 3158), NpcId.PROFESSOR_ONGLEWIP));

    return spawns;
  }
}
