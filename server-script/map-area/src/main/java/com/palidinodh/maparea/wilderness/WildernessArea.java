package com.palidinodh.maparea.wilderness;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Bounds;
import com.palidinodh.osrscore.model.tile.MultiBounds;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  11831, 11832, 11833, 11834, 11835, 11836, 11837, 12087, 12088, 12089, 12090, 12091, 12092, 12093,
  12343, 12344, 12345, 12346, 12347, 12348, 12349, 12599, 12600, 12601, 12602, 12603, 12604, 12605,
  12855, 12856, 12857, 12858, 12859, 12860, 12861, 13111, 13112, 13113, 13114, 13115, 13116, 13117,
  13367, 13368, 13369, 13370, 13371, 13372, 13373
})
public class WildernessArea extends Area {

  private static final MultiBounds FEROX_ENCLAVE =
      new MultiBounds(
          new Bounds(3126, 3618, 3143, 3639),
          new Bounds(3123, 3622, 3125, 3632),
          new Bounds(3125, 3633, 3125, 3639),
          new Bounds(3130, 3617, 3139, 3617),
          new Bounds(3144, 3626, 3160, 3633),
          new Bounds(3144, 3636, 3160, 3641),
          new Bounds(3144, 3619, 3150, 3625),
          new Bounds(3144, 3634, 3154, 3635),
          new Bounds(3138, 3640, 3145, 3645),
          new Bounds(3148, 3642, 3155, 3646));
  private static final MultiBounds FEROX_ENCLAVE_SAFE_ZONE =
      new MultiBounds(
          new Bounds(3129, 3610, 3140, 3616),
          new Bounds(3139, 3616, 3144, 3617),
          new Bounds(3144, 3618, 3144, 3618),
          new Bounds(3124, 3616, 3130, 3617),
          new Bounds(3124, 3618, 3125, 3621),
          new Bounds(3118, 3623, 3122, 3634),
          new Bounds(3120, 3621, 3123, 3622),
          new Bounds(3120, 3633, 3124, 3639),
          new Bounds(3122, 3640, 3137, 3643),
          new Bounds(3155, 3634, 3160, 3635));

  @Override
  public boolean inWilderness() {
    return !FEROX_ENCLAVE.contains(getTile());
  }

  @Override
  public boolean inWildernessSafeZone() {
    return FEROX_ENCLAVE_SAFE_ZONE.contains(getTile());
  }

  @Override
  public int getWildernessLevel() {
    return (getTile().getY() - 3520) / 8 + 1;
  }
}
