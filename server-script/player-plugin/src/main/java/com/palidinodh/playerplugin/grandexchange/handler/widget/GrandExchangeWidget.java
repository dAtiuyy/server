package com.palidinodh.playerplugin.grandexchange.handler.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.ViewportContainer;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.util.RequestManager;
import com.palidinodh.playerplugin.grandexchange.GrandExchangePlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PString;
import com.palidinodh.worldevent.grandexchangepawnshop.GrandExchangePawnShopEvent;

@ReferenceId(WidgetId.GRAND_EXCHANGE_1024)
class GrandExchangeWidget implements WidgetHandler {

  @Override
  public void onOpen(Player player) {
    var plugin = player.getPlugin(GrandExchangePlugin.class);
    plugin.sendItems();
    player.getWidgetManager().closeKeyboardScript();
    player
        .getWidgetManager()
        .sendWidget(ViewportContainer.INVENTORY, WidgetId.GRAND_EXCHANGE_INVENTORY);
  }

  @Override
  public void onClose(Player player) {
    player.getWidgetManager().sendWidget(ViewportContainer.INVENTORY, WidgetId.INVENTORY);
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var plugin = player.getPlugin(GrandExchangePlugin.class);
    switch (childId) {
      case 17:
        plugin.viewHistory();
        break;
      case 23:
        plugin.getOffers().collectAll(option);
        break;
      case 26:
        {
          if (player.getGameMode().isIronType()) {
            break;
          }
          plugin.getOffers().create(false, -1);
          break;
        }
      case 29:
        {
          if (player.getGameMode().isIronType()) {
            break;
          }
          plugin.getOffers().create(true, -1);
          break;
        }
      case 32:
        {
          var pawnShopEvent = player.getWorld().getWorldEvent(GrandExchangePawnShopEvent.class);
          if (!pawnShopEvent.isEnabled()) {
            player.getGameEncoder().sendMessage("The pawn shop is currently closed.");
            break;
          }
          player.openShop(pawnShopEvent.getShop());
          break;
        }
      case 35:
        {
          if (player.getGameMode().isIronType()) {
            break;
          }
          player.openOptionsDialogue(
              new DialogueOption(
                  "Search by name",
                  (c, s) -> {
                    player
                        .getGameEncoder()
                        .sendItemSearch(
                            "What would you like to search?",
                            i -> {
                              player.putAttribute("exchange_item_id", i + 1);
                              player.putAttribute("exchange_item_name", "");
                              player.putAttribute("exchange_username", "");
                              RequestManager.getInstance().addGEHistory(player);
                            });
                  }),
              new DialogueOption(
                  "Search by phrase",
                  (c, s) -> {
                    player
                        .getGameEncoder()
                        .sendEnterString(
                            "What would you like to search?",
                            e -> {
                              player.putAttribute("exchange_item_id", 0);
                              player.putAttribute(
                                  "exchange_item_name",
                                  PString.cleanName(e.replace(" ", "").toLowerCase()));
                              player.putAttribute("exchange_username", "");
                              RequestManager.getInstance().addGEHistory(player);
                            });
                  }));
          player.getDialogue().setKeepWidgets(true);
          break;
        }
      case 38:
        {
          if (player.getGameMode().isIronType()) {
            break;
          }
          player
              .getGameEncoder()
              .sendEnterString(
                  "What user would you like to view?",
                  s -> {
                    player.putAttribute("exchange_item_id", 0);
                    player.putAttribute("exchange_item_name", "");
                    player.putAttribute("exchange_username", s);
                    RequestManager.getInstance().addGEHistory(player);
                  });
          break;
        }
      case 41:
        {
          if (player.getGameMode().isIronType()) {
            break;
          }
          player.putAttribute("exchange_item_id", 0);
          player.putAttribute("exchange_item_name", "");
          player.putAttribute("exchange_username", "-random_username-");
          RequestManager.getInstance().addGEHistory(player);
          break;
        }
    }
    if (childId >= GrandExchangePlugin.WIDGET_ITEM_1) {
      var itemSlot =
          (childId - GrandExchangePlugin.WIDGET_ITEM_1) / GrandExchangePlugin.WIDGET_ITEM_CHILDREN;
      if (option == 0) {
        plugin.getOffers().view(itemSlot);
      } else if (option == 1) {
        plugin.getOffers().abort(itemSlot);
      }
    }
    plugin.sendItems();
  }
}
