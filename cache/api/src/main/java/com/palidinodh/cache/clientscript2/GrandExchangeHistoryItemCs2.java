package com.palidinodh.cache.clientscript2;

import com.palidinodh.cache.id.ScriptId;
import lombok.Getter;

@Getter
public class GrandExchangeHistoryItemCs2 extends ClientScript2 {

  private int itemSlot;
  private int type;
  private int itemId;
  private int itemQuantity;
  private String username;
  private int itemPrice;

  public GrandExchangeHistoryItemCs2() {
    super(ScriptId.GRAND_EXCHANGE_HISTORY_8200);
  }

  public static GrandExchangeHistoryItemCs2 builder() {
    return new GrandExchangeHistoryItemCs2();
  }

  public GrandExchangeHistoryItemCs2 itemSlot(int i) {
    itemSlot = i;
    return this;
  }

  public GrandExchangeHistoryItemCs2 type(int i) {
    type = i;
    return this;
  }

  public GrandExchangeHistoryItemCs2 itemId(int i) {
    itemId = i;
    return this;
  }

  public GrandExchangeHistoryItemCs2 itemQuantity(int i) {
    itemQuantity = i;
    return this;
  }

  public GrandExchangeHistoryItemCs2 username(String s) {
    username = s;
    return this;
  }

  public GrandExchangeHistoryItemCs2 itemPrice(int i) {
    itemPrice = i;
    return this;
  }

  @Override
  public byte[] build() {
    addInt(itemSlot);
    addInt(type);
    addInt(itemId);
    addInt(itemQuantity);
    addString(username);
    addInt(itemPrice);
    return super.build();
  }
}
