package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.BONECRUSHER, ItemId.BONECRUSHER_NECKLACE})
class BonecrusherItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "check":
        player
            .getGameEncoder()
            .sendMessage(
                "Your bonecrusher has " + player.getCharges().getBoneCrusherCharges() + " charges");
        if (player.getCharges().getBoneCrusherCrush()) {
          player.getGameEncoder().sendMessage("Your bonecrusher is currently active");
        } else if (!player.getCharges().getBoneCrusherCrush()) {
          player.getGameEncoder().sendMessage("Your bonecrusher is currently inactive");
        }
        break;
      case "activity":
        player.getCharges().setBoneCrusherCrush(!player.getCharges().getBoneCrusherCrush());
        if (player.getCharges().getBoneCrusherCrush()) {
          player.getGameEncoder().sendMessage("Your bonecrusher is now active");
        } else if (!player.getCharges().getBoneCrusherCrush()) {
          player.getGameEncoder().sendMessage("Your bonecrusher is now inactive");
        }
        break;
      case "uncharge":
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you want to uncharge your bonecrusher?",
                new DialogueOption(
                    "Yes, uncharge my bonecrusher!",
                    (c, s) -> {
                      player.getCharges().setBoneCrusherCharges(0);
                      player
                          .getGameEncoder()
                          .sendMessage(
                              "You uncharge your bonecrusher. It now has "
                                  + player.getCharges().getBoneCrusherCharges()
                                  + " charges.");
                    }),
                new DialogueOption("No!")));
        break;
      case "dismantle":
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you wish to dismantle your Bonecrusher Necklace into<br> a Hydra Tail, Dragonbone Necklace and Bonecrusher?",
                new DialogueOption(
                    "Yes, dismantle my bonecrusher necklace!",
                    (c, s) -> {
                      player.getInventory().deleteItem(ItemId.BONECRUSHER_NECKLACE);
                      player.getInventory().addOrDropItem(ItemId.BONECRUSHER, 1);
                      player.getInventory().addOrDropItem(ItemId.DRAGONBONE_NECKLACE, 1);
                      player.getInventory().addOrDropItem(ItemId.HYDRA_TAIL, 1);
                      player
                          .getGameEncoder()
                          .sendMessage("You dismantle your bonecrusher necklace.");
                    }),
                new DialogueOption("No!")));
        break;
    }
  }
}
