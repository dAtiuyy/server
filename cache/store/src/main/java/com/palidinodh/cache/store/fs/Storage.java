package com.palidinodh.cache.store.fs;

import java.io.IOException;

public interface Storage extends AutoCloseable {

  void init(Store store) throws IOException;

  @Override
  void close() throws IOException;

  void load(Store store) throws IOException;

  void save(Store store) throws IOException;

  byte[] loadArchive(Archive archive) throws IOException;

  void saveArchive(Archive archive, byte[] data) throws IOException;

  byte[] readIndex(int indexId) throws IOException;
}
