package com.palidinodh.cache.store.fs;

import com.palidinodh.cache.store.index.FileData;
import com.palidinodh.cache.store.util.Stream;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ArchiveFiles {

  private final List<FsFile> files = new LinkedList<>();
  private final Map<Integer, FsFile> fileMap = new HashMap<>();

  @Override
  public int hashCode() {
    var hash = 7;
    hash = 67 * hash + Objects.hashCode(files);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    var other = (ArchiveFiles) obj;
    if (!Objects.equals(files, other.files)) {
      return false;
    }
    return true;
  }

  public void addFile(FsFile file) {
    if (file.getFileId() == -1) {
      throw new IllegalArgumentException("File id is -1");
    }
    if (fileMap.containsKey(file.getFileId())) {
      throw new IllegalStateException("Duplicate file id: " + file.getFileId());
    }
    files.add(file);
    fileMap.put(file.getFileId(), file);
  }

  public void updateFile(FileData file) {
    if (file.getId() == -1) {
      throw new IllegalArgumentException("File id is -1");
    }
    var existingFile = getFile(file.getId());
    if (existingFile == null && file.getContents() == null) {
      return;
    }
    if (existingFile != null && file.getContents() == null) {
      files.remove(existingFile);
      fileMap.remove(existingFile.getFileId());
      return;
    }
    if (existingFile == null) {
      if (file.getContents() == null) {
        throw new RuntimeException("Contents of file " + file.getId() + " is null");
      }
      addFile(new FsFile(file.getId(), file.getNameHash(), file.getContents()));
      files.sort(Comparator.comparingInt(FsFile::getFileId));
      return;
    }
    existingFile.setNameHash(file.getNameHash());
    existingFile.setContents(file.getContents());
  }

  public List<FsFile> getFiles() {
    return Collections.unmodifiableList(files);
  }

  public FsFile getFile(int fileId) {
    return fileMap.get(fileId);
  }

  public void clear() {
    files.clear();
    fileMap.clear();
  }

  public void loadContents(byte[] data) {
    if (files.isEmpty()) {
      return;
    }
    if (files.size() == 1) {
      files.get(0).setContents(data);
      return;
    }
    var filesCount = files.size();
    var stream = new Stream(data);
    stream.setPosition(stream.getLength() - 1);
    var chunks = stream.readUnsignedByte();
    stream.setPosition(stream.getLength() - 1 - chunks * filesCount * 4);
    var chunkSizes = new int[filesCount][chunks];
    var filesSize = new int[filesCount];
    for (var chunk = 0; chunk < chunks; ++chunk) {
      var chunkSize = 0;
      for (var id = 0; id < filesCount; ++id) {
        var delta = stream.readInt();
        chunkSize += delta;
        chunkSizes[id][chunk] = chunkSize;
        filesSize[id] += chunkSize;
      }
    }
    var fileContents = new byte[filesCount][];
    var fileOffsets = new int[filesCount];
    for (var i = 0; i < filesCount; ++i) {
      fileContents[i] = new byte[filesSize[i]];
    }
    stream.setPosition(0);
    for (var chunk = 0; chunk < chunks; ++chunk) {
      for (var id = 0; id < filesCount; ++id) {
        var chunkSize = chunkSizes[id][chunk];
        stream.readBytes(fileContents[id], fileOffsets[id], chunkSize);
        fileOffsets[id] += chunkSize;
      }
    }
    for (var i = 0; i < filesCount; ++i) {
      var f = files.get(i);
      f.setContents(fileContents[i]);
    }
  }

  public byte[] saveContents() {
    var stream = new Stream();
    var filesCount = files.size();
    if (filesCount == 1) {
      var file = files.get(0);
      stream.writeBytes(file.getContents());
    } else {
      for (var file : files) {
        var contents = file.getContents();
        stream.writeBytes(contents);
      }
      var offset = 0;
      for (var file : files) {
        var chunkSize = file.getSize();
        var sz = chunkSize - offset;
        offset = chunkSize;
        stream.writeInt(sz);
      }
      stream.writeByte(1);
    }
    var fileData = stream.toByteArray();
    return fileData;
  }
}
