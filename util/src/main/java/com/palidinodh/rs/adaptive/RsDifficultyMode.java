package com.palidinodh.rs.adaptive;

import com.palidinodh.util.PString;

public enum RsDifficultyMode {
  UNSET,
  NORMAL,
  HARD,
  ELITE;

  public String getFormattedName() {
    return PString.formatName(name().toLowerCase().replace("_", " "));
  }

  public boolean isSet() {
    return this != UNSET;
  }

  public boolean isUnset() {
    return this == UNSET;
  }

  public boolean isNormal() {
    return this == NORMAL;
  }

  public boolean isHard() {
    return this == HARD;
  }

  public boolean isElite() {
    return this == ELITE;
  }
}
