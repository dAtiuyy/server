package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class GrayBlackCatPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.PET_CAT, NpcId.CAT_6662, NpcId.CAT));
    return builder;
  }
}
