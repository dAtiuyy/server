package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(-1)
class UnarmedWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_0);
    type.attackSpeed(4);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(422).attackSound(new Sound(2566)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(423)
            .build());
    return type;
  }
}

@ReferenceId(ItemId.EVENT_RPG)
class EventRpgWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_0);
    type.renderAnimations(new int[] {2316, 2321, 2317, 2318, 2319, 2320, 2322});
    type.twoHanded(true);
    type.attackSpeed(3);
    type.defendAnimation(2324);
    type.attackSet(WeaponAttackSet.builder().attackAnimation(2323).build());
    return type;
  }
}

@ReferenceId(ItemId.SLED)
class SledWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_0);
    type.renderAnimations(new int[] {1461, 1468, 1468, 1468, 1468, 1468, 1467});
    type.twoHanded(true);
    type.attackSpeed(4);
    type.attackSet(WeaponAttackSet.builder().build());
    return type;
  }
}

@ReferenceId(ItemId.ALE_OF_THE_GODS)
class AleOfTheGodsWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_0);
    type.renderAnimations(new int[] {3040, 3039, 3039, 3039, 3039, 3039, 824});
    type.attackSpeed(4);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(422).attackSound(new Sound(2566)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(423)
            .build());
    return type;
  }
}

@ReferenceId(ItemId.CLUELESS_SCROLL)
class CluelessScrollWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_0);
    type.renderAnimations(new int[] {7271, 7272, 7272, 7272, 7272, 7272, 7273});
    type.attackSpeed(4);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(422).attackSound(new Sound(2566)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_CRUSH)
            .attackAnimation(423)
            .build());
    return type;
  }
}
