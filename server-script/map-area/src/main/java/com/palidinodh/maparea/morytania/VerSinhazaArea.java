package com.palidinodh.maparea.morytania;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.agility.MoveAgilityEvent;
import com.palidinodh.playerplugin.boss.BossInstance;
import com.palidinodh.playerplugin.boss.BossInstanceController;
import com.palidinodh.playerplugin.boss.BossPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PEventTasks;

@ReferenceId({14641, 14642, 14898})
public class VerSinhazaArea extends Area {
  private static final Tile CENTER_LOBBY = new Tile(3680, 3232);
  private static final Tile DAWNBRINGER_TILE = new Tile(3166, 4303);

  public static void joinPublic(Player player) {
    var verzikVitur = BossPlugin.getVerzikVitur(player);
    if (verzikVitur != null
        && verzikVitur.isVisible()
        && verzikVitur.getId() != BossPlugin.VERZIK_SPAWN.getId()) {
      player.openDialogue(
          new MessageDialogue(
              "A group is already fighting Verzik Vitur. You'll have to wait until they are done."));
      return;
    }
    var controller = new BossInstanceController();
    controller.setKeepExitTileOnDeath(true);
    controller.setTeleportsDisabled(true);
    player.setController(controller);
    enter(player);
    if (verzikVitur == null) {
      startInstance(player);
    }
    for (var x = 3167; x <= 3169; x++) {
      player
          .getController()
          .addMapObject(new MapObject(ObjectId.BARRIER_32755, 10, 0, new Tile(x, 4302)));
    }
  }

  public static void joinPrivate(Player player) {
    var playerInstance =
        player
            .getWorld()
            .getPlayerBossInstance(
                player.getMessaging().getClanChatUsername(), player.getController());
    if (playerInstance == null
        || !playerInstance.is(BossInstanceController.class)
        || playerInstance.getEntity() == null
        || !playerInstance.getEntity().isPlayer()) {
      playerInstance = null;
    }
    if (playerInstance == null) {
      if (!BossInstance.canStart(player, NpcId.VERZIK_VITUR_1040, true)) {
        return;
      }
    } else {
      if (!BossInstance.canJoin(player, NpcId.VERZIK_VITUR_1040, true)) {
        return;
      }
      var verzikVitur = BossPlugin.getVerzikVitur(playerInstance.getEntity().asPlayer());
      if (verzikVitur != null
          && verzikVitur.isVisible()
          && verzikVitur.getId() != BossPlugin.VERZIK_SPAWN.getId()) {
        player.openDialogue(
            new MessageDialogue(
                "A group is already fighting Verzik Vitur. You'll have to wait until they are done."));
        return;
      }
    }
    var controller = new BossInstanceController();
    player.setController(controller);
    controller.setRespawnBoss(false);
    controller.setKeepExitTileOnDeath(true);
    controller.setTeleportsDisabled(true);
    if (playerInstance != null) {
      controller.joinInstance(playerInstance);
    }
    enter(player);
    if (playerInstance == null) {
      controller.startInstance();
      player
          .getController()
          .as(BossInstanceController.class)
          .setBossInstance(NpcId.VERZIK_VITUR_1040, false, null);
      startInstance(player);
      player
          .getWorld()
          .putPlayerBossInstance(
              player.getMessaging().getClanChatUsername(), player.getController());
    }
    for (var x = 3167; x <= 3169; x++) {
      player
          .getController()
          .addMapObject(new MapObject(ObjectId.BARRIER_32755, 10, 0, new Tile(x, 4302)));
    }
  }

  private static void enter(Player player) {
    player.getGameEncoder().setVarbit(VarbitId.THEATRE_OF_BLOOD, 0);
    player.getPlugin(BossPlugin.class).setPoints(8);
    player.lock();
    var moveEvent = MoveAgilityEvent.builder().tile(new Tile(3168, 4303)).noclip(true).build();
    var tasks = new PEventTasks();
    tasks.execute(
        1,
        t -> {
          player.getWidgetManager().sendOverlay(WidgetId.THEATRE_OF_BLOOD_OVERLAY);
          player.getGameEncoder().setVarbit(VarbitId.THEATRE_OF_BLOOD, 3);
          player.getGameEncoder().setVarbit(VarbitId.THEATRE_OF_BLOOD_PARTY_DISPLAY, 1);
          player.getGameEncoder().setVarbit(VarbitId.THEATRE_OF_BLOOD_PARTY_HP_1, 1);
          player.getGameEncoder().setVarbit(VarbitId.THEATRE_OF_BLOOD_HP_TYPE, 0);
          player.getGameEncoder().sendClientScript(ScriptId.TOB_HUD_PORTAL, "");
        });
    tasks.execute(
        2,
        t -> {
          player.getMovement().teleport(new Tile(BossPlugin.VERZIK_ENTER_TILE));
          player.getGameEncoder().setVarbit(VarbitId.THEATRE_OF_BLOOD, 2);
          player
              .getGameEncoder()
              .sendClientScript(ScriptId.TOB_HUD_FADE, 255, 0, "The Final Challenge");
        });
    tasks.condition(
        t -> {
          return moveEvent.complete(player);
        });
    tasks.execute(
        t -> {
          player.unlock();
        });
    player.getController().addEvent(tasks);
  }

  private static void startInstance(Player player) {
    player.getController().removeMapItems(BossPlugin.VERZIK_SPAWN.getTile().getRegionId());
    player.getController().addNpc(BossPlugin.VERZIK_SPAWN);
    player.getController().addMapItem(new Item(ItemId.DAWNBRINGER), DAWNBRINGER_TILE, player);
  }
}
