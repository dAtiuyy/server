package com.palidinodh.playerplugin.treasuretrail;

import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.Scroll;
import com.palidinodh.osrscore.model.item.clue.ClueChestSet;
import com.palidinodh.osrscore.model.item.clue.ClueChestSetEntry;
import com.palidinodh.osrscore.model.item.clue.ClueChestType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import lombok.Setter;

public class TreasureChest {

  @Setter private transient Player player;

  private Map<String, ClueChestSetEntry> beginner;
  private Map<String, ClueChestSetEntry> easy;
  private Map<String, ClueChestSetEntry> medium;
  private Map<String, ClueChestSetEntry> hard;
  private Map<String, ClueChestSetEntry> elite;
  private Map<String, ClueChestSetEntry> master;

  public void open(ClueChestType type) {
    var chest = ClueChestSet.getClueChest(type);
    var myChest = getChest(type);
    var names = new ArrayList<String>();
    for (var set : chest.entrySet()) {
      var name = set.getKey();
      if (myChest.containsKey(name)) {
        name = "<str>" + name + "</str>";
      }
      names.add(name);
    }
    Scroll.open(player, type.getFormattedName() + "-level Treasure Trail rewards", names);
  }

  public void addItem(int id) {
    if (hasId(id)) {
      player.getGameEncoder().sendMessage("Your treasure chest already contains this item.");
      return;
    }
    for (var chestType : ClueChestType.values()) {
      var chest = ClueChestSet.getClueChest(chestType);
      var myChest = getChest(chestType);
      for (var set : chest.entrySet()) {
        if (!set.getValue().containsId(id)) {
          continue;
        }
        var matches = new ArrayList<Integer>();
        for (var entry : set.getValue().getEntries()) {
          var matchId = entry.getMatchId(player.getInventory());
          if (matchId == -1) {
            continue;
          }
          matches.add(matchId);
        }
        if (matches.size() != set.getValue().size()) {
          player.getGameEncoder().sendMessage("You need the complete set to store this item.");
          break;
        }
        for (var matchedId : matches) {
          player.getInventory().deleteItem(matchedId);
        }
        myChest.put(set.getKey(), new ClueChestSetEntry(matches));
        player.getGameEncoder().sendMessage("Your item has been stored in the treasure chest.");
      }
    }
  }

  public boolean hasId(int id) {
    var chests = Arrays.asList(beginner, easy, medium, hard, elite, master);
    for (var chest : chests) {
      if (chest == null) {
        continue;
      }
      for (var entry : chest.values()) {
        if (entry.containsId(id)) {
          return true;
        }
      }
    }
    return false;
  }

  private Map<String, ClueChestSetEntry> getChest(ClueChestType type) {
    if (type == ClueChestType.BEGINNER) {
      return beginner == null ? beginner = new HashMap<>() : beginner;
    }
    if (type == ClueChestType.EASY) {
      return easy == null ? easy = new HashMap<>() : easy;
    }
    if (type == ClueChestType.MEDIUM) {
      return medium == null ? medium = new HashMap<>() : medium;
    }
    if (type == ClueChestType.HARD) {
      return hard == null ? hard = new HashMap<>() : hard;
    }
    if (type == ClueChestType.ELITE) {
      return elite == null ? elite = new HashMap<>() : elite;
    }
    if (type == ClueChestType.MASTER) {
      return master == null ? master = new HashMap<>() : master;
    }
    return null;
  }
}
