package com.palidinodh.playerplugin.slayer.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.SLAYER_RING_8,
  ItemId.SLAYER_RING_7,
  ItemId.SLAYER_RING_6,
  ItemId.SLAYER_RING_5,
  ItemId.SLAYER_RING_4,
  ItemId.SLAYER_RING_3,
  ItemId.SLAYER_RING_2,
  ItemId.SLAYER_RING_1,
  ItemId.SLAYER_RING_ETERNAL
})
class SlayerRingItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    switch (option.getText()) {
      case "rub":
      case "teleport":
        {
          var slot = item.getSlot();
          if (item.getContainer() == player.getEquipment()) {
            slot += 65536;
          }
          if (plugin.getTask().isComplete()) {
            player.getGameEncoder().sendMessage("You need a task to do this.");
            return;
          }
          var teleports = plugin.getTask().getSlayerTask().getTeleports();
          if (teleports == null || teleports.isEmpty()) {
            player
                .getGameEncoder()
                .sendMessage("There are no teleports associated with this task.");
            return;
          }
          player.openDialogue(new TeleportsDialogue(player, slot));
          break;
        }
      case "check":
        player.openDialogue(
            new OptionsDialogue(
                "Which task would you like to check?",
                new DialogueOption("Normal task!", (c, s) -> plugin.sendTask(plugin.getTask())),
                new DialogueOption(
                    "Wilderness task!", (c, s) -> plugin.sendTask(plugin.getWildernessTask()))));
        break;
    }
  }

  private static class TeleportsDialogue extends OptionsDialogue {

    TeleportsDialogue(Player player, int ringSlot) {
      var plugin = player.getPlugin(SlayerPlugin.class);
      var teleports = plugin.getTask().getSlayerTask().getTeleports();
      for (var teleport : teleports) {
        addOption(teleport.getName());
      }
      if (teleports.size() == 1) {
        addOption("Nevermind");
      }
      action(
          (c, s) -> {
            if (plugin.getTask().isComplete()) {
              player.getGameEncoder().sendMessage("You need a task to do this.");
              return;
            }
            if (plugin.getTask().getSlayerTask().getTeleports() == null
                || plugin.getTask().getSlayerTask().getTeleports().isEmpty()) {
              player
                  .getGameEncoder()
                  .sendMessage("There are no teleports associated with this task.");
              return;
            }
            if (s >= plugin.getTask().getSlayerTask().getTeleports().size()) {
              return;
            }
            if (!player.getController().canTeleport(30, true)) {
              return;
            }
            Item ringItem;
            if (ringSlot >= 65536) {
              ringItem = player.getEquipment().getItem(ringSlot - 65536);
            } else {
              ringItem = player.getInventory().getItem(ringSlot);
            }
            if (ringItem == null) {
              player.getGameEncoder().sendMessage("Unable to locate your ring.");
              return;
            }
            var ringIds =
                new int[] {
                  ItemId.SLAYER_RING_8,
                  ItemId.SLAYER_RING_7,
                  ItemId.SLAYER_RING_6,
                  ItemId.SLAYER_RING_5,
                  ItemId.SLAYER_RING_4,
                  ItemId.SLAYER_RING_3,
                  ItemId.SLAYER_RING_2,
                  ItemId.SLAYER_RING_1,
                  -1
                };
            var newRingId = -2;
            for (var i = 0; i < ringIds.length; i++) {
              if (ringItem.getId() == ringIds[i]) {
                newRingId = ringIds[i + 1];
                break;
              }
            }
            if (newRingId != -2) {
              if (ringSlot >= 65536) {
                player.getEquipment().setItem(ringSlot - 65536, new Item(newRingId, ringItem));
              } else {
                player.getInventory().setItem(ringSlot, new Item(newRingId, ringItem));
              }
            }
            player
                .getMagic()
                .standardTeleport(plugin.getTask().getSlayerTask().getTeleports().get(s).getTile());
            player.getController().stopWithTeleport();
            player.getCombat().clearHitEvents();
          });
    }
  }
}
