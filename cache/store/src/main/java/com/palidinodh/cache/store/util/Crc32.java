package com.palidinodh.cache.store.util;

import java.util.zip.CRC32;

public final class Crc32 {

  private static final CRC32 crc32 = new CRC32();

  private Crc32() {}

  public static void reset() {
    crc32.reset();
  }

  public static void update(byte[] data) {
    crc32.update(data, 0, data.length);
  }

  public static void update(byte[] data, int offset, int length) {
    crc32.update(data, offset, length);
  }

  public static int getRoughHash() {
    var value = (int) crc32.getValue();
    crc32.reset();
    return value;
  }

  public static int getRoughHash(byte[] data) {
    return getRoughHash(data, 0, data.length);
  }

  public static int getRoughHash(byte[] data, int offset, int length) {
    reset();
    update(data, offset, length);
    return getRoughHash();
  }

  public static long getHash() {
    var value = crc32.getValue();
    crc32.reset();
    return value;
  }

  public static long getHash(byte[] data) {
    return getRoughHash(data, 0, data.length);
  }

  public static long getHash(byte[] data, int offset, int length) {
    reset();
    update(data, offset, length);
    return getHash();
  }
}
