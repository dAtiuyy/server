package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.TileHitEvent;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.route.AttackRoute;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class DemonicGorillaCombat extends NpcCombat {

  @Inject private Npc npc;
  private HitStyleType attackStyle;
  private int lastStyleChange;
  private int misses;
  private int damageTaken;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .rareDropTableDenominator(100)
            .clue(ClueScrollType.ELITE, 500)
            .clue(ClueScrollType.HARD, 100);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(1500).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.HEAVY_BALLISTA)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(750).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LIGHT_BALLISTA)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(300).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ZENYTE_SHARD)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PAPAYA_TREE_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PALM_TREE_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WILLOW_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAPLE_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YEW_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SPIRIT_SEED, 2)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BOLTS, 102, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_SCIMITAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHARK, 2, 3)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_DWARF_WEED_NOTED, 7, 12)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATERMELON_SEED, 30)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RANARR_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPDRAGON_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORSTOL_SEED, 1, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DIAMOND_NOTED, 4, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_BAR_NOTED, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BAR_NOTED, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_JAVELIN_HEADS, 31, 55)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_JAVELIN_HEADS, 5, 43)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATELEGS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATESKIRT)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_CHAINBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 50, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 50, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_POTION_3, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SARADOMIN_BREW_2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM_NOTED, 7, 12)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE_NOTED, 7, 12)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME_NOTED, 7, 12)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 5366, 9991)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JAVELIN_SHAFT, 266, 1238)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MALICIOUS_ASHES)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat
        .id(NpcId.DEMONIC_GORILLA_275)
        .id(NpcId.DEMONIC_GORILLA_275_7145)
        .id(NpcId.DEMONIC_GORILLA_275_7146);
    combat.hitpoints(NpcCombatHitpoints.total(380));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(205)
            .magicLevel(195)
            .rangedLevel(195)
            .defenceLevel(200)
            .bonus(BonusType.MELEE_ATTACK, 43)
            .bonus(BonusType.ATTACK_MAGIC, 40)
            .bonus(BonusType.ATTACK_RANGED, 43)
            .bonus(BonusType.MELEE_DEFENCE, 50)
            .bonus(BonusType.DEFENCE_MAGIC, 50)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.immunity(NpcCombatImmunity.builder().venom(true).build());
    combat.killCount(NpcCombatKillCount.SAVE);
    combat.type(NpcCombatType.DEMON).deathAnimation(7229).blockAnimation(7224);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(7226).attackSpeed(5);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(7227).attackSpeed(5);
    style.targetGraphic(new Graphic(1303));
    style.projectile(NpcCombatProjectile.id(1302));
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(7225).attackSpeed(5);
    style.targetGraphic(new Graphic(1305));
    style.projectile(NpcCombatProjectile.id(1304));
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    lastStyleChange = 0;
    misses = 0;
    damageTaken = 0;
    chooseProtectionPrayer(
        HitStyleType.getRandomType(HitStyleType.MELEE, HitStyleType.RANGED, HitStyleType.MAGIC));
    attackStyle =
        HitStyleType.getRandomType(HitStyleType.MELEE, HitStyleType.RANGED, HitStyleType.MAGIC);
  }

  @Override
  public void tickStartHook() {
    if (!isAttacking() && !inRecentCombat() && lastStyleChange++ >= 50) {
      chooseAttackStyle();
    }
    checkBoulderAttack();
  }

  @Override
  public void tickEndHook() {
    if (isAttacking() && attackStyle == HitStyleType.MELEE && getHitDelay() < -10) {
      chooseAttackStyle();
    }
  }

  @Override
  public HitStyleType attackTickHitStyleTypeHook(HitStyleType hitStyleType, Entity opponent) {
    return attackStyle;
  }

  @Override
  public void attackTickEndHook(NpcCombatStyle combatStyle, Entity opponent) {
    lastStyleChange = 0;
  }

  @Override
  public void attackedActionsOpponentHook(Entity opponent) {
    if (!opponent.isPlayer() || !opponent.getArea().inRevenantCaves()) {
      return;
    }
    opponent.asPlayer().getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (npc.getArea().inWilderness()
        && PRandom.inRange(
            1, player.getCombat().getDropRateDenominator(5000, ItemId.DRAGON_CLAWS))) {
      npc.getController().addMapItem(new Item(ItemId.DRAGON_CLAWS), dropTile, player);
    }
  }

  @Override
  public int deathDropItemsRollsHook(Player player, int rolls) {
    if (player.getPlugin(SlayerPlugin.class).isWildernessTask(npc)) {
      return rolls + 1;
    }
    return rolls;
  }

  @Override
  public double damageInflictedHook(NpcCombatStyle combatStyle, Entity opponent, double damage) {
    if (damage <= 0 && ++misses >= 3) {
      chooseAttackStyle();
    }
    return damage;
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    damage = filterDamage(damage, hitStyleType);
    if (isDead() || damage <= 0 || !opponent.isPlayer()) {
      return damage;
    }
    if (hitStyleType != HitStyleType.MELEE
        && hitStyleType != HitStyleType.RANGED
        && hitStyleType != HitStyleType.MAGIC) {
      return damage;
    }
    damageTaken += damage;
    if (damageTaken >= 50) {
      chooseProtectionPrayer(hitStyleType);
    }
    return damage;
  }

  private double filterDamage(double damage, HitStyleType hitStyleType) {
    if (npc.getId() == NpcId.DEMONIC_GORILLA_275 && hitStyleType == HitStyleType.MELEE) {
      return 0;
    }
    if (npc.getId() == NpcId.DEMONIC_GORILLA_275_7145 && hitStyleType == HitStyleType.RANGED) {
      return 0;
    }
    if (npc.getId() == NpcId.DEMONIC_GORILLA_275_7146 && hitStyleType == HitStyleType.MAGIC) {
      return 0;
    }
    return damage;
  }

  private void chooseProtectionPrayer(HitStyleType hitStyleType) {
    damageTaken = 0;
    if (hitStyleType == HitStyleType.MELEE) {
      npc.setId(NpcId.DEMONIC_GORILLA_275);
    } else if (hitStyleType == HitStyleType.RANGED) {
      npc.setId(NpcId.DEMONIC_GORILLA_275_7145);
    } else if (hitStyleType == HitStyleType.MAGIC) {
      npc.setId(NpcId.DEMONIC_GORILLA_275_7146);
    }
  }

  private void chooseAttackStyle() {
    misses = 0;
    lastStyleChange = 0;
    if (attackStyle == HitStyleType.MELEE) {
      attackStyle = HitStyleType.getRandomType(HitStyleType.RANGED, HitStyleType.MAGIC);
    } else if (attackStyle == HitStyleType.RANGED) {
      attackStyle = HitStyleType.getRandomType(HitStyleType.MELEE, HitStyleType.MAGIC);
    } else if (attackStyle == HitStyleType.MAGIC) {
      attackStyle = HitStyleType.getRandomType(HitStyleType.MELEE, HitStyleType.RANGED);
    }
  }

  private void checkBoulderAttack() {
    if (!isAttacking()) {
      return;
    }
    if (isHitDelayed()) {
      return;
    }
    if (attackStyle == HitStyleType.MELEE) {
      return;
    }
    if (PRandom.randomE(5) != 0) {
      return;
    }
    var attackingEntity = getAttackingEntity();
    if (!canAttackEntity(attackingEntity)) {
      return;
    }
    if (!npc.withinDistance(attackingEntity, 10)) {
      return;
    }
    if (!AttackRoute.allow(npc, attackingEntity, true)) {
      return;
    }
    addEvent(new TileHitEvent(4, npc.getController(), attackingEntity, 30, HitStyleType.TYPELESS));
    npc.getController().sendMapGraphic(attackingEntity, new Graphic(71, 0, 126));
    var tile = new Tile(attackingEntity);
    tile.setY(tile.getY() - 1);
    npc.getController().sendMapProjectile(null, tile, attackingEntity, 856, 200, 1, 0, 126, 16, 64);
    npc.setAnimation(7228);
    setHitDelay(5);
  }
}
