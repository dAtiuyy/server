package com.palidinodh.playerplugin.grandexchange.handler.widget;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.ViewportContainer;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.playerplugin.grandexchange.GrandExchangePlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.GRAND_EXCHANGE_OFFER_1026)
class GrandExchangeOfferWidget implements WidgetHandler {

  private static void sendItemSearch(Player player) {
    player
        .getGameEncoder()
        .sendItemSearch(
            "What would you like to buy?",
            id -> {
              player.putAttribute("exchange_item_id", id + 1);
              player.putAttribute("exchange_item_price", ItemDef.getConfiguredExchangePrice(id));
              player.getPlugin(GrandExchangePlugin.class).getOffers().loadCreated();
            });
  }

  private static void changeQuantity(Player player, int change, boolean set) {
    var id = player.getAttributeInt("exchange_item_id") - 1;
    var quantity = player.getAttributeInt("exchange_item_quantity");
    var maxQuantity = Item.MAX_AMOUNT;
    if (player.getAttributeBool("exchange_sell")) {
      maxQuantity =
          player.getInventory().getCount(id, ItemDefinition.getDefinition(id).getNotedId());
    }
    if (set) {
      quantity = change;
    } else {
      quantity += change;
    }
    quantity = Math.max(quantity, 1);
    quantity = Math.min(quantity, maxQuantity);
    player.putAttribute("exchange_item_quantity", quantity);
    player.getPlugin(GrandExchangePlugin.class).getOffers().loadCreated();
  }

  private static void changePrice(Player player, int change, boolean percent, boolean set) {
    var id = player.getAttributeInt("exchange_item_id") - 1;
    var price = player.getAttributeInt("exchange_item_price");
    if (set) {
      price = change;
    } else if (percent) {
      var percentChanged = price * (change / 100.0);
      if (percentChanged > -1 && change < 0) {
        percentChanged = -1;
      }
      if (percentChanged < 1 && change > 0) {
        percentChanged = 1;
      }
      price += percentChanged;
    } else {
      price += change;
    }
    price = Math.max(price, 1);
    price = Math.min(price, Item.MAX_AMOUNT);
    player.putAttribute("exchange_item_price", price);
    player.getPlugin(GrandExchangePlugin.class).getOffers().loadCreated();
  }

  private static void addOffer(Player player) {
    var plugin = player.getPlugin(GrandExchangePlugin.class);
    if (player.getAttributeBool("exchange_sell")) {
      plugin
          .getOffers()
          .addSell(
              player.getAttributeInt("exchange_item_id") - 1,
              player.getAttributeInt("exchange_item_quantity"),
              player.getAttributeInt("exchange_item_price"));
    } else {
      plugin
          .getOffers()
          .addBuy(
              player.getAttributeInt("exchange_item_id") - 1,
              player.getAttributeInt("exchange_item_quantity"),
              player.getAttributeInt("exchange_item_price"));
    }
  }

  @Override
  public void onOpen(Player player) {
    var plugin = player.getPlugin(GrandExchangePlugin.class);
    player.getWidgetManager().closeKeyboardScript();
    if (player.getAttributeInt("exchange_slot") > 0) {
      plugin.getOffers().loadExisting(player.getAttributeInt("exchange_slot") - 1);
    } else {
      if (player.getAttributeBool("exchange_sell")) {
        player
            .getWidgetManager()
            .sendWidget(ViewportContainer.INVENTORY, WidgetId.GRAND_EXCHANGE_INVENTORY);
      } else if (player.getAttributeInt("exchange_item_id") - 1 == -1) {
        sendItemSearch(player);
      }
    }
  }

  @Override
  public void onClose(Player player) {
    player.getWidgetManager().sendWidget(ViewportContainer.INVENTORY, WidgetId.INVENTORY);
    player.removeAttribute("exchange_slot");
    player.removeAttribute("exchange_sell");
    player.removeAttribute("exchange_item_id");
    player.removeAttribute("exchange_item_name");
    player.removeAttribute("exchange_item_quantity");
    player.removeAttribute("exchange_item_price");
    player.removeAttribute("exchange_username");
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var plugin = player.getPlugin(GrandExchangePlugin.class);
    switch (childId) {
      case 17:
        player
            .getBank()
            .pinRequiredAction(
                () ->
                    player.getWidgetManager().sendInteractiveOverlay(WidgetId.GRAND_EXCHANGE_1024));
        break;
      case 37:
        sendItemSearch(player);
        break;
      case 64:
      case 81:
        changeQuantity(player, 1, false);
        break;
      case 67:
        changeQuantity(player, 10, false);
        break;
      case 70:
        changeQuantity(player, 100, false);
        break;
      case 73:
        changeQuantity(player, 1_000, false);
        break;
      case 79:
        changeQuantity(player, -1, false);
        break;
      case 76:
        player.getGameEncoder().sendEnterAmount(ie -> changeQuantity(player, ie, true));
        break;
      case 96:
        changePrice(player, -5, true, false);
        break;
      case 99:
        changePrice(
            player,
            ItemDef.getConfiguredExchangePrice(player.getAttributeInt("exchange_item_id") - 1),
            false,
            true);
        break;
      case 102:
        player.getGameEncoder().sendEnterAmount(ie -> changePrice(player, ie, false, true));
        break;
      case 105:
        changePrice(player, 5, true, false);
        break;
      case 108:
        changePrice(player, -1, false, false);
        break;
      case 110:
        changePrice(player, 1, false, false);
        break;
      case 123:
        addOffer(player);
        break;
      case 134:
        plugin.getOffers().abort(player.getAttributeInt("exchange_slot") - 1);
        break;
      case 142:
        plugin.getOffers().collect(player.getAttributeInt("exchange_slot") - 1, true, option);
        break;
      case 149:
        plugin.getOffers().collect(player.getAttributeInt("exchange_slot") - 1, false, option);
        break;
    }
  }
}
