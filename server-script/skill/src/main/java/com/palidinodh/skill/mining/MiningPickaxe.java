package com.palidinodh.skill.mining;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
class MiningPickaxe {

  private int itemId;
  private int level;
  private int animation;
  private int wallAnimation;
}
