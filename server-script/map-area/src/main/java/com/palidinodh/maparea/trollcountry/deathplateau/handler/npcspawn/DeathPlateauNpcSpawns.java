package com.palidinodh.maparea.trollcountry.deathplateau.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class DeathPlateauNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2856, 3596), NpcId.MOUNTAIN_TROLL_69));
    spawns.add(new NpcSpawn(4, new Tile(2861, 3597), NpcId.MOUNTAIN_TROLL_69));
    spawns.add(new NpcSpawn(4, new Tile(2869, 3596), NpcId.MOUNTAIN_TROLL_69));
    spawns.add(new NpcSpawn(4, new Tile(2875, 3593), NpcId.MOUNTAIN_TROLL_69));
    spawns.add(new NpcSpawn(4, new Tile(2880, 3587), NpcId.MOUNTAIN_TROLL_69));
    spawns.add(new NpcSpawn(4, new Tile(2870, 3587), NpcId.MOUNTAIN_TROLL_69));
    spawns.add(new NpcSpawn(4, new Tile(2866, 3591), NpcId.MOUNTAIN_TROLL_69));
    spawns.add(new NpcSpawn(4, new Tile(2859, 3592), NpcId.MOUNTAIN_TROLL_69));
    spawns.add(new NpcSpawn(4, new Tile(2861, 3588), NpcId.MOUNTAIN_TROLL_69));
    spawns.add(new NpcSpawn(4, new Tile(2875, 3588), NpcId.MOUNTAIN_TROLL_69));
    spawns.add(new NpcSpawn(4, new Tile(2874, 3598), NpcId.MOUNTAIN_TROLL_69));
    spawns.add(new NpcSpawn(4, new Tile(2865, 3595), NpcId.MOUNTAIN_TROLL_69_937));
    spawns.add(new NpcSpawn(4, new Tile(2855, 3591), NpcId.MOUNTAIN_TROLL_69_937));
    spawns.add(new NpcSpawn(4, new Tile(2866, 3587), NpcId.MOUNTAIN_TROLL_69_937));
    spawns.add(new NpcSpawn(4, new Tile(2871, 3592), NpcId.MOUNTAIN_TROLL_69_937));
    spawns.add(new NpcSpawn(4, new Tile(2853, 3586), NpcId.MOUNTAIN_TROLL_69_937));
    spawns.add(new NpcSpawn(4, new Tile(2847, 3591), NpcId.MOUNTAIN_TROLL_69_937));
    spawns.add(new NpcSpawn(4, new Tile(2881, 3591), NpcId.MOUNTAIN_TROLL_69_937));

    return spawns;
  }
}
