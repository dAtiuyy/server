package com.palidinodh.playerplugin.magic;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.util.PNumber;
import lombok.Getter;
import lombok.Setter;

public class MagicPlugin implements PlayerPlugin {

  @Inject private transient Player player;
  @Getter @Setter private transient int spellDelay;

  @Getter private int alchWarningValue = 30_000;
  @Getter public boolean leatherStatus;

  @Override
  public void tick() {
    if (spellDelay > 0) {
      spellDelay--;
    }
  }

  public void tanLeatherSetupDialogue() {
    if (leatherStatus) {
      player.openDialogue(
          new OptionsDialogue(
              "The spell currently gives SOFT leather.",
              new DialogueOption(
                  "Switch to HARD leather.",
                  (c, s) -> {
                    leatherStatus = false;
                  }),
              new DialogueOption("Soft leather is fine.")));
    } else if (!leatherStatus) {
      player.openDialogue(
          new OptionsDialogue(
              "The spell currently gives HARD leather.",
              new DialogueOption(
                  "Switch to SOFT leather.",
                  (c, s) -> {
                    leatherStatus = true;
                  }),
              new DialogueOption("Hard leather is fine.")));
    }
  }

  public void alchWarningsDialogue() {
    player.openDialogue(
        new MessageDialogue(
            "A warning will be shown if the item is worth at least:<br><col=800000>"
                + PNumber.formatNumber(alchWarningValue)
                + " coins</col>.<br>Untradeable items <col=800000>will always</col> trigger such a warning.",
            (c, s) -> {
              alchOptionsDialogue();
            }));
  }

  private void alchOptionsDialogue() {
    player.openOptionsDialogue(
        new DialogueOption(
            "Set value threshold",
            (c, s) -> {
              alchSetValueDialogue();
            }),
        new DialogueOption("Cancel"));
  }

  private void alchSetValueDialogue() {
    player
        .getGameEncoder()
        .sendEnterAmount(
            "Set value threshold for alchemy warnings:",
            v -> {
              alchWarningValue = v;
              alchWarningsDialogue();
            });
  }
}
