package com.palidinodh.playerplugin.godwars;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.playerplugin.boss.BossInstanceController;
import com.palidinodh.util.PEvent;
import lombok.Getter;
import lombok.Setter;

public class GodWarsPlugin implements PlayerPlugin {

  @Inject private transient Player player;
  @Getter @Setter private transient PEvent nexCoughEvent;

  @Getter @Setter private boolean godWarsFrozenDoor;

  @Override
  public void restore() {
    if (nexCoughEvent != null) {
      nexCoughEvent.stop();
      nexCoughEvent = null;
    }
  }

  @Override
  public void tick() {
    nexTick();
  }

  public void startNexCough() {
    if (nexCoughEvent != null && nexCoughEvent.isRunning()) {
      nexCoughEvent.setExecutions(0);
      return;
    }
    if (!player.getController().is(BossInstanceController.class)) {
      return;
    }
    var players = player.getController().getNearbyPlayers();
    players.remove(player);
    nexCoughEvent =
        player
            .getController()
            .addContinuousEvent(
                2,
                e -> {
                  var timer = 10;
                  var helmet = player.getEquipment().getHeadItem();
                  if (helmet != null && helmet.getId() != -1) {
                    if (helmet.getId() == ItemId.GAS_MASK
                        || helmet.getId() == ItemId.FACEMASK
                        || helmet.getDef().getLowerCaseName().contains("slayer helmet")) {
                      timer = 8;
                    }
                  }
                  if (!player.getController().is(BossInstanceController.class)
                      || e.getExecutions() > timer) {
                    e.stop();
                    return;
                  }
                  player.setForceMessage("*Cough*");
                  var amount = 2;
                  if (player.getEquipment().getShieldId() == ItemId.SPECTRAL_SPIRIT_SHIELD) {
                    amount = 1;
                  }
                  player.getSkills().changeStat(Skills.PRAYER, -amount);
                  players.forEach(
                      p -> {
                        if (!player.withinDistance(p, 1)) {
                          return;
                        }
                        p.getPlugin(GodWarsPlugin.class).startNexCough();
                      });
                });
  }

  private void nexTick() {
    if (nexCoughEvent != null && !nexCoughEvent.isRunning()) {
      nexCoughEvent = null;
    }
  }
}
