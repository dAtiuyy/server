package com.palidinodh.command.seniormod;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.playstyle.PlayStylePlugin;
import com.palidinodh.rs.adaptive.RsGameMode;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("toregular")
class ToRegularCommand implements CommandHandler, CommandHandler.SeniorModeratorRank {

  @Override
  public String getExample(String name) {
    return "username";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var player2 = player.getWorld().getPlayerByUsername(message);
    if (player2 == null) {
      player.getGameEncoder().sendMessage("Unable to find user " + message + ".");
      return;
    }
    player2.getPlugin(PlayStylePlugin.class).setGameMode(RsGameMode.REGULAR);
    player2
        .getGameEncoder()
        .sendMessage("Your gamemode has been set to Normal mode by " + player.getUsername());
    player.getGameEncoder().sendMessage("Success");
    DiscordBot.sendMessage(
        DiscordChannel.MODERATION_LOG,
        player.getUsername() + " has made " + player2.getUsername() + " a regular.");
  }
}
