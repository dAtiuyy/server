package com.palidinodh.playerplugin.wilderness;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import lombok.Getter;
import lombok.Setter;

public class WildernessPlugin implements PlayerPlugin {

  public static final int REVENANT_CAVE_FEE = 100_000;
  public static final int HP_EVENT_BONDS_HOUR = 18;

  @Inject private transient Player player;

  @Getter @Setter private boolean payedRevenantFee;
  @Getter @Setter private boolean autoPayRevenantFee;

  public static boolean isBloodyKey(int id) {
    if (id == ItemId.BLOODIER_KEY) {
      return true;
    }
    if (id == ItemId.BLOODIER_KEY_32305) {
      return true;
    }
    if (id == ItemId.BLOODIER_KEY_32396) {
      return true;
    }
    if (id == ItemId.BLOODIER_KEY_32397) {
      return true;
    }
    return id == ItemId.BLOODY_KEY || id == ItemId.BLOODY_KEY_32304;
  }

  public static boolean isActiveBloodyKey(int id) {
    if (id == ItemId.BLOODIER_KEY) {
      return true;
    }
    if (id == ItemId.BLOODIER_KEY_32396) {
      return true;
    }
    return id == ItemId.BLOODY_KEY;
  }

  public static boolean isActiveBloodierKey(int id) {
    if (id == ItemId.BLOODIER_KEY_32396) {
      return true;
    }
    return id == ItemId.BLOODIER_KEY;
  }

  public boolean hasBloodyKey() {
    if (player.carryingItem(ItemId.BLOODIER_KEY)) {
      return true;
    }
    if (player.carryingItem(ItemId.BLOODIER_KEY_32396)) {
      return true;
    }
    return player.carryingItem(ItemId.BLOODY_KEY);
  }
}
