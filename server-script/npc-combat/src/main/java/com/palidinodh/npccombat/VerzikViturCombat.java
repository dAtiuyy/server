package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.NullNpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatSummonNpc;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.chat.ChatDialogue;
import com.palidinodh.osrscore.model.entity.shared.ForceMovement;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.map.route.ProjectileRoute;
import com.palidinodh.osrscore.model.tile.Bounds;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import com.palidinodh.playerplugin.boss.BossPlugin;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PEventTasks;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PTime;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;

class VerzikViturCombat extends NpcCombat {

  private static final List<Tile> PILLARS =
      Arrays.asList(
          new Tile(3161, 4318),
          new Tile(3161, 4312),
          new Tile(3161, 4306),
          new Tile(3173, 4312),
          new Tile(3173, 4318),
          new Tile(3173, 4312),
          new Tile(3173, 4306));
  private static final NpcCombatSummonNpc COMMON_NYLOCAS_SUMMONING;
  private static final NpcCombatStyle PHASE_2_MAGIC_ATTACK;
  private static final List<Phase3SpecialAttack> PHASE_3_SPECIAL_ATTACKS =
      Arrays.asList(Phase3SpecialAttack.values());
  private static final Bounds COMBAT_BOUNDS = new Bounds(3154, 4303, 3182, 4322);
  private static final NpcCombatDropTable UNIQUE_DROP_TABLE =
      NpcCombatDropTable.builder()
          .probabilityDenominator(45)
          .store(NpcCombatDropTable.Store.INVENTORY)
          .broadcast(true)
          .log(true)
          .drop(
              NpcCombatDropTableDrop.items(
                  new RandomItem(ItemId.SCYTHE_OF_VITUR_UNCHARGED).weight(1)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GHRAZI_RAPIER).weight(2)))
          .drop(
              NpcCombatDropTableDrop.items(
                  new RandomItem(ItemId.SANGUINESTI_STAFF_UNCHARGED).weight(2)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JUSTICIAR_FACEGUARD).weight(2)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JUSTICIAR_CHESTGUARD).weight(2)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JUSTICIAR_LEGGUARDS).weight(2)))
          .drop(
              NpcCombatDropTableDrop.items(new RandomItem(ItemId.AVERNIC_DEFENDER_HILT).weight(8)))
          .build();
  private static final NpcCombatDropTable COMMON_DROP_TABLE =
      NpcCombatDropTable.builder()
          .probabilityDenominator(NpcCombatDropTable.COMMON)
          .store(NpcCombatDropTable.Store.INVENTORY)
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.VIAL_OF_BLOOD_NOTED, 7)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.VIAL_OF_BLOOD_NOTED, 50, 60)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 500, 600)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 500, 600)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SWAMP_TAR, 500, 600)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COAL_NOTED, 500, 600)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GOLD_ORE_NOTED, 300, 360)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MOLTEN_GLASS_NOTED, 200, 240)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_ORE_NOTED, 130, 156)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_ORE_NOTED, 60, 72)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WINE_OF_ZAMORAK_NOTED, 50, 60)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.POTATO_CACTUS_NOTED, 50, 60)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE_NOTED, 50, 60)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE_NOTED, 40, 48)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TOADFLAX_NOTED, 37, 44)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM_NOTED, 36, 43)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_IRIT_LEAF_NOTED, 34, 40)))
          .drop(
              NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED_NOTED, 30, 36)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_SNAPDRAGON_NOTED, 27, 32)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME_NOTED, 26, 31)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_DWARF_WEED_NOTED, 24, 28)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL_NOTED, 20, 24)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BATTLESTAFF_NOTED, 15, 18)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAHOGANY_SEED, 8, 12)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_BATTLEAXE_NOTED, 4)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATEBODY_NOTED, 4)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_CHAINBODY_NOTED, 4)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PALM_TREE_SEED, 3)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YEW_SEED, 3)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_SEED, 3)))
          .build();

  static {
    var summonNpc = NpcCombatSummonNpc.builder();
    summonNpc
        .type(NpcCombatSummonNpc.Type.NEARBY)
        .minimumDistance(4)
        .maximumDistance(16)
        .follow(true);
    summonNpc.spawn(
        new NpcSpawn(
            NpcId.NYLOCAS_ISCHYROS_162_8381,
            NpcId.NYLOCAS_TOXOBOLOS_162_8382,
            NpcId.NYLOCAS_HAGIOS_162_8383));
    COMMON_NYLOCAS_SUMMONING = summonNpc.build();

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(40).build());
    style.animation(8114).attackSpeed(4);
    style.targetGraphic(new Graphic(1592));
    style.projectile(NpcCombatProjectile.id(1591));
    PHASE_2_MAGIC_ATTACK = style.build();
  }

  @Inject private Npc npc;
  private Phase phase = Phase.PRE_FIRST;
  private PArrayList<Npc> pillars = new PArrayList<>(PILLARS.size());
  private int redSpiderDelay;
  private int attackCount;
  private Npc athanatos;
  private int athanatosCooldown;
  private Deque<Phase3SpecialAttack> phase3SpecialAttacks =
      new ArrayDeque<>(PHASE_3_SPECIAL_ATTACKS.size());
  private Map<Integer, Npc> tornados = new HashMap<>();

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var preCombat1 = NpcCombatDefinition.builder();
    preCombat1.id(Phase.PRE_FIRST.getNpcId());

    var combat1 = NpcCombatDefinition.builder();
    combat1.id(Phase.FIRST.getNpcId());
    combat1.spawn(NpcCombatSpawn.builder().respawnId(Phase.PRE_SECOND.getNpcId()).build());
    combat1.hitpoints(
        NpcCombatHitpoints.builder()
            .total(2000)
            .barType(HitpointsBarType.CYAN_DARKER_100)
            .hitMarkType(HitMarkType.CYAN)
            .build());
    combat1.stats(
        NpcCombatStats.builder()
            .attackLevel(400)
            .magicLevel(400)
            .rangedLevel(400)
            .defenceLevel(20)
            .bonus(BonusType.ATTACK_MAGIC, 80)
            .bonus(BonusType.ATTACK_RANGED, 80)
            .bonus(BonusType.MELEE_DEFENCE, 20)
            .bonus(BonusType.DEFENCE_MAGIC, 20)
            .bonus(BonusType.DEFENCE_RANGED, 20)
            .build());
    combat1.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat1.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    combat1.blockAnimation(8110);

    var preCombat2 = NpcCombatDefinition.builder();
    preCombat2.id(Phase.PRE_SECOND.getNpcId());
    preCombat2.spawn(NpcCombatSpawn.builder().animation(8114).build());
    preCombat2.hitpoints(
        NpcCombatHitpoints.builder().total(3250).barType(HitpointsBarType.GREEN_RED_100).build());

    var combat2 = NpcCombatDefinition.builder();
    combat2.id(Phase.SECOND.getNpcId());
    combat2.spawn(NpcCombatSpawn.builder().respawnId(Phase.PRE_THIRD.getNpcId()).build());
    combat2.hitpoints(
        NpcCombatHitpoints.builder().total(3250).barType(HitpointsBarType.GREEN_RED_100).build());
    combat2.stats(
        NpcCombatStats.builder()
            .attackLevel(400)
            .magicLevel(400)
            .rangedLevel(400)
            .defenceLevel(200)
            .bonus(BonusType.ATTACK_MAGIC, 80)
            .bonus(BonusType.ATTACK_RANGED, 80)
            .bonus(BonusType.DEFENCE_STAB, 100)
            .bonus(BonusType.DEFENCE_SLASH, 60)
            .bonus(BonusType.DEFENCE_CRUSH, 100)
            .bonus(BonusType.DEFENCE_MAGIC, 70)
            .bonus(BonusType.DEFENCE_RANGED, 250)
            .build());
    combat2.aggression(NpcCombatAggression.builder().range(20).build());
    combat2.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat2.focus(
        NpcCombatFocus.builder()
            .meleeUnlessUnreachable(true)
            .disableFollowingOpponent(true)
            .build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.builder().minimum(8).maximum(45).build());
    style.animation(8116).attackSpeed(4);
    combat2.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.RANGED)
            .subHitStyleType(HitStyleType.TYPELESS)
            .build());
    style.damage(NpcCombatDamage.builder().minimum(24).maximum(41).build());
    style.animation(8114).attackSpeed(4);
    style.targetTileGraphic(new Graphic(1584));
    style.projectile(NpcCombatProjectile.builder().id(1583).speedMinimumDistance(10).build());
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.specialAttack(NpcCombatTargetTile.builder().build());
    combat2.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.UNDERNEATH);
    style.damage(NpcCombatDamage.builder().minimum(8).maximum(45).build());
    style.animation(8116).attackSpeed(4);
    combat2.style(style.build());

    var preCombat3 = NpcCombatDefinition.builder();
    preCombat3.id(Phase.PRE_THIRD.getNpcId());
    preCombat3.hitpoints(
        NpcCombatHitpoints.builder().total(3250).barType(HitpointsBarType.GREEN_RED_100).build());
    preCombat3.spawn(NpcCombatSpawn.builder().animation(8119).build());

    var combat3 = NpcCombatDefinition.builder();
    combat3.id(Phase.THIRD.getNpcId());
    combat3.spawn(NpcCombatSpawn.builder().phrase("Behold my true nature!").build());
    combat3.hitpoints(
        NpcCombatHitpoints.builder().total(3250).barType(HitpointsBarType.GREEN_RED_100).build());
    combat3.stats(
        NpcCombatStats.builder()
            .attackLevel(400)
            .magicLevel(300)
            .rangedLevel(300)
            .defenceLevel(150)
            .bonus(BonusType.MELEE_ATTACK, 80)
            .bonus(BonusType.ATTACK_MAGIC, 80)
            .bonus(BonusType.ATTACK_RANGED, 80)
            .bonus(BonusType.DEFENCE_STAB, 70)
            .bonus(BonusType.DEFENCE_SLASH, 20)
            .bonus(BonusType.DEFENCE_CRUSH, 70)
            .bonus(BonusType.DEFENCE_MAGIC, 100)
            .bonus(BonusType.DEFENCE_RANGED, 230)
            .build());
    combat3.aggression(NpcCombatAggression.builder().range(20).build());
    combat3.focus(NpcCombatFocus.builder().keepWithinDistance(1).singleTargetFocus(true).build());
    combat3.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat3.killCount(NpcCombatKillCount.builder().asName("Theatre of Blood").build());
    combat3.deathAnimation(8128);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(
        NpcCombatDamage.builder()
            .maximum(60)
            .prayerEffectiveness(0.5)
            .delayedPrayerProtectable(true)
            .build());
    style.animation(8125).attackSpeed(7);
    combat3.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(
        NpcCombatDamage.builder()
            .maximum(32)
            .prayerEffectiveness(0.5)
            .delayedPrayerProtectable(true)
            .build());
    style.animation(8125).attackSpeed(7);
    style.projectile(NpcCombatProjectile.builder().id(1593).speedMinimumDistance(10).build());
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat3.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(
        NpcCombatDamage.builder()
            .maximum(32)
            .prayerEffectiveness(0.5)
            .delayedPrayerProtectable(true)
            .build());
    style.animation(8123).attackSpeed(7);
    style.targetGraphic(new Graphic(1581));
    style.projectile(NpcCombatProjectile.builder().id(1594).speedMinimumDistance(10).build());
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat3.style(style.build());

    return Arrays.asList(
        preCombat1.build(),
        combat1.build(),
        preCombat2.build(),
        combat2.build(),
        preCombat3.build(),
        combat3.build());
  }

  @Override
  public List<NpcCombatDropTable> getAdditionalDropTables(int npcId) {
    return Arrays.asList(UNIQUE_DROP_TABLE);
  }

  @Override
  public void npcApplyHitStartHook(Hit hit) {
    if (redSpiderDelay > 35) {
      hit.setHitMarkType(HitMarkType.HEAL);
      hit.setHitStyleType(HitStyleType.HEAL);
    }
  }

  @Override
  public void spawnHook() {
    npc.setLargeVisibility();
    PILLARS.forEach(tile -> pillars.add(addNpc(new NpcSpawn(tile, NpcId.SUPPORTING_PILLAR))));
    for (var player : npc.getController().getNearbyPlayers()) {
      if (!COMBAT_BOUNDS.contains(player)) {
        continue;
      }
      player.getMovement().teleport(BossPlugin.VERZIK_ENTER_TILE);
    }
    if (npc.getId() == Phase.PRE_FIRST.getNpcId()) {
      var spawnSeconds = 30;
      var message =
          "<col=ff0000>The fight with Verzik Vitur will begin in " + spawnSeconds + " seconds!";
      npc.getController().sendMessage(message);
      if (!npc.getController().isInstanced()) {
        Area.getArea(BossPlugin.TOB_CENTER_LOBBY).sendMessage(message);
      }
      addSingleEvent(
          PTime.secToTick(spawnSeconds),
          t -> {
            npc.setId(Phase.FIRST.getNpcId());
            npc.getController()
                .getNearbyPlayers()
                .forEach(
                    p -> {
                      p.openDialogue(
                          new ChatDialogue(
                              NpcId.VERZIK_VITUR_1040,
                              -1,
                              "Now that was quite the show! I haven't been that entertained in a long time.",
                              (c, s) -> {}));
                      p.getGameEncoder().setVarbit(VarbitId.THEATRE_OF_BLOOD_HP_TYPE, 3);
                    });
          });
    }
  }

  @Override
  public void despawnHook() {
    pillars.clear();
    calculateRewards();
  }

  @Override
  public void idChangedHook(int oldId, int newId) {
    phase = Phase.getPhase(npc.getId());
    setHitpoints();
    switch (phase) {
      case FIRST:
        {
          setHitDelay(15);
          break;
        }
      case PRE_SECOND:
        {
          var tile = new Tile(3166, 4312);
          var tasks = new PEventTasks();
          tasks.execute(
              2,
              t -> {
                npc.getMovement().clear();
                npc.getMovement().addMovement(tile);
                npc.getController()
                    .getNearbyPlayers()
                    .forEach(
                        p -> {
                          p.getGameEncoder().setVarbit(VarbitId.THEATRE_OF_BLOOD_HP_TYPE, 1);
                        });
              });
          tasks.condition(t -> npc.matchesTile(tile));
          tasks.execute(
              t -> {
                npc.setId(Phase.SECOND.getNpcId());
                npc.moveTile(1, 1);
              });
          addEvent(tasks);
          break;
        }
      case PRE_THIRD:
        {
          addSingleEvent(
              4,
              t -> {
                npc.setId(Phase.THIRD.getNpcId());
                npc.moveTile(-1, -1);
                npc.setAnimation(-1);
                removeNpcs();
              });
          break;
        }
      case SECOND:
      case THIRD_20_PERCENT:
        {
          applyPoints();
          break;
        }
    }
  }

  @Override
  public void tickStartHook() {
    if (npc.getTotalTicks() > 50 && npc.getController().getNearbyPlayers().isEmpty()) {
      removeNpc(npc);
    }
    if (npc.isLocked()) {
      return;
    }
    switch (phase) {
      case FIRST:
        phase1Tick();
        break;
      case SECOND:
        phase2Tick();
        break;
      case THIRD:
      case THIRD_20_PERCENT:
        phase3Tick();
        break;
    }
    if (getDefenceLevel() < getBaseDefenceLevel()) {
      setDefenceLevel(getBaseDefenceLevel());
      npc.getController().sendMessage("Verzik Vitur absorbs the defence reduction!");
    }
  }

  @Override
  public PArrayList<NpcCombatStyle> attackTickCombatStylesHook(
      PArrayList<NpcCombatStyle> combatStyles, Entity opponent) {
    if (phase == Phase.SECOND && PRandom.getPercent(getHitpoints(), getMaxHitpoints()) < 35) {
      combatStyles.add(PHASE_2_MAGIC_ATTACK);
    }
    return combatStyles;
  }

  @Override
  public int attackTickAttackSpeedHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (phase != Phase.THIRD_20_PERCENT) {
      return combatStyle.getAttackSpeed();
    }
    return combatStyle.getAttackSpeed() - 2;
  }

  @Override
  public void applyAttackEndHook(
      NpcCombatStyle combatStyle, Entity opponent, int applyAttackLoopCount, HitEvent hitEvent) {
    if (phase == Phase.SECOND) {
      if (combatStyle.getType().getHitStyleType() == HitStyleType.MELEE
          || combatStyle.getType().getHitStyleType() == HitStyleType.UNDERNEATH) {
        meleeBounceAttack(opponent);
      }
    }
  }

  @Override
  public void attackTickEndHook(NpcCombatStyle combatStyle, Entity opponent) {
    attackCount++;
  }

  @Override
  public double defenceHook(
      Entity opponent, HitStyleType hitStyleType, BonusType meleeAttackStyle, double defence) {
    if (phase != Phase.FIRST) {
      return defence;
    }
    if (hitStyleType != HitStyleType.MAGIC) {
      return defence;
    }
    if (!opponent.isPlayer()) {
      return defence;
    }
    var player = opponent.asPlayer();
    if (player.getEquipment().getWeaponId() != ItemId.DAWNBRINGER) {
      return defence;
    }
    return 0;
  }

  @Override
  public void applyDeadStartHook(int deathDelay) {
    pillars.removeIf(Entity::isLocked);
    pillars.removeEach(n -> n.getCombat().startDeath());
    applyPoints();
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (phase == Phase.FIRST) {
      if (hitStyleType != HitStyleType.MAGIC
          || !opponent.isPlayer()
          || opponent.asPlayer().getEquipment().getWeaponId() != ItemId.DAWNBRINGER) {
        // return Math.min(damage, hitStyleType == HitStyleType.MELEE ? 10 : 3);
      }
    }
    return damage;
  }

  @Override
  public void changeHitpointsHook(int amount, int allowedOver) {
    if (amount == 0) {
      return;
    }
    npc.getController()
        .getNearbyPlayers()
        .forEach(
            p -> {
              p.getGameEncoder().setVarbit(VarbitId.THEATRE_OF_BLOOD_CURRENT_HP, getHitpoints());
            });
  }

  private void setHitpoints() {
    var playerCount = getAttackablePlayers().size();
    var multiplier = Math.min(PNumber.addDoubles(0.375, playerCount * 0.125), 1);
    setMaxHitpoints((int) (getMaxHitpoints() * multiplier));
    setHitpoints(getMaxHitpoints());
    npc.getController()
        .getNearbyPlayers()
        .forEach(
            p -> {
              p.getGameEncoder().setVarbit(VarbitId.THEATRE_OF_BLOOD_MAX_HP, getMaxHitpoints());
              p.getGameEncoder().setVarbit(VarbitId.THEATRE_OF_BLOOD_CURRENT_HP, getHitpoints());
            });
  }

  private void phase1Tick() {
    if (getHitDelay() == 2) {
      npc.setAnimation(8109);
      return;
    }
    if (isHitDelayed()) {
      return;
    }
    var sourceTile = new Tile(npc).setSize(npc.getSizeX(), 1);
    var players = npc.getController().getNearbyPlayers();
    players.removeIf(p -> p.isLocked() || !COMBAT_BOUNDS.contains(p));
    var hitEntities = new PArrayList<Entity>();
    for (var player : players) {
      if (ProjectileRoute.INSTANCE.allow(npc.getController(), sourceTile, player)) {
        hitEntities.add(player);
      } else {
        var closestPillar = (Npc) player.getClosest(pillars);
        hitEntities.addIf(closestPillar, e -> !hitEntities.contains(e));
      }
    }
    for (var entity : hitEntities) {
      var speed = getProjectileSpeed(8);
      speed.setClientDelay(0);
      var projectile =
          Graphic.Projectile.builder().id(1580).startTile(npc).entity(entity).speed(speed).build();
      sendMapProjectile(projectile);
      var damage = PRandom.randomI(137);
      if (entity.isPlayer()) {
        if (entity.asPlayer().getPrayer().hasActive("protect from magic")) {
          damage *= 0.5;
        }
      } else {
        damage *= 0.5;
      }
      entity.setGraphic(
          new Graphic(entity.isPlayer() ? 1581 : 1582, 0, projectile.getContactDelay()));
      entity
          .getCombat()
          .addHitEvent(new HitEvent(projectile.getEventDelay(), new Hit(damage), npc));
    }
    setHitDelay(14);
  }

  private void phase2Tick() {
    redSpiderDelay--;
    athanatosCooldown--;
    if (PRandom.getPercent(getHitpoints(), getMaxHitpoints()) < 35) {
      if (redSpiderDelay <= 0) {
        redSpiderDelay = 45;
        addNpc(new NpcSpawn(2, new Tile(3163, 4314), NpcId.NYLOCAS_MATOMENOS_115_8385));
        if (npc.getController().getNearbyPlayers().size() > 1) {
          addNpc(new NpcSpawn(2, new Tile(3172, 4314), NpcId.NYLOCAS_MATOMENOS_115_8385));
        }
      }
    }
    if (!isHitDelayed() && attackCount == 4) {
      attackCount = 0;
      electricBallAttack();
      return;
    }
    if (!isHitDelayed() && PRandom.randomE(4) == 0) {
      spawnNylocasAthanatos();
    }
  }

  private void phase3Tick() {
    if (phase3SpecialAttacks.isEmpty()) {
      phase3SpecialAttacks.addAll(PHASE_3_SPECIAL_ATTACKS);
    }
    if (phase == Phase.THIRD && PRandom.getPercent(getHitpoints(), getMaxHitpoints()) <= 20) {
      phase = Phase.THIRD_20_PERCENT;
      npc.setForceMessage("I'm not finished with you just yet!");
      tornadoAttack();
    } else if (!isHitDelayed()) {
      if (phase == Phase.THIRD_20_PERCENT && attackCount == 2) {
        tornadoAttack();
      }
      if (attackCount == 4) {
        attackCount = 0;
        var specialAttack = phase3SpecialAttacks.poll();
        switch (specialAttack) {
          case NYLOCAS:
            spawnCommonNylocas();
            break;
          case STICKY_WEBS:
            stickyWebsAttack();
            break;
          case PORTALS:
            portalsAttack();
            break;
          case BOUNCING_BOMB:
            bouncingBombAttack();
            break;
        }
      }
    }
  }

  private void electricBallAttack() {
    var firstOpponent = getAttackingEntity();
    if (firstOpponent == null || firstOpponent.isLocked() || !firstOpponent.isPlayer()) {
      return;
    }
    setHitDelay(npc.getCombatDef().getAttackSpeed());
    npc.setAnimation(npc.getCombatDef().getAttackAnimation());
    var players = getAttackablePlayers();
    players.remove(firstOpponent);
    players.shuffle();
    electricBallAttack(npc, firstOpponent.asPlayer(), players, 1);
  }

  private void electricBallAttack(
      Entity fromEntity, Entity toEntity, PArrayList<Player> players, int count) {
    var speed = getProjectileSpeed(8);
    sendMapProjectile(
        Graphic.Projectile.builder()
            .id(1585)
            .startTile(fromEntity)
            .entity(toEntity)
            .speed(speed)
            .build());
    if (toEntity == npc) {
      return;
    }
    if (toEntity.isPlayer() && players.isEmpty()) {
      var damage = PRandom.randomI(40, 48);
      if (toEntity.asPlayer().getEquipment().getFootId() == ItemId.INSULATED_BOOTS) {
        damage /= 4;
      }
      toEntity.getCombat().addHitEvent(new HitEvent(speed.getEventDelay(), new Hit(damage), npc));
    }
    addSingleEvent(
        speed.getNewEventDelay(),
        e -> {
          players.removeIf(p -> !canAttackEntity(p));
          Entity toNewEntity = players.removeFirst();
          if (toNewEntity == null) {
            return;
          }
          var path = ProjectileRoute.INSTANCE.getPath(npc.getController(), toEntity, toNewEntity);
          if (path.containsIf(t -> npc.withinDistance(t, 0))) {
            toNewEntity = npc;
          }
          electricBallAttack(toEntity, toNewEntity, players, count + 1);
        });
  }

  private void spawnNylocasAthanatos() {
    if (athanatosCooldown > 0) {
      return;
    }
    if (athanatos != null && !athanatos.isLocked()) {
      return;
    }
    athanatosCooldown = 100;
    setHitDelay(npc.getCombatDef().getAttackSpeed());
    npc.setAnimation(npc.getCombatDef().getAttackAnimation());
    var athanatosTile = new Tile();
    var direction = PRandom.randomE(4);
    switch (direction) {
      case 0:
        athanatosTile.setTile(npc.getX() - 4, npc.getY(), npc.getHeight());
        break;
      case 1:
        athanatosTile.setTile(npc.getX() + npc.getSizeX() + 1, npc.getY(), npc.getHeight());
        break;
      case 2:
        athanatosTile.setTile(npc.getX(), npc.getY() - 4, npc.getHeight());
        break;
      default:
        athanatosTile.setTile(npc.getX(), npc.getY() + npc.getSizeY() + 1, npc.getHeight());
        break;
    }
    spawnCommonNylocas();
    var projectile =
        Graphic.Projectile.builder()
            .id(1586)
            .startTile(npc)
            .endTile(athanatosTile)
            .speed(getProjectileSpeed(10))
            .endHeight(0)
            .build();
    sendMapProjectile(projectile);
    addSingleEvent(
        projectile.getEventDelay(),
        e -> athanatos = addNpc(new NpcSpawn(athanatosTile, NpcId.NYLOCAS_ATHANATOS_350)));
  }

  private void spawnCommonNylocas() {
    setHitDelay(npc.getCombatDef().getAttackSpeed());
    var players = getAttackablePlayers();
    players.forEach(p -> COMMON_NYLOCAS_SUMMONING.applyAttack(npc, p));
  }

  private void tornadoAttack() {
    var players = getAttackablePlayers();
    tornados
        .entrySet()
        .removeIf(
            e -> {
              var userId = e.getKey();
              var tornado = e.getValue();
              if (!players.containsIf(p -> p.getId() == userId)) {
                removeNpc(tornado);
                return true;
              }
              return !tornado.isVisible();
            });
    var spots = getRandomTiles(players.size());
    var index = 0;
    for (var player : players) {
      if (tornados.containsKey(player.getId())) {
        continue;
      }
      var tornado = addNpc(new NpcSpawn(spots.get(index++), NullNpcId.NULL_8386));
      tornado.getCombat().setTarget(player);
      tornados.put(player.getId(), tornado);
    }
  }

  private void stickyWebsAttack() {
    var tasks = new PEventTasks();
    lock();
    clear();
    npc.getMovement().clear();
    npc.getMovement().addMovement(3165, 4312);
    tasks.condition(t -> !npc.getMovement().isRouting());
    tasks.execute(t -> npc.setAnimation(8127));
    tasks.execute(
        t -> {
          var player = PRandom.listRandom(getAttackablePlayers());
          npc.setFaceEntity(player);
          var tiles = getWebTiles(player, PRandom.randomI(2, 3));
          var speed = getProjectileSpeed(10);
          tiles.forEach(
              tile -> {
                sendMapProjectile(
                    Graphic.Projectile.builder()
                        .id(1601)
                        .startTile(npc)
                        .endTile(tile)
                        .speed(speed)
                        .endHeight(0)
                        .build());
              });
          addSingleEvent(
              speed.getEventDelay() + 1,
              e -> {
                var npcs = npc.getController().getNpcs();
                tiles.forEach(
                    tile -> {
                      if (npcs.containsIf(n -> n.matchesTile(tile))) {
                        return;
                      }
                      addNpc(new NpcSpawn(tile, NpcId.WEB));
                    });
              });
          if (t.getExecutions() < 10) {
            t.delay(2);
          }
        });
    tasks.execute(2, t -> unlock());
    addEvent(tasks);
  }

  private List<Tile> getWebTiles(Tile tile, int count) {
    var spots = new PArrayList<Tile>();
    if (tile == null) {
      return spots;
    }
    spots.add(tile.copy());
    for (var i = 1; i < count; i++) {
      Tile successfulSpot = null;
      for (var j = 0; j < 50; j++) {
        var spot = tile.copy().randomize(4);
        if (!COMBAT_BOUNDS.contains(spot)) {
          continue;
        }
        if (spots.containsIf(t -> t.matchesTile(spot))) {
          continue;
        }
        if (npc.withinDistance(spot, 0)) {
          continue;
        }
        successfulSpot = spot;
        break;
      }
      if (successfulSpot == null) {
        continue;
      }
      spots.add(successfulSpot);
    }
    return spots;
  }

  private void portalsAttack() {
    var tasks = new PEventTasks();
    npc.lock();
    clear();
    npc.setAnimation(8126);
    var spotCount = getAttackablePlayers().size();
    var tiles = getRandomTiles(spotCount);
    var speed = getProjectileSpeed(10);
    var poolGraphic = new Graphic(1595);
    tiles.forEach(t -> npc.getController().sendMapGraphic(t, poolGraphic));
    tasks.constant(
        2,
        t -> {
          tiles.forEach(tile -> npc.getController().sendMapGraphic(tile, poolGraphic));
        });
    tasks.execute(
        8,
        t -> {
          var largestDelay = 3;
          for (var player : getAttackablePlayers()) {
            sendMapProjectile(
                Graphic.Projectile.builder()
                    .id(1596)
                    .startTile(npc)
                    .entity(player)
                    .startHeight(255)
                    .speed(speed)
                    .build());
            if (speed.getEventDelay() > largestDelay) {
              largestDelay = speed.getEventDelay();
            }
            addSingleEvent(
                speed.getEventDelay(),
                event1 -> {
                  var safe = false;
                  for (var spot : tiles) {
                    if (spot.matchesTile(player)) {
                      tiles.remove(spot);
                      safe = true;
                      break;
                    }
                  }
                  if (safe) {
                    player
                        .getGameEncoder()
                        .sendMessage("The power resonating here protects you from the blast.");
                  } else {
                    player.getCombat().addHitEvent(new HitEvent(new Hit(PRandom.randomI(40, 60))));
                  }
                });
          }
          tasks.execute(
              largestDelay,
              t2 -> {
                npc.unlock();
              });
        });
    addEvent(tasks);
  }

  private List<Tile> getRandomTiles(int count) {
    var spots = new PArrayList<Tile>();
    for (var i = 0; i < count; i++) {
      Tile successfulSpot = null;
      for (var j = 0; j < 50; j++) {
        var spot = COMBAT_BOUNDS.getRandomTile();
        if (spots.containsIf(t -> t.matchesTile(spot))) {
          continue;
        }
        if (npc.withinDistance(spot, 0)) {
          continue;
        }
        successfulSpot = spot;
        break;
      }
      if (successfulSpot == null) {
        continue;
      }
      spots.add(successfulSpot);
    }
    return spots;
  }

  private void bouncingBombAttack() {
    var entity = getAttackingEntity();
    if (entity == null || entity.isLocked() || !entity.isPlayer()) {
      return;
    }
    setHitDelay(12);
    var target = entity.asPlayer();
    npc.setAnimation(PRandom.randomE(2) == 0 ? 8125 : 8124);
    var speed = getProjectileSpeed(10);
    sendMapProjectile(
        Graphic.Projectile.builder().id(1598).startTile(npc).entity(target).speed(speed).build());
    target
        .getGameEncoder()
        .sendMessage("<col=1E9E1E>Verzik Vitur fires a powerful projectile in your direction...");
    addSingleEvent(
        speed.getEventDelay() + 1,
        e -> {
          target.getCombat().addHitEvent(new HitEvent(new Hit(74), npc));
          target.setGraphic(1600);
        });
  }

  private void meleeBounceAttack(Entity opponent) {
    if (opponent.isLocked()) {
      return;
    }
    var distance = 3;
    var direction = opponent.getDirection(npc);
    if (direction == null) {
      direction = Tile.Direction.random();
    }
    int offsetX = 0;
    int offsetY = 0;
    switch (direction) {
      case NORTH:
        offsetY = npc.getSizeY() + distance;
        break;
      case EAST:
        offsetX = npc.getSizeX() + distance;
        break;
      case SOUTH:
        offsetY = -(distance + 1);
        break;
      case WEST:
        offsetX = -(distance + 1);
        break;
    }
    var baseX = offsetX == 0 ? opponent.getX() : npc.getX();
    var baseY = offsetY == 0 ? opponent.getY() : npc.getY();
    var tile = new Tile(baseX + offsetX, baseY + offsetY, opponent.getHeight());
    while (!opponent.getController().routeAllow(tile) && offsetX + offsetY != 0) {
      offsetX = offsetX > 0 ? --offsetX : offsetX < 0 ? ++offsetX : 0;
      offsetY = offsetY > 0 ? --offsetY : offsetY < 0 ? ++offsetY : 0;
      tile = new Tile(baseX + offsetX, baseY + offsetY, opponent.getHeight());
    }
    if (offsetX == 0 && offsetY == 0) {
      return;
    }
    var fm = new ForceMovement(tile, 1, direction.reverse());
    opponent.getController().setMagicBind(8);
    opponent.setGraphic(new Graphic(245, 100));
    opponent.setForceMovementTeleport(fm, 734, 1, null);
  }

  private void applyPoints() {
    var player = getPlayerFromHitCount(false);
    if (player != null) {
      var bossPlugin = player.getPlugin(BossPlugin.class);
      bossPlugin.setPoints(bossPlugin.getPoints() + 2);
    }
    clearHitCount();
  }

  private void calculateRewards() {
    var players = npc.getController().getNearbyPlayers();
    players.removeIf(p -> p.isLocked() || !COMBAT_BOUNDS.contains(p));
    var teleportTile = new Tile(3237, 4308);
    var weightedPlayers = new PArrayList<Player>();
    players.forEach(
        p -> {
          NpcCombatKillCount.save(npc, p);
          NpcCombatKillCount.message(npc, p);
          p.rejuvenate();
          p.getCombat().clearHitEvents();
          p.getGameEncoder().sendMessage("You feel refreshed as your health is replenished.");
          p.getGameEncoder().setVarbit(VarbitId.THEATRE_OF_BLOOD_HP_TYPE, 0);
          var bossPlugin = p.getPlugin(BossPlugin.class);
          bossPlugin.setPoints(bossPlugin.getPoints() + 3);
          for (var i = 0; i < bossPlugin.getPoints() - 8; i++) {
            weightedPlayers.add(p);
          }
          p.getGameEncoder().sendClientScript(ScriptId.TOB_HUD_FADE, 0, 0, "Verzik Vitur's Vault");
        });
    npc.getController()
        .addSingleEvent(
            2,
            e -> {
              players.forEach(
                  p -> {
                    p.getMovement().teleport(teleportTile);
                    p.getGameEncoder()
                        .sendClientScript(ScriptId.TOB_HUD_FADE, 255, 0, "Verzik Vitur's Vault");
                  });
            });
    var playerForUnique = weightedPlayers.getRandom();
    var uniqueRewarded = false;
    if (UNIQUE_DROP_TABLE.canDrop(npc, playerForUnique)) {
      var drop = UNIQUE_DROP_TABLE.getDrop(playerForUnique);
      var item = drop.getItem();
      playerForUnique
          .getCombat()
          .logNPCItem(
              "Theatre of Blood",
              item.getId(),
              item.getAmount(),
              playerForUnique.getCombat().getNPCKillCount("Theatre of Blood"));
      NpcCombatDropTableDrop.announceDrop(npc, playerForUnique, item);
      playerForUnique.getPlugin(BossPlugin.class).getItems().add(item);
      playerForUnique
          .getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.THEATRE_OF_BLOOD_DROPS, 1);
      uniqueRewarded = true;
    }
    var chestTile = new Tile(3233, 4330);
    npc.getController().addMapObject(new MapObject(ObjectId.MONUMENTAL_CHEST, 10, 0, chestTile));
    for (var player : players) {
      if (player.getPlugin(FamiliarPlugin.class).rollPet(ItemId.LIL_ZIK, 0.16)) {
        player
            .getSkills()
            .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.THEATRE_OF_BLOOD_DROPS, 1);
      }
      if (PRandom.randomE(8) == 0) {
        player.getInventory().addOrDropItem(ClueScrollType.ELITE.getBoxId());
        player.getPlugin(TreasureTrailPlugin.class).resetProgress(ClueScrollType.ELITE);
      }
      if (PRandom.randomE(300) == 0) {
        var item = new Item(ItemId.SANGUINE_DUST);
        player.getPlugin(BossPlugin.class).getItems().add(item);
        NpcCombatDropTableDrop.announceDrop(npc, player, item);
        player
            .getSkills()
            .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.THEATRE_OF_BLOOD_DROPS, 1);
      }
      if (PRandom.randomE(100) == 0) {
        var item = new Item(ItemId.VAMPYRIC_SLAYER_HELMET);
        player.getPlugin(BossPlugin.class).getItems().add(item);
        NpcCombatDropTableDrop.announceDrop(npc, player, item);
        player
            .getSkills()
            .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.THEATRE_OF_BLOOD_DROPS, 1);
      }
      var bossPlugin = player.getPlugin(BossPlugin.class);
      if (uniqueRewarded && player == playerForUnique) {
        bossPlugin
            .getMapObjects()
            .add(new MapObject(ObjectId.MONUMENTAL_CHEST_32993, 10, 0, chestTile));
        continue;
      }
      for (var i = 0; i < 3; i++) {
        var drop = COMMON_DROP_TABLE.getDrop(player);
        for (var item : drop.getItems()) {
          bossPlugin.getItems().add(item);
        }
      }
      player
          .getPlugin(BossPlugin.class)
          .getMapObjects()
          .add(new MapObject(ObjectId.MONUMENTAL_CHEST_32992, 10, 0, chestTile));
    }
  }

  @AllArgsConstructor
  @Getter
  private enum Phase {
    PRE_FIRST(NpcId.VERZIK_VITUR_1040),
    FIRST(NpcId.VERZIK_VITUR_1040_8370),
    PRE_SECOND(NpcId.VERZIK_VITUR_1040_8371),
    SECOND(NpcId.VERZIK_VITUR_1265),
    PRE_THIRD(NpcId.VERZIK_VITUR_1265_8373),
    THIRD(NpcId.VERZIK_VITUR_1520),
    THIRD_20_PERCENT(NpcId.VERZIK_VITUR_1520);

    private final int npcId;

    public static Phase getPhase(int npcId) {
      for (var phase : values()) {
        if (phase.getNpcId() != npcId) {
          continue;
        }
        return phase;
      }
      return null;
    }
  }

  private enum Phase3SpecialAttack {
    NYLOCAS,
    STICKY_WEBS,
    PORTALS,
    BOUNCING_BOMB
  }
}
