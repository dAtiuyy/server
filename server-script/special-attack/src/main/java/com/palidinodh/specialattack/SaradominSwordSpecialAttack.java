package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.SARADOMIN_SWORD,
  ItemId.SARAS_BLESSED_SWORD_FULL,
  ItemId.SARADOMINS_BLESSED_SWORD
})
class SaradominSwordSpecialAttack extends SpecialAttack {

  SaradominSwordSpecialAttack() {
    var entry = Entry.builder();
    entry.itemId(ItemId.SARADOMIN_SWORD);
    entry.drain(100);
    entry.animation(1132);
    entry.castGraphic(new Graphic(1213));
    entry.impactGraphic(new Graphic(1196));
    entry.castSound(new Sound(3869));
    entry.impactSound(new Sound(3887));
    entry.damageModifier(1.1);
    entry.doubleHit(true);
    addEntry(entry);

    entry = Entry.builder();
    entry.itemId(ItemId.SARAS_BLESSED_SWORD_FULL);
    entry.itemId(ItemId.SARADOMINS_BLESSED_SWORD);
    entry.drain(65);
    entry.animation(1133);
    entry.castGraphic(new Graphic(1213));
    entry.impactGraphic(new Graphic(1196));
    entry.castSound(new Sound(3869));
    entry.impactSound(new Sound(3887));
    entry.damageModifier(1.25);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    var entry = hooks.getEntry();
    if (entry.containsItemId(ItemId.SARADOMIN_SWORD)
        && hooks.getApplyAttackLoopCount() == 1
        && hooks.getLastHitEvent().getDamage() > 0) {
      hooks.setDamage(PRandom.randomI(16));
    } else if (entry.containsItemId(
        ItemId.SARAS_BLESSED_SWORD_FULL, ItemId.SARADOMINS_BLESSED_SWORD)) {
      hooks.getPlayer().getController().sendMapGraphic(hooks.getOpponent(), new Graphic(1221));
    }
  }
}
