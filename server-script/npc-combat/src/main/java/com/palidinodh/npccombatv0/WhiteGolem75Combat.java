package com.palidinodh.npccombatv0;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import java.util.Arrays;
import java.util.List;

class WhiteGolem75Combat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.WHITE_GOLEM_75);
    combat.hitpoints(NpcCombatHitpoints.total(80));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(80)
            .defenceLevel(80)
            .bonus(BonusType.DEFENCE_STAB, 1)
            .bonus(BonusType.DEFENCE_SLASH, 300)
            .bonus(BonusType.DEFENCE_CRUSH, 300)
            .bonus(BonusType.DEFENCE_MAGIC, 300)
            .bonus(BonusType.DEFENCE_RANGED, 300)
            .build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.deathAnimation(2919).blockAnimation(2918);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(4));
    style.animation(2917).attackSpeed(4);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
