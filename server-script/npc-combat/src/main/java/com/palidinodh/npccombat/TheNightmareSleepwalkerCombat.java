package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import java.util.Arrays;
import java.util.List;

class TheNightmareSleepwalkerCombat extends NpcCombat {

  @Inject private Npc npc;
  private Npc theNightmare;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat
        .id(NpcId.SLEEPWALKER_3)
        .id(NpcId.SLEEPWALKER_3_9447)
        .id(NpcId.SLEEPWALKER_3_9448)
        .id(NpcId.SLEEPWALKER_3_9449)
        .id(NpcId.SLEEPWALKER_3_9450)
        .id(NpcId.SLEEPWALKER_3_9451);
    combat.spawn(NpcCombatSpawn.builder().animation(8572).build());
    combat.hitpoints(NpcCombatHitpoints.total(10));
    combat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    combat.deathAnimation(8570);

    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    npc.getMovement().setMoveDelay(4);
  }

  @Override
  public void tickStartHook() {
    if (!npc.withinDistance(theNightmare, 1)) {
      return;
    }
    npc.setVisible(false);
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    return 10;
  }

  @Override
  public void setTargetHook(Entity target) {
    if (target == null || !target.isNpc()) {
      return;
    }
    theNightmare = target.asNpc();
    npc.getMovement().setFollowing(target);
    setTarget(null);
  }
}
