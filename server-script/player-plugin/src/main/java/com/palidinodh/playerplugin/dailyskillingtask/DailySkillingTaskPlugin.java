package com.palidinodh.playerplugin.dailyskillingtask;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.LargeOptions2Dialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PString;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.Collections;

public class DailySkillingTaskPlugin implements PlayerPlugin {

  private static final int COMPLETED_TASK_BOOST = 7;

  @Inject private transient Player player;

  private AssignedSkillingTask assignedTask;
  private int completed;

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("get_daily_skilling_task_quantity")) {
      return getTaskQuantityText((Integer) args[0]);
    }
    if (name.equals("daily_skilling_task_quest")) {
      if (assignedTask == null || assignedTask.getOptionalTasks() != null) {
        chooseOptionalTask();
        return null;
      }
      var skillId = assignedTask.getTask().getSkill();
      var tName =
          (assignedTask.getTask() == null || assignedTask.getRemaining() <= 0)
              ? "None"
              : Skills.SKILL_NAMES[skillId];
      var tQuantity = getTaskQuantityText(Integer.MAX_VALUE);
      var tMessage = "Daily Skilling Task:<br>";
      tMessage += tName.equals("None") ? tQuantity : tName + ": " + tQuantity;
      var xp = SkillingTask.getExperience(player.getSkills().getLevelForXP(skillId));
      if ((completed + 1) % COMPLETED_TASK_BOOST == 0) {
        xp *= 2;
      }
      player.openDialogue(
          new MessageDialogue(
              tMessage
                  + "<br>Est. Rewards: "
                  + PNumber.formatNumber(xp)
                  + " xp, skilling mystery box and 1 bond"
                  + "<br>Every "
                  + COMPLETED_TASK_BOOST
                  + " tasks, you will receive twice as much xp."));
    }
    return null;
  }

  @Override
  public void login() {
    if (assignedTask != null && !PTime.getDate().equals(assignedTask.getDate())) {
      assignedTask = null;
    }
  }

  @Override
  public void create(
      int skillId,
      Npc npc,
      MapObject mapObject,
      PArrayList<Item> consumed,
      PArrayList<Item> created) {
    if (assignedTask == null) {
      return;
    }
    if (assignedTask.getTask() == null) {
      return;
    }
    if (assignedTask.getRemaining() == 0) {
      return;
    }
    var skillTask = assignedTask.getTask();
    if (skillTask.getSkill() != skillId) {
      return;
    }
    var matches = skillTask.getMatchCount(consumed, created, npc, mapObject);
    if (matches == 0) {
      return;
    }
    assignedTask.setRemaining(Math.max(0, assignedTask.getRemaining() - matches));
    if (assignedTask.getRemaining() > 0) {
      return;
    }
    completed++;
    var xp = SkillingTask.getExperience(player.getSkills().getLevelForXP(skillId));
    if (completed % COMPLETED_TASK_BOOST == 0) {
      xp *= 2;
    }
    xp = player.getSkills().addXp(skillId, xp);
    player.getInventory().addOrDropItem(ItemId.SKILLING_MYSTERY_BOX_32380);
    var plugin = player.getPlugin(BondPlugin.class);
    plugin.setPouch(plugin.getPouch() + 1);
    if (xp == 0) {
      return;
    }
    player
        .getGameEncoder()
        .sendMessage(
            "You have completed your daily skilling task and have been given "
                + PNumber.formatNumber(xp)
                + " experience and 1 bond!");
  }

  private void assignOptionalTasks() {
    var list = new ArrayList<SkillingTask>();
    var tasks = SkillingTask.getTasks(player);
    Collections.shuffle(tasks);
    var total = 2;
    if (player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
      total++;
    }
    if (player.isUsergroup(UserRank.RUBY_MEMBER)) {
      total++;
    }
    if (player.isUsergroup(UserRank.EMERALD_MEMBER)) {
      total++;
    }
    if (player.isUsergroup(UserRank.DIAMOND_MEMBER)) {
      total++;
    }
    if (player.isUsergroup(UserRank.DRAGONSTONE_MEMBER)) {
      total++;
    }
    if (player.isUsergroup(UserRank.ONYX_MEMBER)) {
      total++;
    }
    if (player.isUsergroup(UserRank.ZENYTE_MEMBER)) {
      total++;
    }
    for (var i = 0; i < total && i < tasks.size(); i++) {
      var task = tasks.get(i);
      list.add(task);
      while (tasks.contains(task)) {
        tasks.remove(task);
      }
    }
    if (list.size() == 1) {
      assignedTask = new AssignedSkillingTask(list.get(0));
    } else {
      assignedTask = new AssignedSkillingTask(list);
    }
  }

  private void assignTask(SkillingTask task) {
    if (task == null) {
      return;
    }
    assignedTask = new AssignedSkillingTask(task);
  }

  private void chooseOptionalTask() {
    if (assignedTask == null || !PTime.getDate().equals(assignedTask.getDate())) {
      assignOptionalTasks();
    }
    if (assignedTask.getOptionalTasks() == null) {
      return;
    }
    var tasks = assignedTask.getOptionalTasks();
    var options = new ArrayList<DialogueOption>();
    for (var t : tasks) {
      options.add(
          new DialogueOption(
              Skills.SKILL_NAMES[t.getSkill()] + ": " + t.getName(),
              (c, s) -> {
                assignTask(s < tasks.size() ? tasks.get(s) : null);
              }));
    }
    player.openDialogue(new LargeOptions2Dialogue(options));
  }

  private String getTaskQuantityText(int charLength) {
    var bonus = " (" + (completed % COMPLETED_TASK_BOOST) + "/" + COMPLETED_TASK_BOOST + ")";
    if (assignedTask == null || assignedTask.getOptionalTasks() != null) {
      return "None (Choose)" + bonus;
    }
    if (assignedTask.getTask() == null || assignedTask.getRemaining() <= 0) {
      return "None" + bonus;
    }
    return PString.max(
        assignedTask.getRemaining() + " " + assignedTask.getTask().getName() + bonus, charLength);
  }
}
