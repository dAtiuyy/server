package com.palidinodh.incomingpacket;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.io.Readers;
import com.palidinodh.osrscore.io.incomingpacket.InStreamKey;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.map.route.ProjectileRoute;
import com.palidinodh.playerplugin.grandexchange.GrandExchangePlugin;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;
import java.util.HashMap;
import java.util.Map;

class NpcOptionDecoder extends IncomingPacketDecoder {

  static final Map<Integer, NpcHandler> HANDLERS = new HashMap<>();

  static {
    load(false);
  }

  private static boolean basicAction(Player player, int option, Npc npc) {
    switch (npc.getDef().getLowerCaseName()) {
      case "banker":
      case "ghost banker":
      case "gnome banker":
        if (option == 0) {
          player.openOldDialogue("bank", 1);
        } else if (option == 2) {
          player.getBank().open();
        }
        return true;
      case "grand exchange clerk":
        switch (option) {
          case 0:
          case 2:
            player
                .getBank()
                .pinRequiredAction(
                    () ->
                        player
                            .getWidgetManager()
                            .sendInteractiveOverlay(WidgetId.GRAND_EXCHANGE_1024));
            break;
          case 3:
            player.getPlugin(GrandExchangePlugin.class).viewHistory();
            break;
          case 4:
            // player.getGrandExchange().exchangeItemSets();
            break;
        }
        return true;
    }
    return false;
  }

  public static synchronized void load(boolean force) {
    if (!force && !HANDLERS.isEmpty()) {
      return;
    }
    HANDLERS.clear();
    try {
      var classes = Readers.getClasses(NpcHandler.class, "com.palidinodh.incomingpacket.npc");
      for (var clazz : classes) {
        var handler = Readers.newInstance(clazz);
        var ids = NpcHandler.getIds(handler);
        if (ids == null) {
          continue;
        }
        for (var id : ids) {
          if (HANDLERS.containsKey(id)) {
            throw new RuntimeException(clazz.getName() + " - " + id + ": npc id already used.");
          }
          HANDLERS.put(id, handler);
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  @Override
  public boolean execute(Player player, Stream stream) {
    var option = getInt(InStreamKey.PACKET_OPTION);
    var npcIndex = getInt(InStreamKey.TARGET_INDEX);
    var ctrlRun = getInt(InStreamKey.CTRL_RUN);
    player.clearIdleTime();
    var npc = player.getWorld().getNpcByIndex(npcIndex);
    if (npc == null) {
      return false;
    }
    var message =
        "[NpcOption("
            + option
            + ")] index="
            + npcIndex
            + "; ctrlRun="
            + ctrlRun
            + "; id="
            + npc.getId()
            + "/"
            + NpcId.valueOf(npc.getId());
    if (Settings.getInstance().isLocal()) {
      PLogger.println(message);
    }
    if (player.getOptions().isDebug()) {
      player.getGameEncoder().sendMessage(message);
    }
    if (player.isLocked()) {
      return false;
    }
    if (!player.getMovement().isTeleportStateNone()) {
      return false;
    }
    if (player.getMovement().isViewing()) {
      return false;
    }
    if (player.getDialogue() != null && player.getDialogue().isDisableActions()) {
      return false;
    }
    player.clearAllActions(false, true);
    player.setFaceEntity(npc);
    if (npc.getSpawn().getMoveRange() == null && !npc.getDef().getName().equals("Fishing spot")) {
      player.getMovement().setFollowFront(npc);
    } else {
      player.getMovement().setFollowing(npc);
    }
    if (player.withinDistance(npc, 0)) {
      player.getMovement().followSameTile();
    }
    if (option == 1 && npc.getCombat().getMaxHitpoints() > 0) {
      if (player.getMagic().getAttackStyle().canUse()
          && !player.getCombat().isAttacking()
          && !player.getCombat().isHitDelayed()) {
        switch (player.getEquipment().getWeaponId()) {
          case ItemId.TRIDENT_OF_THE_SEAS_FULL:
          case ItemId.TRIDENT_OF_THE_SEAS:
          case ItemId.TRIDENT_OF_THE_SEAS_E:
          case ItemId.TRIDENT_OF_THE_SWAMP:
          case ItemId.TRIDENT_OF_THE_SWAMP_E:
          case ItemId.SANGUINESTI_STAFF:
          case ItemId.HARMONISED_NIGHTMARE_STAFF:
            break;
          default:
            player.getCombat().setHitDelay(2);
            break;
        }
      }
      player.getCombat().setAttackingEntity(npc);
      player.getCombat().setFollowing(npc);
      return false;
    }
    return true;
  }

  @Override
  public boolean complete(Player player) {
    var option = getInt(InStreamKey.PACKET_OPTION);
    var npcIndex = getInt(InStreamKey.TARGET_INDEX);
    var npc = player.getWorld().getNpcByIndex(npcIndex);
    if (npc == null || !npc.isVisible()) {
      return true;
    }
    if (player.isLocked()) {
      return false;
    }
    if (!player.getMovement().isTeleportStateNone()) {
      return false;
    }
    var definitionOption = npc.getDef().getOption(option);
    if (definitionOption == null) {
      return false;
    }
    var worldEvent =
        player.getWorld().getWorldEvents().getIf(e -> e.getNpcHandler(player, npc.getId()) != null);
    var handler = worldEvent == null ? null : worldEvent.getNpcHandler(player, npc.getId());
    if (handler == null) {
      handler = player.getArea().getNpcHandler(npc.getId());
    }
    if (handler == null) {
      handler = HANDLERS.get(npc.getId());
    }
    var type = handler != null ? handler.canReach(player, npc) : null;
    if (type == NpcHandler.ReachType.FALSE) {
      return false;
    }
    if (type == null || type == NpcHandler.ReachType.DEFAULT) {
      if (!npc.getMovement().isRouting()
          && npc.getSizeX() == 1
          && player.getX() != npc.getX()
          && player.getY() != npc.getY()) {
        return false;
      }
      var range = 1;
      if (npc.getDef().hasOption("bank")) {
        range = 2;
      }
      if (player.getMovement().isRouting() && npc.getMovement().isRouting()) {
        range++;
      }
      if (!player.withinDistance(npc, range)) {
        return false;
      }
      if (!ProjectileRoute.INSTANCE.allow(player, npc)) {
        return false;
      }
    }
    player.setFaceEntity(null);
    player.getMovement().setFollowing(null);
    if (player.getX() == npc.getX() || player.getY() == npc.getY()) {
      player.getMovement().clear();
    }
    player.setFaceTile(npc);
    Diary.getDiaries(player).forEach(d -> d.npcOption(player, definitionOption, npc));
    if (SkillContainer.npcOptionHooks(player, option, npc)) {
      return true;
    }
    if (player.getPluginList().containsIf(p -> p.npcOptionHook(option, npc))) {
      return true;
    }
    if (handler != null) {
      handler.npcOption(player, definitionOption, npc);
      return true;
    }
    if (basicAction(player, option, npc)) {
      return true;
    }
    player.getGameEncoder().sendMessage("Nothing interesting happens.");
    return true;
  }
}
