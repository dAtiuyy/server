package com.palidinodh.maparea.creepycrawly;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({5518, 5520})
public class CreepyCrawlyArea extends Area {

  @Override
  public boolean inMultiCombat() {
    return true;
  }
}
