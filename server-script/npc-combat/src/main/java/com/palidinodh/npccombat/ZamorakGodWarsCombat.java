package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class ZamorakGodWarsCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var gorakDrop =
        NpcCombatDrop.builder().gemDropTableDenominator(25).clue(ClueScrollType.HARD, 128);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_MED_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_CHAINBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RING_OF_LIFE)));
    gorakDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_RUBY, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DIAMOND, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NATURE_TALISMAN, 1, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_KITESHIELD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_PLATESKIRT)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_PLATELEGS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.IRON_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_THROWNAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_THROWNAXE)));
    gorakDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_SAPPHIRE_NOTED, 1, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_EMERALD_NOTED, 1, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.IRON_HALBERD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BRONZE_DAGGER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GORAK_CLAWS)));
    gorakDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BIG_BONES)));
    gorakDrop.table(dropTable.build());

    var gorakCombat = NpcCombatDefinition.builder();
    gorakCombat.id(NpcId.GORAK_149);
    gorakCombat.hitpoints(NpcCombatHitpoints.total(128));
    gorakCombat.stats(NpcCombatStats.builder().attackLevel(133).defenceLevel(135).build());
    gorakCombat.aggression(
        NpcCombatAggression.builder().type(NpcCombatAggression.Type.PLAYERS).always(true).build());
    gorakCombat.deathAnimation(4302).blockAnimation(4301);
    gorakCombat.drop(gorakDrop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.builder().maximum(14).ignorePrayer(true).build());
    style.animation(4300).attackSpeed(4);
    style.effect(
        NpcCombatEffect.builder()
            .statDrain(Skills.ATTACK, 1)
            .statDrain(Skills.DEFENCE, 1)
            .statDrain(Skills.STRENGTH, 1)
            .statDrain(Skills.RANGED, 1)
            .statDrain(Skills.PRAYER, 1)
            .statDrain(Skills.MAGIC, 1)
            .build());
    gorakCombat.style(style.build());

    var hellhoundDrop = NpcCombatDrop.builder().clue(ClueScrollType.HARD, 64);
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(32768);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SMOULDERING_STONE)));
    hellhoundDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    hellhoundDrop.table(dropTable.build());

    var hellhoundCombat = NpcCombatDefinition.builder();
    hellhoundCombat.id(NpcId.HELLHOUND_127);
    hellhoundCombat.hitpoints(NpcCombatHitpoints.total(116));
    hellhoundCombat.stats(NpcCombatStats.builder().attackLevel(107).defenceLevel(106).build());
    hellhoundCombat.aggression(
        NpcCombatAggression.builder().type(NpcCombatAggression.Type.PLAYERS).always(true).build());
    hellhoundCombat.deathAnimation(6576).blockAnimation(6578);
    hellhoundCombat.drop(hellhoundDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(13));
    style.animation(6579).attackSpeed(4);
    hellhoundCombat.style(style.build());

    var impDrop = NpcCombatDrop.builder();
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(25);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ENSOULED_IMP_HEAD_13454)));
    impDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BRONZE_BOLTS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLUE_WIZARD_HAT)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHEFS_HAT)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BREAD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CABBAGE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COOKED_MEAT)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_BEAD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RED_BEAD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WHITE_BEAD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YELLOW_BEAD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BALL_OF_WOOL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.EGG)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TINDERBOX)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BUCKET)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.HAMMER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CADAVA_BERRIES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BREAD_DOUGH)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_CHICKEN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ASHES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BURNT_MEAT)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BURNT_BREAD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JUG)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.POT_OF_FLOUR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CLAY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.POTION)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FLIER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MIND_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JUG_OF_WATER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BUCKET_OF_WATER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHEARS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRAIN)));
    impDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ASHES)));
    impDrop.table(dropTable.build());

    var impCombat = NpcCombatDefinition.builder();
    impCombat.id(NpcId.IMP_7);
    impCombat.hitpoints(NpcCombatHitpoints.total(10));
    impCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(5)
            .defenceLevel(6)
            .bonus(BonusType.DEFENCE_MAGIC, -42)
            .bonus(BonusType.DEFENCE_RANGED, -42)
            .build());
    impCombat.aggression(
        NpcCombatAggression.builder().type(NpcCombatAggression.Type.PLAYERS).always(true).build());
    impCombat.type(NpcCombatType.DEMON).deathAnimation(172);
    impCombat.drop(impDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(1));
    style.animation(173).attackSpeed(4);
    impCombat.style(style.build());

    var werewolfDrop =
        NpcCombatDrop.builder().clue(ClueScrollType.MEDIUM, 512).clue(ClueScrollType.EASY, 128);
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_DWARF_WEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BRASS_KEY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WHITE_BERRIES)));
    werewolfDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_ARROW, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_FULL_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_SCIMITAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_SQ_SHIELD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_CHAINBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_MED_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_JAVELIN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_HARRALANDER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_IRIT_LEAF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TINDERBOX)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.IRON_ORE, 1, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COAL, 2)));
    werewolfDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_CHICKEN, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_BEEF, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_BEAR_MEAT, 1, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JUG_OF_WINE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_SCIMITAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_AXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TARROMIN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_GUAM_LEAF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_MARRENTILL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WEREWOLF_BONE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FUR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GREY_WOLF_FUR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_BAR)));
    werewolfDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WOLF_BONES)));
    werewolfDrop.table(dropTable.build());

    var werewolfCombat = NpcCombatDefinition.builder();
    werewolfCombat.id(NpcId.WEREWOLF_93);
    werewolfCombat.hitpoints(NpcCombatHitpoints.total(100));
    werewolfCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(70)
            .defenceLevel(70)
            .bonus(BonusType.DEFENCE_MAGIC, 60)
            .build());
    werewolfCombat.aggression(
        NpcCombatAggression.builder().type(NpcCombatAggression.Type.PLAYERS).always(true).build());
    werewolfCombat.deathAnimation(6537).blockAnimation(6538);
    werewolfCombat.drop(werewolfDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(8));
    style.animation(6536).attackSpeed(6);
    werewolfCombat.style(style.build());

    var spiritualMageDrop =
        NpcCombatDrop.builder().gemDropTableDenominator(64).clue(ClueScrollType.HARD, 128);
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_BOOTS)));
    spiritualMageDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ASTRAL_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MIST_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DUST_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MUD_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MIND_RUNE, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIPOISON_3_NOTED, 5)));
    spiritualMageDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 90)));
    spiritualMageDrop.table(dropTable.build());

    var spiritualMageCombat = NpcCombatDefinition.builder();
    spiritualMageCombat.id(NpcId.SPIRITUAL_MAGE_121_3161);
    spiritualMageCombat.hitpoints(NpcCombatHitpoints.total(106));
    spiritualMageCombat.stats(NpcCombatStats.builder().magicLevel(180).defenceLevel(61).build());
    spiritualMageCombat.slayer(NpcCombatSlayer.builder().level(83).build());
    spiritualMageCombat.aggression(
        NpcCombatAggression.builder().type(NpcCombatAggression.Type.PLAYERS).always(true).build());
    spiritualMageCombat.type(NpcCombatType.SPECTRAL).deathAnimation(836).blockAnimation(404);
    spiritualMageCombat.drop(spiritualMageDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(15));
    style.animation(811).attackSpeed(4);
    style.targetGraphic(new Graphic(78, 100));
    spiritualMageCombat.style(style.build());

    return Arrays.asList(
        gorakCombat.build(),
        hellhoundCombat.build(),
        impCombat.build(),
        werewolfCombat.build(),
        spiritualMageCombat.build());
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    player.getArea().script("increase_zamorak_killcount");
    if (npc.getArea().inWilderness() && PRandom.randomE(60) == 0) {
      npc.getController().addMapItem(new Item(ItemId.ECUMENICAL_KEY, 1), dropTile, player);
    }
  }

  @Override
  public boolean canBeAggressiveHook(Entity opponent) {
    if (!opponent.isPlayer()) {
      return true;
    }
    var player = opponent.asPlayer();
    return !player.getEquipment().hasItemIgnoreCase("Zamorak")
        && player.getEquipment().getShieldId() != ItemId.UNHOLY_BOOK
        && player.getEquipment().getAmmoId() != ItemId.UNHOLY_BLESSING
        && player.getEquipment().getNeckId() != ItemId.UNHOLY_SYMBOL
        && player.getEquipment().getWeaponId() != ItemId.STAFF_OF_THE_DEAD
        && player.getEquipment().getWeaponId() != ItemId.TOXIC_STAFF_UNCHARGED
        && player.getEquipment().getWeaponId() != ItemId.TOXIC_STAFF_OF_THE_DEAD
        && player.getEquipment().getWeaponId() != ItemId.STAFF_OF_BALANCE
        && player.getEquipment().getWeaponId() != ItemId.INQUISITORS_MACE
        && player.getEquipment().getHeadId() != ItemId.INQUISITORS_GREAT_HELM
        && player.getEquipment().getChestId() != ItemId.INQUISITORS_HAUBERK
        && player.getEquipment().getLegId() != ItemId.INQUISITORS_PLATESKIRT;
  }
}
