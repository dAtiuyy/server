package com.palidinodh.incomingpacket;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.incomingpacket.misc.ItemOnItemAction;
import com.palidinodh.incomingpacket.misc.UseWidgetAction;
import com.palidinodh.osrscore.io.incomingpacket.InStreamKey;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.Runecrafting;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;

class UseWidgetDecoder {

  static class WidgetOnWidgetDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      var useWidgetHash = getInt(InStreamKey.USE_WIDGET_HASH);
      var useWidgetId = WidgetHandler.getWidgetId(useWidgetHash);
      var useWidgetChildId = WidgetHandler.getWidgetChildId(useWidgetHash);
      var useWidgetSlot = getInt(InStreamKey.USE_WIDGET_SLOT);
      var useItemId = getInt(InStreamKey.USE_ITEM_ID);
      var onWidgetHash = getInt(InStreamKey.ON_WIDGET_HASH);
      if (!containsKey(InStreamKey.ON_WIDGET_HASH)) {
        onWidgetHash = useWidgetHash;
      }
      var onWidgetId = WidgetHandler.getWidgetId(onWidgetHash);
      var onWidgetChildId = WidgetHandler.getWidgetChildId(onWidgetHash);
      var onWidgetSlot = getInt(InStreamKey.ON_WIDGET_SLOT);
      var onItemId = getInt(InStreamKey.ON_ITEM_ID);
      player.clearIdleTime();
      var message =
          "[WidgetOnWidget] useWidgetId="
              + useWidgetId
              + "/"
              + WidgetId.valueOf(useWidgetId)
              + "; useWidgetChildId="
              + useWidgetChildId
              + "; onWidgetId="
              + onWidgetId
              + "/"
              + WidgetId.valueOf(onWidgetId)
              + "; onWidgetChildId="
              + onWidgetChildId
              + "; useItemId="
              + useItemId
              + "/"
              + ItemId.valueOf(useItemId)
              + "; useWidgetSlot="
              + useWidgetSlot
              + "; onItemId="
              + onItemId
              + "/"
              + ItemId.valueOf(onItemId)
              + "; onWidgetSlot="
              + onWidgetSlot;
      if (Settings.getInstance().isLocal()) {
        PLogger.println(message);
      }
      if (player.getOptions().isDebug()) {
        player.getGameEncoder().sendMessage(message);
      }
      if (player.isLocked()) {
        return false;
      }
      if (!player.getMovement().isTeleportStateNone()) {
        return false;
      }
      if (player.getMovement().isViewing()) {
        return false;
      }
      if (!player.getWidgetManager().hasWidget(useWidgetId)) {
        return false;
      }
      if (!player.getWidgetManager().hasWidget(onWidgetId)) {
        return false;
      }
      if (player.getDialogue() != null && player.getDialogue().isDisableActions()) {
        return false;
      }
      if (useWidgetId == WidgetId.INVENTORY
          && onWidgetId == WidgetId.INVENTORY
          && useItemId != -1
          && onItemId != -1) {
        if (player.getInventory().getId(useWidgetSlot) != useItemId
            || player.getInventory().getId(onWidgetSlot) != onItemId) {
          return false;
        }
        if (player
            .getController()
            .itemOnItemHook(
                useWidgetId,
                useWidgetChildId,
                onWidgetId,
                onWidgetChildId,
                useWidgetSlot,
                onWidgetSlot,
                useItemId,
                onItemId)) {
          return false;
        }
      }
      return true;
    }

    @Override
    public boolean complete(Player player) {
      var useWidgetHash = getInt(InStreamKey.USE_WIDGET_HASH);
      var useWidgetId = WidgetHandler.getWidgetId(useWidgetHash);
      var useWidgetChildId = WidgetHandler.getWidgetChildId(useWidgetHash);
      var useWidgetSlot = getInt(InStreamKey.USE_WIDGET_SLOT);
      var useItemId = getInt(InStreamKey.USE_ITEM_ID);
      var onWidgetHash = getInt(InStreamKey.ON_WIDGET_HASH);
      if (!containsKey(InStreamKey.ON_WIDGET_HASH)) {
        onWidgetHash = useWidgetHash;
      }
      var onWidgetId = WidgetHandler.getWidgetId(onWidgetHash);
      var onWidgetChildId = WidgetHandler.getWidgetChildId(onWidgetHash);
      var onWidgetSlot = getInt(InStreamKey.ON_WIDGET_SLOT);
      var onItemId = getInt(InStreamKey.ON_ITEM_ID);
      if (!player.getWidgetManager().hasWidget(useWidgetId)) {
        return true;
      }
      if (!player.getWidgetManager().hasWidget(onWidgetId)) {
        return true;
      }
      if (useWidgetId == WidgetId.INVENTORY
          && useItemId != -1
          && player.getInventory().getId(useWidgetSlot) != useItemId) {
        return true;
      }
      if (onWidgetId == WidgetId.INVENTORY
          && onItemId != -1
          && player.getInventory().getId(onWidgetSlot) != onItemId) {
        return true;
      }
      if (player.isLocked() || player.getMovement().isViewing()) {
        return true;
      }
      if (SkillContainer.widgetOnWidgetHooks(
          player,
          useWidgetId,
          useWidgetChildId,
          onWidgetId,
          onWidgetChildId,
          useWidgetSlot,
          useItemId,
          onWidgetSlot,
          onItemId)) {
        return true;
      }
      if (useWidgetId == WidgetId.INVENTORY
          && onWidgetId == WidgetId.INVENTORY
          && useItemId != -1
          && onItemId != -1) {
        var worldEvent =
            player
                .getWorld()
                .getWorldEvents()
                .getIf(e -> e.getItemHandler(player, useItemId) != null);
        var itemHandler = worldEvent == null ? null : worldEvent.getItemHandler(player, useItemId);
        if (itemHandler == null) {
          itemHandler = PlayerPlugin.getItemHandler(useItemId);
        }
        if (itemHandler != null
            && itemHandler.itemOnItem(
                player,
                player.getInventory().getItem(useWidgetSlot),
                player.getInventory().getItem(onWidgetSlot))) {
          return true;
        }
        worldEvent =
            player
                .getWorld()
                .getWorldEvents()
                .getIf(e -> e.getItemHandler(player, onItemId) != null);
        itemHandler = worldEvent == null ? null : worldEvent.getItemHandler(player, onItemId);
        if (itemHandler == null) {
          itemHandler = PlayerPlugin.getItemHandler(onItemId);
        }
        if (itemHandler != null
            && itemHandler.itemOnItem(
                player,
                player.getInventory().getItem(useWidgetSlot),
                player.getInventory().getItem(onWidgetSlot))) {
          return true;
        }
        ItemOnItemAction.doAction(
            player,
            useWidgetId,
            useWidgetChildId,
            onWidgetId,
            onWidgetChildId,
            useWidgetSlot,
            onWidgetSlot,
            useItemId,
            onItemId);
        return true;
      }
      var widgetHandler = WidgetHandler.getHandler(player, useWidgetId);
      if (widgetHandler == null) {
        widgetHandler = WidgetDecoder.WIDGET_HANDLERS.get(useWidgetId);
      }
      if (widgetHandler != null
          && widgetHandler.widgetOnWidget(
              player,
              useWidgetId,
              useWidgetChildId,
              onWidgetId,
              onWidgetChildId,
              useWidgetSlot,
              useItemId,
              onWidgetSlot,
              onItemId)) {
        return true;
      }
      widgetHandler = WidgetHandler.getHandler(player, onWidgetId);
      if (widgetHandler == null) {
        widgetHandler = WidgetDecoder.WIDGET_HANDLERS.get(onWidgetId);
      }
      if (widgetHandler != null) {
        widgetHandler.widgetOnWidget(
            player,
            useWidgetId,
            useWidgetChildId,
            onWidgetId,
            onWidgetChildId,
            useWidgetSlot,
            useItemId,
            onWidgetSlot,
            onItemId);
      }
      return true;
    }
  }

  static class WidgetOnNpcDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      var widgetHash = getInt(InStreamKey.WIDGET_HASH);
      var widgetId = WidgetHandler.getWidgetId(widgetHash);
      var widgetChildId = WidgetHandler.getWidgetChildId(widgetHash);
      var widgetSlot = getInt(InStreamKey.WIDGET_SLOT);
      var itemId = getInt(InStreamKey.ITEM_ID);
      var targetIndex = getInt(InStreamKey.TARGET_INDEX);
      var ctrlRun = getInt(InStreamKey.CTRL_RUN);
      player.clearIdleTime();
      var npc = player.getWorld().getNpcByIndex(targetIndex);
      if (npc == null) {
        return false;
      }
      var message =
          "[WidgetOnNpc widgetId="
              + widgetId
              + "/"
              + WidgetId.valueOf(widgetId)
              + "; widgetChildId="
              + widgetChildId
              + "; itemId="
              + itemId
              + "/"
              + ItemId.valueOf(itemId)
              + "; widgetSlot="
              + widgetSlot
              + "; targetIndex="
              + targetIndex
              + "; ctrlRun="
              + ctrlRun
              + "; npc="
              + npc.getId()
              + "/"
              + NpcId.valueOf(npc.getId());
      if (Settings.getInstance().isLocal()) {
        PLogger.println(message);
      }
      if (player.getOptions().isDebug()) {
        player.getGameEncoder().sendMessage(message);
      }
      if (player.isLocked()) {
        return false;
      }
      if (!player.getMovement().isTeleportStateNone()) {
        return false;
      }
      if (player.getMovement().isViewing()) {
        return false;
      }
      if (player.getDialogue() != null && player.getDialogue().isDisableActions()) {
        return false;
      }
      player.clearAllActions(false, true);
      player.setFaceEntity(npc);
      player.getMovement().setFollowing(npc);
      if (player.withinDistance(npc, 0)) {
        player.getMovement().followSameTile();
      }
      if (widgetId == WidgetId.SPELLBOOK) {
        player.getMagic().setSingleSpellId(widgetChildId);
        player.getCombat().setAttackingEntity(npc);
        player.getCombat().setFollowing(npc);
        player.setFaceTile(npc);
        return false;
      }
      player.getMovement().fullRoute(npc, ctrlRun);
      return true;
    }

    @Override
    public boolean complete(Player player) {
      var widgetHash = getInt(InStreamKey.WIDGET_HASH);
      var widgetId = WidgetHandler.getWidgetId(widgetHash);
      var widgetChildId = WidgetHandler.getWidgetChildId(widgetHash);
      var widgetSlot = getInt(InStreamKey.WIDGET_SLOT);
      var itemId = getInt(InStreamKey.ITEM_ID);
      var targetIndex = getInt(InStreamKey.TARGET_INDEX);
      var npc = player.getWorld().getNpcByIndex(targetIndex);
      if (npc == null || !npc.isVisible()) {
        return true;
      }
      if (player.isLocked()) {
        return false;
      }
      if (!player.getMovement().isTeleportStateNone()) {
        return false;
      }
      if (!player.getWidgetManager().hasWidget(widgetId)) {
        return true;
      }
      if (widgetId == WidgetId.INVENTORY && itemId == -1) {
        return true;
      }
      if (widgetId == WidgetId.INVENTORY && player.getInventory().getId(widgetSlot) != itemId) {
        return true;
      }
      var widgetHandler = WidgetHandler.getHandler(player, widgetId);
      var worldEvent =
          player
              .getWorld()
              .getWorldEvents()
              .getIf(e -> e.getNpcHandler(player, npc.getId()) != null);
      var npcHandler = worldEvent == null ? null : worldEvent.getNpcHandler(player, npc.getId());
      if (npcHandler == null) {
        npcHandler = player.getArea().getNpcHandler(npc.getId());
      }
      if (npcHandler == null) {
        npcHandler = NpcOptionDecoder.HANDLERS.get(npc.getId());
      }
      var type = npcHandler != null ? npcHandler.canReach(player, npc) : null;
      if (type == NpcHandler.ReachType.FALSE) {
        return false;
      }
      if (type == null || type == NpcHandler.ReachType.DEFAULT) {
        if (!npc.getMovement().isRouting()
            && npc.getSizeX() == 1
            && player.getX() != npc.getX()
            && player.getY() != npc.getY()) {
          return false;
        }
        var range = 1;
        if (npc.getDef().hasOption("bank")) {
          range = 2;
        }
        if (player.getMovement().isRouting() && npc.getMovement().isRouting()) {
          range++;
        }
        if (!player.withinDistance(npc, range)) {
          return false;
        }
      }
      player.getMovement().clear();
      player.setFaceTile(npc);
      if (player
          .getPluginList()
          .containsIf(p -> p.widgetOnNpcHook(player, widgetId, widgetChildId, widgetSlot, npc))) {
        return true;
      }
      if (widgetHandler != null) {
        widgetHandler.widgetOnNpc(player, widgetId, widgetChildId, widgetSlot, npc);
        return true;
      }
      if (npcHandler != null && widgetId == WidgetId.INVENTORY) {
        npcHandler.itemOnNpc(player, widgetSlot, itemId, npc);
        return true;
      }
      player.getGameEncoder().sendMessage("Nothing interesting happens.");
      return true;
    }
  }

  static class WidgetOnPlayerDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      var widgetHash = getInt(InStreamKey.WIDGET_HASH);
      var widgetId = WidgetHandler.getWidgetId(widgetHash);
      var widgetChildId = WidgetHandler.getWidgetChildId(widgetHash);
      var widgetSlot = getInt(InStreamKey.WIDGET_SLOT);
      var itemId = getInt(InStreamKey.ITEM_ID);
      var targetIndex = getInt(InStreamKey.TARGET_INDEX);
      var ctrlRun = getInt(InStreamKey.CTRL_RUN);
      player.clearIdleTime();
      var targetPlayer = player.getWorld().getPlayerByIndex(targetIndex);
      if (targetPlayer == null) {
        return false;
      }
      var message =
          "[WidgetOnPlayer widgetId="
              + widgetId
              + "/"
              + WidgetId.valueOf(widgetId)
              + "; widgetChildId="
              + widgetChildId
              + "; itemId="
              + itemId
              + "/"
              + ItemId.valueOf(itemId)
              + "; widgetSlot="
              + widgetSlot
              + "; targetIndex="
              + targetIndex
              + "; ctrlRun="
              + ctrlRun
              + "; player="
              + targetPlayer.getId()
              + "/"
              + targetPlayer.getUsername();
      if (Settings.getInstance().isLocal()) {
        PLogger.println(message);
      }
      if (player.getOptions().isDebug()) {
        player.getGameEncoder().sendMessage(message);
      }
      if (player.isLocked()) {
        return false;
      }
      if (!player.getMovement().isTeleportStateNone()) {
        return false;
      }
      if (player.getMovement().isViewing()) {
        return false;
      }
      if (player.getDialogue() != null && player.getDialogue().isDisableActions()) {
        return false;
      }
      player.clearAllActions(false, true);
      player.setFaceEntity(targetPlayer);
      player.getMovement().setFollowing(targetPlayer);
      if (player.withinDistance(targetPlayer, 0)) {
        player.getMovement().followSameTile();
      }
      if (widgetId == WidgetId.SPELLBOOK) {
        player.getMagic().setSingleSpellId(widgetChildId);
        player.getCombat().setAttackingEntity(targetPlayer);
        player.getCombat().setFollowing(targetPlayer);
        player.setFaceTile(targetPlayer);
        return false;
      }
      player.getMovement().fullRoute(targetPlayer, ctrlRun);
      return true;
    }

    @Override
    public boolean complete(Player player) {
      var widgetHash = getInt(InStreamKey.WIDGET_HASH);
      var widgetId = WidgetHandler.getWidgetId(widgetHash);
      var targetIndex = getInt(InStreamKey.TARGET_INDEX);
      var targetPlayer = player.getWorld().getPlayerByIndex(targetIndex);
      if (targetPlayer == null) {
        return true;
      }
      if (player.isLocked()) {
        return false;
      }
      if (!player.getMovement().isTeleportStateNone()) {
        return false;
      }
      if (!player.getWidgetManager().hasWidget(widgetId)) {
        return true;
      }
      if (!player.withinDistance(targetPlayer, 1)) {
        return false;
      }
      if (!player.getController().routeAllow(targetPlayer)) {
        return false;
      }
      player.getMovement().clear();
      player.setFaceTile(targetPlayer);
      return true;
    }
  }

  static class WidgetOnMapObjectDecoder extends IncomingPacketDecoder {

    @Override
    public boolean execute(Player player, Stream stream) {
      var widgetHash = getInt(InStreamKey.WIDGET_HASH);
      var widgetId = WidgetHandler.getWidgetId(widgetHash);
      var widgetChildId = WidgetHandler.getWidgetChildId(widgetHash);
      var widgetSlot = getInt(InStreamKey.WIDGET_SLOT);
      var itemId = getInt(InStreamKey.ITEM_ID);
      var mapObjectId = getInt(InStreamKey.MAP_OBJECT_ID);
      var tileX = getInt(InStreamKey.TILE_X);
      var tileY = getInt(InStreamKey.TILE_Y);
      var ctrlRun = getInt(InStreamKey.CTRL_RUN);
      player.clearIdleTime();
      var mapObject =
          player.getController().getMapObject(mapObjectId, tileX, tileY, player.getClientHeight());
      if (mapObject == null) {
        return false;
      }
      if (player.getHeight() != player.getClientHeight()) {
        if (mapObject.getDef().hasOption("open") || mapObject.getDef().hasOption("close")) {
          return false;
        }
      }
      var message =
          "[WidgetOnMapObject] widgetId="
              + widgetId
              + "; widgetChildId="
              + widgetChildId
              + "; widgetSlot="
              + widgetSlot
              + "; itemId="
              + itemId
              + "; mapObjectId="
              + mapObjectId
              + "; tileX="
              + tileX
              + "; tileY="
              + tileY
              + "; ctrlRun="
              + ctrlRun;
      if (Settings.getInstance().isLocal()) {
        PLogger.println(message);
      }
      if (player.getOptions().isDebug()) {
        player.getGameEncoder().sendMessage(message);
      }
      if (player.isLocked()) {
        return false;
      }
      if (!player.getMovement().isTeleportStateNone()) {
        return false;
      }
      if (player.getMovement().isViewing()) {
        return false;
      }
      if (player.getDialogue() != null && player.getDialogue().isDisableActions()) {
        return false;
      }
      player.clearAllActions(false, true);
      player.getMovement().fullRoute(mapObject, ctrlRun);
      return true;
    }

    @Override
    public boolean complete(Player player) {
      var widgetHash = getInt(InStreamKey.WIDGET_HASH);
      var widgetId = WidgetHandler.getWidgetId(widgetHash);
      var widgetChildId = WidgetHandler.getWidgetChildId(widgetHash);
      var widgetSlot = getInt(InStreamKey.WIDGET_SLOT);
      var itemId = getInt(InStreamKey.ITEM_ID);
      var mapObjectId = getInt(InStreamKey.MAP_OBJECT_ID);
      var tileX = getInt(InStreamKey.TILE_X);
      var tileY = getInt(InStreamKey.TILE_Y);
      var mapObject =
          player.getController().getMapObject(mapObjectId, tileX, tileY, player.getClientHeight());
      if (mapObject == null) {
        return true;
      }
      if (player.isLocked()) {
        return false;
      }
      if (!player.getMovement().isTeleportStateNone()) {
        return false;
      }
      if (!player.getWidgetManager().hasWidget(widgetId)) {
        return true;
      }
      var range = 1;
      if (mapObject.getType() >= 4 && mapObject.getType() <= 8) {
        range = 0;
      }
      var canReach = !player.getMovement().isRouting() && player.withinDistanceC(mapObject, range);
      if (mapObject.getId() == 5249) {
        canReach = player.withinDistanceC(mapObject, range);
      }
      if (!canReach) {
        return false;
      }
      player.getMovement().clear();
      player.setFaceTile(mapObject);
      if (widgetId == WidgetId.INVENTORY && itemId == -1) {
        return true;
      }
      if (widgetId == WidgetId.INVENTORY && player.getInventory().getId(widgetSlot) != itemId) {
        return true;
      }
      if (player
          .getController()
          .widgetOnMapObjectHook(widgetId, widgetChildId, widgetSlot, mapObject)) {
        return true;
      }
      if (SkillContainer.widgetOnMapObjectHooks(
          player, widgetId, widgetChildId, widgetSlot, itemId, mapObject)) {
        return true;
      }
      if (player
          .getFarming()
          .widgetOnMapObjectHook(widgetId, widgetChildId, widgetSlot, mapObject)) {
        return true;
      }
      if (Runecrafting.widgetOnMapObjectHook(
          player, widgetId, widgetChildId, widgetSlot, mapObject)) {
        return true;
      }
      var widgetHandler = WidgetHandler.getHandler(player, widgetId);
      if (widgetHandler != null) {
        widgetHandler.widgetOnMapObject(player, widgetId, widgetChildId, widgetSlot, mapObject);
        return true;
      }
      var worldEvent =
          player
              .getWorld()
              .getWorldEvents()
              .getIf(e -> e.getMapObjectHandler(player, mapObjectId) != null);
      var mapObjectHandler =
          worldEvent == null ? null : worldEvent.getMapObjectHandler(player, mapObjectId);
      if (mapObjectHandler == null) {
        mapObjectHandler = player.getArea().getMapObjectHandler(mapObjectId);
      }
      if (mapObjectHandler != null && widgetId == WidgetId.INVENTORY) {
        mapObjectHandler.itemOnMapObject(
            player, player.getInventory().getItem(widgetSlot), mapObject);
        return true;
      }
      UseWidgetAction.doActionMapObject(player, widgetId, widgetChildId, widgetSlot, mapObject);
      return true;
    }
  }
}
