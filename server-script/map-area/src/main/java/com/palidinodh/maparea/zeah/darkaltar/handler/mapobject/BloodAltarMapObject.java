package com.palidinodh.maparea.zeah.darkaltar.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PArrayList;

@ReferenceId(ObjectId.BLOOD_ALTAR)
class BloodAltarMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.carryingItem(ItemId.DARK_ESSENCE_FRAGMENTS)
        && player.getCharges().getDarkEssenceFragments() > 0) {
      var runecraftingLvl = player.getSkills().getLevel(Skills.RUNECRAFTING) >= 77;
      var fragmentCharges = player.getCharges().getDarkEssenceFragments();
      if (runecraftingLvl) {
        double xp = fragmentCharges * 24;
        if (player.getEquipment().wearingElidinisOutfit()) {
          xp *= 1.1;
        }
        player.getSkills().addXp(Skills.RUNECRAFTING, (int) xp);
        player.getCharges().decreaseDarkEssenceCharges(fragmentCharges);
        var consumed = new PArrayList<Item>();
        var created = new PArrayList<Item>();
        var consumedItem = new Item(ItemId.DARK_ESSENCE_FRAGMENTS);
        consumed.add(new Item(consumedItem.getId(), fragmentCharges));
        if (player.isRelicUnlocked(BondRelicType.BIGGER_HARVEST_RUNECRAFTING)
            && PRandom.randomE(1) == 0) {
          fragmentCharges *= 2;
        }
        var createdItem = new Item(ItemId.BLOOD_RUNE, fragmentCharges);
        created.add(new Item(createdItem));
        player.getInventory().deleteItem(consumedItem);
        player.getInventory().addOrDropItem(createdItem);
        player
            .getPluginList()
            .forEach(p -> p.create(Skills.RUNECRAFTING, null, null, consumed, created));
        player.setAnimation(791);
        player.setGraphic(186, 100);
      } else {
        player
            .getGameEncoder()
            .sendMessage("You need a runecrafting level of 77 to craft Blood runes.");
      }
    } else {
      player.getInventory().deleteItem(ItemId.DARK_ESSENCE_FRAGMENTS);
    }
  }
}
