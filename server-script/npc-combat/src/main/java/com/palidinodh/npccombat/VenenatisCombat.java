package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class VenenatisCombat extends NpcCombat {

  private static final NpcCombatStyle CURSE =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MAGIC)
          .animation(5330)
          .attackSpeed(1)
          .castGraphic(new Graphic(177, 92))
          .targetGraphic(new Graphic(172, 124))
          .damage(NpcCombatDamage.builder().splashOnMiss(true).ignorePrayer(true).build())
          .projectile(NpcCombatProjectile.id(171))
          .build();

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .clue(ClueScrollType.ELITE, 100)
            .pet(ItemId.VENENATIS_SPIDERLING, 2000)
            .additionalPlayers(3);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(512).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TREASONOUS_RING)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(256).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_2H_SWORD)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(170).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_PICKAXE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YEW_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PALM_TREE_SEED)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_SNAPDRAGON_NOTED, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_FISHING_BAIT, 375)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_LOGS_NOTED, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LIMPWURT_ROOT_NOTED, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DRAGONSTONE)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.RED_SPIDERS_EGGS_NOTED, 500)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DIAMOND_BOLTS_E, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_KNIFE, 60)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPERCOMPOST_NOTED, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNICORN_HORN_NOTED, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GOLD_ORE_NOTED, 300)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CANNONBALL, 250)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIDOTE_PLUS_PLUS_4_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ONYX_BOLT_TIPS, 60)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL_NOTED, 1, 5)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PICKAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 400)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 300)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 200)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_RUBY_NOTED, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DIAMOND_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_CRAB_NOTED, 8)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPER_RESTORE_4, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 15000, 20000)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.VENENATIS_464_6610);
    combat.hitpoints(NpcCombatHitpoints.total(255));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(470)
            .magicLevel(150)
            .defenceLevel(490)
            .bonus(BonusType.MELEE_DEFENCE, 260)
            .bonus(BonusType.DEFENCE_MAGIC, 850)
            .bonus(BonusType.DEFENCE_RANGED, 100)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(8).checkWhileAttacking(true).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.focus(NpcCombatFocus.builder().keepWithinDistance(1).singleTargetFocus(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.deathAnimation(5321).blockAnimation(5320);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(49));
    style.animation(5319).attackSpeed(4);
    style.effect(NpcCombatEffect.builder().includeMiss(true).chance(25).poison(8).build());
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(49));
    style.animation(5330).attackSpeed(4);
    style.castGraphic(new Graphic(164, 92)).targetGraphic(new Graphic(166, 124));
    style.projectile(NpcCombatProjectile.id(165));
    style.effect(NpcCombatEffect.builder().includeMiss(true).chance(25).poison(8).build());
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public NpcCombatStyle attackTickCombatStyleHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (!(getAttackingEntity() instanceof Player) || PRandom.randomE(16) != 0) {
      return combatStyle;
    }
    return CURSE;
  }

  @Override
  public void applyAttackEndHook(
      NpcCombatStyle combatStyle, Entity opponent, int applyAttackLoopCount, HitEvent hitEvent) {
    if (PRandom.randomE(16) == 0) {
      opponent.setLock(1);
      var projectile =
          Graphic.Projectile.builder()
              .id(1254)
              .entity(opponent)
              .startTile(npc)
              .speed(getProjectileSpeed(opponent))
              .build();
      sendMapProjectile(projectile);
      opponent.setGraphic(245, 100);
      var hitEvent2 = new HitEvent(new Hit(PRandom.randomI(50)), npc);
      opponent.getCombat().addHitEvent(hitEvent2);
      if (opponent.isPlayer()) {
        var player = opponent.asPlayer();
        player
            .getGameEncoder()
            .sendMessage("Venenatis hurls her web at you, sticking you to the ground.");
      }
    }
    if (!opponent.isPlayer()) {
      return;
    }
    var player = opponent.asPlayer();
    if (combatStyle != CURSE || hitEvent.isDefenceBlocked()) {
      return;
    }
    var amount = 1 + PRandom.randomI(19);
    player.getSkills().changeStat(Skills.PRAYER, -amount);
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (!npc.getController().inMultiCombat() && damage > 6) {
      damage = 6;
    }
    return damage;
  }
}
