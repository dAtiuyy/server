package com.palidinodh.maparea.kandarin.observatory.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class ObservatoryNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2492, 3184), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(2, new Tile(2488, 3176), NpcId.GILETH));
    spawns.add(new NpcSpawn(new Tile(2469, 3158), NpcId.CLOTHEARS));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2465, 3183), NpcId.SMELLYTOES));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2467, 3183), NpcId.GREASYCHEEKS));
    spawns.add(new NpcSpawn(new Tile(2470, 3185), NpcId.CREAKYKNEES));
    spawns.add(new NpcSpawn(4, new Tile(2462, 3198), NpcId.BLACK_BEAR_19));

    return spawns;
  }
}
