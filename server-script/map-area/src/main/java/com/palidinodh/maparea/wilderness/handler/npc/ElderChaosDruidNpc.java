package com.palidinodh.maparea.wilderness.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.ValueEnteredEvent;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Prayer;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;

@ReferenceId(NpcId.ELDER_CHAOS_DRUID)
class ElderChaosDruidNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    openDialogue(player, player.getInventory().getSlotByNameIgnoreCase("bone"));
  }

  @Override
  public void itemOnNpc(Player player, int slot, int itemId, Npc npc) {
    openDialogue(player, slot);
  }

  public void openDialogue(Player player, int slot) {
    var item = player.getInventory().getItem(slot);
    if (item == null || item.getDef().getUnnotedId() == -1) {
      return;
    }
    if (!Prayer.isBone(item.getDef().getUnnotedId())) {
      return;
    }
    var exchangeCount = player.getInventory().getCount(item.getId());
    exchangeCount = Math.min(exchangeCount, player.getInventory().getRemainingSlots());
    exchangeCount = Math.min(exchangeCount, Item.MAX_AMOUNT / 50);
    player.openDialogue(
        new OptionsDialogue(
                DialogueOption.toOptions(
                    "Exchange '" + ItemDef.getName(item.getDef().getUnnotedId()) + "': 50 coins",
                    "Exchange 5: 250 coins",
                    "Exchange All: " + PNumber.formatNumber(exchangeCount * 50L) + " coins",
                    "Exchange X",
                    "Cancel"))
            .action(
                (c, s) -> {
                  unnote(player, s, slot);
                }));
  }

  public void unnote(Player player, int amountSlot, int itemSlot) {
    var item = player.getInventory().getItem(itemSlot);
    if (item == null || item.getDef().getUnnotedId() == -1) {
      return;
    }
    var amount = 0;
    if (amountSlot == 0) {
      amount = 1;
    } else if (amountSlot == 1) {
      amount = 5;
    } else if (amountSlot == 2) {
      amount = Item.MAX_AMOUNT;
    }
    ValueEnteredEvent.IntegerEvent valueEntered =
        value -> {
          var itemId = player.getInventory().getId(itemSlot);
          if (itemId == -1 || value == 0) {
            return;
          }
          value = Math.min(value, player.getInventory().getCount(itemId));
          value = Math.min(value, player.getInventory().getRemainingSlots());
          value = Math.min(value, Item.MAX_AMOUNT / 50);
          if (player.getInventory().getCount(ItemId.COINS) < value * 50) {
            player.getGameEncoder().sendMessage("You don't have enough coins to do this.");
            return;
          }
          player.getInventory().deleteItem(ItemId.COINS, value * 50);
          player.getInventory().deleteItem(itemId, value);
          player.getInventory().addItem(ItemDef.getUnnotedId(itemId), value);
        };
    if (amountSlot == 3) {
      player.getGameEncoder().sendEnterAmount(valueEntered);
    } else {
      valueEntered.execute(amount);
    }
  }
}
