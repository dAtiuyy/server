package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.JEWELLERY_BOX)
class JewelryBoxWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (player.isLocked()) {
      return;
    }
    switch (childId) {
      case 0:
        if (!player.getController().canTeleport(30, true)) {
          break;
        }
        String location = null;
        Tile tile = null;
        switch (slot) {
          case 0: // duel arena
            location = "Duel Arena";
            tile = new Tile(3315, 3235);
            break;
          case 1: // castle wars
            location = "Castle Wars";
            tile = new Tile(2441, 3091);
            break;
          case 2: // clan wars
            location = "Clan Wars";
            tile = new Tile(3388, 3159);
            break;
          case 3: // burthope
            location = "Burthope";
            tile = new Tile(2899, 3554);
            break;
          case 4: // barbarian outpost
            location = "Barbarian Outpost";
            tile = new Tile(2520, 3571);
            break;
          case 5: // corporeal beast
            location = "Corporeal Beast";
            tile = new Tile(2965, 4382, 2);
            break;
          case 7: // wintertodt camp
            location = "Wintertodt Camp";
            tile = new Tile(1625, 3938);
            break;
          case 8: // warriors' guild
            location = "Warriors' Guild";
            tile = new Tile(2883, 3550);
            break;
          case 9: // champions' guild
            location = "Champions' Guild";
            tile = new Tile(3190, 3368);
            break;
          case 10: // monastery
            location = "Monastery";
            tile = new Tile(3052, 3487);
            break;
          case 11: // ranging guild
            location = "Ranging Guild";
            tile = new Tile(2654, 3442);
            break;
          case 12: // fishing guild
            location = "Fishing Guild";
            tile = new Tile(2613, 3390);
            break;
          case 13: // mining guild
            location = "Mining Guild";
            tile = new Tile(3049, 9762);
            break;
          case 14: // crafting guild
            location = "Crafting Guild";
            tile = new Tile(2933, 3292);
            break;
          case 15: // cooking guild
            location = "Cooking Guild";
            tile = new Tile(3145, 3435);
            break;
          case 16: // woodcutting guild
            location = "Woodcutting Guild";
            tile = new Tile(1662, 3505);
            break;
          case 18: // miscellania
            location = "Miscellania";
            tile = new Tile(2535, 3861);
            break;
          case 19: // grand exchange
            location = "Grand Exchange";
            tile = new Tile(3163, 3481);
            break;
          case 20: // falador park
            location = "Falador Park";
            tile = new Tile(2995, 3375);
            break;
          case 22: // edgeville
            location = "Edgeville";
            tile = new Tile(3094, 3510);
            break;
          case 23: // karamja
            location = "Karamja";
            tile = new Tile(2918, 3176);
            break;
          case 24: // draynor
            location = "Draynor";
            tile = new Tile(3105, 3251);
            break;
          case 25: // al kharid
            location = "Al Kharid";
            tile = new Tile(3293, 3163);
            break;
          case 21:
          case 17:
          case 6:
            tile = new Tile(3102, 3496);
            player.getGameEncoder().sendMessage("Not added.");
            break;
        }
        player
            .getMovement()
            .animatedTeleport(
                tile,
                Magic.NORMAL_MAGIC_ANIMATION_START,
                Magic.NORMAL_MAGIC_ANIMATION_END,
                Magic.NORMAL_MAGIC_GRAPHIC,
                null,
                2);
        player.getController().stopWithTeleport();
        player.getCombat().clearHitEvents();
        player.log(PlayerLogType.TELEPORT, location + " using Jewellery Box");
        player.sendDiscordNewAccountLog("Teleport: " + location + " from Jewellry Box");
        break;
    }
  }
}
