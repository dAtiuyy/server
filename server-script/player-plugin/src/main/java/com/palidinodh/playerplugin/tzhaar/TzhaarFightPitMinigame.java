package com.palidinodh.playerplugin.tzhaar;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.VarpId;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Appearance;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.Scroll;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.clanwars.ClanWarsPlugin;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.Getter;

public class TzhaarFightPitMinigame extends PEvent {

  private static final List<Tile> TZ_KIH_TILES =
      Arrays.asList(
          new Tile(2392, 5154),
          new Tile(2403, 5153),
          new Tile(2413, 5146),
          new Tile(2404, 5137),
          new Tile(2396, 5141),
          new Tile(2405, 5161),
          new Tile(2388, 5162),
          new Tile(2385, 5138),
          new Tile(2381, 5154),
          new Tile(2401, 5145));

  @Getter private static TzhaarFightPitMinigame instance;

  @Getter private static int winnerUserId;
  @Getter private static String winnerUsername = "N/A";

  @Inject private World world;
  private List<Player> lobbyPlayers = new ArrayList<>();
  private List<Player> fightPlayers = new ArrayList<>();
  private List<Npc> npcs = new ArrayList<>();
  private int time;
  @Getter private boolean givePrizes;
  private boolean giveBonds;

  public TzhaarFightPitMinigame(boolean givePrizes, boolean giveBonds) {
    this.givePrizes = givePrizes;
    this.giveBonds = giveBonds;
  }

  public static void start(boolean givePrizes, boolean giveBonds) {
    if (isOpen() && !instance.isGivePrizes() && givePrizes) {
      instance.stop();
    }
    if (isOpen()) {
      return;
    }
    Main.getWorld().addEvent(instance = new TzhaarFightPitMinigame(givePrizes, giveBonds));
  }

  public static boolean isOpen() {
    return instance != null && instance.isRunning();
  }

  private static int getPoints(int position) {
    switch (position) {
      case 0:
        return 5;
      case 1:
        return 4;
      case 2:
      case 3:
        return 3;
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
        return 2;
      default:
        return 1;
    }
  }

  @Override
  public void execute() {
    if (getExecutions() == 0) {
      setTick((int) PTime.minToTick(5));
      if (givePrizes) {
        var message =
            "The TzHaar Fight Pit lobby is open, and it will begin in 5 minutes! Prizes include 1M"
                + (giveBonds ? " and 50 bonds" : "")
                + ".";
        world.sendBroadcast(message);
        DiscordBot.sendMessage(DiscordChannel.EVENTS, message);
        DiscordBot.sendMessage(DiscordChannel.GENERAL_CHAT, message);
      } else {
        world.sendNews("The TzHaar Fight Pit lobby is open! It will begin in 5 minutes!");
      }
      return;
    }
    if (getExecutions() == 1) {
      if (!moveLobby()) {
        stop();
      }
      return;
    }
    if (getExecutions() == 2) {
      startFight();
    }
    setTick(0);
    time++;
    checkNpcs();
    if (fightPlayers.size() <= 1) {
      stop();
    }
  }

  @Override
  public void stopHook() {
    if (fightPlayers.size() == 1) {
      var winner = fightPlayers.get(0);
      winner.getAppearance().setSkullIcon(Appearance.PK_ICON_RED_SKULL);
      if (givePrizes) {
        world.sendAchievement(
            winner.getMessaging().getIconImage()
                + winner.getUsername()
                + " won the TzHaar Fight Pit!");
        winner.getAppearance().setSkullIcon(Appearance.PK_ICON_RED_SKULL);
        var previousWinner = world.getPlayerById(winnerUserId);
        if (previousWinner != null
            && previousWinner.getAppearance().getSkullIcon() == Appearance.PK_ICON_RED_SKULL) {
          previousWinner.getAppearance().setSkullIcon(-1);
        }
        winnerUserId = winner.getId();
        winnerUsername = winner.getUsername();
      } else {
        winner
            .getController()
            .sendMessage(
                winner.getMessaging().getIconImage()
                    + winner.getUsername()
                    + " won the TzHaar Fight Pit!");
      }
      removePlayer(winner);
    }
    var players = new ArrayList<Player>();
    players.addAll(lobbyPlayers);
    players.addAll(fightPlayers);
    lobbyPlayers.clear();
    fightPlayers.clear();
    players.forEach(
        p -> {
          if (!p.getController().is(TzhaarFightPitController.class)) {
            return;
          }
          p.getController().stop();
        });
    world.removeNpcs(npcs);
    npcs.clear();
  }

  private boolean moveLobby() {
    if (lobbyPlayers.size() < 2) {
      return false;
    }
    setTick((int) PTime.secToTick(30));
    lobbyPlayers.forEach(
        p -> {
          p.getMovement().teleport(2399, 5166);
          p.getGameEncoder().sendMessage("<col=ff0000>Wait for my signal before fighting.</col>");
          p.getGameEncoder().setVarp(VarpId.FIGHT_PIT_FOES_REMAINING, lobbyPlayers.size() - 1);
        });
    fightPlayers.addAll(lobbyPlayers);
    lobbyPlayers.clear();
    return true;
  }

  private void startFight() {
    fightPlayers.forEach(
        p -> {
          if (!p.getController().is(TzhaarFightPitController.class)) {
            return;
          }
          var controller = p.getController().as(TzhaarFightPitController.class);
          controller.setState(TzhaarFightPitState.FIGHT);
          p.getGameEncoder().sendMessage("<col=ff0000>FIGHT!</col>");
        });
  }

  private void checkNpcs() {
    var seconds = PTime.tickToSec(time);
    if (seconds == 120 || seconds == 140 || seconds == 160) {
      for (var tile : TZ_KIH_TILES) {
        world.addNpc(new NpcSpawn(64, new Tile(tile).randomize(2), NpcId.TZ_KIH_22));
      }
    }
  }

  public void addPlayer(Player player) {
    lobbyPlayers.add(player);
    lobbyPlayers.forEach(
        p -> p.getGameEncoder().setVarp(VarpId.FIGHT_PIT_FOES_REMAINING, lobbyPlayers.size() - 1));
  }

  public void removePlayer(Player player) {
    var position = fightPlayers.size() - 1;
    if (fightPlayers.contains(player) && givePrizes) {
      var points = getPoints(position);
      if (points > 0) {
        var plugin = player.getPlugin(ClanWarsPlugin.class);
        plugin.setPoints(PNumber.addInt(plugin.getPoints(), points));
      }
      var items = getPrizes(position);
      if (items != null) {
        var lines = new ArrayList<String>();
        for (var item : items) {
          if (item.getId() == -1) {
            continue;
          }
          if (player.getGameMode().isIronType()
              && item.getId() != ItemId.COINS
              && item.getId() != ItemId.BOND_32318) {
            continue;
          }
          var prizeQuantity = item.getAmount();
          if (player.getGameMode().isIronType() && item.getId() != ItemId.BOND_32318) {
            prizeQuantity = Math.max(1, prizeQuantity / 4);
          }
          if (item.getId() == ItemId.BOND_32318) {
            var bondPlugin = player.getPlugin(BondPlugin.class);
            bondPlugin.setPouch(bondPlugin.getPouch() + prizeQuantity);
          } else {
            player.getBank().add(new Item(item.getId(), prizeQuantity));
          }
          lines.add(item.getName() + " x" + PNumber.formatNumber(prizeQuantity));
        }
        Scroll.open(player, "#" + (position + 1) + ": Rewards", lines);
        player
            .getGameEncoder()
            .sendMessage("Your rewards have been placed in your bank or bond pouch.");
      }
    }
    lobbyPlayers.remove(player);
    fightPlayers.remove(player);
    lobbyPlayers.forEach(
        p -> p.getGameEncoder().setVarp(VarpId.FIGHT_PIT_FOES_REMAINING, lobbyPlayers.size() - 1));
    fightPlayers.forEach(
        p -> p.getGameEncoder().setVarp(VarpId.FIGHT_PIT_FOES_REMAINING, fightPlayers.size() - 1));
  }

  private List<Item> getPrizes(int position) {
    List<Item> items = new ArrayList<>();
    switch (position) {
      case 0:
        items.add(new Item(ItemId.COINS, 1_000_000));
        if (giveBonds) {
          items.add(new Item(ItemId.BOND_32318, 50));
        }
        break;
      case 1:
        items.add(new Item(ItemId.COINS, 250_000));
        break;
      case 2:
        items.add(new Item(ItemId.COINS, 50_000));
        break;
    }
    if (Main.getHolidayToken() != -1) {
      items.add(new Item(Main.getHolidayToken(), 50));
    }
    return items;
  }
}
