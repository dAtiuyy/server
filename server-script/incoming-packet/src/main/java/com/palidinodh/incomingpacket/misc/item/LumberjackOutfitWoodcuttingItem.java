package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.LUMBERJACK_OUTFIT_WOODCUTTING_32291)
class LumberjackOutfitWoodcuttingItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getInventory().getRemainingSlots() < 4 - 1) {
      player.getInventory().notEnoughSpace();
      return;
    }
    item.remove();
    player.getInventory().addItem(ItemId.LUMBERJACK_HAT);
    player.getInventory().addItem(ItemId.LUMBERJACK_TOP);
    player.getInventory().addItem(ItemId.LUMBERJACK_LEGS);
    player.getInventory().addItem(ItemId.LUMBERJACK_BOOTS);
  }
}
