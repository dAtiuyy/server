package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class VerzikViturPillarCombat extends NpcCombat {

  private static final int MAP_OBJECT_ID_SUPPORTING_PILLAR = 32687;

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var pillar = NpcCombatDefinition.builder();
    pillar.id(NpcId.SUPPORTING_PILLAR).id(NpcId.COLLAPSING_PILLAR);
    pillar.hitpoints(NpcCombatHitpoints.builder().total(200).build());
    pillar.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    return Arrays.asList(pillar.build());
  }

  @Override
  public void spawnHook() {
    npc.getController().addMapObject(new MapObject(MAP_OBJECT_ID_SUPPORTING_PILLAR, 10, 0, npc));
  }

  @Override
  public void despawnHook() {
    removeMapObject();
  }

  @Override
  public void applyDeadStartHook(int deathDelay) {
    removeMapObject();
    npc.setId(NpcId.COLLAPSING_PILLAR);
    npc.setAnimation(8052);
  }

  @Override
  public void applyDeadEndHook() {
    var tile = new Tile(npc).setSize(3, 3);
    for (var player : npc.getController().getPlayers()) {
      if (player.isLocked()) {
        continue;
      }
      if (!player.withinDistance(tile, 1)) {
        continue;
      }
      player.getCombat().addHitEvent(new HitEvent(new Hit(PRandom.randomI(50, 70))));
    }
  }

  private void removeMapObject() {
    npc.getController().removeMapObject(new MapObject(MAP_OBJECT_ID_SUPPORTING_PILLAR, 10, 0, npc));
  }
}
