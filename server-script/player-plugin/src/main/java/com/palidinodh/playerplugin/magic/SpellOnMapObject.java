package com.palidinodh.playerplugin.magic;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.cache.widget.SpellbookChild;
import com.palidinodh.osrscore.io.cache.widget.ViewportIcon;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.util.PCollection;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SpellOnMapObject {
  CHARGE_WATER_ORB(
      SpellbookChild.CHARGE_WATER_ORB,
      726,
      new Graphic(149, 92),
      56,
      66,
      PCollection.toImmutableList(new Item(ItemId.WATER_RUNE, 30), new Item(ItemId.COSMIC_RUNE, 3)),
      PCollection.toImmutableList(ObjectId.OBELISK_OF_WATER),
      PCollection.toImmutableList(new LinkedItem(ItemId.UNPOWERED_ORB, ItemId.WATER_ORB)),
      null),
  CHARGE_EARTH_ORB(
      SpellbookChild.CHARGE_EARTH_ORB,
      726,
      new Graphic(149, 92),
      60,
      70,
      PCollection.toImmutableList(new Item(ItemId.EARTH_RUNE, 30), new Item(ItemId.COSMIC_RUNE, 3)),
      PCollection.toImmutableList(ObjectId.OBELISK_OF_EARTH),
      PCollection.toImmutableList(new LinkedItem(ItemId.UNPOWERED_ORB, ItemId.EARTH_ORB)),
      null),
  CHARGE_FIRE_ORB(
      SpellbookChild.CHARGE_FIRE_ORB,
      726,
      new Graphic(150, 92),
      63,
      73,
      PCollection.toImmutableList(new Item(ItemId.FIRE_RUNE, 30), new Item(ItemId.COSMIC_RUNE, 3)),
      PCollection.toImmutableList(ObjectId.OBELISK_OF_FIRE),
      PCollection.toImmutableList(new LinkedItem(ItemId.UNPOWERED_ORB, ItemId.FIRE_ORB)),
      null),
  CHARGE_AIR_ORB(
      SpellbookChild.CHARGE_AIR_ORB,
      726,
      new Graphic(150, 92),
      66,
      76,
      PCollection.toImmutableList(new Item(ItemId.AIR_RUNE, 30), new Item(ItemId.COSMIC_RUNE, 3)),
      PCollection.toImmutableList(ObjectId.OBELISK_OF_AIR),
      PCollection.toImmutableList(new LinkedItem(ItemId.UNPOWERED_ORB, ItemId.AIR_ORB)),
      null);

  private final SpellbookChild widgetChild;
  private final int animation;
  private final Graphic graphic;
  private final int level;
  private final int experience;
  private final List<Item> runes;
  private final List<Integer> mapObjectIds;
  private final List<LinkedItem> linkedItems;
  private final Action action;

  public static SpellOnMapObject get(SpellbookChild widgetChild) {
    for (var spell : values()) {
      if (spell.widgetChild != widgetChild) {
        continue;
      }
      return spell;
    }
    return null;
  }

  public void onMapObject(Player player, MapObject mapObject) {
    var plugin = player.getPlugin(MagicPlugin.class);
    if (plugin.getSpellDelay() > 0) {
      return;
    }
    if (player.getHeight() != player.getClientHeight()) {
      player.getGameEncoder().sendMessage("You can't do this here.");
      return;
    }
    if (player.getSkills().getLevel(Skills.MAGIC) < level) {
      player
          .getGameEncoder()
          .sendMessage("You need a Magic level of " + level + " to cast this spell.");
      return;
    }
    if (runes != null && !runes.isEmpty()) {
      if (!player.getMagic().hasRunes(widgetChild, runes)) {
        player.getGameEncoder().sendMessage("You don't have enough runes to cast this spell.");
        return;
      }
    }
    if (mapObjectIds != null && !mapObjectIds.isEmpty()) {
      var foundMapObjectMatch = false;
      for (var mapObjectId : mapObjectIds) {
        if (mapObject.getId() != mapObjectId) {
          continue;
        }
        foundMapObjectMatch = true;
        break;
      }
      if (!foundMapObjectMatch) {
        player.getGameEncoder().sendMessage("Nothing interesting happens.");
        return;
      }
    }
    LinkedItem linkedItem = null;
    if (linkedItems != null && !linkedItems.isEmpty()) {
      for (var aLinkedItem : linkedItems) {
        if (!player.getInventory().hasItem(aLinkedItem.getFromItemId())) {
          continue;
        }
        linkedItem = aLinkedItem;
        break;
      }
      if (linkedItem == null) {
        player
            .getGameEncoder()
            .sendMessage("You can't have the appropriate items to use this spell.");
        return;
      }
    }
    if (action != null && !action.execute(player, this, mapObject)) {
      return;
    }
    if (runes != null && !runes.isEmpty()) {
      player.getMagic().deleteRunes(widgetChild, runes);
    }
    if (linkedItem != null) {
      player.getInventory().deleteItem(linkedItem.getFromItemId());
      player.getInventory().addItem(linkedItem.getToItemId());
    }
    if (experience > 0) {
      player.getSkills().addXp(Skills.MAGIC, experience);
    }
    player.setAnimation(animation);
    player.setGraphic(graphic);
    player.getGameEncoder().sendViewingIcon(ViewportIcon.MAGIC);
    plugin.setSpellDelay(5);
    Diary.getDiaries(player).forEach(d -> d.castSpell(player, widgetChild, null, null, mapObject));
  }

  private interface Action {

    boolean execute(Player player, SpellOnMapObject spell, MapObject mapObject);
  }

  @AllArgsConstructor
  @Getter
  private static class LinkedItem {

    private int fromItemId;
    private int toItemId;
  }
}
