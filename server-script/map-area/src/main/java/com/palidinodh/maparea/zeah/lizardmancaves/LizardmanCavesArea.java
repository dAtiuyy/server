package com.palidinodh.maparea.zeah.lizardmancaves;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(5275)
public class LizardmanCavesArea extends Area {}
