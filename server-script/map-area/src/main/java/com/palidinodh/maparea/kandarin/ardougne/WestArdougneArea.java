package com.palidinodh.maparea.kandarin.ardougne;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId({9779, 10035})
@ReferenceIdSet(
    primary = 9780,
    secondary = {64, 80, 96, 112})
@ReferenceIdSet(
    primary = 10036,
    secondary = {0, 16, 32, 48, 64, 80, 96, 112})
public class WestArdougneArea extends Area {}
