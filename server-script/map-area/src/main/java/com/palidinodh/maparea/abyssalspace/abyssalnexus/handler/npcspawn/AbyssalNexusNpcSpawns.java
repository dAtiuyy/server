package com.palidinodh.maparea.abyssalspace.abyssalnexus.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class AbyssalNexusNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2977, 4855), NpcId.ABYSSAL_SIRE_350));
    spawns.add(new NpcSpawn(new Tile(3102, 4855), NpcId.ABYSSAL_SIRE_350));
    spawns.add(new NpcSpawn(new Tile(2967, 4791), NpcId.ABYSSAL_SIRE_350));
    spawns.add(new NpcSpawn(new Tile(3107, 4791), NpcId.ABYSSAL_SIRE_350));

    return spawns;
  }
}
