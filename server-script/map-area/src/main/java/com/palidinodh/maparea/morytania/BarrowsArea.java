package com.palidinodh.maparea.morytania;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.playerplugin.barrows.BarrowsPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({14131, 14231})
public class BarrowsArea extends Area {

  @Override
  public void unloadPlayer() {
    getPlayer().getWidgetManager().removeFullOverlay();
  }

  @Override
  public void tickPlayer() {
    var player = getPlayer();
    if (player.getWidgetManager().getFullOverlay() != WidgetId.BARROWS_OVERLAY) {
      player.getWidgetManager().sendFullOverlay(WidgetId.BARROWS_OVERLAY);
      player.getPlugin(BarrowsPlugin.class).setVarbits();
    }
  }
}
