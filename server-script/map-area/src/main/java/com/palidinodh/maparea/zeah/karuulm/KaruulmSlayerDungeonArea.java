package com.palidinodh.maparea.zeah.karuulm;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({5022, 5023, 5279, 5280, 5535, 5536})
public class KaruulmSlayerDungeonArea extends Area {

  @Override
  public void tickPlayer() {
    var player = getPlayer();
    if (player.getEquipment().getFootId() != ItemId.BOOTS_OF_STONE
        && player.getEquipment().getFootId() != ItemId.BOOTS_OF_BRIMSTONE
        && player.getEquipment().getFootId() != ItemId.GRANITE_BOOTS) {
      player.getCombat().addHit(new Hit(4));
    }
  }
}
