package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.TOME_OF_EXPERIENCE_3,
  ItemId.TOME_OF_EXPERIENCE_2,
  ItemId.TOME_OF_EXPERIENCE_1
})
class TomeOfExperience3Item implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getGameMode().isRegular()) {
      player.getGameEncoder().sendMessage("You can't use this.");
      return;
    }
    player.openOldDialogue("combatlamp", 1);
  }
}
