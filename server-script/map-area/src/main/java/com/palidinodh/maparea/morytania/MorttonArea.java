package com.palidinodh.maparea.morytania;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({13619, 13875})
public class MorttonArea extends Area {}
