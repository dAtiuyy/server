package com.palidinodh.maparea.misthalin.edgeville.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.LEVER_26761)
class WildernessLeverMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.getMovement().getTeleportBlock() > 0) {
      player
          .getGameEncoder()
          .sendMessage(
              "A teleport block has been cast on you. It should wear off in "
                  + player.getMovement().getTeleportBlockRemaining()
                  + ".");
      return;
    }
    player.openDialogue(
        new OptionsDialogue(
            "Are you sure you want to teleport to the wilderness?",
            new DialogueOption(
                "Yes, teleport me to the wilderness!",
                (c, s) -> {
                  var tile = new Tile(3153, 3923);
                  if (player.getClientHeight() == tile.getHeight()) {
                    tile.setHeight(player.getHeight());
                  }
                  if (!player.getController().canTeleport(tile, true)) {
                    return;
                  }
                  player.getMagic().standardTeleport(tile);
                  player.getCombat().clearHitEvents();
                }),
            new DialogueOption("No!")));
  }
}
