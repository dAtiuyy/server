package com.palidinodh.maparea.kharidiandesert.clanwars.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.clanwars.ClanWarsPlugin;
import com.palidinodh.playerplugin.clanwars.ClanWarsStages;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ObjectId.CHALLENGE_PORTAL,
  ObjectId.CHALLENGE_PORTAL_26643,
  ObjectId.CHALLENGE_PORTAL_26644
})
class ChallengePortalMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    player.openDialogue(
        new OptionsDialogue(
            new DialogueOption(
                "Join your clan's battle.",
                (c, s) -> {
                  ClanWarsStages.openJoin(player, plugin);
                }),
            new DialogueOption(
                "Observe your clan's battle.",
                (c, s) -> {
                  ClanWarsStages.openView(player, plugin);
                }),
            new DialogueOption("Cancel.")));
  }
}
