package com.palidinodh.nio;

import lombok.Getter;
import lombok.Setter;

public abstract class WriteEventHandler {

  @Getter @Setter private Object attachment;

  protected WriteEventHandler() {}

  protected WriteEventHandler(Object attachment) {
    this.attachment = attachment;
  }

  public abstract void complete(Session session, boolean success);
}
