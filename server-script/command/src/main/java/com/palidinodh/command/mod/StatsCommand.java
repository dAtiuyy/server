package com.palidinodh.command.mod;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.event.stats.StatsEvent;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("stats")
class StatsCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Override
  public void execute(Player player, String name, String message) {
    player.getWorld().getWorldEvent(StatsEvent.class).open(player);
    DiscordBot.sendMessage(DiscordChannel.MODERATION_LOG, player.getUsername() + " loaded stats.");
  }
}
