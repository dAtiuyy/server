package com.palidinodh.playerplugin.tirannwn.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.BOW_OF_FAERDHINEN_C,
  ItemId.BOW_OF_FAERDHINEN_C_25884,
  ItemId.BOW_OF_FAERDHINEN_C_25886,
  ItemId.BOW_OF_FAERDHINEN_C_25888,
  ItemId.BOW_OF_FAERDHINEN_C_25890,
  ItemId.BOW_OF_FAERDHINEN_C_25892,
  ItemId.BOW_OF_FAERDHINEN_C_25894,
  ItemId.BOW_OF_FAERDHINEN_C_25896
})
class BowOfFaerdhinenCorruptedItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "uncharge":
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you want to uncharge the bow? <br> You will not get back any of the shards.",
                new DialogueOption(
                    "Yes, turn it back into a normal Bow of Faerdhinen!",
                    (c, s) -> item.replace(new Item(ItemId.BOW_OF_FAERDHINEN_INACTIVE))),
                new DialogueOption("No!")));
        break;
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var bows =
        ItemHandler.matchId(
            useItem,
            onItem,
            ItemId.BOW_OF_FAERDHINEN_C,
            ItemId.BOW_OF_FAERDHINEN_C_25884,
            ItemId.BOW_OF_FAERDHINEN_C_25886,
            ItemId.BOW_OF_FAERDHINEN_C_25888,
            ItemId.BOW_OF_FAERDHINEN_C_25890,
            ItemId.BOW_OF_FAERDHINEN_C_25892,
            ItemId.BOW_OF_FAERDHINEN_C_25894,
            ItemId.BOW_OF_FAERDHINEN_C_25896);
    if (ItemHandler.used(useItem, onItem, bows, ItemId.CRYSTAL_OF_HEFIN)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to remove the recolour of <br> your Bow of faerdhinen (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BOW_OF_FAERDHINEN_C));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    } else if (ItemHandler.used(useItem, onItem, bows, ItemId.CRYSTAL_OF_ITHELL)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Bow of faerdhinen (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BOW_OF_FAERDHINEN_C_25884));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    } else if (ItemHandler.used(useItem, onItem, bows, ItemId.CRYSTAL_OF_IORWERTH)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Bow of faerdhinen (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BOW_OF_FAERDHINEN_C_25886));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    } else if (ItemHandler.used(useItem, onItem, bows, ItemId.CRYSTAL_OF_TRAHAEARN)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Bow of faerdhinen (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BOW_OF_FAERDHINEN_C_25888));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    } else if (ItemHandler.used(useItem, onItem, bows, ItemId.CRYSTAL_OF_CADARN)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Bow of faerdhinen (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BOW_OF_FAERDHINEN_C_25890));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    } else if (ItemHandler.used(useItem, onItem, bows, ItemId.CRYSTAL_OF_CRWYS)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Bow of faerdhinen (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BOW_OF_FAERDHINEN_C_25892));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    } else if (ItemHandler.used(useItem, onItem, bows, ItemId.CRYSTAL_OF_MEILYR)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Bow of faerdhinen (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BOW_OF_FAERDHINEN_C_25894));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    } else if (ItemHandler.used(useItem, onItem, bows, ItemId.CRYSTAL_OF_AMLODD)) {
      player.openDialogue(
          new OptionsDialogue(
              "Are you sure you wish to recolour your Bow of faerdhinen (c)?",
              new DialogueOption(
                  "Yes",
                  (c, s) -> {
                    onItem.replace(new Item(ItemId.BOW_OF_FAERDHINEN_C_25896));
                    useItem.remove();
                  }),
              new DialogueOption("No")));
      return true;
    }
    return false;
  }
}
