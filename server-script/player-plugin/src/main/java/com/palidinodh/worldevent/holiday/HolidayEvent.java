package com.palidinodh.worldevent.holiday;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.osrs.NpcDefinition;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.osrscore.world.WorldEvent;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

public class HolidayEvent extends WorldEvent {

  private static final boolean ENABLED = true;
  private static final int SOON_MINUTES = 15;
  private static final String[] TIME = {"1:40", "4:40", "13:40", "16:40", "19:40", "22:40"};
  private static final int[] HOURS;
  private static final int[] MINUTES;

  static {
    if (TIME == null || TIME.length == 0) {
      HOURS = null;
      MINUTES = null;
    } else {
      HOURS = new int[TIME.length];
      MINUTES = new int[TIME.length];
      for (int i = 0; i < TIME.length; i++) {
        String[] data = TIME[i].split(":");
        HOURS[i] = Integer.parseInt(data[0]);
        MINUTES[i] = Integer.parseInt(data[1]);
      }
    }
  }

  @Inject private transient World world;
  private transient Npc shopNpc;
  private transient Npc boss;
  private transient List<Npc> bossSupports;

  @Getter private HolidayType type;
  private NpcSpawn shopNpcSpawn;
  private int uniqueDrops;

  public HolidayEvent() {
    super(4);
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("holiday_name")) {
      return type != null ? type.getFormattedName() + " Boss" : "Holiday";
    }
    if (name.equals("holiday_message")) {
      return getNextTimeText();
    }
    if (name.equals("holiday_unique_drop")) {
      if (++uniqueDrops >= 10) {
        setType(null);
      }
    }
    return null;
  }

  @Override
  public void execute() {
    setTick(4);
    if (getExecutions() == 0) {
      setType(type);
    }
    if (boss != null && !boss.isVisible()) {
      boss = null;
      world.removeNpcs(bossSupports);
      bossSupports = null;
    }
    if (!canRun()) {
      return;
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    int dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    int[] nextTime = getNextTime();
    if (nextTime == null) {
      return;
    }
    int remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    if (remainingMinutes == SOON_MINUTES) {
      var message =
          NpcDefinition.getName(type.getBossNpcSpawn().getId())
              + " will spawn in "
              + SOON_MINUTES
              + " minutes!";
      world.sendNews(message + " Use ::holidayboss to teleport there!");
      DiscordBot.sendMessage(DiscordChannel.EVENTS, message);
      setTick(105);
    } else if (remainingMinutes == 0) {
      spawnBoss();
      setTick(105);
    }
  }

  @Override
  public boolean useHandlers(Player player) {
    return canRun() || shopNpcSpawn != null;
  }

  public void setType(HolidayType type) {
    if (this.type != type && type != null) {
      uniqueDrops = 0;
    }
    this.type = type;
    if (type != null) {
      shopNpcSpawn = type.getShopNpcSpawn();
    }
    if (shopNpcSpawn != null) {
      world.removeNpc(shopNpc);
      shopNpc = world.addNpc(shopNpcSpawn);
    }
    Main.setHolidayToken(type != null ? type.getItemId() : -1);
  }

  public void endShop() {
    world.removeNpc(shopNpc);
    shopNpc = null;
    shopNpcSpawn = null;
  }

  public void teleport(Player player) {
    if (!canRun()) {
      player.getGameEncoder().sendMessage("There are no active holiday events at this time.");
      return;
    }
    player.getMovement().teleport(type.getBossTeleportTile());
  }

  public void spawnBoss() {
    if (!canRun()) {
      return;
    }
    world.removeNpc(boss);
    world.removeNpcs(bossSupports);
    boss = world.addNpc(type.getBossNpcSpawn());
    if (type.getBossSupportNpcSpawns() != null) {
      bossSupports = new ArrayList<>();
      for (var supportSpawn : type.getBossSupportNpcSpawns()) {
        bossSupports.add(world.addNpc(supportSpawn));
      }
    }
    var message = NpcDefinition.getName(boss.getId()) + " has spawned!";
    world.sendBroadcast(message + " Use ::holidayboss to teleport there!");
    DiscordBot.sendMessage(DiscordChannel.EVENTS, message);
    DiscordBot.sendMessage(DiscordChannel.GENERAL_CHAT, message);
    setType(type);
  }

  private String getNextTimeText() {
    if (boss != null) {
      return "Spawned";
    }
    if (!canRun()) {
      return "N/A";
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    int dayMinute = (int) PTime.hourToMin(currentHour) + currentMinute;
    int[] nextTime = getNextTime();
    int remainingMinutes = PTime.getRemainingMinutes(dayMinute, nextTime[0] * 60 + nextTime[1]);
    return PTime.ticksToLongDuration(PTime.minToTick(remainingMinutes));
  }

  private int[] getNextTime() {
    if (!canRun()) {
      return null;
    }
    int currentHour = PTime.getHour24();
    int currentMinute = PTime.getMinute();
    for (int i = 0; i < HOURS.length; i++) {
      int hour = HOURS[i];
      int minute = MINUTES[i];
      if (currentHour > hour || currentHour == hour && currentMinute > minute) {
        continue;
      }
      return new int[] {hour, minute};
    }
    return new int[] {HOURS[0], MINUTES[0]};
  }

  private boolean canRun() {
    if (!ENABLED) {
      return false;
    }
    if (type == null) {
      return false;
    }
    if (TIME == null) {
      return false;
    }
    if (TIME.length == 0) {
      return false;
    }
    return world.isPrimary();
  }
}
