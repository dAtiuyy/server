package com.palidinodh.playerplugin.tirannwn;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.VarpId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerSubPlugin;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import com.palidinodh.playerplugin.boss.BossInstanceController;
import com.palidinodh.playerplugin.boss.BossPlugin;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.random.PRandom;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class TheGauntletSubPlugin implements PlayerSubPlugin {

  private static final List<RandomItem> CRYSTALLINE_ITEMS =
      RandomItem.buildList(
          new RandomItem(ItemId.RUNE_FULL_HELM_NOTED, 2, 4),
          new RandomItem(ItemId.RUNE_CHAINBODY_NOTED, 1, 2),
          new RandomItem(ItemId.RUNE_PLATEBODY_NOTED, 1, 2),
          new RandomItem(ItemId.RUNE_PLATELEGS_NOTED, 1, 2),
          new RandomItem(ItemId.RUNE_PLATESKIRT_NOTED, 1, 2),
          new RandomItem(ItemId.RUNE_HALBERD_NOTED, 1, 2),
          new RandomItem(ItemId.RUNE_PICKAXE_NOTED, 1, 2),
          new RandomItem(ItemId.DRAGON_HALBERD_NOTED),
          new RandomItem(ItemId.COSMIC_RUNE, 160, 240),
          new RandomItem(ItemId.NATURE_RUNE, 100, 140),
          new RandomItem(ItemId.LAW_RUNE, 80, 140),
          new RandomItem(ItemId.CHAOS_RUNE, 180, 300),
          new RandomItem(ItemId.DEATH_RUNE, 100, 160),
          new RandomItem(ItemId.BLOOD_RUNE, 80, 140),
          new RandomItem(ItemId.MITHRIL_ARROW, 800, 1_200),
          new RandomItem(ItemId.ADAMANT_ARROW, 400, 600),
          new RandomItem(ItemId.RUNE_ARROW, 200, 300),
          new RandomItem(ItemId.DRAGON_ARROW, 30, 80),
          new RandomItem(ItemId.UNCUT_SAPPHIRE_NOTED, 20, 60),
          new RandomItem(ItemId.UNCUT_EMERALD_NOTED, 10, 50),
          new RandomItem(ItemId.UNCUT_RUBY_NOTED, 5, 30),
          new RandomItem(ItemId.UNCUT_DIAMOND_NOTED, 3, 6),
          new RandomItem(ItemId.BATTLESTAFF_NOTED, 4, 8),
          new RandomItem(ItemId.COINS, 20_000, 80_000));
  private static final List<RandomItem> CORRUPTED_ITEMS =
      RandomItem.buildList(
          new RandomItem(ItemId.RUNE_FULL_HELM_NOTED, 3, 5),
          new RandomItem(ItemId.RUNE_CHAINBODY_NOTED, 2, 3),
          new RandomItem(ItemId.RUNE_PLATEBODY_NOTED, 2),
          new RandomItem(ItemId.RUNE_PLATELEGS_NOTED, 2, 3),
          new RandomItem(ItemId.RUNE_PLATESKIRT_NOTED, 2, 3),
          new RandomItem(ItemId.RUNE_HALBERD_NOTED, 2, 3),
          new RandomItem(ItemId.RUNE_PICKAXE_NOTED, 2, 3),
          new RandomItem(ItemId.DRAGON_HALBERD_NOTED, 1, 2),
          new RandomItem(ItemId.COSMIC_RUNE, 175, 250),
          new RandomItem(ItemId.NATURE_RUNE, 125, 150),
          new RandomItem(ItemId.LAW_RUNE, 100, 150),
          new RandomItem(ItemId.CHAOS_RUNE, 200, 350),
          new RandomItem(ItemId.DEATH_RUNE, 125, 175),
          new RandomItem(ItemId.BLOOD_RUNE, 100, 150),
          new RandomItem(ItemId.MITHRIL_ARROW, 1_000, 1_500),
          new RandomItem(ItemId.ADAMANT_ARROW, 500, 725),
          new RandomItem(ItemId.RUNE_ARROW, 250, 450),
          new RandomItem(ItemId.DRAGON_ARROW, 50, 100),
          new RandomItem(ItemId.UNCUT_SAPPHIRE_NOTED, 25, 65),
          new RandomItem(ItemId.UNCUT_EMERALD_NOTED, 15, 60),
          new RandomItem(ItemId.UNCUT_RUBY_NOTED, 10, 40),
          new RandomItem(ItemId.UNCUT_DIAMOND_NOTED, 5, 15),
          new RandomItem(ItemId.BATTLESTAFF_NOTED, 8, 12),
          new RandomItem(ItemId.COINS, 75_000, 150_000));

  @Inject private transient Player player;
  @Inject private transient TirannwnPlugin plugin;

  @Getter @Setter private Reward reward = Reward.NONE;

  @Override
  public void login() {
    setVarps();
  }

  public void enter(int npcId) {
    if (reward != Reward.NONE) {
      player.getGameEncoder().sendMessage("Please claim your reward before entering.");
      return;
    }
    if (!player.getInventory().isEmpty() || !player.getEquipment().isEmpty()) {
      player.getGameEncoder().sendMessage("You can't take items into the Gauntlet.");
      return;
    }
    player.restore();
    player.getPlugin(BossPlugin.class).start(npcId, false);
    if (!player.getController().is(BossInstanceController.class)) {
      return;
    }
    player.getController().as(BossInstanceController.class).setRemoveItemsOnStop(true);
  }

  @SuppressWarnings("fallthrough")
  public int getArmourTier() {
    int[] tierCounts = new int[3];
    switch (player.getEquipment().getHeadId()) {
      case ItemId.CRYSTAL_HELM_PERFECTED:
      case ItemId.CORRUPTED_HELM_PERFECTED:
        tierCounts[2]++;
      case ItemId.CRYSTAL_HELM_ATTUNED:
      case ItemId.CORRUPTED_HELM_ATTUNED:
        tierCounts[1]++;
      case ItemId.CRYSTAL_HELM_BASIC:
      case ItemId.CORRUPTED_HELM_BASIC:
        tierCounts[0]++;
        break;
    }
    switch (player.getEquipment().getChestId()) {
      case ItemId.CRYSTAL_BODY_PERFECTED:
      case ItemId.CORRUPTED_BODY_PERFECTED:
        tierCounts[2]++;
      case ItemId.CRYSTAL_BODY_ATTUNED:
      case ItemId.CORRUPTED_BODY_ATTUNED:
        tierCounts[1]++;
      case ItemId.CRYSTAL_BODY_BASIC:
      case ItemId.CORRUPTED_BODY_BASIC:
        tierCounts[0]++;
        break;
    }
    switch (player.getEquipment().getLegId()) {
      case ItemId.CRYSTAL_LEGS_PERFECTED:
      case ItemId.CORRUPTED_LEGS_PERFECTED:
        tierCounts[2]++;
      case ItemId.CRYSTAL_LEGS_ATTUNED:
      case ItemId.CORRUPTED_LEGS_ATTUNED:
        tierCounts[1]++;
      case ItemId.CRYSTAL_LEGS_BASIC:
      case ItemId.CORRUPTED_LEGS_BASIC:
        tierCounts[0]++;
        break;
    }
    for (var i = tierCounts.length - 1; i >= 0; i--) {
      if (tierCounts[i] < 3) {
        continue;
      }
      return i + 1;
    }
    return 0;
  }

  public void openCrystalline() {
    player.setAnimation(7305);
    var logName = "Gauntlet";
    var count = player.getCombat().getNPCKillCount(logName);
    for (var i = 0; i < 2; i++) {
      var item = RandomItem.getItem(CRYSTALLINE_ITEMS);
      player.getInventory().addOrDropItem(item);
      player
          .getGameEncoder()
          .sendMessage(
              "<col=ff0000>Valuable drop: "
                  + item.getAmount()
                  + " x "
                  + item.getName()
                  + " ("
                  + (item.getInfoDef().getConfiguredExchangePrice() * item.getAmount())
                  + ")</col>");
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(
                120, ItemId.CRYSTAL_WEAPON_SEED, NpcId.CRYSTALLINE_HUNLLEF_674))) {
      var item = new Item(ItemId.CRYSTAL_WEAPON_SEED);
      player.getCombat().logNPCItem(logName, item.getId(), 1, count);
      player.getInventory().addOrDropItem(item);
      player
          .getGameEncoder()
          .sendMessage(
              "<col=ff0000>Valuable drop: 1 x "
                  + item.getName()
                  + " ("
                  + (item.getInfoDef().getConfiguredExchangePrice() * item.getAmount())
                  + ")</col>");
      player.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.LOGGED_DROPS, 1);
      player
          .getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.THE_GAUNTLET_DROPS, 1);
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(
                120, ItemId.CRYSTAL_ARMOUR_SEED, NpcId.CRYSTALLINE_HUNLLEF_674))) {
      var item = new Item(ItemId.CRYSTAL_ARMOUR_SEED);
      player.getCombat().logNPCItem(logName, item.getId(), 1, count);
      player.getInventory().addOrDropItem(item);
      player
          .getGameEncoder()
          .sendMessage(
              "<col=ff0000>Valuable drop: 1 x "
                  + item.getName()
                  + " ("
                  + (item.getInfoDef().getConfiguredExchangePrice() * item.getAmount())
                  + ")</col>");
      player.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.LOGGED_DROPS, 1);
      player
          .getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.THE_GAUNTLET_DROPS, 1);
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(
                2_000, ItemId.ENHANCED_CRYSTAL_WEAPON_SEED, NpcId.CRYSTALLINE_HUNLLEF_674))) {
      var item = new Item(ItemId.ENHANCED_CRYSTAL_WEAPON_SEED);
      player.getCombat().logNPCItem(logName, item.getId(), 1, count);
      player.getInventory().addOrDropItem(item);
      player
          .getGameEncoder()
          .sendMessage(
              "<col=ff0000>Valuable drop: 1 x "
                  + item.getName()
                  + " ("
                  + (item.getInfoDef().getConfiguredExchangePrice() * item.getAmount())
                  + ")</col>");
      player.getWorld().sendItemDropNews(player, item, "from the " + logName);
      player.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.LOGGED_DROPS, 1);
      player
          .getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.THE_GAUNTLET_DROPS, 1);
    }
    if (PRandom.randomE(25) == 0) {
      player.getInventory().addOrDropItem(ClueScrollType.ELITE.getBoxId());
      player
          .getGameEncoder()
          .sendMessage(
              "<col=ff0000>Untradeable drop: 1 x "
                  + ItemDefinition.getName(ClueScrollType.ELITE.getBoxId())
                  + "</col>");
    }
    var shardQuantity = PRandom.randomI(3, 7);
    player.getInventory().addOrDropItem(ItemId.CRYSTAL_SHARD, shardQuantity);
    player
        .getGameEncoder()
        .sendMessage(
            "<col=ff0000>Untradeable drop: "
                + shardQuantity
                + " x "
                + ItemDefinition.getName(ItemId.CRYSTAL_SHARD)
                + "</col>");
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(2_000, ItemId.YOUNGLLEF, NpcId.CRYSTALLINE_HUNLLEF_674))) {
      if (player.getPlugin(FamiliarPlugin.class).unlockPet(ItemId.YOUNGLLEF)) {
        player.getCombat().logNPCItem(logName, ItemId.YOUNGLLEF, 1, count);
        player
            .getSkills()
            .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.LOGGED_DROPS, 1);
        player
            .getSkills()
            .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.THE_GAUNTLET_DROPS, 1);
      }
    }
    setVarps();
  }

  public void openCorrupted() {
    player.setAnimation(7305);
    var logName = "Corrupted Gauntlet";
    var count = player.getCombat().getNPCKillCount(logName);
    for (var i = 0; i < 3; i++) {
      var item = RandomItem.getItem(CORRUPTED_ITEMS);
      player.getInventory().addOrDropItem(item);
      player
          .getGameEncoder()
          .sendMessage(
              "<col=ff0000>Valuable drop: "
                  + item.getAmount()
                  + " x "
                  + item.getName()
                  + " ("
                  + (item.getInfoDef().getConfiguredExchangePrice() * item.getAmount())
                  + ")</col>");
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(50, ItemId.CRYSTAL_WEAPON_SEED, NpcId.CORRUPTED_HUNLLEF_894))) {
      var item = new Item(ItemId.CRYSTAL_WEAPON_SEED);
      player.getCombat().logNPCItem(logName, item.getId(), 1, count);
      player.getInventory().addOrDropItem(item);
      player
          .getGameEncoder()
          .sendMessage(
              "<col=ff0000>Valuable drop: 1 x "
                  + item.getName()
                  + " ("
                  + (item.getInfoDef().getConfiguredExchangePrice() * item.getAmount())
                  + ")</col>");
      player.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.LOGGED_DROPS, 1);
      player
          .getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.THE_GAUNTLET_DROPS, 1);
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(50, ItemId.CRYSTAL_ARMOUR_SEED, NpcId.CORRUPTED_HUNLLEF_894))) {
      var item = new Item(ItemId.CRYSTAL_ARMOUR_SEED);
      player.getCombat().logNPCItem(logName, item.getId(), 1, count);
      player.getInventory().addOrDropItem(item);
      player
          .getGameEncoder()
          .sendMessage(
              "<col=ff0000>Valuable drop: 1 x "
                  + item.getName()
                  + " ("
                  + (item.getInfoDef().getConfiguredExchangePrice() * item.getAmount())
                  + ")</col>");
      player.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.LOGGED_DROPS, 1);
      player
          .getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.THE_GAUNTLET_DROPS, 1);
    }
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(
                400, ItemId.ENHANCED_CRYSTAL_WEAPON_SEED, NpcId.CORRUPTED_HUNLLEF_894))) {
      var item = new Item(ItemId.ENHANCED_CRYSTAL_WEAPON_SEED);
      player.getCombat().logNPCItem(logName, item.getId(), 1, count);
      player.getInventory().addOrDropItem(item);
      player
          .getGameEncoder()
          .sendMessage(
              "<col=ff0000>Valuable drop: 1 x "
                  + item.getName()
                  + " ("
                  + (item.getInfoDef().getConfiguredExchangePrice() * item.getAmount())
                  + ")</col>");
      player.getWorld().sendItemDropNews(player, item, "from the " + logName);
      player.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.LOGGED_DROPS, 1);
      player
          .getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.THE_GAUNTLET_DROPS, 1);
    }
    if (PRandom.randomE(20) == 0) {
      player.getInventory().addOrDropItem(ClueScrollType.ELITE.getBoxId());
      player
          .getGameEncoder()
          .sendMessage(
              "<col=ff0000>Untradeable drop: 1 x "
                  + ItemDefinition.getName(ClueScrollType.ELITE.getBoxId())
                  + "</col>");
    }
    var shardQuantity = PRandom.randomI(5, 9);
    player.getInventory().addOrDropItem(ItemId.CRYSTAL_SHARD, shardQuantity);
    player
        .getGameEncoder()
        .sendMessage(
            "<col=ff0000>Untradeable drop: "
                + shardQuantity
                + " x "
                + ItemDefinition.getName(ItemId.CRYSTAL_SHARD)
                + "</col>");
    if (PRandom.inRange(
        1,
        player
            .getCombat()
            .getDropRateDenominator(800, ItemId.YOUNGLLEF, NpcId.CORRUPTED_HUNLLEF_894))) {
      if (player.getPlugin(FamiliarPlugin.class).unlockPet(ItemId.YOUNGLLEF)) {
        player.getCombat().logNPCItem(logName, ItemId.YOUNGLLEF, 1, count);
        player
            .getSkills()
            .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.LOGGED_DROPS, 1);
        player
            .getSkills()
            .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.THE_GAUNTLET_DROPS, 1);
      }
    }
    setVarps();
  }

  public void setVarps() {
    if (player.getCombat().getNPCKillCount("Gauntlet") > 0) {
      player.getGameEncoder().setVarp(VarpId.THE_GAUNTLET_COMPLETED, 1);
    }
    player.getGameEncoder().setVarbit(VarbitId.THE_GAUNTLET_CHEST, reward == Reward.NONE ? 0 : 1);
  }

  public enum Reward {
    NONE,
    CRYSTALLINE,
    CORRUPTED
  }
}
