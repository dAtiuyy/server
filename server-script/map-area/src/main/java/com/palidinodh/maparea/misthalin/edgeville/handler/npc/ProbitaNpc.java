package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.PROBITA)
class ProbitaNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    player.openOptionsDialogue(
        new DialogueOption(
            "Open shop.",
            (c, s) -> {
              player.openShop("pets");
            }),
        new DialogueOption(
            "View pet insurance.",
            (c, s) -> {
              player.getPlugin(FamiliarPlugin.class).openPetInsurance();
            }));
  }

  @Override
  public void itemOnNpc(Player player, int slot, int itemId, Npc npc) {
    player.getPlugin(FamiliarPlugin.class).insurePet(itemId);
  }
}
