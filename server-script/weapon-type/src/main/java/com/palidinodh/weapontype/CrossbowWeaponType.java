package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.PHOENIX_CROSSBOW,
  ItemId.CROSSBOW,
  ItemId.BRONZE_CROSSBOW,
  ItemId.BLURITE_CROSSBOW,
  ItemId.IRON_CROSSBOW,
  ItemId.STEEL_CROSSBOW,
  ItemId.MITH_CROSSBOW,
  ItemId.ADAMANT_CROSSBOW,
  ItemId.RUNE_CROSSBOW,
  ItemId.PHOENIX_CROSSBOW_11165,
  ItemId.PHOENIX_CROSSBOW_11167,
  ItemId.DRAGON_HUNTER_CROSSBOW,
  ItemId.DRAGON_HUNTER_CROSSBOW_T,
  ItemId.DRAGON_HUNTER_CROSSBOW_B,
  ItemId.DRAGON_CROSSBOW,
  ItemId.RUNE_CROSSBOW_23601,
  ItemId.BLIGHTED_RUNE_CROSSBOW_32374
})
class CrossbowWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_5);
    type.renderAnimations(new int[] {4591, 4226, 4226, 4227, 4227, 4227, 4228});
    type.equipSound(new Sound(2244));
    type.attackSpeed(6);
    type.attackDistance(7);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(4230).attackSound(new Sound(2695)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.KARILS_CROSSBOW,
  ItemId.KARILS_CROSSBOW_100,
  ItemId.KARILS_CROSSBOW_75,
  ItemId.KARILS_CROSSBOW_50,
  ItemId.KARILS_CROSSBOW_25
})
class KarilsCrossbowWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_5);
    type.renderAnimations(new int[] {2074, 2076, 2076, 2076, 2076, 2076, 2077});
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(4);
    type.attackDistance(7);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(2075).attackSound(new Sound(2695)).build());
    return type;
  }
}

@ReferenceId(ItemId.HUNTERS_CROSSBOW)
class HuntersCrossbowWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_5);
    type.renderAnimations(new int[] {4591, 4226, 4226, 4227, 4227, 4227, 4228});
    type.equipSound(new Sound(2244));
    type.attackSpeed(4);
    type.attackDistance(7);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(4230).attackSound(new Sound(2695)).build());
    return type;
  }
}

@ReferenceId(ItemId.DORGESHUUN_CROSSBOW)
class DorgeshuunCrossbowWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_5);
    type.renderAnimations(new int[] {4591, 4226, 4226, 4227, 4227, 4227, 4228});
    type.equipSound(new Sound(2244));
    type.attackSpeed(5);
    type.attackDistance(6);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(4230).attackSound(new Sound(2695)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.ARMADYL_CROSSBOW,
  ItemId.ARMADYL_CROSSBOW_23611,
  ItemId.ARMADYL_CROSSBOW_OR_60049
})
class ArmadylCrossbowWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_5);
    type.renderAnimations(new int[] {4591, 4226, 4226, 4227, 4227, 4227, 4228});
    type.equipSound(new Sound(2244));
    type.attackSpeed(6);
    type.attackDistance(8);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(4230).attackSound(new Sound(2695)).build());
    return type;
  }
}

@ReferenceId(ItemId.ZARYTE_CROSSBOW)
class ZaryteCrossbowWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_5);
    type.renderAnimations(new int[] {4591, 4226, 4226, 4227, 4227, 4227, 4228});
    type.equipSound(new Sound(2244));
    type.attackSpeed(6);
    type.attackDistance(8);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(9166).attackSound(new Sound(2695)).build());
    return type;
  }
}

@ReferenceId({ItemId.LIGHT_BALLISTA, ItemId.HEAVY_BALLISTA, ItemId.HEAVY_BALLISTA_BEGINNER_32328})
class BallistaWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_5);
    type.renderAnimations(new int[] {7220, 7223, 7223, 7223, 7223, 7223, 7221});
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(7);
    type.attackDistance(9);
    type.defendAnimation(7219);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(7218).attackSound(new Sound(2699)).build());
    return type;
  }
}
