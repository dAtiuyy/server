package com.palidinodh.command.beta;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler.Beta;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName({"varbit", "varbitloop"})
class VarbitCommand implements CommandHandler, Beta {

  @Override
  public String getExample(String name) {
    switch (name) {
      case "varbit":
        return "id value";
      case "varbitloop":
        return "start_id end_id value";
    }
    return "";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.splitInt(message);
    switch (name) {
      case "varbit":
        player.getGameEncoder().setVarbit(messages[0], messages[1]);
        break;
      case "varbitloop":
        for (var i = messages[0]; i < messages[1]; i++) {
          player.getGameEncoder().setVarbit(i, messages[2]);
        }
        break;
    }
  }
}
