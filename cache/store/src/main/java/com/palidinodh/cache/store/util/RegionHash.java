package com.palidinodh.cache.store.util;

public final class RegionHash {

  private RegionHash() {}

  public static String regionToNameM(int regionId) {
    return "m" + (regionId >> 8) + "_" + (regionId & 255);
  }

  public static String regionToNameL(int regionId) {
    return "l" + (regionId >> 8) + "_" + (regionId & 255);
  }

  public static int findRegionId(int hash) {
    for (var i = 0; i < 0xFFFF; i++) {
      var hashM = Djb2.hash(regionToNameM(i));
      var hashL = Djb2.hash(regionToNameL(i));
      if (hash != hashM && hash != hashL) {
        continue;
      }
      return i;
    }
    return -1;
  }

  public static String findRegionName(int hash) {
    for (var i = 0; i < 0xFFFF; i++) {
      var nameM = regionToNameM(i);
      var hashM = Djb2.hash(nameM);
      if (hash == hashM) {
        return nameM;
      }
      var nameL = regionToNameL(i);
      var hashL = Djb2.hash(nameL);
      if (hash == hashL) {
        return nameL;
      }
    }
    return Integer.toString(hash);
  }
}
