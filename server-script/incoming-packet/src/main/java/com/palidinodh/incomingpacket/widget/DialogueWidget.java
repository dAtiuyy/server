package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.old.DialogueOld;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  WidgetId.CHATBOX_PLAYER,
  WidgetId.CHATBOX_NPC,
  WidgetId.CHATBOX_MESSAGE,
  WidgetId.CHATBOX_SELECTION,
  WidgetId.CHATBOX_MODEL,
  WidgetId.SCREEN_SELECTION,
  WidgetId.MAKE_X,
  WidgetId.SCREEN_SELECTION_1021
})
class DialogueWidget implements WidgetHandler {

  @Override
  public boolean isLockedUsable() {
    return true;
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (widgetId == WidgetId.CHATBOX_PLAYER
        || widgetId == WidgetId.CHATBOX_NPC
        || widgetId == WidgetId.CHATBOX_MESSAGE) {
      slot = 0;
    } else if (widgetId == WidgetId.CHATBOX_SELECTION) {
      slot--;
    } else if (widgetId == WidgetId.MAKE_X) {
      childId -= 14;
    }
    if (player.getDialogue() != null) {
      var dialogue = player.getDialogue();
      player.getPlugin(TreasureTrailPlugin.class).dialogueHook(dialogue, slot);
      dialogue.widgetSelected(player, childId, slot);
    } else {
      DialogueOld.handleWidget(player, childId, slot);
    }
  }
}
