package com.palidinodh.command.seniormod;

import com.palidinodh.cache.clientscript2.ScreenSelectionCs2;
import com.palidinodh.cache.clientscript2.ScreenSelectionCs2.ControlsType;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.LargeOptions2Dialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.rs.ban.BannedByUser;
import com.palidinodh.rs.ban.BannedUser;
import com.palidinodh.rs.ban.Bans;
import com.palidinodh.rs.ban.ComputerBan;
import com.palidinodh.rs.ban.IpBan;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.UserRank;
import java.util.ArrayList;
import java.util.Map;

@ReferenceName({"bans", "ipban", "pcban"})
class BanCommand implements CommandHandler, CommandHandler.SeniorModeratorRank {

  private static void banPlayerIp(Player player, Player player2, int hours, String reason) {
    Bans.addBan(
        new IpBan(
            new BannedUser(
                player2.getId(),
                player2.getUsername().toLowerCase(),
                player2.getSession().getRemoteAddress(),
                player2.getSession().getMacAddress(),
                player2.getSession().getUuid()),
            new BannedByUser(player.getId(), player.getUsername()),
            hours,
            reason));
    player2.unlock();
    player2.getGameEncoder().sendLogout();
    player2.setVisible(false);
    var duration = hours > 0 ? "for " + hours + " hours" : "permanently";
    player.getGameEncoder().sendMessage(hours + " has been ip banned " + duration + ".");
    player.log(PlayerLogType.STAFF, "ip banned " + player2.getLogName() + " " + duration);
  }

  private static void banIp(Player player, String ip, int hours, String reason) {
    Bans.addBan(
        new IpBan(
            new BannedUser(-1, "", ip, "", ""),
            new BannedByUser(player.getId(), player.getUsername()),
            hours,
            reason));
    var duration = hours > 0 ? "for " + hours + " hours" : "permanently";
    player.getGameEncoder().sendMessage(hours + " has been ip banned " + duration + ".");
    player.log(PlayerLogType.STAFF, "ip banned " + ip + " " + duration);
  }

  private static void startIpBan(Player player) {
    player
        .getGameEncoder()
        .sendEnterString(
            "Username/IP",
            ue -> {
              var player2 = player.getWorld().getPlayerByUsername(ue);
              if (player2 == null && !ue.contains(".")) {
                player.getGameEncoder().sendMessage("Unable to find user " + ue + ".");
                return;
              }
              player
                  .getGameEncoder()
                  .sendEnterAmount(
                      "Number of Hours (Permanent: 0)",
                      he -> {
                        player
                            .getGameEncoder()
                            .sendEnterString(
                                "Reason",
                                re -> {
                                  if (ue.contains(".")) {
                                    banIp(player, ue, he, re);
                                  } else {
                                    banPlayerIp(player, player2, he, re);
                                  }
                                });
                      });
            });
  }

  private static void startPcBan(Player player) {
    player
        .getGameEncoder()
        .sendEnterString(
            "Username",
            ue -> {
              var player2 = player.getWorld().getPlayerByUsername(ue);
              if (player2 == null) {
                player.getGameEncoder().sendMessage("Unable to find user " + ue + ".");
                return;
              }
              player
                  .getGameEncoder()
                  .sendEnterAmount(
                      "Number of Hours (Permanent: 0)",
                      he -> {
                        player
                            .getGameEncoder()
                            .sendEnterString(
                                "Reason",
                                re -> {
                                  Bans.addBan(
                                      new ComputerBan(
                                          new BannedUser(
                                              player2.getId(),
                                              player2.getUsername().toLowerCase(),
                                              player2.getSession().getRemoteAddress(),
                                              player2.getSession().getMacAddress(),
                                              player2.getSession().getUuid()),
                                          new BannedByUser(player.getId(), player.getUsername()),
                                          he,
                                          re));
                                  player2.unlock();
                                  player2.getGameEncoder().sendLogout();
                                  player2.setVisible(false);
                                  var duration = he > 0 ? "for " + he + " hours" : "permanently";
                                  player
                                      .getGameEncoder()
                                      .sendMessage(
                                          ue + " has been computer banned " + duration + ".");
                                  player.log(
                                      PlayerLogType.STAFF,
                                      "computer banned " + player2.getLogName() + " " + duration);
                                });
                      });
            });
  }

  private static void viewBans(Player player) {
    var map =
        Map.of(
            "User Bans", Bans.getInstance().getUserBans(),
            "IP Bans", Bans.getInstance().getIpBans(),
            "Computer Bans", Bans.getInstance().getComputerBans());
    var options = new ArrayList<DialogueOption>();
    for (var entry : map.entrySet()) {
      options.add(new DialogueOption("<col=FFFF00>" + entry.getKey(), (c, s) -> viewBans(player)));
      for (var it = entry.getValue().iterator(); it.hasNext(); ) {
        var ban = it.next();
        var userInfo = ban.getBannedUser().getUsername();
        if (player.isUsergroup(UserRank.ADMINISTRATOR)
            || player.isUsergroup(UserRank.SENIOR_MODERATOR)
            || player.isUsergroup(UserRank.ADVERTISEMENT_MANAGER)
            || player.isUsergroup(UserRank.COMMUNITY_MANAGER)) {
          userInfo = "[" + ban.getBannedUser().getIp() + "] " + ban.getBannedUser().getUsername();
        }
        var bannedBy = ban.getBannedByUser().getUsername() + " with ban reason " + ban.getReason();
        options.add(
            new DialogueOption(
                userInfo + "<br> " + bannedBy,
                (c, s) -> {
                  player.openDialogue(
                      new OptionsDialogue(
                          new DialogueOption(
                              "Lift ban",
                              (c2, s2) -> {
                                it.remove();
                                viewBans(player);
                              }),
                          new DialogueOption(
                              "Nevermind",
                              (c2, s2) -> {
                                viewBans(player);
                              })));
                }));
      }
    }
    var cs2 = new ScreenSelectionCs2();
    cs2.title("Bans");
    cs2.controls(ControlsType.REFRESH);
    cs2.entryLines(2);
    var dialogue = new LargeOptions2Dialogue(cs2, options);
    dialogue.setRefreshAction((c, s) -> viewBans(player));
    player.openDialogue(dialogue);
  }

  @Override
  public void execute(Player player, String name, String message) {
    switch (name) {
      case "bans":
        viewBans(player);
        break;
      case "ipban":
        startIpBan(player);
        break;
      case "pcban":
        startPcBan(player);
        break;
    }
  }
}
