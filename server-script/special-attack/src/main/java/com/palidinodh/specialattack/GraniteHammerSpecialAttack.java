package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.GRANITE_HAMMER)
class GraniteHammerSpecialAttack extends SpecialAttack {

  GraniteHammerSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(60);
    entry.animation(1378);
    entry.castGraphic(new Graphic(1450));
    entry.accuracyModifier(1.5);
    addEntry(entry);
  }
}
