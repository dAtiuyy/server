package com.palidinodh.npccombat.hook;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDeathItemsHooks;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.DifficultyType;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.NameType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import com.palidinodh.osrscore.world.event.bossboost.BossBoostEvent;
import com.palidinodh.osrscore.world.event.wellofgoodwill.WellOfGoodwillEvent;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.setting.Settings;
import java.util.ArrayList;
import java.util.List;

class NpcCombatDeathItems implements NpcCombatDeathItemsHooks {

  private static final int COIN_DROP_RATE = 8;
  private static final int MULTI_TARGET_COIN_DROP_RATE = 32;
  private static final NpcCombatDropTable SUPERIOR_DROP_TABLE =
      NpcCombatDropTable.builder()
          .probabilityDenominator(64)
          .log(true)
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.IMBUED_HEART).weight(1)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ETERNAL_GEM).weight(1)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DUST_BATTLESTAFF).weight(3)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MIST_BATTLESTAFF).weight(3)))
          .build();
  private static final NpcCombatDropTable TOTEM_DROP_TABLE =
      NpcCombatDropTable.builder()
          .order(NpcCombatDropTable.Order.RANDOM_UNIQUE)
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_TOTEM_BASE)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_TOTEM_MIDDLE)))
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_TOTEM_TOP)))
          .build();
  private static final NpcCombatDropTable SHARD_DROP_TABLE =
      NpcCombatDropTable.builder()
          .drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_SHARD)))
          .build();

  private static void dropClue(Npc npc, Player player, Tile dropTile, ClueScrollType clue) {
    var clueItem = new Item(clue.getBoxId());
    if ((player.canDonatorPickupItem(clueItem.getId()))
        && player.getInventory().canAddItem(clueItem)) {
      player.getInventory().addItem(clueItem);
    } else {
      npc.getController().addMapItem(clueItem, dropTile, player);
    }
    player.getPlugin(TreasureTrailPlugin.class).resetProgress(clue);
  }

  @Override
  public List<NpcCombatDropTable> getAdditionalDropTables(int npcId) {
    var tables = new ArrayList<NpcCombatDropTable>();
    var combatDef = NpcCombatDefinition.getDefinition(npcId);
    var isSuperior = combatDef.getKillCountName(npcId).equals("Superior Slayer Creature");
    if (isSuperior) {
      tables.add(SUPERIOR_DROP_TABLE);
    }
    return tables;
  }

  @Override
  public void deathDropItems(
      Npc npc,
      Player player,
      int index,
      boolean isSlayerTask,
      boolean isWildernessSlayerTask,
      Tile dropTile,
      int dropRateDivider) {
    var bondPlugin = player.getPlugin(BondPlugin.class);
    if (!npc.getCombatDef().getDrop().getClues().isEmpty()) {
      var droppedClue = false;
      for (var entry : npc.getCombatDef().getDrop().getClues().entrySet()) {
        var denominator =
            player
                .getCombat()
                .getDropRateDenominator(entry.getValue(), entry.getKey().getBoxId(), npc.getId());
        switch (npc.getId()) {
          case NpcId.GUARD_19:
          case NpcId.GUARD_21_3269:
          case NpcId.GUARD_22_3270:
          case NpcId.GUARD_22_3272:
          case NpcId.GUARD_22_3274: {
            if (npc.getArea().is("FaladorArea")
                && player.getWidgetManager().isDiaryComplete(NameType.FALADOR, DifficultyType.MEDIUM)) {
              denominator /= 1.2;
            }
            break;
          }
        }
        if (bondPlugin.isRelicUnlocked(BondRelicType.TREASURE_SEEKER_MONSTERS)) {
          denominator /= 1.25;
        }
        denominator *= dropRateDivider;
        if (!PRandom.inRange(1, Math.max(denominator, 1))) {
          continue;
        }
        dropClue(npc, player, dropTile, entry.getKey());
        droppedClue = true;
      }
      if (!droppedClue && !npc.getCombatDef().getDrop().getClues().isEmpty()) {
        var isSuperior = npc.getCombatDef().getKillCountName().equals("Superior Slayer Creature");
        var forcedClueDrop = false;
        if (isSuperior && bondPlugin.isRelicUnlocked(BondRelicType.SLAYER_SUPERIOR_CLUE)) {
          forcedClueDrop = true;
        }
        if (forcedClueDrop) {
          dropClue(
              npc,
              player,
              dropTile,
              PRandom.collectionRandom(npc.getCombatDef().getDrop().getClues().keySet()));
        }
      }
    }
    if (!npc.getCombatDef().getDrop().getPets().isEmpty()) {
      for (var entry : npc.getCombatDef().getDrop().getPets().entrySet()) {
        var itemId = entry.getKey();
        var denominator =
            player.getCombat().getDropRateDenominator(entry.getValue(), itemId, npc.getId());
        if (!PRandom.inRange(1, denominator)) {
          continue;
        }
        if (!player.getPlugin(FamiliarPlugin.class).unlockPet(itemId)) {
          continue;
        }
        player.getCombat().logNPCItem(npc.getCombatDef().getKillCountName(npc.getId()), itemId, 1);
        player
            .getSkills()
            .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.LOGGED_DROPS, 1);
        switch (npc.getId()) {
          case NpcId.COMMANDER_ZILYANA_596:
          case NpcId.GENERAL_GRAARDOR_624:
          case NpcId.KRIL_TSUTSAROTH_650:
          case NpcId.KREEARRA_580:
          case NpcId.NEX_1001_11282:
            {
              player
                  .getSkills()
                  .competitiveHiscoresUpdate(
                      CompetitiveHiscoresCategoryType.GOD_WARS_BOSS_DROPS, 1);
              if (npc.getId() == NpcId.NEX_1001_11282) {
                player
                    .getSkills()
                    .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.NEX_DROPS, 1);
              }
              break;
            }
          case NpcId.CORPOREAL_BEAST_785:
            player
                .getSkills()
                .competitiveHiscoresUpdate(
                    CompetitiveHiscoresCategoryType.CORPOREAL_BEAST_DROPS, 1);
            break;
          case NpcId.ABYSSAL_SIRE_350:
          case NpcId.ABYSSAL_SIRE_350_5887:
          case NpcId.ABYSSAL_SIRE_350_5888:
          case NpcId.ABYSSAL_SIRE_350_5889:
          case NpcId.ABYSSAL_SIRE_350_5890:
          case NpcId.ABYSSAL_SIRE_350_5891:
          case NpcId.ABYSSAL_SIRE_350_5908:
          case NpcId.ALCHEMICAL_HYDRA_426:
          case NpcId.ALCHEMICAL_HYDRA_426_8616:
          case NpcId.ALCHEMICAL_HYDRA_426_8617:
          case NpcId.ALCHEMICAL_HYDRA_426_8618:
          case NpcId.ALCHEMICAL_HYDRA_426_8619:
          case NpcId.ALCHEMICAL_HYDRA_426_8620:
          case NpcId.ALCHEMICAL_HYDRA_426_8621:
          case NpcId.CERBERUS_318:
          case NpcId.DUSK_248:
          case NpcId.DUSK_248_7855:
          case NpcId.DUSK_248_7882:
          case NpcId.DUSK_328_7888:
          case NpcId.DUSK_328_7889:
          case NpcId.KRAKEN_291:
          case NpcId.THERMONUCLEAR_SMOKE_DEVIL_301:
            player
                .getSkills()
                .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.SLAYER_BOSS_DROPS, 1);
            break;
        }
      }
    }
    if (npc.getCombatDef().getDrop().getRareDropTableDenominator() > 0) {
      var denominator =
          player
              .getCombat()
              .getDropRateDenominator(npc.getCombatDef().getDrop().getRareDropTableDenominator());
      denominator *= dropRateDivider;
      if (PRandom.inRange(1, denominator)) {
        RareDropTable.rareDropTable(npc, player, dropTile);
      }
    }
    if (npc.getCombatDef().getDrop().getGemDropTableDenominator() > 0) {
      var denominator =
          player
              .getCombat()
              .getDropRateDenominator(npc.getCombatDef().getDrop().getGemDropTableDenominator());
      denominator *= dropRateDivider;
      if (PRandom.inRange(1, denominator)) {
        RareDropTable.gemDropTable(npc, player, dropTile);
      }
    }
    if (index == 0) {
      if (npc.getCombatDef().getKillCount().isSendMessage()) {
        player.getSkills().competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.BOSS_KILLS, 1);
      }
      if (player.getWorld().getWorldEvent(BossBoostEvent.class).isBoss(npc.getId())
          || player.getWorld().getWorldEvent(WellOfGoodwillEvent.class).isNpcBoost(npc.getId())) {
        player
            .getSkills()
            .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.BOOSTED_BOSS_KILLS, 1);
      }
    }
    var allowSpecialDrops =
        (npc.getCombatDef().getDrop().hasDrops() && player.getController().canTeleport(false)
            || npc.getArea().inWilderness());
    var coinRate = COIN_DROP_RATE;
    if (npc.getController().inMultiCombat() && !npc.getCombatDef().getKillCount().isSendMessage()) {
      if (player.getEquipment().getWeaponType().isMultiTarget()
          || player.getMagic().getActiveSpell() != null
              && player.getMagic().getActiveSpell().getMultiTarget()) {
        coinRate = MULTI_TARGET_COIN_DROP_RATE;
      }
    }
    if (!Settings.getInstance().isSpawn() && allowSpecialDrops && PRandom.randomE(coinRate) == 0) {
      int coinPouch = ItemId.COIN_POUCH_T1_60053;
      if (npc.getDef().getCombatLevel() >= 200) {
        coinPouch = ItemId.COIN_POUCH_T4_60056;
      } else if (npc.getDef().getCombatLevel() >= 100) {
        coinPouch = ItemId.COIN_POUCH_T3_60055;
      } else if (npc.getDef().getCombatLevel() >= 50) {
        coinPouch = ItemId.COIN_POUCH_T2_60054;
      }
      npc.getController().addMapItem(new Item(coinPouch), dropTile, player);
    }
    if (!Settings.getInstance().isSpawn()
        && allowSpecialDrops
        && PRandom.randomE(npc.getController().inMultiCombat() ? 100 : 50) == 0) {
      var half1Count =
          (int)
              Math.min(
                  player.getItemCount(ItemId.TOOTH_HALF_OF_KEY)
                      + player.getItemCount(ItemId.TOOTH_HALF_OF_KEY_NOTED),
                  Item.MAX_AMOUNT);
      var half2Count =
          (int)
              Math.min(
                  player.getItemCount(ItemId.LOOP_HALF_OF_KEY)
                      + player.getItemCount(ItemId.LOOP_HALF_OF_KEY_NOTED),
                  Item.MAX_AMOUNT);
      int keyHalfId;
      if (half1Count < half2Count) {
        keyHalfId = ItemId.TOOTH_HALF_OF_KEY;
      } else if (half2Count < half1Count) {
        keyHalfId = ItemId.LOOP_HALF_OF_KEY;
      } else {
        keyHalfId = PRandom.randomI(1) == 0 ? ItemId.TOOTH_HALF_OF_KEY : ItemId.LOOP_HALF_OF_KEY;
      }
      if (player.canDonatorPickupItem(keyHalfId)
          && player.getInventory().canAddItem(keyHalfId, 1)) {
        player.getInventory().addItem(keyHalfId, 1);
      } else {
        npc.getController().addMapItem(new Item(keyHalfId), dropTile, player);
      }
    }
    if (isWildernessSlayerTask) {
      if (player.getPlugin(BountyHunterPlugin.class).hasEmblem()) {
        if (PRandom.randomE(8) == 0) {
          npc.getController().addMapItem(new Item(ItemId.DARK_CRAB), dropTile, player);
        }
      }
      if (npc.getCombat().getMaxHitpoints() * 0.8 < 320
          && PRandom.randomE((int) (320 - npc.getCombat().getMaxHitpoints() * 0.8) / 4) == 0) {
        npc.getController().addMapItem(new Item(ItemId.SLAYERS_ENCHANTMENT), dropTile, player);
      }
    }
    if (npc.getArea().inWilderness()
        && PRandom.randomE(30) == 0
        && !player.hasItem(ItemId.LOOTING_BAG)) {
      npc.getController().addMapItem(new Item(ItemId.LOOTING_BAG), dropTile, player);
    }
    var isCursed =
        npc.getDef().getName().startsWith("Cursed")
            && isWildernessSlayerTask
            && npc.getArea().inWilderness();
    var isSuperior =
        npc.getCombatDef().getKillCountName(npc.getId()).equals("Superior Slayer Creature");
    if (isSuperior || isCursed && PRandom.randomE(25) == 0) {
      var denominator =
          (int) (200 - (StrictMath.pow(npc.getCombatDef().getSlayer().getLevel() + 55, 2) / 125.0));
      denominator =
          player.getCombat().getDropRateDenominator(denominator, ItemId.IMBUED_HEART, npc.getId());
      if (PRandom.inRange(1, denominator)) {
        SUPERIOR_DROP_TABLE.dropItems(npc, player, dropTile);
      }
    }
    if (npc.getArea().inCatacombsOfKourend()) {
      var denominator = 500 - Math.min(npc.getCombat().getMaxHitpoints(), 400);
      denominator =
          player
              .getCombat()
              .getDropRateDenominator(denominator, ItemId.DARK_TOTEM_BASE, npc.getId());
      if (isSuperior || PRandom.inRange(1, denominator)) {
        TOTEM_DROP_TABLE.dropItems(npc, player, dropTile);
      }
      denominator = (int) (0.66 * (500 - Math.min(npc.getCombat().getMaxHitpoints(), 400)));
      denominator =
          player.getCombat().getDropRateDenominator(denominator, ItemId.ANCIENT_SHARD, npc.getId());
      if (PRandom.inRange(1, denominator)) {
        SHARD_DROP_TABLE.dropItems(npc, player, dropTile);
      }
    }
    if (npc.getCombatDef().isTypeUndead() || npc.getArea().is("SlayerTower")) {
      var denominator = 500 - Math.min(npc.getCombat().getMaxHitpoints(), 400);
      denominator =
          player
              .getCombat()
              .getDropRateDenominator(denominator, ItemId.BARROWS_AMULET_60038, npc.getId());
      if (PRandom.inRange(1, denominator)) {
        npc.getController().addMapItem(new Item(ItemId.BARROWS_AMULET_60038), dropTile, player);
      }
    }
  }
}
