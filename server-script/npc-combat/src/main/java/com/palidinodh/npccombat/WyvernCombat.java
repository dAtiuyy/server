package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class WyvernCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().clue(ClueScrollType.HARD, 118);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(12000).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WYVERN_VISAGE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(2560).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRANITE_BOOTS)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(512).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRANITE_LONGSWORD)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BOLTS, 12, 29)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORSTOL_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RANARR_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YEW_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAHOGANY_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNIDENTIFIED_SMALL_FOSSIL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNIDENTIFIED_MEDIUM_FOSSIL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNIDENTIFIED_LARGE_FOSSIL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNIDENTIFIED_RARE_FOSSIL)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_FULL_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BATTLESTAFF_NOTED, 3, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_KITESHIELD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_BATTLESTAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_CROSSBOW_U)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PICKAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_PLATEBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_BAR_NOTED, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_ORE_NOTED, 1, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_ARROW, 38, 42)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM_NOTED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE_NOTED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME_NOTED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATERMELON_SEED, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SEAWEED_SPORE, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_POTION_4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CALCITE, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PYROPHOSPHITE, 2)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TEAK_LOGS_NOTED, 35)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPE_GRASS_NOTED, 10, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COAL_NOTED, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_BAR_NOTED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATER_RUNE, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_RUNE, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LOBSTER, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 3000)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NUMULITE, 1, 21)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WYVERN_BONES)));
    drop.table(dropTable.build());

    var longTailedCombat = NpcCombatDefinition.builder();
    longTailedCombat.id(NpcId.LONG_TAILED_WYVERN_152);
    longTailedCombat.hitpoints(NpcCombatHitpoints.total(200));
    longTailedCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(125)
            .magicLevel(90)
            .rangedLevel(90)
            .defenceLevel(120)
            .bonus(BonusType.MELEE_DEFENCE, 70)
            .bonus(BonusType.DEFENCE_MAGIC, 140)
            .bonus(BonusType.DEFENCE_RANGED, 120)
            .build());
    longTailedCombat.slayer(NpcCombatSlayer.builder().level(66).build());
    longTailedCombat.aggression(NpcCombatAggression.PLAYERS);
    longTailedCombat.killCount(NpcCombatKillCount.builder().asName("Fossil Island wyvern").build());
    longTailedCombat.deathAnimation(7652).blockAnimation(7659);
    longTailedCombat.type(NpcCombatType.DRAGON);
    longTailedCombat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(13));
    style.animation(7654).attackSpeed(5);
    longTailedCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.DRAGONFIRE);
    style.damage(NpcCombatDamage.maximum(50));
    style.animation(7657).attackSpeed(6);
    style.castGraphic(new Graphic(501, 124)).targetGraphic(new Graphic(502));
    style.projectile(NpcCombatProjectile.id(500));
    longTailedCombat.style(style.build());

    var spittingCombat = NpcCombatDefinition.builder();
    spittingCombat.id(NpcId.SPITTING_WYVERN_139);
    spittingCombat.hitpoints(NpcCombatHitpoints.total(200));
    spittingCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(125)
            .magicLevel(125)
            .rangedLevel(125)
            .defenceLevel(120)
            .bonus(BonusType.MELEE_DEFENCE, 70)
            .bonus(BonusType.DEFENCE_MAGIC, 140)
            .bonus(BonusType.DEFENCE_RANGED, 120)
            .build());
    spittingCombat.slayer(NpcCombatSlayer.builder().level(66).build());
    spittingCombat.aggression(NpcCombatAggression.PLAYERS);
    spittingCombat.killCount(NpcCombatKillCount.builder().asName("Fossil Island wyvern").build());
    spittingCombat.deathAnimation(7652).blockAnimation(7659);
    spittingCombat.type(NpcCombatType.DRAGON);
    spittingCombat.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(9));
    style.animation(7658).attackSpeed(4);
    spittingCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.DRAGONFIRE);
    style.damage(NpcCombatDamage.maximum(50));
    style.animation(7657).attackSpeed(6);
    style.castGraphic(new Graphic(501, 124)).targetGraphic(new Graphic(502));
    style.projectile(NpcCombatProjectile.id(500));
    spittingCombat.style(style.build());

    var talonedCombat = NpcCombatDefinition.builder();
    talonedCombat.id(NpcId.TALONED_WYVERN_147);
    talonedCombat.hitpoints(NpcCombatHitpoints.total(200));
    talonedCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(120)
            .magicLevel(90)
            .rangedLevel(90)
            .defenceLevel(120)
            .bonus(BonusType.MELEE_DEFENCE, 70)
            .bonus(BonusType.DEFENCE_MAGIC, 140)
            .bonus(BonusType.DEFENCE_RANGED, 120)
            .build());
    talonedCombat.slayer(NpcCombatSlayer.builder().level(66).build());
    talonedCombat.aggression(NpcCombatAggression.PLAYERS);
    talonedCombat.killCount(NpcCombatKillCount.builder().asName("Fossil Island wyvern").build());
    talonedCombat.deathAnimation(7652).blockAnimation(7659);
    talonedCombat.type(NpcCombatType.DRAGON);
    talonedCombat.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(13));
    style.animation(7651).attackSpeed(4);
    talonedCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.DRAGONFIRE);
    style.damage(NpcCombatDamage.maximum(50));
    style.animation(7657).attackSpeed(6);
    style.castGraphic(new Graphic(501, 124)).targetGraphic(new Graphic(502));
    style.projectile(NpcCombatProjectile.id(500));
    talonedCombat.style(style.build());

    var cursedCombat = NpcCombatDefinition.builder();
    cursedCombat.id(NpcId.CURSED_WYVERN_210_16011);
    cursedCombat.hitpoints(NpcCombatHitpoints.builder().total(300).build());
    cursedCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(150)
            .magicLevel(90)
            .rangedLevel(90)
            .defenceLevel(120)
            .bonus(BonusType.MELEE_DEFENCE, 70)
            .bonus(BonusType.DEFENCE_MAGIC, 140)
            .bonus(BonusType.DEFENCE_RANGED, 120)
            .build());
    cursedCombat.slayer(NpcCombatSlayer.builder().level(66).build());
    cursedCombat.deathAnimation(7652).blockAnimation(7659);
    cursedCombat.type(NpcCombatType.DRAGON);
    cursedCombat.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(16));
    style.animation(7658).attackSpeed(5);
    cursedCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.DRAGONFIRE);
    style.damage(NpcCombatDamage.maximum(50));
    style.animation(7657).attackSpeed(6);
    style.castGraphic(new Graphic(501, 124)).targetGraphic(new Graphic(502));
    style.projectile(NpcCombatProjectile.id(500));
    cursedCombat.style(style.build());

    return Arrays.asList(
        longTailedCombat.build(),
        spittingCombat.build(),
        talonedCombat.build(),
        cursedCombat.build());
  }

  @Override
  public void applyAttackEndHook(
      NpcCombatStyle combatStyle, Entity opponent, int applyAttackLoopCount, HitEvent hitEvent) {
    if (!(opponent instanceof Player)
        || combatStyle.getType().getHitStyleType() != HitStyleType.DRAGONFIRE
        || PRandom.randomE(5) != 0) {
      return;
    }
    var player = opponent.asPlayer();
    var hasAncientWyvernShield =
        player.getEquipment().getShieldId() == ItemId.ANCIENT_WYVERN_SHIELD
            || player.getEquipment().getShieldId() == ItemId.ANCIENT_WYVERN_SHIELD_21634;
    if (player.getController().canMagicBind() && !hasAncientWyvernShield) {
      player.getController().setMagicBind(8, npc);
      if (player.getCombat().getHitDelay() < 8) {
        player.getCombat().setHitDelay(8);
      }
      player.getGameEncoder().sendMessage("You've been frozen!");
    }
  }

  @Override
  public void attackedActionsOpponentHook(Entity opponent) {
    if (!opponent.isPlayer() || npc.getId() != NpcId.CURSED_WYVERN_210_16011) {
      return;
    }
    opponent.asPlayer().getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (npc.getId() != NpcId.CURSED_WYVERN_210_16011) {
      return;
    }
    if (!player.getPlugin(SlayerPlugin.class).isWildernessTask(npc)) {
      return;
    }
    var table =
        NpcCombatDefinition.getDefinition(NpcId.ANCIENT_WYVERN_210)
            .getDrop()
            .getTable(npc, player, 2, 0, true);
    if (table == null) {
      return;
    }
    table.dropItems(npc, player, dropTile);
  }

  @Override
  public NpcCombatDropTable deathDropItemsTableHook(
      Player player, int dropRateDivider, int roll, NpcCombatDropTable table) {
    if (npc.getId() == NpcId.CURSED_WYVERN_210_16011) {
      if (!player.getPlugin(SlayerPlugin.class).isWildernessTask(npc)) {
        player.getGameEncoder().sendMessage("Without an assigned task, the loot turns to dust...");
        return null;
      }
      if (roll != 0) {
        return table;
      }
      if (table.getProbabilityDenominator() != NpcCombatDropTable.COMMON) {
        return table;
      }
      return NpcCombatDefinition.getDefinition(NpcId.ANCIENT_WYVERN_210)
          .getDrop()
          .getTable(NpcCombatDropTable.COMMON);
    }
    return table;
  }
}
