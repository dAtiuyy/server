package com.palidinodh.maparea.kandarin.krakencove.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class KrakenCoveNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(2278, 10035), NpcId.WHIRLPOOL));
    spawns.add(new NpcSpawn(2, new Tile(2245, 10013), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2245, 10026), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2246, 10019), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2266, 10013), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2261, 10016), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2259, 10020), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2255, 10024), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2255, 10029), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2254, 10035), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2257, 10039), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2264, 10003), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2259, 9996), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2270, 9990), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2266, 9992), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2269, 9994), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2275, 10009), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2279, 10010), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2285, 10005), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2293, 9991), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2297, 9990), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2297, 9998), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2296, 10002), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2299, 10011), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2298, 10017), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(2, new Tile(2291, 10016), NpcId.WHIRLPOOL_127));
    spawns.add(new NpcSpawn(4, new Tile(2288, 9988), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(2286, 9989), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(2286, 9987), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(2281, 9988), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(2281, 9990), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(2264, 9988), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(2261, 9987), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(2259, 9990), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(2258, 9988), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(2255, 9989), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(2249, 9991), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(2251, 9993), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(2248, 9994), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(2250, 9995), NpcId.WATERFIEND_115));
    spawns.add(new NpcSpawn(4, new Tile(2248, 9997), NpcId.WATERFIEND_115));

    return spawns;
  }
}
