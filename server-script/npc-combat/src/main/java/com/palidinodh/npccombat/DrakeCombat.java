package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import java.util.Arrays;
import java.util.List;

class DrakeCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().gemDropTableDenominator(85).clue(ClueScrollType.HARD, 128);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(5000).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_KNIFE, 100, 200)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_THROWNAXE, 100, 200)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(1280).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAKES_CLAW)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAKES_TOOTH)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_MACE)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_SNAPDRAGON_NOTED, 2, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE_NOTED)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME_NOTED, 2, 3)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_DWARF_WEED_NOTED, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL_NOTED, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPE_GRASS_SEED, 3)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_EARTH_STAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_DHIDE_VAMB)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_FULL_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 22, 40)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED_NOTED, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE_NOTED, 1, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM_NOTED, 1, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.IRIT_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LANTADYME_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOADFLAX_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPDRAGON_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.KWUARM_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AVANTOE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CACTUS_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.POISON_IVY_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BELLADONNA_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DIAMOND_NOTED, 3, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SWORDFISH, 1, 2)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NATURE_RUNE, 30, 60)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_RUNE, 103, 197)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 25, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_ARROW, 35, 65)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_HARRALANDER, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 1008, 1947)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAKE_BONES)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.DRAKE_192);
    combat.hitpoints(NpcCombatHitpoints.total(250));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(140)
            .magicLevel(112)
            .rangedLevel(140)
            .defenceLevel(120)
            .bonus(BonusType.MELEE_ATTACK, 40)
            .bonus(BonusType.ATTACK_RANGED, 40)
            .bonus(BonusType.MELEE_DEFENCE, 60)
            .bonus(BonusType.DEFENCE_MAGIC, 20)
            .bonus(BonusType.DEFENCE_RANGED, 100)
            .build());
    combat.slayer(NpcCombatSlayer.builder().level(84).superiorId(NpcId.GUARDIAN_DRAKE_386).build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.deathAnimation(8277);
    combat.type(NpcCombatType.DRAGON);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(15));
    style.animation(8275).attackSpeed(4);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(15));
    style.animation(8276).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(1636));
    combat.style(style.build());

    var superiorCombat = NpcCombatDefinition.builder();
    superiorCombat.id(NpcId.GUARDIAN_DRAKE_386);
    superiorCombat.hitpoints(
        NpcCombatHitpoints.builder().total(630).barType(HitpointsBarType.GREEN_RED_60).build());
    superiorCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(270)
            .magicLevel(300)
            .rangedLevel(240)
            .defenceLevel(200)
            .bonus(BonusType.MELEE_ATTACK, 40)
            .bonus(BonusType.ATTACK_RANGED, 40)
            .bonus(BonusType.DEFENCE_STAB, 60)
            .bonus(BonusType.DEFENCE_SLASH, 100)
            .bonus(BonusType.DEFENCE_CRUSH, 100)
            .bonus(BonusType.DEFENCE_MAGIC, 20)
            .bonus(BonusType.DEFENCE_RANGED, 150)
            .build());
    superiorCombat.slayer(NpcCombatSlayer.builder().level(84).experience(7245).build());
    superiorCombat.aggression(NpcCombatAggression.PLAYERS);
    superiorCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    superiorCombat.killCount(
        NpcCombatKillCount.builder().asName("Superior Slayer Creature").build());
    superiorCombat.deathAnimation(8277);
    superiorCombat.type(NpcCombatType.DRAGON);
    superiorCombat.drop(drop.rolls(3).build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(35));
    style.animation(8275).attackSpeed(4);
    superiorCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(35));
    style.animation(8276).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(1636));
    superiorCombat.style(style.build());

    return Arrays.asList(combat.build(), superiorCombat.build());
  }

  @Override
  public int dropTableDenominatorHook(
      Player player, int dropRateDivider, int roll, NpcCombatDropTable table, int denominator) {
    if (denominator > 100 && player.getPlugin(SlayerPlugin.class).isAnyTask(npc)) {
      denominator /= 5;
    }
    return denominator;
  }
}
