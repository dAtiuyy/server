package com.palidinodh.npccombat.hook;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.item.RandomItem;
import java.util.Arrays;
import java.util.List;

class CreatureCreationCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var swordchickDrop = NpcCombatDrop.builder();
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(10);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_SATCHEL)));
    swordchickDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(8);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TEA_FLASK)));
    swordchickDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FEATHER, 10, 40)));
    swordchickDrop.table(dropTable.build());

    var swordchickCombat = NpcCombatDefinition.builder();
    swordchickCombat.id(NpcId.SWORDCHICK_46);
    swordchickCombat.hitpoints(NpcCombatHitpoints.total(35));
    swordchickCombat.deathAnimation(5389).blockAnimation(5388);
    swordchickCombat.drop(swordchickDrop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(5));
    style.animation(5387).attackSpeed(4);
    swordchickCombat.style(style.build());

    return Arrays.asList(swordchickCombat.build());
  }
}
