package com.palidinodh.rs.communicationv2.event;

import com.palidinodh.rs.communicationv2.CommunicationEvent;
import com.palidinodh.rs.communicationv2.notification.PrivateChatStateNotification;
import lombok.Getter;

@Getter
public class PrivateChatStateEvent extends CommunicationEvent {

  private int userId;
  private int privateChatState;

  public PrivateChatStateEvent(int userId, int privateChatState) {
    this.userId = userId;
    this.privateChatState = privateChatState;
  }

  @Override
  public void mainAction() {
    var player = getMainServer().getPlayersById().get(userId);
    if (player == null) {
      return;
    }
    player.setPrivateChatStatus(privateChatState);
    getMainServer().addNotification(new PrivateChatStateNotification(player));
  }
}
