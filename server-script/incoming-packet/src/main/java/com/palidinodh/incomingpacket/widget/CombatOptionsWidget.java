package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.VarpId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.ViewportContainer;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.magic.AutocastType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.COMBAT)
class CombatOptionsWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    switch (childId) {
      case 3:
      case 7:
      case 11:
      case 15:
        player.getMagic().getAttackStyle().clear();
        player.getCombat().setAttackStyle((childId - 3) / 4);
        break;
      case 20:
      case 25:
        {
          var autocast = AutocastType.get(player);
          if (autocast == null) {
            player.getGameEncoder().sendMessage("You can't autocast right now.");
            break;
          }
          player.getGameEncoder().setVarp(VarpId.SPELL_SELECT_WEAPON, autocast.getFirstItemId());
          player.getWidgetManager().sendWidget(ViewportContainer.COMBAT, WidgetId.SPELL_SELECT);
          player.getGameEncoder().sendWidgetSettings(WidgetId.SPELL_SELECT, 1, 0, 52, 2);
          player.putAttribute("magic_defensive", childId == 20);
          break;
        }
      case 29:
        player.getCombat().setAutoRetaliate(!player.getCombat().getAutoRetaliate());
        break;
      case 35:
        player.getCombat().activateSpecialAttack();
        break;
    }
  }
}
