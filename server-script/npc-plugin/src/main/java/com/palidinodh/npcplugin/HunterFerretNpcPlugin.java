package com.palidinodh.npcplugin;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.NpcPlugin;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.playerplugin.hunter.HunterPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.FERRET)
class HunterFerretNpcPlugin implements NpcPlugin {

  @Inject private Npc npc;

  @Override
  public void tick() {
    if (npc.getMovement().getWalkDir() == -1) {
      return;
    }
    if (npc.isLocked()) {
      return;
    }
    var mapObject = npc.getController().getSolidMapObject(npc);
    if (mapObject == null || mapObject.getId() != ObjectId.BOX_TRAP_9380) {
      return;
    }
    if (!(mapObject.getAttachment() instanceof TempMapObject)
        || !(((TempMapObject) mapObject.getAttachment()).getAttachment() instanceof Integer)) {
      return;
    }
    var tempMapObject = ((TempMapObject) mapObject.getAttachment());
    var player = Main.getWorld().getPlayerById((Integer) tempMapObject.getAttachment());
    if (player == null
        || !player.getPlugin(HunterPlugin.class).canCaptureTrap(ObjectId.SHAKING_BOX_9384)) {
      return;
    }
    if (player
        .getPlugin(HunterPlugin.class)
        .trapSuccess(
            npc, HunterPlugin.getCapturedTrapLevelRequirement(ObjectId.SHAKING_BOX_9384))) {
      tempMapObject.updateMapObject(ObjectId.SHAKING_BOX_9384);
      npc.getCombat().startDeath(2);
    } else {
      tempMapObject.updateMapObject(ObjectId.BOX_TRAP_9385);
    }
    tempMapObject.setTick(HunterPlugin.TRAP_EXPIRIY);
    npc.getController().sendMapObject(mapObject);
  }
}
