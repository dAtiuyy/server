package com.palidinodh.maparea.morytania.templetrekking.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class TempleTrekkingNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2449, 5019), NpcId.NAIL_BEAST_69));
    spawns.add(new NpcSpawn(4, new Tile(2454, 5019), NpcId.NAIL_BEAST_69));
    spawns.add(new NpcSpawn(4, new Tile(2456, 5024), NpcId.NAIL_BEAST_69));
    spawns.add(new NpcSpawn(4, new Tile(2452, 5024), NpcId.NAIL_BEAST_69));
    spawns.add(new NpcSpawn(4, new Tile(2447, 5024), NpcId.NAIL_BEAST_69));
    spawns.add(new NpcSpawn(4, new Tile(2453, 5029), NpcId.NAIL_BEAST_69));
    spawns.add(new NpcSpawn(4, new Tile(2458, 5031), NpcId.NAIL_BEAST_69));
    spawns.add(new NpcSpawn(4, new Tile(2462, 5022), NpcId.NAIL_BEAST_69));
    spawns.add(new NpcSpawn(4, new Tile(2462, 5027), NpcId.NAIL_BEAST_69));
    spawns.add(new NpcSpawn(4, new Tile(2467, 5027), NpcId.NAIL_BEAST_69));
    spawns.add(new NpcSpawn(4, new Tile(2465, 5031), NpcId.NAIL_BEAST_69));
    spawns.add(new NpcSpawn(4, new Tile(2455, 5036), NpcId.NAIL_BEAST_69));
    spawns.add(new NpcSpawn(4, new Tile(2449, 5035), NpcId.NAIL_BEAST_69));
    spawns.add(new NpcSpawn(4, new Tile(2447, 5040), NpcId.NAIL_BEAST_69));
    spawns.add(new NpcSpawn(4, new Tile(2459, 5040), NpcId.NAIL_BEAST_69));

    return spawns;
  }
}
