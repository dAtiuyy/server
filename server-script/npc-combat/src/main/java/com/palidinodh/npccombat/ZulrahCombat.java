package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.NullObjectId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropLocationType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;

class ZulrahCombat extends NpcCombat {

  private static final int RANGED = 0;
  private static final int MELEE = 1;
  private static final int MAGIC = 2;
  private static final int RANGED_MAGIC = 3;
  private static final int MAGIC_RANGED = 4;
  private static final int RANGED_RANDOM_MAGIC = 5;
  private static final Tile TILE_CENTER = new Tile(2266, 3072);
  private static final Tile TILE_BOTTOM = new Tile(2266, 3061);
  private static final Tile TILE_EAST = new Tile(2276, 3072);
  private static final Tile TILE_WEST = new Tile(2256, 3070);
  private static final Tile[] GAS_TILES = {
    new Tile(2262, 3075),
    new Tile(2262, 3072),
    new Tile(2262, 3069),
    new Tile(2265, 3068),
    new Tile(2268, 3068),
    new Tile(2271, 3068),
    new Tile(2272, 3071),
    new Tile(2272, 3074)
  };
  private static final long PHASE_LENGTH = 14;
  private static final Phase[][] PHASES = {
    {
      new Phase(
          TILE_CENTER,
          0,
          new GasSpawn(PHASE_LENGTH, new int[] {4, 5}),
          new GasSpawn(PHASE_LENGTH, new int[] {2, 3}),
          new GasSpawn(PHASE_LENGTH, new int[] {6, 7}),
          new GasSpawn(PHASE_LENGTH, new int[] {0, 1})),
      new Phase(TILE_CENTER, 1),
      new Phase(PHASE_LENGTH - 1, TILE_CENTER, 2),
      new Phase(
          TILE_BOTTOM,
          0,
          new SpawnModel[] {
            new SnakelingSpawn(PHASE_LENGTH, new Tile(2263, 3076)),
            new SnakelingSpawn(PHASE_LENGTH, new Tile(2263, 3073)),
            new GasSpawn(PHASE_LENGTH, new int[] {2, 3}),
            new GasSpawn(PHASE_LENGTH, new int[] {5, 6}),
            new SnakelingSpawn(PHASE_LENGTH, new Tile(2273, 3075)),
            new SnakelingSpawn(PHASE_LENGTH, new Tile(2273, 3078))
          }),
      new Phase(PHASE_LENGTH + 4, TILE_CENTER, 1),
      new Phase(PHASE_LENGTH + 2, TILE_WEST, 2),
      new Phase(
          TILE_BOTTOM,
          0,
          new SpawnModel[] {
            new GasSpawn(new int[] {4, 5}),
            new GasSpawn(new int[] {2, 3}),
            new GasSpawn(new int[] {1, 0}),
            new SnakelingSpawn(new Tile(2272, 3069)),
            new SnakelingSpawn(new Tile(2273, 3075)),
            new SnakelingSpawn(new Tile(2273, 3078)),
            new SnakelingSpawn(new Tile(2273, 3072))
          }),
      new Phase(
          TILE_BOTTOM,
          2,
          new SpawnModel[] {
            new SnakelingSpawn(PHASE_LENGTH, new Tile(2263, 3070)),
            new GasSpawn(PHASE_LENGTH, new int[] {3, 4}),
            new SnakelingSpawn(PHASE_LENGTH, new Tile(2263, 3076)),
            new GasSpawn(PHASE_LENGTH, new int[] {5, 6}),
            new SnakelingSpawn(PHASE_LENGTH, new Tile(2263, 3073))
          }),
      new Phase(
          TILE_WEST,
          3,
          new SpawnModel[] {
            new GasSpawn(PHASE_LENGTH * 2, new int[] {2, 3}),
            new GasSpawn(PHASE_LENGTH * 2, new int[] {4, 5}),
            new GasSpawn(PHASE_LENGTH * 2, new int[] {1, 0}),
            new GasSpawn(PHASE_LENGTH * 2, new int[] {6, 7})
          }),
      new Phase(TILE_CENTER, 1)
    }
  };

  @Inject private Npc npc;
  private boolean initialLock = true;
  private Entity lastAttackingEntity;
  private int rotation;
  private int phase = -1;
  private int phaseTick;
  private HitStyleType attackStyle;
  private List<SpawnModel> spawnModels = new ArrayList<>();
  private boolean spawningStarted;
  private List<SpawnEvent> spawnEvents = new ArrayList<>();
  private int eventDelay;
  private List<TempMapObject> mapObjectEvents = new ArrayList<>();
  private List<Npc> snakelings = new ArrayList<>();

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .locationType(NpcCombatDropLocationType.UNDER_OPPONENT)
            .rareDropTableDenominator(27)
            .clue(ClueScrollType.ELITE, 75)
            .pet(ItemId.PET_SNAKELING, 8000)
            .rolls(2);
    var dropTable =
        NpcCombatDropTable.builder().probabilityDenominator(6580).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TANZANITE_MUTAGEN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGMA_MUTAGEN)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(6000).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JAR_OF_SWAMP)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(256).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TANZANITE_FANG)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_FANG)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SERPENTINE_VISAGE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_ONYX)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SPIRIT_SEED)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_MED_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_HALBERD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPDRAGON_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DWARF_WEED_NOTED, 30)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOADFLAX_NOTED, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORSTOL_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOADFLAX_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPDRAGON_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DWARF_WEED_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORSTOL_SEED)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BATTLESTAFF_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 300)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 200)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 500)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PALM_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PAPAYA_TREE_SEED, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CALQUAT_TREE_SEED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAKESKIN_NOTED, 35)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_ORE_NOTED, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 1500)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FLAX_NOTED, 1000)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YEW_LOGS_NOTED, 35)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_BAR_NOTED, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COAL_NOTED, 200)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_BONES_NOTED, 12)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAHOGANY_LOGS_NOTED, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ZUL_ANDRA_TELEPORT, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MANTA_RAY_NOTED, 35)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIDOTE_PLUS_PLUS_4_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGONSTONE_BOLT_TIPS, 12)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRAPES_NOTED, 250)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COCONUT_NOTED, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SWAMP_TAR, 1000)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ZULRAHS_SCALES, 500)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ZULRAHS_SCALES, 100, 300)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ZUL_ANDRA_TELEPORT)));
    drop.table(dropTable.build());

    var combat1 = NpcCombatDefinition.builder();
    combat1.id(NpcId.ZULRAH_725);
    combat1.hitpoints(
        NpcCombatHitpoints.builder().total(500).barType(HitpointsBarType.GREEN_RED_100).build());
    combat1.stats(
        NpcCombatStats.builder()
            .magicLevel(300)
            .rangedLevel(300)
            .defenceLevel(300)
            .bonus(BonusType.ATTACK_MAGIC, 50)
            .bonus(BonusType.ATTACK_RANGED, 50)
            .bonus(BonusType.DEFENCE_MAGIC, -45)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    combat1.aggression(NpcCombatAggression.builder().range(20).build());
    combat1.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat1.focus(NpcCombatFocus.builder().bypassMapObjects(true).build());
    combat1.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat1.deathAnimation(5804).blockAnimation(5808);
    combat1.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(41));
    style.animation(5069).attackSpeed(3).attackRange(20);
    style.projectile(NpcCombatProjectile.builder().id(1044).speedType(HitStyleType.MAGIC).build());
    style.effect(NpcCombatEffect.builder().includeMiss(true).chance(75).venom(6).build());
    combat1.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(41));
    style.animation(5069).attackSpeed(3).attackRange(20);
    style.projectile(NpcCombatProjectile.id(1046));
    style.effect(NpcCombatEffect.builder().includeMiss(true).chance(75).venom(6).build());
    combat1.style(style.build());

    var combat2 = NpcCombatDefinition.builder();
    combat2.id(NpcId.ZULRAH_725_2043);
    combat2.hitpoints(
        NpcCombatHitpoints.builder().total(500).barType(HitpointsBarType.GREEN_RED_100).build());
    combat2.stats(
        NpcCombatStats.builder()
            .magicLevel(300)
            .rangedLevel(300)
            .defenceLevel(300)
            .bonus(BonusType.ATTACK_MAGIC, 50)
            .bonus(BonusType.ATTACK_RANGED, 50)
            .bonus(BonusType.DEFENCE_RANGED, 300)
            .build());
    combat2.aggression(NpcCombatAggression.builder().range(20).build());
    combat2.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat2.focus(NpcCombatFocus.builder().bypassMapObjects(true).build());
    combat2.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat2.deathAnimation(5804).blockAnimation(5808);
    combat2.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.RANGED)
            .subHitStyleType(HitStyleType.MELEE)
            .build());
    style.damage(NpcCombatDamage.builder().maximum(41).ignorePrayer(true).build());
    style.animation(5806).attackSpeed(7).attackRange(20);
    style.projectile(NpcCombatProjectile.builder().id(0).speedMinimumDistance(10).build());
    style.effect(NpcCombatEffect.builder().includeMiss(true).chance(75).venom(6).build());
    var targetTile = NpcCombatTargetTile.builder().radius(2).radiusXY(true);
    style.specialAttack(targetTile.build());
    combat2.style(style.build());

    var combat3 = NpcCombatDefinition.builder();
    combat3.id(NpcId.ZULRAH_725_2044);
    combat3.hitpoints(
        NpcCombatHitpoints.builder().total(500).barType(HitpointsBarType.GREEN_RED_100).build());
    combat3.stats(
        NpcCombatStats.builder()
            .magicLevel(300)
            .rangedLevel(300)
            .defenceLevel(300)
            .bonus(BonusType.ATTACK_MAGIC, 50)
            .bonus(BonusType.ATTACK_RANGED, 50)
            .bonus(BonusType.DEFENCE_MAGIC, 300)
            .build());
    combat3.aggression(NpcCombatAggression.builder().range(20).build());
    combat3.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat3.focus(NpcCombatFocus.builder().bypassMapObjects(true).build());
    combat3.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat3.deathAnimation(5804).blockAnimation(5808);
    combat3.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(41));
    style.animation(5069).attackSpeed(3).attackRange(20);
    style.projectile(NpcCombatProjectile.builder().id(1044).speedType(HitStyleType.MAGIC).build());
    style.effect(NpcCombatEffect.builder().includeMiss(true).chance(75).venom(6).build());
    combat3.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(41));
    style.animation(5069).attackSpeed(3).attackRange(20);
    style.projectile(NpcCombatProjectile.id(1046));
    style.effect(NpcCombatEffect.builder().includeMiss(true).chance(75).venom(6).build());
    combat3.style(style.build());

    return Arrays.asList(combat1.build(), combat2.build(), combat3.build());
  }

  @Override
  public void spawnHook() {
    npc.setLargeVisibility();
    phaseChange();
    spawnedActions();
    initialLock = true;
    lastAttackingEntity = null;
    rotation = 0;
    phase = -1;
    attackStyle = HitStyleType.MELEE;
    spawnModels.clear();
    spawningStarted = false;
    spawnEvents.clear();
    eventDelay = 0;
  }

  @Override
  public void despawnHook() {
    if (!mapObjectEvents.isEmpty()) {
      for (var tempMapObject : mapObjectEvents) {
        if (tempMapObject.isRunning()) {
          tempMapObject.execute();
        }
      }
      mapObjectEvents.clear();
    }
    removeNpcs(snakelings);
    snakelings.clear();
  }

  @Override
  public void tickStartHook() {
    if (eventDelay > 0) {
      --eventDelay;
    }
    snakelings.removeIf(n -> !n.isVisible());
    if (isDead()
        || !npc.isVisible()
        || npc.isLocked()
        || npc.getMovement().isTeleportStateFinished()) {
      if (initialLock) {
        checkPlayerAggression();
        setAttackingDisabled(true);
      } else if (npc.getLock() == 3) {
        npc.setFaceTile(npc.getSpawn().getTile());
      }
      if (isDead() || !npc.isVisible()) {
        if (!mapObjectEvents.isEmpty()) {
          for (var tempMapObject : mapObjectEvents) {
            if (tempMapObject.isRunning()) {
              tempMapObject.execute();
            }
          }
          mapObjectEvents.clear();
        }
        spawnModels.clear();
        spawnEvents.clear();
      }
      return;
    }
    if (initialLock) {
      initialLock = false;
      setHitDelay(14);
    }
    ++phaseTick;
    if (!spawnEvents.isEmpty()) {
      var iterator3 = spawnEvents.iterator();
      while (iterator3.hasNext()) {
        var spawnEvent = iterator3.next();
        if (spawnEvent.delay-- > 0) {
          continue;
        }
        if (spawnEvent instanceof GasEvent) {
          var gasEvent = (GasEvent) spawnEvent;
          var tempMapObject2 =
              new TempMapObject(
                  30,
                  npc.getController(),
                  new MapObject(NullObjectId.NULL_11700, 10, 0, gasEvent.getTile()));
          mapObjectEvents.add(tempMapObject2);
          addEvent(tempMapObject2);
        } else if (spawnEvent instanceof SnakelingEvent && snakelings.size() < 16) {
          var snakelingEvent = (SnakelingEvent) spawnEvent;
          var npc2 =
              npc.getController()
                  .addNpc(new NpcSpawn(snakelingEvent.getTile(), NpcId.SNAKELING_90));
          npc2.getCombat().setTarget(getTarget());
          snakelings.add(npc2);
        }
        iterator3.remove();
      }
    }
    if (!phaseChange()) {
      checkAutoRetaliate();
      checkModelSpawns();
    }
  }

  @Override
  public HitStyleType attackTickHitStyleTypeHook(HitStyleType hitStyleType, Entity opponent) {
    if (getPhase().combatType == 0) {
      return HitStyleType.RANGED;
    }
    if (getPhase().combatType == 2 && PRandom.randomI(1) == 0) {
      return HitStyleType.MAGIC;
    }
    if (getPhase().combatType == 3 || getPhase().combatType == 4) {
      return attackStyle;
    }
    return hitStyleType;
  }

  @Override
  public int attackTickAnimationHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (getPhase().combatType == 1
        && (lastAttackingEntity != null && npc.getX() < lastAttackingEntity.getX()
            || getAttackingEntity() != null && npc.getX() < getAttackingEntity().getX()
            || getTarget() != null && npc.getX() < getTarget().getX())) {
      return combatStyle.getAnimation() + 1;
    }
    return combatStyle.getAnimation();
  }

  @Override
  public void attackTickEndHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (getPhase().combatType == 3 || getPhase().combatType == 4) {
      if (attackStyle == HitStyleType.RANGED) {
        attackStyle = HitStyleType.MAGIC;
      } else if (attackStyle == HitStyleType.MAGIC) {
        attackStyle = HitStyleType.RANGED;
      }
    }
  }

  @Override
  public Graphic.ProjectileSpeed projectileSpeedHook(
      NpcCombatStyle combatStyle,
      Graphic.ProjectileSpeed speed,
      Tile tile,
      HitStyleType hitStyleType,
      int minimumDistance,
      int maximumDistance) {
    speed.setClientSpeed(speed.getClientSpeed() + 10);
    speed.setClientDelay(speed.getClientDelay() - 10);
    if (combatStyle != null
        && combatStyle.getType().getHitStyleType() == HitStyleType.RANGED
        && combatStyle.getType().getSubHitStyleType() == HitStyleType.MELEE) {
      speed.setEventDelay(5);
    }
    return speed;
  }

  @Override
  public boolean canBeAttackedHook(
      Entity opponent, boolean sendMessage, HitStyleType hitStyleType) {
    return phaseTick != 0;
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (hitStyleType == HitStyleType.MELEE) {
      damage *= 0.5;
    }
    return damage;
  }

  public void setAttackingDisabled(boolean b) {
    setDisableAutoRetaliate(b);
    if (b) {
      if (getAttackingEntity() != null) {
        lastAttackingEntity = getAttackingEntity();
      }
      clear();
    } else if (npc.withinDistance(lastAttackingEntity, 20)) {
      startAttacking(lastAttackingEntity);
    }
  }

  public void checkAutoRetaliate() {
    if ((!spawningStarted || spawnModels.isEmpty()) && !isHitDelayed() && phaseTick != 0) {
      setAttackingDisabled(false);
    }
    if (spawnModels.isEmpty() && phaseTick + 1 >= getPhase().length
        || getPhase().combatType == 1 && isHitDelayed()) {
      setAttackingDisabled(true);
    }
    if (getPhase().combatType == 1 && lastAttackingEntity != null) {
      if (lastAttackingEntity.getX() >= 2262
          && lastAttackingEntity.getX() <= 2266
          && lastAttackingEntity.getY() <= 3070) {
        npc.setFaceTile(TILE_BOTTOM);
        setAttackingDisabled(true);
      } else if (lastAttackingEntity.getX() >= 2262
          && lastAttackingEntity.getX() <= 2264
          && (lastAttackingEntity.getY() == 3071 || lastAttackingEntity.getY() == 3072)) {
        npc.setFaceTile(TILE_WEST);
        setAttackingDisabled(true);
      }
      if (lastAttackingEntity.getX() >= 2270
          && lastAttackingEntity.getX() <= 2273
          && lastAttackingEntity.getY() <= 3070) {
        npc.setFaceTile(TILE_BOTTOM);
        setAttackingDisabled(true);
      } else if (lastAttackingEntity.getX() >= 2272
          && lastAttackingEntity.getX() <= 2274
          && (lastAttackingEntity.getY() == 3071 || lastAttackingEntity.getY() == 3072)) {
        npc.setFaceTile(TILE_EAST);
        setAttackingDisabled(true);
      }
    }
  }

  public void checkModelSpawns() {
    if (spawnModels.isEmpty() || !spawningStarted && eventDelay > 0) {
      return;
    }
    var iterator = spawnModels.iterator();
    while (iterator.hasNext()) {
      var spawnModel = iterator.next();
      if (spawnModel instanceof GasSpawn) {
        if (checkSpawnGas((GasSpawn) spawnModel)) {
          spawningStarted = true;
          iterator.remove();
          break;
        }
      } else {
        if (spawnModel instanceof SnakelingSpawn
            && checkSpawnSnakeling((SnakelingSpawn) spawnModel)) {
          spawningStarted = true;
          iterator.remove();
          break;
        }
      }
    }
  }

  public boolean checkSpawnGas(GasSpawn gasSpawn) {
    if (phaseTick < gasSpawn.getStart()) {
      return false;
    }
    setAttackingDisabled(true);
    if (isHitDelayed()) {
      return false;
    }
    var faceTile = GAS_TILES[gasSpawn.indices[0]];
    npc.setFaceTile(faceTile);
    var speed = getProjectileSpeed(10);
    var npc = speed.getEventDelay();
    var n2 = speed.getClientSpeed();
    var n3 = speed.getClientDelay();
    this.npc
        .getController()
        .sendMapProjectile(null, this.npc, faceTile, 1045, 43, 1, n3, n2, 16, 64);
    spawnEvents.add(new GasEvent(npc - 1, faceTile));
    var tile = GAS_TILES[gasSpawn.indices[1]];
    this.npc
        .getController()
        .sendMapProjectile(null, this.npc, tile, 1045, 43, 1, n3 + 10, n2, 16, 64);
    spawnEvents.add(new GasEvent(npc, tile));
    eventDelay = 30;
    this.npc.setAnimation(5069);
    setHitDelay(3);
    return true;
  }

  public boolean checkSpawnSnakeling(SnakelingSpawn snakelingSpawn) {
    if (phaseTick < snakelingSpawn.getStart()) {
      return false;
    }
    setAttackingDisabled(true);
    if (isHitDelayed()) {
      return false;
    }
    npc.setFaceTile(snakelingSpawn.tile);
    var speed = getProjectileSpeed(10);
    var npc = speed.getEventDelay();
    this.npc
        .getController()
        .sendMapProjectile(
            null,
            this.npc,
            snakelingSpawn.tile,
            1047,
            43,
            1,
            speed.getClientDelay(),
            speed.getClientSpeed(),
            16,
            64);
    spawnEvents.add(new SnakelingEvent(npc - 1, snakelingSpawn.tile));
    eventDelay = 30;
    this.npc.setAnimation(5069);
    setHitDelay(3);
    return true;
  }

  public boolean phaseChange() {
    var b = !spawnModels.isEmpty();
    if (phase != -1
        && (b || phaseTick < getPhase().length || getPhase().waitForHitDelay && isHitDelayed())) {
      return false;
    }
    ++phase;
    if (phase == PHASES[rotation].length) {
      phase = 0;
    }
    setAttackingDisabled(true);
    phaseTick = 0;
    spawnModels.clear();
    spawningStarted = false;
    if (!initialLock && (npc.getId() != getNpcId() || !npc.matchesTile(getPhase().tile))) {
      spawnedActions();
      npc.getMovement().animatedTeleport(getPhase().tile, 5072, 5073, null, null, 2);
      addEvent(
          new PEvent(1) {
            @Override
            public void execute() {
              stop();
              npc.setId(getNpcId());
            }
          });
    } else {
      npc.setId(getNpcId());
    }
    if (getPhase().combatType == 3) {
      attackStyle = HitStyleType.RANGED;
    } else if (getPhase().combatType == 4) {
      attackStyle = HitStyleType.MAGIC;
    }
    if (getPhase().spawnModels != null && !getPhase().spawnModels.isEmpty()) {
      spawnModels.addAll(getPhase().spawnModels);
    }
    return true;
  }

  public int getNpcId() {
    if (getPhase().combatType == 1) {
      return NpcId.ZULRAH_725_2043;
    } else if (getPhase().combatType == 2) {
      return NpcId.ZULRAH_725_2044;
    }
    return NpcId.ZULRAH_725;
  }

  public Phase getPhase() {
    return phase == -1 ? null : PHASES[rotation][phase];
  }

  public void spawnedActions() {
    npc.setAnimation(5071);
    npc.setLock(6);
  }

  private static class Phase {

    private long length;
    private boolean waitForHitDelay;
    private Tile tile;
    private int combatType;
    private List<SpawnModel> spawnModels;

    private Phase(Tile tile, int npc) {
      this(14L, tile, npc, (SpawnModel[]) null);
      waitForHitDelay = true;
    }

    private Phase(long npc, Tile tile, int n2) {
      this(npc, tile, n2, (SpawnModel[]) null);
    }

    private Phase(Tile tile, int npc, SpawnModel... array) {
      this(14L, tile, npc, array);
      waitForHitDelay = true;
    }

    private Phase(long length, Tile tile, int combatType, SpawnModel... array) {
      this.length = length;
      this.tile = new Tile(tile);
      this.combatType = combatType;
      if (array != null && array.length > 0 && array[0] != null) {
        spawnModels = Arrays.asList(array);
      }
    }
  }

  @AllArgsConstructor
  @Getter
  private static class SpawnModel {

    private int start;
  }

  private static class GasSpawn extends SpawnModel {

    private int[] indices;

    private GasSpawn(int... array) {
      this(0L, array);
    }

    private GasSpawn(long npc, int... indices) {
      super((int) npc);
      this.indices = indices;
    }
  }

  private static class SnakelingSpawn extends SpawnModel {

    private Tile tile;

    private SnakelingSpawn(Tile tile) {
      this(0L, tile);
    }

    private SnakelingSpawn(long npc, Tile tile) {
      super((int) npc);
      this.tile = new Tile(tile);
    }
  }

  @Getter
  private static class SpawnEvent {

    private int delay;
    private Tile tile;

    private SpawnEvent(int delay, Tile tile) {
      this.delay = delay;
      this.tile = new Tile(tile);
    }
  }

  private static class GasEvent extends SpawnEvent {

    private GasEvent(int delay, Tile tile) {
      super(delay, tile);
    }
  }

  private static class SnakelingEvent extends SpawnEvent {

    private SnakelingEvent(int delay, Tile tile) {
      super(delay, tile);
    }
  }
}
