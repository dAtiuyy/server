package com.palidinodh.playerplugin.clanwars;

import com.google.inject.Inject;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.controller.PController;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;

public class ClanWarsFreeForAllController extends PController {

  @Inject private Player player;

  @Override
  public void startHook() {
    setItemStorageDisabled(true);
    setTeleportsDisabled(true);
    setKeepItemsOnDeath(true);
    setAreaLocked("ClanWarsFreeForAll");
    player.getPrayer().setAllowAllPrayers(true);
    player.getGameEncoder().sendPlayerOption("Attack", 2, false);
    if (player.getInventory().isEmpty() && player.getEquipment().isEmpty()) {
      player.getMovement().teleport(new Tile(3327, 4752));
    }
  }

  @Override
  public void stopHook() {
    player.getInventory().clear();
    player.getEquipment().clear();
    player.getEquipment().weaponUpdate(true);
    player.getPrayer().setAllowAllPrayers(false);
    player.getGameEncoder().sendPlayerOption("null", 2, false);
    player.restore();
    if (Area.getArea(getFirstTile()).is("ClanWarsFreeForAll")) {
      player.getMovement().teleport(player.getWidgetManager().getHomeTile());
    } else {
      player.getMovement().teleport(getFirstTile());
    }
  }

  @Override
  public void applyDeadHook() {
    setRespawnTile(new Tile(player.getX(), 4758));
    player.getCombat().setSpecialAttackAmount(PCombat.MAX_SPECIAL_ATTACK);
  }

  @Override
  public MapItem addMapItemHook(MapItem mapItem) {
    mapItem.setNeverAppear();
    return mapItem;
  }

  @Override
  public boolean canAttackPlayer(Player opponent, boolean sendMessage, HitStyleType hitStyleType) {
    if (player.getY() < 4760 || opponent.getY() < 4760) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You need to move north to attack.");
      }
      return false;
    }
    return true;
  }
}
