package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.List;

@ReferenceId({
  ItemId.BIRD_NEST,
  ItemId.BIRD_NEST_5071,
  ItemId.BIRD_NEST_5072,
  ItemId.BIRD_NEST_5073,
  ItemId.BIRD_NEST_5074,
  ItemId.BIRD_NEST_13653
})
class BirdNestItem implements ItemHandler {

  private static final NpcCombatDrop TREE_SEED_NEST;
  private static final NpcCombatDrop WYSON_SEED_NEST;
  private static final List<RandomItem> RING_NEST =
      RandomItem.buildList(
          new RandomItem(ItemId.SAPPHIRE_RING),
          new RandomItem(ItemId.GOLD_RING),
          new RandomItem(ItemId.EMERALD_RING),
          new RandomItem(ItemId.RUBY_RING),
          new RandomItem(ItemId.DIAMOND_RING));

  static {
    var drop = NpcCombatDrop.builder();
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGONFRUIT_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TEAK_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAHOGANY_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CELASTRUS_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.REDWOOD_TREE_SEED)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PAPAYA_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YEW_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PALM_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CALQUAT_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SPIRIT_SEED)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ACORN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.APPLE_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WILLOW_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BANANA_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ORANGE_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CURRY_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAPLE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PINEAPPLE_SEED)));
    drop.table(dropTable.build());
    TREE_SEED_NEST = drop.build();

    drop = NpcCombatDrop.builder();
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAPLE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORSTOL_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RANARR_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPDRAGON_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YEW_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SPIRIT_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PALM_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGONFRUIT_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CELASTRUS_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.REDWOOD_TREE_SEED)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CADANTINE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TEAK_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAHOGANY_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WILLOW_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PINEAPPLE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CALQUAT_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PAPAYA_TREE_SEED)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SWEETCORN_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STRAWBERRY_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ACORN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LIMPWURT_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATERMELON_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPE_GRASS_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LANTADYME_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DWARF_WEED_SEED)));
    drop.table(dropTable.build());
    WYSON_SEED_NEST = drop.build();
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.replace(new Item(ItemId.BIRD_NEST_5075));
    switch (item.getId()) {
      case ItemId.BIRD_NEST:
        player.getInventory().addOrDropItem(ItemId.BIRDS_EGG);
        player
            .getGameEncoder()
            .sendMessage(
                "<col=ff0000>The skilling seller might be interested in this birds egg.</col>");
        break;
      case ItemId.BIRD_NEST_5071:
        player.getInventory().addOrDropItem(ItemId.BIRDS_EGG_5078);
        player
            .getGameEncoder()
            .sendMessage(
                "<col=ff0000>The skilling seller might be interested in this birds egg.</col>");
        break;
      case ItemId.BIRD_NEST_5072:
        player.getInventory().addOrDropItem(ItemId.BIRDS_EGG_5077);
        player
            .getGameEncoder()
            .sendMessage(
                "<col=ff0000>The skilling seller might be interested in this birds egg.</col>");
        break;
      case ItemId.BIRD_NEST_5073:
        TREE_SEED_NEST.getItems(player).forEach(i -> player.getInventory().addOrDropItem(i));
        break;
      case ItemId.BIRD_NEST_5074:
        player.getInventory().addOrDropItem(RandomItem.getItem(RING_NEST));
        break;
      case ItemId.BIRD_NEST_13653:
        WYSON_SEED_NEST.getItems(player).forEach(i -> player.getInventory().addOrDropItem(i));
        break;
    }
  }
}
