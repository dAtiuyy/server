package com.palidinodh.playerplugin.magic.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.NIGHTMARE_STAFF,
  ItemId.HARMONISED_NIGHTMARE_STAFF,
  ItemId.VOLATILE_NIGHTMARE_STAFF,
  ItemId.ELDRITCH_NIGHTMARE_STAFF
})
class NightmareStaffItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "dismantle":
        {
          if (player.getInventory().isFull()) {
            player.getInventory().notEnoughSpace();
            break;
          }
          item.replace(new Item(ItemId.NIGHTMARE_STAFF));
          switch (item.getId()) {
            case ItemId.HARMONISED_NIGHTMARE_STAFF:
              player.getInventory().addItem(ItemId.HARMONISED_ORB);
              break;
            case ItemId.VOLATILE_NIGHTMARE_STAFF:
              player.getInventory().addItem(ItemId.VOLATILE_ORB);
              break;
            case ItemId.ELDRITCH_NIGHTMARE_STAFF:
              player.getInventory().addItem(ItemId.ELDRITCH_ORB);
              break;
          }
          break;
        }
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var onSlot = onItem.getSlot();
    if (ItemHandler.used(useItem, onItem, ItemId.NIGHTMARE_STAFF, ItemId.VOLATILE_ORB)) {
      player.getInventory().deleteItem(ItemId.NIGHTMARE_STAFF);
      player.getInventory().deleteItem(ItemId.VOLATILE_ORB);
      player.getInventory().addItem(ItemId.VOLATILE_NIGHTMARE_STAFF, 1, onSlot);
      return true;
    } else if (ItemHandler.used(useItem, onItem, ItemId.NIGHTMARE_STAFF, ItemId.ELDRITCH_ORB)) {
      player.getInventory().deleteItem(ItemId.NIGHTMARE_STAFF);
      player.getInventory().deleteItem(ItemId.ELDRITCH_ORB);
      player.getInventory().addItem(ItemId.ELDRITCH_NIGHTMARE_STAFF, 1, onSlot);
      return true;
    } else if (ItemHandler.used(useItem, onItem, ItemId.NIGHTMARE_STAFF, ItemId.HARMONISED_ORB)) {
      player.getInventory().deleteItem(ItemId.NIGHTMARE_STAFF);
      player.getInventory().deleteItem(ItemId.HARMONISED_ORB);
      player.getInventory().addItem(ItemId.HARMONISED_NIGHTMARE_STAFF, 1, onSlot);
      return true;
    }
    return false;
  }
}
