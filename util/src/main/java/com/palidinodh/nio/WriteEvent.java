package com.palidinodh.nio;

import java.nio.ByteBuffer;

public class WriteEvent {

  private ByteBuffer buffer;
  private WriteEventHandler handler;

  public WriteEvent(byte[] bytes, int offset, int length, WriteEventHandler handler) {
    buffer = ByteBuffer.allocate(length);
    buffer.put(bytes, offset, length);
    buffer.flip();
    buffer = buffer.asReadOnlyBuffer();
    this.handler = handler;
  }

  ByteBuffer getBuffer() {
    return buffer;
  }

  WriteEventHandler getHandler() {
    return handler;
  }
}
