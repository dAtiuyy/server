package com.palidinodh.maparea.zeah.darkaltar.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.DARK_ALTAR)
class DarkAltarMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.carryingItem(ItemId.DENSE_ESSENCE_BLOCK)) {
      var blockCount = player.getInventory().getCount(ItemId.DENSE_ESSENCE_BLOCK);
      player.getInventory().deleteItem(ItemId.DENSE_ESSENCE_BLOCK, blockCount);
      player.getInventory().addOrDropItem(ItemId.DARK_ESSENCE_BLOCK, blockCount);
      double xp = blockCount * 3;
      if (player.getEquipment().wearingElidinisOutfit()) {
        xp *= 1.1;
      }
      player.getSkills().addXp(Skills.RUNECRAFTING, (int) xp);
      player.getPrayer().changePoints(-blockCount);
    } else {
      player.getGameEncoder().sendMessage("You are not carrying any Dense essence blocks.");
    }
  }
}
