package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerMaster;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.KRYSTILIA)
class KrystiliaNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    if (option.getIndex() == 0) {
      player.openOptionsDialogue(
          new DialogueOption(
              "Get task",
              (c, s) -> {
                plugin.getChooseAssignment().choose(SlayerMaster.WILDERNESS_MASTER);
              }),
          new DialogueOption(
              "Current task",
              (c, s) -> {
                plugin.sendTask(plugin.getWildernessTask());
              }),
          new DialogueOption(
              "Cancel task (30 points)",
              (c, s) -> {
                plugin.getRewards().cancelWildernessTask();
              }));
    } else if (option.getIndex() == 2) {
      plugin.getChooseAssignment().choose(SlayerMaster.WILDERNESS_MASTER);
    } else if (option.getIndex() == 3) {
      player.openShop("slayer");
    } else if (option.getIndex() == 4) {
      plugin.getRewards().open();
    }
  }
}
