package com.palidinodh.maparea.morytania;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({13620, 13621, 13877})
public class MortMyreSwamp extends Area {}
