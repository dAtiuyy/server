package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.graphic.Graphic;
import java.util.Arrays;
import java.util.List;

class TheNightmareHuskCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var magicCombat = NpcCombatDefinition.builder();
    magicCombat.id(NpcId.HUSK_48);
    magicCombat.spawn(NpcCombatSpawn.builder().animation(8567).lock(2).deathDelay(2).build());
    magicCombat.hitpoints(NpcCombatHitpoints.total(20));
    magicCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(80)
            .rangedLevel(80)
            .defenceLevel(20)
            .bonus(BonusType.MELEE_DEFENCE, 20)
            .bonus(BonusType.DEFENCE_MAGIC, 80)
            .bonus(BonusType.DEFENCE_RANGED, 80)
            .build());
    magicCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    magicCombat.deathAnimation(8566);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(14));
    style.animation(8565).attackSpeed(4).targetGraphic(new Graphic(1777, 124));
    style.projectile(NpcCombatProjectile.id(1776));
    magicCombat.style(style.build());

    var rangedCombat = NpcCombatDefinition.builder();
    rangedCombat.id(NpcId.HUSK_48_9455);
    rangedCombat.spawn(NpcCombatSpawn.builder().animation(8567).lock(2).deathDelay(2).build());
    rangedCombat.hitpoints(NpcCombatHitpoints.total(20));
    rangedCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(80)
            .rangedLevel(80)
            .defenceLevel(20)
            .bonus(BonusType.MELEE_DEFENCE, 20)
            .bonus(BonusType.DEFENCE_MAGIC, 80)
            .bonus(BonusType.DEFENCE_RANGED, 80)
            .build());
    rangedCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    rangedCombat.deathAnimation(8566);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(14));
    style.animation(8564).attackSpeed(4).targetGraphic(new Graphic(1772, 124));
    style.projectile(NpcCombatProjectile.id(1771));
    rangedCombat.style(style.build());

    return Arrays.asList(magicCombat.build(), rangedCombat.build());
  }

  @Override
  public void despawnHook() {
    var target = getTarget();
    if (target == null) {
      return;
    }
    if (target.isLocked()) {
      return;
    }
    var npcs = target.getController().getTargetNpcs(NpcId.HUSK_48, NpcId.HUSK_48_9455);
    for (var npc : npcs) {
      if (!npc.isLocked()) {
        return;
      }
    }
    target.getController().clearMagicBind();
    if (target.isPlayer()) {
      target
          .asPlayer()
          .getGameEncoder()
          .sendMessage(
              "<col=005500>As the last husk dies, you feel yourself become free of the Nightmare's trance.</col>");
    }
  }

  @Override
  public void tickStartHook() {
    if (!npc.withinDistance(getTarget(), 32)) {
      startDeath();
    }
  }

  @Override
  public void setTargetHook(Entity target) {
    if (target.getController().canMagicBind()) {
      target.getMovement().clearMagicBind();
      target.getController().setMagicBind(Integer.MAX_VALUE);
      if (target.isPlayer()) {
        target
            .asPlayer()
            .getGameEncoder()
            .sendMessage(
                "<col=ff0000>The Nightmare puts you in a strange trance, preventing you from moving!</col>");
      }
    }
  }
}
