package com.palidinodh.incomingpacket;

import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler.Beta;
import com.palidinodh.osrscore.io.incomingpacket.InStreamKey;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.Scroll;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.UserRank;
import java.util.ArrayList;

class CommandDecoder extends IncomingPacketDecoder {

  public static boolean canUse(CommandHandler command, Player player) {
    if (!command.canUse(player)) {
      return false;
    }
    if (Settings.getInstance().isLocal()) {
      return true;
    }
    if (command instanceof CommandHandler.Teleport) {
      if (!Main.isOwnerIp(player.getIP()) && !player.getController().canTeleport(true)) {
        return false;
      }
    }
    if (command instanceof CommandHandler.OverseerRank) {
      if (!player.isUsergroup(UserRank.OVERSEER)
          && !player.isUsergroup(UserRank.FORUM_MODERATOR)
          && !player.isUsergroup(UserRank.MODERATOR)
          && !player.isUsergroup(UserRank.SENIOR_MODERATOR)
          && !player.isUsergroup(UserRank.ADVERTISEMENT_MANAGER)
          && !player.isUsergroup(UserRank.COMMUNITY_MANAGER)
          && !player.isUsergroup(UserRank.ADMINISTRATOR)) {
        return false;
      }
    }
    if (command instanceof CommandHandler.ModeratorRank) {
      if (!player.isUsergroup(UserRank.MODERATOR)
          && !player.isUsergroup(UserRank.SENIOR_MODERATOR)
          && !player.isUsergroup(UserRank.ADVERTISEMENT_MANAGER)
          && !player.isUsergroup(UserRank.COMMUNITY_MANAGER)
          && !player.isUsergroup(UserRank.ADMINISTRATOR)) {
        return false;
      }
    }
    if (command instanceof CommandHandler.SeniorModeratorRank) {
      if (!player.isUsergroup(UserRank.SENIOR_MODERATOR)
          && !player.isUsergroup(UserRank.ADVERTISEMENT_MANAGER)
          && !player.isUsergroup(UserRank.COMMUNITY_MANAGER)
          && !player.isUsergroup(UserRank.ADMINISTRATOR)) {
        return false;
      }
    }
    if (command instanceof CommandHandler.AdministratorRank) {
      if (!player.isUsergroup(UserRank.ADMINISTRATOR)) {
        return false;
      }
      if (!Main.adminPrivledges(player)) {
        return false;
      }
    }
    if (command instanceof Beta) {
      return Settings.getInstance().isBeta()
          || player.isUsergroup(UserRank.ADMINISTRATOR) && Main.adminPrivledges(player);
    }
    return true;
  }

  @Override
  public boolean execute(Player player, Stream stream) {
    var name = getString(InStreamKey.STRING_INPUT);
    player.clearIdleTime();
    if (player.getDialogue() != null && player.getDialogue().isDisableActions()) {
      return false;
    }
    if (name.equals("commands")) {
      var examples = new ArrayList<String>();
      for (var entry : CommandHandler.COMMAND_HANDLERS.entrySet()) {
        var command = entry.getValue();
        if (command instanceof CommandHandler.Beta) {
          continue;
        }
        if (!canUse(command, player)) {
          continue;
        }
        var rank = "";
        if (command instanceof CommandHandler.OverseerRank) {
          rank += "[Overseer]";
        }
        if (command instanceof CommandHandler.ModeratorRank) {
          rank += "[Moderator]";
        }
        if (command instanceof CommandHandler.SeniorModeratorRank) {
          rank += "[Senior Moderator]";
        }
        if (command instanceof CommandHandler.AdministratorRank) {
          rank += "[Administrator]";
        }
        examples.add(rank + " " + getExample(entry.getKey(), command));
      }
      Scroll.open(player, "Commands", examples);
      return true;
    }
    var message = "";
    if (name.contains(" ")) {
      var indexOfSpace = name.indexOf(' ');
      message = name.substring(indexOfSpace + 1);
      name = name.substring(0, indexOfSpace);
    }
    name = name.toLowerCase();
    player.log(PlayerLogType.COMMAND, "::" + name + " " + message);
    player.sendDiscordNewAccountLog("Command: " + name + " " + message);
    var command = CommandHandler.getHandler(name);
    if (command == null) {
      player.getGameEncoder().sendMessage("Command not found.");
      return false;
    }
    if (!canUse(command, player)) {
      player.getGameEncoder().sendMessage("You don't have permission to use this command.");
      return false;
    }
    try {
      command.execute(player, name, message);
    } catch (Exception e) {
      player.getGameEncoder().sendMessage(getExample(name, command));
      if (Settings.getInstance().isLocal()) {
        e.printStackTrace();
      }
    }
    return true;
  }

  public String getExample(String name, CommandHandler command) {
    return "::" + name.toLowerCase() + " " + command.getExample(name);
  }
}
