package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.LARUPIA_OUTFIT_HUNTER_32296)
class LarupiaOutfitHunterItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getInventory().getRemainingSlots() < 3 - 1) {
      player.getInventory().notEnoughSpace();
      return;
    }
    item.remove();
    player.getInventory().addItem(ItemId.LARUPIA_HAT);
    player.getInventory().addItem(ItemId.LARUPIA_TOP);
    player.getInventory().addItem(ItemId.LARUPIA_LEGS);
  }
}
