package com.palidinodh.maparea.misthalin.varrock.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.REDBERRY_BUSH_23628)
public class RedberryBushMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    player.setAnimation(2292);
    player.getInventory().addItem(ItemId.REDBERRIES);
  }
}
