package com.palidinodh.playerplugin.slayer;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.entity.player.slayer.AssignedSlayerTask;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerMaster;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerTaskIdentifier;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.CompetitiveHiscoresCategoryType;
import com.palidinodh.osrscore.world.event.wellofgoodwill.WellOfGoodwillBoostType;
import com.palidinodh.osrscore.world.event.wellofgoodwill.WellOfGoodwillEvent;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.playerplugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.playerplugin.bountyhunter.MysteriousEmblem;
import com.palidinodh.random.PRandom;
import lombok.AllArgsConstructor;

@AllArgsConstructor
class SlayerKill {

  private transient Player player;
  private transient SlayerPlugin plugin;

  private static int getRewardPointsMultiplier(int tasksCompleted) {
    if (tasksCompleted % 1000 == 0) {
      return 50;
    }
    if (tasksCompleted % 250 == 0) {
      return 35;
    }
    if (tasksCompleted % 100 == 0) {
      return 25;
    }
    if (tasksCompleted % 50 == 0) {
      return 15;
    }
    if (tasksCompleted % 10 == 0) {
      return 5;
    }
    return 1;
  }

  public void check(AssignedSlayerTask assignedTask, Npc npc) {
    if (assignedTask == plugin.getWildernessTask() && player.getSkills().getCombatLevel() < 75) {
      return;
    }
    if (!countsTowardTaskQuantity(assignedTask, npc.getId())) {
      return;
    }
    var isSuperior = npc.getCombatDef().getKillCountName().equals("Superior Slayer Creature");
    var isTask =
        !assignedTask.isComplete()
            && plugin.isTask(assignedTask, npc.getId(), npc.getDef().getLowerCaseName());
    var well = player.getWorld().getWorldEvent(WellOfGoodwillEvent.class);
    if (isSuperior || isTask) {
      var experience = npc.getCombat().getMaxHitpoints();
      if (npc.getCombatDef().getSlayer().getExperience() > 0) {
        experience = npc.getCombatDef().getSlayer().getExperience();
      }
      if (well.isBoost(WellOfGoodwillBoostType.SLAYER)) {
        experience *= WellOfGoodwillBoostType.SLAYER.getMultiplier();
      }
      player.getSkills().addXp(Skills.SLAYER, experience);
    }
    if (!isTask) {
      return;
    }
    var superiorDenom = 100;
    if (player.getPlugin(BondPlugin.class).isRelicUnlocked(BondRelicType.SLAYER_APPEAR_SUPERIOR)) {
      superiorDenom = 50;
    }
    if (npc.getCombatDef().getSlayer().getSuperiorId() != -1
        && plugin.getRewards().isUnlocked(SlayerUnlock.BIGGER_BADDER)
        && PRandom.randomE(superiorDenom) == 0) {
      player.getGameEncoder().sendMessage("<col=ff0000>A superior foe has appeared...");
      var superiorNpc =
          player
              .getController()
              .addNpc(new NpcSpawn(npc, npc.getCombatDef().getSlayer().getSuperiorId()));
      superiorNpc.getCombat().setTarget(player);
    }
    int brimstoneKeyChance;
    if (npc.getDef().getCombatLevel() >= 100) {
      brimstoneKeyChance = (int) (-0.2 * Math.min(npc.getDef().getCombatLevel(), 350) + 120);
    } else {
      brimstoneKeyChance =
          (int) (0.2 * StrictMath.pow(npc.getDef().getCombatLevel() - 100, 2) + 100);
    }
    if (player.getPlugin(BondPlugin.class).isRelicUnlocked(BondRelicType.SLAYER_KEYS)) {
      brimstoneKeyChance /= 1.25;
    }
    if (brimstoneKeyChance > 0
        && assignedTask == plugin.getTask()
        && PRandom.inRange(1, brimstoneKeyChance)) {
      Tile tile = npc;
      if (npc.getCombatDef().getDrop().isLocationTypeUnderOpponent()) {
        tile = player;
      }
      if ((player.canDonatorPickupItem(ItemId.BRIMSTONE_KEY))
          && player.getInventory().canAddItem(ItemId.BRIMSTONE_KEY)) {
        player.getInventory().addItem(ItemId.BRIMSTONE_KEY);
      } else {
        player.getController().addMapItem(new Item(ItemId.BRIMSTONE_KEY), tile, player);
      }
    }
    int larransKeyChance;
    if (npc.getDef().getCombatLevel() >= 81) {
      larransKeyChance = (int) (-0.18 * Math.min(npc.getDef().getCombatLevel(), 350) + 113);
    } else {
      larransKeyChance = (int) (0.3 * StrictMath.pow(80 - npc.getDef().getCombatLevel(), 2) + 100);
    }
    if (npc.getCombatDef().getSlayer().getLevel() > 1) {
      larransKeyChance /= 1.25;
    }
    if (player.getPlugin(BondPlugin.class).isRelicUnlocked(BondRelicType.SLAYER_KEYS)) {
      larransKeyChance /= 1.25;
    }
    if (larransKeyChance > 0
        && assignedTask == plugin.getWildernessTask()
        && PRandom.inRange(1, larransKeyChance)) {
      Tile tile = npc;
      if (npc.getCombatDef().getDrop().isLocationTypeUnderOpponent()) {
        tile = player;
      }
      if ((player.canDonatorPickupItem(ItemId.LARRANS_KEY))
          && player.getInventory().canAddItem(ItemId.LARRANS_KEY)) {
        player.getInventory().addItem(ItemId.LARRANS_KEY);
      } else {
        player.getController().addMapItem(new Item(ItemId.LARRANS_KEY), tile, player);
      }
    }
    if (assignedTask == plugin.getWildernessTask()) {
      if (assignedTask.getQuantity() == assignedTask.getTotal()) {
        plugin.setWildernessTaskEmblemId(player.getPlugin(BountyHunterPlugin.class).getEmblemId());
      } else if (plugin.getWildernessTaskEmblemId() > -1
          && !player.getInventory().hasItem(plugin.getWildernessTaskEmblemId())) {
        player
            .getGameEncoder()
            .sendMessage("Your task is no longer eligible to upgrade an emblem.");
        player
            .getGameEncoder()
            .sendMessage(
                "You're missing an "
                    + ItemDefinition.getName(plugin.getWildernessTaskEmblemId())
                    + "!");
        plugin.setWildernessTaskEmblemId(-1);
      }
    }
    if (!plugin.deincrimentBraceletOfSlaughter()) {
      assignedTask.decreaseQuantity();
    }
    if (!assignedTask.isComplete() && plugin.deincrimentExpeditiousBracelet()) {
      assignedTask.decreaseQuantity();
    }
    if (assignedTask.isComplete()) {
      complete(assignedTask, npc);
    }
  }

  private void complete(AssignedSlayerTask assignedTask, Npc npc) {
    var tasksCompleted = 0;
    var masterName = assignedTask.getMaster();
    if (assignedTask == plugin.getTask()) {
      plugin.setTotalTasks(plugin.getTotalTasks() + 1);
      if (!masterName.equals(SlayerMaster.RESET_MASTER)) {
        plugin.setConsecutiveTasks(plugin.getConsecutiveTasks() + 1);
        tasksCompleted = plugin.getConsecutiveTasks();
        if (Main.getHolidayToken() != -1) {
          for (var i = 1; i < SlayerMaster.STANDARD_MASTERS.length; i++) {
            if (!masterName.equals(SlayerMaster.STANDARD_MASTERS[i])) {
              continue;
            }
            player.getInventory().addOrDropItem(Main.getHolidayToken(), i * 10);
            break;
          }
        }
      }
    } else if (assignedTask == plugin.getWildernessTask()) {
      plugin.setTotalTasks(plugin.getTotalTasks() + 1);
      plugin.setConsecutiveWildernessTasks(plugin.getConsecutiveWildernessTasks() + 1);
      tasksCompleted = plugin.getConsecutiveWildernessTasks();
      if (Main.getHolidayToken() != -1) {
        player.getInventory().addOrDropItem(Main.getHolidayToken(), 50);
      }
    }
    var slayerMaster = SlayerMaster.get(masterName);
    var slayerTask = assignedTask.getSlayerTask();
    var rewardPoints = slayerMaster.getPoints();
    if (rewardPoints > 0) {
      var well = player.getWorld().getWorldEvent(WellOfGoodwillEvent.class);
      if (well.isBoost(WellOfGoodwillBoostType.SLAYER)) {
        rewardPoints *= WellOfGoodwillBoostType.SLAYER.getMultiplier();
      }
      if (player.getPlugin(BondPlugin.class).isRelicUnlocked(BondRelicType.SLAYER_FREE_POINTS)) {
        rewardPoints += Math.max(rewardPoints * 0.1, 1);
      }
      if (player.getPlugin(BondPlugin.class).isRelicUnlocked(BondRelicType.SLAYER_MORE_POINTS)) {
        rewardPoints += Math.max(rewardPoints * 0.25, 1);
      }
    }
    if (assignedTask == plugin.getWildernessTask()) {
      if (plugin.getWildernessTaskEmblemId() > -1
          && player.getInventory().hasItem(plugin.getWildernessTaskEmblemId())) {
        var nextEmblemId = MysteriousEmblem.getNextId(plugin.getWildernessTaskEmblemId());
        if (nextEmblemId != -1) {
          player.getInventory().deleteItem(plugin.getWildernessTaskEmblemId());
          player.getInventory().addItem(nextEmblemId);
        }
      }
      if (PRandom.randomE(20) == 0) {
        Tile tile = npc;
        if (npc.getCombatDef().getDrop().isLocationTypeUnderOpponent()) {
          tile = player;
        }
        player.getController().addMapItem(new Item(ItemId.BLOODY_KEY), tile, player);
        player
            .getWorld()
            .sendMessage(
                "<col=ff0000>A bloody key has been dropped near "
                    + assignedTask.getName()
                    + "s at Level "
                    + Area.getWildernessLevel(tile)
                    + " for "
                    + player.getUsername()
                    + "!");
      }
    }
    rewardPoints *= getRewardPointsMultiplier(tasksCompleted);
    plugin.setPoints(plugin.getPoints() + rewardPoints);
    if (rewardPoints > 0) {
      player
          .getGameEncoder()
          .sendMessage(
              "<col=8F4808>You've completed "
                  + tasksCompleted
                  + " tasks in a row and received "
                  + rewardPoints
                  + " points; return to a Slayer master.");
    } else {
      player
          .getGameEncoder()
          .sendMessage(
              "<col=8F4808>You've completed "
                  + tasksCompleted
                  + " tasks; return to a Slayer master.");
    }
    Diary.getDiaries(player)
        .forEach(d -> d.slayerAssignmentComplete(player, slayerMaster, slayerTask));
    if (assignedTask == plugin.getTask()) {
      if (!masterName.equalsIgnoreCase(SlayerMaster.RESET_MASTER)) {
        player
            .getSkills()
            .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.SLAYER_TASKS, 1);
      }
      if (masterName.equalsIgnoreCase("Nieve") || masterName.equalsIgnoreCase("Duradel")) {
        player
            .getSkills()
            .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.HIGH_LEVEL_SLAYER_TASKS, 1);
      }
    } else if (assignedTask == plugin.getWildernessTask()) {
      player
          .getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.WILD_SLAYER_TASKS, 1);
    }
    if (assignedTask.isBoss()) {
      player
          .getSkills()
          .competitiveHiscoresUpdate(CompetitiveHiscoresCategoryType.BOSS_SLAYER_TASKS, 1);
    }
    player.sendDiscordNewAccountLog("Slayer Task: Completed");
  }

  private boolean countsTowardTaskQuantity(AssignedSlayerTask assignedTask, int id) {
    if (id == NpcId.RESPIRATORY_SYSTEM) {
      return false;
    }
    if (assignedTask.getIdentifier() == SlayerTaskIdentifier.TZTOK_JAD
        && id != NpcId.TZTOK_JAD_702) {
      return false;
    }
    if ((id == NpcId.DAWN_228 || id == NpcId.DAWN_228_7885)
        && !plugin.getRewards().isUnlocked(SlayerUnlock.DOUBLE_TROUBLE)) {
      return false;
    }
    if (assignedTask.isBoss()) {
      switch (id) {
        case NpcId.WINGMAN_SKREE_143:
        case NpcId.FLOCKLEADER_GEERIN_149:
        case NpcId.FLIGHT_KILISA_159:
        case NpcId.STARLIGHT_149:
        case NpcId.GROWLER_139:
        case NpcId.BREE_146:
        case NpcId.SERGEANT_STRONGSTACK_141:
        case NpcId.SERGEANT_STEELWILL_142:
        case NpcId.SERGEANT_GRIMSPIKE_142:
        case NpcId.TSTANON_KARLAK_145:
        case NpcId.ZAKLN_GRITCH_142:
        case NpcId.BALFRUG_KREEYATH_151:
        case NpcId.GREAT_OLM_RIGHT_CLAW:
        case NpcId.GREAT_OLM_LEFT_CLAW:
        case NpcId.GREAT_OLM_RIGHT_CLAW_549:
        case NpcId.GREAT_OLM_LEFT_CLAW_750:
        case NpcId.SCORPIAS_OFFSPRING_15:
        case NpcId.TOTEM:
        case NpcId.TOTEM_9435:
        case NpcId.TOTEM_9436:
        case NpcId.TOTEM_9437:
        case NpcId.TOTEM_9438:
        case NpcId.TOTEM_9439:
        case NpcId.TOTEM_9440:
        case NpcId.TOTEM_9441:
        case NpcId.TOTEM_9442:
        case NpcId.TOTEM_9443:
        case NpcId.TOTEM_9444:
        case NpcId.TOTEM_9445:
        case NpcId.SLEEPWALKER_3:
        case NpcId.SLEEPWALKER_3_9447:
        case NpcId.SLEEPWALKER_3_9448:
        case NpcId.SLEEPWALKER_3_9449:
        case NpcId.SLEEPWALKER_3_9450:
        case NpcId.SLEEPWALKER_3_9451:
        case NpcId.PARASITE_86:
        case NpcId.PARASITE_57:
        case NpcId.HUSK_48:
        case NpcId.HUSK_48_9455:
          return false;
      }
    }
    return true;
  }
}
