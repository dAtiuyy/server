package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.DRAGON_SPEAR,
  ItemId.DRAGON_SPEAR_P,
  ItemId.DRAGON_SPEAR_KP,
  ItemId.DRAGON_SPEAR_P_PLUS,
  ItemId.DRAGON_SPEAR_P_PLUS_PLUS,
  ItemId.ZAMORAKIAN_SPEAR,
  ItemId.ZAMORAKIAN_HASTA
})
class DragonSpearSpecialAttack extends SpecialAttack {

  DragonSpearSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(25);
    entry.animation(1064);
    entry.castGraphic(new Graphic(253, 100));
    entry.impactGraphic(new Graphic(80, 100));
    addEntry(entry);
  }

  @Override
  public boolean canUseHook(Player player, Entry entry, Entity opponent) {
    if (!opponent.isPlayer()) {
      return false;
    }
    if (opponent.getSizeX() != 1 || opponent.getSizeY() != 1) {
      return false;
    }
    if (opponent.isLocked()) {
      return false;
    }
    if (opponent.getMovement().getForceRunning() != null) {
      return false;
    }
    return !opponent.isPlayer() || opponent.asPlayer().getCombat().getStunImmunity() <= 0;
  }
}
