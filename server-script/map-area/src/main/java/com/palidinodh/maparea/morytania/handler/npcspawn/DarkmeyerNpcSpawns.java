package com.palidinodh.maparea.morytania.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class DarkmeyerNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(3603, 3369), NpcId.BANKER_9719));
    spawns.add(new NpcSpawn(new Tile(3604, 3369), NpcId.BANKER_9719));
    spawns.add(new NpcSpawn(new Tile(3605, 3369), NpcId.BANKER_9719));
    spawns.add(new NpcSpawn(new Tile(3606, 3369), NpcId.BANKER_9718));
    spawns.add(new NpcSpawn(new Tile(3607, 3369), NpcId.BANKER_9719));
    spawns.add(new NpcSpawn(4, new Tile(3612, 3362), NpcId.VYREWATCH_SENTINEL_151));
    spawns.add(new NpcSpawn(4, new Tile(3599, 3361), NpcId.VYREWATCH_SENTINEL_151_9757));
    spawns.add(new NpcSpawn(4, new Tile(3596, 3366), NpcId.VYREWATCH_SENTINEL_151_9758));
    spawns.add(new NpcSpawn(4, new Tile(3594, 3358), NpcId.VYREWATCH_SENTINEL_151_9759));
    spawns.add(new NpcSpawn(4, new Tile(3597, 3352), NpcId.VYREWATCH_SENTINEL_151_9760));
    spawns.add(new NpcSpawn(4, new Tile(3596, 3346), NpcId.VYREWATCH_SENTINEL_151_9761));
    spawns.add(new NpcSpawn(4, new Tile(3599, 3342), NpcId.VYREWATCH_SENTINEL_151_9762));
    spawns.add(new NpcSpawn(4, new Tile(3597, 3336), NpcId.VYREWATCH_SENTINEL_151_9763));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3647, 3208), NpcId.VYREWATCH_105_8306));
    spawns.add(new NpcSpawn(4, new Tile(3597, 3326), NpcId.VYREWATCH_SENTINEL_151));
    spawns.add(new NpcSpawn(4, new Tile(3604, 3325), NpcId.VYREWATCH_SENTINEL_151_9757));
    spawns.add(new NpcSpawn(4, new Tile(3595, 3376), NpcId.VYREWATCH_SENTINEL_151_9758));
    spawns.add(new NpcSpawn(4, new Tile(3596, 3383), NpcId.VYREWATCH_SENTINEL_151_9759));
    spawns.add(new NpcSpawn(4, new Tile(3604, 3385), NpcId.VYREWATCH_SENTINEL_151_9760));
    spawns.add(new NpcSpawn(4, new Tile(3610, 3389), NpcId.VYREWATCH_SENTINEL_151_9761));
    spawns.add(new NpcSpawn(4, new Tile(3614, 3385), NpcId.VYREWATCH_SENTINEL_151_9762));
    spawns.add(new NpcSpawn(4, new Tile(3621, 3383), NpcId.VYREWATCH_SENTINEL_151_9763));
    spawns.add(new NpcSpawn(4, new Tile(3624, 3376), NpcId.VYREWATCH_SENTINEL_151));
    spawns.add(new NpcSpawn(4, new Tile(3634, 3376), NpcId.VYREWATCH_SENTINEL_151_9758));
    spawns.add(new NpcSpawn(4, new Tile(3639, 3382), NpcId.VYREWATCH_SENTINEL_151_9759));
    spawns.add(new NpcSpawn(4, new Tile(3648, 3382), NpcId.VYREWATCH_SENTINEL_151_9760));
    spawns.add(new NpcSpawn(4, new Tile(3654, 3377), NpcId.VYREWATCH_SENTINEL_151_9761));
    spawns.add(new NpcSpawn(4, new Tile(3661, 3376), NpcId.VYREWATCH_SENTINEL_151_9762));
    spawns.add(new NpcSpawn(4, new Tile(3634, 3388), NpcId.VYREWATCH_SENTINEL_151_9763));
    spawns.add(new NpcSpawn(4, new Tile(3628, 3393), NpcId.VYREWATCH_SENTINEL_151_9757));
    spawns.add(new NpcSpawn(4, new Tile(3655, 3368), NpcId.VYREWATCH_SENTINEL_151_9758));
    spawns.add(new NpcSpawn(4, new Tile(3651, 3362), NpcId.VYREWATCH_SENTINEL_151_9759));
    spawns.add(new NpcSpawn(4, new Tile(3653, 3356), NpcId.VYREWATCH_SENTINEL_151_9760));
    spawns.add(new NpcSpawn(4, new Tile(3657, 3352), NpcId.VYREWATCH_SENTINEL_151_9761));
    spawns.add(new NpcSpawn(4, new Tile(3655, 3347), NpcId.VYREWATCH_SENTINEL_151_9762));
    spawns.add(new NpcSpawn(4, new Tile(3654, 3341), NpcId.VYREWATCH_SENTINEL_151_9763));
    spawns.add(new NpcSpawn(4, new Tile(3646, 3341), NpcId.VYREWATCH_SENTINEL_151_9757));
    spawns.add(new NpcSpawn(4, new Tile(3635, 3340), NpcId.VYREWATCH_SENTINEL_151_9758));
    spawns.add(new NpcSpawn(4, new Tile(3628, 3336), NpcId.VYREWATCH_SENTINEL_151_9759));
    spawns.add(new NpcSpawn(4, new Tile(3629, 3342), NpcId.VYREWATCH_SENTINEL_151_9760));

    return spawns;
  }
}
