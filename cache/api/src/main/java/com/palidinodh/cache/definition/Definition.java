package com.palidinodh.cache.definition;

import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.Stream;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public interface Definition {

  static boolean isFieldMatch(String s, Definition definition) {
    try {
      s = s.toLowerCase();
      var hypenIndex = s.indexOf(':');
      if (hypenIndex == -1) {
        return false;
      }
      var fieldNameSearch = s.substring(0, hypenIndex).strip();
      var fieldValueSearch = s.substring(hypenIndex + 1).strip();
      var fields = definition.getClass().getDeclaredFields();
      for (var field : fields) {
        if (Modifier.isStatic(field.getModifiers())) {
          continue;
        }
        if (Modifier.isTransient(field.getModifiers())) {
          continue;
        }
        var fieldName = field.getName().toLowerCase();
        if (!fieldName.contains(fieldNameSearch) && !fieldNameSearch.contains(fieldName)) {
          continue;
        }
        field.setAccessible(true);
        var fieldValue = String.valueOf(field.get(definition));
        if (field.getType().isArray()) {
          if (isPrimitiveArray(field.get(definition))) {
            fieldValue = Arrays.toString((int[]) field.get(definition));
          } else {
            fieldValue = Arrays.toString((Object[]) field.get(definition));
          }
        }
        if (!fieldValue.contains(fieldValueSearch)) {
          continue;
        }
        return true;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return false;
  }

  static boolean isPrimitiveArray(Object obj) {
    if (obj == null) {
      return false;
    }
    if (!obj.getClass().isArray()) {
      return false;
    }
    return obj.getClass().getComponentType() != null
        && obj.getClass().getComponentType().isPrimitive();
  }

  void load(Stream stream);

  default void postLoad() {}

  default void postLoadSpecial(Store store) {}

  Stream save(Stream stream);

  default boolean isMatch(String s) {
    return isFieldMatch(s, this);
  }

  int getId();

  Definition getDefaultDefinition();

  Definition[] allDefinitions();
}
