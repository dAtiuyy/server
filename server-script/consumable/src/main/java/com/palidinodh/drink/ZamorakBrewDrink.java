package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableStat;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.ZAMORAK_BREW_1,
  ItemId.ZAMORAK_BREW_2,
  ItemId.ZAMORAK_BREW_3,
  ItemId.ZAMORAK_BREW_4
})
class ZamorakBrewDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.ATTACK, 2, 20, true));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 2, -10, false));
    builder.stat(new ConsumableStat(Skills.STRENGTH, 2, 12, true));
    builder.stat(new ConsumableStat(Skills.HITPOINTS, 2, -12, false));
    return builder;
  }
}
