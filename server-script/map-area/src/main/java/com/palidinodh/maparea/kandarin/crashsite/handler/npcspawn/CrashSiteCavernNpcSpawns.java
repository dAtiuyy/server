package com.palidinodh.maparea.kandarin.crashsite.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class CrashSiteCavernNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2153, 5668), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2154, 5677), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2146, 5678), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2133, 5677), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2128, 5672), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2125, 5679), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2102, 5673), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2098, 5679), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2095, 5672), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2073, 5680), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2080, 5675), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2074, 5672), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2073, 5646), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2079, 5645), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2076, 5652), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2119, 5659), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2111, 5661), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2104, 5658), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2111, 5651), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2104, 5643), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2096, 5645), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2095, 5658), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2094, 5651), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2102, 5651), NpcId.DEMONIC_GORILLA_275));
    spawns.add(new NpcSpawn(4, new Tile(2136, 5645), NpcId.TORTURED_GORILLA_141));
    spawns.add(new NpcSpawn(4, new Tile(2143, 5649), NpcId.TORTURED_GORILLA_141));
    spawns.add(new NpcSpawn(4, new Tile(2144, 5644), NpcId.TORTURED_GORILLA_141));
    spawns.add(new NpcSpawn(4, new Tile(2150, 5648), NpcId.TORTURED_GORILLA_141));
    spawns.add(new NpcSpawn(4, new Tile(2152, 5653), NpcId.TORTURED_GORILLA_141));
    spawns.add(new NpcSpawn(4, new Tile(2158, 5655), NpcId.TORTURED_GORILLA_141));
    spawns.add(new NpcSpawn(4, new Tile(2156, 5660), NpcId.TORTURED_GORILLA_141));
    spawns.add(new NpcSpawn(4, new Tile(2140, 5661), NpcId.TORTURED_GORILLA_141));
    spawns.add(new NpcSpawn(4, new Tile(2133, 5660), NpcId.TORTURED_GORILLA_141));

    return spawns;
  }
}
