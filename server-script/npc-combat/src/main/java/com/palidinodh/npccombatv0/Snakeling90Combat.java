package com.palidinodh.npccombatv0;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import java.util.Arrays;
import java.util.List;

class Snakeling90Combat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.SNAKELING_90);
    combat.spawn(NpcCombatSpawn.builder().animation(2413).lock(2).build());
    combat.hitpoints(NpcCombatHitpoints.total(1));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(140)
            .bonus(BonusType.MELEE_ATTACK, 120)
            .bonus(BonusType.ATTACK_MAGIC, 120)
            .bonus(BonusType.MELEE_DEFENCE, -40)
            .bonus(BonusType.DEFENCE_MAGIC, -40)
            .bonus(BonusType.DEFENCE_RANGED, -40)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(20).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.focus(
        NpcCombatFocus.builder().meleeUnlessUnreachable(true).bypassMapObjects(true).build());
    combat.deathAnimation(2408).blockAnimation(1742);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(15));
    style.animation(1741).attackSpeed(4);
    style.effect(NpcCombatEffect.builder().includeMiss(true).chance(75).venom(6).build());
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(15));
    style.animation(1741).attackSpeed(4).attackRange(3);
    style.projectile(NpcCombatProjectile.id(1230));
    style.effect(NpcCombatEffect.builder().includeMiss(true).chance(75).venom(6).build());
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
