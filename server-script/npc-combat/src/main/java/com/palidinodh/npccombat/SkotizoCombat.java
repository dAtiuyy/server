package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class SkotizoCombat extends NpcCombat {

  private static final Tile[] ALTAR_TILES = {
    new Tile(1693, 9904), new Tile(1697, 9871), new Tile(1714, 9889), new Tile(1678, 9887)
  };

  @Inject private Npc npc;
  private Npc[] altars = new Npc[4];
  private int altarSpawnTime;
  private List<Npc> minions = new ArrayList<>();

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .clue(ClueScrollType.ELITE, 5)
            .clue(ClueScrollType.HARD, 1)
            .pet(ItemId.SKOTOS, 65);
    var dropTable =
        NpcCombatDropTable.builder().probabilityDenominator(2500).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JAR_OF_DARKNESS)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(1000);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_ONYX)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(25).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_CLAW)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_TOTEM_BASE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_TOTEM_MIDDLE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_TOTEM)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 500)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 450)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 450)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATEBODY_NOTED, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_KITESHIELD_NOTED, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATELEGS_NOTED, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATESKIRT_NOTED, 3)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED_NOTED, 40)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_SNAPDRAGON_NOTED, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL_NOTED, 20)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DRAGONSTONE_NOTED, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BATTLESTAFF_NOTED, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ONYX_BOLT_TIPS, 40)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_ORE_NOTED, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BAR_NOTED, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_ANGLERFISH_NOTED, 60)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAHOGANY_PLANK_NOTED, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHIELD_LEFT_HALF)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.INFERNAL_ASHES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANCIENT_SHARD, 1, 5)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.SKOTIZO_321);
    combat.hitpoints(NpcCombatHitpoints.total(450));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(240)
            .magicLevel(280)
            .defenceLevel(200)
            .bonus(BonusType.MELEE_ATTACK, 160)
            .bonus(BonusType.MELEE_DEFENCE, 80)
            .bonus(BonusType.DEFENCE_MAGIC, 130)
            .bonus(BonusType.DEFENCE_RANGED, 130)
            .build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.type(NpcCombatType.DEMON).deathAnimation(4677).blockAnimation(4676);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(38));
    style.animation(4680).attackSpeed(6);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(38));
    style.animation(69).attackSpeed(6);
    style.targetGraphic(new Graphic(1243));
    style.projectile(NpcCombatProjectile.id(1242));
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    altarSpawnTime = 1 + PRandom.randomE(100);
  }

  @Override
  public void despawnHook() {
    npc.getWorld().removeNpcs(altars);
    npc.getWorld().removeNpcs(minions);
    if (getTarget() instanceof Player) {
      var player = (Player) getTarget();
      if (player.getWidgetManager().getOverlay() == WidgetId.SKOTIZO_OVERLAY) {
        player.getWidgetManager().removeOverlay();
      }
    }
  }

  @Override
  public void tickStartHook() {
    if (isDead()) {
      npc.getWorld().removeNpcs(altars);
    }
    if (npc.isVisible() && !isDead() && --altarSpawnTime <= 0) {
      altarSpawnTime = 1 + PRandom.randomE(100);
      for (var i = 0; i < altars.length; i++) {
        var index = PRandom.randomE(altars.length);
        var altar = altars[index];
        if (altar != null && (altar.isVisible() || !altar.getCombat().isDead())) {
          continue;
        }
        var tile = new Tile(ALTAR_TILES[index]).setHeight(npc.getHeight());
        altar =
            altars[index] = npc.getController().addNpc(new NpcSpawn(tile, NpcId.AWAKENED_ALTAR));
        altar.getSpawn().respawnable(npc.getSpawn().isRespawnable());
        break;
      }
    }
    if (npc.isVisible() && !isDead() && getTarget() instanceof Player) {
      var p = (Player) getTarget();
      if (p.getWidgetManager().getOverlay() != WidgetId.SKOTIZO_OVERLAY) {
        p.getWidgetManager().sendOverlay(WidgetId.SKOTIZO_OVERLAY);
      }
      // top is right
      // right is bottom
      p.getGameEncoder()
          .sendScript(
              ScriptId.CATA_ALTAR_UPDATE,
              altars[0] != null && altars[0].isVisible() ? 1 : 0,
              altars[1] != null && altars[1].isVisible() ? 1 : 0,
              altars[2] != null && altars[2].isVisible() ? 1 : 0,
              altars[3] != null && altars[3].isVisible() ? 1 : 0);
    }
    if (minions.isEmpty() && getTarget() != null && getHitpoints() < getMaxHitpoints() / 2) {
      if (PRandom.randomI(1) == 0) {
        minions.add(
            npc.getController()
                .addNpc(
                    new NpcSpawn(
                        new Tile(npc.getX() + 1, npc.getY(), npc.getHeight()),
                        NpcId.REANIMATED_DEMON_SPAWN_87)));
        minions.add(
            npc.getController()
                .addNpc(
                    new NpcSpawn(
                        new Tile(npc.getX() + 2, npc.getY() + 1, npc.getHeight()),
                        NpcId.REANIMATED_DEMON_SPAWN_87)));
        minions.add(
            npc.getController()
                .addNpc(
                    new NpcSpawn(
                        new Tile(npc.getX() + 3, npc.getY(), npc.getHeight()),
                        NpcId.REANIMATED_DEMON_SPAWN_87)));
      } else {
        minions.add(
            npc.getController()
                .addNpc(
                    new NpcSpawn(
                        new Tile(npc.getX() + 2, npc.getY() + 1, npc.getHeight()),
                        NpcId.DARK_ANKOU_95)));
      }
      for (var minion : minions) {
        minion.getCombat().setTarget(getTarget());
      }
    }
  }

  @Override
  public double defenceHook(
      Entity opponent, HitStyleType hitStyleType, BonusType meleeAttackStyle, double defence) {
    var boost = 1.0;
    for (var altar : altars) {
      if (altar == null || altar.isLocked()) {
        continue;
      }
      boost += 2;
    }
    return defence * boost;
  }
}
