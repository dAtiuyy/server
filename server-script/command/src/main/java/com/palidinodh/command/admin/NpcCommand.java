package com.palidinodh.command.admin;

import com.palidinodh.cache.definition.osrs.NpcDefinition;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.io.Readers;
import com.palidinodh.io.Writers;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.util.PLogger;
import com.palidinodh.util.PString;
import java.io.File;
import java.util.ArrayList;

@ReferenceName({"npc", "nspawn", "noption"})
class NpcCommand implements CommandHandler, CommandHandler.AdministratorRank {

  private static final String BASE_DIR = "./server-script/map-area/src/main/java";

  private static void createSpawn(File file, String line) {
    var filePath = file.getPath();
    var packageName = filePath.substring(BASE_DIR.length() + 1).replace("/", ".");
    packageName = packageName.substring(0, packageName.lastIndexOf('.'));
    var className = file.getName().substring(0, file.getName().indexOf('.'));
    packageName = packageName.substring(0, packageName.lastIndexOf('.'));
    var lines = new ArrayList<String>();

    lines.add("package " + packageName + ";");
    lines.add("");
    lines.add("import com.palidinodh.cache.id.NpcId;");
    lines.add("import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;");
    lines.add("import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;");
    lines.add("import com.palidinodh.osrscore.model.tile.Tile;");
    lines.add("import java.util.ArrayList;");
    lines.add("import java.util.List;");
    lines.add("");
    lines.add("class " + className + " implements NpcSpawnHandler {");
    lines.add("");
    lines.add("  @Override");
    lines.add("  public List<NpcSpawn> getSpawns() {");
    lines.add("    var spawns = new ArrayList<NpcSpawn>();");
    lines.add("");
    lines.add(line);
    lines.add("");
    lines.add("    return spawns;");
    lines.add("  }");
    lines.add("}");
    Writers.writeTextFile(file, lines);
  }

  private static void addSpawn(File file, String line) {
    var lines = Readers.readTextFileList(file);
    if (lines == null) {
      return;
    }
    var index = lines.indexOf("    return spawns;");
    if (index == -1) {
      return;
    }
    lines.add(index - 1, line);
    Writers.writeTextFile(file, lines);
  }

  private static void createOption(File file, String idName) {
    var filePath = file.getPath();
    var packageName = filePath.substring(BASE_DIR.length() + 1).replace("/", ".");
    packageName = packageName.substring(0, packageName.lastIndexOf('.'));
    var className = file.getName().substring(0, file.getName().indexOf('.'));
    packageName = packageName.substring(0, packageName.lastIndexOf('.'));
    var lines = new ArrayList<String>();

    lines.add("package " + packageName + ";");
    lines.add("");
    lines.add("import com.palidinodh.cache.definition.util.DefinitionOption;");
    lines.add("import com.palidinodh.cache.id.NpcId;");
    lines.add("import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;");
    lines.add("import com.palidinodh.osrscore.model.entity.npc.Npc;");
    lines.add("import com.palidinodh.osrscore.model.entity.player.Player;");
    lines.add("import com.palidinodh.rs.reference.ReferenceId;");
    lines.add("");
    lines.add("@ReferenceId(NpcId." + idName + ")");
    lines.add("public class " + className + " implements NpcHandler {");
    lines.add("");
    lines.add("  @Override");
    lines.add("  public void npcOption(Player player, DefinitionOption option, Npc npc) {}");
    lines.add("}");
    Writers.writeTextFile(file, lines);
  }

  private static void spawn(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var id =
        messages[0].matches("[0-9]+")
            ? Integer.parseInt(messages[0])
            : NpcId.valueOf(messages[0].replace(" ", "_").toUpperCase());
    if (id == -1) {
      player.getGameEncoder().sendMessage("Couldn't find npc.");
      return;
    }
    var distance = 0;
    var direction = Tile.Direction.SOUTH;
    if (messages.length == 2) {
      if (messages[1].matches("[0-9]+")) {
        distance = Integer.parseInt(messages[1]);
      } else {
        direction = Tile.Direction.valueOf(messages[1].replace(" ", "_").toUpperCase());
      }
    }
    if (direction == null) {
      player.getGameEncoder().sendMessage("Couldn't find direction.");
      return;
    }
    Npc npc;
    if (name.equals("nspawn")) {
      npc = player.getController().addNpc(new NpcSpawn(direction, player, id));
    } else {
      if (distance > 0) {
        npc = player.getController().addNpc(new NpcSpawn(0, player, id));
      } else {
        npc = player.getController().addNpc(new NpcSpawn(direction, player, id));
      }
    }
    if (!name.equals("nspawn")) {
      player.getGameEncoder().sendMessage("Spawned " + npc.getDef().getName() + "!");
      return;
    }
    var npcIdInfo = "NpcId." + NpcId.valueOf(id);
    var tileInfo =
        "new Tile("
            + player.getX()
            + ", "
            + player.getY()
            + (player.getHeight() > 0 ? ", " + player.getHeight() : "")
            + ")";
    var directionInfo = "Tile.Direction." + direction;
    var line = "    spawns.add(new NpcSpawn(" + tileInfo + ", " + npcIdInfo + "));";
    if (distance > 0) {
      line = "    spawns.add(new NpcSpawn(" + distance + ", " + tileInfo + ", " + npcIdInfo + "));";
    } else if (direction != Tile.Direction.SOUTH) {
      line =
          "    spawns.add(new NpcSpawn("
              + directionInfo
              + ", "
              + tileInfo
              + ", "
              + npcIdInfo
              + "));";
    }
    PLogger.println(line);
    var areaName = player.getArea().getClass().getName();
    if (areaName.endsWith(".Area")) {
      player.getGameEncoder().sendMessage(areaName + ": bad class name.");
      return;
    }
    areaName = areaName.replace(".", "/");
    var simpleAreaName = areaName.substring(areaName.lastIndexOf('/') + 1);
    areaName = BASE_DIR + "/" + areaName + ".java";
    var areaPackage = areaName.substring(0, areaName.lastIndexOf('/'));
    if (!(new File(areaName)).exists()) {
      player.getGameEncoder().sendMessage(areaName + ": area doesn't exist.");
      return;
    }
    var spawnName = areaPackage + "/handler/npcspawn/" + simpleAreaName + "NpcSpawns.java";
    var spawnFile = new File(spawnName);
    if (!spawnFile.exists()) {
      spawnName =
          areaPackage
              + "/handler/npcspawn/"
              + simpleAreaName.replace("Area", "")
              + "NpcSpawns.java";
      spawnFile = new File(spawnName);
    }
    player.getGameEncoder().sendMessage("Saved to " + spawnFile.getName());
    if (spawnFile.exists()) {
      addSpawn(spawnFile, line);
    } else {
      createSpawn(spawnFile, line);
    }
  }

  private static void option(Player player, String name, String message) {
    var messages = CommandHandler.split(message);
    var npcId = -1;
    var npcIdName = "";
    if (messages[0].matches("[0-9]+")) {
      npcId = Integer.parseInt(messages[0]);
      npcIdName = NpcId.valueOf(npcId);
    } else {
      npcIdName = messages[0].replace(" ", "_").toUpperCase();
      npcId = NpcId.valueOf(npcIdName);
    }
    if (npcId == -1 || npcIdName == null || npcIdName.isEmpty()) {
      player.getGameEncoder().sendMessage("Couldn't find npc.");
      return;
    }
    var formattedNpcName = PString.formatName(NpcDefinition.getName(npcId)).replace(" ", "");
    var areaName = player.getArea().getClass().getName();
    if (areaName.endsWith(".Area")) {
      player.getGameEncoder().sendMessage(areaName + ": bad class name.");
      return;
    }
    areaName = areaName.replace(".", "/");
    areaName = BASE_DIR + "/" + areaName + ".java";
    var areaPackage = areaName.substring(0, areaName.lastIndexOf('/'));
    if (!(new File(areaName)).exists()) {
      player.getGameEncoder().sendMessage(areaName + ": area doesn't exist.");
      return;
    }
    var optionName = areaPackage + "/handler/npc/" + formattedNpcName + "Npc.java";
    var optionFile = new File(optionName);
    if (optionFile.exists()) {
      player.getGameEncoder().sendMessage("Npc option already exists.");
      return;
    }
    player.getGameEncoder().sendMessage("Saved to " + optionFile.getName());
    createOption(optionFile, npcIdName);
  }

  @Override
  public String getExample(String name) {
    return "id_or_name (distance) (direction)";
  }

  @Override
  public void execute(Player player, String name, String message) {
    switch (name) {
      case "npc":
      case "nspawn":
        spawn(player, name, message);
        break;
      case "noption":
        option(player, name, message);
        break;
    }
  }
}
