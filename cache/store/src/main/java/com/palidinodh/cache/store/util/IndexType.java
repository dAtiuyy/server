package com.palidinodh.cache.store.util;

public enum IndexType {
  ANIMATION,
  ANIMATION_SKELETON,
  CONFIG,
  WIDGET,
  SOUND_FX,
  MAP,
  MUSIC_TRACK,
  MODEL,
  SPRITE,
  TEXTURE,
  BINARY,
  MUSIC_JINGLE,
  CLIENTSCRIPT,
  FONT,
  MUSIC_SAMPLE,
  MUSIC_PATCH,
  OLD_WORLD_MAP,
  UNKNOWN_17,
  WORLD_MAP_GEOGRAPHY,
  WORLD_MAP,
  WORLD_MAP_GROUND;

  public static IndexType get(int index) {
    return index >= 0 && index <= values().length ? values()[index] : null;
  }

  public static IndexType get(String name) {
    name = name.toUpperCase();
    for (var indexType : values()) {
      if (!name.equals(indexType.name()) && !name.equals(indexType.name().replace("_", ""))) {
        continue;
      }
      return indexType;
    }
    return null;
  }

  public static String getName(int index) {
    var type = get(index);
    if (type == null || type.name().contains("UNKNOWN")) {
      return Integer.toString(index);
    }
    return type.name().toLowerCase();
  }

  public int getId() {
    return ordinal();
  }
}
