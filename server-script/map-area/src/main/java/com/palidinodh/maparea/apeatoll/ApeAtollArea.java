package com.palidinodh.maparea.apeatoll;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({10794, 10795, 11050, 11051})
public class ApeAtollArea extends Area {}
