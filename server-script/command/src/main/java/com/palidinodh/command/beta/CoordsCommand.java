package com.palidinodh.command.beta;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler.Beta;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("coords")
class CoordsCommand implements CommandHandler, Beta {

  @Override
  public void execute(Player player, String name, String message) {
    player
        .getGameEncoder()
        .sendMessage(
            "x="
                + player.getX()
                + ", y="
                + player.getY()
                + ", z="
                + player.getHeight()
                + ", client-z="
                + player.getClientHeight()
                + ", region-id="
                + player.getRegionId()
                + ", instanced="
                + player.getController().isInstanced());
    player.getGameEncoder().sendMessage("area=" + player.getArea().getClass().getSimpleName());
    var localChunkX = (player.getX() - Tile.getAbsRegionX(player.getRegionId())) >> 3;
    var localChunkY = (player.getY() - Tile.getAbsRegionY(player.getRegionId())) >> 3;
    var localChunkId = (localChunkX << 4) + localChunkY;
    player
        .getGameEncoder()
        .sendMessage("local-chunk=" + localChunkX + ", " + localChunkY + "=" + localChunkId);
    var mapClip =
        player.getController().getMapClip(player.getX(), player.getY(), player.getHeight());
    if (mapClip != 0) {
      player
          .getGameEncoder()
          .sendMessage(
              "map-clip="
                  + player
                      .getController()
                      .getMapClip(player.getX(), player.getY(), player.getHeight()));
    }
    var mapObject = player.getController().getSolidMapObject(player);
    if (mapObject != null) {
      player
          .getGameEncoder()
          .sendMessage(
              "solid-map-object=" + mapObject + ", anim=" + mapObject.getDef().getAnimationId());
    }
    mapObject = player.getController().getDirectionalMapObject(player);
    if (mapObject != null) {
      player
          .getGameEncoder()
          .sendMessage(
              "directional-map-object="
                  + mapObject
                  + ", anim="
                  + mapObject.getDef().getAnimationId());
    }
    mapObject = player.getController().getMapObjectByType(22, player);
    if (mapObject != null) {
      player.getGameEncoder().sendMessage("map-object-22: " + mapObject);
    }
    var overlay = player.getController().getMapOverlay(player);
    if (overlay != -1) {
      player
          .getGameEncoder()
          .sendMessage("overlay=" + player.getController().getMapOverlay(player));
    }
    var controllerName = player.getController().getClass().getSimpleName();
    if (!"DefaultPC".equals(controllerName)) {
      player.getGameEncoder().sendMessage("controller=" + controllerName);
    }
  }
}
