package com.palidinodh.skill.hunter;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.hunter.HunterPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import java.util.List;

public class Hunter extends SkillContainer {

  private static final Item MAGIC_BUTTERFLY_NET = new Item(ItemId.MAGIC_BUTTERFLY_NET);
  private static final Item MAGIC_SECATEURS = new Item(ItemId.MAGIC_SECATEURS);

  @Override
  public int getSkillId() {
    return Skills.HUNTER;
  }

  @Override
  public List<SkillEntry> getEntries() {
    return HunterEntries.getEntries();
  }

  @Override
  public int getDefaultMakeAmount(Player player, Npc npc, MapObject mapObject) {
    return npc != null && npc.getId() == NpcId.AFK_HERBIBOAR_16078 ? Integer.MAX_VALUE : 1;
  }

  @Override
  public int getEventTick(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null && npc.getId() == NpcId.AFK_HERBIBOAR_16078) {
      return 4;
    }
    return entry.getTick();
  }

  @Override
  public void actionSuccess(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null) {
      if (npc.getId() == NpcId.AFK_HERBIBOAR_16078) {
        player.getSkills().addXp(Skills.HERBLORE, entry.getExperience());
      } else {
        npc.getCombat().startDeath(2);
      }
    }
  }

  @Override
  public Item createHook(
      Player player, PEvent event, Item item, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (entry.getConsume() != null && entry.getConsume().getId() == ItemId.IMPLING_JAR) {
      player
          .getGameEncoder()
          .sendFilteredMessage("You manage to catch the impling and squeeze it into a jar.");
    } else if (entry.getConsume() != null && entry.getConsume().getId() == ItemId.BUTTERFLY_JAR) {
      player
          .getGameEncoder()
          .sendFilteredMessage("You manage to catch the butterfly and squeeze it into a jar.");
    }
    return item;
  }

  @Override
  public int experienceHook(
      Player player, PEvent event, int experience, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (player.getEquipment().wearingLarupiaOutfit()) {
      experience *= 1.1;
    }
    return experience;
  }

  @Override
  public boolean canDoActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null && npc.getId() == NpcId.AFK_HERBIBOAR_16078) {
      if (player.hasIPMatch()) {
        player.getGameEncoder().sendMessage("This feature is disabled with multi-logging.");
        return false;
      }
    }
    return true;
  }

  @Override
  public Item toolHook(Player player, Item tool, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (tool.getId() == ItemId.BUTTERFLY_NET
        && player.getEquipment().getWeaponId() == ItemId.MAGIC_BUTTERFLY_NET) {
      return MAGIC_BUTTERFLY_NET;
    }
    if (tool.getId() == ItemId.SECATEURS
        && player.getEquipment().getWeaponId() == ItemId.MAGIC_SECATEURS) {
      return MAGIC_SECATEURS;
    }
    return tool;
  }

  @Override
  public boolean skipActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null && npc.getId() == NpcId.AFK_HERBIBOAR_16078) {
      return PRandom.randomE(8) != 0;
    }
    var power = player.getSkills().getLevel(getSkillId()) + 8;
    var failure = entry.getLevel() + 2;
    if (player.inWildernessResourceArea()) {
      failure /= 2;
    }
    var chance = 0;
    if (power > failure) {
      chance = (int) ((1 - (failure + 2) / ((power + 1) * 2.0)) * 100);
    } else {
      chance = (int) ((power / ((failure + 1) * 2.0)) * 100);
    }
    chance *= 1 + getMembershipBoost(player);
    if (npc != null && player.getEquipment().getWeaponId() == ItemId.MAGIC_BUTTERFLY_NET) {
      chance += 10;
    }
    if (player.getEquipment().wearingLarupiaOutfit()) {
      chance += 10;
    }
    chance = Math.min(chance, 100);
    return PRandom.randomE(100) > chance;
  }

  @Override
  public boolean mapObjectOptionHook(Player player, DefinitionOption option, MapObject mapObject) {
    switch (mapObject.getName().toLowerCase()) {
      case "bird snare":
      case "net trap":
      case "shaking box":
      case "box trap":
      case "young tree":
        if (!player.getPlugin(HunterPlugin.class).pickupTrap(mapObject)) {
          player.getPlugin(HunterPlugin.class).layTrap(-1, mapObject);
        }
        return true;
    }
    return false;
  }
}
