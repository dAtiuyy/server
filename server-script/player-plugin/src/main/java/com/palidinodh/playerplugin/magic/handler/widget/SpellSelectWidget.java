package com.palidinodh.playerplugin.magic.handler.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.SpellSelectChild;
import com.palidinodh.osrscore.io.cache.widget.ViewportContainer;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.magic.AutocastType;
import com.palidinodh.osrscore.model.entity.player.magic.CombatSpell;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.SPELL_SELECT)
class SpellSelectWidget implements WidgetHandler {

  private static void selectSpell(Player player, CombatSpell spell) {
    var attackStyle = player.getMagic().getAttackStyle();
    var defensive = player.getAttributeBool("magic_defensive");
    attackStyle.setSpell(spell == null ? null : spell.getSpellbook());
    attackStyle.setDefensive(defensive);
    player.getWidgetManager().sendWidget(ViewportContainer.COMBAT, WidgetId.COMBAT);
    player.getEquipment().sendCombatTabText();
    player.getCombat().sendAttackStyle();
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (slot == 0) {
      selectSpell(player, null);
      return;
    }
    var autocast = AutocastType.get(player);
    if (autocast == null) {
      return;
    }
    var spellSelectChild = SpellSelectChild.get(slot);
    if (spellSelectChild == null) {
      return;
    }
    var spellChild = spellSelectChild.getSpellbookChild();
    if (spellChild == null) {
      return;
    }
    if (!autocast.hasSpell(spellChild)) {
      return;
    }
    CombatSpell spell = CombatSpell.getDef(spellChild);
    if (slot != 0 && spell == null) {
      player.getGameEncoder().sendMessage("Unable to find the spell you selected.");
      return;
    }
    selectSpell(player, spell);
  }
}
