package com.palidinodh.food;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableHeal;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.PYSK_FISH_0, ItemId.GUANIC_BAT_0})
class CoxTier0Food implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(5));
    return builder;
  }
}

@ReferenceId({ItemId.SUPHI_FISH_1, ItemId.PRAEL_BAT_1})
class CoxTier1Food implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(8));
    return builder;
  }
}

@ReferenceId({ItemId.LECKISH_FISH_2, ItemId.GIRAL_BAT_2})
class CoxTier2Food implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(11));
    return builder;
  }
}

@ReferenceId({ItemId.BRAWK_FISH_3, ItemId.PHLUXIA_BAT_3})
class CoxTier3Food implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(14));
    return builder;
  }
}

@ReferenceId({ItemId.MYCIL_FISH_4, ItemId.KRYKET_BAT_4})
class CoxTier4Food implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(17));
    return builder;
  }
}

@ReferenceId({ItemId.ROQED_FISH_5, ItemId.MURNG_BAT_5})
class CoxTier5Food implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(20));
    return builder;
  }
}

@ReferenceId({ItemId.KYREN_FISH_6, ItemId.PSYKK_BAT_6})
class CoxTier6Food implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(23));
    return builder;
  }
}
