package com.palidinodh.playerplugin.hunter.birdhouse;

import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.util.PTime;
import lombok.Getter;

@Getter
public class BirdHouseTrap {

  private static final long TOTAL_TIME = PTime.minToMilli(50);
  private static final int BIRD_CAPACITY = 10;

  private MapObject mapObject;
  private BirdHouseLocationType locationType;
  private BirdHouseType type;
  private int varpValue;
  private int seededPercent;
  private long time;

  public BirdHouseTrap(
      MapObject mapObject, BirdHouseLocationType locationType, BirdHouseType type) {
    this.mapObject = mapObject;
    this.locationType = locationType;
    this.type = type;
    varpValue = type.getEmptyVarpValue();
  }

  public void increaseSeededPercent(int percent) {
    var wasEmpty = seededPercent == 0;
    seededPercent = Math.min(seededPercent + percent, 100);
    if (seededPercent == 100) {
      mapObject = new MapObject(type.getCatchingObjectId(), mapObject);
      varpValue = type.getCatchingVarpValue();
      time = PTime.currentTimeMillis();
    } else if (wasEmpty) {
      mapObject = new MapObject(type.getSeedingObjectId(), mapObject);
      varpValue = type.getSeedingVarpValue();
    }
  }

  public long getRemainingTime() {
    if (!isStarted()) {
      return Long.MAX_VALUE;
    }
    return Math.min(PTime.currentTimeMillis() - time, TOTAL_TIME);
  }

  public boolean isStarted() {
    return time != 0;
  }

  public int getBirdsCaught() {
    long remaining = getRemainingTime();
    if (remaining == Long.MAX_VALUE) {
      return 0;
    }
    return (int) (BIRD_CAPACITY * (remaining / TOTAL_TIME));
  }

  public MapObject getDisplayMapObject() {
    if (locationType != null && locationType.getVarpId() != -1) {
      return null;
    }
    return getMapObject();
  }

  public MapObject getMapObject() {
    mapObject.setVisible(true);
    return mapObject;
  }
}
