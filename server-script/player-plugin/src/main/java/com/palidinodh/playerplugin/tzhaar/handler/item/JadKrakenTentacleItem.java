package com.palidinodh.playerplugin.tzhaar.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.KRAKEN_TENTACLE_OR_60048)
class JadKrakenTentacleItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "revert":
        {
          item.remove();
          player.getInventory().addItem(ItemId.KRAKEN_TENTACLE);
          player.getInventory().addItem(ItemId.KRAKEN_ORNAMENT_KIT_60059);
          break;
        }
    }
  }
}
