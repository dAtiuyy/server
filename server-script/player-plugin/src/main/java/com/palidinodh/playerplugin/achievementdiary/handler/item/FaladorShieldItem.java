package com.palidinodh.playerplugin.achievementdiary.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.achievementdiary.AchievementDiaryPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PTime;

@ReferenceId({
  ItemId.FALADOR_SHIELD_1,
  ItemId.FALADOR_SHIELD_2,
  ItemId.FALADOR_SHIELD_3,
  ItemId.FALADOR_SHIELD_4
})
class FaladorShieldItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var plugin = player.getPlugin(AchievementDiaryPlugin.class);
    switch (item.getId()) {
      case ItemId.FALADOR_SHIELD_1:
      case ItemId.FALADOR_SHIELD_2:
        {
          var prayerLevel = player.getController().getLevelForXP(Skills.PRAYER);
          if (option.equals("recharge-prayer") || option.equals("recharge prayer")) {
            if (PTime.getDate().equals(plugin.getFaladorShield1And2())) {
              player.getGameEncoder().sendMessage("You can only use this once a day.");
              break;
            }
            plugin.setFaladorShield1And2(PTime.getDate());
            if (item.getId() == ItemId.FALADOR_SHIELD_1) {
              player.getPrayer().changePoints((int) (prayerLevel * 0.25));
            } else if (item.getId() == ItemId.FALADOR_SHIELD_2) {
              player.getPrayer().changePoints((int) (prayerLevel * 0.5));
            }
            player.getGameEncoder().sendMessage("Your prayer points have been restored.");
          }
          break;
        }
      case ItemId.FALADOR_SHIELD_3:
      case ItemId.FALADOR_SHIELD_4:
        {
          var prayerLevel = player.getController().getLevelForXP(Skills.PRAYER);
          if (option.equals("recharge-prayer") || option.equals("recharge prayer")) {
            if (!PTime.getDate().equals(plugin.getFaladorShield3And4())) {
              plugin.setFaladorShield3And4(PTime.getDate());
              player.getPrayer().changePoints(prayerLevel);
              player.getGameEncoder().sendMessage("Your prayer points have been restored.");
            } else if (!PTime.getDate().equals(plugin.getFaladorShield1And2())) {
              plugin.setFaladorShield1And2(PTime.getDate());
              if (item.getId() == ItemId.FALADOR_SHIELD_3) {
                player.getPrayer().changePoints((int) (prayerLevel * 0.5));
              } else if (item.getId() == ItemId.FALADOR_SHIELD_4) {
                player.getPrayer().changePoints(prayerLevel);
              }
              player.getGameEncoder().sendMessage("Your prayer points have been restored.");
            } else {
              player.getGameEncoder().sendMessage("You can only use this once a day.");
            }
          } else if (option.equals("check")) {
            if (player.getRegionId() != 6992 && player.getRegionId() != 6993) {
              player.getGameEncoder().sendMessage("You can't use this here.");
              break;
            }
            var mole = player.getController().getNpc(NpcId.GIANT_MOLE_230);
            if (mole == null) {
              player.getGameEncoder().sendMessage("Unable to locate the giant mole.");
              break;
            }
            player.getGameEncoder().sendHintIconTile(mole);
            player
                .getWorld()
                .addEvent(
                    PEvent.singleEvent(
                        16,
                        e -> {
                          if (!player.isVisible()) {
                            return;
                          }
                          player.getGameEncoder().sendHintIconReset();
                        }));
          }
          break;
        }
    }
  }
}
