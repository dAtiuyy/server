package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.TOXIC_BLOWPIPE)
class ToxicBlowpipeItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "unload":
        player.getCharges().unloadToxicBlowpipe(item.getSlot(), false);
        break;
      case "uncharge":
        player.getCharges().unloadToxicBlowpipe(item.getSlot(), true);
        break;
    }
  }
}
