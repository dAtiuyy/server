package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;

@ReferenceId(NpcId.ELISABETA)
class ElisabetaNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    if (!Settings.getInstance().isLocal() && !Settings.getInstance().isBeta()) {
      player.getGameEncoder().sendMessage("Elisabeta doesn't want to talk to you.");
      return;
    }
    player.openOptionsDialogue(
        new DialogueOption(
            "Max my stats.",
            (c, s) -> {
              for (var id = 0; id < Skills.SKILL_COUNT; id++) {
                player.getSkills().setLevel(id, 99);
                player.getSkills().setXP(id, Skills.XP_TABLE[99]);
                player.getGameEncoder().sendSkillLevel(id);
              }
              player.getSkills().setCombatLevel();
              player.getCombat().setMaxHitpoints(99);
              player.rejuvenate();
              player.restore();
            }),
        new DialogueOption(
            "View shop.",
            (c, s) -> {
              player.openShop("beta");
            }));
  }
}
