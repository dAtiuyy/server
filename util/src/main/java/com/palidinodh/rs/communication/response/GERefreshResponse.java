package com.palidinodh.rs.communication.response;

import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeItem;
import com.palidinodh.rs.communication.request.Request;
import java.util.List;

public class GERefreshResponse extends Response {

  private int userId;
  private int gameMode;
  private List<GrandExchangeItem> items;

  public GERefreshResponse(Request request, List<GrandExchangeItem> items) {
    super(request);
    this.items = items;
  }

  public GERefreshResponse(
      Request request, int userId, int gameMode, List<GrandExchangeItem> items) {
    super(request);
    this.userId = userId;
    this.gameMode = gameMode;
    this.items = items;
  }

  public int getUserId() {
    return userId;
  }

  public int getGameMode() {
    return gameMode;
  }

  public List<GrandExchangeItem> getItems() {
    return items;
  }
}
