package com.palidinodh.maparea.voidknightsoutpost.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({NpcId.SQUIRE_1765, NpcId.SQUIRE_1766, NpcId.SQUIRE_1767, NpcId.SQUIRE_1768})
class EquipmentSquireNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    player.openShop("equipment");
  }
}
