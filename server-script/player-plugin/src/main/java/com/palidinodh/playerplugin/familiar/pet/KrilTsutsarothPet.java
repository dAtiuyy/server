package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class KrilTsutsarothPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(
            ItemId.PET_KRIL_TSUTSAROTH, NpcId.KRIL_TSUTSAROTH_JR_6647, NpcId.KRIL_TSUTSAROTH_JR));
    return builder;
  }
}
