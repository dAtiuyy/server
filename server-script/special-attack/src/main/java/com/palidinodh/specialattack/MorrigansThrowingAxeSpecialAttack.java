package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.MORRIGANS_THROWING_AXE)
class MorrigansThrowingAxeSpecialAttack extends SpecialAttack {

  MorrigansThrowingAxeSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(806);
    entry.castGraphic(new Graphic(1626, 96));
    entry.projectileId(1625);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    var opponent = hooks.getOpponent();
    if (!opponent.isPlayer()) {
      return;
    }
    opponent.asPlayer().getCombat().setMorrigansThrowingAxeSpecial(100);
  }
}
