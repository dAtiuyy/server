package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.KBD_HEADS)
class KbdHeadsItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "separate":
        player.openDialogue(
            new OptionsDialogue(
                "Are you sure you want to separate the " + item.getName() + "?",
                new DialogueOption(
                    "Yes, dismantle the Kbd heads for 3 ensouled dragon heads",
                    (c, s) -> {
                      if (player.getInventory().getRemainingSlots() < 3 - 1) {
                        player.getInventory().notEnoughSpace();
                        return;
                      }
                      item.remove();
                      player.getInventory().addOrDropItem(ItemId.ENSOULED_DRAGON_HEAD_13511, 3);
                    }),
                new DialogueOption("Cancel.")));
        break;
    }
  }
}
