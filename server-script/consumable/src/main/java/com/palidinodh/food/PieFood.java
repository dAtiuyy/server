package com.palidinodh.food;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.PMovement;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableHeal;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.SUMMER_PIE)
class SummerPieFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(11));
    builder.addItem(new Item(ItemId.HALF_A_SUMMER_PIE));
    builder.tick(1);
    builder.action(
        (p, c) -> {
          var restore = (int) (PMovement.MAX_ENERGY * 0.1);
          p.getMovement().setEnergy(p.getMovement().getEnergy() + restore);
          p.getSkills().changeStat(Skills.AGILITY, 5, true, 0);
        });
    return builder;
  }
}

@ReferenceId(ItemId.HALF_A_SUMMER_PIE)
class HalfASummerPieFood implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.heal(new ConsumableHeal(11));
    builder.addItem(new Item(ItemId.PIE_DISH));
    builder.tick(1);
    builder.action(
        (p, c) -> {
          var restore = (int) (PMovement.MAX_ENERGY * 0.1);
          p.getMovement().setEnergy(p.getMovement().getEnergy() + restore);
          p.getSkills().changeStat(Skills.AGILITY, 5, true, 0);
        });
    return builder;
  }
}
