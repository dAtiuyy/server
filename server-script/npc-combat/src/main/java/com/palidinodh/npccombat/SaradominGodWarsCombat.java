package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class SaradominGodWarsCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var knightOfSaradominDrop = NpcCombatDrop.builder().clue(ClueScrollType.HARD, 128);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    knightOfSaradominDrop.table(dropTable.build());

    var knightOfSaradomin101Combat = NpcCombatDefinition.builder();
    knightOfSaradomin101Combat.id(NpcId.KNIGHT_OF_SARADOMIN_101);
    knightOfSaradomin101Combat.hitpoints(NpcCombatHitpoints.total(108));
    knightOfSaradomin101Combat.stats(
        NpcCombatStats.builder()
            .attackLevel(75)
            .magicLevel(60)
            .defenceLevel(82)
            .bonus(BonusType.MELEE_ATTACK, 13)
            .bonus(BonusType.DEFENCE_STAB, 12)
            .bonus(BonusType.DEFENCE_SLASH, 14)
            .bonus(BonusType.DEFENCE_CRUSH, 13)
            .bonus(BonusType.DEFENCE_RANGED, 13)
            .build());
    knightOfSaradomin101Combat.aggression(
        NpcCombatAggression.builder().type(NpcCombatAggression.Type.PLAYERS).always(true).build());
    knightOfSaradomin101Combat.deathAnimation(836).blockAnimation(410);
    knightOfSaradomin101Combat.drop(knightOfSaradominDrop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(12));
    style.animation(406).attackSpeed(6);
    knightOfSaradomin101Combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(12));
    style.animation(407).attackSpeed(6);
    knightOfSaradomin101Combat.style(style.build());

    var knightOfSaradomin103Combat = NpcCombatDefinition.builder();
    knightOfSaradomin103Combat.id(NpcId.KNIGHT_OF_SARADOMIN_103);
    knightOfSaradomin103Combat.hitpoints(NpcCombatHitpoints.total(135));
    knightOfSaradomin103Combat.stats(
        NpcCombatStats.builder()
            .attackLevel(70)
            .magicLevel(60)
            .defenceLevel(70)
            .bonus(BonusType.MELEE_ATTACK, 8)
            .bonus(BonusType.DEFENCE_STAB, 10)
            .bonus(BonusType.DEFENCE_SLASH, 10)
            .bonus(BonusType.DEFENCE_CRUSH, 7)
            .bonus(BonusType.DEFENCE_RANGED, 13)
            .build());
    knightOfSaradomin103Combat.aggression(
        NpcCombatAggression.builder().type(NpcCombatAggression.Type.PLAYERS).always(true).build());
    knightOfSaradomin103Combat.deathAnimation(836).blockAnimation(410);
    knightOfSaradomin103Combat.drop(knightOfSaradominDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(11));
    style.animation(406).attackSpeed(6);
    knightOfSaradomin103Combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(11));
    style.animation(407).attackSpeed(6);
    knightOfSaradomin103Combat.style(style.build());

    var saradominPriestDrop = NpcCombatDrop.builder().clue(ClueScrollType.HARD, 128);
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    saradominPriestDrop.table(dropTable.build());

    var saradominPriestCombat = NpcCombatDefinition.builder();
    saradominPriestCombat.id(NpcId.SARADOMIN_PRIEST_113);
    saradominPriestCombat.hitpoints(NpcCombatHitpoints.total(89));
    saradominPriestCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(120)
            .magicLevel(125)
            .defenceLevel(120)
            .bonus(BonusType.MELEE_ATTACK, 9)
            .bonus(BonusType.DEFENCE_STAB, 12)
            .bonus(BonusType.DEFENCE_SLASH, 14)
            .bonus(BonusType.DEFENCE_CRUSH, 13)
            .bonus(BonusType.DEFENCE_MAGIC, 5)
            .bonus(BonusType.DEFENCE_RANGED, 13)
            .build());
    saradominPriestCombat.aggression(
        NpcCombatAggression.builder().type(NpcCombatAggression.Type.PLAYERS).always(true).build());
    saradominPriestCombat.deathAnimation(836).blockAnimation(404);
    saradominPriestCombat.drop(saradominPriestDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(13));
    style.animation(811).attackSpeed(5);
    style.targetGraphic(new Graphic(76, 100));
    saradominPriestCombat.style(style.build());

    var spiritualMageDrop =
        NpcCombatDrop.builder().gemDropTableDenominator(64).clue(ClueScrollType.HARD, 128);
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_BOOTS)));
    spiritualMageDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ASTRAL_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MIST_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DUST_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MUD_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MIND_RUNE, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIPOISON_3_NOTED, 5)));
    spiritualMageDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 90)));
    spiritualMageDrop.table(dropTable.build());

    var spiritualMageCombat = NpcCombatDefinition.builder();
    spiritualMageCombat.id(NpcId.SPIRITUAL_MAGE_120);
    spiritualMageCombat.hitpoints(NpcCombatHitpoints.total(85));
    spiritualMageCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(160)
            .defenceLevel(86)
            .bonus(BonusType.DEFENCE_STAB, 8)
            .bonus(BonusType.DEFENCE_SLASH, 7)
            .bonus(BonusType.DEFENCE_CRUSH, 3)
            .bonus(BonusType.DEFENCE_MAGIC, 16)
            .bonus(BonusType.DEFENCE_RANGED, 2)
            .build());
    spiritualMageCombat.slayer(NpcCombatSlayer.builder().level(83).build());
    spiritualMageCombat.aggression(
        NpcCombatAggression.builder().type(NpcCombatAggression.Type.PLAYERS).always(true).build());
    spiritualMageCombat.type(NpcCombatType.SPECTRAL).deathAnimation(836).blockAnimation(404);
    spiritualMageCombat.drop(spiritualMageDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(17));
    style.animation(811).attackSpeed(4);
    style.targetGraphic(new Graphic(76, 100));
    spiritualMageCombat.style(style.build());

    return Arrays.asList(
        knightOfSaradomin101Combat.build(),
        knightOfSaradomin103Combat.build(),
        saradominPriestCombat.build(),
        spiritualMageCombat.build());
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    player.getArea().script("increase_saradomin_killcount");
    if (npc.getArea().inWilderness() && PRandom.randomE(60) == 0) {
      npc.getController().addMapItem(new Item(ItemId.ECUMENICAL_KEY, 1), dropTile, player);
    }
  }

  @Override
  public boolean canBeAggressiveHook(Entity opponent) {
    if (!opponent.isPlayer()) {
      return true;
    }
    var player = opponent.asPlayer();
    return !player.getEquipment().hasItemIgnoreCase("Saradomin")
        && player.getEquipment().getShieldId() != ItemId.HOLY_BOOK
        && player.getEquipment().getAmmoId() != ItemId.HOLY_BLESSING
        && player.getEquipment().getNeckId() != ItemId.HOLY_SYMBOL
        && !player.getEquipment().hasItemIgnoreCase("Monk's robe")
        && player.getEquipment().getFootId() != ItemId.HOLY_SANDALS;
  }
}
