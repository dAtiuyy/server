package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;

class KalphitePrincessPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(
            ItemId.KALPHITE_PRINCESS, NpcId.KALPHITE_PRINCESS_6654, NpcId.KALPHITE_PRINCESS_6638));
    builder.entry(
        new Pet.Entry(
            ItemId.KALPHITE_PRINCESS_12654, NpcId.KALPHITE_PRINCESS_6653, NpcId.KALPHITE_PRINCESS));
    builder.optionVariation(
        (p, n) -> {
          switch (n.getId()) {
            case NpcId.KALPHITE_PRINCESS_6653:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.KALPHITE_PRINCESS_6654);
              break;
            case NpcId.KALPHITE_PRINCESS_6654:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.KALPHITE_PRINCESS_6653);
              break;
          }
        });
    return builder;
  }
}
