package com.palidinodh.maparea.asgarnia.donator.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.BOB_BARTER_HERBS)
class DzoneBobBarterNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    if (option.getIndex() == 0) {
      player.openOldDialogue("bobbarter", 0);
    } else if (option.getIndex() == 2) {
      player.openShop("herb_exchange");
    } else if (option.getIndex() == 3) {
      player.getSkills().decantAllPotions();
    }
  }
}
