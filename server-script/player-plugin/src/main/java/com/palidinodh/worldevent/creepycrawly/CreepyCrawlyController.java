package com.palidinodh.worldevent.creepycrawly;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.controller.PController;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.graphic.Graphic.ProjectileSpeed;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PNumber;
import lombok.Getter;
import lombok.Setter;

public class CreepyCrawlyController extends PController {

  @Inject private Player player;
  @Getter private CreepyCrawlyMinigame minigame;
  @Getter @Setter private int roundsParticipated;
  private Consumable overloadPotion = buildOverloadPotion();
  private Consumable absorptionPotion = buildAbsorptionPotion();
  @Getter private int absorption;
  @Getter @Setter private int points;
  private boolean[] abilityDisabled = new boolean[CreepyCrawlyAbilityType.values().length];
  private int[] abilityCooldown = new int[CreepyCrawlyAbilityType.values().length];

  private static Consumable buildOverloadPotion() {
    var builder = Consumable.builder();
    builder
        .dose(ItemId.OVERLOAD_1)
        .dose(ItemId.OVERLOAD_2)
        .dose(ItemId.OVERLOAD_3)
        .dose(ItemId.OVERLOAD_4);
    builder.addItem(new Item(-1));
    builder.action(
        (p, c) -> {
          p.getSkills().setOverload(0.15, 5);
        });
    return builder.build();
  }

  public CreepyCrawlyController(CreepyCrawlyMinigame minigame) {
    this.minigame = minigame;
  }

  @Override
  public void startHook() {
    setItemStorageDisabledType(ItemStorageDisabledType.ALL_BUT_RUNE_POUCH);
    setTeleportsDisabled(true);
    setKeepItemsOnDeath(true);
    setExitTile(getFirstTile());
  }

  @Override
  public void stopHook() {
    minigame.removePlayer(player);
  }

  @Override
  public void tickHook() {
    for (var i = 0; i < abilityCooldown.length; i++) {
      if (abilityCooldown[i] == 0) {
        continue;
      }
      abilityCooldown[i]--;
      updateAbility(CreepyCrawlyAbilityType.values()[i]);
    }
  }

  @Override
  public void applyDeadCompleteEndHook() {
    minigame.removePlayer(player);
  }

  @Override
  public double damageInflictedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceTypes) {
    if (damage > 0 && minigame.getRecurrentDamageTimer() > 0) {
      opponent.getCombat().addHit(new Hit((int) (damage * 0.75)));
      player.getCombat().increaseDamageInflicted((int) (damage * 0.75));
    }
    return damage;
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (absorption > 0) {
      var absorbed = Math.min(damage, absorption);
      damage -= absorbed;
      absorption -= absorbed;
      if (absorption == 0) {
        player.getGameEncoder().sendMessage("<col=ff0000>You have run out of absorption.</col>");
      }
    }
    return damage;
  }

  @Override
  public MapItem addMapItemHook(MapItem mapItem) {
    if (mapItem.getId() == ItemId.VIAL) {
      return null;
    }
    if (!mapItem.getDef().getUntradable()
        && (isFood(mapItem.getId()) || isDrink(mapItem.getId()))) {
      mapItem.setAlwaysAppear();
    }
    return mapItem;
  }

  @Override
  public void logoutHook() {
    minigame.removePlayer(player);
  }

  @Override
  public Consumable getDrinkHook(int itemId, Consumable drink) {
    if (absorptionPotion.isDoseId(itemId)) {
      return absorptionPotion;
    }
    if (overloadPotion.isDoseId(itemId)) {
      return overloadPotion;
    }
    return drink;
  }

  @Override
  public int getExpMultiplier(int id) {
    var multiplier = super.getExpMultiplier(id);
    if (CreepyCrawlyMinigame.GAME_ITEMS.contains(player.getEquipment().getWeaponId())) {
      return multiplier / 2;
    }
    return multiplier;
  }

  @Override
  public boolean widgetHook(int option, int widgetId, int childId, int slot, int itemId) {
    if (widgetId != WidgetId.CREEPY_OVERLAY_1019) {
      return false;
    }
    if (roundsParticipated == 0) {
      return true;
    }
    var ability = CreepyCrawlyAbilityType.getByOptionId(childId);
    if (ability == null) {
      return true;
    }
    if (abilityCooldown[ability.ordinal()] > 0) {
      return true;
    }
    abilityCooldown[ability.ordinal()] = ability.getCooldown();
    switch (ability) {
      case REGENERATE:
        {
          for (var i = 0; i < Skills.SKILL_COUNT; i++) {
            player.getSkills().changeStat(i, (int) (player.getController().getLevelForXP(i) * 0.2));
          }
          player.setGraphic(84);
          break;
        }
      case SPECIAL_ATTACK_RESTORE:
        {
          player.getCombat().setSpecialAttackAmount(PCombat.MAX_SPECIAL_ATTACK);
          player.setGraphic(255);
          break;
        }
      case HURRICANE:
        {
          player.setAnimation(2890);
          player.setGraphic(483);
          for (var npc : getNearbyNpcs()) {
            if (!player.withinDistance(npc, 3)) {
              continue;
            }
            if (!player.getCombat().canAttackEntity(npc, false, HitStyleType.TYPELESS)) {
              continue;
            }
            npc.getCombat().addHitEvent(new HitEvent(2, new Hit(PRandom.randomI(5, 10))));
          }
          break;
        }
      case OMNIPOWER:
        {
          player.setAnimation(6696);
          player.setGraphic(1165);
          for (var npc : getNearbyNpcs()) {
            if (!player.withinDistance(npc, 6)) {
              continue;
            }
            if (!player.getCombat().canAttackEntity(npc, false, HitStyleType.TYPELESS)) {
              continue;
            }
            var projectile =
                Graphic.Projectile.builder()
                    .id(557)
                    .startTile(player)
                    .entity(npc)
                    .speed(new ProjectileSpeed(4, 71, 7 * 10))
                    .build();
            player.getController().sendMapProjectile(projectile);
            npc.setGraphic(558, 0, projectile.getContactDelay());
            npc.getCombat()
                .addHitEvent(
                    new HitEvent(projectile.getEventDelay(), new Hit(PRandom.randomI(10, 20))));
          }
          break;
        }
    }
    updateAbility(ability);
    return true;
  }

  private void updateAbility(CreepyCrawlyAbilityType ability) {
    var index = ability.ordinal();
    var cooldown = abilityCooldown[index];
    if ((cooldown == 0) == abilityDisabled[index]) {
      abilityDisabled[index] = cooldown != 0;
      player
          .getGameEncoder()
          .sendHideWidget(
              WidgetId.CREEPY_OVERLAY_1019, ability.getDisableId(), !abilityDisabled[index]);
    }
    if (cooldown % 4 == 0) {
      player
          .getGameEncoder()
          .sendWidgetText(
              WidgetId.CREEPY_OVERLAY_1019,
              ability.getTextId(),
              cooldown == 0 ? "" : Integer.toString(cooldown));
    }
  }

  private Consumable buildAbsorptionPotion() {
    var builder = Consumable.builder();
    builder
        .dose(ItemId.ABSORPTION_1)
        .dose(ItemId.ABSORPTION_2)
        .dose(ItemId.ABSORPTION_3)
        .dose(ItemId.ABSORPTION_4);
    builder.addItem(new Item(-1));
    builder.action(
        (p, c) -> {
          absorption += 50;
          p.getGameEncoder()
              .sendMessage(
                  "You have " + PNumber.formatNumber(absorption) + " points of absorption.");
        });
    return builder.build();
  }
}
