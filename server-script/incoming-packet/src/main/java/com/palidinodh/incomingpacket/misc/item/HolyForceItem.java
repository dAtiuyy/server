package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.HOLY_FORCE)
class HolyForceItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getCombat().getLegendsQuest() == 2
        && player.getX() >= 2387
        && player.getX() <= 2397
        && player.getY() >= 4673
        && player.getY() <= 4689
        && player.getWorld().getTargetNpc(player, NpcId.NEZIKCHENED_187) == null) {
      item.remove();
      player.getGameEncoder().sendMessage("You cast the spell, clensing the water.");
      player.getPrayer().changePoints(-99);
      Npc nezikchened =
          player
              .getController()
              .addNpc(
                  new NpcSpawn(new Tile(2396, 4678, player.getHeight()), NpcId.NEZIKCHENED_187));
      nezikchened.setForceMessage("Now I am revealed to you, so shall ye perish.");
      nezikchened.getCombat().setTarget(player);
    } else {
      player.getGameEncoder().sendMessage("Nothing interesting happens.");
    }
  }
}
