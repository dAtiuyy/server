var entries = new ArrayList();
var title = "";
var lines = new ArrayList();
var actions = new ArrayList();

title = "Select an Option";
lines.add("Create Raid");
actions.add("close|script");
lines.add("Join Raid");
actions.add("close|script");
var obj0 = new DialogueEntry();
entries.add(obj0);
obj0.setSelection(title, PString.toStringArray(lines, true), PString.toStringArray(actions, true));

title = "No-one may join the party after the raid begins.";
lines.add("Begin a Great Olm only raid.");
actions.add("close|script");
lines.add("Begin a normal raid.");
actions.add("close|script");
lines.add("Don't begin the raid yet.");
actions.add("close");
var obj1 = new DialogueEntry();
entries.add(obj1);
obj1.setSelection(title, PString.toStringArray(lines, true), PString.toStringArray(actions, true));

instance = new DialogueScript() {
    execute: function(player, index, childId, slot) {
        if (index == 0) {
            var clanChatUsername = player.getMessaging().getClanChatUsername();
            var playerInstance = player.getWorld().getPlayerRaidInstance(clanChatUsername, player.getController());
            if (slot == 0) {
                if (playerInstance != null) {
                    player.getGameEncoder().sendMessage("There is already a raid instance for this Clan Chat.");
                    return;
                } else if (!player.getMessaging().canClanChatEvent()) {
                    player.getGameEncoder().sendMessage("Your Clan Chat privledges aren't high enough to do that.");
                    return;
                }
                player.setController(PController.getController("chambersofxeric"));
                player.getController().startInstance();
                player.getMovement().teleport(player.getController().script("get_spawn_coords"));
                player.getWorld().putPlayerRaidInstance(clanChatUsername, player.getController());
            } else if (slot == 1) {
                if (playerInstance == null) {
                    player.getGameEncoder().sendMessage("Unable to locate a raid instance for this Clan Chat.");
                    return;
                } else if (playerInstance.script("get_is_loaded")) {
                    player.getGameEncoder().sendMessage("The raid has already started.");
                    return;
                }
                player.setController(PController.getController("chambersofxeric"));
                player.getController().joinInstance(playerInstance);
                player.getMovement().teleport(player.getController().script("get_spawn_coords"));
            }
        } else if (index == 1) {
            var clanChatUsername = player.getMessaging().getClanChatUsername();
            var playerInstance = player.getWorld().getPlayerRaidInstance(clanChatUsername, player.getController());
            if (slot == 0) {
                if (playerInstance == null) {
                    player.getGameEncoder().sendMessage("Unable to locate a raid instance for this Clan Chat.");
                    return;
                } else if (playerInstance.script("get_is_loaded")) {
                    player.getGameEncoder().sendMessage("The raid has already started.");
                    return;
                } else if (!player.getMessaging().canClanChatEvent()) {
                    player.getGameEncoder().sendMessage("Your Clan Chat privledges aren't high enough to do that.");
                    return;
                }
                playerInstance.script("set_load", true);
            } else if (slot == 1) {
                if (playerInstance == null) {
                    player.getGameEncoder().sendMessage("Unable to locate a raid instance for this Clan Chat.");
                    return;
                } else if (playerInstance.script("get_is_loaded")) {
                    player.getGameEncoder().sendMessage("The raid has already started.");
                    return;
                } else if (!player.getMessaging().canClanChatEvent()) {
                    player.getGameEncoder().sendMessage("Your Clan Chat privledges aren't high enough to do that.");
                    return;
                }
                playerInstance.script("set_load", false);
            }
        }
    },

    getDialogueEntries: function() {
        return entries;
    }
}
