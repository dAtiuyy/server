package com.palidinodh.maparea.kandarin.ourania.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class OuraniaCaveNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(3014, 5625), NpcId.ENIOLA));
    spawns.add(new NpcSpawn(4, new Tile(3015, 5603), NpcId.ZAMORAK_WARRIOR_85));
    spawns.add(new NpcSpawn(4, new Tile(3011, 5601), NpcId.CAVE_LIZARD_37));
    spawns.add(new NpcSpawn(4, new Tile(3017, 5595), NpcId.CAVE_LIZARD_37));
    spawns.add(new NpcSpawn(4, new Tile(3013, 5591), NpcId.ZAMORAK_RANGER_81));
    spawns.add(new NpcSpawn(4, new Tile(3012, 5596), NpcId.ZAMORAK_CRAFTER_19));
    spawns.add(new NpcSpawn(4, new Tile(3016, 5587), NpcId.ZAMORAK_CRAFTER_19));
    spawns.add(new NpcSpawn(4, new Tile(3012, 5585), NpcId.ZAMORAK_WARRIOR_84));
    spawns.add(new NpcSpawn(4, new Tile(3018, 5578), NpcId.ZAMORAK_MAGE_84));
    spawns.add(new NpcSpawn(4, new Tile(3011, 5573), NpcId.ZAMORAK_MAGE_84));
    spawns.add(new NpcSpawn(4, new Tile(3013, 5578), NpcId.CAVE_LIZARD_37));
    spawns.add(new NpcSpawn(4, new Tile(3017, 5573), NpcId.CAVE_LIZARD_37));
    spawns.add(new NpcSpawn(4, new Tile(3018, 5583), NpcId.CAVE_LIZARD_37));
    spawns.add(new NpcSpawn(4, new Tile(3027, 5572), NpcId.ZAMORAK_WARRIOR_84));
    spawns.add(new NpcSpawn(4, new Tile(3032, 5580), NpcId.ZAMORAK_MAGE_84));
    spawns.add(new NpcSpawn(4, new Tile(3034, 5573), NpcId.ZAMORAK_RANGER_82));
    spawns.add(new NpcSpawn(4, new Tile(3029, 5577), NpcId.CAVE_LIZARD_37));
    spawns.add(new NpcSpawn(4, new Tile(3031, 5572), NpcId.CAVE_LIZARD_37));
    spawns.add(new NpcSpawn(4, new Tile(3033, 5576), NpcId.CAVE_LIZARD_37));
    spawns.add(new NpcSpawn(4, new Tile(3043, 5580), NpcId.CAVE_LIZARD_37));
    spawns.add(new NpcSpawn(4, new Tile(3047, 5577), NpcId.ZAMORAK_RANGER_81));

    return spawns;
  }
}
