package com.palidinodh.rs.adaptive;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RsChatIcon {
  NONE(0, -1, false),
  SILVER_CROWN(1, 0, true),
  GOLD_CROWN(2, 1, true),
  IRONMAN(3, 2, false),
  ULTIMATE_IRONMAN(4, 3, false),
  HARDCORE_IRONMAN(5, 10, false),
  SUPPORT(6, 40, true),
  MODERATOR(7, 41, true),
  HEAD_MODERATOR(8, 42, true),
  COMMUNITY_MANAGER(9, 43, true),
  DONATOR_1(10, 44, false),
  DONATOR_2(11, 45, false),
  DONATOR_3(12, 46, false),
  DONATOR_4(13, 47, false),
  DONATOR_5(14, 48, false),
  DONATOR_6(15, 49, false),
  DONATOR_7(16, 50, false),
  YOUTUBE(17, 51, false),
  DISCORD(18, 52, false),
  GROUP_IRONMAN(19, 53, false),
  GROUP_HARDCORE_IRONMAN(20, 54, false),
  GROUP_ULTIMATE_IRONMAN(21, 55, false);

  private final int osrsRankIndex;
  private final int osrsSpriteIndex;
  private final boolean staff;

  public static boolean isStaffIcon(int index) {
    var values = values();
    if (index < 0 || index >= values.length) {
      return false;
    }
    return values[index].isStaff();
  }
}
