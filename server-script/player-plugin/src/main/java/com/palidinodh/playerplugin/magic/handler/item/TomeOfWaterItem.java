package com.palidinodh.playerplugin.magic.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.TOME_OF_WATER_EMPTY, ItemId.TOME_OF_WATER})
class TomeOfWaterItem implements ItemHandler {

  private static void empty(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "check":
        player.getGameEncoder().sendMessage("This book is empty.");
        break;
      case "pages":
        if (!player.getInventory().hasItem(ItemId.SOAKED_PAGE)) {
          player.getGameEncoder().sendMessage("You need a soaked page to do this.");
          break;
        }
        item.remove();
        player.getInventory().deleteItem(ItemId.SOAKED_PAGE);
        player.getInventory().addItem(ItemId.TOME_OF_WATER);
        break;
    }
  }

  private static void full(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "check":
        player.getGameEncoder().sendMessage("This book has unlimited charges.");
        break;
      case "pages":
        if (player.getInventory().getRemainingSlots() < 1) {
          player.getInventory().notEnoughSpace();
          break;
        }
        item.replace(new Item(ItemId.TOME_OF_WATER_EMPTY));
        player.getInventory().addItem(ItemId.SOAKED_PAGE);
        break;
    }
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (item.getId()) {
      case ItemId.TOME_OF_WATER_EMPTY:
        empty(player, option, item);
        break;
      case ItemId.TOME_OF_WATER:
        full(player, option, item);
        break;
    }
  }
}
