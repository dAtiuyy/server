package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.PMovement;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.playerplugin.agility.AgilityPlugin;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.MAX_CAPE_13280, ItemId.MAX_CAPE, ItemId.ARDOUGNE_MAX_CAPE})
class MaxCapeItem implements ItemHandler {

  private static void features(Player player) {
    player.openOptionsDialogue(
        new DialogueOption(
            "Spellbook.",
            (c, s) -> {
              player.openOldDialogue("spellbooks", 1);
            }),
        new DialogueOption(
            "Stamina Boost.",
            (c, s) -> {
              var agilityPlugin = player.getPlugin(AgilityPlugin.class);
              if (!agilityPlugin.canUseCapeStaminaBoost()) {
                player.getGameEncoder().sendMessage("You can't use this again today.");
                return;
              }
              agilityPlugin.updateCapeStaminaBoostDate();
              player.getMovement().setEnergy(PMovement.MAX_ENERGY);
              player.getSkills().setStaminaTime(100);
            }));
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getEquipment().meetsMaxCapeRequirements(true)) {
      return;
    }
    if (!player.getController().canTeleport(30, true)) {
      return;
    }
    switch (option.getText()) {
      case "teleports":
        player.openDialogue(new MaxCapeDialogue(player));
        break;
      case "features":
        features(player);
        break;
      case "warriors' guild":
        SpellTeleport.normalTeleport(player, new Tile(2845, 3544));
        break;
      case "crafting guild":
        SpellTeleport.normalTeleport(player, new Tile(2936, 3282));
        break;
      case "tele to poh":
        SpellTeleport.normalTeleport(player, player.getWidgetManager().getHomeTile());
        break;
      case "ardougne farm":
      case "farm teleport":
        SpellTeleport.normalTeleport(player, new Tile(2673, 3376));
        break;
      case "kandarin monastery":
      case "monastery teleport":
        SpellTeleport.normalTeleport(player, new Tile(2606, 3222));
        break;
      case "spellbook":
        player.openOldDialogue("spellbooks", 1);
        break;
    }
  }

  public static class MaxCapeDialogue extends OptionsDialogue {

    public MaxCapeDialogue(Player player) {
      action(
          (c, s) -> {
            Tile maxCapeTele = null;
            if (s == 0) {
              maxCapeTele = new Tile(World.DEFAULT_TILE);
            } else if (s == 1) {
              maxCapeTele = new Tile(1233, 3565);
            } else if (s == 2) {
              maxCapeTele = new Tile(1666, 10050);
            }
            if (!player.getController().canTeleport(true)) {
              return;
            }
            if (maxCapeTele == null) {
              return;
            }
            SpellTeleport.normalTeleport(player, maxCapeTele);
          });
      addOption("Edgeville");
      addOption("Chambers of Xeric");
      addOption("Catacombs of Kourend");
    }
  }
}
