package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.CUSTOM_BLACKLIST)
class BlacklistWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (player.isLocked()) {
      return;
    }
    if (player.getArea().inWilderness()) {
      player.getGameEncoder().sendMessage("You can't use the blacklist while in the Wilderness.");
      return;
    }
    if (childId >= 18 && childId <= 27) {
      player.getCombat().getEdgevilleBlacklist().removeName(childId - 18);
    } else if (childId == 28) {
      player
          .getGameEncoder()
          .sendEnterString(
              "Enter Name:",
              value -> {
                player.getCombat().getEdgevilleBlacklist().addName(value);
              });
    }
  }
}
