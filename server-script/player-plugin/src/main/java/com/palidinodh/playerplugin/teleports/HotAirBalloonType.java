package com.palidinodh.playerplugin.teleports;

import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.osrscore.model.tile.Tile;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum HotAirBalloonType {
  CASTLE_WARS(13, new Tile(2461, 3109), new Tile(2460, 3109), VarbitId.HOT_AIR_BALLOON_CASTLE_WARS),
  GRAND_TREE(14, new Tile(2481, 3457), new Tile(2480, 3458), VarbitId.HOT_AIR_BALLOON_GRAND_TREE),
  CRAFTING_GUILD(
      15, new Tile(2924, 3301), new Tile(2926, 3302), VarbitId.HOT_AIR_BALLOON_CRAFTING_GUILD),
  ENTRANA(16, new Tile(2806, 3355), new Tile(2809, 3356), VarbitId.HOT_AIR_BALLOON_ENTRANA),
  TAVERLEY(17, new Tile(2939, 3421), new Tile(2939, 3423), VarbitId.HOT_AIR_BALLOON_TAVERLEY),
  VARROCK(18, new Tile(3296, 3481), new Tile(3298, 3482), VarbitId.HOT_AIR_BALLOON_VARROCK);

  private final int widgetChildId;
  private final Tile mapObjectTile;
  private final Tile tile;
  private final int varbit;

  public static HotAirBalloonType getByWidgetChildId(int widgetChildId) {
    for (var type : values()) {
      if (widgetChildId != type.getWidgetChildId()) {
        continue;
      }
      return type;
    }
    return null;
  }

  public static HotAirBalloonType getByMapObjectTile(Tile tile) {
    for (var type : values()) {
      if (!tile.matchesTile(type.getMapObjectTile())) {
        continue;
      }
      return type;
    }
    return null;
  }
}
