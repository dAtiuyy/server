package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class DarkBeastCombat extends NpcCombat {

  private static final NpcCombatStyle SPECIAL_ATTACK =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MAGIC)
          .damage(NpcCombatDamage.maximum(25))
          .projectile(NpcCombatProjectile.builder().id(130).speedMinimumDistance(10).build())
          .animation(2731)
          .attackSpeed(2)
          .targetTileGraphic(new Graphic(101, 124))
          .specialAttack(NpcCombatTargetTile.builder().radius(1).radiusProjectiles(true).build())
          .build();

  @Inject private Npc npc;
  private boolean usingSpecialAttack;
  private int specialAttackCount;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .rareDropTableDenominator(42)
            .gemDropTableDenominator(42)
            .clue(ClueScrollType.ELITE, 1200)
            .clue(ClueScrollType.HARD, 128);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(512).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DARK_BOW)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_TALISMAN)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_DWARF_WEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORSTOL_SEED)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(10);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_ARROWTIPS, 5, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_DART_TIP, 5, 25)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_JAVELIN_HEADS, 1, 10)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_AXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_CHAINBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_MED_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_FULL_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 30)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_RUNE, 47)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_HARRALANDER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_IRIT_LEAF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AVANTOE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.KWUARM_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CADANTINE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPDRAGON_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_ORE_NOTED, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_BAR_NOTED, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_ORE_NOTED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ATTACK_POTION_3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHARK, 1, 2)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_SQ_SHIELD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_GUAM_LEAF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_MARRENTILL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TARROMIN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 64, 220)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BIG_BONES)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.DARK_BEAST_182);
    combat.hitpoints(NpcCombatHitpoints.total(220));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(140)
            .magicLevel(160)
            .defenceLevel(120)
            .bonus(BonusType.DEFENCE_STAB, 30)
            .bonus(BonusType.DEFENCE_SLASH, 40)
            .bonus(BonusType.DEFENCE_CRUSH, 100)
            .bonus(BonusType.DEFENCE_MAGIC, 90)
            .bonus(BonusType.DEFENCE_RANGED, 100)
            .build());
    combat.slayer(NpcCombatSlayer.builder().level(90).superiorId(NpcId.NIGHT_BEAST_374).build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.focus(NpcCombatFocus.builder().meleeUnlessUnreachable(true).build());
    combat.deathAnimation(2733).blockAnimation(2732);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(17));
    style.animation(2731).attackSpeed(4);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(8));
    style.animation(2731).attackSpeed(4);
    style.targetGraphic(new Graphic(131, 124));
    style.projectile(NpcCombatProjectile.id(130));
    combat.style(style.build());

    var combat2 = NpcCombatDefinition.builder();
    combat2.id(NpcId.DARK_BEAST_182_7250);
    combat2.hitpoints(NpcCombatHitpoints.total(220));
    combat2.stats(
        NpcCombatStats.builder()
            .attackLevel(140)
            .magicLevel(160)
            .defenceLevel(120)
            .bonus(BonusType.DEFENCE_STAB, 30)
            .bonus(BonusType.DEFENCE_SLASH, 40)
            .bonus(BonusType.DEFENCE_CRUSH, 100)
            .bonus(BonusType.DEFENCE_MAGIC, 90)
            .bonus(BonusType.DEFENCE_RANGED, 100)
            .build());
    combat2.slayer(NpcCombatSlayer.builder().level(90).superiorId(NpcId.NIGHT_BEAST_374).build());
    combat2.aggression(NpcCombatAggression.PLAYERS);
    combat2.focus(NpcCombatFocus.builder().meleeUnlessUnreachable(true).build());
    combat2.deathAnimation(2733).blockAnimation(2732);
    combat2.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(17));
    style.animation(2731).attackSpeed(4);
    combat2.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(8));
    style.animation(2731).attackSpeed(4);
    style.targetGraphic(new Graphic(101, 124));
    style.projectile(NpcCombatProjectile.id(130));
    combat2.style(style.build());

    var cursedCombat = NpcCombatDefinition.builder();
    cursedCombat.id(NpcId.CURSED_DARK_BEAST_374_16009);
    cursedCombat.hitpoints(NpcCombatHitpoints.builder().total(330).build());
    cursedCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(210)
            .magicLevel(240)
            .defenceLevel(180)
            .bonus(BonusType.DEFENCE_STAB, 45)
            .bonus(BonusType.DEFENCE_SLASH, 60)
            .bonus(BonusType.DEFENCE_CRUSH, 150)
            .bonus(BonusType.DEFENCE_MAGIC, 135)
            .bonus(BonusType.DEFENCE_RANGED, 150)
            .build());
    cursedCombat.slayer(NpcCombatSlayer.builder().level(90).build());
    cursedCombat.focus(NpcCombatFocus.builder().meleeUnlessUnreachable(true).build());
    cursedCombat.deathAnimation(2733).blockAnimation(2732);
    cursedCombat.drop(drop.rolls(2).build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(32));
    style.animation(2731).attackSpeed(4);
    cursedCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(25));
    style.animation(2731).attackSpeed(4);
    style.targetGraphic(new Graphic(131, 124));
    style.projectile(NpcCombatProjectile.id(130));
    cursedCombat.style(style.build());

    var superiorCombat = NpcCombatDefinition.builder();
    superiorCombat.id(NpcId.NIGHT_BEAST_374);
    superiorCombat.hitpoints(
        NpcCombatHitpoints.builder().total(550).barType(HitpointsBarType.GREEN_RED_60).build());
    superiorCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(270)
            .magicLevel(300)
            .defenceLevel(220)
            .bonus(BonusType.DEFENCE_STAB, 75)
            .bonus(BonusType.DEFENCE_SLASH, 80)
            .bonus(BonusType.DEFENCE_CRUSH, 200)
            .bonus(BonusType.DEFENCE_MAGIC, 190)
            .bonus(BonusType.DEFENCE_RANGED, 200)
            .build());
    superiorCombat.slayer(NpcCombatSlayer.builder().level(90).experience(6462).build());
    superiorCombat.aggression(NpcCombatAggression.PLAYERS);
    superiorCombat.focus(NpcCombatFocus.builder().meleeUnlessUnreachable(true).build());
    superiorCombat.killCount(
        NpcCombatKillCount.builder().asName("Superior Slayer Creature").build());
    superiorCombat.deathAnimation(2733).blockAnimation(2732);
    superiorCombat.drop(drop.rolls(3).build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(32));
    style.animation(2731).attackSpeed(4);
    superiorCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(25));
    style.animation(2731).attackSpeed(4);
    style.targetGraphic(new Graphic(131, 124));
    style.projectile(NpcCombatProjectile.id(130));
    superiorCombat.style(style.build());

    return Arrays.asList(
        combat.build(), combat2.build(), cursedCombat.build(), superiorCombat.build());
  }

  @Override
  public void spawnHook() {
    usingSpecialAttack = false;
    specialAttackCount = 0;
  }

  @Override
  public void tickStartHook() {
    if ((npc.getId() == NpcId.NIGHT_BEAST_374 || npc.getId() == NpcId.CURSED_DARK_BEAST_374_16009)
        && isAttacking()
        && !isHitDelayed()
        && !usingSpecialAttack
        && PRandom.randomE(20) == 0) {
      usingSpecialAttack = true;
    }
  }

  @Override
  public NpcCombatStyle attackTickCombatStyleHook(NpcCombatStyle combatStyle, Entity opponent) {
    return usingSpecialAttack ? SPECIAL_ATTACK : combatStyle;
  }

  @Override
  public void applyAttackEndHook(
      NpcCombatStyle combatStyle, Entity opponent, int applyAttackLoopCount, HitEvent hitEvent) {
    if (!usingSpecialAttack) {
      return;
    }
    if (++specialAttackCount >= 3) {
      usingSpecialAttack = false;
      specialAttackCount = 0;
    }
  }

  @Override
  public double accuracyHook(NpcCombatStyle combatStyle, double accuracy) {
    return usingSpecialAttack ? Integer.MAX_VALUE : accuracy;
  }

  @Override
  public void attackedActionsOpponentHook(Entity opponent) {
    if (!opponent.isPlayer() || npc.getId() != NpcId.CURSED_DARK_BEAST_374_16009) {
      return;
    }
    opponent.asPlayer().getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
  }

  @Override
  public NpcCombatDropTable deathDropItemsTableHook(
      Player player, int dropRateDivider, int roll, NpcCombatDropTable table) {
    if (npc.getId() == NpcId.CURSED_DARK_BEAST_374_16009) {
      if (!player.getPlugin(SlayerPlugin.class).isWildernessTask(npc)) {
        player.getGameEncoder().sendMessage("Without an assigned task, the loot turns to dust...");
        return null;
      }
    }
    return table;
  }
}
