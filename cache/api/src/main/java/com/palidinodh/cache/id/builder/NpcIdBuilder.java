package com.palidinodh.cache.id.builder;

import com.palidinodh.cache.definition.osrs.NpcDefinition;
import com.palidinodh.cache.id.NameIdLookup;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.NullNpcId;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class NpcIdBuilder {

  public static void build() {
    var namesUsed = new ArrayList<String>();
    var nullFields = new LinkedHashMap<Integer, String>();
    var nonNullFields = new LinkedHashMap<Integer, String>();
    for (var def : NpcDefinition.getDefinitions()) {
      if (def == null) {
        continue;
      }
      var npcId = def.getId();
      if (def.getName() == null || def.getName().equalsIgnoreCase("null")) {
        nullFields.put(npcId, "NULL_" + npcId);
        continue;
      }
      var name = def.getName().toLowerCase();
      while (name.indexOf("<") != -1 && name.indexOf(">") != -1) {
        int start = name.indexOf("<");
        int end = name.indexOf(">", start);
        name = name.substring(0, start) + name.substring(end + 1);
      }
      name = name.replace("+", "_plus").replace("(-)", "_minus");
      name = name.replace("(", "_").replace(' ', '_').replace('-', '_');
      name =
          name.replace("_____", "_")
              .replace("____", "_")
              .replace("___", "_")
              .replace("__", "_")
              .replace("__", "_");
      while (name.endsWith("_")) {
        name = name.substring(0, name.length() - 1);
      }
      name = IdBuilder.cleanName(name).toUpperCase();
      if (name.matches("[0-9]_*[a-zA-z0-9]*")) {
        name = "_" + name;
      }
      if (name.length() == 0) {
        continue;
      }
      if (def.getCombatLevel() > 0) {
        name = name + "_" + def.getCombatLevel();
      }
      if (npcId >= 16000 /* customs */ || name.equals("fishing spot") || namesUsed.contains(name)) {
        name = name + "_" + npcId;
      }
      nonNullFields.put(npcId, name);
      namesUsed.add(name);
    }
    var lookup = new NameIdLookup(NpcId.class);
    lookup.generateClass(nonNullFields, IdBuilder.TO_DIR);
    lookup = new NameIdLookup(NullNpcId.class);
    lookup.generateClass(nullFields, IdBuilder.TO_DIR);
  }
}
