package com.palidinodh.cache.id.builder;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NameIdLookup;
import com.palidinodh.cache.id.NullItemId;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class ItemIdBuilder {

  public static void build() {
    var namesUsed = new ArrayList<String>();
    var nullFields = new LinkedHashMap<Integer, String>();
    var nonNullFields = new LinkedHashMap<Integer, String>();
    for (var def : ItemDefinition.getDefinitions()) {
      if (def == null) {
        continue;
      }
      var itemId = def.getId();
      var name = def.getName();
      if (name == null || name.contains("null") || name.contains("Null")) {
        nullFields.put(itemId, "NULL_" + itemId);
        continue;
      }
      name = name.replace("+", "_plus").replace("(-)", "_minus");
      name = name.replace("(", "_").replace(" ", "_").replace("-", "_");
      while (name.endsWith("_")) {
        name = name.substring(0, name.length() - 1);
      }
      name = name.replace("/", "_");
      name = IdBuilder.cleanName(name).toUpperCase();
      if (name.matches("[0-9]_*[a-zA-z0-9]*")) {
        name = "_" + name;
      }
      while (name.contains("__")) {
        name = name.replace("__", "_");
      }
      if (name.length() == 0) {
        continue;
      }
      // Append id to less used items that share the same name as an important item but have a
      // higher id. Also verify the id is still the name it's expected to be first.
      switch (itemId) {
        case 617:
          if (!name.equals("COINS")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 2513:
          if (!name.equals("DRAGON_CHAINBODY")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 2722:
          if (!name.equals("CLUE_SCROLL_HARD")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 4083:
          if (!name.equals("SLED")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 4561:
          if (!name.equals("PURPLE_SWEETS")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 6660:
          if (!name.equals("FISHING_EXPLOSIVE")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 9976:
          if (!name.equals("CHINCHOMPA")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 9977:
          if (!name.equals("RED_CHINCHOMPA")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 11068:
          if (!name.equals("GOLD_BRACELET")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 11071:
          if (!name.equals("SAPPHIRE_BRACELET")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 11277:
          if (!name.equals("CAVALIER_MASK")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 11278:
          if (!name.equals("BERET_MASK")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 13280:
        case 13282:
          if (!name.startsWith("MAX_CAPE")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 19492:
          if (!name.equals("ZENYTE_BRACELET")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 21284:
          if (!name.equals("INFERNAL_MAX_CAPE")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 21913:
          if (!name.equals("MYTHICAL_CAPE")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 22405:
          if (!name.equals("VIAL_OF_BLOOD")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 21841:
        case 21842:
        case 21843:
        case 21844:
        case 21845:
        case 21846:
          if (!name.startsWith("SNOW_IMP_COSTUME")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 24300:
        case 24301:
        case 24302:
        case 24303:
        case 24304:
          if (!name.startsWith("SPOOKY")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 21719:
          if (!name.equals("JONAS_MASK")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 23804:
        case 23867:
          if (!name.equals("CRYSTAL_DUST")) {
            break;
          }
          name = name + "_" + itemId;
          break;
        case 11015:
        case 11016:
        case 11017:
        case 11018:
          if (!name.startsWith("CHICKEN_")) {
            break;
          }
          name = name + "_" + itemId;
          break;
      }
      if (itemId >= 32250 /* customs */ || namesUsed.contains(name)) {
        name = name + "_" + itemId;
      }
      nonNullFields.put(itemId, name);
      namesUsed.add(name);
    }
    var lookup = new NameIdLookup(ItemId.class);
    lookup.generateClass(nonNullFields, IdBuilder.TO_DIR);
    lookup = new NameIdLookup(NullItemId.class);
    lookup.generateClass(nullFields, IdBuilder.TO_DIR);
  }
}
