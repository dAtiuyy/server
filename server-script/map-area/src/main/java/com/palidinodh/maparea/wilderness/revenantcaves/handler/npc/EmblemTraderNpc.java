package com.palidinodh.maparea.wilderness.revenantcaves.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.EMBLEM_TRADER_7942)
class EmblemTraderNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    if (option.getIndex() == 0) {
      player.openOldDialogue("emblemtrader", 1);
    } else if (option.getIndex() == 2) {
      player.getGameEncoder().sendMessage("You can't trade him here.");
    } else if (option.getIndex() == 3) {
      boolean show = !player.getCombat().showKDR();
      player.getCombat().setShowKDR(show);
      if (show) {
        player.getGameEncoder().sendMessage("Now displaying KDR.");
      } else {
        player.getGameEncoder().sendMessage("Now hiding KDR.");
      }
    } else if (option.getIndex() == 4) {
      player.getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
    }
  }
}
