package com.palidinodh.command.seniormod;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.util.PNumber;

@ReferenceName("hp")
class HitpointsCommand implements CommandHandler, CommandHandler.SeniorModeratorRank {

  @Override
  public String getExample(String name) {
    return "amount";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var amount = PNumber.getNumber(message);
    if (amount < 1) {
      return;
    }
    player.getCombat().setHitpoints(amount);
    player.getGameEncoder().sendSkillLevel(Skills.HITPOINTS);
    player
        .getGameEncoder()
        .sendMessage("<col=ff0000> You have set your hitpoints to " + amount + "!");
    DiscordBot.sendMessage(
        DiscordChannel.MODERATION_LOG,
        player.getUsername() + " has set their hp to " + amount + ".");
  }
}
