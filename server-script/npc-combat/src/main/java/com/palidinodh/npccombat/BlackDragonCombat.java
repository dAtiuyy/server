package com.palidinodh.npccombat;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import java.util.Arrays;
import java.util.List;

class BlackDragonCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .rareDropTableDenominator(64)
            .gemDropTableDenominator(42)
            .clue(ClueScrollType.ELITE, 500)
            .clue(ClueScrollType.HARD, 128);
    var dropTable =
        NpcCombatDropTable.builder().probabilityDenominator(10000).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRACONIC_VISAGE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(35);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ENSOULED_DRAGON_HEAD_13511)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_PLATEBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_LONGSWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_JAVELIN_HEADS, 10)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_AXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_KITESHIELD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_KNIFE, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_RUNE, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHOCOLATE_CAKE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_DART_P, 16)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_JAVELIN, 30)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_RUNE, 50)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_BONES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_DRAGONHIDE)));
    drop.table(dropTable.build());

    var combat227 = NpcCombatDefinition.builder();
    combat227.id(NpcId.BLACK_DRAGON_227);
    combat227.hitpoints(NpcCombatHitpoints.total(190));
    combat227.stats(
        NpcCombatStats.builder()
            .attackLevel(200)
            .magicLevel(100)
            .defenceLevel(200)
            .bonus(BonusType.DEFENCE_STAB, 50)
            .bonus(BonusType.DEFENCE_SLASH, 70)
            .bonus(BonusType.DEFENCE_CRUSH, 70)
            .bonus(BonusType.DEFENCE_MAGIC, 60)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    combat227.aggression(NpcCombatAggression.PLAYERS);
    combat227.killCount(NpcCombatKillCount.builder().asName("Chromatic dragon").build());
    combat227.deathAnimation(92).blockAnimation(89);
    combat227.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(21));
    style.animation(80).attackSpeed(6);
    combat227.type(NpcCombatType.DRAGON);
    combat227.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.DRAGONFIRE);
    style.damage(NpcCombatDamage.maximum(50));
    style.animation(81).attackSpeed(6).attackRange(1);
    style.castGraphic(new Graphic(1, 100));
    combat227.style(style.build());

    var combat247 = NpcCombatDefinition.builder();
    combat247
        .id(NpcId.BLACK_DRAGON_247)
        .id(NpcId.BLACK_DRAGON_247_7862)
        .id(NpcId.BLACK_DRAGON_247_7863);
    combat247.hitpoints(NpcCombatHitpoints.total(250));
    combat247.stats(
        NpcCombatStats.builder()
            .attackLevel(200)
            .magicLevel(150)
            .defenceLevel(200)
            .bonus(BonusType.DEFENCE_STAB, 50)
            .bonus(BonusType.DEFENCE_SLASH, 70)
            .bonus(BonusType.DEFENCE_CRUSH, 70)
            .bonus(BonusType.DEFENCE_MAGIC, 60)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    combat247.aggression(NpcCombatAggression.PLAYERS);
    combat247.killCount(NpcCombatKillCount.builder().asName("Chromatic dragon").build());
    combat247.deathAnimation(92).blockAnimation(89);
    combat247.type(NpcCombatType.DRAGON);
    combat247.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(21));
    style.animation(80).attackSpeed(6);
    combat247.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.DRAGONFIRE);
    style.damage(NpcCombatDamage.maximum(50));
    style.animation(81).attackSpeed(6).attackRange(1);
    style.castGraphic(new Graphic(1, 100));
    combat247.style(style.build());

    return Arrays.asList(combat227.build(), combat247.build());
  }
}
