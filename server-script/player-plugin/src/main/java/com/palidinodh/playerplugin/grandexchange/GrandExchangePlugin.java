package com.palidinodh.playerplugin.grandexchange;

import com.google.inject.Inject;
import com.palidinodh.cache.clientscript2.ScrollbarSizeCs2;
import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.util.RequestManager;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeHistoryItem;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeHistoryType;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeItem;
import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeUser;
import com.palidinodh.util.PCollection;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PString;
import com.palidinodh.util.PTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
public class GrandExchangePlugin implements PlayerPlugin {

  public static final int WIDGET_ITEM_1 = 51;
  public static final int WIDGET_ITEM_CHILDREN = 13;
  public static final int MAX_OFFERS = 16;

  @Inject private transient Player player;
  private transient List<GrandExchangeHistoryItem> searchItems = new ArrayList<>();
  private transient boolean update;
  @Setter private transient long refresh;
  private transient GrandExchangeOffers offers;
  private List<GrandExchangeItem> items = new ArrayList<>();

  private List<GrandExchangeItem> historyItems = new ArrayList<>();

  @Override
  public void loadLegacy(Map<String, Object> map) {
    if (map.containsKey("grandExchange.historyItems")) {
      var hItems = (GrandExchangeItem[]) map.get("grandExchange.historyItems");
      if (hItems != null) {
        for (var hItem : hItems) {
          if (hItem == null || hItem.getId() == -1 || hItem.getAmount() <= 0) {
            continue;
          }
          historyItems.add(hItem);
        }
      }
    }
  }

  @SuppressWarnings("rawtypes")
  @Override
  public Object script(String name, Object... args) {
    if (name.equals("grand_exchange_refresh")) {
      refresh((long) args[0], PCollection.castList((List) args[1], GrandExchangeItem.class));
    }
    if (name.equals("grand_exchange_refresh_history")) {
      refreshHistory(PCollection.castList((List) args[0], GrandExchangeHistoryItem.class));
    }
    if (name.equals("grand_exchange_item_information")) {
      itemInformation(
          (int) args[0], (int) args[1], (int) args[2], (int) args[3], (int) args[4], (int) args[5]);
    }
    return null;
  }

  @Override
  public void login() {
    historyItems.removeIf(i -> i == null || i.getId() == -1);
    offers = new GrandExchangeOffers(player, this);
    update();
  }

  @Override
  public void tick() {
    if (update) {
      update = false;
      sendItems();
      RequestManager.getInstance().addGERefresh(player, refresh);
    }
  }

  public void sendItems() {
    var widgetId = WidgetId.GRAND_EXCHANGE_1024;
    var slot = 0;
    player
        .getGameEncoder()
        .sendWidgetText(widgetId, 20, "My Offers (" + items.size() + "/" + MAX_OFFERS + ")");
    for (var item : items) {
      var offset = WIDGET_ITEM_1 + (slot++ * WIDGET_ITEM_CHILDREN);
      var barColor = 0xd88020;
      if (item.isAborted()) {
        barColor = 0x8f0000;
      }
      if (item.getRemainingAmount() == 0) {
        barColor = 0x055f00;
      }
      player.getGameEncoder().sendHideWidget(widgetId, offset, false);
      player.getGameEncoder().sendWidgetItemModel(widgetId, offset + 2, item.getId(), 1);
      player
          .getGameEncoder()
          .sendWidgetText(widgetId, offset + 3, PNumber.abbreviateNumber(item.getPrice()));
      player
          .getGameEncoder()
          .sendWidgetText(widgetId, offset + 4, ItemDefinition.getName(item.getId()));
      player
          .getGameEncoder()
          .sendClientScript(
              ScriptId.WIDGET_SIZE_8195,
              widgetId << 16 | offset + 11,
              item.getCompletedPercent() / 100 * 232,
              18,
              barColor);
      player
          .getGameEncoder()
          .sendWidgetText(
              widgetId,
              offset + 12,
              item.getExchangedAmount()
                  + " of "
                  + item.getAmount()
                  + " "
                  + (item.isStateBuying() ? "bought" : "sold"));
    }
    for (var i = slot; i < 16; i++) {
      var offset = WIDGET_ITEM_1 + (i * WIDGET_ITEM_CHILDREN);
      player.getGameEncoder().sendHideWidget(widgetId, offset, true);
    }
    player
        .getGameEncoder()
        .sendClientScriptData(
            ScrollbarSizeCs2.builder()
                .root(widgetId)
                .container(50)
                .scrollbar(49)
                .height(slot * 44 + 4)
                .build());
  }

  public void update() {
    update = true;
    refresh = PTime.currentTimeMillis();
  }

  public void viewHistory() {
    searchItems.clear();
    for (var item : historyItems) {
      searchItems.add(
          new GrandExchangeHistoryItem(
              item,
              item.isStateBuying()
                  ? GrandExchangeHistoryType.BOUGHT
                  : GrandExchangeHistoryType.SOLD,
              ""));
    }
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.GRAND_EXCHANGE_HISTORY_1027);
  }

  private void refresh(long time, List<GrandExchangeItem> list) {
    if (time != 0 && time != refresh) {
      return;
    }
    if (time == refresh) {
      refresh = 0;
    }
    if (refresh != 0) {
      update();
    }
    boolean itemsChanged = items.size() != list.size();
    for (int i = 0; i < list.size(); i++) {
      GrandExchangeItem currentItem = i < items.size() ? items.get(i) : null;
      GrandExchangeItem newItem = list.get(i);
      if (currentItem == null || currentItem != null && !currentItem.matches(newItem)) {
        itemsChanged = true;
        if (newItem.isAborted() && newItem.getId() != -1 && newItem.getExchangedAmount() > 0) {
          historyItems.add(0, newItem);
          if (historyItems.size() > GrandExchangeUser.HISTORY_SIZE) {
            historyItems.remove(historyItems.size() - 1);
          }
        }
      }
    }
    items.clear();
    items.addAll(list);
    if (player.getWidgetManager().getInteractiveOverlay() == WidgetId.GRAND_EXCHANGE_1024) {
      sendItems();
    }
    if (itemsChanged) {
      player.getGameEncoder().sendMessage("Your Grand Exchange offers have changed!");
    }
  }

  private void refreshHistory(List<GrandExchangeHistoryItem> list) {
    if (player.getWidgetManager().getInteractiveOverlay() != WidgetId.GRAND_EXCHANGE_1024) {
      return;
    }
    searchItems.clear();
    searchItems.addAll(list);
    var specifiedUsername = player.getAttribute("exchange_username") != "";
    searchItems.forEach(
        i -> i.setUsername((specifiedUsername ? "<t>" : "") + PString.formatName(i.getUsername())));
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.GRAND_EXCHANGE_HISTORY_1027);
  }

  private void itemInformation(
      int itemId, int totalBuy, int averageBuy, int totalSell, int averageSell, int cheapestSell) {
    if (player.getWidgetManager().getInteractiveOverlay() != WidgetId.GRAND_EXCHANGE_OFFER_1026) {
      return;
    }
    if (itemId != player.getAttributeInt("exchange_item_id") - 1) {
      return;
    }
    player
        .getGameEncoder()
        .sendWidgetText(
            WidgetId.GRAND_EXCHANGE_OFFER_1026,
            50,
            "Cheapest for sale: "
                + (cheapestSell > 0 ? PNumber.abbreviateNumber(PNumber.round(cheapestSell)) : "N/A")
                + "<br>"
                + PNumber.formatNumber(totalBuy)
                + " buy offers averaged: "
                + PNumber.abbreviateNumber(averageBuy)
                + "<br>"
                + PNumber.formatNumber(totalSell)
                + " sell offers averaged: "
                + PNumber.abbreviateNumber(averageSell));
  }
}
