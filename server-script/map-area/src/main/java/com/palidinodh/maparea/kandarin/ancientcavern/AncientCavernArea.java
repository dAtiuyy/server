package com.palidinodh.maparea.kandarin.ancientcavern;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(6995)
public class AncientCavernArea extends Area {}
