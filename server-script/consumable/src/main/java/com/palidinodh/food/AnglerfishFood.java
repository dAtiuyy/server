package com.palidinodh.food;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.ANGLERFISH, ItemId.BLIGHTED_ANGLERFISH})
class AnglerfishFood implements Consumable.BuildType {

  private static int getAdditional(int level) {
    if (level <= 24) {
      return 2;
    }
    if (level <= 49) {
      return 4;
    }
    if (level <= 74) {
      return 6;
    }
    if (level <= 92) {
      return 8;
    }
    return 13;
  }

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.FOOD);
    builder.action(
        (p, c) -> {
          var hitpointsLevel = p.getController().getLevelForXP(Skills.HITPOINTS);
          var heal = hitpointsLevel / 10 + getAdditional(hitpointsLevel);
          var allowedOver = heal;
          if (p.getCombat().isPlayerInvolved()) {
            allowedOver = 0;
          }
          p.getCombat().changeHitpoints(heal, allowedOver);
        });
    return builder;
  }
}
