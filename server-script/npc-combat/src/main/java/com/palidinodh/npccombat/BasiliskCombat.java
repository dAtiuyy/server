package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import java.util.Arrays;
import java.util.List;

class BasiliskCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().gemDropTableDenominator(25);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(2000);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BASILISK_HEAD)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(512);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_HAT_LIGHT)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_DAGGER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_DWARF_WEED)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_SPEAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_AXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_KITESHIELD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_FULL_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_ORE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATER_RUNE, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_GUAM_LEAF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_MARRENTILL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TARROMIN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_HARRALANDER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_IRIT_LEAF)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.BASILISK_61);
    combat.hitpoints(NpcCombatHitpoints.total(75));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(30)
            .defenceLevel(75)
            .bonus(BonusType.DEFENCE_STAB, 20)
            .bonus(BonusType.DEFENCE_SLASH, 20)
            .bonus(BonusType.DEFENCE_MAGIC, 20)
            .build());
    combat.slayer(NpcCombatSlayer.builder().level(40).build());
    combat.deathAnimation(1548).blockAnimation(1547);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(5));
    style.animation(1546).attackSpeed(4);
    combat.style(style.build());

    var knightDrop =
        NpcCombatDrop.builder().gemDropTableDenominator(6).clue(ClueScrollType.HARD, 128);
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(5000).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BASILISK_JAW)));
    knightDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_HAT_LIGHT)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_MED_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_SNAPDRAGON_NOTED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL_NOTED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANTITE_ORE_NOTED, 1, 2)));
    knightDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_SCIMITAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_AXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_SPEAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_DAGGER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 8, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED_NOTED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE_NOTED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 500, 2500)));
    knightDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_PLATELEGS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_KITESHIELD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NATURE_RUNE, 15, 30)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 10, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 20, 30)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ASTRAL_RUNE, 16, 35)));
    knightDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BIG_BONES)));
    knightDrop.table(dropTable.build());

    var knightCombat = NpcCombatDefinition.builder();
    knightCombat.id(NpcId.BASILISK_KNIGHT_204);
    knightCombat.hitpoints(NpcCombatHitpoints.total(300));
    knightCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(186)
            .magicLevel(186)
            .defenceLevel(186)
            .bonus(BonusType.DEFENCE_STAB, 30)
            .bonus(BonusType.DEFENCE_SLASH, 30)
            .bonus(BonusType.DEFENCE_MAGIC, 30)
            .build());
    knightCombat.slayer(
        NpcCombatSlayer.builder().level(60).superiorId(NpcId.BASILISK_SENTINEL_358).build());
    knightCombat.aggression(NpcCombatAggression.PLAYERS);
    knightCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    knightCombat.deathAnimation(8501);
    knightCombat.drop(knightDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(20));
    style.animation(8499).attackSpeed(4);
    knightCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(20));
    style.animation(8500).attackSpeed(4);
    style.targetGraphic(new Graphic(1736, 124));
    style.projectile(NpcCombatProjectile.id(1735));
    knightCombat.style(style.build());

    var cursedKnightCombat = NpcCombatDefinition.builder();
    cursedKnightCombat.id(NpcId.CURSED_BASILISK_KNIGHT_358_16059);
    cursedKnightCombat.hitpoints(NpcCombatHitpoints.builder().total(520).build());
    cursedKnightCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(274)
            .magicLevel(274)
            .defenceLevel(274)
            .bonus(BonusType.MELEE_ATTACK, 12)
            .bonus(BonusType.DEFENCE_STAB, 50)
            .bonus(BonusType.DEFENCE_SLASH, 50)
            .bonus(BonusType.DEFENCE_CRUSH, 10)
            .bonus(BonusType.DEFENCE_MAGIC, 50)
            .bonus(BonusType.DEFENCE_RANGED, 10)
            .build());
    cursedKnightCombat.slayer(NpcCombatSlayer.builder().level(60).build());
    cursedKnightCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    cursedKnightCombat.deathAnimation(8501);
    cursedKnightCombat.drop(knightDrop.rolls(2).build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(28));
    style.animation(8499).attackSpeed(4);
    cursedKnightCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(28));
    style.animation(8500).attackSpeed(4);
    style.targetGraphic(new Graphic(1736, 124));
    style.projectile(NpcCombatProjectile.id(1735));
    cursedKnightCombat.style(style.build());

    var superiorKnightCombat = NpcCombatDefinition.builder();
    superiorKnightCombat.id(NpcId.BASILISK_SENTINEL_358);
    superiorKnightCombat.hitpoints(
        NpcCombatHitpoints.builder().total(520).barType(HitpointsBarType.GREEN_RED_60).build());
    superiorKnightCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(274)
            .magicLevel(274)
            .defenceLevel(274)
            .bonus(BonusType.MELEE_ATTACK, 12)
            .bonus(BonusType.DEFENCE_STAB, 50)
            .bonus(BonusType.DEFENCE_SLASH, 50)
            .bonus(BonusType.DEFENCE_CRUSH, 10)
            .bonus(BonusType.DEFENCE_MAGIC, 50)
            .bonus(BonusType.DEFENCE_RANGED, 10)
            .build());
    superiorKnightCombat.slayer(NpcCombatSlayer.builder().level(60).experience(5590).build());
    superiorKnightCombat.aggression(NpcCombatAggression.PLAYERS);
    superiorKnightCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    superiorKnightCombat.killCount(
        NpcCombatKillCount.builder().asName("Superior Slayer Creature").build());
    superiorKnightCombat.deathAnimation(8501);
    superiorKnightCombat.drop(drop.rolls(3).build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(28));
    style.animation(8499).attackSpeed(4);
    superiorKnightCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(28));
    style.animation(8500).attackSpeed(4);
    style.targetGraphic(new Graphic(1736, 124));
    style.projectile(NpcCombatProjectile.id(1735));
    superiorKnightCombat.style(style.build());

    return Arrays.asList(
        combat.build(),
        knightCombat.build(),
        cursedKnightCombat.build(),
        superiorKnightCombat.build());
  }

  @Override
  public void attackedActionsOpponentHook(Entity opponent) {
    if (!opponent.isPlayer() || npc.getId() != NpcId.CURSED_BASILISK_KNIGHT_358_16059) {
      return;
    }
    opponent.asPlayer().getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
  }

  @Override
  public NpcCombatDropTable deathDropItemsTableHook(
      Player player, int dropRateDivider, int roll, NpcCombatDropTable table) {
    if (npc.getId() == NpcId.CURSED_BASILISK_KNIGHT_358_16059) {
      if (!player.getPlugin(SlayerPlugin.class).isWildernessTask(npc)) {
        player.getGameEncoder().sendMessage("Without an assigned task, the loot turns to dust...");
        return null;
      }
    }
    return table;
  }

  @Override
  public int dropTableDenominatorHook(
      Player player, int dropRateDivider, int roll, NpcCombatDropTable table, int denominator) {
    if (denominator > 100 && player.getPlugin(SlayerPlugin.class).isAnyTask(npc)) {
      denominator /= 5;
    }
    return denominator;
  }
}
