package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.MAGIC_SHORTBOW, ItemId.MAGIC_SHORTBOW_20558, ItemId.MAGIC_SHORTBOW_I})
class MagicShortbowSpecialAttack extends SpecialAttack {

  MagicShortbowSpecialAttack() {
    var entry = Entry.builder();
    entry.itemId(ItemId.MAGIC_SHORTBOW);
    entry.itemId(ItemId.MAGIC_SHORTBOW_20558);
    entry.drain(55);
    entry.animation(1074);
    entry.castGraphic(new Graphic(256, 92, 30));
    entry.projectileId(249);
    entry.castSound(new Sound(2545));
    entry.doubleHit(true);
    addEntry(entry);

    entry = Entry.builder();
    entry.itemId(ItemId.MAGIC_SHORTBOW_I);
    entry.drain(50);
    entry.animation(1074);
    entry.castGraphic(new Graphic(256, 92, 30));
    entry.projectileId(249);
    entry.castSound(new Sound(2545));
    entry.doubleHit(true);
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (hooks.getApplyAttackLoopCount() > 0) {
      return;
    }
    var player = hooks.getPlayer();
    var distance = hooks.getPlayer().getDistance(hooks.getOpponent());
    var projectile = hooks.getProjectile();
    var projectileSpeed = projectile.getProjectileSpeed();
    projectileSpeed.setClientDelay(50);
    projectileSpeed.setClientSpeed(distance * 3);
    player.getController().sendMapProjectile(projectile);
    projectileSpeed.setClientDelay(20);
    projectileSpeed.setClientSpeed(projectileSpeed.getClientSpeed() + 10);
    player.getController().sendMapProjectile(projectile);
    hooks.setProjectile(null);
  }
}
