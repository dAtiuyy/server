package com.palidinodh.npccombat;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import java.util.Arrays;
import java.util.List;

class BrutalBlackDragonCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .rareDropTableDenominator(64)
            .gemDropTableDenominator(42)
            .clue(ClueScrollType.ELITE, 250)
            .clue(ClueScrollType.HARD, 128);
    var dropTable =
        NpcCombatDropTable.builder().probabilityDenominator(10000).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRACONIC_VISAGE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(256).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_PLATESKIRT)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_PLATELEGS)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_SPEAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DRAGONSTONE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ENSOULED_DRAGON_HEAD_13511)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_DAGGER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_LONGSWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_DHIDE_BODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_DHIDE_VAMB)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_MED_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_JAVELIN_HEADS, 40)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_ORE_NOTED, 3)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_HASTA)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_SPEAR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_LONGSWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATELEGS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_FULL_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_ARROW, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_KNIFE, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_THROWNAXE, 30)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_ARROWTIPS, 40)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAVA_SCALE_NOTED, 5)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 370, 2200)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANGLERFISH, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_JAVELIN, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_DART, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 75)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 75)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_BONES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_DRAGONHIDE, 2)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.BRUTAL_BLACK_DRAGON_318);
    combat.hitpoints(NpcCombatHitpoints.total(315));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(330)
            .magicLevel(250)
            .defenceLevel(258)
            .bonus(BonusType.DEFENCE_STAB, 50)
            .bonus(BonusType.DEFENCE_SLASH, 70)
            .bonus(BonusType.DEFENCE_CRUSH, 70)
            .bonus(BonusType.DEFENCE_MAGIC, 60)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    combat.slayer(NpcCombatSlayer.builder().level(77).build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.deathAnimation(92).blockAnimation(89);
    combat.type(NpcCombatType.DRAGON);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(29));
    style.animation(80).attackSpeed(4);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(29));
    style.animation(6722).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(130));
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.DRAGONFIRE);
    style.damage(NpcCombatDamage.maximum(65));
    style.animation(81).attackSpeed(6);
    style.castGraphic(new Graphic(1, 100));
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
