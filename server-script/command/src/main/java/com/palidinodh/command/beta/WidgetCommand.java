package com.palidinodh.command.beta;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler.Beta;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName({"widget", "widgetat"})
class WidgetCommand implements CommandHandler, Beta {

  @Override
  public String getExample(String name) {
    switch (name) {
      case "widget":
        return "id";
      case "widgetat":
        return "pos id";
    }
    return "";
  }

  @Override
  public void execute(Player player, String name, String message) {
    switch (name) {
      case "widget":
        player.getWidgetManager().sendInteractiveOverlay(Integer.parseInt(message));
        break;
      case "widgetat":
        {
          if (player.getAttributeInt("widget_at_index") != 0) {
            player
                .getGameEncoder()
                .sendRemoveWidget(
                    player.getAttributeInt("on_widget"), player.getAttributeInt("widget_at_index"));
          }
          var ints = CommandHandler.splitInt(message);
          if (ints.length == 3) {
            player.putAttribute("on_widget", ints[0]);
            player.putAttribute("widget_at_index", ints[1]);
            player.getGameEncoder().sendWidget(ints[0], ints[1], ints[2], true);
          } else {
            player.putAttribute("on_widget", player.getWidgetManager().getRootWidgetId());
            player.putAttribute("widget_at_index", ints[0]);
            player
                .getGameEncoder()
                .sendWidget(player.getWidgetManager().getRootWidgetId(), ints[0], ints[1], true);
          }
          break;
        }
    }
  }
}
