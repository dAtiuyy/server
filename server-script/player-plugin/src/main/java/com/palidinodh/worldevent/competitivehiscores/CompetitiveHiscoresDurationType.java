package com.palidinodh.worldevent.competitivehiscores;

import com.palidinodh.util.PTime;
import java.util.Calendar;
import java.util.function.BooleanSupplier;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum CompetitiveHiscoresDurationType {
  HOURS_1("Hour", "Current hour", "Previous hour", Calendar.HOUR_OF_DAY, 1, null, true),
  HOURS_3(
      "3 Hr",
      "12AM-3AM, 3AM-6AM, 6AM-9AM, 9AM-12PM, 12PM-3PM, 3PM-6PM, 6PM-9PM, 9PM-12AM",
      "Previous 3 hours",
      Calendar.HOUR_OF_DAY,
      3,
      null,
      true),
  HOURS_6(
      "6 Hr",
      "12AM-6AM, 6AM-12PM, 12PM-6PM, 6PM-12AM",
      "Previous 6 hours",
      Calendar.HOUR_OF_DAY,
      6,
      null,
      true),
  HOURS_12(
      "12 Hr", "12AM-12PM, 12PM-12AM", "Previous 12 hours", Calendar.HOUR_OF_DAY, 12, null, false),
  DAY("Today", "Today", "Yesterday", Calendar.DAY_OF_WEEK, -1, null, false),
  WEEK("Week", "This week", "Previous week", Calendar.WEEK_OF_MONTH, -1, null, false),
  WEEKEND(
      "Weekend",
      "This weekend",
      "Previous weekend",
      -1,
      -1,
      CompetitiveHiscoresDurationType::isWeekend,
      false),
  MONTH("Month", "This month", "Previous month", Calendar.MONTH, -1, null, false);

  private final String currentName;
  private final String currentDescription;
  private final String previousDescription;
  private final int calendarIdentifier;
  private final int hours;
  private final BooleanSupplier booleanSupplier;
  private final boolean hidden;

  private static boolean isWeekend() {
    var day = PTime.getDayOfWeek();
    var hour = PTime.getHour24();
    if (day == Calendar.FRIDAY && hour < 8) {
      return false;
    }
    if (day == Calendar.SUNDAY && hour > 20) {
      return false;
    }
    return day == Calendar.FRIDAY || day == Calendar.SATURDAY || day == Calendar.SUNDAY;
  }
}
