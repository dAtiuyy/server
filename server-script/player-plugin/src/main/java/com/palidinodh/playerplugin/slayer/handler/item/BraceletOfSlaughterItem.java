package com.palidinodh.playerplugin.slayer.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.BRACELET_OF_SLAUGHTER)
public class BraceletOfSlaughterItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    switch (option.getText()) {
      case "check":
        player
            .getGameEncoder()
            .sendMessage(
                "Your bracelet of slaughter has "
                    + plugin.getBraceletOfSlaughterCharges()
                    + " charges left.");
        break;
      case "break":
        plugin.breakBraceletOfSlaughter();
        break;
    }
  }
}
