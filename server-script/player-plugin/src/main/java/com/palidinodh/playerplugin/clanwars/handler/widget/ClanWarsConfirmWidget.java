package com.palidinodh.playerplugin.clanwars.handler.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.clanwars.ClanWarsPlugin;
import com.palidinodh.playerplugin.clanwars.ClanWarsStages;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.CLAN_WARS_CONFIRM)
class ClanWarsConfirmWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    switch (childId) {
      case 6:
        ClanWarsStages.acceptRuleConfirmation(player, player.getPlugin(ClanWarsPlugin.class));
        break;
    }
  }
}
