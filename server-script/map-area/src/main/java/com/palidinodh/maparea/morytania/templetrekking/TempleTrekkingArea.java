package com.palidinodh.maparea.morytania.templetrekking;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(9806)
public class TempleTrekkingArea extends Area {}
