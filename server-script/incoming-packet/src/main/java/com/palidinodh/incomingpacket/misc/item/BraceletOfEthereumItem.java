package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.BRACELET_OF_ETHEREUM, ItemId.BRACELET_OF_ETHEREUM_UNCHARGED})
class BraceletOfEthereumItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "toggle-absorption":
      case "toggle absorption":
        player.getCharges().setEthereumAutoAbsorb(!player.getCharges().getEthereumAutoAbsorb());
        player
            .getGameEncoder()
            .sendMessage(
                "Ether automatic absorption: " + player.getCharges().getEthereumAutoAbsorb());
        break;
      case "uncharge":
        if (player.getInventory().getRemainingSlots() < 1) {
          player.getInventory().notEnoughSpace();
          return;
        }
        player.getInventory().addItem(ItemId.REVENANT_ETHER, item.getCharges());
        item.replace(new Item(ItemId.BRACELET_OF_ETHEREUM_UNCHARGED));
        break;
      case "dismantle":
        item.remove();
        player.getInventory().addItem(ItemId.REVENANT_ETHER, 250);
        break;
    }
  }
}
