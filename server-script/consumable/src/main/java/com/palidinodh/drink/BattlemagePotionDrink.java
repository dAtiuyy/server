package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableStat;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.playerplugin.skill.SkillPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.BATTLEMAGE_POTION_1,
  ItemId.BATTLEMAGE_POTION_2,
  ItemId.BATTLEMAGE_POTION_3,
  ItemId.BATTLEMAGE_POTION_4
})
class BattlemagePotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.MAGIC, 4, 0, true));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 5, 15, true));
    return builder;
  }
}

@ReferenceId({
  ItemId.BLIGHTED_BATTLEMAGE_1_60035,
  ItemId.BLIGHTED_BATTLEMAGE_2_60033,
  ItemId.BLIGHTED_BATTLEMAGE_3_60031,
  ItemId.BLIGHTED_BATTLEMAGE_4_60029
})
class BlightedBattlemagePotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.MAGIC, 4, 0, true));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 5, 15, true));
    return builder;
  }
}

@ReferenceId({
  ItemId.DIVINE_BATTLEMAGE_POTION_1,
  ItemId.DIVINE_BATTLEMAGE_POTION_2,
  ItemId.DIVINE_BATTLEMAGE_POTION_3,
  ItemId.DIVINE_BATTLEMAGE_POTION_4
})
class DivineBattlemagePotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.stat(new ConsumableStat(Skills.MAGIC, 4, 0, true));
    builder.stat(new ConsumableStat(Skills.DEFENCE, 5, 15, true));
    builder.complete(
        (p, c) -> {
          if (p.getCombat().getHitpoints() <= 10) {
            p.getGameEncoder().sendMessage("You don't have enough hitpoints to do this.");
            return false;
          }
          return true;
        });
    builder.action(
        (p, c) -> {
          p.getPlugin(SkillPlugin.class)
              .getRestore(Skills.MAGIC)
              .startToLimits(500, p.getSkills().getLevel(Skills.MAGIC));
          p.getPlugin(SkillPlugin.class)
              .getRestore(Skills.DEFENCE)
              .startToLimits(500, p.getSkills().getLevel(Skills.DEFENCE));
          p.getCombat().applyHit(new Hit(10));
        });
    return builder;
  }
}
