package com.palidinodh.maparea.deathsoffice.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.chat.DespairChatDialogue;
import com.palidinodh.playerplugin.gravestone.GravestonePlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.DEATH_9855)
class DeathNpc implements NpcHandler {

  @Override
  public ReachType canReach(Player player, Npc npc) {
    player.getMovement().setFollowing(null);
    player.getMovement().quickRoute(3176, player.getY());
    return player.getX() == 3176 && (player.getY() >= 5725 && player.getY() <= 5728)
        ? ReachType.TRUE
        : ReachType.FALSE;
  }

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    var plugin = player.getPlugin(GravestonePlugin.class);
    if (option.getIndex() == 0) {
      player.openDialogue(
          new DespairChatDialogue(
              NpcId.DEATH_9855,
              "Hello mortal. Have you come to retrieve something you lost when you died?",
              (c, s) -> {
                player.openOptionsDialogue(
                    new DialogueOption(
                        "Yes, have you got anything for me?",
                        (c1, s1) -> {
                          plugin.openDeathsRetrieval();
                        }),
                    new DialogueOption(
                        "Yes, please collect my gravestone for me.",
                        (c1, s1) -> {
                          if (plugin.getCountdown() <= 0) {
                            player
                                .getGameEncoder()
                                .sendMessage("You don't have a gravestone to be collected.");
                            return;
                          }
                          plugin.moveGravestoneItems();
                          plugin.openDeathsRetrieval();
                        }),
                    new DialogueOption("Not just now, thanks."));
              }));
    } else if (option.getIndex() == 1) {
      plugin.openDeathsRetrieval();
    }
  }
}
