package com.palidinodh.maparea.asgarnia.cerberuslair.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class CerberusLairNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(new Tile(1238, 1250), NpcId.CERBERUS_318));
    spawns.add(new NpcSpawn(new Tile(1366, 1250), NpcId.CERBERUS_318));
    spawns.add(new NpcSpawn(new Tile(1302, 1314), NpcId.CERBERUS_318));

    return spawns;
  }
}
