package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.ABYSSAL_TENTACLE, ItemId.ABYSSAL_TENTACLE_OR_60047})
class AbyssalTentacleSpecialAttack extends SpecialAttack {

  AbyssalTentacleSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(1658);
    entry.impactGraphic(new Graphic(341, 100));
    entry.castSound(new Sound(2713));
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (hooks.getDamage() == 0) {
      return;
    }
    var opponent = hooks.getOpponent();
    if (!opponent.getController().canMagicBind()) {
      return;
    }
    opponent.getController().setMagicBind(9, hooks.getPlayer());
  }
}
