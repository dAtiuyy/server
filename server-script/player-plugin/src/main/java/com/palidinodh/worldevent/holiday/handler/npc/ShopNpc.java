package com.palidinodh.worldevent.holiday.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({NpcId.DEATH_16050, NpcId.JACK_FROST_16052, NpcId.BUNNY_MAN})
public class ShopNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    switch (npc.getId()) {
      case NpcId.DEATH_16050:
        player.openShop("halloween");
        break;
      case NpcId.JACK_FROST_16052:
        player.openShop("christmas");
        break;
      case NpcId.BUNNY_MAN:
        player.openShop("easter");
        break;
    }
  }
}
