package com.palidinodh.nio;

import com.palidinodh.util.PLogger;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NioServer implements Runnable {

  private List<Session> sessions = new ArrayList<>();
  private Map<String, Integer> connectionCounts = new HashMap<>();
  private InetSocketAddress hostAddress;
  private ServerSocketChannel serverSocketChannel;
  private Selector selector;
  private SessionHandler sessionHandler;
  private ByteBuffer directBuffer = ByteBuffer.allocateDirect(0xFFFF);
  private boolean running;

  private int sessionIdleTimeout;
  private int maxConnectionsPerAddress;

  private static void printException(Exception e) {
    if (e instanceof ClosedChannelException) {
      return;
    }
    var message = e.getMessage() == null ? "" : e.getMessage().toLowerCase();
    if (message.contains("connection reset by peer")) {
      return;
    }
    if (message.contains("broken pipe")) {
      return;
    }
    e.printStackTrace();
  }

  public NioServer() throws IOException {
    selector = Selector.open();
    serverSocketChannel = ServerSocketChannel.open();
    serverSocketChannel.configureBlocking(false);
  }

  @Override
  public void run() {
    if (running) {
      throw new IllegalStateException("Server is already running");
    }
    running = true;
    while (serverSocketChannel != null && serverSocketChannel.isOpen()) {
      cycle();
    }
    running = false;
  }

  public void start(String remoteAddress, int port) throws IOException {
    if (hostAddress != null) {
      throw new IllegalStateException("Server already started");
    }
    if (sessionHandler == null) {
      throw new IllegalStateException("SsessionHandler can't be null");
    }
    hostAddress = new InetSocketAddress(remoteAddress, port);
    serverSocketChannel.socket().bind(hostAddress);
    serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
    PLogger.println("Starting server on " + remoteAddress + ":" + port);
    new Thread(this, "NioServer").start();
  }

  public void stop() {
    try {
      if (serverSocketChannel != null) {
        serverSocketChannel.close();
        serverSocketChannel = null;
      }
    } catch (IOException ioe) {
      printException(ioe);
    }
  }

  public void setSessionHandler(SessionHandler sessionHandler) {
    this.sessionHandler = sessionHandler;
  }

  public void setSessionIdleTimeout(int seconds) {
    if (hostAddress != null) {
      throw new IllegalStateException("Server already started");
    }
    if (seconds <= 0) {
      throw new IllegalArgumentException("seconds must be greater than 0");
    }
    sessionIdleTimeout = seconds * 1000;
  }

  public void setMaxConnectionsPerAddress(int maxConnectionsPerAddress) {
    if (hostAddress != null) {
      throw new IllegalStateException("Server already started");
    }
    if (maxConnectionsPerAddress <= 0) {
      throw new IllegalArgumentException("maxConnectionsPerAddress must be greater than 0");
    }
    this.maxConnectionsPerAddress = maxConnectionsPerAddress;
  }

  public void printStats() {
    PLogger.println(
        "NioServer: sessions: "
            + sessions.size()
            + "; connectionCounts: "
            + connectionCounts.size());
  }

  private void cycle() {
    try {
      selector.select();
      for (var it = selector.selectedKeys().iterator(); it.hasNext(); ) {
        var selectionKey = it.next();
        it.remove();
        Session session = null;
        try {
          if (serverSocketChannel == null || !serverSocketChannel.isOpen()) {
            break;
          }
          session = (Session) selectionKey.attachment();
          if (selectionKey.isValid() && selectionKey.isAcceptable()) {
            session = accept(selectionKey);
          }
          if (session == null) {
            continue;
          }
          if (selectionKey.isValid() && selectionKey.isReadable()) {
            read(selectionKey);
          }
          if (selectionKey.isValid() && selectionKey.isWritable()) {
            write(selectionKey);
          }
        } catch (Exception socketError) {
          error(socketError, session);
        }
      }
      checkSessions();
      Thread.sleep(10);
    } catch (Exception e) {
      printException(e);
    }
  }

  private Session accept(SelectionKey selectionKey) throws IOException {
    var socketChannel = serverSocketChannel.accept();
    socketChannel.configureBlocking(false);
    var remoteAddress = socketChannel.socket().getInetAddress().getHostAddress();
    int connectionCount = getConnectionCount(remoteAddress);
    if (maxConnectionsPerAddress > 0 && connectionCount >= maxConnectionsPerAddress) {
      socketChannel.close();
      return null;
    }
    connectionCounts.put(remoteAddress, connectionCount + 1);
    var session =
        new Session(
            socketChannel, remoteAddress, socketChannel.register(selector, SelectionKey.OP_READ));
    sessionHandler.accept(session);
    sessions.add(session);
    return session;
  }

  private void read(SelectionKey selectionKey) throws IOException {
    var socketChannel = (SocketChannel) selectionKey.channel();
    if (!socketChannel.isOpen()) {
      return;
    }
    var session = (Session) selectionKey.attachment();
    directBuffer.clear();
    var count = socketChannel.read(directBuffer);
    if (count == -1) {
      session.close();
      return;
    }
    if (count == 0) {
      return;
    }
    session.updateLastRead();
    directBuffer.flip();
    var bytes = new byte[count];
    directBuffer.get(bytes);
    sessionHandler.read(session, bytes);
  }

  private void write(SelectionKey selectionKey) {
    var socketChannel = (SocketChannel) selectionKey.channel();
    if (!socketChannel.isOpen()) {
      return;
    }
    var session = (Session) selectionKey.attachment();
    if (session.getWriteEvents().isEmpty()) {
      return;
    }
    try {
      while (!session.getWriteEvents().isEmpty()) {
        var writeEvent = session.getWriteEvents().peek();
        var fromBuffer = writeEvent.getBuffer();
        var position = fromBuffer.position();
        var limit = fromBuffer.limit();
        do {
          directBuffer.clear();
          if (fromBuffer.remaining() > directBuffer.capacity()) {
            fromBuffer.limit(fromBuffer.position() + directBuffer.capacity());
          }
          directBuffer.put(fromBuffer);
          directBuffer.flip();
          position += socketChannel.write(directBuffer);
          if (position != fromBuffer.position()) {
            fromBuffer.position(position);
          }
          if (limit != fromBuffer.limit()) {
            fromBuffer.limit(limit);
          }
        } while (directBuffer.remaining() == 0 && fromBuffer.remaining() > 0);
        if (fromBuffer.remaining() > 0) {
          break;
        }
        session.getWriteEvents().poll();
        if (writeEvent.getHandler() == null) {
          continue;
        }
        writeEvent.getHandler().complete(session, true);
      }
    } catch (Exception e) {
      error(e, session);
    }
    if (selectionKey.isValid() && session.getWriteEvents().isEmpty()) {
      selectionKey.interestOps(SelectionKey.OP_READ);
    }
  }

  private void error(Exception exception, Session session) {
    printException(exception);
    try {
      sessionHandler.error(exception, session);
    } catch (Exception e) {
      if (session != null) {
        session.close();
      }
    }
  }

  private void checkSessions() {
    if (sessions.isEmpty()) {
      return;
    }
    for (var it = sessions.iterator(); it.hasNext(); ) {
      var session = it.next();
      var selectionKey = session.getSelectionKey();
      if (selectionKey.isValid() && !session.getWriteEvents().isEmpty()) {
        selectionKey.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
      }
      if (session.idleTimeout(sessionIdleTimeout)) {
        session.close();
      }
      if (session.isOpen()) {
        continue;
      }
      var remoteAddress = session.getRemoteAddress();
      var connectionCount = getConnectionCount(remoteAddress);
      if (connectionCount > 1) {
        connectionCounts.put(remoteAddress, connectionCount - 1);
      } else {
        connectionCounts.remove(remoteAddress);
      }
      sessionHandler.closed(session);
      if (selectionKey.isValid()) {
        selectionKey.cancel();
      }
      while (!session.getWriteEvents().isEmpty()) {
        var writeEvent = session.getWriteEvents().poll();
        if (writeEvent.getHandler() == null) {
          continue;
        }
        writeEvent.getHandler().complete(session, false);
      }
      it.remove();
    }
  }

  private int getConnectionCount(String remoteAddress) {
    return connectionCounts.getOrDefault(remoteAddress, 0);
  }
}
