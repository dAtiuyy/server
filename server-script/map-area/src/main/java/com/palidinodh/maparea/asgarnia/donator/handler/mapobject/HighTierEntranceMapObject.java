package com.palidinodh.maparea.asgarnia.donator.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.UserRank;

@ReferenceId(ObjectId.SHIMMERING_BARRIER_30398)
class HighTierEntranceMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (!player.isUsergroup(UserRank.DIAMOND_MEMBER) || !player.isStaff()) {
      player.getGameEncoder().sendMessage("Your donator rank is not a high enough tier to enter.");
      return;
    }
    player.getMovement().clear();
    if (player.getX() >= 3042) {
      player.getMovement().addMovement(3040, 3503);
    } else {
      player.getMovement().addMovement(3042, 3503);
    }
  }
}
