package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.combat.TileHitEvent;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.TempMapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PNumber;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class XarpusCombat extends NpcCombat {

  private static final int HEAL_TIME = 160;
  private static final int DIRECTION_TIME = 9;
  private static final Tile[] ATTACKING_DIRECTIONS = {
    new Tile(3177, 4394, 1),
    new Tile(3177, 4380, 1),
    new Tile(3163, 4380, 1),
    new Tile(3163, 4394, 1)
  };
  private static final NpcCombatStyle PHASE_3_ATTACK =
      NpcCombatStyle.builder()
          .type(
              NpcCombatStyleType.builder()
                  .hitStyleType(HitStyleType.MAGIC)
                  .subHitStyleType(HitStyleType.TYPELESS)
                  .hitMarkType(HitMarkType.GREEN)
                  .build())
          .damage(NpcCombatDamage.builder().maximum(49).ignorePrayer(true).build())
          .projectile(NpcCombatProjectile.id(1555))
          .animation(8059)
          .attackSpeed(6)
          .targetGraphic(new Graphic(1556))
          .build();

  @Inject private Npc npc;
  private boolean loaded;
  private int baseHitpoints;
  private int healTime = HEAL_TIME;
  private List<PEvent> exhumedList = new ArrayList<>();
  private List<PEvent> acidicList = new ArrayList<>();
  private boolean hasScreeched;
  private Tile attackingDirection = PRandom.arrayRandom(ATTACKING_DIRECTIONS);
  private int directionChange = DIRECTION_TIME;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder();
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OVERLOAD_PLUS_4, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.REVITALISATION_PLUS_4, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRAYER_ENHANCE_PLUS_4)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.XARPUS_960).id(NpcId.XARPUS_960_8339);
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(400).barType(HitpointsBarType.GREEN_RED_100).build());
    combat.stats(
        NpcCombatStats.builder()
            .magicLevel(220)
            .defenceLevel(250)
            .bonus(BonusType.DEFENCE_RANGED, 160)
            .build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    combat.deathAnimation(7495);

    var combat2 = NpcCombatDefinition.builder();
    combat2.id(NpcId.XARPUS_960_8340);
    combat2.hitpoints(
        NpcCombatHitpoints.builder().total(400).barType(HitpointsBarType.GREEN_RED_100).build());
    combat2.stats(
        NpcCombatStats.builder()
            .magicLevel(220)
            .defenceLevel(250)
            .bonus(BonusType.DEFENCE_RANGED, 160)
            .build());
    combat2.aggression(NpcCombatAggression.builder().range(12).always(true).build());
    combat2.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat2.focus(NpcCombatFocus.builder().disableFollowingOpponent(true).build());
    combat2.deathAnimation(8063);
    combat2.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.MAGIC)
            .subHitStyleType(HitStyleType.TYPELESS)
            .hitMarkType(HitMarkType.GREEN)
            .build());
    style.damage(
        NpcCombatDamage.builder().maximum(6).alwaysMaximum(true).ignorePrayer(true).build());
    style.animation(8059).attackSpeed(6);
    style.targetGraphic(new Graphic(1556));
    style.projectile(NpcCombatProjectile.builder().id(1555).speedMinimumDistance(8).build());
    style.effect(NpcCombatEffect.builder().poison(3).build());
    var targetTile = NpcCombatTargetTile.builder().radius(1);
    targetTile.breakOff(
        NpcCombatTargetTile.BreakOff.builder()
            .count(1)
            .distance(7)
            .damage(NpcCombatDamage.maximum(8))
            .afterTargettedTile(true)
            .build());
    style.specialAttack(targetTile.build());
    combat2.style(style.build());

    return Arrays.asList(combat.build(), combat2.build());
  }

  @Override
  public void tickStartHook() {
    if (!loaded) {
      loadProfile();
      return;
    }
    if (!npc.getController().isRegionLoaded()) {
      return;
    }
    if (npc.getId() == NpcId.XARPUS_960) {
      var players = npc.getController().getPlayers();
      for (var player : players) {
        if (!npc.withinDistance(player, 4)) {
          continue;
        }
        npc.setId(NpcId.XARPUS_960_8339);
        break;
      }
    } else if (npc.getId() == NpcId.XARPUS_960_8339) {
      healPhase();
    } else if (npc.getId() == NpcId.XARPUS_960_8340) {
      combatPhase();
    }
  }

  @Override
  public void targetTileHitEventHook(
      NpcCombatStyle combatStyle,
      Entity opponent,
      TileHitEvent tileHitEvent,
      Graphic.Projectile projectile) {
    for (var event : acidicList) {
      if ((event.getAttachment() instanceof MapObject)
              && tileHitEvent.getTile().matchesTile((Tile) event.getAttachment())
          || (event.getAttachment() instanceof TempMapObject)
              && tileHitEvent
                  .getTile()
                  .matchesTile(((TempMapObject) event.getAttachment()).getTempMapObject(0))) {
        return;
      }
    }
    var mapObject =
        new MapObject(32744, 22, MapObject.getRandomDirection(), tileHitEvent.getTile());
    var event =
        new PEvent(projectile.getEventDelay()) {
          @Override
          public void execute() {
            if (getExecutions() == 0) {
              setTick(0);
              var tempMapObject =
                  new TempMapObject(Integer.MAX_VALUE, npc.getController(), mapObject);
              npc.getWorld().addEvent(tempMapObject);
              setAttachment(tempMapObject);
            } else if (getExecutions() == 1) {
              for (var player : npc.getController().getPlayers()) {
                player.getGameEncoder().sendMapObjectAnimation(mapObject, 8068);
              }
            } else if (!npc.isVisible()) {
              stop();
            } else {
              for (var player : npc.getController().getPlayers()) {
                if (!player.matchesTile(mapObject)) {
                  continue;
                }
                player.getCombat().addHit(new Hit(6, HitMarkType.GREEN));
              }
            }
          }

          @Override
          public void stopHook() {
            if (!(getAttachment() instanceof TempMapObject)) {
              return;
            }
            ((PEvent) getAttachment()).stop();
          }
        };
    event.setAttachment(mapObject);
    npc.getWorld().addEvent(event);
    acidicList.add(event);
  }

  @Override
  public boolean canAttackEntityHook(NpcCombatStyle combatStyle, Entity opponent) {
    return getHitpoints() > getMaxHitpoints() * 0.23 || combatStyle == PHASE_3_ATTACK;
  }

  @Override
  public boolean canBeAttackedHook(
      Entity opponent, boolean sendMessage, HitStyleType hitStyleType) {
    return npc.getId() != NpcId.XARPUS_960;
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (!hasScreeched || isHitDelayed() || !opponent.withinDistance(attackingDirection, 7)) {
      return damage;
    }
    applyAttack(PHASE_3_ATTACK, opponent, getProjectileSpeed(opponent), 0);
    return damage;
  }

  private void loadProfile() {
    if (loaded || npc.getController().getPlayers().isEmpty()) {
      return;
    }
    loaded = true;
    var averageHP = 0;
    var playerMultiplier = 1.0;
    var players = npc.getController().getPlayers();
    for (var player : players) {
      averageHP += player.getCombat().getMaxHitpoints();
      playerMultiplier = PNumber.addDoubles(playerMultiplier, 0.5);
    }
    averageHP /= players.size();
    baseHitpoints = (int) ((50 + (players.size() * 25) + (averageHP * 2)) * playerMultiplier);
    setMaxHitpoints(baseHitpoints);
    setHitpoints(getMaxHitpoints());
  }

  private void healPhase() {
    if (getMaxHitpoints() >= baseHitpoints * 1.33) {
      healTime = 0;
    }
    if (healTime-- > 0) {
      var playerCount = npc.getController().getPlayers().size();
      var exhumedCount = Math.min(playerCount, Math.max(1, ((HEAL_TIME - healTime) / 20)));
      var newExhumedCount = exhumedCount - exhumedList.size();
      for (var iterator = exhumedList.iterator(); iterator.hasNext(); ) {
        var exumedEvent = iterator.next();
        if (exumedEvent.isRunning()) {
          continue;
        }
        iterator.remove();
        newExhumedCount++;
      }
      for (var i = 0; i < newExhumedCount; i++) {
        var tile = new Tile(3170, 4387, 1);
        var attempts = 0;
        do {
          tile.randomize(7);
        } while (attempts++ < 4 && npc.withinDistance(tile, 0));
        var mapObject = new MapObject(32743, 22, 0, tile);
        var tempMapObject = new TempMapObject(16, npc.getController(), mapObject);
        npc.getWorld().addEvent(tempMapObject);
        var event =
            new PEvent(0) {
              @Override
              public void execute() {
                var mapObject = ((TempMapObject) getAttachment()).getTempMapObject(0);
                if (getExecutions() == 1) {
                  if (mapObject != null) {
                    for (var player : npc.getController().getPlayers()) {
                      player.getGameEncoder().sendMapObjectAnimation(mapObject, 8065);
                    }
                  }
                } else if (getExecutions() == 16
                    || !npc.isVisible()
                    || npc.getId() != NpcId.XARPUS_960_8339) {
                  stop();
                  if (mapObject != null) {
                    npc.getController().sendMapGraphic(mapObject, new Graphic(1549));
                  }
                } else if (getExecutions() > 1) {
                  var hasPlayer = false;
                  for (var player : npc.getController().getPlayers()) {
                    if (!player.matchesTile(mapObject)) {
                      continue;
                    }
                    hasPlayer = true;
                    break;
                  }
                  if (!hasPlayer && mapObject != null) {
                    var projectile =
                        Graphic.Projectile.builder()
                            .id(1550)
                            .entity(npc)
                            .endTile(mapObject)
                            .startHeight(0)
                            .endHeight(50)
                            .speed(getProjectileSpeed(tile))
                            .curve(48)
                            .radius(64)
                            .build();
                    sendMapProjectile(projectile);
                    addHitEvent(
                        new HitEvent(
                            projectile.getEventDelay(),
                            new Hit(6, HitMarkType.HEAL, HitStyleType.HEAL)));
                    if (getMaxHitpoints() < baseHitpoints * 1.33) {
                      setHitpoints(getHitpoints() + 6);
                      setMaxHitpoints(getMaxHitpoints() + 6);
                    }
                  }
                }
              }

              @Override
              public void stopHook() {
                ((PEvent) getAttachment()).stop();
              }
            };
        npc.getWorld().addEvent(event);
        event.setAttachment(tempMapObject);
        exhumedList.add(event);
      }
    } else if (healTime <= 0) {
      npc.setId(NpcId.XARPUS_960_8340);
      npc.setAnimation(8061);
      for (var event : exhumedList) {
        event.stop();
      }
    }
  }

  private void combatPhase() {
    for (var player : npc.getController().getPlayers()) {
      if (!npc.withinDistance(player, 0)) {
        continue;
      }
      player.getCombat().addHit(new Hit(6));
    }
    if (getHitpoints() > getMaxHitpoints() * 0.23) {
      return;
    }
    if (!hasScreeched) {
      hasScreeched = true;
      npc.setForceMessage("Screeeeech!");
      npc.setFaceTile(attackingDirection);
      setDisableAutoRetaliate(true);
      for (var player : npc.getController().getPlayers()) {
        player.getGameEncoder().sendMessage("Xarpus begins to stare intently.");
      }
      setHitDelay(12);
    }
    if (directionChange-- > 0) {
      return;
    }
    attackingDirection = PRandom.arrayRandom(ATTACKING_DIRECTIONS);
    directionChange = DIRECTION_TIME;
    npc.setFaceTile(attackingDirection);
  }
}
