package com.palidinodh.maparea.kandarin.yanille.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.Region;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.MAGIC_GUILD_DOOR, ObjectId.MAGIC_GUILD_DOOR_1733})
public class MagicGuildDoorMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (mapObject.isBusy()) {
      return;
    }
    if (player.getSkills().getLevelForXP(Skills.MAGIC) < 66) {
      player.getGameEncoder().sendMessage("You need a Magic level of 66 to enter.");
      return;
    }
    player.getMovement().clear();
    switch (mapObject.getDirection()) {
      case 0:
        player.getMovement().addMovement(player.getX() - 1, player.getY());
        break;
      case 2:
        player.getMovement().addMovement(player.getX() + 1, player.getY());
        break;
    }
    Region.openDoors(player, mapObject, 1, false);
  }
}
