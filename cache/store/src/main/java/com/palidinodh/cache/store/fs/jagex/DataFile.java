package com.palidinodh.cache.store.fs.jagex;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;

public class DataFile implements Closeable {

  private static final int SECTOR_SIZE = 520;

  private final RandomAccessFile dat;

  public DataFile(File file) throws FileNotFoundException {
    dat = new RandomAccessFile(file, "rw");
  }

  @Override
  public void close() throws IOException {
    dat.close();
  }

  public void clear() throws IOException {
    dat.setLength(0L);
  }

  public byte[] read(int indexId, int archiveId, int sector, int size) throws IOException {
    if (sector < 0L || dat.length() / SECTOR_SIZE < (long) sector) {
      throw new IOException(
          "Bad read, dat length "
              + dat.length()
              + ", requested sector "
              + sector
              + " for "
              + indexId
              + "/"
              + archiveId);
    }
    var readBuffer = new byte[SECTOR_SIZE];
    var buffer = ByteBuffer.allocate(size);
    for (int part = 0, readBytesCount = 0, nextSector; size > readBytesCount; sector = nextSector) {
      if (sector < 0) {
        throw new IOException("Unexpected end of file for " + indexId + "/" + archiveId);
      }
      dat.seek(SECTOR_SIZE * sector);
      var dataBlockSize = size - readBytesCount;
      byte headerSize;
      int currentIndex;
      int currentPart;
      int currentArchive;
      if (archiveId > 0xFFFF) {
        headerSize = 10;
        if (dataBlockSize > SECTOR_SIZE - headerSize) {
          dataBlockSize = SECTOR_SIZE - headerSize;
        }
        var i = dat.read(readBuffer, 0, headerSize + dataBlockSize);
        if (i != headerSize + dataBlockSize) {
          throw new IOException(
              "Short read when reading file data for " + indexId + "/" + archiveId);
        }
        currentArchive =
            ((readBuffer[0] & 0xFF) << 24)
                | ((readBuffer[1] & 0xFF) << 16)
                | ((readBuffer[2] & 0xFF) << 8)
                | (readBuffer[3] & 0xFF);
        currentPart = ((readBuffer[4] & 0xFF) << 8) + (readBuffer[5] & 0xFF);
        nextSector =
            ((readBuffer[6] & 0xFF) << 16) | ((readBuffer[7] & 0xFF) << 8) | (readBuffer[8] & 0xFF);
        currentIndex = readBuffer[9] & 0xFF;
      } else {
        headerSize = 8;
        if (dataBlockSize > SECTOR_SIZE - headerSize) {
          dataBlockSize = SECTOR_SIZE - headerSize;
        }
        var i = dat.read(readBuffer, 0, headerSize + dataBlockSize);
        if (i != headerSize + dataBlockSize) {
          throw new IOException("Short read for " + indexId + "/" + archiveId);
        }
        currentArchive = ((readBuffer[0] & 0xFF) << 8) | (readBuffer[1] & 0xFF);
        currentPart = ((readBuffer[2] & 0xFF) << 8) | (readBuffer[3] & 0xFF);
        nextSector =
            ((readBuffer[4] & 0xFF) << 16) | ((readBuffer[5] & 0xFF) << 8) | (readBuffer[6] & 0xFF);
        currentIndex = readBuffer[7] & 0xFF;
      }
      if (archiveId != currentArchive || currentPart != part || indexId != currentIndex) {
        throw new IOException(
            "Data mismatch "
                + archiveId
                + " != "
                + currentArchive
                + ", "
                + part
                + " != "
                + currentPart
                + ", "
                + indexId
                + " != "
                + currentIndex);
      }
      if (nextSector < 0 || dat.length() / SECTOR_SIZE < (long) nextSector) {
        throw new IOException("Invalid next sector for " + indexId + "/" + archiveId);
      }
      buffer.put(readBuffer, headerSize, dataBlockSize);
      readBytesCount += dataBlockSize;
      part++;
    }
    buffer.flip();
    return buffer.array();
  }

  public DataFileWriteResult write(int indexId, int archiveId, byte[] compressedData)
      throws IOException {
    int sector;
    int startSector;
    var writeBuffer = new byte[SECTOR_SIZE];
    var data = ByteBuffer.wrap(compressedData);
    sector = (int) ((dat.length() + (long) (SECTOR_SIZE - 1)) / (long) SECTOR_SIZE);
    if (sector == 0) {
      sector = 1;
    }
    startSector = sector;
    for (int part = 0; data.hasRemaining(); ++part) {
      var nextSector = sector + 1;
      int dataToWrite;
      if (archiveId > 0xFFFF) {
        if (data.remaining() <= 510) {
          nextSector = 0;
        }
        writeBuffer[0] = (byte) (archiveId >> 24);
        writeBuffer[1] = (byte) (archiveId >> 16);
        writeBuffer[2] = (byte) (archiveId >> 8);
        writeBuffer[3] = (byte) archiveId;
        writeBuffer[4] = (byte) (part >> 8);
        writeBuffer[5] = (byte) part;
        writeBuffer[6] = (byte) (nextSector >> 16);
        writeBuffer[7] = (byte) (nextSector >> 8);
        writeBuffer[8] = (byte) nextSector;
        writeBuffer[9] = (byte) indexId;
        dat.seek(SECTOR_SIZE * sector);
        dat.write(writeBuffer, 0, 10);
        dataToWrite = data.remaining();
        if (dataToWrite > 510) {
          dataToWrite = 510;
        }
      } else {
        if (data.remaining() <= 512) {
          nextSector = 0;
        }
        writeBuffer[0] = (byte) (archiveId >> 8);
        writeBuffer[1] = (byte) archiveId;
        writeBuffer[2] = (byte) (part >> 8);
        writeBuffer[3] = (byte) part;
        writeBuffer[4] = (byte) (nextSector >> 16);
        writeBuffer[5] = (byte) (nextSector >> 8);
        writeBuffer[6] = (byte) nextSector;
        writeBuffer[7] = (byte) indexId;
        dat.seek(SECTOR_SIZE * sector);
        dat.write(writeBuffer, 0, 8);
        dataToWrite = data.remaining();
        if (dataToWrite > 512) {
          dataToWrite = 512;
        }
      }
      data.get(writeBuffer, 0, dataToWrite);
      dat.write(writeBuffer, 0, dataToWrite);
      sector = nextSector;
    }
    var res = new DataFileWriteResult();
    res.sector = startSector;
    res.compressedLength = compressedData.length;
    return res;
  }
}
