package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.SANGUINESTI_STAFF_UNCHARGED, ItemId.HOLY_SANGUINESTI_STAFF_UNCHARGED})
class SanguinestiStaffUnchargedItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (item.getId()) {
      case ItemId.HOLY_SANGUINESTI_STAFF_UNCHARGED:
        switch (option.getText()) {
          case "dismantle":
            player.getInventory().deleteItem(ItemId.HOLY_SANGUINESTI_STAFF_UNCHARGED);
            player.getInventory().addOrDropItem(ItemId.SANGUINESTI_STAFF_UNCHARGED);
            player.getInventory().addOrDropItem(ItemId.HOLY_ORNAMENT_KIT);
            break;
          case "charge":
            player.getGameEncoder().sendMessage("This is charged with blood runes.");
            break;
        }
        break;
      case ItemId.SANGUINESTI_STAFF_UNCHARGED:
        switch (option.getText()) {
          case "charge":
            player.getGameEncoder().sendMessage("This is charged with blood runes.");
            break;
        }
        break;
    }
  }
}
