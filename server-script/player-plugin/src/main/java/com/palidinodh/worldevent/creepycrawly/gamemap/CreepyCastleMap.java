package com.palidinodh.worldevent.creepycrawly.gamemap;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.util.PArrayList;
import com.palidinodh.worldevent.creepycrawly.util.CreepyBarrier;
import com.palidinodh.worldevent.creepycrawly.util.CreepyRoom;
import lombok.Getter;

@Getter
public class CreepyCastleMap implements CreepyMap {

  private static final Tile PLAYER_TILE = new Tile(1354, 9226);
  private static final Tile BOSS_TILE = new Tile(1363, 9234);

  private PArrayList<CreepyRoom> rooms = new PArrayList<>();
  private PArrayList<CreepyBarrier> barriers = new PArrayList<>();
  private int openedSupplyChestId = ObjectId.CHEST_32573;
  private int supplyChestCount = 20;
  private int rareChestCount = 4;

  public CreepyCastleMap() {
    var builder = CreepyRoom.builder().id(1);
    builder.npcSpawn(new Tile(1359, 9224));
    builder.npcSpawn(new Tile(1352, 9239));
    builder.npcSpawn(new Tile(1365, 9244));
    builder.npcSpawn(new Tile(1374, 9238));
    builder.npcSpawn(new Tile(1374, 9224));
    builder.npcSpawn(new Tile(1368, 9224));
    builder.unlocked(true);
    var room1 = builder.build();
    rooms.add(room1);

    builder = CreepyRoom.builder().id(2);
    builder.npcSpawn(new Tile(1352, 9249));
    builder.npcSpawn(new Tile(1356, 9267));
    builder.npcSpawn(new Tile(1364, 9254));
    builder.npcSpawn(new Tile(1369, 9254));
    var room2 = builder.build();
    rooms.add(room2);

    builder = CreepyRoom.builder().id(3);
    builder.npcSpawn(new Tile(1377, 9258));
    builder.npcSpawn(new Tile(1387, 9258));
    builder.npcSpawn(new Tile(1387, 9247));
    var room3 = builder.build();
    rooms.add(room3);

    builder = CreepyRoom.builder().id(4);
    var room4 = builder.build();
    builder.npcSpawn(new Tile(1388, 9227));
    builder.npcSpawn(new Tile(1396, 9232));
    builder.npcSpawn(new Tile(1384, 9235));
    builder.npcSpawn(new Tile(1384, 9241));
    rooms.add(room4);

    builder = CreepyRoom.builder().id(5);
    var room5 = builder.build();
    builder.npcSpawn(new Tile(1374, 9243));
    builder.npcSpawn(new Tile(1382, 9250));
    rooms.add(room5);

    builder = CreepyRoom.builder().id(6);
    var room6 = builder.build();
    builder.npcSpawn(new Tile(1396, 9244));
    builder.npcSpawn(new Tile(1396, 9263));
    rooms.add(room6);

    builder = CreepyRoom.builder().id(7);
    var room7 = builder.build();
    builder.npcSpawn(new Tile(1390, 9268));
    rooms.add(room7);

    // Room barriers
    barriers.add(
        new CreepyBarrier(
            1000,
            new PArrayList<>(
                new Tile(1352, 9249),
                new Tile(1353, 9249),
                new Tile(1354, 9249),
                new Tile(1355, 9249),
                new Tile(1356, 9249),
                new Tile(1357, 9249),
                new Tile(1358, 9249),
                new Tile(1359, 9249),
                new Tile(1360, 9249)),
            new PArrayList<>(room1, room2)));
    barriers.add(
        new CreepyBarrier(
            1000,
            new PArrayList<>(
                new Tile(1377, 9258),
                new Tile(1377, 9259),
                new Tile(1377, 9260),
                new Tile(1377, 9261),
                new Tile(1377, 9262)),
            new PArrayList<>(room2, room3)));
    barriers.add(
        new CreepyBarrier(
            1000,
            new PArrayList<>(
                new Tile(1387, 9247),
                new Tile(1388, 9247),
                new Tile(1389, 9247),
                new Tile(1390, 9247),
                new Tile(1391, 9247)),
            new PArrayList<>(room3, room4)));
    barriers.add(
        new CreepyBarrier(
            1000,
            new PArrayList<>(
                new Tile(1386, 9250),
                new Tile(1386, 9251),
                new Tile(1386, 9252),
                new Tile(1386, 9253),
                new Tile(1386, 9254)),
            new PArrayList<>(room3, room5)));
    barriers.add(
        new CreepyBarrier(
            1000,
            new PArrayList<>(
                new Tile(1392, 9254),
                new Tile(1392, 9255),
                new Tile(1392, 9256),
                new Tile(1392, 9257),
                new Tile(1392, 9258)),
            new PArrayList<>(room3, room6)));
    barriers.add(
        new CreepyBarrier(
            1000,
            new PArrayList<>(
                new Tile(1379, 9227),
                new Tile(1379, 9228),
                new Tile(1379, 9229),
                new Tile(1379, 9230),
                new Tile(1379, 9231)),
            new PArrayList<>(room1, room4)));
    barriers.add(
        new CreepyBarrier(
            1000,
            new PArrayList<>(
                new Tile(1374, 9243),
                new Tile(1375, 9243),
                new Tile(1376, 9243),
                new Tile(1377, 9243),
                new Tile(1378, 9243)),
            new PArrayList<>(room1, room5)));
    barriers.add(
        new CreepyBarrier(
            1000,
            new PArrayList<>(
                new Tile(1396, 9267),
                new Tile(1397, 9267),
                new Tile(1398, 9267),
                new Tile(1399, 9267),
                new Tile(1400, 9267),
                new Tile(1401, 9267)),
            new PArrayList<>(room6, room7)));

    // Supply chest barriers
    barriers.add(
        new CreepyBarrier(250, new PArrayList<>(new Tile(1386, 9263), new Tile(1387, 9263)), null));
    barriers.add(
        new CreepyBarrier(250, new PArrayList<>(new Tile(1392, 9240), new Tile(1392, 9241)), null));
    barriers.add(
        new CreepyBarrier(250, new PArrayList<>(new Tile(1391, 9267), new Tile(1392, 9267)), null));

    // Rare chest barriers
    barriers.add(
        new CreepyBarrier(
            500,
            new PArrayList<>(
                new Tile(1366, 9264),
                new Tile(1367, 9264),
                new Tile(1368, 9264),
                new Tile(1369, 9264)),
            null));
    barriers.add(
        new CreepyBarrier(
            500,
            new PArrayList<>(
                new Tile(1397, 9231),
                new Tile(1398, 9231),
                new Tile(1399, 9231),
                new Tile(1400, 9231)),
            null));
    barriers.add(
        new CreepyBarrier(
            500,
            new PArrayList<>(
                new Tile(1397, 9243),
                new Tile(1398, 9243),
                new Tile(1399, 9243),
                new Tile(1400, 9243)),
            null));
    barriers.add(
        new CreepyBarrier(
            500,
            new PArrayList<>(
                new Tile(1378, 9269),
                new Tile(1378, 9270),
                new Tile(1378, 9271),
                new Tile(1378, 9272)),
            null));
  }

  @Override
  public Tile getPlayerSpawn() {
    return PLAYER_TILE;
  }

  @Override
  public Tile getBossTile() {
    return BOSS_TILE;
  }
}
