package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PCollection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class CerberusCombat extends NpcCombat {

  private static final NpcCombatStyle SPECIAL_ATTACK =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MELEE_SLASH)
          .damage(NpcCombatDamage.maximum(23))
          .animation(4491)
          .attackSpeed(4)
          .attackRange(10)
          .build();
  private static final int TRIPLE_DELAY = 66;
  private static final int SOULS_DELAY = 50;
  private static final int AOE_DELAY = 25;
  private static final int AOE_LENGTH = 14;

  @Inject private Npc npc;
  private int tripleDelay;
  private int soulsDelay;
  private int aoeDelay;
  private List<Npc> souls = new ArrayList<>();
  private List<AoeAttack> aoeAttacks = new ArrayList<>();

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .rareDropTableDenominator(42)
            .clue(ClueScrollType.ELITE, 100)
            .pet(ItemId.HELLPUPPY, 3000);
    var dropTable =
        NpcCombatDropTable.builder().probabilityDenominator(2000).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JAR_OF_SOULS)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PRIMORDIAL_CRYSTAL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PEGASIAN_CRYSTAL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ETERNAL_CRYSTAL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SMOULDERING_STONE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_HALBERD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLACK_DHIDE_BODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_FULL_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_AXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PICKAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_CHAINBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATEBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 60)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BOLTS_UNF, 40)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CANNONBALL, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORSTOL_SEED, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TORSTOL_NOTED, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_ORE_NOTED, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.KEY_MASTER_TELEPORT, 3)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAVA_BATTLESTAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BATTLESTAFF_NOTED, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 300)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_RUNE, 300)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPER_RESTORE_4, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COAL_NOTED, 120)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 10000, 20000)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNHOLY_SYMBOL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUMMER_PIE, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ASHES_NOTED, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_BONES_NOTED, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WINE_OF_ZAMORAK_NOTED, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_ORB_NOTED, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DIAMOND_NOTED, 5)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.INFERNAL_ASHES)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.CERBERUS_318);
    combat.spawn(NpcCombatSpawn.builder().respawnDelay(14).build());
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(600).barType(HitpointsBarType.GREEN_RED_100).build());
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(220)
            .magicLevel(220)
            .rangedLevel(220)
            .defenceLevel(100)
            .bonus(BonusType.MELEE_ATTACK, 50)
            .bonus(BonusType.ATTACK_MAGIC, 50)
            .bonus(BonusType.ATTACK_RANGED, 50)
            .bonus(BonusType.DEFENCE_STAB, 50)
            .bonus(BonusType.DEFENCE_SLASH, 100)
            .bonus(BonusType.DEFENCE_CRUSH, 25)
            .bonus(BonusType.DEFENCE_MAGIC, 100)
            .bonus(BonusType.DEFENCE_RANGED, 100)
            .build());
    combat.slayer(NpcCombatSlayer.builder().level(91).taskOnly(true).build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.type(NpcCombatType.DEMON).deathAnimation(4495).blockAnimation(4489);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(23));
    style.animation(4491).attackSpeed(4);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(23));
    style.animation(4490).attackSpeed(4);
    style.targetGraphic(new Graphic(1244, 100));
    style.projectile(NpcCombatProjectile.id(1245));
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(23));
    style.animation(4490).attackSpeed(4);
    style.targetGraphic(new Graphic(1243, 100));
    style.projectile(NpcCombatProjectile.id(1242));
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void spawnHook() {
    tripleDelay = TRIPLE_DELAY;
    soulsDelay = SOULS_DELAY;
    aoeDelay = AOE_DELAY;
  }

  @Override
  public void despawnHook() {
    removeSouls();
    aoeAttacks.clear();
  }

  @Override
  public void tickStartHook() {
    if (!npc.isVisible() || isDead()) {
      removeSouls();
      return;
    }
    if (npc.getController().getPlayers().isEmpty()) {
      if (getHitpoints() < getMaxHitpoints()) {
        npc.restore();
      }
      return;
    }
    checkTriple();
    checkSouls();
    checkAOE();
  }

  @Override
  public HitStyleType attackTickHitStyleTypeHook(HitStyleType hitStyleType, Entity opponent) {
    if (tripleDelay == TRIPLE_DELAY) {
      return HitStyleType.MAGIC;
    }
    if (tripleDelay == TRIPLE_DELAY - 1) {
      return HitStyleType.RANGED;
    }
    if (tripleDelay == TRIPLE_DELAY - 2) {
      return HitStyleType.MELEE;
    }
    return hitStyleType;
  }

  @Override
  public NpcCombatStyle attackTickCombatStyleHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (tripleDelay == TRIPLE_DELAY - 2) {
      return SPECIAL_ATTACK;
    }
    return combatStyle;
  }

  @Override
  public int attackTickAttackSpeedHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (tripleDelay == TRIPLE_DELAY
        || tripleDelay == TRIPLE_DELAY - 1
        || tripleDelay == TRIPLE_DELAY - 2) {
      return 2;
    }
    return combatStyle.getAttackSpeed();
  }

  @Override
  public boolean canBeAttackedHook(
      Entity opponent, boolean sendMessage, HitStyleType hitStyleType) {
    if (!opponent.isPlayer()) {
      return false;
    }
    var player = opponent.asPlayer();
    if (isAttacking() && getLastAttackedByEntity() != null && player != getLastAttackedByEntity()) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("Cerberus is busy attacking someone else.");
      }
      return false;
    }
    return true;
  }

  public void checkTriple() {
    if (!isAttacking()) {
      return;
    }
    if (tripleDelay > 0) {
      tripleDelay--;
      return;
    }
    var entity = getAttackingEntity();
    if (!isAttacking() || !npc.withinDistance(entity, 10) || entity.isLocked()) {
      return;
    }
    tripleDelay = TRIPLE_DELAY;
  }

  public void checkSouls() {
    if (soulsDelay > 0) {
      soulsDelay--;
      if (souls == null || souls.isEmpty()) {
        return;
      }
      var entity = getAttackingEntity();
      if (soulsDelay == 49) {
        for (Npc npc : souls) {
          npc.getMovement().clear();
          npc.getMovement().addMovement(npc.getX(), npc.getY() - 9);
        }
      } else if (soulsDelay == 36) {
        if (entity != null) {
          soulAttack(souls.get(0), entity);
        }
      } else if (soulsDelay == 33) {
        if (entity != null) {
          soulAttack(souls.get(1), entity);
        }
        souls.get(0).getMovement().clear();
        souls.get(0).getMovement().addMovement(souls.get(0).getSpawn().getTile());
      } else if (soulsDelay == 30) {
        if (entity != null) {
          soulAttack(souls.get(2), entity);
        }
        souls.get(1).getMovement().clear();
        souls.get(1).getMovement().addMovement(souls.get(1).getSpawn().getTile());
      } else if (soulsDelay == 27) {
        souls.get(2).getMovement().clear();
        souls.get(2).getMovement().addMovement(souls.get(2).getSpawn().getTile());
      } else if (soulsDelay == 16) {
        removeSouls();
      }
    } else {
      startSouls();
    }
  }

  public void soulAttack(Npc soul, Entity entity) {
    if (!(entity instanceof Player) || !soul.withinMapDistance(entity)) {
      return;
    }
    var player = (Player) entity;
    soul.setFaceTile(entity);
    var speed = getProjectileSpeed(entity);
    var hitStyleType = HitStyleType.TYPELESS;
    var prayer = "";
    if (soul.getId() == NpcId.SUMMONED_SOUL_96) {
      soul.setAnimation(8530);
      var projectile =
          Graphic.Projectile.builder().id(11).startTile(soul).entity(entity).speed(speed).build();
      sendMapProjectile(projectile);
      hitStyleType = HitStyleType.RANGED;
      prayer = "protect from missiles";
    } else if (soul.getId() == NpcId.SUMMONED_SOUL_96_5868) {
      soul.setAnimation(8529);
      var projectile =
          Graphic.Projectile.builder().id(100).startTile(soul).entity(entity).speed(speed).build();
      sendMapProjectile(projectile);
      hitStyleType = HitStyleType.MAGIC;
      prayer = "protect from magic";
    } else if (soul.getId() == NpcId.SUMMONED_SOUL_79) {
      soul.setAnimation(8528);
      speed.setClientDelay(speed.getClientDelay() / 2);
      speed.setClientSpeed(speed.getClientSpeed() + speed.getClientDelay());
      var projectile =
          Graphic.Projectile.builder().id(1248).startTile(soul).entity(entity).speed(speed).build();
      sendMapProjectile(projectile);
      hitStyleType = HitStyleType.MELEE;
      prayer = "protect from melee";
    }
    if (player.getPrayer().hasActive(prayer)) {
      if (player.getEquipment().getShieldId() == ItemId.SPECTRAL_SPIRIT_SHIELD) {
        player.getPrayer().changePoints(-15);
      } else {
        player.getPrayer().changePoints(-30);
      }
    } else {
      var hitEvent =
          new HitEvent(speed.getEventDelay(), new Hit(soul, 30, HitMarkType.RED, hitStyleType));
      entity.getCombat().addHitEvent(hitEvent);
    }
  }

  public void startSouls() {
    if (soulsDelay > 0 || getHitpoints() >= 400 || PRandom.randomE(20) != 0) {
      return;
    }
    var entity = getAttackingEntity();
    if (!isAttacking() || !npc.withinDistance(entity, 10) || entity.isLocked()) {
      return;
    }
    npc.getWorld().removeNpcs(souls);
    souls.clear();
    List<Tile> tiles = null;
    if (npc.getRegionId() == 4883) {
      tiles = PCollection.toList(new Tile(1239, 1265), new Tile(1240, 1265), new Tile(1241, 1265));
    } else if (npc.getRegionId() == 5395) {
      tiles = PCollection.toList(new Tile(1367, 1265), new Tile(1368, 1265), new Tile(1369, 1265));
    } else if (npc.getRegionId() == 5140) {
      tiles = PCollection.toList(new Tile(1303, 1329), new Tile(1304, 1329), new Tile(1305, 1329));
    }
    if (tiles == null) {
      return;
    }
    var soulIds =
        PCollection.toList(
            NpcId.SUMMONED_SOUL_96, NpcId.SUMMONED_SOUL_96_5868, NpcId.SUMMONED_SOUL_79);
    Collections.shuffle(soulIds);
    for (var i = 0; i < soulIds.size(); i++) {
      souls.add(
          npc.getController()
              .addNpc(
                  new NpcSpawn(tiles.get(i), soulIds.get(i))
                      .respawnable(npc.getSpawn().isRespawnable())));
    }
    npc.setForceMessage("Aaarrrooooooo");
    soulsDelay = SOULS_DELAY;
  }

  public void removeSouls() {
    npc.getWorld().removeNpcs(souls);
    souls.clear();
  }

  public void checkAOE() {
    if (aoeDelay > 0) {
      aoeDelay--;
      if (aoeAttacks == null || aoeAttacks.isEmpty()) {
        return;
      }
      var entity = getAttackingEntity();
      var withinDistance = npc.withinDistance(entity, 10);
      for (var aoe : aoeAttacks) {
        aoe.tick--;
        if (aoe.tick < 0) {
          continue;
        }
        if (aoe.tick == 1) {
          npc.getController().sendMapGraphic(aoe.tile, new Graphic(1247));
        }
        if (!withinDistance
            || entity.isLocked()
            || aoe.tick - AOE_LENGTH + 2 >= aoe.speed.getEventDelay()) {
          continue;
        }
        if ((aoe.tick % 4) != 0) {
          continue;
        }
        if (entity.withinDistance(aoe.tile, 0)) {
          entity.getCombat().addHit(new Hit(10 + PRandom.randomI(5)));
        } else {
          if (!entity.withinDistance(aoe.tile, 1)) {
            continue;
          }
          entity.getCombat().addHit(new Hit(7));
        }
      }
    } else {
      startAOE();
    }
  }

  public void startAOE() {
    if (aoeDelay > 0 || getHitpoints() >= 200 || PRandom.randomE(20) != 0) {
      return;
    }
    var entity = getAttackingEntity();
    if (!isAttacking() || !npc.withinDistance(entity, 10) || entity.isLocked()) {
      return;
    }
    aoeAttacks.clear();
    var tiles =
        new Tile[] {new Tile(entity), new Tile(entity).randomize(2), new Tile(entity).randomize(4)};
    for (var tile : tiles) {
      var projectile =
          Graphic.Projectile.builder()
              .id(1247)
              .startTile(npc)
              .endTile(tile)
              .speed(getProjectileSpeed(10))
              .build();
      sendMapProjectile(projectile);
      npc.getController().sendMapGraphic(tile, new Graphic(1246, 0, projectile.getContactDelay()));
      aoeAttacks.add(new AoeAttack(tile, projectile.getProjectileSpeed()));
    }
    npc.setForceMessage("Grrrrrrrrrrrrrr");
    aoeDelay = AOE_DELAY + PRandom.randomI(AOE_DELAY);
  }

  private static class AoeAttack {

    private Tile tile;
    private Graphic.ProjectileSpeed speed;
    private int tick;

    private AoeAttack(Tile tile, Graphic.ProjectileSpeed speed) {
      this.tile = tile;
      this.speed = speed;
      tick = speed.getEventDelay() + AOE_LENGTH;
    }
  }
}
