package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.playerplugin.bountyhunter.BountyHunterPlugin;
import com.palidinodh.playerplugin.bountyhunter.MysteriousEmblem;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.EMBLEM_TRADER_316)
class EmblemTraderNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    var plugin = player.getPlugin(BountyHunterPlugin.class);
    if (option.getIndex() == 0) {
      player.openOptionsDialogue(
          new DialogueOption(
              "View shop.",
              (c, s) -> {
                player.openShop("blood_money");
              }),
          new DialogueOption(
              "Exchange mysterious emblems.",
              (c, s) -> {
                for (var i = 0; i < player.getInventory().size(); i++) {
                  var itemId = player.getInventory().getId(i);
                  var coinValue = MysteriousEmblem.getCoinValue(itemId);
                  if (itemId == ItemId.ARCHAIC_EMBLEM_TIER_1_12747 || coinValue == 0) {
                    continue;
                  }
                  var inventoryAmount = player.getInventory().getAmount(i);
                  player.getInventory().deleteItem(itemId, inventoryAmount, i);
                  player.getInventory().addOrDropItem(ItemId.COINS, inventoryAmount * coinValue);
                  var bloodMoneyValue = MysteriousEmblem.getBloodMoneyValue(itemId);
                  player
                      .getInventory()
                      .addOrDropItem(ItemId.BLOOD_MONEY, inventoryAmount * bloodMoneyValue);
                }
              }),
          new DialogueOption(
              "Options.",
              (c, s) -> {
                player.openOldDialogue("wilderness", 1);
              }));
    } else if (option.getIndex() == 2) {
      player.openShop("blood_money");
    } else if (option.getIndex() == 3) {
      player.getCombat().setShowKDR(!player.getCombat().showKDR());
      player.getGameEncoder().sendMessage("Streaks: " + player.getCombat().showKDR());
    } else if (option.getIndex() == 4) {
      player.getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
    }
  }
}
