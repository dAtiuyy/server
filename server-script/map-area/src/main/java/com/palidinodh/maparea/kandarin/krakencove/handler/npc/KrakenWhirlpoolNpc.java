package com.palidinodh.maparea.kandarin.krakencove.handler.npc;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.WHIRLPOOL)
public class KrakenWhirlpoolNpc implements NpcHandler {

  @Override
  public ReachType canReach(Player player, Npc npc) {
    return player.withinDistance(npc, 4) ? ReachType.TRUE : ReachType.DEFAULT;
  }

  @Override
  public void itemOnNpc(Player player, int slot, int itemId, Npc npc) {
    if (npc.isLocked()) {
      return;
    }
    if (itemId != ItemId.FISHING_EXPLOSIVE) {
      return;
    }
    npc.getCombat().script("kraken_combat_emerge", player);
    player.setAnimation(806);
    player.setGraphic(new Graphic(50, 96));
    var projectile =
        Graphic.Projectile.builder()
            .id(49)
            .startTile(player)
            .entity(npc)
            .speed(Graphic.ProjectileSpeed.getMagicProjectile(player.getDistance(npc)))
            .build();
    player.getController().sendMapProjectile(projectile);
  }
}
