package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;
import com.palidinodh.random.PRandom;

class SnakelingPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(new Pet.Entry(ItemId.PET_SNAKELING, NpcId.SNAKELING_2127, NpcId.SNAKELING_2130));
    builder.entry(
        new Pet.Entry(ItemId.PET_SNAKELING_12939, NpcId.SNAKELING_2128, NpcId.SNAKELING_2131));
    builder.entry(
        new Pet.Entry(ItemId.PET_SNAKELING_12940, NpcId.SNAKELING_2129, NpcId.SNAKELING_2132));
    builder.optionVariation(
        (p, n) -> {
          switch (n.getId()) {
            case NpcId.SNAKELING_2127:
              p.getPlugin(FamiliarPlugin.class)
                  .transformPet(PRandom.arrayRandom(NpcId.SNAKELING_2128, NpcId.SNAKELING_2129));
              break;
            case NpcId.SNAKELING_2128:
              p.getPlugin(FamiliarPlugin.class)
                  .transformPet(PRandom.arrayRandom(NpcId.SNAKELING_2127, NpcId.SNAKELING_2129));
              break;
            case NpcId.SNAKELING_2129:
              p.getPlugin(FamiliarPlugin.class)
                  .transformPet(PRandom.arrayRandom(NpcId.SNAKELING_2127, NpcId.SNAKELING_2128));
              break;
          }
        });
    return builder;
  }
}
