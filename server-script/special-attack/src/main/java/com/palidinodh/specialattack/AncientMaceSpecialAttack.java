package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ANCIENT_MACE)
class AncientMaceSpecialAttack extends SpecialAttack {

  AncientMaceSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(100);
    entry.animation(6147);
    entry.castGraphic(new Graphic(1052));
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (!hooks.getOpponent().isPlayer()) {
      return;
    }
    var opponent = hooks.getOpponent().asPlayer();
    var prayerDrain = Math.min(hooks.getDamage(), opponent.getPrayer().getPoints());
    opponent.getPrayer().changePoints(-prayerDrain);
    hooks.getPlayer().getPrayer().changePoints(prayerDrain);
  }
}
