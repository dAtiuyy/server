package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropLocationType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;
import lombok.Setter;

class TztokJadCombat extends NpcCombat {

  @Inject private Npc npc;
  private HitStyleType currentHitStyle = HitStyleType.MAGIC;
  private Npc[] healers;
  @Setter private boolean spawnHealersNorth;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.TZTOK_JAD_702);
    combat.hitpoints(NpcCombatHitpoints.total(250));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(640)
            .magicLevel(480)
            .rangedLevel(960)
            .defenceLevel(480)
            .bonus(BonusType.ATTACK_MAGIC, 60)
            .build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.immunity(NpcCombatImmunity.builder().venom(true).build());
    combat.slayer(NpcCombatSlayer.builder().experience(25_250).build());
    combat.killCount(NpcCombatKillCount.builder().sendMessage(true).build());
    combat.deathAnimation(2654);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(97));
    style.animation(2655).attackSpeed(8);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(97));
    style.animation(2652).attackSpeed(8).attackRange(14).applyAttackDelay(4);
    style.targetTileGraphic(new Graphic(451));
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(97));
    style.animation(2656).attackSpeed(8).attackRange(14).applyAttackDelay(4);
    style.castGraphic(new Graphic(447, 500));
    style.projectile(NpcCombatProjectile.builder().id(448).startHeight(124).delay(0).build());
    combat.style(style.build());

    var combatLevel900 = NpcCombatDefinition.builder();
    combatLevel900.id(NpcId.JALTOK_JAD_900);
    combatLevel900.hitpoints(NpcCombatHitpoints.total(350));
    combatLevel900.stats(
        NpcCombatStats.builder()
            .attackLevel(750)
            .magicLevel(510)
            .rangedLevel(1020)
            .defenceLevel(480)
            .bonus(BonusType.ATTACK_STAB, 80)
            .bonus(BonusType.ATTACK_SLASH, 80)
            .bonus(BonusType.ATTACK_CRUSH, 80)
            .bonus(BonusType.ATTACK_MAGIC, 100)
            .bonus(BonusType.ATTACK_RANGED, 80)
            .build());
    combatLevel900.aggression(
        NpcCombatAggression.builder().range(8).always(true).forceable(false).build());
    combatLevel900.immunity(NpcCombatImmunity.builder().venom(true).build());
    combatLevel900.focus(NpcCombatFocus.builder().bypassMapObjects(true).build());
    combatLevel900.deathAnimation(7594);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(113));
    style.animation(7590).attackSpeed(8);
    combatLevel900.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(113));
    style.animation(7593).attackSpeed(8).attackRange(14).applyAttackDelay(4);
    style.targetTileGraphic(new Graphic(451));
    combatLevel900.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(113));
    style.animation(7592).attackSpeed(8).attackRange(14).applyAttackDelay(4);
    style.projectile(NpcCombatProjectile.builder().id(448).startHeight(124).delay(0).build());
    combatLevel900.style(style.build());

    var combatLevel900_2 = NpcCombatDefinition.builder();
    combatLevel900_2.id(NpcId.JALTOK_JAD_900_7704);
    combatLevel900_2.hitpoints(NpcCombatHitpoints.total(350));
    combatLevel900_2.stats(
        NpcCombatStats.builder()
            .attackLevel(750)
            .magicLevel(510)
            .rangedLevel(1020)
            .defenceLevel(480)
            .bonus(BonusType.ATTACK_STAB, 80)
            .bonus(BonusType.ATTACK_SLASH, 80)
            .bonus(BonusType.ATTACK_CRUSH, 80)
            .bonus(BonusType.ATTACK_MAGIC, 100)
            .bonus(BonusType.ATTACK_RANGED, 80)
            .build());
    combatLevel900_2.aggression(
        NpcCombatAggression.builder().range(8).always(true).forceable(false).build());
    combatLevel900_2.immunity(NpcCombatImmunity.builder().venom(true).build());
    combatLevel900_2.focus(NpcCombatFocus.builder().bypassMapObjects(true).build());
    combatLevel900_2.deathAnimation(7594);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(113));
    style.animation(7590).attackSpeed(9);
    combatLevel900_2.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(113));
    style.animation(7593).attackSpeed(9).attackRange(14).applyAttackDelay(4);
    style.targetTileGraphic(new Graphic(451));
    combatLevel900_2.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(113));
    style.animation(7592).attackSpeed(9).attackRange(14).applyAttackDelay(4);
    style.projectile(NpcCombatProjectile.builder().id(448).startHeight(124).delay(0).build());
    combatLevel900_2.style(style.build());

    var eventDrop =
        NpcCombatDrop.builder()
            .locationType(NpcCombatDropLocationType.INVENTORY)
            .pet(ItemId.TINY_MELEE_JAD_60044, 200)
            .pet(ItemId.TINY_RANGED_JAD_60045, 200)
            .pet(ItemId.TINY_MAGIC_JAD_60046, 200)
            .additionalPlayers(255)
            .additionalDropRateDivider(0);
    var dropTable =
        NpcCombatDropTable.builder().probabilityDenominator(900).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TZKAL_SLAYER_HELMET)));
    eventDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(800).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.INFERNAL_CAPE)));
    eventDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().broadcast(true).probabilityDenominator(300).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TZTOK_SLAYER_HELMET)));
    eventDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().broadcast(true).probabilityDenominator(200);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_ONYX)));
    eventDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(50).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.KRAKEN_ORNAMENT_KIT_60059)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ACB_ORNAMENT_KIT_60060)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MTA_ORNAMENT_KIT_60061)));
    eventDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(2);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOKTZ_XIL_AK)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOKTZ_XIL_EK)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOKTZ_MEJ_TAL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TZHAAR_KET_EM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TZHAAR_KET_OM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOKTZ_XIL_UL, 100)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOKTZ_KET_XIL)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OBSIDIAN_CAPE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OBSIDIAN_HELMET)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OBSIDIAN_PLATEBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.OBSIDIAN_PLATELEGS)));
    eventDrop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOKKUL, 8032, 16064)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_CAPE)));
    eventDrop.table(dropTable.build());

    var eventCombat = NpcCombatDefinition.builder();
    eventCombat.id(NpcId.JAD_900_16065).id(NpcId.JAD_900_16066).id(NpcId.JAD_900_16068);
    eventCombat.hitpoints(
        NpcCombatHitpoints.builder().total(8_000).barType(HitpointsBarType.GREEN_RED_160).build());
    eventCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(750)
            .magicLevel(510)
            .rangedLevel(1020)
            .defenceLevel(120)
            .bonus(BonusType.ATTACK_STAB, 80)
            .bonus(BonusType.ATTACK_SLASH, 80)
            .bonus(BonusType.ATTACK_CRUSH, 80)
            .bonus(BonusType.ATTACK_MAGIC, 100)
            .bonus(BonusType.ATTACK_RANGED, 80)
            .build());
    eventCombat.aggression(
        NpcCombatAggression.builder().range(8).always(true).forceable(false).build());
    eventCombat.immunity(NpcCombatImmunity.builder().venom(true).build());
    eventCombat.focus(NpcCombatFocus.builder().bypassMapObjects(true).build());
    eventCombat.killCount(
        NpcCombatKillCount.builder().asName("Event Jad").sendMessage(true).build());
    eventCombat.drop(eventDrop.build());
    eventCombat.deathAnimation(7594);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(28));
    style.animation(7590).attackSpeed(8);
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    eventCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(28));
    style.animation(7593).attackSpeed(8).attackRange(14).applyAttackDelay(4);
    style.targetTileGraphic(new Graphic(451));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    eventCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(28));
    style.animation(7592).attackSpeed(8).attackRange(14).applyAttackDelay(4);
    style.projectile(NpcCombatProjectile.builder().id(448).startHeight(124).delay(0).build());
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    eventCombat.style(style.build());

    return Arrays.asList(
        combat.build(), combatLevel900.build(), combatLevel900_2.build(), eventCombat.build());
  }

  @Override
  public void spawnHook() {
    if (!isHitDelayed()) {
      setHitDelay(8);
    }
  }

  @Override
  public void despawnHook() {
    if (healers != null) {
      for (var npc2 : healers) {
        npc.getWorld().removeNpc(npc2);
      }
      healers = null;
    }
  }

  private void checkHealers() {
    if (npc.getId() == NpcId.JAD_900_16065
        || npc.getId() == NpcId.JAD_900_16066
        || npc.getId() == NpcId.JAD_900_16068) {
      return;
    }
    if (getHitpoints() >= getMaxHitpoints() / 2) {
      return;
    }
    if (isDead()) {
      return;
    }
    if (healers != null) {
      return;
    }
    if (!(getAttackingEntity() instanceof Player)) {
      return;
    }
    var player = (Player) getAttackingEntity();
    var healerId = -1;
    switch (npc.getId()) {
      case NpcId.TZTOK_JAD_702:
      case NpcId.TZTOK_JAD_702_6506:
        {
          healerId = NpcId.YT_HURKOT_108;
          healers = new Npc[4];
          break;
        }
      case NpcId.JALTOK_JAD_900:
      case NpcId.JALTOK_JAD_900_7704:
        {
          healerId = NpcId.YT_HURKOT_141;
          healers = new Npc[npc.getId() == NpcId.JALTOK_JAD_900 ? 5 : 3];
          break;
        }
    }
    if (healerId == -1) {
      return;
    }
    for (int i = 0; i < healers.length; i++) {
      int[] coords;
      if (player.getCombat().getTzHaar().getFightCaveWave() > 0) {
        coords = player.getCombat().getTzHaar().getFightCaveCoords(healerId);
      } else {
        if (spawnHealersNorth) {
          coords = new int[] {npc.getX() + PRandom.randomI(4), npc.getY() + 4 + PRandom.randomI(2)};
        } else {
          coords =
              new int[] {npc.getX() - 4 + PRandom.randomI(8), npc.getY() - 4 + PRandom.randomI(8)};
        }
      }
      if (coords == null) {
        break;
      }
      healers[i] =
          npc.getController()
              .addNpc(new NpcSpawn(new Tile(coords[0], coords[1], npc.getHeight()), healerId));
      healers[i].getMovement().setClipNpcs(true);
      healers[i].getCombat().script("jad", npc);
    }
  }

  @Override
  public void tickStartHook() {
    if (!isHitDelayed()) {
      currentHitStyle = getHitStyleType(getAttackingEntity(), null);
      if (isEventJad()) {
        switch (currentHitStyle) {
          case MELEE:
            npc.setId(NpcId.JAD_900_16065);
            break;
          case RANGED:
            npc.setId(NpcId.JAD_900_16066);
            break;
          case MAGIC:
            npc.setId(NpcId.JAD_900_16068);
            break;
        }
      }
      switch (npc.getId()) {
        case NpcId.JAD_900_16065:
        case NpcId.JAD_900_16066:
        case NpcId.JAD_900_16068:
      }
    }
    checkHealers();
  }

  @Override
  public HitStyleType attackTickHitStyleTypeHook(HitStyleType hitStyleType, Entity opponent) {
    return currentHitStyle;
  }

  @Override
  public Graphic.Projectile mapProjectileHook(Graphic.Projectile projectile) {
    if (projectile.getId() == 448) {
      npc.getController()
          .sendMapProjectile(
              projectile
                  .rebuilder()
                  .id(449)
                  .speed(
                      new Graphic.ProjectileSpeed(
                          projectile.getEventDelay(),
                          projectile.getDelay(),
                          projectile.getSpeed() + 10))
                  .build());
      npc.getController()
          .sendMapProjectile(
              projectile
                  .rebuilder()
                  .id(450)
                  .speed(
                      new Graphic.ProjectileSpeed(
                          projectile.getEventDelay(),
                          projectile.getDelay(),
                          projectile.getSpeed() + 20))
                  .build());
    }
    return projectile;
  }

  @Override
  public void applyDeadEndHook() {
    if (!isEventJad()) {
      return;
    }
    npc.getController()
        .getPlayers()
        .forEach(
            p -> {
              if (p.isLocked()) {
                return;
              }
              p.getController().stop();
            });
  }

  private boolean isEventJad() {
    switch (npc.getId()) {
      case NpcId.JAD_900_16065:
      case NpcId.JAD_900_16066:
      case NpcId.JAD_900_16068:
        return true;
    }
    return false;
  }
}
