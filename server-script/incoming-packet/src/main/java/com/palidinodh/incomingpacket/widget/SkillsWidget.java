package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.Settings;

@ReferenceId({WidgetId.SKILLS, WidgetId.SKILL_GUIDE})
class SkillsWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (player.isLocked()) {
      return;
    }
    if (widgetId == WidgetId.SKILLS) {
      tab(player, childId);
    } else if (widgetId == WidgetId.SKILL_GUIDE) {
      if (childId >= 11 && childId <= 24) {
        int guideId = player.getAttributeInt("skill_guide_id");
        int guideIndex = childId - 11;
        if (guideIndex >= Skills.GUIDE_CONFIGS[guideId].length) {
          return;
        }
        player.getGameEncoder().setVarp(965, Skills.GUIDE_CONFIGS[guideId][guideIndex]);
      }
    }
  }

  public void tab(Player player, int childId) {
    int guideId = -1;
    switch (childId) {
      case 1:
        guideId = Skills.ATTACK;
        break;
      case 2:
        guideId = Skills.STRENGTH;
        break;
      case 3:
        guideId = Skills.DEFENCE;
        break;
      case 4:
        guideId = Skills.RANGED;
        break;
      case 5:
        guideId = Skills.PRAYER;
        break;
      case 6:
        guideId = Skills.MAGIC;
        break;
      case 7:
        guideId = Skills.RUNECRAFTING;
        break;
      case 8:
        guideId = Skills.CONSTRUCTION;
        break;
      case 9:
        guideId = Skills.HITPOINTS;
        break;
      case 10:
        guideId = Skills.AGILITY;
        break;
      case 11:
        guideId = Skills.HERBLORE;
        break;
      case 12:
        guideId = Skills.THIEVING;
        break;
      case 13:
        guideId = Skills.CRAFTING;
        break;
      case 14:
        guideId = Skills.FLETCHING;
        break;
      case 15:
        guideId = Skills.SLAYER;
        break;
      case 16:
        guideId = Skills.HUNTER;
        break;
      case 17:
        guideId = Skills.MINING;
        break;
      case 18:
        guideId = Skills.SMITHING;
        break;
      case 19:
        guideId = Skills.FISHING;
        break;
      case 20:
        guideId = Skills.COOKING;
        break;
      case 21:
        guideId = Skills.FIREMAKING;
        break;
      case 22:
        guideId = Skills.WOODCUTTING;
        break;
      case 23:
        guideId = Skills.FARMING;
        break;
    }
    if (guideId == -1) {
      return;
    }
    player.getWidgetManager().removeInteractiveWidgets();
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.SKILL_GUIDE);
    player.getGameEncoder().sendWidgetSettings(WidgetId.SKILL_GUIDE, 8, 0, 99, 0);
    player.getGameEncoder().setVarp(965, Skills.GUIDE_CONFIGS[guideId][0]);
    player.putAttribute("skill_guide_id", guideId);
    if (guideId < player.getSkills().getXPLocks().length || guideId == Skills.PRAYER) {
      var finalGuideId = guideId;
      if (Settings.getInstance().isSpawn()) {
        player.openOptionsDialogue(
            new DialogueOption(
                "Set Level",
                (c, s) -> {
                  player
                      .getGameEncoder()
                      .sendEnterAmount(
                          value -> {
                            player.getSkills().changeCombatXP(finalGuideId, value);
                          });
                }),
            new DialogueOption(
                "Toggle XP Lock",
                (c, s) -> {
                  if (player.getSkills().getXPLocks()[finalGuideId] == Skills.DISABLED_XP) {
                    player.getSkills().setXPLock(finalGuideId, Skills.NORMAL_XP);
                    player
                        .getGameEncoder()
                        .sendMessage(Skills.SKILL_NAMES[finalGuideId] + " XP rate normal.");
                  } else {
                    player.getSkills().setXPLock(finalGuideId, Skills.DISABLED_XP);
                    player
                        .getGameEncoder()
                        .sendMessage(Skills.SKILL_NAMES[finalGuideId] + " XP locked.");
                  }
                }));
      } else {
        player.openDialogue(
            new OptionsDialogue(
                new DialogueOption(
                    "Normal XP",
                    (c, s) -> {
                      player.getSkills().setXPLock(finalGuideId, Skills.NORMAL_XP);
                      player
                          .getGameEncoder()
                          .sendMessage(Skills.SKILL_NAMES[finalGuideId] + " XP rate normal.");
                    }),
                new DialogueOption(
                    "RS XP",
                    (c, s) -> {
                      player.getSkills().setXPLock(finalGuideId, Skills.X1_XP);
                      player
                          .getGameEncoder()
                          .sendMessage(Skills.SKILL_NAMES[finalGuideId] + " XP rate x1 (RS).");
                    }),
                new DialogueOption(
                    "Toggle XP Lock",
                    (c, s) -> {
                      if (player.getSkills().getXPLocks()[finalGuideId] == Skills.DISABLED_XP) {
                        player.getSkills().setXPLock(finalGuideId, Skills.NORMAL_XP);
                        player
                            .getGameEncoder()
                            .sendMessage(Skills.SKILL_NAMES[finalGuideId] + " XP rate normal.");
                      } else {
                        player.getSkills().setXPLock(finalGuideId, Skills.DISABLED_XP);
                        player
                            .getGameEncoder()
                            .sendMessage(Skills.SKILL_NAMES[finalGuideId] + " XP locked.");
                      }
                    }),
                new DialogueOption(
                    "Reset Level",
                    (c, s) -> {
                      if (player.getController().getLevelForXP(finalGuideId) == 1) {
                        player.getGameEncoder().sendMessage("You can't reset this skill.");
                        return;
                      }
                      if (player.getInventory().getCount(ItemId.COINS) < 500_000) {
                        player.getGameEncoder().sendMessage("You need 500k coins to do this.");
                        return;
                      }
                      if (player.getBank().pinRequired(false)) {
                        return;
                      }
                      if (!player.getSkills().changeCombatXP(finalGuideId, 1)) {
                        return;
                      }
                      player.getInventory().deleteItem(ItemId.COINS, 500_000);
                    })));
      }
    }
  }
}
