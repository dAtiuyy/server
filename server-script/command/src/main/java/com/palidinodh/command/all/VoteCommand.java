package com.palidinodh.command.all;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName({"vote", "claimvote", "claimvotes"})
class VoteCommand implements CommandHandler {

  @Override
  public String getExample(String name) {
    return "- Opens up vote controls";
  }

  @Override
  public void execute(Player player, String name, String message) {
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.VOTING_1020);
  }
}
