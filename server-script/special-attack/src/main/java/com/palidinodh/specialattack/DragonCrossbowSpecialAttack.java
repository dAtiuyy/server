package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.DRAGON_CROSSBOW)
class DragonCrossbowSpecialAttack extends SpecialAttack {

  DragonCrossbowSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(60);
    entry.animation(4230);
    entry.impactGraphic(new Graphic(157, 96));
    entry.projectileId(698);
    entry.castSound(new Sound(1080));
    entry.impactSound(new Sound(163));
    entry.damageModifier(1.2);
    addEntry(entry);
  }
}
