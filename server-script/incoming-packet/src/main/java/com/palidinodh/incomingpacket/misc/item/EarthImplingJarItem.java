package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.EARTH_IMPLING_JAR)
class EarthImplingJarItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.remove();
    var randomItems =
        RandomItem.buildList(
            new RandomItem(1442, 1) /* Fire talisman */,
            new RandomItem(1440, 1) /* Earth talisman */,
            new RandomItem(5535, 1) /* Earth tiara */,
            new RandomItem(557, 32) /* Earth rune */,
            new RandomItem(448, 1, 3) /* Mithril ore (noted) */,
            new RandomItem(237, 1) /* Unicorn horn */,
            new RandomItem(2353, 1) /* Steel bar */,
            new RandomItem(1273, 1) /* Mithril pickaxe */,
            new RandomItem(5311, 2) /* Wildblood seed */,
            new RandomItem(5104, 2) /* Jangerberry seed */,
            new RandomItem(6033, 6) /* Compost (noted) */,
            new RandomItem(6035, 2) /* Supercompost (noted) */,
            new RandomItem(1784, 4) /* Bucket of sand (noted) */,
            new RandomItem(5294, 2) /* Harralander seed */,
            new RandomItem(454, 2) /* Coal (noted) */,
            new RandomItem(444, 1) /* Gold ore */,
            new RandomItem(1622, 2) /* Uncut emerald (noted) */,
            new RandomItem(1606, 2) /* Emerald (noted) */,
            new RandomItem(1603, 1) /* Ruby */,
            new RandomItem(2801, 1, 1).weight(4) /* Clue scroll (medium) */);
    player.getInventory().addOrDropItem(RandomItem.getItem(randomItems));
  }
}
