package com.palidinodh.npcplugin;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.NpcPlugin;
import com.palidinodh.osrscore.model.entity.shared.Movement;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  NpcId.BABY_IMPLING,
  NpcId.YOUNG_IMPLING,
  NpcId.GOURMET_IMPLING,
  NpcId.EARTH_IMPLING,
  NpcId.ESSENCE_IMPLING,
  NpcId.ECLECTIC_IMPLING,
  NpcId.NATURE_IMPLING,
  NpcId.MAGPIE_IMPLING,
  NpcId.NINJA_IMPLING,
  NpcId.DRAGON_IMPLING,
  NpcId.LUCKY_IMPLING
})
class ImplingNpcPlugin implements NpcPlugin {

  @Inject private Npc npc;
  private int transformDelay;

  @Override
  public void spawn() {
    transform(true);
    npc.getMovement().setType(Movement.Type.FLY);
    npc.getCombat().setSpecialBinding(true);
  }

  @Override
  public void tick() {
    if (npc.getController().canMagicBind() && transformDelay > 0) {
      transformDelay--;
      if (transformDelay == 0) {
        transform(false);
      }
    }
  }

  public void transform(boolean fromSpawn) {
    npc.setId(getNpcId());
    transformDelay = 100 + PRandom.randomI(100);
  }

  public int getNpcId() {
    if (PRandom.randomE(24) == 0) {
      return NpcId.LUCKY_IMPLING;
    } else if (PRandom.randomE(12) == 0) {
      return NpcId.DRAGON_IMPLING;
    } else if (PRandom.randomE(9) == 0) {
      return NpcId.NINJA_IMPLING;
    } else if (PRandom.randomE(8) == 0) {
      return NpcId.MAGPIE_IMPLING;
    } else if (PRandom.randomE(7) == 0) {
      return NpcId.NATURE_IMPLING;
    } else if (PRandom.randomE(6) == 0) {
      return NpcId.ECLECTIC_IMPLING;
    } else if (PRandom.randomE(5) == 0) {
      return NpcId.ESSENCE_IMPLING;
    } else if (PRandom.randomE(4) == 0) {
      return NpcId.EARTH_IMPLING;
    } else if (PRandom.randomE(3) == 0) {
      return NpcId.GOURMET_IMPLING;
    } else if (PRandom.randomE(2) == 0) {
      return NpcId.YOUNG_IMPLING;
    }
    return NpcId.BABY_IMPLING;
  }
}
