package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.PROSPECTOR_OUTFIT_MINING_32292)
class ProspectorOutfitMiningItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (player.getInventory().getRemainingSlots() < 4 - 1) {
      player.getInventory().notEnoughSpace();
      return;
    }
    item.remove();
    player.getInventory().addItem(ItemId.PROSPECTOR_HELMET);
    player.getInventory().addItem(ItemId.PROSPECTOR_JACKET);
    player.getInventory().addItem(ItemId.PROSPECTOR_LEGS);
    player.getInventory().addItem(ItemId.PROSPECTOR_BOOTS);
  }
}
