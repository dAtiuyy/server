package com.palidinodh.maparea.abyssalspace.abyssalnexus.handler.mapobject;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Prayer;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.THE_FONT_OF_CONSUMPTION)
class TheFontOfConsumptionMapObject implements MapObjectHandler {

  private static int getItemId(Player player) {
    if (PRandom.randomE(128) < 5) {
      return ItemId.ABYSSAL_ORPHAN;
    }
    if (PRandom.randomE(128) < 10) {
      return ItemId.ABYSSAL_HEAD;
    }
    if (PRandom.randomE(128) < 12) {
      return ItemId.ABYSSAL_WHIP;
    }
    if (PRandom.randomE(128) < 13) {
      return ItemId.JAR_OF_MIASMA;
    }
    if (PRandom.randomE(128) < 26) {
      return ItemId.ABYSSAL_DAGGER;
    }
    if (!player.hasItem(ItemId.BLUDGEON_SPINE)) {
      return ItemId.BLUDGEON_SPINE;
    }
    if (!player.hasItem(ItemId.BLUDGEON_CLAW)) {
      return ItemId.BLUDGEON_CLAW;
    }
    if (!player.hasItem(ItemId.BLUDGEON_AXON)) {
      return ItemId.BLUDGEON_AXON;
    }
    return PRandom.arrayRandom(ItemId.BLUDGEON_SPINE, ItemId.BLUDGEON_CLAW, ItemId.BLUDGEON_AXON);
  }

  @Override
  public void itemOnMapObject(Player player, Item item, MapObject mapObject) {
    if (item.getId() != ItemId.UNSIRED && item.getId() != ItemId.CURSED_UNSIRED_60009) {
      player.getGameEncoder().sendMessage("Nothing interesting happens.");
      return;
    }
    player.setAnimation(Prayer.PRAY_ANIMATION);
    player.getController().sendMapGraphic(new Tile(3039, 4774), new Graphic(1276));
    var droppedId = getItemId(player);
    if (item.getId() == ItemId.CURSED_UNSIRED_60009) {
      var cursedUnsiredKillCount =
          player.getInventory().getAttachment(item.getSlot()) != null
              ? (int) player.getInventory().getAttachment(item.getSlot())
              : 0;
      if (cursedUnsiredKillCount > 0) {
        player.getCombat().logNPCItem("Abyssal Sire", droppedId, 1, cursedUnsiredKillCount);
      } else {
        player.getCombat().logNPCItem("Abyssal Sire", droppedId, 1);
        cursedUnsiredKillCount++;
      }
      player.getCombat().logNPCItem("Cursed abyssal demon", droppedId, 1, cursedUnsiredKillCount);
      player.getWorld().sendItemDropNews(player, new Item(droppedId), "from a Cursed unsired");
    } else {
      var unsiredKillCount =
          player.getInventory().getAttachment(item.getSlot()) != null
              ? (int) player.getInventory().getAttachment(item.getSlot())
              : 0;
      if (unsiredKillCount > 0) {
        player.getCombat().logNPCItem("Abyssal Sire", droppedId, 1, unsiredKillCount);
      } else {
        player.getCombat().logNPCItem("Abyssal Sire", droppedId, 1);
        unsiredKillCount++;
      }
      player.getWorld().sendItemDropNews(player, new Item(droppedId), "from an Unsired");
    }
    item.replace(new Item(droppedId));
  }
}
