package com.palidinodh.maparea.kandarin.ardougne.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.map.Region;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.DOOR_11724)
class DoorMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (mapObject.isBusy()) {
      return;
    }
    if (!player.getInventory().hasItem(ItemId.LOCKPICK)) {
      player.getGameEncoder().sendMessage("You need a lockpick to do this.");
      return;
    }
    player.getMovement().clear();
    switch (mapObject.getDirection()) {
      case 1:
        if (player.getY() <= mapObject.getY()) {
          player.getMovement().addMovement(mapObject.getX(), mapObject.getY() + 1);
        } else {
          player.getMovement().addMovement(mapObject.getX(), mapObject.getY());
        }
        break;
      case 3:
        if (player.getY() >= mapObject.getY()) {
          player.getMovement().addMovement(mapObject.getX(), mapObject.getY() - 1);
        } else {
          player.getMovement().addMovement(mapObject.getX(), mapObject.getY());
        }
        break;
      default:
        return;
    }
    Region.openDoors(player, mapObject, 1, false);
  }
}
