package com.palidinodh.maparea.asgarnia.donator.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.WidgetSetting;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.WIZARD_16048)
class DzoneWizardNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    if (option.getIndex() == 0) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.TELEPORTS_1028);
    } else if (option.getIndex() == 3) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.JEWELLERY_BOX);
      player
          .getGameEncoder()
          .sendScript(ScriptId.POH_JEWELLERY_BOX_INIT, 15, "Ornate Jewellery Box", 3);
      player
          .getGameEncoder()
          .sendWidgetSettings(WidgetId.JEWELLERY_BOX, 0, 0, 24, WidgetSetting.OPTION_0);
    }
  }

  @Override
  public void itemOnNpc(Player player, int slot, int itemId, Npc npc) {
    player.getPlugin(FamiliarPlugin.class).insurePet(itemId);
  }
}
