package com.palidinodh.playerplugin.loot;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum LootTakeType {
  ONE(1, 25),
  FIVE(5, 27),
  TEN(10, 29),
  ALL(28, 31);

  private final int quantity;
  private final int childId;

  public static LootTakeType getByChildId(int childId) {
    for (var type : values()) {
      if (childId != type.getChildId()) {
        continue;
      }
      return type;
    }
    return null;
  }
}
