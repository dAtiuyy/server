package com.palidinodh.playerplugin.tzhaar.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.MAGES_BOOK_OR_60051)
class JadMagesBookItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "revert":
        {
          item.remove();
          player.getInventory().addItem(ItemId.MAGES_BOOK);
          player.getInventory().addItem(ItemId.MTA_ORNAMENT_KIT_60061);
          break;
        }
    }
  }
}
