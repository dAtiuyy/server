package com.palidinodh.maparea.wilderness.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;

@ReferenceId({ObjectId.CAVERN_31555, ObjectId.CAVERN_31556, ObjectId.CAVERN_40386})
class RevenantCavesEntranceMapObject implements MapObjectHandler {

  private static void enter(Player player, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.CAVERN_31555:
        player.getMovement().teleport(new Tile(3196, 10056));
        break;
      case ObjectId.CAVERN_31556:
        player.getMovement().teleport(new Tile(3241, 10234));
        break;
      case ObjectId.CAVERN_40386:
        player.getMovement().teleport(new Tile(3187, 10128));
        break;
    }
  }

  private static void payFeeAndEnter(Player player, MapObject mapObject) {
    var inventoryCount = player.getInventory().getCount(ItemId.COINS);
    var bankCount = (int) Math.min(player.getBank().getCount(ItemId.COINS), Integer.MAX_VALUE);
    var total = PNumber.addInt(inventoryCount, bankCount);
    if (inventoryCount + bankCount < WildernessPlugin.REVENANT_CAVE_FEE) {
      player.getGameEncoder().sendMessage("You don't have enough coins to pay the fee.");
      return;
    }
    var remaining = WildernessPlugin.REVENANT_CAVE_FEE;
    var fromInventory = Math.min(inventoryCount, remaining);
    remaining -= fromInventory;
    var fromBank = Math.min(bankCount, remaining);
    player.getInventory().deleteItem(ItemId.COINS, fromInventory);
    player.getBank().deleteItem(ItemId.COINS, fromBank);
    player.getPlugin(WildernessPlugin.class).setPayedRevenantFee(true);
    enter(player, mapObject);
  }

  private static void infoDialogue(Player player, MapObject mapObject) {
    player.openDialogue(
        new MessageDialogue(
            "You need to pay a "
                + PNumber.formatNumber(WildernessPlugin.REVENANT_CAVE_FEE)
                + " coins fee to enter the Revenant Cave.<br>This can be taken from your inventory, bank or both.",
            (c, s) -> {
              payFeeDialogue(player, mapObject);
            }));
  }

  private static void payFeeDialogue(Player player, MapObject mapObject) {
    player.openDialogue(
        new OptionsDialogue(
            "Pay " + PNumber.formatNumber(WildernessPlugin.REVENANT_CAVE_FEE) + " coins Entry Fee?",
            new DialogueOption(
                "Yes.",
                (c, s) -> {
                  payFeeAndEnter(player, mapObject);
                }),
            new DialogueOption(
                "Yes, don't ask again.",
                (c, s) -> {
                  player.getPlugin(WildernessPlugin.class).setAutoPayRevenantFee(false);
                  payFeeAndEnter(player, mapObject);
                }),
            new DialogueOption("No.")));
  }

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.getController().isMagicBound()) {
      player
          .getGameEncoder()
          .sendMessage(
              "A magical force stops you from moving for "
                  + player.getMovement().getMagicBindSeconds()
                  + " more seconds.");
      return;
    }
    var plugin = player.getPlugin(WildernessPlugin.class);
    if (plugin.isPayedRevenantFee()) {
      enter(player, mapObject);
    } else {
      if (plugin.isAutoPayRevenantFee()) {
        payFeeAndEnter(player, mapObject);
      } else {
        infoDialogue(player, mapObject);
      }
    }
  }
}
