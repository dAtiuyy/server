package com.palidinodh.playerplugin.combat.handler.item;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.MASTER_WAND)
class MasterWandItem implements ItemHandler {

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    if (ItemHandler.used(useItem, onItem, ItemId.MASTER_WAND, ItemId.MTA_ORNAMENT_KIT_60061)) {
      onItem.replace(new Item(ItemId.MASTER_WAND_OR_60050));
      useItem.remove();
      return true;
    }
    return false;
  }
}
