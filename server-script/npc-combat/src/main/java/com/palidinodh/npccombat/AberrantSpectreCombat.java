package com.palidinodh.npccombat;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import java.util.Arrays;
import java.util.List;

class AberrantSpectreCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().gemDropTableDenominator(25).clue(ClueScrollType.HARD, 128);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(512);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_ROBE_BOTTOM_DARK)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DWARF_WEED_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPDRAGON_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORSTOL_SEED)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CADANTINE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.KWUARM_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.POISON_IVY_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LANTADYME_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_DWARF_WEED, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_AXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_KITESHIELD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAVA_BATTLESTAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_PLATELEGS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_FULL_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 460)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BELLADONNA_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CACTUS_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.IRIT_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOADFLAX_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AVANTOE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_GUAM_LEAF, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_MARRENTILL, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TARROMIN, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_HARRALANDER, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_IRIT_LEAF, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM, 1, 3)));
    drop.table(dropTable.build());

    var normalCombat = NpcCombatDefinition.builder();
    normalCombat.id(NpcId.ABERRANT_SPECTRE_96);
    normalCombat.hitpoints(NpcCombatHitpoints.total(90));
    normalCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(105)
            .defenceLevel(90)
            .bonus(BonusType.MELEE_DEFENCE, 20)
            .bonus(BonusType.DEFENCE_RANGED, 15)
            .build());
    normalCombat.slayer(
        NpcCombatSlayer.builder().level(60).superiorId(NpcId.ABHORRENT_SPECTRE_253).build());
    normalCombat.aggression(NpcCombatAggression.PLAYERS);
    normalCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(1508)
        .blockAnimation(1509);
    normalCombat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(11));
    style.animation(1507).attackSpeed(4);
    style.targetGraphic(new Graphic(336, 300));
    style.projectile(NpcCombatProjectile.id(335));
    normalCombat.style(style.build());

    var catacombsCombat = NpcCombatDefinition.builder();
    catacombsCombat.id(NpcId.DEVIANT_SPECTRE_169);
    catacombsCombat.hitpoints(NpcCombatHitpoints.total(190));
    catacombsCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(205)
            .defenceLevel(90)
            .bonus(BonusType.MELEE_DEFENCE, 80)
            .bonus(BonusType.DEFENCE_RANGED, 85)
            .build());
    catacombsCombat.slayer(
        NpcCombatSlayer.builder().level(60).superiorId(NpcId.REPUGNANT_SPECTRE_335).build());
    catacombsCombat.aggression(NpcCombatAggression.PLAYERS);
    catacombsCombat.killCount(NpcCombatKillCount.builder().asName("Aberrant spectre").build());
    catacombsCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(1508)
        .blockAnimation(1509);
    catacombsCombat.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(21));
    style.animation(1507).attackSpeed(4);
    style.targetGraphic(new Graphic(336, 300));
    style.projectile(NpcCombatProjectile.id(335));
    catacombsCombat.style(style.build());

    var superiorCombat = NpcCombatDefinition.builder();
    superiorCombat.id(NpcId.ABHORRENT_SPECTRE_253);
    superiorCombat.hitpoints(
        NpcCombatHitpoints.builder().total(250).barType(HitpointsBarType.GREEN_RED_60).build());
    superiorCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(300)
            .defenceLevel(180)
            .bonus(BonusType.MELEE_DEFENCE, 40)
            .bonus(BonusType.DEFENCE_RANGED, 30)
            .build());
    superiorCombat.slayer(NpcCombatSlayer.builder().level(60).experience(2500).build());
    superiorCombat.aggression(NpcCombatAggression.PLAYERS);
    superiorCombat.killCount(
        NpcCombatKillCount.builder().asName("Superior Slayer Creature").build());
    superiorCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(6369)
        .blockAnimation(6370);
    superiorCombat.drop(drop.rolls(3).build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(31));
    style.animation(6368).attackSpeed(4);
    style.targetGraphic(new Graphic(336, 300));
    style.projectile(NpcCombatProjectile.id(335));
    superiorCombat.style(style.build());

    var superiorCatacombsCombat = NpcCombatDefinition.builder();
    superiorCatacombsCombat.id(NpcId.REPUGNANT_SPECTRE_335);
    superiorCatacombsCombat.hitpoints(
        NpcCombatHitpoints.builder().total(390).barType(HitpointsBarType.GREEN_RED_60).build());
    superiorCatacombsCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(380)
            .defenceLevel(220)
            .bonus(BonusType.MELEE_DEFENCE, 120)
            .bonus(BonusType.DEFENCE_RANGED, 115)
            .build());
    superiorCatacombsCombat.slayer(NpcCombatSlayer.builder().level(60).experience(4085).build());
    superiorCatacombsCombat.aggression(NpcCombatAggression.PLAYERS);
    superiorCatacombsCombat.killCount(
        NpcCombatKillCount.builder().asName("Superior Slayer Creature").build());
    superiorCatacombsCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(6369)
        .blockAnimation(6370);
    superiorCatacombsCombat.drop(drop.rolls(3).build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(39));
    style.animation(6368).attackSpeed(4);
    style.targetGraphic(new Graphic(336, 300));
    style.projectile(NpcCombatProjectile.id(335));
    superiorCatacombsCombat.style(style.build());

    return Arrays.asList(
        normalCombat.build(),
        catacombsCombat.build(),
        superiorCombat.build(),
        superiorCatacombsCombat.build());
  }
}
