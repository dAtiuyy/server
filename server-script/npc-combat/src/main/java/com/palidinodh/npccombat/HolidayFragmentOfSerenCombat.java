package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.route.Route;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class HolidayFragmentOfSerenCombat extends NpcCombat {

  private static final int SPECIAL_ATTACK_DELAY = 100;

  @Inject private Npc npc;
  private HitStyleType forcedHitStyle;
  private int specialAttackDelay = SPECIAL_ATTACK_DELAY;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.FRAGMENT_OF_SEREN_16049);
    combat.hitpoints(
        NpcCombatHitpoints.builder().total(10_000).barType(HitpointsBarType.GREEN_RED_160).build());
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(242)
            .magicLevel(102)
            .rangedLevel(235)
            .defenceLevel(102)
            .bonus(BonusType.MELEE_ATTACK, 236)
            .bonus(BonusType.ATTACK_RANGED, 224)
            .bonus(BonusType.DEFENCE_STAB, 320)
            .bonus(BonusType.DEFENCE_SLASH, 220)
            .bonus(BonusType.DEFENCE_CRUSH, 320)
            .bonus(BonusType.DEFENCE_MAGIC, 10)
            .bonus(BonusType.DEFENCE_RANGED, 480)
            .build());
    combat.aggression(NpcCombatAggression.builder().range(32).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).bind(true).build());
    combat.deathAnimation(8384);
    combat.drop(NpcCombatDrop.builder().additionalPlayers(255).build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE);
    style.damage(NpcCombatDamage.maximum(10));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(8379).attackSpeed(7).applyAttackDelay(2);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.builder().maximum(12).prayerEffectiveness(0.3).build());
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(8376).attackSpeed(7);
    style.projectile(NpcCombatProjectile.id(1700));
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void tickStartHook() {
    specialAttackDelay--;
    if (!isHitDelayed() && isAttacking() && specialAttackDelay <= 0) {
      specialAttackDelay = SPECIAL_ATTACK_DELAY;
      switch (PRandom.randomE(2)) {
        case 0:
          teleportAttack();
          break;
        case 1:
          whirlwindAttack();
          break;
      }
    }
  }

  @Override
  public void tickEndHook() {
    if (!isHitDelayed()) {
      forcedHitStyle = null;
    }
  }

  @Override
  public HitStyleType attackTickHitStyleTypeHook(HitStyleType hitStyleType, Entity opponent) {
    return forcedHitStyle == null ? hitStyleType : forcedHitStyle;
  }

  @Override
  public void attackTickEndHook(NpcCombatStyle combatStyle, Entity opponent) {
    forcedHitStyle = null;
  }

  @Override
  public void attackTickHitEventHook(
      NpcCombatStyle combatStyle, Entity opponent, HitEvent hitEvent) {
    if (combatStyle.getType().getHitStyleType() == HitStyleType.MELEE) {
      npc.getMovement().setMoveDelay(5);
      getAttackablePlayers()
          .forEach(
              p -> {
                addContinuousEvent(
                    1,
                    e -> {
                      if (!p.isLocked() && npc.withinDistance(p, 1)) {
                        var damage = 10;
                        if (damage >= p.getCombat().getHitpoints()) {
                          damage = 1;
                        }
                        p.getCombat()
                            .addHitEvent(new HitEvent(0, new Hit(PRandom.randomI(damage)), npc));
                      }
                      if (e.getExecutions() == 3) {
                        e.stop();
                      }
                    });
              });
    } else if (combatStyle.getType().getHitStyleType() == HitStyleType.RANGED) {
      var damage = Math.max(0, hitEvent.getDamage() - PRandom.randomI(1));
      var secondHitEvent =
          new HitEvent(hitEvent.getTick() + 1, new Hit(damage), hitEvent.getOffender());
      opponent.getCombat().addHitEvent(secondHitEvent);
    }
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    var tokenId = Main.getHolidayToken();
    if (tokenId == -1) {
      return;
    }
    npc.getController().addMapItem(new Item(tokenId, PRandom.randomI(50, 250)), dropTile, player);
    npc.getController().addMapItem(new Item(ItemId.SKILLING_MYSTERY_BOX_32380), dropTile, player);
    if (PRandom.randomE(100) == 0) {
      var crackerItem = new Item(ItemId.HALLOWEEN_CRACKER_32392);
      npc.getController().addMapItem(crackerItem, dropTile, player);
      npc.getWorld().sendItemDropNews(player, crackerItem, "from the Fragment of Seren");
      npc.getWorld().worldEventScript("holiday_unique_drop");
    }
    if (PRandom.randomE(10) == 0) {
      var bondItem = new Item(ItemId.BOND_32318, PRandom.randomI(1, 10));
      npc.getController().addMapItem(bondItem, dropTile, player);
    }
  }

  public void teleportAttack() {
    npc.setAnimation(8376);
    npc.setForceMessage("She acted like I was a monster! Come... look upon her monster!");
    npc.getMovement().setMoveDelay(10);
    var speed = getProjectileSpeed(10);
    setHitDelay(speed.getEventDelay() + 1);
    forcedHitStyle = HitStyleType.MELEE;
    getAttackablePlayers()
        .forEach(
            p -> {
              var projectile =
                  Graphic.Projectile.builder()
                      .id(1694)
                      .startTile(npc)
                      .entity(p)
                      .speed(speed)
                      .build();
              sendMapProjectile(projectile);
              addSingleEvent(
                  speed.getEventDelay(),
                  e -> {
                    if (p.isLocked()) {
                      return;
                    }
                    p.getMovement().teleport(Route.offTile(npc));
                  });
            });
  }

  public void whirlwindAttack() {
    npc.setAnimation(8378);
    npc.setForceMessage("Did she think I would rot here? I am eternal, unlike you!");
    setHitDelay(7);
    addNpc(new NpcSpawn(new Tile(3037, 5362, 4), NpcId.CRYSTAL_WHIRLWIND));
    addNpc(new NpcSpawn(new Tile(3052, 5346, 4), NpcId.CRYSTAL_WHIRLWIND));
    addNpc(new NpcSpawn(new Tile(3037, 5330, 4), NpcId.CRYSTAL_WHIRLWIND));
    addNpc(new NpcSpawn(new Tile(3023, 5345, 4), NpcId.CRYSTAL_WHIRLWIND));
  }
}
