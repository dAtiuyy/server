package com.palidinodh.rs.communication.log;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum PlayerLogType {
  DEATH("death"),
  PVP_RISKED_ITEM_DEATH("pk"),
  PVP_TOURNAMENT("pvptournament"),
  BOSS_LOOT("bossloot"),
  BANK("bank"),
  SHOP("shop"),
  TRADE("trade"),
  DUEL("duel"),
  LOOT_BOX("lootbox"),
  MAP_ITEM("mapitem"),
  EXCHANGE("exchange"),
  WELL_OF_GOODWILL("wellofgoodwill"),
  TELEPORT("teleport"),
  COMMAND("commands"),
  STAFF("staff"),
  PUBLIC_CHAT("publicchat"),
  PRIVATE_CHAT("privatechat"),
  CLAN_CHAT("clanchat"),
  YELL("yell"),
  ALL_CHAT("chat"),
  CONNECT("ip"),
  NEWS("news");

  private String directoryName;

  public String getFileName(int userId) {
    return getFileName(Integer.toString(userId));
  }

  public String getFileName(String name) {
    return directoryName + "/" + name + ".txt";
  }
}
