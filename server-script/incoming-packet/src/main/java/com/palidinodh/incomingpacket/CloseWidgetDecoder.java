package com.palidinodh.incomingpacket;

import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.model.entity.player.Player;

class CloseWidgetDecoder extends IncomingPacketDecoder {

  @Override
  public boolean execute(Player player, Stream stream) {
    player.clearIdleTime();
    player.getWidgetManager().removeInteractiveWidgets();
    return false;
  }
}
