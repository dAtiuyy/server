package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.teleports.TeleportsPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.WIZARD_16048)
class WizardNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    if (option.getIndex() == 0) {
      player.getWidgetManager().sendInteractiveOverlay(WidgetId.TELEPORTS_1028);
    } else if (option.getIndex() == 4) {
      var plugin = player.getPlugin(TeleportsPlugin.class).getMainTeleports();
      plugin.selectTeleport(plugin.getRecent(0));
    }
  }
}
