package com.palidinodh.maparea.fremennikprovince.svenscave.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class SvensCaveNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2697, 10120), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2710, 10121), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2723, 10122), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2734, 10123), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2735, 10128), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2730, 10131), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2723, 10133), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2715, 10133), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2707, 10136), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2703, 10134), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2708, 10130), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2710, 10132), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2706, 10133), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2704, 10136), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2737, 10134), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2736, 10139), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2730, 10140), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2726, 10143), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2721, 10143), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2715, 10144), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2709, 10146), NpcId.BRINE_RAT_70));
    spawns.add(new NpcSpawn(4, new Tile(2711, 10143), NpcId.BRINE_RAT_70));

    return spawns;
  }
}
