package com.palidinodh.playerplugin.treasuretrail.stage;

import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.treasuretrail.action.DigClueScrollActon;
import lombok.Getter;

@Getter
enum CoordinateClueStageType implements ClueStage {
  COORD_ASGARNIAN_ICE_DUNGEON_ENTRANCE(new DigClueScrollActon(new Tile(3007, 3144))),
  COORD_RIVER_LUM(new DigClueScrollActon(new Tile(3121, 3384))),
  COORD_FELDIP_HILLS(new DigClueScrollActon(new Tile(2594, 2899))),
  COORD_SLAYER_TOWER(new DigClueScrollActon(new Tile(3443, 3515))),
  COORD_RELLEKKA(new DigClueScrollActon(new Tile(2681, 3653))),
  COORD_LLETYA(new DigClueScrollActon(new Tile(2357, 3151))),
  COORD_NEITIZNOT(new DigClueScrollActon(new Tile(2359, 3799))),
  COORD_NEITIZNOT_2(new DigClueScrollActon(new Tile(2316, 3814))),
  COORD_RELLEKKA_HUNTER(new DigClueScrollActon(new Tile(2700, 3808))),
  COORD_WILDERNESS_1(new DigClueScrollActon(new Tile(3113, 3602))),
  COORD_WILDERNESS_2(new DigClueScrollActon(new Tile(3168, 3677))),
  COORD_WILDERNESS_3(new DigClueScrollActon(new Tile(3305, 3692))),
  COORD_WILDERNESS_4(new DigClueScrollActon(new Tile(3055, 3696))),
  COORD_WILDERNESS_5(new DigClueScrollActon(new Tile(3302, 3696))),
  COORD_WILDERNESS_6(new DigClueScrollActon(new Tile(2970, 3749))),
  COORD_WILDERNESS_7(new DigClueScrollActon(new Tile(3094, 3764))),
  COORD_WILDERNESS_8(new DigClueScrollActon(new Tile(3311, 3769))),
  COORD_WILDERNESS_9(new DigClueScrollActon(new Tile(3244, 3792))),
  COORD_WILDERNESS_10(new DigClueScrollActon(new Tile(3140, 3804))),
  COORD_WILDERNESS_11(new DigClueScrollActon(new Tile(2946, 3819))),
  COORD_WILDERNESS_12(new DigClueScrollActon(new Tile(3013, 3846))),
  COORD_WILDERNESS_13(new DigClueScrollActon(new Tile(3058, 3884))),
  COORD_WILDERNESS_14(new DigClueScrollActon(new Tile(3290, 3889))),
  COORD_WILDERNESS_15(new DigClueScrollActon(new Tile(3285, 3942))),
  COORD_WILDERNESS_16(new DigClueScrollActon(new Tile(3159, 3959))),
  COORD_WILDERNESS_17(new DigClueScrollActon(new Tile(3039, 3960))),
  COORD_WILDERNESS_18(new DigClueScrollActon(new Tile(2987, 3963))),
  COORD_WILDERNESS_19(new DigClueScrollActon(new Tile(3189, 3963))),
  COORD_WILDERNESS_20(new DigClueScrollActon(new Tile(3143, 3774))),
  COORD_WILDERNESS_21(new DigClueScrollActon(new Tile(3215, 3835))),
  COORD_WILDERNESS_22(new DigClueScrollActon(new Tile(3369, 3894))),
  COORD_WILDERNESS_23(new DigClueScrollActon(new Tile(3188, 3933))),
  COORD_WILDERNESS_24(new DigClueScrollActon(new Tile(3380, 3963))),
  COORD_WILDERNESS_25(new DigClueScrollActon(new Tile(3051, 3736)));

  private final DigClueScrollActon action;
  private final String text;

  CoordinateClueStageType(DigClueScrollActon action) {
    this.action = action;
    text = getText(action.getTile());
  }

  private static String getText(Tile tile) {
    var obsX = 2440;
    var obsY = 3161;

    var diffX = Math.abs(obsX - tile.getX());
    var degX = diffX / 32;
    var minX = (int) ((diffX - degX * 32) * 1.875);
    var directionX = obsX >= tile.getX() ? "west" : "east";

    var diffY = Math.abs(obsY - tile.getY());
    var degY = diffY / 32;
    var minY = (int) ((diffY - degY * 32) * 1.875);
    var directionY = obsY >= tile.getY() ? "south" : "north";

    return numberToString(degY)
        + " degrees "
        + numberToString(minY)
        + " minutes "
        + directionY
        + " "
        + numberToString(degX)
        + " degrees "
        + numberToString(minX)
        + " minutes "
        + directionX;
  }

  private static String numberToString(int number) {
    var toString = Integer.toString(number);
    return toString.length() > 1 ? toString : "0" + toString;
  }

  @Override
  public int getWidgetId() {
    return -1;
  }

  @Override
  public String getText() {
    return text;
  }
}
