package com.palidinodh.maparea.kandarin.yanille;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(10388)
public class YanilleAgilityDungeonArea extends Area {}
