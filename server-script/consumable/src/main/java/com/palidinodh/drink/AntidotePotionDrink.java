package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.ANTIDOTE_PLUS_PLUS_1,
  ItemId.ANTIDOTE_PLUS_PLUS_2,
  ItemId.ANTIDOTE_PLUS_PLUS_3,
  ItemId.ANTIDOTE_PLUS_PLUS_4
})
class AntidotePlusPlusPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.action(
        (p, c) -> {
          p.getSkills().setAntidotePlusPlusTime(1200);
          p.setPoison(0);
        });
    return builder;
  }
}
