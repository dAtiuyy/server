package com.palidinodh.random;

import com.palidinodh.util.PNumber;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class PRandom {

  private static final Xoshiro256StarStar RANDOM_GEN = new Xoshiro256StarStar();

  public static <T> T listRandom(List<T> list) {
    if (list.isEmpty()) {
      return null;
    }
    return list.get(randomE(list.size()));
  }

  public static <T> T collectionRandom(Collection<T> set) {
    int index = randomE(set.size());
    Iterator<T> it = set.iterator();
    for (int i = 0; i < index; i++) {
      it.next();
    }
    return it.next();
  }

  public static int arrayRandom(int[] values) {
    if (values == null || values.length == 0) {
      return 0;
    }
    return values[randomE(values.length)];
  }

  @SafeVarargs
  @SuppressWarnings("unchecked")
  public static <T> T arrayRandom(T... values) {
    if (values == null || values.length == 0) {
      return null;
    }
    if (values.length == 1 && values[0] instanceof Object[]) {
      values = (T[]) values[0];
    }
    return values[randomE(values.length)];
  }

  public static int randomE(int bound) { // exclusive
    synchronized (RANDOM_GEN) {
      return RANDOM_GEN.nextInt(bound);
    }
  }

  public static int randomI(int bound) { // inclusive
    synchronized (RANDOM_GEN) {
      return RANDOM_GEN.nextInt(bound + 1);
    }
  }

  public static int randomI(int min, int max) { // inclusive
    synchronized (RANDOM_GEN) {
      return min + RANDOM_GEN.nextInt(max - min + 1);
    }
  }

  public static long nextLongE(long bound) { // exclusive
    synchronized (RANDOM_GEN) {
      return RANDOM_GEN.nextLong(bound);
    }
  }

  public static long nextLongI(long bound) { // inclusive
    synchronized (RANDOM_GEN) {
      return RANDOM_GEN.nextLong(bound + 1);
    }
  }

  public static double nextDouble() {
    synchronized (RANDOM_GEN) {
      return RANDOM_GEN.nextDouble();
    }
  }

  public static <T> void shuffleArray(T[] array) {
    synchronized (RANDOM_GEN) {
      for (int i = 0; i < array.length; i++) {
        int randomPosition = RANDOM_GEN.nextInt(array.length);
        T temp = array[i];
        array[i] = array[randomPosition];
        array[randomPosition] = temp;
      }
    }
  }

  public static boolean inRange(int numerator, int denominator) {
    if (numerator == 1 && denominator == 1) {
      return true;
    }
    if (numerator == 1) {
      return randomE(denominator) == 0;
    }
    return randomE(denominator) < numerator;
  }

  @Deprecated
  public static boolean inRange(double percent) {
    if (percent <= 0) {
      return false;
    }
    if (percent >= 100) {
      return true;
    }
    int[] range = getRange(percent);
    return range[0] == 1 ? randomE(range[1]) == 0 : randomE(range[1]) < range[0];
  }

  @Deprecated
  public static int[] getRange(double percent) {
    percent = PNumber.reduceDecimals(percent, 4);
    String toString = Double.toString(percent);
    String decimalString = toString.substring(toString.indexOf('.') + 1);
    int factor = (int) StrictMath.pow(10, decimalString.length());
    int base = (int) (percent * factor);
    int range = 100 * factor;
    if (base > 1) {
      int gcd = PNumber.greatestCommonDenominator(base, range);
      base /= gcd;
      range /= gcd;
    }
    return new int[] {base, range};
  }

  public static double probabilityToPercent(int numerator, int denominator) {
    return numerator / (double) denominator * 100.0;
  }

  public static double getPercent(double current, double max) {
    double difference = max - (max - current);
    return difference / max * 100.0;
  }
}
