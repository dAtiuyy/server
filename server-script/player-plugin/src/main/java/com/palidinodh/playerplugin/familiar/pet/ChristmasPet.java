package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.FamiliarPlugin;
import com.palidinodh.playerplugin.familiar.Pet;

class ChristmasPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(
            ItemId.KING_PENGUIN_60000, NpcId.KING_PENGUIN_16054, NpcId.KING_PENGUIN_16053));
    builder.entry(
        new Pet.Entry(
            ItemId.WINTUMBER_TREE_60001, NpcId.WINTUMBER_TREE_16056, NpcId.WINTUMBER_TREE_16055));
    builder.optionVariation(
        (p, n) -> {
          switch (n.getId()) {
            case NpcId.KING_PENGUIN_16054:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.WINTUMBER_TREE_16056);
              break;
            case NpcId.WINTUMBER_TREE_16056:
              p.getPlugin(FamiliarPlugin.class).transformPet(NpcId.KING_PENGUIN_16054);
              break;
          }
        });
    return builder;
  }
}
