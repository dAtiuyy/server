package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.osrscore.world.World;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.AMULET_OF_GLORY,
  ItemId.AMULET_OF_GLORY_1,
  ItemId.AMULET_OF_GLORY_2,
  ItemId.AMULET_OF_GLORY_3,
  ItemId.AMULET_OF_GLORY_4,
  ItemId.AMULET_OF_GLORY_T,
  ItemId.AMULET_OF_GLORY_T1,
  ItemId.AMULET_OF_GLORY_T2,
  ItemId.AMULET_OF_GLORY_T3,
  ItemId.AMULET_OF_GLORY_T4,
  ItemId.AMULET_OF_ETERNAL_GLORY,
  ItemId.BLIGHTED_AMULET_OF_GLORY_4_32360
})
class AmuletOfGloryItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getController().canTeleport(30, true)) {
      return;
    }
    switch (option.getText()) {
      case "rub":
        player.openOldDialogue("amuletofglory", 0);
        break;
      case "edgeville":
        SpellTeleport.normalTeleport(player, World.DEFAULT_TILE);
        break;
      case "karamja":
        SpellTeleport.normalTeleport(player, new Tile(2915, 3152));
        break;
      case "draynor village":
        SpellTeleport.normalTeleport(player, new Tile(3085, 3249));
        break;
      case "al kharid":
        SpellTeleport.normalTeleport(player, new Tile(3293, 3177));
        break;
    }
  }
}
