package com.palidinodh.cache.store.fs.jagex;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Objects;
import lombok.Getter;

public class IndexFile implements Closeable {

  private static final int INDEX_ENTRY_LEN = 6;

  @Getter private final int indexFileId;
  private final File file;
  private final RandomAccessFile idx;
  private final byte[] buffer = new byte[INDEX_ENTRY_LEN];

  public IndexFile(int indexFileId, File file) throws FileNotFoundException {
    this.indexFileId = indexFileId;
    this.file = file;
    this.idx = new RandomAccessFile(file, "rw");
  }

  @Override
  public void close() throws IOException {
    idx.close();
  }

  public void clear() throws IOException {
    idx.setLength(0L);
  }

  @Override
  public int hashCode() {
    var hash = 3;
    hash = 41 * hash + Objects.hashCode(this.file);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    var other = (IndexFile) obj;
    if (!Objects.equals(this.file, other.file)) {
      return false;
    }
    return true;
  }

  public synchronized void write(IndexEntry entry) throws IOException {
    idx.seek(entry.getId() * INDEX_ENTRY_LEN);
    buffer[0] = (byte) (entry.getLength() >> 16);
    buffer[1] = (byte) (entry.getLength() >> 8);
    buffer[2] = (byte) entry.getLength();
    buffer[3] = (byte) (entry.getSector() >> 16);
    buffer[4] = (byte) (entry.getSector() >> 8);
    buffer[5] = (byte) entry.getSector();
    idx.write(buffer);
  }

  public synchronized IndexEntry read(int id) throws IOException {
    idx.seek(id * INDEX_ENTRY_LEN);
    var i = idx.read(buffer);
    if (i != INDEX_ENTRY_LEN) {
      throw new IOException("Short read for id " + id + " on index " + indexFileId + ": " + i);
    }
    var length = ((buffer[0] & 0xFF) << 16) | ((buffer[1] & 0xFF) << 8) | (buffer[2] & 0xFF);
    var sector = ((buffer[3] & 0xFF) << 16) | ((buffer[4] & 0xFF) << 8) | (buffer[5] & 0xFF);
    if (length <= 0 || sector < 0) {
      throw new IOException(
          "invalid length or sector " + length + "/" + sector + " for " + indexFileId + "/" + id);
    }
    return new IndexEntry(this, id, sector, length);
  }

  public synchronized int getIndexCount() throws IOException {
    return (int) (idx.length() / INDEX_ENTRY_LEN);
  }
}
