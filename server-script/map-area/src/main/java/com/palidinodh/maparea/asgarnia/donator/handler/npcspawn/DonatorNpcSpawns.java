package com.palidinodh.maparea.asgarnia.donator.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class DonatorNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3049, 3500), NpcId.MARTIN_THWAIT));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3064, 3497), NpcId.NIEVE));
    spawns.add(new NpcSpawn(Tile.Direction.SOUTH, new Tile(3058, 3515), NpcId.LLIANN));
    spawns.add(new NpcSpawn(Tile.Direction.SOUTH, new Tile(3061, 3514), NpcId.HONEST_JIMMY));
    spawns.add(new NpcSpawn(Tile.Direction.SOUTH, new Tile(3062, 3512), NpcId.LOGAVA));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3040, 3496), NpcId.HORVIK));
    spawns.add(new NpcSpawn(Tile.Direction.SOUTH, new Tile(3053, 3505), NpcId.WIZARD_16048));
    spawns.add(new NpcSpawn(Tile.Direction.SOUTH, new Tile(3043, 3501), NpcId.BOB_BARTER_HERBS));

    return spawns;
  }
}
