package com.palidinodh.playerplugin.loot.handler.widget;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.ScriptId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playermisc.PickupItem;
import com.palidinodh.playerplugin.loot.LootPlugin;
import com.palidinodh.playerplugin.loot.LootTakeType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.LOOT_1022)
class LootWidget implements WidgetHandler {

  private static void setTake(Player player) {
    var plugin = player.getPlugin(LootPlugin.class);
    for (var type : LootTakeType.values()) {
      if (type == plugin.getTakeType()) {
        player
            .getGameEncoder()
            .sendClientScript(
                ScriptId.V2_STONE_BUTTON_IN, WidgetId.LOOT_1022 << 16 | type.getChildId());
      } else {
        player
            .getGameEncoder()
            .sendClientScript(
                ScriptId.V2_STONE_BUTTON, WidgetId.LOOT_1022 << 16 | type.getChildId());
      }
    }
  }

  @Override
  public boolean isCombatUsable() {
    return true;
  }

  @Override
  public void onOpen(Player player) {
    var plugin = player.getPlugin(LootPlugin.class);
    plugin.refreshItems();
    player.getGameEncoder().sendItems(1022, 23, 0, plugin.getSendableItems());
    setTake(player);
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var plugin = player.getPlugin(LootPlugin.class);
    var type = LootTakeType.getByChildId(childId);
    if (type != null) {
      plugin.setTakeType(type);
      setTake(player);
      return;
    }
    player.getWidgetManager().removeInteractiveWidgets();
    if (childId == 33) {
      for (var item : plugin.getItems()) {
        if (item.getDef().isStackable()) {
          PickupItem.complete(player, item.getId(), player.getX(), player.getY());
        } else {
          for (var i = 0; i < Math.min(item.getAmount(), 28); i++) {
            PickupItem.complete(player, item.getId(), player.getX(), player.getY());
          }
        }
      }
      return;
    }
    if (itemId == -1) {
      return;
    }
    if (ItemDefinition.getDefinition(itemId).isStackable()) {
      PickupItem.complete(player, itemId, player.getX(), player.getY());
    } else {
      for (var i = 0; i < plugin.getTakeType().getQuantity(); i++) {
        PickupItem.complete(player, itemId, player.getX(), player.getY());
      }
    }
    if (plugin.getItems().size() == 1) {
      return;
    }
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.LOOT_1022);
  }
}
