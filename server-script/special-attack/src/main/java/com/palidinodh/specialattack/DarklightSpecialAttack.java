package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.DARKLIGHT, ItemId.ARCLIGHT})
class DarklightSpecialAttack extends SpecialAttack {

  DarklightSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(2890);
    entry.impactGraphic(new Graphic(483));
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    if (hooks.getDamage() == 0) {
      return;
    }
    var opponent = hooks.getOpponent();
    var changeAmount = opponent.getCombat().getDefenceLevel() * 0.05;
    if (opponent.isNpc() && opponent.asNpc().getCombatDef().isTypeDemon()) {
      changeAmount = opponent.getCombat().getDefenceLevel() * 0.1;
    }
    opponent.getCombat().changeDefenceLevel((int) -changeAmount);
  }
}
