package com.palidinodh.io;

import com.google.inject.Inject;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;
import com.palidinodh.util.PTime;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Writers {

  public static void writeFile(File file, byte[] bytes) {
    if (file.getParentFile() != null) {
      file.getParentFile().mkdirs();
    }
    try (DataOutputStream out =
        new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)))) {
      out.write(bytes);
      out.flush();
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  public static void writeTextFile(File file, boolean append, String... lines) {
    if (file.getParentFile() != null) {
      file.getParentFile().mkdirs();
    }
    try (BufferedWriter out = new BufferedWriter(new FileWriter(file, append))) {
      for (int i = 0; i < lines.length; i++) {
        String line = lines[i];
        if (line == null) {
          continue;
        }
        out.write(line);
        if (!line.isEmpty() || i + 1 < lines.length) {
          out.newLine();
        }
      }
      out.flush();
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  public static void writeTextFile(File file, List<String> lines) {
    writeTextFile(file, false, lines.toArray(new String[0]));
  }

  public static void writeTextFile(File file, String... lines) {
    writeTextFile(file, false, lines);
  }

  public static void appendTextFile(File file, String... lines) {
    writeTextFile(file, true, lines);
  }

  public static void writeLog(File baseDirectory, String fileName, String line) {
    if (Settings.getInstance().isLocal()) {
      return;
    }
    appendTextFile(
        new File(new File(baseDirectory, PTime.getDate()), fileName),
        PTime.getExactDate() + " " + line + System.getProperty("line.separator"));
  }

  public static void writePlayerLog(String fileName, String line) {
    writeLog(Settings.getInstance().getPlayerLogsDirectory(), fileName, line);
  }

  public static byte[] serialize(Object object) {
    try (ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        ObjectOutputStream oOut = new ObjectOutputStream(bOut)) {
      oOut.writeObject(object);
      oOut.flush();
      return bOut.toByteArray();
    } catch (Exception e) {
      PLogger.error(e);
    }
    return null;
  }

  public static boolean isSerializable(Object object) {
    boolean success = true;
    if (object instanceof Map) {
      Map<?, ?> map = (Map<?, ?>) object;
      for (Map.Entry<?, ?> e : map.entrySet()) {
        byte[] serialized = serialize(e.getValue());
        if (serialized == null) {
          PLogger.error("Serialization failure: " + e.getKey() + ", " + e.getValue());
          success = false;
        }
      }
    } else if (object instanceof List) {
      List<?> list = (List<?>) object;
      for (Object e : list) {
        byte[] serialized = serialize(e);
        if (serialized == null) {
          PLogger.error("Serialization failure: " + e);
          success = false;
        }
      }
    } else {
      byte[] serialized = serialize(object);
      if (serialized == null) {
        PLogger.error("Serialization failure: " + object);
        success = false;
      }
    }
    return success;
  }

  public static byte[] gzCompress(byte[] bytes) {
    try (ByteArrayOutputStream out = new ByteArrayOutputStream();
        GZIPOutputStream gOut = new GZIPOutputStream(out)) {
      gOut.write(bytes);
      gOut.finish();
      return out.toByteArray();
    } catch (Exception e) {
      PLogger.error(e);
    }
    return null;
  }

  public static void zip(File zipFile, File... files) {
    AccessController.doPrivileged(
        (PrivilegedAction<Boolean>)
            () -> {
              try {
                Path zipFilePath = zipFile.toPath();
                if (Files.exists(zipFilePath)) {
                  Files.delete(zipFilePath);
                }
                Path p = Files.createFile(zipFilePath);
                try (ZipOutputStream zs = new ZipOutputStream(Files.newOutputStream(p))) {
                  for (File file : files) {
                    if (!file.exists()) {
                      continue;
                    }
                    Path pp = file.toPath();
                    Files.walk(pp)
                        .filter(path -> !Files.isDirectory(path))
                        .forEach(
                            path -> {
                              if (pp.relativize(path).toString().startsWith(".")) {
                                return;
                              }
                              ZipEntry ze =
                                  new ZipEntry(
                                      pp.getFileName().toString()
                                          + "/"
                                          + pp.relativize(path).toString());
                              try {
                                zs.putNextEntry(ze);
                                Files.copy(path, zs);
                                zs.closeEntry();
                              } catch (IOException e2) {
                                PLogger.error(e2);
                              }
                            });
                  }
                }
              } catch (Exception e) {
                PLogger.error(e);
              }
              return true;
            });
  }

  public static void unzip(File zipFile, File destFile) {
    AccessController.doPrivileged(
        (PrivilegedAction<Boolean>)
            () -> {
              if (!destFile.exists()) {
                destFile.mkdirs();
              }
              byte[] buffer = new byte[1024];
              try {
                ZipInputStream zs = new ZipInputStream(new FileInputStream(zipFile));
                ZipEntry ze = zs.getNextEntry();
                while (ze != null) {
                  File nf = new File(destFile, ze.getName());
                  if (ze.isDirectory()) {
                    nf.mkdirs();
                  } else {
                    FileOutputStream fos = new FileOutputStream(nf);
                    int length;
                    while ((length = zs.read(buffer)) != -1) {
                      fos.write(buffer, 0, length);
                    }
                    fos.close();
                  }
                  zs.closeEntry();
                  ze = zs.getNextEntry();
                }
                zs.close();
              } catch (IOException e) {
                PLogger.error(e);
              }
              return true;
            });
  }

  public static void inject(Object injectingObject, Object... injectingWithObjects) {
    // How does Google field injection work so that I don't have to use this lmao
    try {
      Class<?> clazz = injectingObject.getClass();
      do {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
          if (!field.isAnnotationPresent(Inject.class)) {
            continue;
          }
          for (Object injectingWithObject : injectingWithObjects) {
            if (!field.getType().isAssignableFrom(injectingWithObject.getClass())) {
              continue;
            }
            field.setAccessible(true);
            field.set(clazz.cast(injectingObject), injectingWithObject);
          }
        }
        clazz = clazz.getSuperclass();
      } while (clazz != null && !clazz.equals(Object.class));
    } catch (Exception e) {
      PLogger.error(e);
    }
  }
}
