package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackStyle;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.IRON_WARHAMMER,
  ItemId.BRONZE_WARHAMMER,
  ItemId.STEEL_WARHAMMER,
  ItemId.BLACK_WARHAMMER,
  ItemId.MITHRIL_WARHAMMER,
  ItemId.ADAMANT_WARHAMMER,
  ItemId.RUNE_WARHAMMER,
  ItemId.WHITE_WARHAMMER,
  ItemId.DRAGON_WARHAMMER,
  ItemId.ZOMBIE_HEAD_19912,
  ItemId.DRAGON_WARHAMMER_20785
})
class WarhammerWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.equipSound(new Sound(2246));
    type.attackSpeed(6);
    type.defendAnimation(403);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(401).attackSound(new Sound(2567)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.GRANITE_HAMMER,
  ItemId.DRAMEN_STAFF,
  ItemId.MOUSE_TOY,
  ItemId.RUBBER_CHICKEN,
  ItemId.OAK_BLACKJACK,
  ItemId.WILLOW_BLACKJACK,
  ItemId.MAPLE_BLACKJACK,
  ItemId.ASSORTED_FLOWERS,
  ItemId.RED_FLOWERS,
  ItemId.BLUE_FLOWERS,
  ItemId.YELLOW_FLOWERS,
  ItemId.PURPLE_FLOWERS,
  ItemId.ORANGE_FLOWERS,
  ItemId.MIXED_FLOWERS,
  ItemId.WHITE_FLOWERS,
  ItemId.BLACK_FLOWERS,
  ItemId.CANDY_CANE,
  ItemId.CRIER_BELL,
  ItemId.CURSED_GOBLIN_HAMMER,
  ItemId.GOLDEN_TENCH,
  ItemId.STALE_BAGUETTE,
  ItemId.RUBBER_CHICKEN_22666
})
class Blunt4TickWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.equipSound(new Sound(2246));
    type.attackSpeed(4);
    type.defendAnimation(403);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(401).attackSound(new Sound(2567)).build());
    return type;
  }
}

@ReferenceId({ItemId.TZHAAR_KET_OM, ItemId.TZHAAR_KET_OM_T, ItemId.HILL_GIANT_CLUB})
class MaulWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.renderAnimations(new int[] {2065, 2064, 2064, 2064, 2064, 2064, 824});
    type.equipSound(new Sound(2246));
    type.twoHanded(true);
    type.attackSpeed(7);
    type.defendAnimation(1666);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(2661).attackSound(new Sound(2520)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.GRANITE_MAUL,
  ItemId.GRANITE_MAUL_12848,
  ItemId.GRANITE_MAUL_20557,
  ItemId.GRANITE_MAUL_24225,
  ItemId.GRANITE_MAUL_24227
})
class GraniteMaulWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.renderAnimations(new int[] {1662, 1663, 1663, 1663, 1663, 1663, 1664});
    type.equipSound(new Sound(2246));
    type.twoHanded(true);
    type.attackSpeed(7);
    type.defendAnimation(1666);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(1665).attackSound(new Sound(2714)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.TORAGS_HAMMERS,
  ItemId.TORAGS_HAMMERS_100,
  ItemId.TORAGS_HAMMERS_75,
  ItemId.TORAGS_HAMMERS_50,
  ItemId.TORAGS_HAMMERS_25,
  ItemId.NUNCHAKU
})
class ToragsHammersWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.equipSound(new Sound(2246));
    type.twoHanded(true);
    type.attackSpeed(5);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(2068).attackSound(new Sound(1332)).build());
    return type;
  }
}

@ReferenceId(ItemId.TZHAAR_KET_EM)
class TzhaarKetEmWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.equipSound(new Sound(2246));
    type.attackSpeed(5);
    type.defendAnimation(403);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(401).attackSound(new Sound(2567)).build());
    return type;
  }
}

@ReferenceId(ItemId.ABYSSAL_BLUDGEON)
class AbyssalBludgeonWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.renderAnimations(new int[] {1652, 3293, 3293, 3293, 3293, 3293, 2847});
    type.equipSound(new Sound(2246));
    type.twoHanded(true);
    type.attackSpeed(4);
    type.defendAnimation(1666);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(419).attackSound(new Sound(2566)).build());
    return type;
  }
}

@ReferenceId({ItemId.ELDER_MAUL, ItemId.ELDER_MAUL_21205})
class ElderMaulWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.renderAnimations(new int[] {7518, 7520, 7520, 7520, 7520, 7520, 7519});
    type.equipSound(new Sound(2246));
    type.twoHanded(true);
    type.attackSpeed(6);
    type.defendAnimation(7517);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(7516).attackSound(new Sound(2520)).build());
    return type;
  }
}

@ReferenceId(ItemId.HEAVY_CASKET)
class HeavyCasketWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.renderAnimations(new int[] {4193, 4194, 4194, 4194, 4194, 4194, 7274});
    type.twoHanded(true);
    type.attackSpeed(6);
    type.defendAnimation(7276);
    type.attackSet(WeaponAttackSet.builder().attackAnimation(7275).build());
    return type;
  }
}

@ReferenceId(ItemId.GIANT_PRESENT)
class GiantPresentWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.renderAnimations(new int[] {4193, 4194, 4194, 4194, 4194, 4194, 7274});
    type.twoHanded(true);
    type.attackSpeed(4);
    type.defendAnimation(7276);
    type.attackSet(WeaponAttackSet.builder().attackAnimation(7275).build());
    return type;
  }
}

@ReferenceId({
  ItemId.STATIUSS_WARHAMMER,
  ItemId.STATIUSS_WARHAMMER_23620,
  ItemId.STATIUSS_WARHAMMER_CHARGED_32255
})
class StatiussWarhammerWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.equipSound(new Sound(2246));
    type.attackSpeed(5);
    type.defendAnimation(403);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(401).attackSound(new Sound(2567)).build());
    return type;
  }
}

@ReferenceId(ItemId.HAM_JOINT)
class HamJointWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.equipSound(new Sound(2246));
    type.attackSpeed(3);
    type.defendAnimation(403);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(401).attackSound(new Sound(2567)).build());
    return type;
  }
}

@ReferenceId(ItemId.LARGE_SPADE)
class LargeSpadeWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.renderAnimations(new int[] {7053, 7044, 7052, 7048, 7048, 7047, 7043});
    type.equipSound(new Sound(2244));
    type.twoHanded(true);
    type.attackSpeed(4);
    type.defendAnimation(7056);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(7045).attackSound(new Sound(3846)).build());
    return type;
  }
}

@ReferenceId(ItemId.GILDED_SPADE)
class GildedSpadeWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.renderAnimations(new int[] {2561, 2562, 2562, 2562, 2562, 2562, 2563});
    type.equipSound(new Sound(2242));
    type.twoHanded(true);
    type.attackSpeed(4);
    type.defendAnimation(410);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(407).attackSound(new Sound(3846)).build());
    return type;
  }
}

@ReferenceId(ItemId.HUNTING_KNIFE)
class HuntingKnifeWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.renderAnimations(new int[] {2911, 7327, 7327, 7327, 7327, 7327, 2322});
    type.equipSound(new Sound(2246));
    type.attackSpeed(4);
    type.defendAnimation(403);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(401).attackSound(new Sound(2567)).build());
    return type;
  }
}

@ReferenceId(ItemId.SKELETON_LANTERN)
class SkeletonLanterneWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.renderAnimations(new int[] {8521, 8492, 8492, 8492, 8492, 8492, 8492});
    type.equipSound(new Sound(2246));
    type.twoHanded(true);
    type.attackSpeed(4);
    type.defendAnimation(403);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(401).attackSound(new Sound(2567)).build());
    return type;
  }
}

@ReferenceId(ItemId.GIANT_EASTER_EGG)
class GiantEasterEggWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.renderAnimations(new int[] {4193, 4194, 4194, 4194, 4194, 4194, 7274});
    type.equipSound(new Sound(2246));
    type.twoHanded(true);
    type.attackSpeed(4);
    type.defendAnimation(403);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(401).attackSound(new Sound(2567)).build());
    return type;
  }
}

@ReferenceId(ItemId.EASTER_BASKET)
class EasterBasketWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.renderAnimations(new int[] {1837, 1836, 1836, 1836, 1836, 1836, 1836});
    type.equipSound(new Sound(2246));
    type.twoHanded(false);
    type.attackSpeed(4);
    type.defendAnimation(403);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(401).attackSound(new Sound(2567)).build());
    return type;
  }
}

@ReferenceId({ItemId.CARROT_SWORD, ItemId._24_CARAT_SWORD})
class CarrotSwordWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_2);
    type.renderAnimations(new int[] {809, 823, 819, 820, 821, 822, 824});
    type.equipSound(new Sound(2238));
    type.attackSpeed(4);
    type.defendAnimation(388);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(390).attackSound(new Sound(2549)).build());
    type.attackSet(
        WeaponAttackSet.builder()
            .attackStyle(WeaponAttackStyle.AGGRESSIVE_SLASH)
            .attackAnimation(395)
            .attackSound(new Sound(2548))
            .build());
    return type;
  }
}
