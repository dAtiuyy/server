package com.palidinodh.cache.clientscript2;

import com.palidinodh.cache.store.util.Stream;
import java.util.ArrayList;
import java.util.List;

public class ClientScript2 {

  private int clientScriptId;
  private List<Object> parameters = new ArrayList<>();

  ClientScript2(int id) {
    clientScriptId = id;
  }

  public byte[] build() {
    var stream = new Stream();
    StringBuilder types = new StringBuilder();
    for (var o : parameters) {
      types.append(o instanceof String ? "s" : "i");
    }
    stream.writeString(types.toString());
    for (var i = parameters.size() - 1; i >= 0; i--) {
      Object parameter = parameters.get(i);
      if (parameter instanceof String) {
        stream.writeString((String) parameter);
      } else {
        stream.writeInt((Integer) parameter);
      }
    }
    stream.writeInt(clientScriptId);
    parameters.clear();
    return stream.toByteArray();
  }

  void addInt(int i) {
    parameters.add(i);
  }

  void addString(String s) {
    parameters.add(s);
  }
}
