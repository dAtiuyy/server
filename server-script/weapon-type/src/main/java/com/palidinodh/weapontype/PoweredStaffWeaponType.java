package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.TRIDENT_OF_THE_SEAS_FULL,
  ItemId.TRIDENT_OF_THE_SEAS,
  ItemId.UNCHARGED_TRIDENT,
  ItemId.TRIDENT_OF_THE_SWAMP,
  ItemId.UNCHARGED_TOXIC_TRIDENT,
  ItemId.TRIDENT_OF_THE_SEAS_E,
  ItemId.UNCHARGED_TRIDENT_E,
  ItemId.TRIDENT_OF_THE_SWAMP_E,
  ItemId.UNCHARGED_TOXIC_TRIDENT_E,
  ItemId.SANGUINESTI_STAFF,
  ItemId.SANGUINESTI_STAFF_UNCHARGED,
  ItemId.HOLY_SANGUINESTI_STAFF,
  ItemId.HOLY_SANGUINESTI_STAFF_UNCHARGED,
  ItemId.STARTER_STAFF,
  ItemId.DAWNBRINGER,
  ItemId.CRYSTAL_STAFF_BASIC,
  ItemId.CRYSTAL_STAFF_ATTUNED,
  ItemId.CRYSTAL_STAFF_PERFECTED,
  ItemId.CORRUPTED_STAFF_BASIC,
  ItemId.CORRUPTED_STAFF_ATTUNED,
  ItemId.CORRUPTED_STAFF_PERFECTED,
  ItemId.CORRUPTED_STAFF_BASIC_32384,
  ItemId.CORRUPTED_STAFF_ATTUNED_32385,
  ItemId.CORRUPTED_STAFF_PERFECTED_32386
})
class PoweredStaffWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_23);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.defendAnimation(420);
    type.attackSet(WeaponAttackSet.builder().attackAnimation(1167).build());
    return type;
  }
}
