package com.palidinodh.maparea.kandarin.ardougne;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({10291, 10292, 10547, 10548})
public class EastArdougneArea extends Area {}
