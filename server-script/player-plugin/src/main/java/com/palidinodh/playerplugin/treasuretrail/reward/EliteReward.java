package com.palidinodh.playerplugin.treasuretrail.reward;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.random.PRandom;
import java.util.List;

class EliteReward implements TreasureTrailReward {

  private List<RandomItem> items =
      RandomItem.buildList(
          new RandomItem(ItemId.DRAGON_FULL_HELM_ORNAMENT_KIT),
          new RandomItem(ItemId.DRAGON_CHAINBODY_ORNAMENT_KIT),
          new RandomItem(ItemId.DRAGON_LEGS_SKIRT_ORNAMENT_KIT),
          new RandomItem(ItemId.DRAGON_SQ_SHIELD_ORNAMENT_KIT),
          new RandomItem(ItemId.DRAGON_SCIMITAR_ORNAMENT_KIT),
          new RandomItem(ItemId.LIGHT_INFINITY_COLOUR_KIT),
          new RandomItem(ItemId.DARK_INFINITY_COLOUR_KIT),
          new RandomItem(ItemId.FURY_ORNAMENT_KIT),
          new RandomItem(ItemId.MUSKETEER_HAT),
          new RandomItem(ItemId.MUSKETEER_TABARD),
          new RandomItem(ItemId.MUSKETEER_PANTS),
          new RandomItem(ItemId.DRAGON_CANE),
          new RandomItem(ItemId.BRIEFCASE),
          new RandomItem(ItemId.SAGACIOUS_SPECTACLES),
          new RandomItem(ItemId.TOP_HAT),
          new RandomItem(ItemId.MONOCLE),
          new RandomItem(ItemId.BIG_PIRATE_HAT),
          new RandomItem(ItemId.DEERSTALKER),
          new RandomItem(ItemId.BRONZE_DRAGON_MASK),
          new RandomItem(ItemId.IRON_DRAGON_MASK),
          new RandomItem(ItemId.STEEL_DRAGON_MASK),
          new RandomItem(ItemId.MITHRIL_DRAGON_MASK),
          new RandomItem(ItemId.ADAMANT_DRAGON_MASK),
          new RandomItem(ItemId.RUNE_DRAGON_MASK),
          new RandomItem(ItemId.LAVA_DRAGON_MASK),
          new RandomItem(ItemId.BLACK_DHIDE_BODY_T),
          new RandomItem(ItemId.BLACK_DHIDE_CHAPS_T),
          new RandomItem(ItemId.BLACK_DHIDE_BODY_G),
          new RandomItem(ItemId.BLACK_DHIDE_CHAPS_G),
          new RandomItem(ItemId.RANGERS_TUNIC),
          new RandomItem(ItemId.AFRO),
          new RandomItem(ItemId.KATANA),
          new RandomItem(ItemId.ROYAL_CROWN),
          new RandomItem(ItemId.ROYAL_GOWN_TOP),
          new RandomItem(ItemId.ROYAL_GOWN_BOTTOM),
          new RandomItem(ItemId.ROYAL_SCEPTRE),
          new RandomItem(ItemId.ARCEUUS_SCARF),
          new RandomItem(ItemId.HOSIDIUS_SCARF),
          new RandomItem(ItemId.LOVAKENGJ_SCARF),
          new RandomItem(ItemId.PISCARILIUS_SCARF),
          new RandomItem(ItemId.SHAYZIEN_SCARF),
          new RandomItem(ItemId.BLACKSMITHS_HELM),
          new RandomItem(ItemId.BUCKET_HELM),
          new RandomItem(ItemId.RANGER_GLOVES),
          new RandomItem(ItemId.HOLY_WRAPS),
          new RandomItem(ItemId.RING_OF_NATURE),
          new RandomItem(ItemId.DARK_BOW_TIE),
          new RandomItem(ItemId.DARK_TUXEDO_JACKET),
          new RandomItem(ItemId.DARK_TROUSERS),
          new RandomItem(ItemId.DARK_TUXEDO_CUFFS),
          new RandomItem(ItemId.DARK_TUXEDO_SHOES),
          new RandomItem(ItemId.LIGHT_BOW_TIE),
          new RandomItem(ItemId.LIGHT_TUXEDO_JACKET),
          new RandomItem(ItemId.LIGHT_TROUSERS),
          new RandomItem(ItemId.LIGHT_TUXEDO_CUFFS),
          new RandomItem(ItemId.LIGHT_TUXEDO_SHOES),
          new RandomItem(ItemId.RANGERS_TIGHTS),
          new RandomItem(ItemId.URIS_HAT),
          new RandomItem(ItemId.FREMENNIK_KILT),
          new RandomItem(ItemId.HEAVY_CASKET),
          new RandomItem(ItemId.GIANT_BOOT));
  private List<RandomItem> gildedItems =
      RandomItem.buildList(
          new RandomItem(ItemId.GILDED_BOOTS),
          new RandomItem(ItemId.GILDED_SCIMITAR),
          new RandomItem(ItemId.GILDED_DHIDE_VAMBS),
          new RandomItem(ItemId.GILDED_DHIDE_BODY),
          new RandomItem(ItemId.GILDED_DHIDE_CHAPS),
          new RandomItem(ItemId.GILDED_COIF),
          new RandomItem(ItemId.GILDED_AXE),
          new RandomItem(ItemId.GILDED_PICKAXE),
          new RandomItem(ItemId.GILDED_SPADE));
  private List<RandomItem> thirdAgeItems =
      RandomItem.buildList(
          new RandomItem(ItemId._3RD_AGE_FULL_HELMET),
          new RandomItem(ItemId._3RD_AGE_PLATEBODY),
          new RandomItem(ItemId._3RD_AGE_PLATELEGS),
          new RandomItem(ItemId._3RD_AGE_KITESHIELD),
          new RandomItem(ItemId._3RD_AGE_RANGE_COIF),
          new RandomItem(ItemId._3RD_AGE_RANGE_TOP),
          new RandomItem(ItemId._3RD_AGE_RANGE_LEGS),
          new RandomItem(ItemId._3RD_AGE_VAMBRACES),
          new RandomItem(ItemId._3RD_AGE_MAGE_HAT),
          new RandomItem(ItemId._3RD_AGE_ROBE_TOP),
          new RandomItem(ItemId._3RD_AGE_ROBE),
          new RandomItem(ItemId._3RD_AGE_AMULET),
          new RandomItem(ItemId._3RD_AGE_CLOAK),
          new RandomItem(ItemId._3RD_AGE_WAND),
          new RandomItem(ItemId._3RD_AGE_BOW),
          new RandomItem(ItemId._3RD_AGE_LONGSWORD),
          new RandomItem(ItemId.RING_OF_3RD_AGE),
          new RandomItem(ItemId._3RD_AGE_PLATESKIRT));
  private List<RandomItem> commonItems =
      RandomItem.combine(
          BASE_COMMON,
          RandomItem.buildList(
              new RandomItem(ItemId.RUNE_PLATEBODY),
              new RandomItem(ItemId.RUNE_PLATELEGS),
              new RandomItem(ItemId.RUNE_KITESHIELD),
              new RandomItem(ItemId.RUNE_CROSSBOW),
              new RandomItem(ItemId.DRAGON_DAGGER),
              new RandomItem(ItemId.DRAGON_MACE),
              new RandomItem(ItemId.DRAGON_LONGSWORD),
              new RandomItem(ItemId.ONYX_BOLT_TIPS, 8, 12),
              new RandomItem(ItemId.LAW_RUNE, 50, 75),
              new RandomItem(ItemId.DEATH_RUNE, 50, 75),
              new RandomItem(ItemId.BLOOD_RUNE, 50, 75),
              new RandomItem(ItemId.SOUL_RUNE, 50, 75),
              new RandomItem(ItemId.DRAGONSTONE_BRACELET),
              new RandomItem(ItemId.DRAGON_NECKLACE),
              new RandomItem(ItemId.DRAGONSTONE_RING),
              new RandomItem(ItemId.TUNA_POTATO_NOTED, 15, 20),
              new RandomItem(ItemId.SUMMER_PIE_NOTED, 15, 20),
              new RandomItem(ItemId.OAK_PLANK_NOTED, 60, 80),
              new RandomItem(ItemId.TEAK_PLANK_NOTED, 40, 50),
              new RandomItem(ItemId.MAHOGANY_PLANK_NOTED, 20, 30),
              new RandomItem(ItemId.RUNITE_BAR_NOTED, 1, 3),
              new RandomItem(ItemId.TOOTH_HALF_OF_KEY),
              new RandomItem(ItemId.LOOP_HALF_OF_KEY),
              new RandomItem(ItemId.PALM_TREE_SEED),
              new RandomItem(ItemId.YEW_SEED),
              new RandomItem(ItemId.MAGIC_SEED)));

  @Override
  public Item getCommon(Player player) {
    return RandomItem.getItem(commonItems);
  }

  @Override
  public Item getUnique(Player player) {
    if (PRandom.inRange(1, player.getCombat().getDropRateDenominator(1_000))) {
      return RandomItem.getItem(thirdAgeItems);
    }
    if (PRandom.inRange(1, player.getCombat().getDropRateDenominator(200))) {
      return RandomItem.getItem(gildedItems);
    }
    return RandomItem.getItem(items);
  }

  @Override
  public int getRandomRolls() {
    return PRandom.randomI(4, 6);
  }

  @Override
  public int getUniqueDenominator() {
    return 25;
  }

  @Override
  public int getRandomCoinQuantity() {
    return PRandom.randomI(100_000, 200_000);
  }

  @Override
  public int getMasterScrollRate() {
    return 5;
  }
}
