package com.palidinodh.cache.store.util;

import com.palidinodh.cache.store.fs.Archive;
import com.palidinodh.cache.store.fs.ArchiveFiles;
import com.palidinodh.cache.store.fs.Container;
import com.palidinodh.cache.store.fs.FsFile;
import com.palidinodh.cache.store.fs.Index;
import com.palidinodh.cache.store.fs.Storage;
import com.palidinodh.cache.store.fs.jagex.CompressionType;
import com.palidinodh.cache.store.index.ArchiveData;
import com.palidinodh.cache.store.index.FileData;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.zip.ZipException;

public class ArchiveUpdater {

  private Index index;
  private int id;
  private Storage storage;

  public ArchiveUpdater(Index index, Storage storage) {
    this.index = index;
    this.storage = storage;
    id = index.getId();
  }

  public void addEmptyArchive(ArchiveData ad) throws IOException {
    if (!ad.getFiles().isEmpty()) {
      ad = new ArchiveData(ad.getIndex(), ad.getId(), ad.getNameHash(), ad.getCompression());
    }
    var existingArchive = index.getArchive(ad.getId());
    if (existingArchive != null) {
      return;
    }
    var archive = addArchiveToIndex(ad);
    archive.load(
        ad.getRevision() + 1, ad.getNameHash(), ad.getCrc(), ad.getCompression(), ad.getFiles());
    var files = new ArchiveFiles();
    var container = new Container(archive.getCompression(), -1);
    container.compress(files.saveContents(), null);
    storage.saveArchive(archive, container.data);
  }

  public boolean updateFile(FileData fd) throws IOException {
    var existingArchive = index.getArchive(fd.getArchiveId());
    if (existingArchive == null) {}
    var archive =
        existingArchive == null
            ? addArchiveToIndex(
                new ArchiveData(fd.getIndex(), fd.getArchiveId(), 0, CompressionType.GZ))
            : existingArchive;
    ArchiveFiles files;
    if (existingArchive != null) {
      files = archive.loadArchiveFiles(null);
    } else {
      files = new ArchiveFiles();
    }
    var existingFile = files.getFile(fd.getId());
    if (existingFile == null) {}
    if (fd.getContents() == null) {}
    if (filesMatch(existingFile, fd)) {
      return false;
    }
    updateFile(archive, fd);
    files.updateFile(fd);
    if (fd.getContents() == null) {}
    archive.load(
        archive.getRevision() + 1,
        archive.getNameHash(),
        archive.getCrc(),
        existingArchive == null ? CompressionType.GZ : archive.getCompression(),
        archive.getFiles());
    var container = new Container(archive.getCompression(), -1);
    container.compress(files.saveContents(), null);
    storage.saveArchive(archive, container.data);
    return true;
  }

  public boolean updateArchive(ArchiveData ad) throws IOException {
    var existingArchive = index.getArchive(ad.getId());
    var archive = existingArchive == null ? addArchiveToIndex(ad) : existingArchive;
    ArchiveFiles files;
    var regionId = -1;
    try {
      if (existingArchive != null) {
        files = archive.loadArchiveFiles(null);
      } else {
        files = new ArchiveFiles();
      }
      if (IndexType.get(id) == IndexType.MAP) {
        regionId =
            RegionHash.findRegionId(
                existingArchive != null ? existingArchive.getNameHash() : ad.getNameHash());
      }
    } catch (ZipException e) {
      if (IndexType.get(id) != IndexType.MAP) {
        throw e;
      }
      files = new ArchiveFiles();
      regionId =
          RegionHash.findRegionId(
              existingArchive != null ? existingArchive.getNameHash() : ad.getNameHash());
    }
    var foundChanges = ad.getNameHash() != 0 && ad.getNameHash() != archive.getNameHash();
    for (var file : ad.getFiles()) {
      var existingFile = files.getFile(file.getId());
      if (existingFile == null
          || file.getContents() == null
          || Crc32.getRoughHash(file.getContents())
              != Crc32.getRoughHash(existingFile.getContents())) {
        updateFile(archive, file);
        files.updateFile(file);
        foundChanges = true;
      }
    }
    if (!foundChanges) {
      return false;
    }
    archive.load(
        archive.getRevision() + 1,
        ad.getNameHash() == 0 ? archive.getNameHash() : ad.getNameHash(),
        archive.getCrc(),
        existingArchive == null ? ad.getCompression() : archive.getCompression(),
        archive.getFiles());
    var container = new Container(archive.getCompression(), -1);
    container.compress(files.saveContents(), null);
    storage.saveArchive(archive, container.data);
    if (regionId != -1) {
      System.out.println("Region " + regionId + " no longer has xteas.");
    }
    return true;
  }

  public void updateDirectArchive(Archive fromArchive, byte[] contents) throws IOException {
    var existingArchive = index.getArchive(fromArchive.getId());
    var archive =
        existingArchive == null ? addArchiveToIndex(fromArchive.getArchiveData()) : existingArchive;
    var existingContents = existingArchive == null ? null : storage.loadArchive(archive);
    var bothValid = existingArchive != null && existingContents != null;
    var matchingHash =
        fromArchive.getNameHash() == 0
            || fromArchive.getNameHash() == existingArchive.getNameHash();
    var matchingCrc = Crc32.getRoughHash(existingContents) == Crc32.getRoughHash(contents);
    if (bothValid && matchingHash && matchingCrc) {
      return;
    }
    archive.load(
        archive.getRevision() + 1,
        fromArchive.getNameHash() == 0 ? archive.getNameHash() : fromArchive.getNameHash(),
        archive.getCrc(),
        existingArchive == null ? fromArchive.getCompression() : archive.getCompression(),
        fromArchive.getFiles());
    storage.saveArchive(archive, contents);
  }

  public boolean copyArchive(Archive fromArchive, boolean skipExisting) throws IOException {
    var existingArchive = index.getArchive(fromArchive.getId());
    var archive =
        existingArchive == null ? addArchiveToIndex(fromArchive.getArchiveData()) : existingArchive;
    var fromFiles = fromArchive.loadArchiveFiles(null);
    ArchiveFiles files;
    if (existingArchive != null) {
      files = archive.loadArchiveFiles(null);
    } else {
      files = new ArchiveFiles();
    }
    var foundChanges =
        fromArchive.getNameHash() != 0 && fromArchive.getNameHash() != archive.getNameHash();
    for (var file : fromFiles.getFiles()) {
      var existingFile = files.getFile(file.getFileId());
      if (existingFile == null
          || !skipExisting
              && Crc32.getRoughHash(file.getContents())
                  != Crc32.getRoughHash(existingFile.getContents())) {
        var fileData =
            new FileData(
                fromArchive.getIndex().getId(),
                fromArchive.getId(),
                file.getFileId(),
                file.getNameHash());
        fileData.setContents(file.getContents());
        updateFile(archive, fileData);
        files.updateFile(fileData);
        foundChanges = true;
      }
    }
    if (!foundChanges) {
      return false;
    }
    archive.load(
        archive.getRevision() + 1,
        fromArchive.getNameHash() == 0 ? archive.getNameHash() : fromArchive.getNameHash(),
        archive.getCrc(),
        existingArchive == null ? fromArchive.getCompression() : archive.getCompression(),
        archive.getFiles());
    var container = new Container(archive.getCompression(), -1);
    container.compress(files.saveContents(), null);
    var compressedData = container.data;
    storage.saveArchive(archive, compressedData);
    return true;
  }

  public boolean copyDirectArchive(Archive fromArchive, boolean skipExisting) throws IOException {
    var existingArchive = index.getArchive(fromArchive.getId());
    if (skipExisting && existingArchive != null) {
      return false;
    }
    var archive =
        existingArchive == null ? addArchiveToIndex(fromArchive.getArchiveData()) : existingArchive;
    var existingContents = existingArchive == null ? null : storage.loadArchive(archive);
    var contents = fromArchive.readArchiveData();
    var bothValid = existingArchive != null && existingContents != null;
    var matchingHash =
        fromArchive.getNameHash() == 0
            || existingArchive != null
                && fromArchive.getNameHash() == existingArchive.getNameHash();
    var matchingCrc =
        existingContents != null
            && Crc32.getRoughHash(existingContents) == Crc32.getRoughHash(contents);
    if (bothValid && matchingHash && matchingCrc) {
      return false;
    }
    archive.load(
        archive.getRevision() + 1,
        fromArchive.getNameHash() == 0 ? archive.getNameHash() : fromArchive.getNameHash(),
        archive.getCrc(),
        existingArchive == null ? fromArchive.getCompression() : archive.getCompression(),
        fromArchive.getFiles());
    storage.saveArchive(archive, contents);
    return true;
  }

  public void copyArchiveFiles(Archive fromArchive, List<Integer> fileIds) throws IOException {
    var existingArchive = index.getArchive(fromArchive.getId());
    var archive =
        existingArchive == null ? addArchiveToIndex(fromArchive.getArchiveData()) : existingArchive;
    var adFiles = fromArchive.loadArchiveFiles(null);
    ArchiveFiles files;
    if (existingArchive != null) {
      files = archive.loadArchiveFiles(null);
    } else {
      files = new ArchiveFiles();
    }
    var foundChanges =
        fromArchive.getNameHash() != 0 && fromArchive.getNameHash() != archive.getNameHash();
    for (var file : adFiles.getFiles()) {
      var existingFile = files.getFile(file.getFileId());
      if (existingFile == null
          || fileIds.contains(file.getFileId())
              && Crc32.getRoughHash(file.getContents())
                  != Crc32.getRoughHash(existingFile.getContents())) {
        var fileData =
            new FileData(
                fromArchive.getIndex().getId(),
                fromArchive.getId(),
                file.getFileId(),
                file.getNameHash());
        fileData.setContents(file.getContents());
        updateFile(archive, fileData);
        files.updateFile(fileData);
        foundChanges = true;
      }
    }
    if (!foundChanges) {
      return;
    }
    archive.load(
        archive.getRevision() + 1,
        fromArchive.getNameHash() == 0 ? archive.getNameHash() : fromArchive.getNameHash(),
        archive.getCrc(),
        existingArchive == null ? fromArchive.getCompression() : archive.getCompression(),
        archive.getFiles());
    var container = new Container(archive.getCompression(), -1);
    container.compress(files.saveContents(), null);
    var compressedData = container.data;
    storage.saveArchive(archive, compressedData);
  }

  private void updateFile(Archive archive, FileData fd) {
    if (fd.getId() == -1) {
      throw new IllegalArgumentException("File id is -1");
    }
    var existingFile = archive.getFile(fd.getId());
    if (existingFile == null && fd.getContents() == null) {
      return;
    }
    if (existingFile == null) {
      var newFile = new FileData(index.getId(), id, fd.getId(), fd.getNameHash());
      archive.getFiles().add(newFile);
      archive.getFiles().sort(Comparator.comparingInt(FileData::getId));
      return;
    }
    if (fd.getContents() == null) {
      for (var file : archive.getFiles()) {
        if (fd.getId() != file.getId()) {
          continue;
        }
        archive.getFiles().remove(file);
        break;
      }
      return;
    }
    if (fd.getNameHash() != 0) {
      existingFile.setNameHash(fd.getNameHash());
    }
    existingFile.setContents(fd.getContents());
  }

  private Archive addArchiveToIndex(ArchiveData archiveData) {
    var archive = new Archive(index, storage, archiveData);
    index.getArchives().add(archive);
    return archive;
  }

  private boolean filesMatch(FsFile existing, FileData loaded) {
    if (existing == null && loaded.getContents() == null) {
      return true;
    }
    if (existing == null && loaded.getContents() != null) {
      return false;
    }
    if (existing != null && loaded.getContents() == null) {
      return false;
    }
    return Crc32.getRoughHash(loaded.getContents()) == Crc32.getRoughHash(existing.getContents());
  }
}
