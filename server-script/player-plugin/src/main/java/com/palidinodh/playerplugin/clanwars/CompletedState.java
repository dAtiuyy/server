package com.palidinodh.playerplugin.clanwars;

import java.util.Map;

enum CompletedState {
  NONE,
  LOSE,
  WIN;

  private static final Map<ClanWarsRuleOption, String> DESCRIPTIONS;

  static {
    DESCRIPTIONS =
        Map.of(
            ClanWarsRuleOption.LAST_TEAM_STANDING,
            "was the last standing.",
            ClanWarsRuleOption.KILLS_25,
            "achieved 25 kills.",
            ClanWarsRuleOption.KILLS_50,
            "achieved 50 kills.",
            ClanWarsRuleOption.KILLS_100,
            "achieved 100 kills.",
            ClanWarsRuleOption.KILLS_200,
            "achieved 200 kills.",
            ClanWarsRuleOption.KILLS_500,
            "achieved 500 kills.",
            ClanWarsRuleOption.KILLS_1000,
            "achieved 1,000 kills.",
            ClanWarsRuleOption.KILLS_5000,
            "achieved 5,000 kills.",
            ClanWarsRuleOption.KILLS_10000,
            "achieved 10,000 kills.");
  }

  public static String getDescription(ClanWarsRuleOption option, boolean won) {
    var description = DESCRIPTIONS.get(option);
    if (description == null) {
      return won ? "You have won!" : "You have lost!";
    }
    return (won ? "Your" : "The other") + " team " + description;
  }
}
