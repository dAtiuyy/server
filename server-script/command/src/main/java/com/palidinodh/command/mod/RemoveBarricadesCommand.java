package com.palidinodh.command.mod;

import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("removebarricade")
class RemoveBarricadeCommand implements CommandHandler, CommandHandler.ModeratorRank {

  @Override
  public String getExample(String name) {
    return "- removes barricades from a 3x3 area around you";
  }

  @Override
  public void execute(Player player, String name, String message) {

    Tile[] tiles = {
      new Tile(player.getX() + 1, player.getY() + 1, player.getHeight()),
      new Tile(player.getX() + 1, player.getY() - 1, player.getHeight()),
      new Tile(player.getX() + 1, player.getY(), player.getHeight()),
      new Tile(player.getX() - 1, player.getY() + 1, player.getHeight()),
      new Tile(player.getX() - 1, player.getY() - 1, player.getHeight()),
      new Tile(player.getX() - 1, player.getY(), player.getHeight()),
      new Tile(player.getX(), player.getY() - 1, player.getHeight()),
      new Tile(player.getX(), player.getY() + 1, player.getHeight()),
    };

    for (Tile tile : tiles) {
      player.getController().removeMapObject(new MapObject(ObjectId.BARRICADE, 10, 0, tile));
    }
    player.getGameEncoder().sendMessage("Removed barricades.");
    DiscordBot.sendMessage(
        DiscordChannel.MODERATION_LOG, player.getUsername() + " removed a barricade.");
  }
}
