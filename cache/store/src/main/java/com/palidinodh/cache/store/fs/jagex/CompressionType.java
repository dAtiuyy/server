package com.palidinodh.cache.store.fs.jagex;

public final class CompressionType {

  public static final int NONE = 0;
  public static final int BZ2 = 1;
  public static final int GZ = 2;

  private CompressionType() {}
}
