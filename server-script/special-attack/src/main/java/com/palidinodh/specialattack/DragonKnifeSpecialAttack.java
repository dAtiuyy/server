package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.DRAGON_KNIFE,
  ItemId.DRAGON_KNIFE_P,
  ItemId.DRAGON_KNIFE_P_PLUS,
  ItemId.DRAGON_KNIFE_P_PLUS_PLUS
})
class DragonKnifeSpecialAttack extends SpecialAttack {

  DragonKnifeSpecialAttack() {
    var entry = Entry.builder();
    entry.itemId(ItemId.DRAGON_KNIFE);
    entry.drain(25);
    entry.animation(2068);
    entry.projectileId(699);
    entry.castSound(new Sound(2528));
    entry.doubleHit(true);
    addEntry(entry);

    entry = Entry.builder();
    entry.itemId(ItemId.DRAGON_KNIFE_P);
    entry.itemId(ItemId.DRAGON_KNIFE_P_PLUS);
    entry.itemId(ItemId.DRAGON_KNIFE_P_PLUS_PLUS);
    entry.drain(25);
    entry.animation(2068);
    entry.projectileId(1629);
    entry.castSound(new Sound(2528));
    entry.doubleHit(true);
    addEntry(entry);
  }
}
