package com.palidinodh.maparea.karamja.tzhaar.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.tzhaar.TzhaarPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.THE_INFERNO_30352)
class TheInfernoEntranceMapObject implements MapObjectHandler {

  private static void modeOptionsDialogue(Player player) {
    player.openOptionsDialogue(
        new DialogueOption(
            "Official modes.",
            (c, s) -> {
              officialModesDialogue(player);
            }),
        new DialogueOption(
            "Practice modes.",
            (c, s) -> {
              practiceOptionsDialogue(player);
            }));
  }

  private static void officialModesDialogue(Player player) {
    var plugin = player.getPlugin(TzhaarPlugin.class);
    var boost = plugin.getInfernoBoost();
    player.openOptionsDialogue(
        new DialogueOption(
            "Wave 1 (timed).",
            (c, s) -> {
              plugin.startInferno(1, true);
            }),
        new DialogueOption(
            "Wave " + plugin.getInfernoBoost() + ".",
            (c, s) -> {
              plugin.startInferno(boost, boost == 1);
            }));
  }

  private static void practiceOptionsDialogue(Player player) {
    var plugin = player.getPlugin(TzhaarPlugin.class);
    player.openOptionsDialogue(
        new DialogueOption(
            "Practice modes with your own stats and gear.",
            (c, s) -> {
              normalPracticeModesDialogue(player);
            }),
        new DialogueOption(
            "Practice modes with provided stats and gear.",
            (c, s) -> {
              spawnPracticeModesDialogue(player);
            }));
  }

  private static void normalPracticeModesDialogue(Player player) {
    var plugin = player.getPlugin(TzhaarPlugin.class);
    player.openOptionsDialogue(
        new DialogueOption(
            "Wave 67 (1 Jad).",
            (c, s) -> {
              plugin.startPracticeInferno(67, false);
            }),
        new DialogueOption(
            "Wave 68 (3 Jads).",
            (c, s) -> {
              plugin.startPracticeInferno(68, false);
            }),
        new DialogueOption(
            "Wave 69 (Zuk).",
            (c, s) -> {
              plugin.startPracticeInferno(69, false);
            }));
  }

  private static void spawnPracticeModesDialogue(Player player) {
    var plugin = player.getPlugin(TzhaarPlugin.class);
    player.openOptionsDialogue(
        new DialogueOption(
            "Wave 1.",
            (c, s) -> {
              plugin.startPracticeInferno(1, true);
            }),
        new DialogueOption(
            "Wave 67 (1 Jad).",
            (c, s) -> {
              plugin.startPracticeInferno(67, true);
            }),
        new DialogueOption(
            "Wave 68 (3 Jads).",
            (c, s) -> {
              plugin.startPracticeInferno(68, true);
            }),
        new DialogueOption(
            "Wave 69 (Zuk).",
            (c, s) -> {
              plugin.startPracticeInferno(69, true);
            }),
        new DialogueOption(
            "Choose wave.",
            (c, s) -> {
              player
                  .getGameEncoder()
                  .sendEnterAmount(
                      "Choose Wave",
                      ie -> {
                        if (player.isLocked()) {
                          return;
                        }
                        if (!player.getArea().is("Tzhaar")) {
                          return;
                        }
                        if (ie < 1 || ie > 69) {
                          return;
                        }
                        plugin.startPracticeInferno(ie, true);
                      });
            }));
  }

  private static void sacrificeCapeDialogue(Player player) {
    player.openOptionsDialogue(
        new DialogueOption(
            "Sacrifice a Fire Cape.",
            (c, s) -> {
              if (!player.getInventory().hasItem(ItemId.FIRE_CAPE)) {
                player.getGameEncoder().sendMessage("You need a Fire Cape to do this.");
                return;
              }
              player
                  .getGameEncoder()
                  .sendMessage(
                      "You sacrifice your Fire Cape, allowing you access to The Inferno...");
              player.getInventory().deleteItem(ItemId.FIRE_CAPE);
              player.getPlugin(TzhaarPlugin.class).getInferno().setSacrificedCape(true);
            }),
        new DialogueOption("Nevermind."));
  }

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.getInventory().hasItem(ItemId.PURPLE_SWEETS)) {
      player.getGameEncoder().sendMessage("You can't take purple sweets into The Inferno.");
      return;
    }
    var plugin = player.getPlugin(TzhaarPlugin.class);
    if (plugin.getInferno().isSacrificedCape()) {
      plugin.getInferno().setWave(0);
      modeOptionsDialogue(player);
    } else {
      sacrificeCapeDialogue(player);
    }
  }
}
