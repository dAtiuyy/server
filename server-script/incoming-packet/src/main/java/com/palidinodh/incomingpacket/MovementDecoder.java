package com.palidinodh.incomingpacket;

import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.osrscore.io.incomingpacket.InStreamKey;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.shared.Movement;
import com.palidinodh.rs.setting.UserRank;

class MovementDecoder extends IncomingPacketDecoder {

  @Override
  public boolean execute(Player player, Stream stream) {
    var option = getInt(InStreamKey.PACKET_OPTION);
    var tileX = getInt(InStreamKey.TILE_X);
    var tileY = getInt(InStreamKey.TILE_Y);
    var ctrlRun = getInt(InStreamKey.CTRL_RUN);
    player.clearIdleTime();
    var message =
        "[Movement(" + option + ")] tileX=" + tileX + "; tileY=" + tileY + "; ctrlRun=" + ctrlRun;
    if (player.getGameMode().isUnset() || player.getCombat().isDead()) {
      return false;
    }
    if (player.getDialogue() != null && player.getDialogue().isDisableActions()) {
      return false;
    }
    if (player.getMovement().isViewing()
        || player.getAppearance().getNpcId() == Movement.VIEWING_NPC_ID) {
      if (!player.getMovement().isTeleportStateStarted()
          && !player.getMovement().isTeleportStateFinished()) {
        player.getMovement().stopViewing();
        player.getWidgetManager().removeInteractiveWidgets();
      }
      return false;
    }
    player.getCombat().setAttackingEntity(null);
    return true;
  }

  @Override
  public boolean complete(Player player) {
    var option = getInt(InStreamKey.PACKET_OPTION);
    var tileX = getInt(InStreamKey.TILE_X);
    var tileY = getInt(InStreamKey.TILE_Y);
    var ctrlRun = getInt(InStreamKey.CTRL_RUN);
    if (player.isLocked()) {
      return false;
    }
    if (!player.getMovement().isRouting() && player.getX() == tileX && player.getY() == tileY) {
      player.getCombat().clear(false);
      return false;
    }
    if (player.getController().movementHook(option, tileX, tileY, ctrlRun)) {
      return true;
    }
    if (player.isUsergroup(UserRank.ADMINISTRATOR) && ctrlRun == 1) {
      player.getMovement().teleport(tileX, tileY, player.getHeight());
      return true;
    }
    player.getMovement().fullRoute(tileX, tileY, ctrlRun);
    return true;
  }
}
