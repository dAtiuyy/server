package com.palidinodh.npccombat;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import java.util.Arrays;
import java.util.List;

class AbyssalSireTentacleCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.TENTACLE_5912);
    combat.spawn(NpcCombatSpawn.builder().respawnDelay(6000).build());
    combat.stats(
        NpcCombatStats.builder().attackLevel(180).bonus(BonusType.MELEE_ATTACK, 65).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.focus(
        NpcCombatFocus.builder().bypassMapObjects(true).disableFollowingOpponent(true).build());

    var style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.UNDERNEATH)
            .subHitStyleType(HitStyleType.MELEE)
            .meleeAttackStyle(BonusType.ATTACK_CRUSH)
            .build());
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(7109).attackSpeed(4);
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }
}
