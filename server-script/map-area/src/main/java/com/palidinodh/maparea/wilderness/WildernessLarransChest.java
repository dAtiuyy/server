package com.palidinodh.maparea.wilderness;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import java.util.List;

public class WildernessLarransChest {

  private static final NpcCombatDrop DROPS;

  static {
    var drop = NpcCombatDrop.builder();
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(256);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DAGONHAI_HAT)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DAGONHAI_ROBE_TOP)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DAGONHAI_ROBE_BOTTOM)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(20);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_TUNA_NOTED, 150, 525)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_LOBSTER_NOTED, 150, 525)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_SWORDFISH_NOTED, 150, 450)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_MONKFISH_NOTED, 150, 450)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_SHARK_NOTED, 150, 375)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_SEA_TURTLE_NOTED, 120, 300)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.RAW_MANTA_RAY_NOTED, 120, 240)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_9, 1, 1, 1)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_10, 1, 1, 1)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(8);
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_ORE_NOTED, 15, 20, 5)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_BAR_NOTED, 350, 550, 5)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_LOGS_NOTED, 180, 220, 5)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_DART_TIP, 80, 200, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PALM_TREE_SEED, 3, 5, 1)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_SEED, 3, 4, 1)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CELASTRUS_SEED, 3, 5, 1)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGONFRUIT_TREE_SEED, 3, 5, 1)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.REDWOOD_TREE_SEED, 1, 1, 1)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORSTOL_SEED, 4, 6, 1)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPDRAGON_SEED, 4, 6, 1)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RANARR_SEED, 4, 6, 1)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_5, 1, 1, 1)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_6, 1, 1, 1)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_7, 1, 1, 1)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_8, 1, 1, 1)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DIAMOND_NOTED, 35, 45, 8)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_RUBY_NOTED, 35, 45, 8)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COAL_NOTED, 450, 650, 8)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GOLD_ORE_NOTED, 150, 250, 6)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_ARROWTIPS, 100, 250, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 75_000, 175_000, 5)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.IRON_ORE_NOTED, 500, 650, 5)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_FULL_HELM_NOTED, 3, 5, 5)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATEBODY_NOTED, 2, 3, 5)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATELEGS_NOTED, 2, 3, 5)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 4_500, 7_500, 5)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_1, 1, 1, 1)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_2, 1, 1, 1)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_3, 1, 1, 1)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.ANTIQUE_EMBLEM_TIER_4, 1, 1, 1)));
    drop.table(dropTable.build());
    DROPS = drop.build();
  }

  public static void open(Player player, MapObject mapObject) {
    if (!player.getInventory().hasItem(ItemId.LARRANS_KEY)) {
      player.getGameEncoder().sendMessage("You need a key to open this.");
      return;
    }
    List<Item> items;
    if (mapObject.getId() == ObjectId.LARRANS_BIG_CHEST_34832) {
      items = DROPS.getItems(player);
    } else {
      return;
    }
    player.getGameEncoder().sendMessage("You find some treasure in the chest!");
    player.getInventory().deleteItem(ItemId.LARRANS_KEY);
    items.forEach(i -> player.getInventory().addOrDropItem(i));
    var plugin = player.getPlugin(SlayerPlugin.class);
    plugin.incrimentLarransKeys();
    player
        .getGameEncoder()
        .sendMessage("You have opened " + plugin.getLarransKeys() + " Larrans chests!");
  }
}
