package com.palidinodh.maparea.zeah.lizardmancaves.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class LizardmanCavesNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(1286, 9952), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1292, 9958), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1302, 9945), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1308, 9950), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1318, 9947), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1324, 9953), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1324, 9963), NpcId.LIZARDMAN_SHAMAN_150));
    spawns.add(new NpcSpawn(4, new Tile(1331, 9969), NpcId.LIZARDMAN_SHAMAN_150));

    return spawns;
  }
}
