package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.TileHitEvent;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.route.AttackRoute;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class LizardmanShamanCombat extends NpcCombat {

  private static final NpcCombatStyle ACIDIC =
      NpcCombatStyle.builder()
          .type(NpcCombatStyleType.MAGIC)
          .damage(NpcCombatDamage.builder().maximum(30).minimum(25).ignorePrayer(true).build())
          .effect(NpcCombatEffect.builder().includeMiss(true).chance(25).poison(10).build())
          .projectile(NpcCombatProjectile.builder().id(1293).speedMinimumDistance(10).build())
          .animation(7193)
          .attackSpeed(4)
          .attackRange(5)
          .targetTileGraphic(new Graphic(1294))
          .specialAttack(NpcCombatTargetTile.builder().radius(1).build())
          .build();
  private static final int JUMP_TIME = 6;
  private static final int JUMP_LAND = 1;

  @Inject private Npc npc;
  private boolean loaded;
  private Tile jumpTile;
  private int jumpTime;
  private int spawnAttackDelay;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .rareDropTableDenominator(25)
            .clue(ClueScrollType.HARD, 1200)
            .clue(ClueScrollType.ELITE, 200);
    var dropTable =
        NpcCombatDropTable.builder().probabilityDenominator(5000).broadcast(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_WARHAMMER)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(250);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.XERICS_TALISMAN_INERT)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATERMELON_SEED, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TORSTOL_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAPLE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PAPAYA_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PALM_TREE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.YEW_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SPIRIT_SEED)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RED_DHIDE_VAMB)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_EARTH_STAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.EARTH_BATTLESTAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_MED_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_CHAINBODY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_WARHAMMER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_ORE_NOTED, 3, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM_NOTED, 1, 3)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_DWARF_WEED_NOTED, 1, 3)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME_NOTED, 1, 3)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE_NOTED, 1, 3)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WILLOW_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RANARR_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SNAPDRAGON_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 45, 57)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_RUNE, 66, 74)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 22, 30)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.IRON_ORE_NOTED, 24, 35)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COAL_NOTED, 20, 23)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_RUNE, 71, 80)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 100, 6000)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.XERICIAN_FABRIC, 2)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LIZARDMAN_FANG, 10, 14)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHILLI_POTATO, 2)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BIG_BONES)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.LIZARDMAN_SHAMAN_150);
    combat.hitpoints(NpcCombatHitpoints.total(150));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(120)
            .magicLevel(130)
            .rangedLevel(120)
            .defenceLevel(140)
            .bonus(BonusType.MELEE_ATTACK, 45)
            .bonus(BonusType.ATTACK_RANGED, 45)
            .bonus(BonusType.DEFENCE_SLASH, 40)
            .bonus(BonusType.DEFENCE_CRUSH, 30)
            .bonus(BonusType.DEFENCE_MAGIC, 50)
            .build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.killCount(NpcCombatKillCount.SAVE);
    combat.deathAnimation(7196).blockAnimation(7194);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(31));
    style.animation(7158).attackSpeed(4);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(31));
    style.animation(7193).attackSpeed(4).attackRange(5);
    style.projectile(NpcCombatProjectile.id(1291));
    combat.style(style.build());

    var raidsDrop = NpcCombatDrop.builder().additionalPlayers(255);
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BIG_BONES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BUCHU_SEED, 8, 12)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GOLPAR_SEED, 8, 12)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NOXIFER_SEED, 8, 12)));
    raidsDrop.table(dropTable.build());

    var raidsCombat = NpcCombatDefinition.builder();
    raidsCombat.id(NpcId.LIZARDMAN_SHAMAN).id(NpcId.LIZARDMAN_SHAMAN_7574);
    raidsCombat.hitpoints(NpcCombatHitpoints.total(150));
    raidsCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(130)
            .magicLevel(130)
            .rangedLevel(130)
            .defenceLevel(210)
            .bonus(BonusType.MELEE_ATTACK, 58)
            .bonus(BonusType.ATTACK_RANGED, 56)
            .bonus(BonusType.DEFENCE_STAB, 102)
            .bonus(BonusType.DEFENCE_SLASH, 160)
            .bonus(BonusType.DEFENCE_CRUSH, 150)
            .bonus(BonusType.DEFENCE_MAGIC, 160)
            .build());
    raidsCombat.aggression(NpcCombatAggression.builder().range(6).always(true).build());
    raidsCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    raidsCombat.deathAnimation(7196).blockAnimation(7194);
    raidsCombat.drop(raidsDrop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(31));
    style.animation(7158).attackSpeed(4);
    raidsCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(31));
    style.animation(7193).attackSpeed(4).attackRange(9);
    style.projectile(NpcCombatProjectile.id(1291));
    raidsCombat.style(style.build());

    var cursedCombat = NpcCombatDefinition.builder();
    cursedCombat.id(NpcId.CURSED_LIZARDMAN_SHAMAN_150_16015);
    cursedCombat.hitpoints(NpcCombatHitpoints.total(225));
    cursedCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(120)
            .magicLevel(130)
            .rangedLevel(120)
            .defenceLevel(140)
            .bonus(BonusType.MELEE_ATTACK, 45)
            .bonus(BonusType.ATTACK_RANGED, 45)
            .bonus(BonusType.DEFENCE_SLASH, 40)
            .bonus(BonusType.DEFENCE_CRUSH, 30)
            .bonus(BonusType.DEFENCE_MAGIC, 50)
            .build());
    cursedCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    cursedCombat.killCount(NpcCombatKillCount.SAVE);
    cursedCombat.deathAnimation(7196).blockAnimation(7194);
    cursedCombat.drop(drop.rolls(2).build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(31));
    style.animation(7158).attackSpeed(4);
    cursedCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(31));
    style.animation(7193).attackSpeed(4).attackRange(5);
    style.projectile(NpcCombatProjectile.id(1291));
    cursedCombat.style(style.build());

    return Arrays.asList(combat.build(), raidsCombat.build(), cursedCombat.build());
  }

  @Override
  public void spawnHook() {
    loaded = false;
    jumpTile = null;
    jumpTime = 0;
  }

  @Override
  public void tickStartHook() {
    if (!loaded
        && (npc.getId() == NpcId.LIZARDMAN_SHAMAN || npc.getId() == NpcId.LIZARDMAN_SHAMAN_7574)) {
      loadProfile();
      return;
    }
    if (!npc.isVisible() || isDead()) {
      return;
    }
    if (spawnAttackDelay > 0) {
      spawnAttackDelay--;
    }
    jumpTick();
    specialAttackTick();
  }

  @Override
  public NpcCombatStyle attackTickCombatStyleHook(NpcCombatStyle combatStyle, Entity opponent) {
    if (!(getAttackingEntity() instanceof Player) || PRandom.randomE(4) != 0) {
      return combatStyle;
    }
    int type = PRandom.randomE(1);
    if (type == 0) {
      return ACIDIC;
    }
    return combatStyle;
  }

  @Override
  public void targetTileHitEventHook(
      NpcCombatStyle combatStyle,
      Entity opponent,
      TileHitEvent tileHitEvent,
      Graphic.Projectile projectile) {
    tileHitEvent.setShamanPoison(true);
  }

  @Override
  public void attackedActionsOpponentHook(Entity opponent) {
    if (!opponent.isPlayer() || npc.getId() != NpcId.CURSED_LIZARDMAN_SHAMAN_150_16015) {
      return;
    }
    opponent.asPlayer().getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
  }

  @Override
  public NpcCombatDropTable deathDropItemsTableHook(
      Player player, int dropRateDivider, int roll, NpcCombatDropTable table) {
    if (npc.getId() == NpcId.CURSED_LIZARDMAN_SHAMAN_150_16015) {
      if (!player.getPlugin(SlayerPlugin.class).isWildernessTask(npc)) {
        player.getGameEncoder().sendMessage("Without an assigned task, the loot turns to dust...");
        return null;
      }
    }
    return table;
  }

  @Override
  public double damageInflictedHook(NpcCombatStyle combatStyle, Entity opponent, double damage) {
    if (combatStyle == ACIDIC) {
      if (damage > 0 && damage < 25) {
        damage = 25;
      }
      if (opponent.isPlayer()) {
        Player player = (Player) opponent;
        if (player.getEquipment().wearingFullShayzien5()) {
          damage = 0;
        }
      }
    }
    return damage;
  }

  private void loadProfile() {
    if (loaded || npc.getController().getPlayers().isEmpty()) {
      return;
    }
    loaded = true;
    var averageHP = 0;
    var playerMultiplier = 1.0;
    var players = npc.getController().getPlayers();
    for (var player : players) {
      averageHP += player.getCombat().getMaxHitpoints();
      playerMultiplier += 0.5;
    }
    averageHP /= players.size();
    var hitpoints = (int) ((50 + players.size() * 25 + averageHP * 2) * playerMultiplier / 2.5);
    setMaxHitpoints(hitpoints);
    setHitpoints(getMaxHitpoints());
  }

  private void jumpTick() {
    if (jumpTime <= 0) {
      return;
    }
    jumpTime--;
    if (jumpTile != null) {
      if (jumpTime == JUMP_TIME / 2) {
        npc.getMovement().teleport(jumpTile);
      }
      if (jumpTime == 0) {
        clearHitEvents();
        npc.setAnimation(6946);
        jumpTile = null;
        jumpTime = JUMP_LAND;
        npc.setLock(jumpTime);
      }
    } else {
      if (jumpTime == 0) {
        clearHitEvents();
        var players = npc.getController().getPlayers();
        for (var player : players) {
          if (npc.withinDistance(player, 0)) {
            player.getCombat().addHit(new Hit(25 + PRandom.randomI(5)));
          }
        }
      }
    }
  }

  private void specialAttackTick() {
    if (!isAttacking()) {
      return;
    }
    var attackingEntity = getAttackingEntity();
    if (attackingEntity == null || !attackingEntity.isPlayer()) {
      return;
    }
    if (npc.isLocked()) {
      return;
    }
    if (isHitDelayed()) {
      return;
    }
    if (!npc.withinDistance(attackingEntity, 5)) {
      return;
    }
    if (!AttackRoute.allow(npc, attackingEntity, true)) {
      return;
    }
    if (jumpTile == null
        && jumpTime == 0
        && PRandom.randomE(8) == 0
        && npc.getController().routeAllow(attackingEntity)) {
      npc.setAnimation(7152);
      jumpTile = new Tile(getAttackingEntity());
      jumpTime = JUMP_TIME;
      npc.setLock(jumpTime + 1);
    } else if (PRandom.randomE(8) == 0 && spawnAttackDelay == 0) {
      spawnAttackDelay = 15;
      npc.setAnimation(7157);
      setHitDelay(4);
      var tiles =
          new Tile[] {
            new Tile(getAttackingEntity()).moveX(1),
            new Tile(getAttackingEntity()).moveY(1),
            new Tile(getAttackingEntity()).moveX(-1)
          };
      for (var i = 0; i < tiles.length; i++) {
        var t = tiles[i];
        if (!getAttackingEntity().getController().routeAllow(t)) {
          continue;
        }
        var spawn = npc.getController().addNpc(new NpcSpawn(t, NpcId.SPAWN_6768));
        spawn.getMovement().setFollowing(getAttackingEntity());
        spawn.pluginScript("countdown1", 7 + i * 2);
      }
    }
  }
}
