package com.palidinodh.maparea.morytania.canifis.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class CanifisNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(3508, 3479), NpcId.HUNTING_EXPERT_1504));
    spawns.add(new NpcSpawn(new Tile(3509, 3483), NpcId.WIZARD_4399));
    spawns.add(new NpcSpawn(4, new Tile(3481, 3489), NpcId.YADVIGA_24));
    spawns.add(new NpcSpawn(4, new Tile(3483, 3493), NpcId.IRINA_24));
    spawns.add(new NpcSpawn(4, new Tile(3482, 3497), NpcId.SOFIYA_24));
    spawns.add(new NpcSpawn(4, new Tile(3490, 3493), NpcId.BORIS_24));
    spawns.add(new NpcSpawn(4, new Tile(3496, 3493), NpcId.YURI_24));
    spawns.add(new NpcSpawn(4, new Tile(3500, 3491), NpcId.SVETLANA_24));
    spawns.add(new NpcSpawn(4, new Tile(3503, 3487), NpcId.ZOJA_24));
    spawns.add(new NpcSpawn(4, new Tile(3506, 3483), NpcId.LEV_24));
    spawns.add(new NpcSpawn(4, new Tile(3477, 3496), NpcId.SHOP_KEEPER));
    spawns.add(new NpcSpawn(4, new Tile(3491, 3501), NpcId.SBOTT));
    spawns.add(new NpcSpawn(4, new Tile(3507, 3494), NpcId.RUFUS));
    spawns.add(new NpcSpawn(4, new Tile(3499, 3482), NpcId.MILLA_24));
    spawns.add(new NpcSpawn(4, new Tile(3493, 3483), NpcId.NIKOLAI_24));
    spawns.add(new NpcSpawn(4, new Tile(3478, 3483), NpcId.GEORGY_24));
    spawns.add(new NpcSpawn(4, new Tile(3479, 3485), NpcId.TAXIDERMIST));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3514, 3478), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3514, 3479), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3514, 3481), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3514, 3480), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3514, 3482), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3514, 3483), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(4, new Tile(3494, 3469), NpcId.ROAVAR));
    spawns.add(new NpcSpawn(4, new Tile(3490, 3472), NpcId.VERA_24));
    spawns.add(new NpcSpawn(4, new Tile(3491, 3477), NpcId.IMRE_24));
    spawns.add(new NpcSpawn(4, new Tile(3496, 3477), NpcId.MALAK));
    spawns.add(new NpcSpawn(new Tile(3503, 3477), NpcId.STRANGER));
    spawns.add(new NpcSpawn(4, new Tile(3501, 3505), NpcId.BARKER));

    return spawns;
  }
}
