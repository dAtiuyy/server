package com.palidinodh.maparea.kandarin.ardougne.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.ARDOUGNE_WALL_DOOR, ObjectId.ARDOUGNE_WALL_DOOR_8739})
class WestWallDoorsMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.getX() >= 2558) {
      player.getMovement().teleport(player.getX() - 3, player.getY());
    } else {
      player.getMovement().teleport(player.getX() + 3, player.getY());
    }
  }
}
