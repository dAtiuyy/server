package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.DRAGON_SWORD, ItemId.DRAGON_SWORD_21206})
class DragonSwordSpecialAttack extends SpecialAttack {

  DragonSwordSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(40);
    entry.animation(7515);
    entry.castGraphic(new Graphic(1369, 100));
    entry.accuracyModifier(1.25);
    entry.damageModifier(1.25);
    addEntry(entry);
  }
}
