package com.palidinodh.command.overseer;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.ban.BannedByUser;
import com.palidinodh.rs.ban.BannedUser;
import com.palidinodh.rs.ban.Bans;
import com.palidinodh.rs.ban.UserBan;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.rs.setting.UserRank;

@ReferenceName("userban")
class UserBanCommand implements CommandHandler, CommandHandler.OverseerRank {

  private static int getMaxBanLength(Player player) {
    if (player.isUsergroup(UserRank.ADMINISTRATOR)) {
      return 24;
    }
    if (player.isUsergroup(UserRank.SENIOR_MODERATOR)
        || player.isUsergroup(UserRank.ADVERTISEMENT_MANAGER)
        || player.isUsergroup(UserRank.COMMUNITY_MANAGER)) {
      return 12;
    }
    if (player.isUsergroup(UserRank.MODERATOR)) {
      return 6;
    }
    return 1;
  }

  @Override
  public boolean canUse(Player player) {
    return !Settings.getInstance().isLocal();
  }

  @Override
  public void execute(Player player, String name, String message) {
    player
        .getGameEncoder()
        .sendEnterString(
            "Username",
            ue -> {
              var player2 = player.getWorld().getPlayerByUsername(ue);
              if (player2 == null) {
                player.getGameEncoder().sendMessage("Unable to find user " + ue + ".");
                return;
              }
              var maxBanLength = getMaxBanLength(player);
              player
                  .getGameEncoder()
                  .sendEnterAmount(
                      "Number of Hours (Max: " + maxBanLength + ")",
                      he -> {
                        if (he <= 0 || he > maxBanLength) {
                          player
                              .getGameEncoder()
                              .sendMessage(
                                  "Bans longer than "
                                      + maxBanLength
                                      + " hours need to be applied on the website.");
                          return;
                        }
                        player
                            .getGameEncoder()
                            .sendEnterString(
                                "Reason",
                                re -> {
                                  Bans.addBan(
                                      new UserBan(
                                          new BannedUser(
                                              player2.getId(),
                                              player2.getUsername().toLowerCase(),
                                              player2.getSession().getRemoteAddress(),
                                              player2.getSession().getMacAddress(),
                                              player2.getSession().getUuid()),
                                          new BannedByUser(player.getId(), player.getUsername()),
                                          he,
                                          re));
                                  player2.unlock();
                                  player2.getGameEncoder().sendLogout();
                                  player2.setVisible(false);
                                  var duration = he > 0 ? "for " + he + " hours" : "permanently";
                                  player
                                      .getGameEncoder()
                                      .sendMessage(ue + " has been user banned " + duration + ".");
                                  DiscordBot.sendMessage(
                                      DiscordChannel.MODERATION_LOG,
                                      player.getUsername()
                                          + " banned "
                                          + player2.getUsername()
                                          + " for "
                                          + duration
                                          + ".");
                                  player.log(
                                      PlayerLogType.STAFF,
                                      "user banned " + player2.getLogName() + " " + duration);
                                });
                      });
            });
  }
}
