package com.palidinodh.playerplugin.dailyskillingtask;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.definition.osrs.NpcDefinition;
import com.palidinodh.cache.definition.osrs.ObjectDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.MapObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;

@Builder
@Getter
class SkillingTask {

  private static final Map<Integer, List<SkillingTask>> tasks = new HashMap<>();

  private int skill;
  private String name;
  private int level;
  private int quantity;
  @Singular private List<Integer> items;
  @Singular private List<Integer> objects;
  @Singular private List<Integer> npcs;

  static {
    tasks.put(Skills.MINING, getMiningTasks());
    tasks.put(Skills.AGILITY, getAgilityTasks());
    tasks.put(Skills.SMITHING, getSmithingTasks());
    tasks.put(Skills.HERBLORE, getHerbloreTasks());
    tasks.put(Skills.FISHING, getFishingTasks());
    tasks.put(Skills.THIEVING, getThievingTasks());
    tasks.put(Skills.COOKING, getCookingTasks());
    tasks.put(Skills.CRAFTING, getCraftingTasks());
    tasks.put(Skills.FIREMAKING, getFiremakingTasks());
    tasks.put(Skills.FLETCHING, getFletchingTasks());
    tasks.put(Skills.WOODCUTTING, getWoodcuttingTasks());
    tasks.put(Skills.RUNECRAFTING, getRunecraftingTasks());
    tasks.put(Skills.HUNTER, getHunterTasks());
  }

  public static int getExperience(int lvl) {
    return (int) (StrictMath.pow(lvl, 2) / 600 * 525 * 3);
  }

  public static List<SkillingTask> getTasks(Player player) {
    var list = new ArrayList<SkillingTask>();
    for (var entry : tasks.entrySet()) {
      var skill = entry.getKey();
      var ts = entry.getValue();
      var playerLevel = player.getSkills().getLevelForXP(skill);
      for (var task : ts) {
        if (playerLevel < task.getLevel()) {
          continue;
        }
        var foundTasks = !list.isEmpty();
        var levelDifference = Math.abs(playerLevel - task.getLevel());
        if (foundTasks && levelDifference > 24) {
          continue;
        }
        list.add(task);
        if (playerLevel == 99) {
          continue;
        }
        if (foundTasks && levelDifference > 12) {
          continue;
        }
        for (var c = 0; c < 3; c++) {
          list.add(task);
        }
      }
    }
    return list;
  }

  public static List<SkillingTask> getMiningTasks() {
    var list = new ArrayList<SkillingTask>();

    list.add(builder().skill(Skills.MINING).level(1).quantity(250).item(ItemId.COPPER_ORE).build());
    list.add(builder().skill(Skills.MINING).level(1).quantity(250).item(ItemId.TIN_ORE).build());
    list.add(builder().skill(Skills.MINING).level(15).quantity(250).item(ItemId.IRON_ORE).build());
    list.add(builder().skill(Skills.MINING).level(30).quantity(250).item(ItemId.COAL).build());
    list.add(builder().skill(Skills.MINING).level(40).quantity(250).item(ItemId.GOLD_ORE).build());
    list.add(
        builder()
            .skill(Skills.MINING)
            .name("Gem rock")
            .level(40)
            .quantity(250)
            .item(ItemId.ZENYTE_SHARD)
            .item(ItemId.UNCUT_ONYX)
            .item(ItemId.UNCUT_DRAGONSTONE)
            .item(ItemId.UNCUT_DIAMOND)
            .item(ItemId.UNCUT_RUBY)
            .item(ItemId.UNCUT_EMERALD)
            .item(ItemId.UNCUT_SAPPHIRE)
            .item(ItemId.UNCUT_RED_TOPAZ)
            .item(ItemId.UNCUT_JADE)
            .item(ItemId.UNCUT_OPAL)
            .build());
    list.add(
        builder().skill(Skills.MINING).level(55).quantity(250).item(ItemId.MITHRIL_ORE).build());
    list.add(
        builder().skill(Skills.MINING).level(70).quantity(250).item(ItemId.ADAMANTITE_ORE).build());
    list.add(
        builder().skill(Skills.MINING).level(85).quantity(100).item(ItemId.RUNITE_ORE).build());
    list.add(builder().skill(Skills.MINING).level(92).quantity(250).item(ItemId.AMETHYST).build());

    list.sort((c1, c2) -> Integer.compare(c2.getLevel(), c1.getLevel()));
    return list;
  }

  public static List<SkillingTask> getAgilityTasks() {
    var list = new ArrayList<SkillingTask>();

    list.add(
        builder()
            .skill(Skills.AGILITY)
            .name("Gnome Stronghold")
            .level(1)
            .quantity(25)
            .object(ObjectId.OBSTACLE_PIPE_23138)
            .object(ObjectId.OBSTACLE_PIPE_23139)
            .build());
    list.add(
        builder()
            .skill(Skills.AGILITY)
            .name("Barbarian Outpost")
            .level(35)
            .quantity(25)
            .object(ObjectId.CRUMBLING_WALL_1948)
            .build());
    list.add(
        builder()
            .skill(Skills.AGILITY)
            .name("Canifis rooftop")
            .level(40)
            .quantity(25)
            .object(ObjectId.GAP_14897)
            .build());
    list.add(
        builder()
            .skill(Skills.AGILITY)
            .name("Seers' Village rooftop")
            .level(60)
            .quantity(25)
            .object(ObjectId.EDGE_14931)
            .build());
    list.add(
        builder()
            .skill(Skills.AGILITY)
            .name("Rellekka rooftop")
            .level(80)
            .quantity(25)
            .object(ObjectId.PILE_OF_FISH)
            .build());
    list.add(
        builder()
            .skill(Skills.AGILITY)
            .name("Ardougne rooftop")
            .level(90)
            .quantity(25)
            .object(ObjectId.GAP_15612)
            .build());

    list.sort((c1, c2) -> Integer.compare(c2.getLevel(), c1.getLevel()));
    return list;
  }

  public static List<SkillingTask> getSmithingTasks() {
    var list = new ArrayList<SkillingTask>();

    list.add(
        builder()
            .skill(Skills.SMITHING)
            .name("Bronze")
            .level(1)
            .quantity(50)
            .item(ItemId.BRONZE_BAR)
            .build());
    list.add(
        builder()
            .skill(Skills.SMITHING)
            .name("Iron")
            .level(15)
            .quantity(50)
            .item(ItemId.IRON_BAR)
            .build());
    list.add(
        builder()
            .skill(Skills.SMITHING)
            .name("Steel")
            .level(30)
            .quantity(50)
            .item(ItemId.STEEL_BAR)
            .build());
    list.add(
        builder()
            .skill(Skills.SMITHING)
            .name("Gold")
            .level(40)
            .quantity(50)
            .item(ItemId.GOLD_BAR)
            .build());
    list.add(
        builder()
            .skill(Skills.SMITHING)
            .name("Mithril")
            .level(50)
            .quantity(50)
            .item(ItemId.MITHRIL_BAR)
            .build());
    list.add(
        builder()
            .skill(Skills.SMITHING)
            .name("Adamantite")
            .level(70)
            .quantity(50)
            .item(ItemId.ADAMANTITE_BAR)
            .build());
    list.add(
        builder()
            .skill(Skills.SMITHING)
            .name("Runite")
            .level(85)
            .quantity(50)
            .item(ItemId.RUNITE_BAR)
            .build());

    list.sort((c1, c2) -> Integer.compare(c2.getLevel(), c1.getLevel()));
    return list;
  }

  public static List<SkillingTask> getHerbloreTasks() {
    var list = new ArrayList<SkillingTask>();

    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(3)
            .quantity(25)
            .item(ItemId.ATTACK_POTION_3)
            .item(ItemId.ATTACK_POTION_4)
            .build());
    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(12)
            .quantity(25)
            .item(ItemId.STRENGTH_POTION_3)
            .item(ItemId.STRENGTH_POTION_4)
            .build());
    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(26)
            .quantity(25)
            .item(ItemId.ENERGY_POTION_3)
            .item(ItemId.ENERGY_POTION_4)
            .build());
    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(30)
            .quantity(25)
            .item(ItemId.DEFENCE_POTION_3)
            .item(ItemId.DEFENCE_POTION_4)
            .build());
    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(38)
            .quantity(25)
            .item(ItemId.PRAYER_POTION_3)
            .item(ItemId.PRAYER_POTION_4)
            .build());
    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(45)
            .quantity(25)
            .item(ItemId.SUPER_ATTACK_3)
            .item(ItemId.SUPER_ATTACK_4)
            .build());
    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(52)
            .quantity(25)
            .item(ItemId.SUPER_ENERGY_3)
            .item(ItemId.SUPER_ENERGY_4)
            .build());
    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(55)
            .quantity(25)
            .item(ItemId.SUPER_STRENGTH_3)
            .item(ItemId.SUPER_STRENGTH_4)
            .build());
    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(63)
            .quantity(25)
            .item(ItemId.SUPER_RESTORE_3)
            .item(ItemId.SUPER_RESTORE_4)
            .build());
    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(66)
            .quantity(25)
            .item(ItemId.SUPER_DEFENCE_3)
            .item(ItemId.SUPER_DEFENCE_4)
            .build());
    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(68)
            .quantity(25)
            .item(ItemId.ANTIDOTE_PLUS_PLUS_3)
            .item(ItemId.ANTIDOTE_PLUS_PLUS_4)
            .build());
    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(69)
            .quantity(25)
            .item(ItemId.ANTIFIRE_POTION_3)
            .item(ItemId.ANTIFIRE_POTION_4)
            .build());
    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(72)
            .quantity(25)
            .item(ItemId.RANGING_POTION_3)
            .item(ItemId.RANGING_POTION_4)
            .build());
    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(76)
            .quantity(25)
            .item(ItemId.MAGIC_POTION_3)
            .item(ItemId.MAGIC_POTION_4)
            .build());
    list.add(
        builder()
            .skill(Skills.HERBLORE)
            .level(81)
            .quantity(25)
            .item(ItemId.SARADOMIN_BREW_3)
            .item(ItemId.SARADOMIN_BREW_4)
            .build());

    list.sort((c1, c2) -> Integer.compare(c2.getLevel(), c1.getLevel()));
    return list;
  }

  public static List<SkillingTask> getFishingTasks() {
    var list = new ArrayList<SkillingTask>();

    list.add(
        builder().skill(Skills.FISHING).level(1).quantity(250).item(ItemId.RAW_SHRIMPS).build());
    list.add(
        builder().skill(Skills.FISHING).level(5).quantity(250).item(ItemId.RAW_SARDINE).build());
    list.add(
        builder().skill(Skills.FISHING).level(15).quantity(250).item(ItemId.RAW_ANCHOVIES).build());
    list.add(builder().skill(Skills.FISHING).level(25).quantity(250).item(ItemId.RAW_PIKE).build());
    list.add(
        builder().skill(Skills.FISHING).level(30).quantity(250).item(ItemId.RAW_SALMON).build());
    list.add(
        builder().skill(Skills.FISHING).level(40).quantity(250).item(ItemId.RAW_LOBSTER).build());
    list.add(builder().skill(Skills.FISHING).level(46).quantity(250).item(ItemId.RAW_BASS).build());
    list.add(
        builder().skill(Skills.FISHING).level(50).quantity(250).item(ItemId.RAW_SWORDFISH).build());
    list.add(
        builder().skill(Skills.FISHING).level(62).quantity(250).item(ItemId.RAW_MONKFISH).build());
    list.add(
        builder().skill(Skills.FISHING).level(65).quantity(250).item(ItemId.RAW_KARAMBWAN).build());
    list.add(
        builder()
            .skill(Skills.FISHING)
            .level(70)
            .quantity(250)
            .item(ItemId.LEAPING_STURGEON)
            .build());
    list.add(
        builder().skill(Skills.FISHING).level(76).quantity(250).item(ItemId.RAW_SHARK).build());
    list.add(
        builder().skill(Skills.FISHING).level(80).quantity(250).item(ItemId.INFERNAL_EEL).build());
    list.add(
        builder()
            .skill(Skills.FISHING)
            .level(82)
            .quantity(250)
            .item(ItemId.RAW_ANGLERFISH)
            .build());
    list.add(
        builder().skill(Skills.FISHING).level(85).quantity(250).item(ItemId.RAW_DARK_CRAB).build());
    list.add(
        builder().skill(Skills.FISHING).level(87).quantity(250).item(ItemId.SACRED_EEL).build());

    list.sort((c1, c2) -> Integer.compare(c2.getLevel(), c1.getLevel()));
    return list;
  }

  public static List<SkillingTask> getThievingTasks() {
    var list = new ArrayList<SkillingTask>();

    list.add(
        builder()
            .skill(Skills.THIEVING)
            .name("Man/Woman")
            .level(1)
            .quantity(250)
            .npc(NpcId.MAN_2)
            .npc(NpcId.MAN_2_3101)
            .npc(NpcId.MAN_2_3106)
            .npc(NpcId.MAN_2_3107)
            .npc(NpcId.MAN_2_3108)
            .npc(NpcId.MAN_2_3109)
            .npc(NpcId.MAN_2_3110)
            .npc(NpcId.MAN_2_3260)
            .npc(NpcId.MAN_2_3261)
            .npc(NpcId.MAN_2_3264)
            .npc(NpcId.MAN_2_3265)
            .npc(NpcId.MAN_2_3266)
            .npc(NpcId.MAN_2_3652)
            .npc(NpcId.MAN_2_6987)
            .npc(NpcId.MAN_2_6988)
            .npc(NpcId.MAN_2_6989)
            .npc(NpcId.MAN_2_7547)
            .npc(NpcId.WOMAN_2)
            .npc(NpcId.WOMAN_2_3111)
            .npc(NpcId.WOMAN_2_3112)
            .npc(NpcId.WOMAN_2_3113)
            .npc(NpcId.WOMAN_2_3268)
            .npc(NpcId.WOMAN_2_6990)
            .npc(NpcId.WOMAN_2_6991)
            .npc(NpcId.WOMAN_2_6992)
            .build());
    list.add(
        builder()
            .skill(Skills.THIEVING)
            .name("Warrior woman/Al-Kharid warrior")
            .level(25)
            .quantity(250)
            .npc(NpcId.WARRIOR_WOMAN_24)
            .npc(NpcId.AL_KHARID_WARRIOR_9)
            .build());
    list.add(
        builder()
            .skill(Skills.THIEVING)
            .level(38)
            .quantity(250)
            .npc(NpcId.MASTER_FARMER)
            .npc(NpcId.MASTER_FARMER_3258)
            .npc(NpcId.MARTIN_THE_MASTER_GARDENER)
            .build());
    list.add(
        builder()
            .skill(Skills.THIEVING)
            .level(40)
            .quantity(250)
            .npc(NpcId.GUARD_19)
            .npc(NpcId.GUARD_20)
            .npc(NpcId.GUARD_21_3010)
            .npc(NpcId.GUARD_21_3094)
            .npc(NpcId.GUARD_21_3269)
            .npc(NpcId.GUARD_22_3270)
            .npc(NpcId.GUARD_22_3272)
            .npc(NpcId.GUARD_22_3273)
            .npc(NpcId.GUARD_22_3274)
            .build());
    list.add(
        builder()
            .skill(Skills.THIEVING)
            .level(55)
            .quantity(250)
            .npc(NpcId.KNIGHT_OF_ARDOUGNE_46)
            .build());
    list.add(
        builder().skill(Skills.THIEVING).level(65).quantity(250).npc(NpcId.WATCHMAN_33).build());
    list.add(
        builder().skill(Skills.THIEVING).level(70).quantity(250).npc(NpcId.PALADIN_62).build());
    list.add(
        builder()
            .skill(Skills.THIEVING)
            .level(75)
            .quantity(250)
            .npc(NpcId.GNOME_5130)
            .npc(NpcId.GNOME_CHILD_1)
            .npc(NpcId.GNOME_CHILD_1_6078)
            .npc(NpcId.GNOME_CHILD_1_6079)
            .npc(NpcId.GNOME_WOMAN_1)
            .npc(NpcId.GNOME_WOMAN_1_6087)
            .npc(NpcId.GNOME_1_6094)
            .npc(NpcId.GNOME_1_6095)
            .npc(NpcId.GNOME_1_6096)
            .build());
    list.add(builder().skill(Skills.THIEVING).level(80).quantity(250).npc(NpcId.HERO_69).build());
    list.add(
        builder()
            .skill(Skills.THIEVING)
            .name("Elf")
            .level(85)
            .quantity(250)
            .npc(NpcId.GOREU)
            .npc(NpcId.YSGAWYN)
            .npc(NpcId.ARVEL)
            .npc(NpcId.MAWRTH)
            .npc(NpcId.KELYN)
            .build());

    list.sort((c1, c2) -> Integer.compare(c2.getLevel(), c1.getLevel()));
    return list;
  }

  public static List<SkillingTask> getCookingTasks() {
    var list = new ArrayList<SkillingTask>();

    list.add(builder().skill(Skills.COOKING).level(1).quantity(100).item(ItemId.SHRIMPS).build());
    list.add(builder().skill(Skills.COOKING).level(1).quantity(100).item(ItemId.ANCHOVIES).build());
    list.add(builder().skill(Skills.COOKING).level(1).quantity(100).item(ItemId.SARDINE).build());
    list.add(
        builder()
            .skill(Skills.COOKING)
            .name("Trout/Salmon")
            .level(15)
            .quantity(100)
            .item(ItemId.TROUT)
            .item(ItemId.SALMON)
            .build());
    list.add(builder().skill(Skills.COOKING).level(20).quantity(100).item(ItemId.PIKE).build());
    list.add(
        builder()
            .skill(Skills.COOKING)
            .level(30)
            .quantity(100)
            .item(ItemId.COOKED_KARAMBWAN)
            .build());
    list.add(builder().skill(Skills.COOKING).level(40).quantity(100).item(ItemId.LOBSTER).build());
    list.add(builder().skill(Skills.COOKING).level(43).quantity(100).item(ItemId.BASS).build());
    list.add(
        builder().skill(Skills.COOKING).level(45).quantity(100).item(ItemId.SWORDFISH).build());
    list.add(builder().skill(Skills.COOKING).level(62).quantity(100).item(ItemId.MONKFISH).build());
    list.add(builder().skill(Skills.COOKING).level(80).quantity(100).item(ItemId.SHARK).build());
    list.add(
        builder().skill(Skills.COOKING).level(84).quantity(100).item(ItemId.ANGLERFISH).build());
    list.add(
        builder().skill(Skills.COOKING).level(90).quantity(100).item(ItemId.DARK_CRAB).build());

    list.sort((c1, c2) -> Integer.compare(c2.getLevel(), c1.getLevel()));
    return list;
  }

  public static List<SkillingTask> getCraftingTasks() {
    var list = new ArrayList<SkillingTask>();

    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(1)
            .name("Opal jewelry")
            .quantity(50)
            .item(ItemId.OPAL_RING)
            .item(ItemId.OPAL_NECKLACE)
            .item(ItemId.OPAL_BRACELET)
            .item(ItemId.OPAL_AMULET)
            .build());
    list.add(
        builder().skill(Skills.CRAFTING).level(1).quantity(50).item(ItemId.BEER_GLASS).build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .name("Leather")
            .level(1)
            .quantity(50)
            .item(ItemId.LEATHER)
            .item(ItemId.LEATHER_GLOVES)
            .item(ItemId.LEATHER_BOOTS)
            .item(ItemId.LEATHER_COWL)
            .item(ItemId.LEATHER_VAMBRACES)
            .item(ItemId.LEATHER_BODY)
            .item(ItemId.LEATHER_CHAPS)
            .item(ItemId.COIF)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(4)
            .quantity(50)
            .item(ItemId.EMPTY_CANDLE_LANTERN)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(5)
            .name("Gold jewelry")
            .quantity(50)
            .item(ItemId.GOLD_RING)
            .item(ItemId.GOLD_NECKLACE)
            .item(ItemId.GOLD_BRACELET)
            .item(ItemId.GOLD_AMULET)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(12)
            .quantity(50)
            .item(ItemId.EMPTY_OIL_LAMP)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(13)
            .name("Jade jewelry")
            .quantity(50)
            .item(ItemId.JADE_RING)
            .item(ItemId.JADE_NECKLACE)
            .item(ItemId.JADE_BRACELET)
            .item(ItemId.JADE_AMULET)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(16)
            .name("Topaz jewelry")
            .quantity(50)
            .item(ItemId.TOPAZ_RING)
            .item(ItemId.TOPAZ_NECKLACE)
            .item(ItemId.TOPAZ_BRACELET)
            .item(ItemId.TOPAZ_AMULET)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(20)
            .name("Sapphire jewelry")
            .quantity(50)
            .item(ItemId.SAPPHIRE_RING)
            .item(ItemId.SAPPHIRE_NECKLACE)
            .item(ItemId.SAPPHIRE_BRACELET)
            .item(ItemId.SAPPHIRE_AMULET)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(27)
            .name("Emerald jewelry")
            .quantity(50)
            .item(ItemId.EMERALD_RING)
            .item(ItemId.EMERALD_NECKLACE)
            .item(ItemId.EMERALD_BRACELET)
            .item(ItemId.EMERALD_AMULET)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(28)
            .name("Hard leather")
            .quantity(50)
            .item(ItemId.HARD_LEATHER)
            .item(ItemId.HARDLEATHER_BODY)
            .item(ItemId.HARD_LEATHER_SHIELD)
            .build());
    list.add(builder().skill(Skills.CRAFTING).level(33).quantity(50).item(ItemId.VIAL).build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(34)
            .name("Ruby jewelry")
            .quantity(50)
            .item(ItemId.RUBY_RING)
            .item(ItemId.RUBY_NECKLACE)
            .item(ItemId.RUBY_BRACELET)
            .item(ItemId.RUBY_AMULET)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(42)
            .quantity(50)
            .item(ItemId.EMPTY_FISHBOWL)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(43)
            .name("Diamond jewelry")
            .quantity(50)
            .item(ItemId.DIAMOND_RING)
            .item(ItemId.DIAMOND_NECKLACE)
            .item(ItemId.DIAMOND_BRACELET)
            .item(ItemId.DIAMOND_AMULET)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(45)
            .name("Snakeskin")
            .quantity(50)
            .item(ItemId.SNAKESKIN)
            .item(ItemId.SNAKESKIN_BOOTS)
            .item(ItemId.SNAKESKIN_VAMBRACES)
            .item(ItemId.SNAKESKIN_BANDANA)
            .item(ItemId.SNAKESKIN_CHAPS)
            .item(ItemId.SNAKESKIN_BODY)
            .item(ItemId.SNAKESKIN_SHIELD)
            .build());
    list.add(
        builder().skill(Skills.CRAFTING).level(46).quantity(50).item(ItemId.UNPOWERED_ORB).build());
    list.add(
        builder().skill(Skills.CRAFTING).level(49).quantity(50).item(ItemId.LANTERN_LENS).build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(54)
            .quantity(50)
            .item(ItemId.EARTH_BATTLESTAFF)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(55)
            .name("Dragonstone jewelry")
            .quantity(50)
            .item(ItemId.DRAGONSTONE_RING)
            .item(ItemId.DRAGON_NECKLACE)
            .item(ItemId.DRAGONSTONE_BRACELET)
            .item(ItemId.DRAGONSTONE_AMULET)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(57)
            .name("Green dragon leather")
            .quantity(50)
            .item(ItemId.GREEN_DRAGON_LEATHER)
            .item(ItemId.GREEN_DHIDE_VAMB)
            .item(ItemId.GREEN_DHIDE_CHAPS)
            .item(ItemId.GREEN_DHIDE_BODY)
            .item(ItemId.GREEN_DHIDE_SHIELD)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(62)
            .quantity(50)
            .item(ItemId.FIRE_BATTLESTAFF)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(66)
            .quantity(50)
            .item(ItemId.AIR_BATTLESTAFF)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(66)
            .name("Blue dragon leather")
            .quantity(50)
            .item(ItemId.BLUE_DRAGON_LEATHER)
            .item(ItemId.BLUE_DHIDE_VAMB)
            .item(ItemId.BLUE_DHIDE_CHAPS)
            .item(ItemId.BLUE_DHIDE_BODY)
            .item(ItemId.BLUE_DHIDE_SHIELD)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(73)
            .name("Red dragon leather")
            .quantity(50)
            .item(ItemId.RED_DRAGON_LEATHER)
            .item(ItemId.RED_DHIDE_VAMB)
            .item(ItemId.RED_DHIDE_CHAPS)
            .item(ItemId.RED_DHIDE_BODY)
            .item(ItemId.RED_DHIDE_SHIELD)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(79)
            .name("Black dragon leather")
            .quantity(50)
            .item(ItemId.BLACK_DRAGON_LEATHER)
            .item(ItemId.BLACK_DHIDE_VAMB)
            .item(ItemId.BLACK_DHIDE_CHAPS)
            .item(ItemId.BLACK_DHIDE_BODY)
            .item(ItemId.BLACK_DHIDE_SHIELD)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(83)
            .name("Amethyst")
            .quantity(50)
            .item(ItemId.AMETHYST_BOLT_TIPS)
            .item(ItemId.AMETHYST_ARROWTIPS)
            .item(ItemId.AMETHYST_JAVELIN_HEADS)
            .item(ItemId.AMETHYST_DART_TIP)
            .build());
    list.add(
        builder()
            .skill(Skills.CRAFTING)
            .level(87)
            .quantity(50)
            .item(ItemId.EMPTY_LIGHT_ORB)
            .build());

    list.sort((c1, c2) -> Integer.compare(c2.getLevel(), c1.getLevel()));
    return list;
  }

  public static List<SkillingTask> getFiremakingTasks() {
    var list = new ArrayList<SkillingTask>();

    list.add(builder().skill(Skills.FIREMAKING).level(1).quantity(100).item(ItemId.LOGS).build());
    list.add(
        builder().skill(Skills.FIREMAKING).level(15).quantity(100).item(ItemId.OAK_LOGS).build());
    list.add(
        builder()
            .skill(Skills.FIREMAKING)
            .level(30)
            .quantity(100)
            .item(ItemId.WILLOW_LOGS)
            .build());
    list.add(
        builder().skill(Skills.FIREMAKING).level(45).quantity(100).item(ItemId.MAPLE_LOGS).build());
    list.add(
        builder().skill(Skills.FIREMAKING).level(60).quantity(100).item(ItemId.YEW_LOGS).build());
    list.add(
        builder().skill(Skills.FIREMAKING).level(75).quantity(100).item(ItemId.MAGIC_LOGS).build());
    list.add(
        builder()
            .skill(Skills.FIREMAKING)
            .level(90)
            .quantity(100)
            .item(ItemId.REDWOOD_LOGS)
            .build());

    list.sort((c1, c2) -> Integer.compare(c2.getLevel(), c1.getLevel()));
    return list;
  }

  public static List<SkillingTask> getFletchingTasks() {
    var list = new ArrayList<SkillingTask>();

    list.add(
        builder()
            .skill(Skills.FLETCHING)
            .name("Logs")
            .level(1)
            .quantity(100)
            .item(ItemId.SHORTBOW)
            .item(ItemId.LONGBOW)
            .item(ItemId.BRONZE_CROSSBOW)
            .build());
    list.add(
        builder()
            .skill(Skills.FLETCHING)
            .name("Oak")
            .level(20)
            .quantity(100)
            .item(ItemId.OAK_SHORTBOW)
            .item(ItemId.OAK_LONGBOW)
            .item(ItemId.BLURITE_CROSSBOW)
            .item(ItemId.OAK_SHIELD)
            .build());
    list.add(
        builder()
            .skill(Skills.FLETCHING)
            .name("Willow")
            .level(35)
            .quantity(100)
            .item(ItemId.WILLOW_SHORTBOW)
            .item(ItemId.WILLOW_LONGBOW)
            .item(ItemId.IRON_CROSSBOW)
            .item(ItemId.WILLOW_SHIELD)
            .build());
    list.add(
        builder()
            .skill(Skills.FLETCHING)
            .name("Maple")
            .level(50)
            .quantity(100)
            .item(ItemId.MAPLE_SHORTBOW)
            .item(ItemId.MAPLE_LONGBOW)
            .item(ItemId.MITH_CROSSBOW)
            .item(ItemId.MAPLE_SHIELD)
            .build());
    list.add(
        builder()
            .skill(Skills.FLETCHING)
            .name("Yew")
            .level(65)
            .quantity(100)
            .item(ItemId.YEW_SHORTBOW)
            .item(ItemId.YEW_LONGBOW)
            .item(ItemId.RUNE_CROSSBOW)
            .item(ItemId.YEW_SHIELD)
            .build());
    list.add(
        builder()
            .skill(Skills.FLETCHING)
            .name("Magic")
            .level(80)
            .quantity(100)
            .item(ItemId.MAGIC_SHORTBOW)
            .item(ItemId.MAGIC_LONGBOW)
            .item(ItemId.DRAGON_CROSSBOW)
            .item(ItemId.MAGIC_SHIELD)
            .build());

    list.sort((c1, c2) -> Integer.compare(c2.getLevel(), c1.getLevel()));
    return list;
  }

  public static List<SkillingTask> getWoodcuttingTasks() {
    var list = new ArrayList<SkillingTask>();

    list.add(builder().skill(Skills.WOODCUTTING).level(1).quantity(250).item(ItemId.LOGS).build());
    list.add(
        builder().skill(Skills.WOODCUTTING).level(15).quantity(250).item(ItemId.OAK_LOGS).build());
    list.add(
        builder()
            .skill(Skills.WOODCUTTING)
            .level(30)
            .quantity(250)
            .item(ItemId.WILLOW_LOGS)
            .build());
    list.add(
        builder()
            .skill(Skills.WOODCUTTING)
            .level(45)
            .quantity(250)
            .item(ItemId.MAPLE_LOGS)
            .build());
    list.add(
        builder().skill(Skills.WOODCUTTING).level(60).quantity(250).item(ItemId.YEW_LOGS).build());
    list.add(
        builder()
            .skill(Skills.WOODCUTTING)
            .level(75)
            .quantity(250)
            .item(ItemId.MAGIC_LOGS)
            .build());
    list.add(
        builder()
            .skill(Skills.WOODCUTTING)
            .level(90)
            .quantity(250)
            .item(ItemId.REDWOOD_LOGS)
            .build());

    list.sort((c1, c2) -> Integer.compare(c2.getLevel(), c1.getLevel()));
    return list;
  }

  public static List<SkillingTask> getRunecraftingTasks() {
    var list = new ArrayList<SkillingTask>();

    list.add(
        builder().skill(Skills.RUNECRAFTING).level(1).quantity(250).item(ItemId.AIR_RUNE).build());
    list.add(
        builder().skill(Skills.RUNECRAFTING).level(2).quantity(250).item(ItemId.MIND_RUNE).build());
    list.add(
        builder()
            .skill(Skills.RUNECRAFTING)
            .level(5)
            .quantity(250)
            .item(ItemId.WATER_RUNE)
            .build());
    list.add(
        builder()
            .skill(Skills.RUNECRAFTING)
            .level(9)
            .quantity(250)
            .item(ItemId.EARTH_RUNE)
            .build());
    list.add(
        builder()
            .skill(Skills.RUNECRAFTING)
            .level(14)
            .quantity(250)
            .item(ItemId.FIRE_RUNE)
            .build());
    list.add(
        builder()
            .skill(Skills.RUNECRAFTING)
            .level(20)
            .quantity(250)
            .item(ItemId.BODY_RUNE)
            .build());
    list.add(
        builder()
            .skill(Skills.RUNECRAFTING)
            .level(27)
            .quantity(250)
            .item(ItemId.COSMIC_RUNE)
            .build());
    list.add(
        builder()
            .skill(Skills.RUNECRAFTING)
            .level(35)
            .quantity(250)
            .item(ItemId.CHAOS_RUNE)
            .build());
    list.add(
        builder()
            .skill(Skills.RUNECRAFTING)
            .level(40)
            .quantity(250)
            .item(ItemId.ASTRAL_RUNE)
            .build());
    list.add(
        builder()
            .skill(Skills.RUNECRAFTING)
            .level(44)
            .quantity(250)
            .item(ItemId.NATURE_RUNE)
            .build());
    list.add(
        builder().skill(Skills.RUNECRAFTING).level(54).quantity(250).item(ItemId.LAW_RUNE).build());
    list.add(
        builder()
            .skill(Skills.RUNECRAFTING)
            .level(65)
            .quantity(250)
            .item(ItemId.DEATH_RUNE)
            .build());
    list.add(
        builder()
            .skill(Skills.RUNECRAFTING)
            .level(77)
            .quantity(250)
            .item(ItemId.BLOOD_RUNE)
            .build());
    list.add(
        builder()
            .skill(Skills.RUNECRAFTING)
            .level(90)
            .quantity(250)
            .item(ItemId.SOUL_RUNE)
            .build());
    list.add(
        builder()
            .skill(Skills.RUNECRAFTING)
            .level(95)
            .quantity(250)
            .item(ItemId.WRATH_RUNE)
            .build());

    list.sort((c1, c2) -> Integer.compare(c2.getLevel(), c1.getLevel()));
    return list;
  }

  public static List<SkillingTask> getHunterTasks() {
    var list = new ArrayList<SkillingTask>();

    list.add(
        builder()
            .skill(Skills.HUNTER)
            .name("Crimson swift")
            .level(1)
            .quantity(15)
            .object(ObjectId.BIRD_SNARE_9373)
            .build());
    list.add(
        builder()
            .skill(Skills.HUNTER)
            .name("Golden warbler")
            .level(5)
            .quantity(15)
            .object(ObjectId.BIRD_SNARE_9377)
            .build());
    list.add(
        builder()
            .skill(Skills.HUNTER)
            .name("Copper longtail")
            .level(9)
            .quantity(15)
            .object(ObjectId.BIRD_SNARE_9379)
            .build());
    list.add(
        builder()
            .skill(Skills.HUNTER)
            .name("Cerulean twitch")
            .level(11)
            .quantity(15)
            .object(ObjectId.BIRD_SNARE_9375)
            .build());
    list.add(
        builder().skill(Skills.HUNTER).level(15).quantity(100).item(ItemId.RUBY_HARVEST).build());
    list.add(
        builder()
            .skill(Skills.HUNTER)
            .name("Tropical wagtail")
            .level(19)
            .quantity(15)
            .object(ObjectId.BIRD_SNARE_9348)
            .build());
    list.add(
        builder()
            .skill(Skills.HUNTER)
            .level(25)
            .quantity(100)
            .item(ItemId.SAPPHIRE_GLACIALIS)
            .build());
    list.add(
        builder()
            .skill(Skills.HUNTER)
            .name("Ferret")
            .level(27)
            .quantity(100)
            .object(ObjectId.SHAKING_BOX_9384)
            .build());
    list.add(
        builder()
            .skill(Skills.HUNTER)
            .name("Swamp lizard")
            .level(29)
            .quantity(50)
            .object(ObjectId.NET_TRAP_9004)
            .build());
    list.add(
        builder().skill(Skills.HUNTER).level(35).quantity(100).item(ItemId.SNOWY_KNIGHT).build());
    list.add(
        builder().skill(Skills.HUNTER).level(45).quantity(100).item(ItemId.BLACK_WARLOCK).build());
    list.add(
        builder()
            .skill(Skills.HUNTER)
            .name("Orange salamander")
            .level(47)
            .quantity(50)
            .object(ObjectId.NET_TRAP_8734)
            .build());
    list.add(
        builder()
            .skill(Skills.HUNTER)
            .name("Chinchompa")
            .level(53)
            .quantity(500)
            .item(ItemId.CHINCHOMPA)
            .build());
    list.add(
        builder()
            .skill(Skills.HUNTER)
            .name("Red salamander")
            .level(59)
            .quantity(50)
            .object(ObjectId.NET_TRAP_8986)
            .build());
    list.add(
        builder()
            .skill(Skills.HUNTER)
            .name("Red chinchompa")
            .level(63)
            .quantity(500)
            .item(ItemId.RED_CHINCHOMPA)
            .build());
    list.add(
        builder()
            .skill(Skills.HUNTER)
            .name("Black salamander")
            .level(67)
            .quantity(50)
            .object(ObjectId.NET_TRAP_8996)
            .build());
    list.add(
        builder()
            .skill(Skills.HUNTER)
            .name("Black chinchompa")
            .level(73)
            .quantity(500)
            .item(ItemId.BLACK_CHINCHOMPA)
            .build());

    list.sort((c1, c2) -> Integer.compare(c2.getLevel(), c1.getLevel()));
    return list;
  }

  public String getName() {
    if (name != null) {
      return name;
    }
    if (!items.isEmpty()) {
      return ItemDefinition.getName(items.get(0));
    }
    if (!objects.isEmpty()) {
      return ObjectDefinition.getName(objects.get(0));
    }
    if (!npcs.isEmpty()) {
      return NpcDefinition.getName(npcs.get(0));
    }
    return "";
  }

  public int getMatchCount(
      List<Item> consumedItems, List<Item> createdItems, Npc npc, MapObject mapObject) {
    var itemMatches = Math.max(getItemMatchCount(consumedItems), getItemMatchCount(createdItems));
    if (itemMatches > 0) {
      return itemMatches;
    }
    if (isNpcMatch(npc)) {
      return 1;
    }
    if (isMapObjectMatch(mapObject)) {
      return 1;
    }
    return 0;
  }

  private int getItemMatchCount(List<Item> list) {
    if (list == null || list.isEmpty()) {
      return 0;
    }
    int matches = 0;
    for (var i : list) {
      if (!items.contains(i.getId())) {
        continue;
      }
      switch (i.getId()) {
        case ItemId.FIRE_RUNE:
        case ItemId.WATER_RUNE:
        case ItemId.AIR_RUNE:
        case ItemId.EARTH_RUNE:
        case ItemId.MIND_RUNE:
        case ItemId.BODY_RUNE:
        case ItemId.DEATH_RUNE:
        case ItemId.NATURE_RUNE:
        case ItemId.CHAOS_RUNE:
        case ItemId.LAW_RUNE:
        case ItemId.COSMIC_RUNE:
        case ItemId.BLOOD_RUNE:
        case ItemId.SOUL_RUNE:
        case ItemId.STEAM_RUNE:
        case ItemId.MIST_RUNE:
        case ItemId.DUST_RUNE:
        case ItemId.SMOKE_RUNE:
        case ItemId.MUD_RUNE:
        case ItemId.LAVA_RUNE:
        case ItemId.ASTRAL_RUNE:
        case ItemId.WRATH_RUNE:
        case ItemId.CHINCHOMPA:
        case ItemId.RED_CHINCHOMPA:
        case ItemId.BLACK_CHINCHOMPA:
          matches += i.getAmount();
          break;
        default:
          matches += i.getDef().isStackable() ? 1 : i.getAmount();
          break;
      }
    }
    return matches;
  }

  private boolean isNpcMatch(Npc npc) {
    if (npc == null) {
      return false;
    }
    return npcs.contains(npc.getId());
  }

  private boolean isMapObjectMatch(MapObject mapObject) {
    if (mapObject == null) {
      return false;
    }
    return objects.contains(mapObject.getId());
  }
}
