package com.palidinodh.maparea.morytania.canifis.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.HUNTING_EXPERT_1504)
class HuntingExpertNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    SpellTeleport.normalTeleport(player, new Tile(3530, 3444));
  }
}
