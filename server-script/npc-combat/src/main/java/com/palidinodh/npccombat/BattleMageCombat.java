package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import java.util.Arrays;
import java.util.List;

class BattleMageCombat extends NpcCombat {

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .gemDropTableDenominator(256)
            .clue(ClueScrollType.HARD, 128)
            .clue(ClueScrollType.MEDIUM, 64);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(512).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MASTER_WAND)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.INFINITY_TOP)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.INFINITY_HAT)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.INFINITY_BOOTS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.INFINITY_GLOVES)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.INFINITY_BOTTOMS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGES_BOOK)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_FIRE_STAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_WATER_STAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_AIR_STAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_EARTH_STAFF)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BATTLESTAFF_NOTED, 3, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNPOWERED_ORB_NOTED, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_ORB_NOTED, 3, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATER_ORB_NOTED, 3, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_ORB_NOTED, 3, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.EARTH_ORB_NOTED, 3, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_BATTLESTAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATER_BATTLESTAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_BATTLESTAFF)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.EARTH_BATTLESTAFF)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_RUNE, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATER_RUNE, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_RUNE, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.EARTH_RUNE, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BODY_RUNE, 35)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NATURE_RUNE, 25)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ASTRAL_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MIND_RUNE, 50)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COSMIC_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SOUL_RUNE, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WRATH_RUNE, 10)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEAM_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MIST_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DUST_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SMOKE_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MUD_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAVA_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STAFF_OF_AIR)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STAFF_OF_WATER)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STAFF_OF_EARTH)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STAFF_OF_FIRE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.ALWAYS);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BONES)));
    drop.table(dropTable.build());

    var zamorakCombat = NpcCombatDefinition.builder();
    zamorakCombat.id(NpcId.BATTLE_MAGE_54);
    zamorakCombat.hitpoints(NpcCombatHitpoints.total(100));
    zamorakCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(60)
            .bonus(BonusType.ATTACK_MAGIC, 16)
            .bonus(BonusType.DEFENCE_MAGIC, 16)
            .build());
    zamorakCombat.aggression(NpcCombatAggression.builder().range(3).build());
    zamorakCombat.killCount(NpcCombatKillCount.SAVE);
    zamorakCombat.deathAnimation(836).blockAnimation(415);
    zamorakCombat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(20).splashOnMiss(true).build());
    style.animation(811).attackSpeed(5).attackRange(7);
    style.targetGraphic(new Graphic(78));
    zamorakCombat.style(style.build());

    var saradominCombat = NpcCombatDefinition.builder();
    saradominCombat.id(NpcId.BATTLE_MAGE_54_1611);
    saradominCombat.hitpoints(NpcCombatHitpoints.total(100));
    saradominCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(60)
            .bonus(BonusType.ATTACK_MAGIC, 16)
            .bonus(BonusType.DEFENCE_MAGIC, 16)
            .build());
    saradominCombat.aggression(NpcCombatAggression.builder().range(3).build());
    saradominCombat.killCount(NpcCombatKillCount.SAVE);
    saradominCombat.deathAnimation(836).blockAnimation(415);
    saradominCombat.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(20).splashOnMiss(true).build());
    style.animation(811).attackSpeed(5).attackRange(7);
    style.targetGraphic(new Graphic(76, 100));
    saradominCombat.style(style.build());

    var guthixCombat = NpcCombatDefinition.builder();
    guthixCombat.id(NpcId.BATTLE_MAGE_54_1612);
    guthixCombat.hitpoints(NpcCombatHitpoints.total(100));
    guthixCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(60)
            .bonus(BonusType.ATTACK_MAGIC, 16)
            .bonus(BonusType.DEFENCE_MAGIC, 16)
            .build());
    guthixCombat.aggression(NpcCombatAggression.builder().range(3).build());
    guthixCombat.killCount(NpcCombatKillCount.SAVE);
    guthixCombat.deathAnimation(196).blockAnimation(193);
    guthixCombat.drop(drop.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(20).splashOnMiss(true).build());
    style.animation(198).attackSpeed(5).attackRange(7);
    style.targetGraphic(new Graphic(77, 100));
    guthixCombat.style(style.build());

    return Arrays.asList(zamorakCombat.build(), saradominCombat.build(), guthixCombat.build());
  }

  @Override
  public boolean canBeAggressiveHook(Entity opponent) {
    if (!opponent.isPlayer()) {
      return true;
    }
    var player = opponent.asPlayer();
    if (npc.getId() == NpcId.BATTLE_MAGE_54
        && (player.getEquipment().getWeaponId() == ItemId.ZAMORAK_STAFF
            || player.getEquipment().getCapeId() == ItemId.ZAMORAK_CAPE
            || player.getEquipment().getCapeId() == ItemId.ZAMORAK_MAX_CAPE
            || player.getEquipment().getCapeId() == ItemId.IMBUED_ZAMORAK_CAPE
            || player.getEquipment().getCapeId() == ItemId.IMBUED_ZAMORAK_MAX_CAPE)) {
      return false;
    }
    if (npc.getId() == NpcId.BATTLE_MAGE_54_1611
        && (player.getEquipment().getWeaponId() == ItemId.SARADOMIN_STAFF
            || player.getEquipment().getCapeId() == ItemId.SARADOMIN_CAPE
            || player.getEquipment().getCapeId() == ItemId.SARADOMIN_MAX_CAPE
            || player.getEquipment().getCapeId() == ItemId.IMBUED_SARADOMIN_CAPE
            || player.getEquipment().getCapeId() == ItemId.IMBUED_SARADOMIN_MAX_CAPE)) {
      return false;
    }
    return npc.getId() != NpcId.BATTLE_MAGE_54_1612
        || (player.getEquipment().getWeaponId() != ItemId.GUTHIX_STAFF
            && player.getEquipment().getCapeId() != ItemId.GUTHIX_CAPE
            && player.getEquipment().getCapeId() != ItemId.GUTHIX_MAX_CAPE
            && player.getEquipment().getCapeId() != ItemId.IMBUED_GUTHIX_CAPE
            && player.getEquipment().getCapeId() != ItemId.IMBUED_GUTHIX_MAX_CAPE);
  }
}
