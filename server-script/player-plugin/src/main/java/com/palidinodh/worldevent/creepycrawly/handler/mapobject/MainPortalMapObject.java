package com.palidinodh.worldevent.creepycrawly.handler.mapobject;

import com.google.inject.Inject;
import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.worldevent.creepycrawly.CreepyCrawlyController;
import com.palidinodh.worldevent.creepycrawly.CreepyCrawlyEvent;

@ReferenceId(ObjectId.PORTAL_6282)
class MainPortalMapObject implements MapObjectHandler {

  @Inject private CreepyCrawlyEvent event;

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (player.getController().is(CreepyCrawlyController.class)) {
      player.getController().stop();
    } else {
      player.openOptionsDialogue(
          new DialogueOption("Join public game.", (c, s) -> event.joinPublicGame(player)),
          new DialogueOption(
              "Join private game.",
              (c, s) -> {
                player
                    .getGameEncoder()
                    .sendEnterString(
                        "Chooose a name:",
                        name -> {
                          event.joinPrivateGame(player, name);
                        });
              }),
          new DialogueOption(
              "Create private game.",
              (c, s) -> {
                player
                    .getGameEncoder()
                    .sendEnterString(
                        "Chooose a name:",
                        name -> {
                          event.createPrivateGame(player, name);
                        });
              }));
    }
  }
}
