package com.palidinodh.playerplugin.bountyhunter;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.cache.widget.SpellbookChild;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.combat.RecentCombatant;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PTime;
import com.palidinodh.worldevent.wildernesshotspot.WildernessHotspotEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class BountyHunterPlugin implements PlayerPlugin {

  private static final boolean ENABLED = true;
  private static final int MAX_COMBAT_DIFFERENCE = 10;
  private static final int UPDATE_DELAY = 100;
  private static final int ASSIGNED_LOCKED_DELAY = 200;
  private static final int WILD_STATE_LOCKED_DELAY = 100;
  private static final int CANCELLED_LOCKED_DELAY = 100;
  private static final int MISSING_TIME = 200;
  private static final List<Item> TARGET_SPELL_RUNES =
      Arrays.asList(
          new Item(ItemId.LAW_RUNE), new Item(ItemId.DEATH_RUNE), new Item(ItemId.CHAOS_RUNE));

  @Inject private transient Player player;
  private transient RiskTier targetRisk;
  private transient MysteriousEmblem targetEmblem;
  private transient String targetLevelRange;
  @Getter private transient boolean inWilderness;
  private transient int teleportCooldown;
  private transient int updateDelay = UPDATE_DELAY;
  @Getter private transient int lockedDelay = ASSIGNED_LOCKED_DELAY;
  private transient int missingTime;

  private int targetId = -1;
  private boolean foundTarget;
  private int targetKills;
  private List<RecentCombatant> recentKills = new ArrayList<>();
  @Getter private boolean minimised;
  private boolean targetIndicator;
  @Getter private boolean teleportUnlocked;
  @Getter @Setter private boolean teleportWarning;

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("bounty_hunter_target")) {
      return getTarget();
    }
    if (name.equals("bounty_hunter_target_kills")) {
      return targetKills;
    }
    return null;
  }

  @Override
  public void login() {
    setTeleportUnlocked(teleportUnlocked);
    player.getGameEncoder().setVarbit(VarbitId.BOUNTY_HUNTER_ROGUE_HUNTER_COUNTS, 1);
  }

  @Override
  public void tick() {
    if (!ENABLED) {
      return;
    }
    if (!player.getWorld().isPrimary()) {
      return;
    }
    if (lockedDelay > 0) {
      lockedDelay--;
    }
    if (teleportCooldown > 0) {
      teleportCooldown--;
    }
    if (!foundTarget && player.withinVisibilityDistance(getTarget())) {
      foundTarget = true;
      sendTarget();
    }
    checkWildernessState();
    checkTargetState();
    checkMissingTime();
    if (updateDelay > 0) {
      updateDelay--;
      if (updateDelay == 0 && inWilderness) {
        findTarget();
        sendTarget();
      }
    }
  }

  @Override
  public void playerKilled(Player player2) {
    if (!inWilderness || !player2.getArea().inWilderness()) {
      return;
    }
    if (getTarget() != player2) {
      return;
    }
    player
        .getGameEncoder()
        .sendMessage("<col=ff0000>You've killed your target: " + player2.getUsername() + "!</col>");
    player.getGameEncoder().sendMessage("Next target in: <col=ff0000>1 minute.</col>");
    isRecentTargetKill(player2, true);
    player2.getPlugin(BountyHunterPlugin.class).isRecentTargetKill(player, true);
    cancelTarget();
    targetKills++;
  }

  public void checkWildernessState() {
    var previouslyInWilderness = inWilderness;
    inWilderness = player.getArea().inWilderness();
    if (player.getArea().inPvpWorld()) {
      inWilderness = false;
      return;
    }
    if (previouslyInWilderness != inWilderness) {
      updateDelay = UPDATE_DELAY;
      player.getGameEncoder().sendHideWidget(WidgetId.WILDERNESS, 55, !inWilderness);
      if (targetId <= 0) {
        lockedDelay = Math.max(lockedDelay, WILD_STATE_LOCKED_DELAY);
      }
    }
    if (previouslyInWilderness != inWilderness && inWilderness) {
      sendTarget();
      if (player.getArea().inPvpWorld() || player.getHeight() != player.getClientHeight()) {
        player.getGameEncoder().sendHideWidget(WidgetId.WILDERNESS, 6, true);
      }
      setMinimised(minimised);
    }
  }

  public void checkTargetState() {
    if (targetId <= 0) {
      return;
    }
    var target = getTarget();
    if (target == null) {
      missingTime++;
      return;
    }
    var targetPlugin = target.getPlugin(BountyHunterPlugin.class);
    if (targetPlugin.getTarget() != player) {
      missingTime = MISSING_TIME;
      return;
    }
    if (target.getCombat().isDead() && target.getCombat().getRespawnDelay() == 1) {
      if (player != target.getCombat().getPlayerFromHitCount(false)) {
        cancelTarget();
        player
            .getGameEncoder()
            .sendMessage("Your target has died. You'll get a new one presently.");
        return;
      }
    }
    if (player.getSkills().getCombatLevel() - target.getSkills().getCombatLevel()
        > MAX_COMBAT_DIFFERENCE + 1) {
      cancelTarget();
      return;
    }
    if (!targetPlugin.isInWilderness()) {
      missingTime++;
    }
  }

  public void checkMissingTime() {
    if (missingTime < MISSING_TIME) {
      return;
    }
    var target = getTarget();
    if (target != null && target.getPlugin(BountyHunterPlugin.class).getTarget() == player) {
      player.getGameEncoder().sendMessage("<col=ff0000>You have abandoned your target.</col>");
    }
    cancelTarget();
    player
        .getGameEncoder()
        .sendMessage("Your target is no longer available, so you shall be assigned a new target.");
  }

  public void cancelTarget() {
    var target = getTarget();
    targetId = -1;
    targetRisk = null;
    targetEmblem = null;
    targetLevelRange = null;
    updateDelay = 0;
    missingTime = 0;
    lockedDelay = Math.max(lockedDelay, CANCELLED_LOCKED_DELAY);
    sendTarget();
    if (target != null && target.getPlugin(BountyHunterPlugin.class).getTarget() == player) {
      target.getPlugin(BountyHunterPlugin.class).cancelTarget();
    }
  }

  public void skipTarget() {
    if (targetId == -1) {
      player.getGameEncoder().sendMessage("You don't have a target.");
      return;
    }
    if (lockedDelay > 0) {
      player.getGameEncoder().sendMessage("You can't do this yet.");
      return;
    }
    if (player.getCombat().inRecentCombat()) {
      player.getGameEncoder().sendMessage("You can't do this right now.");
      return;
    }
    cancelTarget();
  }

  public void findTarget() {
    updateDelay = UPDATE_DELAY;
    if (targetId > 0) {
      return;
    }
    if (!inWilderness) {
      return;
    }
    if (player.getArea().inPvpWorld()) {
      return;
    }
    if (player.getHeight() != player.getClientHeight()) {
      return;
    }
    if (player.getController().isInstanced()) {
      return;
    }
    if (!hasEmblem()) {
      return;
    }
    if (lockedDelay > 0) {
      return;
    }
    var players = player.getWorld().getWildernessPlayers();
    Collections.shuffle(players);
    var hotspot = player.getWorld().getWorldEvent(WildernessHotspotEvent.class).inside(player);
    Player newTarget = null;
    Player hotspotTarget = null;
    for (var player2 : players) {
      if (player == player2) {
        continue;
      }
      var targetPlugin = player2.getPlugin(BountyHunterPlugin.class);
      if (player2.getArea().inPvpWorld()
          || player2.getHeight() != player2.getClientHeight()
          || player2.getController().isInstanced()) {
        continue;
      }
      if (targetPlugin.getTarget() != null) {
        continue;
      }
      if (isRecentTargetKill(player2, false) || targetPlugin.isRecentTargetKill(player, false)) {
        continue;
      }
      if (Math.abs(player.getSkills().getCombatLevel() - player2.getSkills().getCombatLevel())
          > MAX_COMBAT_DIFFERENCE) {
        continue;
      }
      var targetHotspot =
          player.getWorld().getWorldEvent(WildernessHotspotEvent.class).inside(player2);
      if (targetPlugin.getLockedDelay() > 0) {
        continue;
      }
      if (newTarget == null && (hotspot || !targetHotspot)) {
        newTarget = player2;
      }
      if (hotspot && targetHotspot) {
        hotspotTarget = player2;
        break;
      }
    }
    if (hotspotTarget != null) {
      newTarget = hotspotTarget;
    }
    if (newTarget != null && newTarget.isVisible()) {
      newTarget.getPlugin(BountyHunterPlugin.class).assignTarget(player);
      assignTarget(newTarget);
    }
  }

  public void assignTarget(Player opponent) {
    targetId = opponent != null ? opponent.getId() : -1;
    if (targetId <= 0) {
      return;
    }
    foundTarget = false;
    lockedDelay = ASSIGNED_LOCKED_DELAY;
    sendTarget();
    player.getGameEncoder().sendMessage("<col=ff0000>You've been assigned a target.");
  }

  public void sendTarget() {
    updateDelay = UPDATE_DELAY;
    targetRisk = null;
    targetEmblem = null;
    var target = getTarget();
    if (target != null) {
      targetRisk = RiskTier.getByValue(target.getCombat().getRiskedValue());
      for (var emblem : MysteriousEmblem.values()) {
        if (target.getInventory().hasItem(emblem.getItemId())) {
          targetEmblem = emblem;
        }
      }
      var min = Math.max(1, target.getArea().getWildernessLevel() - 1 - PRandom.randomI(2));
      var max = target.getArea().getWildernessLevel() + 1 + PRandom.randomI(2);
      if (target.getArea().getWildernessLevel() == 0) {
        min = max = 0;
      }
      var distance = player.getDistance(target);
      var color = "<col=1462ab>";
      if (distance < 20) {
        color = "<col=009900>";
      } else if (distance < 40) {
        color = "<col=93732a>";
      } else if (distance < 60) {
        color = "<col=620000>";
      } else if (distance < 80) {
        color = "<col=ad0400>";
      }
      targetLevelRange = min == 0 && max == 0 ? "Safe" : color + "Lvl " + min + "-" + max;
    }
    var targetName = "None";
    if (!hasEmblem()) {
      targetName = "<col=ff0000>Emblem Required</col>";
    }
    var targetInfo = "Level: -----";
    if (targetId <= 0) {
      if (targetIndicator) {
        player.getGameEncoder().sendHintIconReset();
      }
    } else {
      if (targetIndicator && target != null) {
        player.getGameEncoder().sendHintIconPlayer(target.getIndex());
      }
      targetName = "Offline";
      if (target != null) {
        targetName =
            (target.getCombat().getPKSkullDelay() > 0 ? "<img=9>" : "") + target.getUsername();
        if (!foundTarget) {
          targetName = "Unknown";
        }
        targetInfo = targetLevelRange + ", Cmb " + target.getSkills().getCombatLevel();
      }
    }
    player.getGameEncoder().sendWidgetText(WidgetId.WILDERNESS, 54, targetName);
    player.getGameEncoder().sendWidgetText(WidgetId.WILDERNESS, 55, targetInfo);
    player
        .getGameEncoder()
        .setVarbit(
            VarbitId.BOUNTY_HUNTER_WEALTH, targetRisk != null ? targetRisk.getVarbitValue() : 0);
    player
        .getGameEncoder()
        .setVarbit(
            VarbitId.BOUNTY_HUNTER_EMBLEM,
            targetEmblem != null ? targetEmblem.getVarbitValue() : 0);
  }

  public boolean isRecentTargetKill(Player opponent, boolean shouldUpdate) {
    if (!Settings.getInstance().isLocal() && player.getIP().equals(opponent.getIP())) {
      return true;
    }
    var minimumTime = 15;
    if (player.getArea().getWildernessLevel() <= 10
        && opponent.getArea().getWildernessLevel() <= 10) {
      minimumTime = 5;
    }
    for (var rc : recentKills) {
      if (opponent.getId() != rc.userId && !opponent.getIP().equals(rc.ip)) {
        continue;
      }
      var minutes = PTime.betweenMilliToMin(rc.time);
      if (minutes < minimumTime) {
        return true;
      }
      break;
    }
    if (shouldUpdate) {
      recentKills.removeIf(r -> r.userId == opponent.getId());
      while (recentKills.size() >= 8) {
        recentKills.remove(0);
      }
      recentKills.add(new RecentCombatant(opponent));
    }
    return false;
  }

  public boolean upgradeEmblem() {
    var values = MysteriousEmblem.values();
    for (var i = values.length - 1; i >= 0; i--) {
      var emblem = values[i];
      var slot = player.getInventory().getSlotById(emblem.getItemId());
      if (slot == -1) {
        continue;
      }
      var nextId = MysteriousEmblem.getNextId(emblem.getItemId());
      if (nextId == -1 || emblem.getItemId() == nextId) {
        continue;
      }
      player.getInventory().deleteItem(emblem.getItemId(), 1, slot);
      player.getInventory().addItem(nextId, 1, slot);
      return true;
    }
    return false;
  }

  public boolean hasEmblem() {
    return getEmblemId() != -1;
  }

  public int getEmblemId() {
    var values = MysteriousEmblem.values();
    for (var i = values.length - 1; i >= 0; i--) {
      var emblem = values[i];
      if (!player.getInventory().hasItem(emblem.getItemId())) {
        continue;
      }
      return emblem.getItemId();
    }
    return -1;
  }

  public void selectTeleportToTarget(boolean isTab) {
    if (!canTeleportToTarget(isTab)) {
      return;
    }
    if (teleportWarning) {
      var target = getTarget();
      var inMulti = target.getController().inMultiCombat();
      player.openDialogue(
          new OptionsDialogue(
              target.getUsername()
                  + ": "
                  + (inMulti ? "Multi-way" : "Single-way")
                  + ", Wilderness level "
                  + target.getArea().getWildernessLevel(),
              new DialogueOption(
                  "Teleport <col=ff0000>near</col> level "
                      + target.getArea().getWildernessLevel()
                      + " <col=ff0000>"
                      + (inMulti ? "multi-way" : "single-way")
                      + "</col> Wilderness.",
                  (c2, s2) -> teleportToTarget(isTab)),
              new DialogueOption("Cancel.")));
    } else {
      teleportToTarget(isTab);
    }
  }

  public void teleportToTarget(boolean isTab) {
    if (!canTeleportToTarget(isTab)) {
      return;
    }
    if (isTab) {
      player.getInventory().deleteItem(ItemId.BOUNTY_TARGET_TELEPORT);
    } else {
      if (player.getInventory().hasItem(ItemId.BLIGHTED_TELEPORT_SPELL_SACK)) {
        player.getInventory().deleteItem(ItemId.BLIGHTED_TELEPORT_SPELL_SACK);
      } else {
        player.getMagic().deleteRunes(SpellbookChild.TELEPORT_TO_BOUNTY_TARGET, TARGET_SPELL_RUNES);
      }
    }
    var target = getTarget();
    var tile = new Tile(target);
    for (var i = PRandom.randomI(4, 8); i > 0; i--) {
      if (target.getController().routeAllow(target.getX() - i, target.getY())) {
        tile.setTile(target.getX() - i, target.getY(), target.getHeight());
        break;
      }
      if (target.getController().routeAllow(target.getX() + i, target.getY())) {
        tile.setTile(target.getX() + i, target.getY(), target.getHeight());
        break;
      }
      if (target.getController().routeAllow(target.getX(), target.getY() - i)) {
        tile.setTile(target.getX(), target.getY() - i, target.getHeight());
        break;
      }
      if (target.getController().routeAllow(target.getX(), target.getY() + i)) {
        tile.setTile(target.getX(), target.getY() + i, target.getHeight());
        break;
      }
    }
    SpellTeleport.normalTeleport(player, tile);
    player.getController().stopWithTeleport();
    player.getCombat().clearHitEvents();
    teleportCooldown = 100;
    target.getGameEncoder().sendMessage("<col=ff0000>You feel a magical surge near you...</col>");
    target.getGameEncoder().sendMapGraphic(tile, new Graphic(455));
  }

  public boolean canTeleportToTarget(boolean isTab) {
    if (player.isLocked()) {
      return false;
    }
    if (targetId <= 0) {
      player.getGameEncoder().sendMessage("You don't have a target.");
      return false;
    }
    var target = getTarget();
    if (target == null || !target.isVisible()) {
      player.getGameEncoder().sendMessage("Unable to find your target.");
      return false;
    }
    if (!teleportUnlocked) {
      player.getGameEncoder().sendMessage("You need to unlock this spell first.");
      return false;
    }
    if (teleportCooldown > 0) {
      player.getGameEncoder().sendMessage("This spell is currently on cooldown.");
      return false;
    }
    if (player.getCombat().inRecentCombat()) {
      player.getGameEncoder().sendMessage("You can't use this right now.");
      return false;
    }
    if (!target.getArea().inWilderness()) {
      player.getGameEncoder().sendMessage("Your target isn't in the Wilderness.");
      return false;
    }
    if (player.getHeight() != target.getHeight()) {
      player.getGameEncoder().sendMessage("There was a problem locating your target.");
      return false;
    }
    if (player.getArea().inPvpWorld() || target.getArea().inPvpWorld()) {
      player.getGameEncoder().sendMessage("There was a problem locating your target.");
      return false;
    }
    if (isTab) {
      if (!player.getInventory().hasItem(ItemId.BOUNTY_TARGET_TELEPORT)) {
        return false;
      }
    } else {
      if (!player.getInventory().hasItem(ItemId.BLIGHTED_TELEPORT_SPELL_SACK)
          && !player
              .getMagic()
              .hasRunes(SpellbookChild.TELEPORT_TO_BOUNTY_TARGET, TARGET_SPELL_RUNES)) {
        player.getMagic().notEnoughRunes();
        return false;
      }
    }
    return player.getController().canTeleport(true);
  }

  public void setTeleportUnlocked(boolean teleportUnlocked) {
    this.teleportUnlocked = teleportUnlocked;
    player
        .getGameEncoder()
        .setVarbit(VarbitId.MAGIC_BOUNTY_TELEPORT_UNLOCKED, teleportUnlocked ? 1 : 0);
  }

  public void setMinimised(boolean minimised) {
    this.minimised = minimised;
    player.getGameEncoder().setVarbit(VarbitId.BOUNTY_HUNTER_MINIMISED, minimised ? 1 : 0);
  }

  public Player getTarget() {
    return player.getWorld().getPlayerById(targetId);
  }
}
