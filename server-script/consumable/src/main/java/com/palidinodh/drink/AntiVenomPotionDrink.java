package com.palidinodh.drink;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.consumable.ConsumableType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.ANTI_VENOM_1, ItemId.ANTI_VENOM_2, ItemId.ANTI_VENOM_3, ItemId.ANTI_VENOM_4})
class AntiVenomPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.action(
        (p, c) -> {
          p.getSkills().setAntivenomTime(1200);
          p.getSkills().setAntivenomPlusTime(100);
          p.setVenom(0);
          p.setPoison(0);
        });
    return builder;
  }
}

@ReferenceId({
  ItemId.ANTI_VENOM_PLUS_1,
  ItemId.ANTI_VENOM_PLUS_2,
  ItemId.ANTI_VENOM_PLUS_3,
  ItemId.ANTI_VENOM_PLUS_4
})
class AntiVenomPlusPotionDrink implements Consumable.BuildType {

  @Override
  public Consumable.ConsumableBuilder builder() {
    var builder = Consumable.builder();
    builder.type(ConsumableType.POTION);
    builder.action(
        (p, c) -> {
          p.getSkills().setAntivenomTime(1500);
          p.getSkills().setAntivenomPlusTime(300);
          p.setVenom(0);
          p.setPoison(0);
        });
    return builder;
  }
}
