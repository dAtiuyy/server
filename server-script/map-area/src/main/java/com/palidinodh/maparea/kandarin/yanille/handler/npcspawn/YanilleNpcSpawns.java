package com.palidinodh.maparea.kandarin.yanille.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class YanilleNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2676, 3088, 1), NpcId.HAZELMERE));
    spawns.add(new NpcSpawn(4, new Tile(2677, 3091), NpcId.JUNGLE_SPIDER_44));
    spawns.add(new NpcSpawn(4, new Tile(2678, 3096), NpcId.JUNGLE_SPIDER_44));
    spawns.add(new NpcSpawn(4, new Tile(2679, 3102), NpcId.JUNGLE_SPIDER_44));
    spawns.add(new NpcSpawn(4, new Tile(2676, 3111), NpcId.JUNGLE_SPIDER_44));
    spawns.add(new NpcSpawn(4, new Tile(2672, 3085), NpcId.JUNGLE_SPIDER_44));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2615, 3091), NpcId.BANKER_395));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2615, 3092), NpcId.BANKER));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2615, 3094), NpcId.BANKER));
    spawns.add(new NpcSpawn(4, new Tile(2497, 3088), NpcId.OGRE_53));
    spawns.add(new NpcSpawn(4, new Tile(2496, 3092), NpcId.OGRE_53));
    spawns.add(new NpcSpawn(4, new Tile(2500, 3094), NpcId.OGRE_53));
    spawns.add(new NpcSpawn(4, new Tile(2499, 3089), NpcId.OGRE_53));
    spawns.add(new NpcSpawn(4, new Tile(2511, 3089), NpcId.OGRE_53));
    spawns.add(new NpcSpawn(4, new Tile(2511, 3089), NpcId.GREW));
    spawns.add(new NpcSpawn(4, new Tile(2512, 3086), NpcId.OGRE_53));
    spawns.add(new NpcSpawn(4, new Tile(2514, 3081), NpcId.OGRE_53));
    spawns.add(new NpcSpawn(4, new Tile(2511, 3080), NpcId.OGRE_53));
    spawns.add(new NpcSpawn(4, new Tile(2537, 3091), NpcId.TOWER_GUARD_28));
    spawns.add(new NpcSpawn(4, new Tile(2548, 3114), NpcId.TOWER_GUARD_28));
    spawns.add(new NpcSpawn(4, new Tile(2559, 3115), NpcId.TOWER_GUARD_28));
    spawns.add(new NpcSpawn(4, new Tile(2544, 3115), NpcId.TOWER_GUARD_28));
    spawns.add(new NpcSpawn(4, new Tile(2549, 3121), NpcId.TOWER_GUARD_28));
    spawns.add(new NpcSpawn(4, new Tile(2541, 3096), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2542, 3102), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2546, 3087), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2543, 3087), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2545, 3091), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2550, 3091), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2551, 3077), NpcId.GUARD_CAPTAIN));
    spawns.add(new NpcSpawn(4, new Tile(2554, 3077), NpcId.BARTENDER_1320));
    spawns.add(new NpcSpawn(4, new Tile(2551, 3099), NpcId.BERT));
    spawns.add(new NpcSpawn(4, new Tile(2606, 3101), NpcId.TRAMP));
    spawns.add(new NpcSpawn(4, new Tile(2546, 3116, 1), NpcId.WATCHMAN_33));
    spawns.add(new NpcSpawn(4, new Tile(2545, 3112, 1), NpcId.WATCHMAN_33));
    spawns.add(new NpcSpawn(4, new Tile(2549, 3116, 1), NpcId.WATCHMAN_33));
    spawns.add(new NpcSpawn(4, new Tile(2547, 3112, 1), NpcId.WATCHMAN_33));
    spawns.add(new NpcSpawn(4, new Tile(2547, 3112, 2), NpcId.WATCHTOWER_WIZARD));
    spawns.add(new NpcSpawn(4, new Tile(2537, 3101, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2534, 3100, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2534, 3095, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2534, 3091, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2534, 3087, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2535, 3083, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2538, 3080, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2542, 3074, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2554, 3073, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2563, 3073, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2593, 3074, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2606, 3074, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2618, 3074, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2621, 3083, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2620, 3093, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2617, 3101, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2610, 3108, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2592, 3109, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2568, 3109, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2549, 3109, 1), NpcId.SOLDIER_28));
    spawns.add(new NpcSpawn(4, new Tile(2583, 3119), NpcId.COW_2));
    spawns.add(new NpcSpawn(4, new Tile(2588, 3120), NpcId.COW_2));
    spawns.add(new NpcSpawn(4, new Tile(2585, 3126), NpcId.COW_2));
    spawns.add(new NpcSpawn(4, new Tile(2580, 3124), NpcId.COW_2));
    spawns.add(new NpcSpawn(4, new Tile(2594, 3089), NpcId.WIZARD_DISTENTOR));
    spawns.add(new NpcSpawn(4, new Tile(2589, 3085), NpcId.WIZARD_9));
    spawns.add(new NpcSpawn(4, new Tile(2589, 3092), NpcId.WIZARD_9));
    spawns.add(new NpcSpawn(4, new Tile(2590, 3086), NpcId.WIZARD_9));
    spawns.add(new NpcSpawn(4, new Tile(2593, 3091, 1), NpcId.WIZARD_9));
    spawns.add(new NpcSpawn(4, new Tile(2593, 3084, 1), NpcId.WIZARD_9));
    spawns.add(new NpcSpawn(4, new Tile(2587, 3090, 1), NpcId.WIZARD_9));
    spawns.add(new NpcSpawn(4, new Tile(2587, 3085, 1), NpcId.WIZARD_9));
    spawns.add(new NpcSpawn(4, new Tile(2590, 3092, 2), NpcId.WIZARD_9));
    spawns.add(new NpcSpawn(4, new Tile(2588, 3086, 2), NpcId.WIZARD_9));
    spawns.add(new NpcSpawn(4, new Tile(2594, 3087, 2), NpcId.WIZARD_9));
    spawns.add(new NpcSpawn(4, new Tile(2588, 3086), NpcId.ZAVISTIC_RARVE));
    spawns.add(new NpcSpawn(4, new Tile(2596, 3105), NpcId.MAN_2_3106));
    spawns.add(new NpcSpawn(new Tile(2592, 3104), NpcId.MAN_2_3108));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(2571, 3106), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(4, new Tile(2565, 3097), NpcId.FRENITA));
    spawns.add(new NpcSpawn(4, new Tile(2566, 3083), NpcId.LEON));
    spawns.add(new NpcSpawn(4, new Tile(2566, 3081), NpcId.ALECK));
    spawns.add(new NpcSpawn(4, new Tile(2537, 3092), NpcId.COLONEL_RADICK_38));

    return spawns;
  }
}
