package com.palidinodh.rs.communication.response;

import com.palidinodh.rs.communication.request.Request;

public class VoteResponse extends Response {

  private String text;

  public VoteResponse(Request request, String text) {
    super(request);
    this.text = text;
  }

  public String getText() {
    return text;
  }
}
