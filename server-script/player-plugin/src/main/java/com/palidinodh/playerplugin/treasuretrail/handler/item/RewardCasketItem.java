package com.palidinodh.playerplugin.treasuretrail.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.playerplugin.treasuretrail.TreasureTrailPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.REWARD_CASKET_EASY,
  ItemId.REWARD_CASKET_MEDIUM,
  ItemId.REWARD_CASKET_HARD,
  ItemId.REWARD_CASKET_ELITE,
  ItemId.REWARD_CASKET_MASTER
})
class RewardCasketItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var type = ClueScrollType.getById(item.getId());
    if (type == null) {
      return;
    }
    player.getPlugin(TreasureTrailPlugin.class).openCasket(type);
  }
}
