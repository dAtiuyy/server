package com.palidinodh.command.seniormod;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.rs.setting.DiscordChannel;

@ReferenceName("droprate")
class DropRateCommand implements CommandHandler, CommandHandler.SeniorModeratorRank {

  @Override
  public String getExample(String name) {
    return "percent";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var amount = Integer.parseInt(message.replace("%", ""));
    if (amount < 0 || amount > 100) {
      return;
    }
    if (amount == 0) {
      PCombat.dropRateMultiplier = 1.0;
    } else {
      PCombat.dropRateMultiplier = 1.0 + (amount / 100.0);
    }
    player
        .getGameEncoder()
        .sendMessage(
            "<col=ff0000> You have set the drop rate multiplier to "
                + PCombat.dropRateMultiplier
                + "!");
    DiscordBot.sendMessage(
        DiscordChannel.MODERATION_LOG,
        player.getUsername()
            + " has set the drop rate multiplier to "
            + PCombat.dropRateMultiplier
            + ".");
  }
}
