package com.palidinodh.maparea.asgarnia.donator.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.setting.UserRank;

@ReferenceId(ObjectId.ENERGY_BARRIER_4470)
@SuppressWarnings("unused")
class EntranceMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (!player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
      player.getGameEncoder().sendMessage("Only donators may enter.");
      return;
    }
    player.getMovement().clear();
    if (mapObject.getX() == 3070
        && (mapObject.getY() == 3502 || mapObject.getY() == 3503 || mapObject.getY() == 3504)) {
      if (player.getX() >= 3070) {
        player.getMovement().addMovement(3069, 3503);
      } else {
        player.getMovement().addMovement(3070, 3503);
      }
    } else if ((mapObject.getX() >= 3051 && mapObject.getX() <= 3055) && mapObject.getY() == 3515) {
      if (player.getY() >= 3516) {
        player.getMovement().addMovement(3053, 3515);
      } else {
        player.getMovement().addMovement(3053, 3516);
      }
    }
  }
}
