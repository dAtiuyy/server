package com.palidinodh.playerplugin.slayer.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.WILDERNESS_BOSS_TASKS_32336)
class WildernessBossTasksItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    if (plugin.getRewards().isUnlocked(SlayerUnlock.WILDERNESS_BOSS)) {
      plugin.getRewards().lock(SlayerUnlock.WILDERNESS_BOSS);
      player.getGameEncoder().sendMessage("You can now be assigned wilderness boss tasks.");
    } else {
      plugin.getRewards().unlock(SlayerUnlock.WILDERNESS_BOSS);
      player.getGameEncoder().sendMessage("You can no longer be assigned wilderness boss tasks.");
    }
  }
}
