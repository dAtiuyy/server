package com.palidinodh.playerplugin.teleports;

import com.google.inject.Inject;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

public class TeleportsPlugin implements PlayerPlugin {

  @Inject private transient Player player;

  @Getter private MainTeleportsSubPlugin mainTeleports = new MainTeleportsSubPlugin();
  @Getter private List<HotAirBalloonType> hotAirBalloons = new ArrayList<>();

  @Override
  public void login() {
    if (!hotAirBalloons.contains(HotAirBalloonType.ENTRANA)) {
      hotAirBalloons.add(HotAirBalloonType.ENTRANA);
    }
    for (var type : hotAirBalloons) {
      player.getGameEncoder().setVarbit(type.getVarbit(), 1);
    }
  }

  public void sendHotAirBalloonTransport(Tile mapObjectTile) {
    var type = HotAirBalloonType.getByMapObjectTile(mapObjectTile);
    if (type == null) {
      return;
    }
    if (!hotAirBalloons.contains(type)) {
      return;
    }
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.HOT_AIR_BALLOON_TRANSPORT);
  }
}
