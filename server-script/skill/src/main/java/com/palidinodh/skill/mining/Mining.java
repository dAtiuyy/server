package com.palidinodh.skill.mining;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import com.palidinodh.util.PNumber;
import java.util.List;

class Mining extends SkillContainer {

  public static final int UNIDENTIFIED_MINERAL_VARIABLE = 0;
  private static final MiningPickaxe[] PICKAXES = {
    new MiningPickaxe(ItemId.BRONZE_PICKAXE, 1, 625, 6753),
    new MiningPickaxe(ItemId.IRON_PICKAXE, 1, 626, 6754),
    new MiningPickaxe(ItemId.STEEL_PICKAXE, 6, 627, 6755),
    new MiningPickaxe(ItemId.BLACK_PICKAXE, 11, 625, 6753),
    new MiningPickaxe(ItemId.MITHRIL_PICKAXE, 21, 629, 6757),
    new MiningPickaxe(ItemId.ADAMANT_PICKAXE, 31, 628, 6756),
    new MiningPickaxe(ItemId.RUNE_PICKAXE, 41, 624, 6752),
    new MiningPickaxe(ItemId.GILDED_PICKAXE, 41, 8314, 8312),
    new MiningPickaxe(ItemId.DRAGON_PICKAXE, 61, 7139, 6758),
    new MiningPickaxe(ItemId.DRAGON_PICKAXE_12797, 61, 642, 335),
    new MiningPickaxe(ItemId.DRAGON_PICKAXE_OR, 61, 7139, 6758),
    new MiningPickaxe(ItemId._3RD_AGE_PICKAXE, 61, 7283, 7282),
    new MiningPickaxe(ItemId.INFERNAL_PICKAXE, 61, 4482, 4481),
    new MiningPickaxe(ItemId.INFERNAL_PICKAXE_UNCHARGED, 61, 4482, 4481)
  };

  private static boolean inMembersMiningGuild(Player player) {
    return player.within(3009, 9696, 3058, 9729);
  }

  private static MiningPickaxe getPickaxe(Player player) {
    for (var i = PICKAXES.length - 1; i >= 0; i--) {
      var pickaxe = PICKAXES[i];
      if (!player.carryingItem(pickaxe.getItemId())) {
        continue;
      }
      if (player.getSkills().getLevel(Skills.MINING) < pickaxe.getLevel()) {
        continue;
      }
      return pickaxe;
    }
    return null;
  }

  private static boolean canUseAccomplishmentCapeResourceBoost(Player player, SkillEntry entry) {
    if (entry.getCreate() == null) {
      return false;
    }
    if (entry.getCreate().getId() == ItemId.RUNITE_ORE) {
      return false;
    }
    if (entry.getCreate().getId() == ItemId.AMETHYST) {
      return false;
    }
    return player.getEquipment().wearingAccomplishmentCape(Skills.MINING);
  }

  private static int getVolcanicAshAmount(Player player, Item item) {
    int level = player.getSkills().getLevel(Skills.MINING);
    if (level >= 97) {
      return item.getAmount() * 6;
    }
    if (level >= 82) {
      return item.getAmount() * 5;
    }
    if (level >= 67) {
      return item.getAmount() * 4;
    }
    if (level >= 52) {
      return item.getAmount() * 3;
    }
    if (level >= 37) {
      return item.getAmount() * 2;
    }
    return item.getAmount();
  }

  private static int getRandomGemId(Player player) {
    if (PRandom.inRange(player.getCombat().getDropRate(0.0025, ItemId.ZENYTE_SHARD))) {
      return ItemId.ZENYTE_SHARD;
    }
    if (PRandom.inRange(player.getCombat().getDropRate(0.005, ItemId.UNCUT_ONYX))) {
      return ItemId.UNCUT_ONYX;
    }
    if (PRandom.inRange(player.getCombat().getDropRate(0.01, ItemId.UNCUT_DRAGONSTONE))) {
      return ItemId.UNCUT_DRAGONSTONE;
    }
    if (PRandom.randomE(128) < 4) {
      return ItemId.UNCUT_DIAMOND;
    }
    if (PRandom.randomE(128) < 5) {
      return ItemId.UNCUT_RUBY;
    }
    if (PRandom.randomE(128) < 5) {
      return ItemId.UNCUT_EMERALD;
    }
    if (PRandom.randomE(128) < 9) {
      return ItemId.UNCUT_SAPPHIRE;
    }
    if (PRandom.randomE(128) < 15) {
      return ItemId.UNCUT_RED_TOPAZ;
    }
    if (PRandom.randomE(128) < 30) {
      return ItemId.UNCUT_JADE;
    }
    return ItemId.UNCUT_OPAL;
  }

  @Override
  public int getSkillId() {
    return Skills.MINING;
  }

  @Override
  public List<SkillEntry> getEntries() {
    return MiningEntries.getEntries();
  }

  @Override
  public int getEventTick(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null && npc.getId() == NpcId.AFK_ZALCANO_16076) {
      return 4;
    }
    return 2;
  }

  @Override
  public void eventStarted(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    player.getGameEncoder().sendMessage("You swing your pickaxe at the rock.");
  }

  @Override
  public void eventStopped(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    player.setAnimation(-1);
  }

  @Override
  public void actionSuccess(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    double moreResourcesChance = 0;
    if (canUseAccomplishmentCapeResourceBoost(player, entry)) {
      moreResourcesChance = PNumber.addDoubles(moreResourcesChance, 5);
    }
    if (entry.getCreate() != null
        && (player.getEquipment().getHandId() == ItemId.MINING_GLOVES
            || player.getEquipment().getHandId() == ItemId.EXPERT_MINING_GLOVES)) {
      if (entry.getCreate().getId() == ItemId.SILVER_ORE) {
        moreResourcesChance = PNumber.addDoubles(moreResourcesChance, 50);
      } else if (entry.getCreate().getId() == ItemId.COAL) {
        moreResourcesChance = PNumber.addDoubles(moreResourcesChance, 40);
      } else if (entry.getCreate().getId() == ItemId.GOLD_ORE) {
        moreResourcesChance = PNumber.addDoubles(moreResourcesChance, 33.33);
      }
    }
    if (entry.getCreate() != null
        && (player.getEquipment().getHandId() == ItemId.SUPERIOR_MINING_GLOVES
            || player.getEquipment().getHandId() == ItemId.EXPERT_MINING_GLOVES)) {
      if (entry.getCreate().getId() == ItemId.MITHRIL_ORE) {
        moreResourcesChance = PNumber.addDoubles(moreResourcesChance, 25);
      } else if (entry.getCreate().getId() == ItemId.ADAMANTITE_ORE) {
        moreResourcesChance = PNumber.addDoubles(moreResourcesChance, 16.66);
      } else if (entry.getCreate().getId() == ItemId.RUNITE_ORE) {
        moreResourcesChance = PNumber.addDoubles(moreResourcesChance, 12.5);
      }
    }
    if (moreResourcesChance <= 0 || !PRandom.inRange(moreResourcesChance)) {
      setTemporaryMapObject(player, mapObject, entry);
    }
    if (npc != null && npc.getId() == NpcId.AFK_ZALCANO_16076) {
      player.getSkills().addXp(Skills.SMITHING, entry.getExperience());
    }
  }

  @Override
  public void clueRolled(Player player, Npc npc, MapObject mapObject, SkillEntry entry) {
    var items =
        RandomItem.buildList(
            new RandomItem(ClueScrollType.EASY.getGeodeId()).weight(4),
            new RandomItem(ClueScrollType.MEDIUM.getGeodeId()).weight(3),
            new RandomItem(ClueScrollType.HARD.getGeodeId()).weight(2),
            new RandomItem(ClueScrollType.ELITE.getGeodeId()).weight(1));
    var clueItem = RandomItem.getItem(items);
    player.getInventory().addOrDropItem(clueItem);
  }

  @Override
  public Item createHook(
      Player player, PEvent event, Item item, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (entry.getCreate() != null && entry.getCreate().getId() == ItemId.UNCUT_OPAL) {
      return new Item(getRandomGemId(player), item.getAmount());
    }
    if (entry.getCreate() != null && entry.getCreate().getId() == ItemId.VOLCANIC_ASH) {
      var ash = PRandom.randomE(32) == 0 ? ItemId.SODA_ASH : ItemId.VOLCANIC_ASH;
      if (ash == ItemId.SODA_ASH) {
        return new Item(ItemId.SODA_ASH, item.getAmount());
      }
      return new Item(ash, getVolcanicAshAmount(player, item));
    }
    var freeSlots = player.getWidgetManager().getCoalBag().getExactCount(ItemId.COAL) < 27;
    if (entry.getCreate() != null
        && entry.getCreate().getId() == ItemId.COAL
        && player.getInventory().hasItem(ItemId.OPENED_COAL_BAG)
        && freeSlots) {
      player.getWidgetManager().getCoalBag().addItem(ItemId.COAL, 1);
      var coalAmount = player.getWidgetManager().getCoalBag().getExactCount(ItemId.COAL);
      player
          .getGameEncoder()
          .sendMessage("The coal bag contains " + coalAmount + " pieces of coal.");
      return null;
    }
    MiningPickaxe pickaxe = getPickaxe(player);
    int smithingXp = 0;
    if (entry.getCreate() != null
        && pickaxe.getItemId() == ItemId.INFERNAL_PICKAXE
        && PRandom.randomE(3) == 0) {
      switch (entry.getCreate().getId()) {
        case ItemId.GOLD_ORE:
          smithingXp = 9;
          break;
        case ItemId.SANDSTONE_1KG:
        case ItemId.SANDSTONE_2KG:
        case ItemId.SANDSTONE_5KG:
        case ItemId.SANDSTONE_10KG:
        case ItemId.GRANITE_500G:
        case ItemId.GRANITE_2KG:
        case ItemId.GRANITE_5KG:
          smithingXp = 6 + PRandom.randomI(15);
          break;
        case ItemId.COPPER_ORE:
        case ItemId.TIN_ORE:
        case ItemId.BLURITE_ORE:
          smithingXp = 4;
          break;
        case ItemId.IRON_ORE:
          smithingXp = 6;
          break;
        case ItemId.SILVER_ORE:
          smithingXp = 8;
          break;
        case ItemId.MITHRIL_ORE:
          smithingXp = 12;
          break;
        case ItemId.ADAMANTITE_ORE:
          smithingXp = 18;
          break;
        case ItemId.RUNITE_ORE:
          smithingXp = 25;
          break;
      }
    }
    if (smithingXp != 0) {
      player.setGraphic(86, 100);
      player.getSkills().addXp(Skills.SMITHING, smithingXp);
      player.getCharges().degradeItem(pickaxe.getItemId());
      return null;
    }
    if (entry.getVariable(UNIDENTIFIED_MINERAL_VARIABLE) > 0
        && PRandom.randomE(entry.getVariable(UNIDENTIFIED_MINERAL_VARIABLE)) == 0) {
      player.getInventory().addOrDropItem(ItemId.UNIDENTIFIED_MINERALS, 1);
      player
          .getGameEncoder()
          .sendMessage(
              "<col=ff0000>The skilling seller might be interested in these unidentified minerals.</col>");
    }
    if (player.getPlugin(BondPlugin.class).isRelicUnlocked(BondRelicType.BIGGER_HARVEST_MINING)
        && PRandom.randomE(2) == 0) {
      item = new Item(item.getId(), item.getAmount() * 2);
    }
    return item;
  }

  @Override
  public int experienceHook(
      Player player, PEvent event, int experience, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (player.getEquipment().wearingProspectorOutfit()) {
      experience *= 1.1;
    }
    return experience;
  }

  @Override
  public int animationHook(
      Player player, int animation, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (entry.getCreate() != null && entry.getCreate().getId() == ItemId.AMETHYST) {
      return getPickaxe(player).getWallAnimation();
    }
    return getPickaxe(player).getAnimation();
  }

  @Override
  public boolean canDoActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (getPickaxe(player) == null) {
      player.getGameEncoder().sendMessage("You need a pickaxe to do this.");
      return false;
    }
    if (npc != null && npc.getId() == NpcId.AFK_ZALCANO_16076) {
      if (player.hasIPMatch()) {
        player.getGameEncoder().sendMessage("This feature is disabled with multi-logging.");
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean skipActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (npc != null && npc.getId() == NpcId.AFK_ZALCANO_16076) {
      return PRandom.randomE(8) != 0;
    }
    var power =
        (player.getSkills().getLevel(getSkillId()) / 2) + (getPickaxe(player).getLevel() / 2) + 8;
    var failure = entry.getLevel() + 2;
    if (player.inWildernessResourceArea()) {
      failure /= 2;
    }
    if (inMembersMiningGuild(player)) {
      power += 7;
    }
    var chance = 0;
    if (power > failure) {
      chance = (int) ((1 - (failure + 2) / ((power + 1) * 2.0)) * 100);
    } else {
      chance = (int) ((power / ((failure + 1) * 2.0)) * 100);
    }
    chance *= 1 + getMembershipBoost(player);
    if (player.getEquipment().wearingProspectorOutfit()) {
      chance += 10;
    }
    chance = Math.min(chance, 100);
    return PRandom.randomE(100) > chance;
  }

  @Override
  public int clueChanceHook(
      Player player, int chance, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (player.getEquipment().wearingProspectorOutfit()) {
      return (int) (chance / 1.1);
    }
    return chance;
  }
}
