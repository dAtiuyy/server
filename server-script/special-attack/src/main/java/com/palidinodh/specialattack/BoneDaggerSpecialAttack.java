package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.BONE_DAGGER,
  ItemId.BONE_DAGGER_P,
  ItemId.BONE_DAGGER_P_PLUS,
  ItemId.BONE_DAGGER_P_PLUS_PLUS
})
class BoneDaggerSpecialAttack extends SpecialAttack {

  BoneDaggerSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(75);
    entry.animation(4198);
    entry.castGraphic(new Graphic(704));
    addEntry(entry);
  }

  @Override
  public void applyAttackHook(ApplyAttackHooks hooks) {
    hooks.getOpponent().getCombat().changeDefenceLevel(-hooks.getDamage());
  }
}
