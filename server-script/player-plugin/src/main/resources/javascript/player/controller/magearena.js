var ARENA_2_MONSTERS = [
    [ 7860 ],
    [ 7858 ],
    [ 7859 ],
    [ 7860, 7858, 7859 ]
];
var ARENA_2_SPAWNS = [
    [ 3098, 3925 ],
    [ 3112, 3938 ],
    [ 3108, 3925 ]
];

var player;
var monsters = new ArrayList();
var spawnDelay = 8;
var paused = false;
var arena2WaveId = 1;
var spawnIndex = PRandom.randomE(ARENA_2_SPAWNS.length);
var time = 0;

pc = new PController() {
    /* @Override */
    startHook: function() {
        player = pc.getTile();
        if (player == null) {
            return;
        }
        pc.setExitTile(new Tile(2539, 4715));
        pc.setKeepItemsOnDeath(true);
        pc.setTeleportsDisabled(true);
        pc.setMultiCombatFlag(true);
        player.setLargeVisibility();
        pc.startInstance();
        pc.setDisableMonsterDrops(true);
    },

    /* @Override */
    stopHook: function() {
        player.restore();
        player.getWorld().removeNpcs(monsters);
        player.getGameEncoder().sendPlayerOption("null", 2, false);
        player = null;
    },

    /* @Override */
    script: function(name, args) {
        if (name.equals("arena2_wave_id")) {
            arena2WaveId = args[0];
        }
    },

    /* @Override */
    loadHook: function(map) {
        arena2WaveId = map.get("controller.arena2WaveId");
        spawnIndex = map.get("controller.spawnIndex");
        time = map.get("controller.time");
    },

    /* @Override */
    saveHook: function(map) {
        if (map != null) {
            map.put("controller.arena2WaveId", arena2WaveId);
            map.put("controller.spawnIndex", spawnIndex);
            map.put("controller.time", time);
        }
        return true;
    },

    /* @Override */
    instanceChangedHook: function(controller) {
        if (!controller.isInstanced()) {
            return;
        }
        pc.addMapObject(new MapObject(-1, 10, 0, new Tile(3097, 3938, 0))); // Skeleton
        pc.addMapObject(new MapObject(-1, 10, 0, new Tile(3104, 3938, 0))); // Skeleton
        pc.addMapObject(new MapObject(-1, 10, 0, new Tile(3111, 3938, 0))); // Skeleton
        pc.addMapObject(new MapObject(-1, 10, 0, new Tile(3099, 3931, 0))); // Skeleton
        pc.addMapObject(new MapObject(-1, 10, 0, new Tile(3108, 3931, 0))); // Skeleton
        pc.addMapObject(new MapObject(-1, 10, 0, new Tile(3105, 3925, 0))); // Skeleton
        pc.addMapObject(new MapObject(-1, 10, 0, new Tile(3110, 3927, 0))); // Skeleton
        pc.addMapObject(new MapObject(12351, 10, 2, new Tile(3092, 3934, 0))); // Barrier
        pc.addMapObject(new MapObject(12351, 10, 2, new Tile(3092, 3933, 0))); // Barrier
    },

    /* @Override */
    tickHook: function() {
        if (!monsters.isEmpty()) {
            for (var it = monsters.iterator(); it.hasNext();) {
                var npc = it.next();
                if (npc.isVisible() || !npc.getCombat().isDead()) {
                    continue;
                }
                it.remove();
            }
            if (monsters.isEmpty() && arena2WaveId > 0) {
                player.getGameEncoder().sendMessage("Wave completed!");
                spawnDelay = 4;
                if (arena2WaveId++ == ARENA_2_MONSTERS.length) {
                    this.finishArena2();
                }
            }
        }
        if (player.getCombat().isDead()) {
            return;
        }
        if (spawnDelay > 0) {
            if (!paused && --spawnDelay == 0) {
                player.getGameEncoder().sendMessage("<col=ff0000>Wave: " + arena2WaveId);
                this.spawnArena2Monsters();
            }
            return;
        }
        time++;
    },

    /* @Override */
    applyDeadHook: function() {
        pc.stopWithTeleport();
    },

    /* @Override */
    mapObjectOptionHook: function(index, mapObject) {
        switch (mapObject.getId()) {
        case 12351: // Barrier
            pc.exitTileTeleport();
            return true;
        }
        return false;
    },

    /* @Override */
    logoutWidgetHook: function() {
        if (paused) {
            return;
        }
        paused = true;
        if (spawnDelay > 0) {
            player.getGameEncoder().sendMessage("<col=ff0000>The Mage Arena has been paused.");
        } else {
            player.getGameEncoder().sendMessage("<col=ff0000>The Mage Arena will be paused after this wave.");
        }
    },

    /* @Override */
    inWilderness: function() {
        return false;
    },

    finishArena2: function() {
        pc.exitTileTeleport();
        player.getGameEncoder().sendMessage("Duration: <col=ff0000>" + PTime.ticksToDuration(time));
        if (!player.getInventory().addItem(ItemId.IMBUED_SARADOMIN_CAPE, 1).success()) {
            player.getBank().add(new Item(ItemId.IMBUED_SARADOMIN_CAPE, 1));
        }
        if (!player.getInventory().addItem(ItemId.IMBUED_GUTHIX_CAPE, 1).success()) {
            player.getBank().add(new Item(ItemId.IMBUED_GUTHIX_CAPE, 1));
        }
        if (!player.getInventory().addItem(ItemId.IMBUED_ZAMORAK_CAPE, 1).success()) {
            player.getBank().add(new Item(ItemId.IMBUED_ZAMORAK_CAPE, 1));
        }
        player.getCombat().setMageArena2(true);
    },

    spawnArena2Monsters: function() {
        var npcIds = ARENA_2_MONSTERS[arena2WaveId - 1];
        for (var i = 0; i < npcIds.length; i++) {
            var coords = this.getArena2Coords(npcIds[i]);
            var npc = player.getController().addNpc(new NpcSpawn(new Tile(coords[0], coords[1], player.getHeight()), npcIds[i]));
            npc.setLargeVisibility();
            npc.getMovement().setClipNpcs(true);
            npc.getController().setMultiCombatFlag(true);
            npc.getController().setDisableMonsterDrops(true);
            npc.getCombat().setTarget(player);
            monsters.add(npc);
        }
    },

    getArena2Coords: function(npcId) {
        spawnIndex = (spawnIndex + 1) % ARENA_2_SPAWNS.length;
        return ARENA_2_SPAWNS[spawnIndex];
    }
};
