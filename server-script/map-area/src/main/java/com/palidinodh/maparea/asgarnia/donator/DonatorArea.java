package com.palidinodh.maparea.asgarnia.donator;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12086)
public class DonatorArea extends Area {

  @Override
  public boolean inLoadoutZone() {
    if (getTile().getX() >= 3042
        && getTile().getY() >= 3502
        && getTile().getX() <= 3065
        && getTile().getY() <= 3514) {
      // Center
      return true;
    }
    if (getTile().getX() >= 3032
        && getTile().getY() >= 3490
        && getTile().getX() <= 3034
        && getTile().getY() <= 3495) {
      // Runecrafting bank
      return true;
    }
    if (getTile().getX() >= 3022
        && getTile().getY() >= 3495
        && getTile().getX() <= 3030
        && getTile().getY() <= 3517) {
      // High tier zone
      return true;
    }
    if (getTile().getX() >= 3013
        && getTile().getY() >= 3510
        && getTile().getX() <= 3022
        && getTile().getY() <= 3517) {
      // High tier zone
      return true;
    }
    if (getTile().getX() >= 3031
        && getTile().getY() >= 3498
        && getTile().getX() <= 3035
        && getTile().getY() <= 3508) {
      // High tier zone
      return true;
    }
    if (getTile().getX() >= 3031
        && getTile().getY() >= 3509
        && getTile().getX() <= 3037
        && getTile().getY() <= 3517) {
      // High tier zone
      return true;
    }
    // High tier zone
    return getTile().getX() >= 3036
        && getTile().getY() >= 3502
        && getTile().getX() <= 3040
        && getTile().getY() <= 3504;
  }
}
