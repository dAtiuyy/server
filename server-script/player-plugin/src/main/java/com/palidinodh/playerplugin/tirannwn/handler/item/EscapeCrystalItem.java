package com.palidinodh.playerplugin.tirannwn.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.boss.BossInstanceController;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.ESCAPE_CRYSTAL, ItemId.CORRUPTED_ESCAPE_CRYSTAL})
class EscapeCrystalItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    if (!player.getController().is(BossInstanceController.class)) {
      return;
    }
    player.rejuvenate();
    player.getController().stopWithTeleport();
    SpellTeleport.teleport(
        player, SpellTeleport.TeleportStyle.TABLET, player.getController().getExitTile());
    player.log(PlayerLogType.TELEPORT, "teleported using " + item.getDef().getName());
  }
}
