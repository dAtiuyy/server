var entries = new ArrayList();
var title = "";
var lines = new ArrayList();
var continueLine = "";
var actions = new ArrayList();

title = "Select an Option";
lines.add("Mage Arena 1");
actions.add("close|script");
lines.add("Mage Arena 2");
actions.add("dialogue=kolodion,1");
lines.add("View Shop");
actions.add("close|script");
var obj0 = new DialogueEntry();
entries.add(obj0);
obj0.setSelection(title, PString.toStringArray(lines, true), PString.toStringArray(actions, true));

lines.add("<col=ff0000>WARNING</col>: This minigame requires all three god spells to complete. Make sure you have the correct runes, spells, and staves before you continue.");
continueLine = "Click here to continue";
actions.add("close|script");
var obj1 = new DialogueEntry();
entries.add(obj1);
obj1.setTextContinue(continueLine, PString.toStringArray(lines, true), PString.toStringArray(actions, true));

instance = new DialogueScript() {
    execute: function(player, index, childId, slot) {
        if (index == 0) {
            if (slot == 0) {
                if (player.getCombat().getMageArena()) {
                    player.getGameEncoder().sendMessage("You've already completed this.");
                    return;
                }
                var height = player.getWorld().getUniqueHeight();
                player.getMovement().teleport(3105, 3934, height);
                var event = new PEvent(5) {
                    execute: function() {
                        event.stop();
                        if (!player.isVisible()) {
                            return;
                        }
                        var kolodion = player.getController().addNpc(new NpcSpawn(new Tile(3106, 3934, height), NpcId.KOLODION_1605));
                        kolodion.getCombat().setTarget(player);
                    }
                };
                player.getWorld().addEvent(event);
            } else if (slot == 2) {
                if (!player.getCombat().getMageArena()) {
                    player.getGameEncoder().sendMessage("You can't view this shop.");
                    return;
                }
                player.openShop("kolodion");
            }
        } else if (index == 1) {
            if (!player.getCombat().getMageArena()) {
                player.getGameEncoder().sendMessage("You need to complete the Mage Arena first.");
                return;
            }
            player.setController(PController.getController("magearena"));
            player.getController().script("arena2_wave_id", 1);
            player.getMovement().teleport(3104, 3933);
        }
    },

    getDialogueEntries: function() {
        return entries;
    }
}
