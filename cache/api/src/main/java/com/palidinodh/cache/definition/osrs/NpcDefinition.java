package com.palidinodh.cache.definition.osrs;

import com.palidinodh.cache.definition.Definition;
import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.store.fs.Store;
import com.palidinodh.cache.store.util.ConfigType;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Stream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;

@SuppressWarnings("FieldNotUsedInToString")
@Getter
public class NpcDefinition implements Definition {

  private static final NpcDefinition DEFAULT = new NpcDefinition(-1);

  @Getter private static NpcDefinition[] definitions;

  private String name = "null";
  private int combatLevel = -1;
  private boolean minimapVisible = true;
  private int headIcon = -1;
  private String[] optionsArray = {null, null, null, null, null};
  private boolean renderPriority;
  private boolean pet;
  private boolean interactable = true;
  private boolean rotationFlag = true;
  private int standAnimation = -1;
  private int walkAnimation = -1;
  private int turn180Animation = -1;
  private int turn90RightAnimation = -1;
  private int turn90LeftAnimation = -1;
  private int size = 1;
  private int[] modelIds = new int[0];
  private int[] chatheadModelIds = new int[0];
  private int[] colorToFind = new int[0];
  private int[] colorToReplace = new int[0];
  private int[] textureToFind = new int[0];
  private int[] textureToReplace = new int[0];
  private int resizeX = 128;
  private int resizeY = 128;
  private int ambient;
  private int contrast;
  private int rotationSpeed = 32;
  private int category;
  private int varpId = -1;
  private int varbitId = -1;
  private int[] configs = new int[0];
  private Map<Integer, Object> attributes = new HashMap<>();
  private transient int id;
  private transient String lowerCaseName = "null";
  private transient List<DefinitionOption> options;
  private transient int[] modelColors;

  public NpcDefinition(int id) {
    this.id = id;
  }

  public static NpcDefinition getDefinition(int id) {
    if (definitions == null) {
      return DEFAULT;
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id]
        : DEFAULT;
  }

  public static String getName(int id) {
    if (definitions == null) {
      return DEFAULT.getName();
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id].getName()
        : DEFAULT.getName();
  }

  public static String getLowerCaseName(int id) {
    if (definitions == null) {
      return DEFAULT.getLowerCaseName();
    }
    return id >= 0 && id < definitions.length && definitions[id] != null
        ? definitions[id].getLowerCaseName()
        : DEFAULT.getLowerCaseName();
  }

  public static int size() {
    return definitions.length;
  }

  public static synchronized void load(boolean force, Store store) {
    if (!force && definitions != null) {
      return;
    }
    try {
      var files = store.getIndex(IndexType.CONFIG).getArchive(ConfigType.NPC).loadFiles();
      definitions = new NpcDefinition[files.get(files.size() - 1).getFileId() + 1];
      for (var file : files) {
        var def = definitions[file.getFileId()] = new NpcDefinition(file.getFileId());
        if (file.getContents() != null) {
          def.load(new Stream(file.getContents()));
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString() {
    var str = id + "-" + name;
    if (combatLevel > 0) {
      str += " (Level " + combatLevel + ")";
    }
    return str;
  }

  @Override
  public void load(Stream stream) {
    while (true) {
      var opcode = stream != null ? stream.readUnsignedByte() : 0;
      if (opcode == 0) {
        break;
      }
      switch (opcode) {
        case 1:
          {
            modelIds = new int[stream.readUnsignedByte()];
            for (var i = 0; i < modelIds.length; i++) {
              var modelId = stream.readUnsignedShort();
              if (modelId == 0xFFFF) {
                modelId = -1;
              }
              modelIds[i] = modelId;
            }
            break;
          }
        case 2:
          {
            name = stream.readString();
            lowerCaseName = name.toLowerCase();
            break;
          }
        case 12:
          size = stream.readUnsignedByte();
          break;
        case 13:
          standAnimation = stream.readUnsignedShort();
          break;
        case 14:
          walkAnimation = stream.readUnsignedShort();
          break;
        case 15:
          turn90LeftAnimation = stream.readUnsignedShort();
          break;
        case 16:
          turn90RightAnimation = stream.readUnsignedShort();
          break;
        case 17:
          walkAnimation = stream.readUnsignedShort();
          turn180Animation = stream.readUnsignedShort();
          turn90RightAnimation = stream.readUnsignedShort();
          turn90LeftAnimation = stream.readUnsignedShort();
          break;
        case 18:
          category = stream.readUnsignedShort();
          break;
        case 30:
        case 31:
        case 32:
        case 33:
        case 34:
          {
            optionsArray[opcode - 30] = stream.readString();
            if (optionsArray[opcode - 30].equalsIgnoreCase("Hidden")) {
              optionsArray[opcode - 30] = null;
            }
            break;
          }
        case 40:
          {
            var length = stream.readUnsignedByte();
            colorToFind = new int[length];
            colorToReplace = new int[length];
            for (var i = 0; i < length; i++) {
              colorToFind[i] = stream.readUnsignedShort();
              colorToReplace[i] = stream.readUnsignedShort();
            }
            break;
          }
        case 41:
          {
            var length = stream.readUnsignedByte();
            textureToFind = new int[length];
            textureToReplace = new int[length];
            for (var i = 0; i < length; i++) {
              textureToFind[i] = stream.readUnsignedShort();
              textureToReplace[i] = stream.readUnsignedShort();
            }
            break;
          }
        case 60:
          {
            chatheadModelIds = new int[stream.readUnsignedByte()];
            for (var i = 0; i < chatheadModelIds.length; i++) {
              var modelId = stream.readUnsignedShort();
              if (modelId == 0xFFFF) {
                modelId = -1;
              }
              chatheadModelIds[i] = modelId;
            }
            break;
          }
        case 93:
          minimapVisible = false;
          break;
        case 95:
          combatLevel = stream.readUnsignedShort();
          break;
        case 97:
          resizeX = stream.readUnsignedShort();
          break;
        case 98:
          resizeY = stream.readUnsignedShort();
          break;
        case 99:
          renderPriority = true;
          break;
        case 100:
          ambient = stream.readByte();
          break;
        case 101:
          contrast = stream.readByte();
          break;
        case 102:
          headIcon = stream.readUnsignedShort();
          break;
        case 103:
          rotationSpeed = stream.readUnsignedShort();
          break;
        case 107:
          interactable = false;
          break;
        case 109:
          rotationFlag = false;
          break;
        case 111:
          pet = true;
          break;
        case 106:
        case 118:
          {
            varbitId = stream.readUnsignedShort();
            if (varbitId == 0xFFFF) {
              varbitId = -1;
            }
            varpId = stream.readUnsignedShort();
            if (varpId == 0xFFFF) {
              varpId = -1;
            }
            var endVar = -1;
            if (opcode == 118) {
              endVar = stream.readUnsignedShort();
              if (endVar == 0xFFFF) {
                endVar = -1;
              }
            }
            var length = stream.readUnsignedByte();
            configs = new int[length + 2];
            for (var i = 0; i <= length; i++) {
              configs[i] = stream.readUnsignedShort();
              if (configs[i] == 0xFFFF) {
                configs[i] = -1;
              }
            }
            configs[length + 1] = endVar;
            break;
          }
        case 249:
          attributes = stream.readScript();
          break;
        default:
          System.out.println("Npc Definition (" + id + ") Unknown Opcode: " + opcode);
          break;
      }
    }
    postLoad();
  }

  @Override
  public void postLoad() {
    options = new ArrayList<>();
    for (var i = 0; i < optionsArray.length; i++) {
      if (optionsArray[i] == null) {
        continue;
      }
      options.add(new DefinitionOption(i, optionsArray[i].toLowerCase()));
    }
    options = Collections.unmodifiableList(options);
  }

  @Override
  public void postLoadSpecial(Store store) {
    var colors = new ArrayList<Integer>();
    for (var modelId : modelIds) {
      try {
        var bytes = store.getFileData(IndexType.MODEL.getId(), modelId, 0, null);
        var model = new ModelDefinition(bytes);
        for (var color : model.faceColors) {
          if (colors.contains(color)) {
            continue;
          }
          colors.add(color);
        }
      } catch (Exception ignored) {
      }
    }
    Collections.sort(colors);
    modelColors = colors.stream().mapToInt(i -> i).toArray();
  }

  @Override
  public Stream save(Stream stream) {
    if (modelIds != null && modelIds.length > 0) {
      if (modelIds.length > 0xFF) {
        throw new IllegalArgumentException(id + ": modelIds.length");
      }
      stream.writeByte(1);
      stream.writeByte(modelIds.length);
      for (var modelId : modelIds) {
        stream.writeShort(modelId);
      }
    }
    if (name != null && !name.isEmpty() && !name.equals("null")) {
      stream.writeByte(2);
      stream.writeString(name);
    }
    if (size != 1) {
      if (size < 0 || size > 0xFF) {
        throw new IllegalArgumentException(id + ": size");
      }
      stream.writeByte(12);
      stream.writeByte(size);
    }
    if (standAnimation != -1) {
      if (standAnimation < 0 || standAnimation > 0xFFFF) {
        throw new IllegalArgumentException(id + ": standAnimation");
      }
      stream.writeByte(13);
      stream.writeShort(standAnimation);
    }
    if (walkAnimation != -1
        && turn180Animation != -1
        && turn90RightAnimation != -1
        && turn90LeftAnimation != -1) {
      if (walkAnimation < 0 || walkAnimation > 0xFFFF) {
        throw new IllegalArgumentException(id + ": walkAnimation");
      }
      if (turn180Animation < 0 || turn180Animation > 0xFFFF) {
        throw new IllegalArgumentException(id + ": turn180Animation");
      }
      if (turn90RightAnimation < 0 || turn90RightAnimation > 0xFFFF) {
        throw new IllegalArgumentException(id + ": turn90RightAnimation");
      }
      if (turn90LeftAnimation < 0 || turn90LeftAnimation > 0xFFFF) {
        throw new IllegalArgumentException(id + ": turn90LeftAnimation");
      }
      stream.writeByte(17);
      stream.writeShort(walkAnimation);
      stream.writeShort(turn180Animation);
      stream.writeShort(turn90RightAnimation);
      stream.writeShort(turn90LeftAnimation);
    } else {
      if (walkAnimation != -1) {
        if (walkAnimation < 0 || walkAnimation > 0xFFFF) {
          throw new IllegalArgumentException(id + ": walkAnimation");
        }
        stream.writeByte(14);
        stream.writeShort(walkAnimation);
      }
      if (turn90LeftAnimation != -1) {
        if (turn90LeftAnimation < 0 || turn90LeftAnimation > 0xFFFF) {
          throw new IllegalArgumentException(id + ": turn90LeftAnimation");
        }
        stream.writeByte(15);
        stream.writeShort(turn90LeftAnimation);
      }
      if (turn90RightAnimation != -1) {
        if (turn90RightAnimation < 0 || turn90RightAnimation > 0xFFFF) {
          throw new IllegalArgumentException(id + ": turn90RightAnimation");
        }
        stream.writeByte(16);
        stream.writeShort(turn90RightAnimation);
      }
    }
    if (category != 0) {
      if (category < 0 || category > 0xFFFF) {
        throw new IllegalArgumentException(id + ": category");
      }
      stream.writeByte(18);
      stream.writeShort(category);
    }
    if (optionsArray != null && optionsArray.length != 0) {
      if (optionsArray.length != 5) {
        throw new IllegalArgumentException(id + ": optionsArray.length");
      }
      for (var i = 0; i < optionsArray.length; i++) {
        var option = optionsArray[i];
        if (option == null) {
          continue;
        }
        if (option.equalsIgnoreCase("null")) {
          continue;
        }
        if (option.equalsIgnoreCase("Hidden")) {
          continue;
        }
        stream.writeByte(30 + i);
        stream.writeString(option);
      }
    }
    if (colorToFind != null
        && colorToFind.length != 0
        && colorToReplace != null
        && colorToReplace.length != 0) {
      if (colorToFind.length > 0xFF) {
        throw new IllegalArgumentException(id + ": colorToFind.length");
      }
      if (colorToFind.length != colorToReplace.length) {
        throw new IllegalArgumentException(id + ": colorToFind.length != colorToReplace.length");
      }
      stream.writeByte(40);
      stream.writeByte(colorToFind.length);
      for (var i = 0; i < colorToFind.length; i++) {
        stream.writeShort(colorToFind[i]);
        stream.writeShort(colorToReplace[i]);
      }
    }
    if (textureToFind != null
        && textureToFind.length != 0
        && textureToReplace != null
        && textureToReplace.length != 0) {
      if (textureToFind.length > 0xFF) {
        throw new IllegalArgumentException(id + ": colorToFind.length");
      }
      if (textureToFind.length != textureToReplace.length) {
        throw new IllegalArgumentException(
            id + ": textureToFind.length != textureToReplace.length");
      }
      stream.writeByte(41);
      stream.writeByte(textureToFind.length);
      for (var i = 0; i < textureToFind.length; i++) {
        stream.writeShort(textureToFind[i]);
        stream.writeShort(textureToReplace[i]);
      }
    }
    if (chatheadModelIds != null && chatheadModelIds.length != 0) {
      if (chatheadModelIds.length > 0xFF) {
        throw new IllegalArgumentException(id + ": chatheadModelIds.length");
      }
      stream.writeByte(60);
      stream.writeByte(modelIds.length);
      for (var modelId : modelIds) {
        if (modelId < -1 || modelId > 0xFFFF) {
          throw new IllegalArgumentException(id + ": chatheadModelIds");
        }
        stream.writeShort(modelId);
      }
    }
    if (!minimapVisible) {
      stream.writeByte(93);
    }
    if (combatLevel != -1) {
      if (combatLevel < 0 || combatLevel > 0xFFFF) {
        throw new IllegalArgumentException(id + ": combatLevel");
      }
      stream.writeByte(95);
      stream.writeShort(combatLevel);
    }
    if (resizeX != 128) {
      if (resizeX < 0 || resizeX > 0xFFFF) {
        throw new IllegalArgumentException(id + ": widthScale");
      }
      stream.writeByte(97);
      stream.writeShort(resizeX);
    }
    if (resizeY != 128) {
      if (resizeY < 0 || resizeY > 0xFFFF) {
        throw new IllegalArgumentException(id + ": heightScale");
      }
      stream.writeByte(98);
      stream.writeShort(resizeY);
    }
    if (renderPriority) {
      stream.writeByte(99);
    }
    if (ambient != 0) {
      stream.writeByte(100);
      stream.writeByte(ambient);
    }
    if (contrast != 0) {
      stream.writeByte(101);
      stream.writeByte(contrast);
    }
    if (headIcon != -1) {
      if (headIcon < 0 || headIcon > 0xFFFF) {
        throw new IllegalArgumentException(id + ": headIcon");
      }
      stream.writeByte(102);
      stream.writeShort(headIcon);
    }
    if (rotationSpeed != 32) {
      if (rotationSpeed < 0 || rotationSpeed > 0xFFFF) {
        throw new IllegalArgumentException(id + ": rotationSpeed");
      }
      stream.writeByte(103);
      stream.writeShort(rotationSpeed);
    }
    if (!interactable) {
      stream.writeByte(107);
    }
    if (!rotationFlag) {
      stream.writeByte(109);
    }
    if (pet) {
      stream.writeByte(111);
    }
    if (varbitId != -1 || varpId != -1 || configs != null && configs.length != 0) {
      if (varbitId < -1 || varbitId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": varbitId");
      }
      if (varpId < -1 || varpId > 0xFFFF) {
        throw new IllegalArgumentException(id + ": varpId");
      }
      if (configs.length > 0xFFFF) {
        throw new IllegalArgumentException(id + ": configs.length");
      }
      var is118 = configs[configs.length - 1] != 0;
      stream.writeByte(is118 ? 118 : 106);
      stream.writeShort(varbitId);
      stream.writeShort(varpId);
      if (is118) {
        stream.writeShort(configs[configs.length - 1]);
      }
      stream.writeByte(configs.length - 2);
      for (var i = 0; i < configs.length - 1; i++) {
        stream.writeShort(configs[i]);
      }
    }
    if (attributes != null && !attributes.isEmpty()) {
      if (attributes.size() > 0xFF) {
        throw new IllegalArgumentException(id + ": attributes.size()");
      }
      stream.writeByte(249);
      stream.writeScript(attributes);
    }
    stream.writeByte(0);
    return stream;
  }

  @Override
  public Definition getDefaultDefinition() {
    return DEFAULT;
  }

  @Override
  public NpcDefinition[] allDefinitions() {
    return definitions;
  }

  public int[] getStanceAnimations() {
    int turn180Anim = turn180Animation;
    int turn90RightAnim = turn90RightAnimation;
    int turn90LeftAnim = turn90LeftAnimation;
    if (turn180Anim == -1) {
      turn180Anim = walkAnimation;
    }
    if (turn90RightAnim == -1) {
      turn90RightAnim = walkAnimation;
    }
    if (turn90LeftAnim == -1) {
      turn90LeftAnim = walkAnimation;
    }
    return new int[] {
      standAnimation,
      standAnimation,
      walkAnimation,
      turn180Anim,
      turn90RightAnim,
      turn90LeftAnim,
      walkAnimation
    };
  }

  public boolean hasOptions() {
    return options != null && !options.isEmpty();
  }

  public boolean hasOption(String search) {
    if (!hasOptions()) {
      return false;
    }
    for (var option : options) {
      if (!search.equalsIgnoreCase(option.getText())) {
        continue;
      }
      return true;
    }
    return false;
  }

  public boolean isOption(int index, String search) {
    var option = getOption(index);
    return option != null && search.equalsIgnoreCase(option.getText());
  }

  public DefinitionOption getOption(int index) {
    if (!hasOptions()) {
      return null;
    }
    for (var option : options) {
      if (index != option.getIndex()) {
        continue;
      }
      return option;
    }
    return null;
  }

  public String getOptionText(int index) {
    var option = getOption(index);
    return option == null ? null : option.getText();
  }
}
