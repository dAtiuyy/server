package com.palidinodh.worldevent.creepycrawly.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.worldevent.creepycrawly.CreepyCrawlyController;
import com.palidinodh.worldevent.creepycrawly.CreepyCrawlyMinigame;

@ReferenceId(ObjectId.CHEST_32572)
class SupplyChestMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    if (!player.getController().is(CreepyCrawlyController.class)) {
      return;
    }
    var controller = player.getController().as(CreepyCrawlyController.class);
    var minigame = controller.getMinigame();
    var players = minigame.getSupplyChestPlayerIds(mapObject);
    if (players.contains(player.getId())) {
      player.getGameEncoder().sendMessage("You search the chest but find nothing.");
      return;
    }
    players.add(player.getId());
    for (var i = 0; i < 2; i++) {
      player.getInventory().addOrDropItem(RandomItem.getItem(CreepyCrawlyMinigame.SUPPLY_ITEMS));
    }
    player
        .getGameEncoder()
        .sendMapObject(new MapObject(minigame.getMap().getOpenedSupplyChestId(), mapObject));
  }
}
