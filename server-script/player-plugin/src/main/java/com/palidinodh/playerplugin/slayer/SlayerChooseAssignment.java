package com.palidinodh.playerplugin.slayer;

import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.chat.NormalChatDialogue;
import com.palidinodh.osrscore.model.entity.player.slayer.AssignedSlayerTask;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerMaster;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerTask;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerTaskIdentifier;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PTime;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SlayerChooseAssignment {

  private transient Player player;
  private transient SlayerPlugin plugin;

  public void openChooseMasterDialogue() {
    player.openOptionsDialogue(
        new DialogueOption(
            "Turael - level 1",
            (c, s) -> {
              choose("Turael");
            }),
        new DialogueOption(
            "Mazchna - level 20",
            (c, s) -> {
              choose("Mazchna");
            }),
        new DialogueOption(
            "Chaeldar - level 70",
            (c, s) -> {
              choose("Chaeldar");
            }),
        new DialogueOption(
            "Nieve - level 85",
            (c, s) -> {
              choose("Nieve");
            }),
        new DialogueOption(
            "Duradel - level 100",
            (c, s) -> {
              choose("Duradel");
            }));
  }

  public void choose(String masterName) {
    var master = SlayerMaster.get(masterName);
    if (master == null) {
      return;
    }
    var slayerLevel = player.getController().getLevelForXP(Skills.SLAYER);
    var rewards = plugin.getRewards();
    var isBossMaster = masterName.equals(SlayerMaster.BOSS_MASTER);
    var isWilderness = masterName.equals(SlayerMaster.WILDERNESS_MASTER);
    if (isWilderness && !SlayerPlugin.isWildernessTasksEnabled()) {
      player.getGameEncoder().sendMessage("Wilderness tasks can't currently be assigned.");
      return;
    }
    var assignedTask = isWilderness ? plugin.getWildernessTask() : plugin.getTask();
    var assignedSlayerTask = assignedTask.getSlayerTask();
    if (!assignedTask.isComplete()) {
      if (assignedTask == plugin.getTask() && !masterName.equals(SlayerMaster.RESET_MASTER)) {
        if (!player.isUsergroup(UserRank.SAPPHIRE_MEMBER)
            || PTime.getDate().equals(assignedTask.getDate())) {
          player.getGameEncoder().sendMessage("You already have a task.");
          return;
        }
      }
      if (assignedTask == plugin.getWildernessTask()) {
        player.getGameEncoder().sendMessage("You already have a task.");
        return;
      }
    }
    var masterCombatLevel = master.getCombatLevel();
    if (slayerLevel == 99) {
      masterCombatLevel = 0;
    }
    if (player.getSkills().getCombatLevel() < masterCombatLevel) {
      player
          .getGameEncoder()
          .sendMessage(
              "You need a combat level of "
                  + master.getCombatLevel()
                  + " to use this Slayer master.");
      return;
    }
    if (slayerLevel < master.getSlayerLevel()) {
      player
          .getGameEncoder()
          .sendMessage(
              "You need a Slayer level of "
                  + master.getSlayerLevel()
                  + " to get one of these tasks.");
      return;
    }
    SlayerTask selectedTask = null;
    if (assignedSlayerTask != null
        && player.getEquipment().wearingAccomplishmentCape(Skills.SLAYER)
        && PRandom.randomE(10) == 0
        && !plugin.isBlockedTask(assignedSlayerTask.getIdentifier())) {
      selectedTask = assignedSlayerTask;
    }
    if (selectedTask == null
        && rewards.isUnlocked(SlayerUnlock.LIKE_A_BOSS)
        && master.hasTask(SlayerTaskIdentifier.BOSS)
        && player.getPlugin(BondPlugin.class).isRelicUnlocked(BondRelicType.SLAYER_IM_A_BOSS)) {
      if (PRandom.randomE(4) == 0) {
        var aTask = master.getTask(SlayerTaskIdentifier.BOSS);
        if (aTask.getMaximumQuantity() == 0) {
          aTask = getRandomBossTask(assignedSlayerTask);
        }
        if (aTask != null) {
          selectedTask = aTask;
        }
      }
    }
    for (var i = 0; i < 128 && selectedTask == null; i++) {
      var aTask = PRandom.listRandom(master.getTasks());
      if (isBossMaster || aTask.getIdentifier() == SlayerTaskIdentifier.BOSS) {
        if (!isBossMaster && !rewards.isUnlocked(SlayerUnlock.LIKE_A_BOSS)) {
          continue;
        }
        if (aTask.getMaximumQuantity() == 0) {
          aTask = getRandomBossTask(assignedSlayerTask);
        }
        if (aTask == null) {
          continue;
        }
      }
      if (slayerLevel < aTask.getSlayerLevel()) {
        continue;
      }
      if (!isWilderness && plugin.isBlockedTask(aTask.getIdentifier())) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.RED_DRAGON
          && !rewards.isUnlocked(SlayerUnlock.SEEING_RED)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.MITHRIL_DRAGON
          && !rewards.isUnlocked(SlayerUnlock.I_HOPE_YOU_MITH_ME)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.AVIANSIE
          && !rewards.isUnlocked(SlayerUnlock.WATCH_THE_BIRDIE)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.TZHAAR
          && !rewards.isUnlocked(SlayerUnlock.HOT_STUFF)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.LIZARDMAN
          && !rewards.isUnlocked(SlayerUnlock.REPTILE_GOT_RIPPED)) {
        continue;
      }
      if (aTask.getIdentifier() == SlayerTaskIdentifier.FOSSIL_ISLAND_WYVERN
          && rewards.isUnlocked(SlayerUnlock.STOP_THE_WYVERN)) {
        continue;
      }
      if (assignedSlayerTask != null
          && assignedSlayerTask.getIdentifier() != null
          && assignedSlayerTask.getIdentifier() == aTask.getIdentifier()) {
        continue;
      }
      if (assignedSlayerTask != null
          && assignedSlayerTask.getName() != null
          && assignedSlayerTask.getName().equals(aTask.getName())) {
        continue;
      }
      selectedTask = aTask;
      break;
    }
    if (selectedTask == null) {
      player.getGameEncoder().sendMessage("Failed to assign a task!");
      return;
    }
    int quantity = selectedTask.getRandomQuantity();
    if (selectedTask.getIdentifier() != null
        && selectedTask.getIdentifier().getExtendedUnlock() != null
        && rewards.isUnlocked(selectedTask.getIdentifier().getExtendedUnlock())) {
      quantity *= 1.5;
    }
    var isBoss =
        isBossMaster
            || selectedTask.getMaster().getName().equals(SlayerMaster.BOSS_MASTER)
            || selectedTask.getIdentifier() == SlayerTaskIdentifier.BOSS;
    if (isBoss) {
      quantity = selectedTask.getMinimumQuantity();
    }
    if (assignedTask != null && !assignedTask.isComplete() && assignedTask == plugin.getTask()) {
      if (masterName.equalsIgnoreCase(SlayerMaster.RESET_MASTER)
          && !assignedTask.getName().equals(SlayerMaster.RESET_MASTER)) {
        plugin.setConsecutiveTasks(0);
      }
    }
    var newAssignedTask = new AssignedSlayerTask(selectedTask, isBoss, quantity);
    var newSlayerTask = newAssignedTask.getSlayerTask();
    if (isWilderness) {
      plugin.setWildernessTask(newAssignedTask);
    } else {
      plugin.setTask(newAssignedTask);
    }
    player.openDialogue(
        new NormalChatDialogue(
            master.getNpcId(),
            "Your new task is to kill "
                + newAssignedTask.getQuantity()
                + " "
                + newAssignedTask.getPluralName()
                + ".",
            (c, s) -> {
              if (newAssignedTask.isBoss()) {
                setBossTaskQuantity(newAssignedTask);
                return;
              }
              if (isWilderness) {
                plugin.sendAssignedTaskMessages(newAssignedTask);
                return;
              }
              if (newAssignedTask.getIdentifier() == SlayerTaskIdentifier.TZHAAR) {
                setTzHaarTaskQuantity(newAssignedTask);
                return;
              }
              setTaskQuantity(newAssignedTask);
            }));
    player.sendDiscordNewAccountLog(
        "Slayer Task: " + newAssignedTask.getQuantity() + " " + newAssignedTask.getPluralName());
  }

  private SlayerTask getRandomBossTask(SlayerTask assignedTask) {
    var rewards = plugin.getRewards();
    if (!rewards.isUnlocked(SlayerUnlock.LIKE_A_BOSS)) {
      return null;
    }
    SlayerTask selectedTask = null;
    for (var i = 0; i < 128; i++) {
      var aTask = PRandom.listRandom(SlayerMaster.get(SlayerMaster.BOSS_MASTER).getTasks());
      if (aTask.getIdentifier() == SlayerTaskIdentifier.TZTOK_JAD) {
        continue;
      }
      if (aTask.getName().equals("Grotesque Guardians")
          && !rewards.isUnlocked(SlayerUnlock.GROTESQUE_GUARDIANS)) {
        continue;
      }
      if (aTask.isWilderness() && rewards.isUnlocked(SlayerUnlock.WILDERNESS_BOSS)) {
        continue;
      }
      if (aTask.getName().equals("Corporeal Beast")
          && !rewards.isUnlocked(SlayerUnlock.CORPOREAL_BEAST)) {
        continue;
      }
      if (aTask.getName().equals("Raids Boss") && !rewards.isUnlocked(SlayerUnlock.RAIDS)) {
        continue;
      }
      if (aTask.getName().equals("The Nightmare")
          && !rewards.isUnlocked(SlayerUnlock.THE_NIGHTMARE)) {
        continue;
      }
      if (assignedTask != null && assignedTask.isWilderness() && aTask.isWilderness()) {
        continue;
      }
      selectedTask = aTask;
      break;
    }
    return selectedTask;
  }

  private void setBossTaskQuantity(AssignedSlayerTask assignedTask) {
    if (!assignedTask.isBoss()) {
      return;
    }
    if (assignedTask.getQuantity() != assignedTask.getTotal()) {
      return;
    }
    var slayerTask = assignedTask.getSlayerTask();
    player
        .getGameEncoder()
        .sendEnterAmount(
            "Quantity between "
                + slayerTask.getMinimumQuantity()
                + " and "
                + slayerTask.getMaximumQuantity()
                + ":",
            ie -> {
              if (ie >= slayerTask.getMinimumQuantity() && ie <= slayerTask.getMaximumQuantity()) {
                assignedTask.resetQuantity(ie);
              }
              plugin.sendAssignedTaskMessages(assignedTask);
            });
  }

  private void setTzHaarTaskQuantity(AssignedSlayerTask assignedTask) {
    var slayerTask = assignedTask.getSlayerTask();
    if (slayerTask == null) {
      return;
    }
    if (slayerTask.isWilderness()) {
      return;
    }
    if (assignedTask.getQuantity() != assignedTask.getTotal()) {
      return;
    }
    player.openOptionsDialogue(
        new DialogueOption(
            "Complete task through the Fight Cave.",
            (c, s) -> {
              var master = assignedTask.getSlayerMaster();
              var task =
                  SlayerMaster.get(SlayerMaster.BOSS_MASTER)
                      .getTask(SlayerTaskIdentifier.TZTOK_JAD);
              assignedTask.reset(task, true, 1);
              player.openDialogue(
                  new NormalChatDialogue(
                      master.getNpcId(),
                      "Your new task is to kill "
                          + assignedTask.getQuantity()
                          + " "
                          + assignedTask.getPluralName()
                          + ".",
                      (c1, s1) -> {
                        plugin.sendAssignedTaskMessages(assignedTask);
                      }));
            }),
        new DialogueOption(
            "Keep quantity at " + assignedTask.getTotal() + ".",
            (c, s) -> {
              plugin.sendAssignedTaskMessages(assignedTask);
            }),
        new DialogueOption(
            "Reduce quantity to " + (assignedTask.getTotal() / 2) + ".",
            (c, s) -> {
              assignedTask.resetQuantity(assignedTask.getTotal() / 2);
              plugin.sendAssignedTaskMessages(assignedTask);
            }));
  }

  private void setTaskQuantity(AssignedSlayerTask assignedTask) {
    var slayerTask = assignedTask.getSlayerTask();
    if (slayerTask == null) {
      return;
    }
    if (slayerTask.isWilderness()) {
      return;
    }
    if (assignedTask.getQuantity() != assignedTask.getTotal()) {
      return;
    }
    player.openOptionsDialogue(
        new DialogueOption(
            "Keep quantity at " + assignedTask.getTotal() + ".",
            (c, s) -> {
              plugin.sendAssignedTaskMessages(assignedTask);
            }),
        new DialogueOption(
            "Reduce quantity to " + (assignedTask.getTotal() / 2) + ".",
            (c, s) -> {
              assignedTask.resetQuantity(assignedTask.getTotal() / 2);
              plugin.sendAssignedTaskMessages(assignedTask);
              player.sendDiscordNewAccountLog("Slayer Task: Quantity Reduced");
            }));
  }
}
