package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.definition.osrs.ItemDefinition;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.playerplugin.wilderness.WildernessPlugin;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PEvent;
import java.util.ArrayList;
import java.util.List;

@ReferenceId(WidgetId.CUSTOM_MYSTERY_BOX)
class MysteryBoxWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (player.isLocked()) {
      return;
    }
    switch (childId) {
      case 58:
        var boxId = player.getAttributeInt("mystery_box");
        if (boxId <= 0) {
          break;
        }
        if (!player.getInventory().hasItem(boxId)) {
          player
              .getGameEncoder()
              .sendMessage("You need a " + ItemDefinition.getName(boxId) + " to spin.");
          break;
        }
        var mysteryBox = MysteryBox.getMysteryBox(boxId);
        if (mysteryBox == null) {
          return;
        }
        player.getInventory().deleteItem(boxId);
        List<Item> mysteryBoxItems = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
          mysteryBoxItems.add(mysteryBox.getRandomItem(player));
        }
        player.getGameEncoder().sendItems(WidgetId.CUSTOM_MYSTERY_BOX, 41, 0, mysteryBoxItems);
        player.getGameEncoder().sendHideWidget(WidgetId.CUSTOM_MYSTERY_BOX, 57, true);
        int totalRolls = mysteryBox.getRolls();
        var event =
            new PEvent(0) {
              private int rollsComplete = 0;

              @Override
              public void execute() {
                if (!player.isVisible()) {
                  stop();
                  return;
                }
                setTick(0);
                mysteryBoxItems.remove(0);
                mysteryBoxItems.add(mysteryBox.getRandomItem(player));
                player
                    .getGameEncoder()
                    .sendItems(WidgetId.CUSTOM_MYSTERY_BOX, 41, 0, mysteryBoxItems);
                if (getExecutions() > 0 && (getExecutions() % 4) == 0) {
                  boolean complete = ++rollsComplete >= totalRolls;
                  if (complete) {
                    stop();
                    player.getGameEncoder().sendHideWidget(WidgetId.CUSTOM_MYSTERY_BOX, 57, false);
                    var alwaysItems = mysteryBox.getAlwaysItems(boxId, player);
                    if (alwaysItems != null) {
                      alwaysItems.forEach(i -> player.getInventory().addOrDropItem(i));
                    }
                  }
                  var boxItem = mysteryBoxItems.get(mysteryBoxItems.size() - 3);
                  player.getInventory().addOrDropItem(boxItem);
                  setTick(2);
                  if (complete) {
                    if (mysteryBox.shouldSendItemDropNews(boxItem)) {
                      player
                          .getWorld()
                          .sendItemDropNews(
                              player, boxItem, "from a " + ItemDefinition.getName(boxId));
                    }
                    if (WildernessPlugin.isBloodyKey(boxId)) {
                      player
                          .getWorld()
                          .sendItemDropNews(
                              player, boxItem, "from a " + ItemDefinition.getName(boxId));
                    }
                    player.log(
                        PlayerLogType.LOOT_BOX,
                        boxItem.getLogName() + " from " + new Item(boxId).getLogName());
                    setTick(2);
                  }
                }
              }
            };
        player.getWorld().addEvent(event);
        break;
    }
  }
}
