package com.palidinodh.rs.communicationv2.notification;

import com.palidinodh.rs.adaptive.RsFriend;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communicationv2.CommunicationNotification;
import java.util.List;
import lombok.Getter;

@Getter
public class PrivateChatStateNotification extends CommunicationNotification {

  private String username;
  private int worldId;
  private int privateChatState;
  private List<RsFriend> friends;
  private List<String> ignores;

  public PrivateChatStateNotification(RsPlayer player) {
    username = player.getUsername();
    worldId = player.getWorldId();
    privateChatState = player.getPrivateChatStatus();
    friends = player.getFriends();
    ignores = player.getIgnores();
  }
}
