package com.palidinodh.playerplugin.tzhaar.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.MASTER_WAND_OR_60050)
class JadMasterWandItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "revert":
        {
          item.remove();
          player.getInventory().addItem(ItemId.MASTER_WAND);
          player.getInventory().addItem(ItemId.MTA_ORNAMENT_KIT_60061);
          break;
        }
    }
  }
}
