package com.palidinodh.maparea.wilderness.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Equipment;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.MAGE_OF_ZAMORAK_2581)
class MageOfZamorakNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    if (option.getIndex() == 0 || option.getIndex() == 2) {
      player.openShop("wild_runes");
    } else if (option.getIndex() == 3) {
      if (!player.getController().canTeleport(true)) {
        return;
      }
      if (player.getEquipment().getHandId() == 11095
          || player.getEquipment().getHandId() == 11097
          || player.getEquipment().getHandId() == 11099
          || player.getEquipment().getHandId() == 11101
          || player.getEquipment().getHandId() == 11103) {
        player
            .getEquipment()
            .setItem(
                Equipment.Slot.HAND,
                player.getEquipment().getHandId() == 11103
                    ? null
                    : new Item(player.getEquipment().getHandId() + 2, 1));
        player.getAppearance().setUpdate(true);
      } else {
        player.getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
      }
      npc.setForceMessage("Veniens! Sallakar! Rinnesset!");
      npc.setAnimation(1818);
      npc.setGraphic(343);
      var tiles =
          new Tile[] {
            new Tile(3045, 4810),
            new Tile(3059, 4818),
            new Tile(3062, 4835),
            new Tile(3054, 4850),
            new Tile(3043, 4854),
            new Tile(3027, 4851),
            new Tile(3017, 4840),
            new Tile(3015, 4826),
            new Tile(3021, 4813),
            new Tile(3035, 4809)
          };
      player
          .getMovement()
          .animatedTeleport(PRandom.arrayRandom(tiles), 1816, 715, new Graphic(342), null, 2);
      player.getController().stopWithTeleport();
      player.getCombat().clearHitEvents();
    }
  }
}
