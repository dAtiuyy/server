package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class RevenantCombat extends NpcCombat {

  private static final List<RandomItem> UNIQUE_DROP_TABLE =
      RandomItem.buildList(
          new RandomItem(ItemId.VIGGORAS_CHAINMACE_U).weight(1),
          new RandomItem(ItemId.CRAWS_BOW_U).weight(1),
          new RandomItem(ItemId.THAMMARONS_SCEPTRE_U).weight(1),
          new RandomItem(ItemId.AMULET_OF_AVARICE).weight(2));
  private static final List<RandomItem> MEDIOCRE_DROP_TABLE =
      RandomItem.buildList(
          new RandomItem(ItemId.DRAGON_PLATELEGS).weight(1),
          new RandomItem(ItemId.DRAGON_PLATESKIRT).weight(1),
          new RandomItem(ItemId.RUNE_FULL_HELM).weight(2),
          new RandomItem(ItemId.RUNE_PLATEBODY).weight(2),
          new RandomItem(ItemId.RUNE_PLATELEGS).weight(2),
          new RandomItem(ItemId.RUNE_KITESHIELD).weight(2),
          new RandomItem(ItemId.RUNE_WARHAMMER).weight(2),
          new RandomItem(ItemId.DRAGON_LONGSWORD).weight(1),
          new RandomItem(ItemId.DRAGON_DAGGER).weight(1),
          new RandomItem(ItemId.SUPER_RESTORE_4_NOTED, 1, 2).weight(4),
          new RandomItem(ItemId.ONYX_BOLT_TIPS, 2, 5).weight(4),
          new RandomItem(ItemId.DRAGONSTONE_BOLT_TIPS, 20, 35).weight(4),
          new RandomItem(ItemId.UNCUT_DRAGONSTONE_NOTED, 2, 3).weight(1),
          new RandomItem(ItemId.DEATH_RUNE, 30, 50).weight(3),
          new RandomItem(ItemId.BLOOD_RUNE, 30, 50).weight(3),
          new RandomItem(ItemId.LAW_RUNE, 40, 60).weight(3),
          new RandomItem(ItemId.RUNITE_ORE_NOTED, 1, 3).weight(6),
          new RandomItem(ItemId.ADAMANTITE_BAR_NOTED, 4, 6).weight(6),
          new RandomItem(ItemId.COAL_NOTED, 25, 50).weight(6),
          new RandomItem(ItemId.BATTLESTAFF_NOTED, 1).weight(5),
          new RandomItem(ItemId.BLACK_DRAGONHIDE_NOTED, 5, 7).weight(6),
          new RandomItem(ItemId.MAHOGANY_PLANK_NOTED, 7, 12).weight(5),
          new RandomItem(ItemId.MAGIC_LOGS_NOTED, 7, 12).weight(2),
          new RandomItem(ItemId.YEW_LOGS_NOTED, 30, 50).weight(3),
          new RandomItem(ItemId.MANTA_RAY_NOTED, 15, 25).weight(3),
          new RandomItem(ItemId.RUNITE_BAR_NOTED, 1, 2).weight(6),
          new RandomItem(ItemId.REVENANT_CAVE_TELEPORT).weight(7),
          new RandomItem(ItemId.BRACELET_OF_ETHEREUM_UNCHARGED).weight(15));

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var impCombat = NpcCombatDefinition.builder();
    impCombat.id(NpcId.REVENANT_IMP_7);
    impCombat.spawn(NpcCombatSpawn.builder().respawnDelay(50).build());
    impCombat.hitpoints(NpcCombatHitpoints.total(10));
    impCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(5)
            .magicLevel(9)
            .rangedLevel(5)
            .defenceLevel(4)
            .bonus(BonusType.ATTACK_MAGIC, 5)
            .bonus(BonusType.DEFENCE_MAGIC, 5)
            .build());
    impCombat.aggression(NpcCombatAggression.builder().always(true).build());
    impCombat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    impCombat.killCount(NpcCombatKillCount.builder().asName("Revenant").build());
    impCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(172)
        .blockAnimation(170);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(2));
    style.animation(169).attackSpeed(5);
    impCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(2));
    style.animation(173).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(206));
    impCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(2).splashOnMiss(true).build());
    style.animation(173).attackSpeed(5);
    style.targetGraphic(new Graphic(1454, 124));
    style.projectile(NpcCombatProjectile.id(384));
    impCombat.style(style.build());

    var goblinCombat = NpcCombatDefinition.builder();
    goblinCombat.id(NpcId.REVENANT_GOBLIN_15);
    goblinCombat.spawn(NpcCombatSpawn.builder().respawnDelay(50).build());
    goblinCombat.hitpoints(NpcCombatHitpoints.total(14));
    goblinCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(13)
            .magicLevel(12)
            .rangedLevel(15)
            .defenceLevel(14)
            .bonus(BonusType.MELEE_ATTACK, 6)
            .bonus(BonusType.ATTACK_MAGIC, 37)
            .bonus(BonusType.ATTACK_RANGED, 21)
            .bonus(BonusType.DEFENCE_STAB, 25)
            .bonus(BonusType.DEFENCE_SLASH, 28)
            .bonus(BonusType.DEFENCE_CRUSH, 31)
            .bonus(BonusType.DEFENCE_MAGIC, 1)
            .bonus(BonusType.DEFENCE_RANGED, 31)
            .build());
    goblinCombat.aggression(NpcCombatAggression.builder().always(true).build());
    goblinCombat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    goblinCombat.killCount(NpcCombatKillCount.builder().asName("Revenant").build());
    goblinCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(6182)
        .blockAnimation(6183);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(3));
    style.animation(6185).attackSpeed(5);
    goblinCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(3));
    style.animation(6184).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(206));
    goblinCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(3).splashOnMiss(true).build());
    style.animation(6184).attackSpeed(5);
    style.targetGraphic(new Graphic(1454, 124));
    style.projectile(NpcCombatProjectile.id(384));
    goblinCombat.style(style.build());

    var pyrefiendCombat = NpcCombatDefinition.builder();
    pyrefiendCombat.id(NpcId.REVENANT_PYREFIEND_52);
    pyrefiendCombat.spawn(NpcCombatSpawn.builder().respawnDelay(50).build());
    pyrefiendCombat.hitpoints(NpcCombatHitpoints.total(48));
    pyrefiendCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(60)
            .magicLevel(67)
            .rangedLevel(40)
            .defenceLevel(33)
            .bonus(BonusType.DEFENCE_STAB, 45)
            .bonus(BonusType.DEFENCE_SLASH, 40)
            .bonus(BonusType.DEFENCE_CRUSH, 50)
            .bonus(BonusType.DEFENCE_MAGIC, 15)
            .bonus(BonusType.DEFENCE_RANGED, 10)
            .build());
    pyrefiendCombat.aggression(NpcCombatAggression.builder().always(true).build());
    pyrefiendCombat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    pyrefiendCombat.killCount(NpcCombatKillCount.builder().asName("Revenant").build());
    pyrefiendCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(1580)
        .blockAnimation(1581);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(8));
    style.animation(1582).attackSpeed(5);
    pyrefiendCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(8));
    style.animation(1582).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(206));
    pyrefiendCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(8).splashOnMiss(true).build());
    style.animation(1582).attackSpeed(5);
    style.targetGraphic(new Graphic(1454, 124));
    style.projectile(NpcCombatProjectile.id(384));
    pyrefiendCombat.style(style.build());

    var hobgoblinCombat = NpcCombatDefinition.builder();
    hobgoblinCombat.id(NpcId.REVENANT_HOBGOBLIN_60);
    hobgoblinCombat.spawn(NpcCombatSpawn.builder().respawnDelay(50).build());
    hobgoblinCombat.hitpoints(NpcCombatHitpoints.total(72));
    hobgoblinCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(50)
            .magicLevel(55)
            .rangedLevel(60)
            .defenceLevel(41)
            .bonus(BonusType.MELEE_ATTACK, 20)
            .bonus(BonusType.ATTACK_MAGIC, 5)
            .bonus(BonusType.ATTACK_RANGED, 25)
            .bonus(BonusType.DEFENCE_STAB, 65)
            .bonus(BonusType.DEFENCE_SLASH, 60)
            .bonus(BonusType.DEFENCE_CRUSH, 68)
            .bonus(BonusType.DEFENCE_MAGIC, 30)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    hobgoblinCombat.aggression(NpcCombatAggression.builder().always(true).build());
    hobgoblinCombat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    hobgoblinCombat.killCount(NpcCombatKillCount.builder().asName("Revenant").build());
    hobgoblinCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(167)
        .blockAnimation(165);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(8));
    style.animation(164).attackSpeed(5);
    hobgoblinCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(8));
    style.animation(163).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(206));
    hobgoblinCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(8).splashOnMiss(true).build());
    style.animation(163).attackSpeed(5);
    style.targetGraphic(new Graphic(1454, 124));
    style.projectile(NpcCombatProjectile.id(384));
    hobgoblinCombat.style(style.build());

    var cyclopsCombat = NpcCombatDefinition.builder();
    cyclopsCombat.id(NpcId.REVENANT_CYCLOPS_82);
    cyclopsCombat.spawn(NpcCombatSpawn.builder().respawnDelay(50).build());
    cyclopsCombat.hitpoints(NpcCombatHitpoints.total(44));
    cyclopsCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(60)
            .magicLevel(65)
            .rangedLevel(70)
            .defenceLevel(49)
            .bonus(BonusType.MELEE_ATTACK, 53)
            .bonus(BonusType.DEFENCE_STAB, 140)
            .bonus(BonusType.DEFENCE_SLASH, 130)
            .bonus(BonusType.DEFENCE_CRUSH, 135)
            .bonus(BonusType.DEFENCE_MAGIC, 10)
            .bonus(BonusType.DEFENCE_RANGED, 135)
            .build());
    cyclopsCombat.aggression(NpcCombatAggression.builder().always(true).build());
    cyclopsCombat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    cyclopsCombat.killCount(NpcCombatKillCount.builder().asName("Revenant").build());
    cyclopsCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(4653)
        .blockAnimation(4651);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(16));
    style.animation(4652).attackSpeed(5);
    cyclopsCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(16));
    style.animation(4652).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(206));
    cyclopsCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(16).splashOnMiss(true).build());
    style.animation(4652).attackSpeed(5);
    style.targetGraphic(new Graphic(1454, 124));
    style.projectile(NpcCombatProjectile.id(384));
    cyclopsCombat.style(style.build());

    var hellhoundCombat = NpcCombatDefinition.builder();
    hellhoundCombat.id(NpcId.REVENANT_HELLHOUND_90);
    hellhoundCombat.spawn(NpcCombatSpawn.builder().respawnDelay(50).build());
    hellhoundCombat.hitpoints(NpcCombatHitpoints.total(80));
    hellhoundCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(76)
            .magicLevel(104)
            .rangedLevel(80)
            .defenceLevel(80)
            .bonus(BonusType.MELEE_ATTACK, 38)
            .bonus(BonusType.ATTACK_MAGIC, 30)
            .bonus(BonusType.DEFENCE_STAB, 138)
            .bonus(BonusType.DEFENCE_SLASH, 140)
            .bonus(BonusType.DEFENCE_CRUSH, 142)
            .bonus(BonusType.DEFENCE_MAGIC, 62)
            .bonus(BonusType.DEFENCE_RANGED, 140)
            .build());
    hellhoundCombat.aggression(NpcCombatAggression.builder().always(true).build());
    hellhoundCombat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    hellhoundCombat.killCount(NpcCombatKillCount.builder().asName("Revenant").build());
    hellhoundCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(6576)
        .blockAnimation(6578);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(14));
    style.animation(6581).attackSpeed(5);
    hellhoundCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(14));
    style.animation(6579).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(206));
    hellhoundCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(14).splashOnMiss(true).build());
    style.animation(6579).attackSpeed(5);
    style.targetGraphic(new Graphic(1454, 124));
    style.projectile(NpcCombatProjectile.id(384));
    hellhoundCombat.style(style.build());

    var demonCombat = NpcCombatDefinition.builder();
    demonCombat.id(NpcId.REVENANT_DEMON_98);
    demonCombat.spawn(NpcCombatSpawn.builder().respawnDelay(50).build());
    demonCombat.hitpoints(NpcCombatHitpoints.total(80));
    demonCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(83)
            .magicLevel(120)
            .rangedLevel(80)
            .defenceLevel(80)
            .bonus(BonusType.MELEE_ATTACK, 30)
            .bonus(BonusType.ATTACK_MAGIC, 50)
            .bonus(BonusType.ATTACK_RANGED, 40)
            .bonus(BonusType.DEFENCE_STAB, 124)
            .bonus(BonusType.DEFENCE_SLASH, 118)
            .bonus(BonusType.DEFENCE_CRUSH, 130)
            .bonus(BonusType.DEFENCE_MAGIC, 85)
            .bonus(BonusType.DEFENCE_RANGED, 90)
            .build());
    demonCombat.aggression(NpcCombatAggression.builder().always(true).build());
    demonCombat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    demonCombat.killCount(NpcCombatKillCount.builder().asName("Revenant").build());
    demonCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.DEMON)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(67)
        .blockAnimation(65);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(18));
    style.animation(64).attackSpeed(5);
    demonCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(18));
    style.animation(69).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(206));
    demonCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(18).splashOnMiss(true).build());
    style.animation(69).attackSpeed(5);
    style.targetGraphic(new Graphic(1454, 124));
    style.projectile(NpcCombatProjectile.id(384));
    demonCombat.style(style.build());

    var orkCombat = NpcCombatDefinition.builder();
    orkCombat.id(NpcId.REVENANT_ORK_105);
    orkCombat.spawn(NpcCombatSpawn.builder().respawnDelay(50).build());
    orkCombat.hitpoints(NpcCombatHitpoints.total(105));
    orkCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(99)
            .magicLevel(110)
            .rangedLevel(130)
            .defenceLevel(60)
            .bonus(BonusType.MELEE_ATTACK, 60)
            .bonus(BonusType.DEFENCE_STAB, 148)
            .bonus(BonusType.DEFENCE_SLASH, 150)
            .bonus(BonusType.DEFENCE_CRUSH, 146)
            .bonus(BonusType.DEFENCE_MAGIC, 50)
            .bonus(BonusType.DEFENCE_RANGED, 148)
            .build());
    orkCombat.aggression(NpcCombatAggression.builder().always(true).build());
    orkCombat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    orkCombat.killCount(NpcCombatKillCount.builder().asName("Revenant").build());
    orkCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(4321)
        .blockAnimation(4322);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(20));
    style.animation(4320).attackSpeed(5);
    orkCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(20));
    style.animation(4320).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(206));
    orkCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MAGIC).weight(32).build());
    style.damage(NpcCombatDamage.builder().maximum(20).splashOnMiss(true).build());
    style.animation(4320).attackSpeed(5);
    style.targetGraphic(new Graphic(1454, 124));
    style.projectile(NpcCombatProjectile.id(384));
    orkCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(20).splashOnMiss(true).build());
    style.animation(4320).attackSpeed(5);
    style.targetGraphic(new Graphic(363));
    style.effect(NpcCombatEffect.builder().bind(16).build());
    orkCombat.style(style.build());

    var darkBeastCombat = NpcCombatDefinition.builder();
    darkBeastCombat.id(NpcId.REVENANT_DARK_BEAST_120);
    darkBeastCombat.spawn(NpcCombatSpawn.builder().respawnDelay(50).build());
    darkBeastCombat.hitpoints(NpcCombatHitpoints.total(140));
    darkBeastCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(93)
            .magicLevel(130)
            .rangedLevel(135)
            .defenceLevel(80)
            .bonus(BonusType.MELEE_ATTACK, 65)
            .bonus(BonusType.ATTACK_RANGED, 45)
            .bonus(BonusType.DEFENCE_STAB, 153)
            .bonus(BonusType.DEFENCE_SLASH, 152)
            .bonus(BonusType.DEFENCE_CRUSH, 155)
            .bonus(BonusType.DEFENCE_MAGIC, 70)
            .bonus(BonusType.DEFENCE_RANGED, 158)
            .build());
    darkBeastCombat.aggression(NpcCombatAggression.builder().always(true).build());
    darkBeastCombat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    darkBeastCombat.killCount(NpcCombatKillCount.builder().asName("Revenant").build());
    darkBeastCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(2733)
        .blockAnimation(2732);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_CRUSH);
    style.damage(NpcCombatDamage.maximum(23));
    style.animation(2731).attackSpeed(5);
    darkBeastCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(23));
    style.animation(2731).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(206));
    darkBeastCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MAGIC).weight(32).build());
    style.damage(NpcCombatDamage.builder().maximum(23).splashOnMiss(true).build());
    style.animation(2731).attackSpeed(5);
    style.targetGraphic(new Graphic(1454, 124));
    style.projectile(NpcCombatProjectile.id(384));
    darkBeastCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(23).splashOnMiss(true).build());
    style.animation(2731).attackSpeed(5);
    style.targetGraphic(new Graphic(363));
    style.effect(NpcCombatEffect.builder().bind(16).build());
    darkBeastCombat.style(style.build());

    var knightCombat = NpcCombatDefinition.builder();
    knightCombat.id(NpcId.REVENANT_KNIGHT_126);
    knightCombat.spawn(NpcCombatSpawn.builder().respawnDelay(50).build());
    knightCombat.hitpoints(NpcCombatHitpoints.total(143));
    knightCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(100)
            .magicLevel(146)
            .rangedLevel(146)
            .defenceLevel(80)
            .bonus(BonusType.MELEE_ATTACK, 69)
            .bonus(BonusType.ATTACK_MAGIC, 55)
            .bonus(BonusType.ATTACK_RANGED, 55)
            .bonus(BonusType.DEFENCE_STAB, 195)
            .bonus(BonusType.DEFENCE_SLASH, 200)
            .bonus(BonusType.DEFENCE_CRUSH, 180)
            .bonus(BonusType.DEFENCE_MAGIC, 95)
            .bonus(BonusType.DEFENCE_RANGED, 190)
            .build());
    knightCombat.aggression(NpcCombatAggression.builder().always(true).build());
    knightCombat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    knightCombat.killCount(NpcCombatKillCount.builder().asName("Revenant").build());
    knightCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(836)
        .blockAnimation(404);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(27));
    style.animation(390).attackSpeed(5);
    knightCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(27));
    style.animation(2614).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(206));
    knightCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MAGIC).weight(32).build());
    style.damage(NpcCombatDamage.builder().maximum(27).splashOnMiss(true).build());
    style.animation(727).attackSpeed(5);
    style.targetGraphic(new Graphic(1454, 124));
    style.projectile(NpcCombatProjectile.id(384));
    knightCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(27).splashOnMiss(true).build());
    style.animation(1979).attackSpeed(5);
    style.targetGraphic(new Graphic(363));
    style.effect(NpcCombatEffect.builder().bind(16).build());
    knightCombat.style(style.build());

    var dragonCombat = NpcCombatDefinition.builder();
    dragonCombat.id(NpcId.REVENANT_DRAGON_135);
    dragonCombat.spawn(NpcCombatSpawn.builder().respawnDelay(50).build());
    dragonCombat.hitpoints(NpcCombatHitpoints.total(140));
    dragonCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(106)
            .magicLevel(150)
            .rangedLevel(151)
            .defenceLevel(87)
            .bonus(BonusType.MELEE_ATTACK, 72)
            .bonus(BonusType.ATTACK_MAGIC, 61)
            .bonus(BonusType.ATTACK_RANGED, 60)
            .bonus(BonusType.DEFENCE_STAB, 201)
            .bonus(BonusType.DEFENCE_SLASH, 206)
            .bonus(BonusType.DEFENCE_CRUSH, 188)
            .bonus(BonusType.DEFENCE_MAGIC, 101)
            .bonus(BonusType.DEFENCE_RANGED, 197)
            .build());
    dragonCombat.aggression(NpcCombatAggression.builder().always(true).build());
    dragonCombat.focus(NpcCombatFocus.builder().keepWithinDistance(1).build());
    dragonCombat.killCount(NpcCombatKillCount.builder().asName("Revenant").build());
    dragonCombat
        .type(NpcCombatType.UNDEAD)
        .type(NpcCombatType.SPECTRAL)
        .deathAnimation(92)
        .blockAnimation(89);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(80).attackSpeed(5);
    dragonCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(6722).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(206));
    dragonCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MAGIC).weight(32).build());
    style.damage(NpcCombatDamage.builder().maximum(30).splashOnMiss(true).build());
    style.animation(6722).attackSpeed(5);
    style.targetGraphic(new Graphic(1454, 124));
    style.projectile(NpcCombatProjectile.id(384));
    dragonCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(30).splashOnMiss(true).build());
    style.animation(81).attackSpeed(5);
    style.targetGraphic(new Graphic(363));
    style.effect(NpcCombatEffect.builder().bind(16).build());
    dragonCombat.style(style.build());

    return Arrays.asList(
        impCombat.build(),
        goblinCombat.build(),
        pyrefiendCombat.build(),
        hobgoblinCombat.build(),
        cyclopsCombat.build(),
        hellhoundCombat.build(),
        demonCombat.build(),
        orkCombat.build(),
        darkBeastCombat.build(),
        knightCombat.build(),
        dragonCombat.build());
  }

  @Override
  public void tickStartHook() {
    healAttack();
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    var playerMultiplier = player.getCombat().getDropRateMultiplier(-1, npc.getId());
    var total = player.getPlugin(SlayerPlugin.class).isWildernessTask(npc) ? 2 : 1;
    for (var i = 0; i < total; i++) {
      Item item = null;
      var logDrop = false;
      var clampedLevel = Math.min(Math.max(1, npc.getDef().getCombatLevel()), 144);
      var chanceA = 2200.0 / Math.sqrt(clampedLevel);
      chanceA /= playerMultiplier;
      var chanceB = 15 + (StrictMath.pow(npc.getDef().getCombatLevel() + 60, 2) / 200);
      var selectedChance = PRandom.randomE((int) chanceA);
      if (selectedChance == 0) {
        logDrop = true;
        var roll = PRandom.randomI(player.getCombat().getPKSkullDelay() > 0 ? 26 : 39);
        if (roll >= 0 && roll <= 1) {
          item = RandomItem.getItem(UNIQUE_DROP_TABLE);
        } else if (roll == 2) {
          item = new Item(ItemId.ANCIENT_RELIC);
        } else if (roll == 3) {
          item = new Item(ItemId.ANCIENT_EFFIGY);
        } else if (roll >= 4 && roll <= 5) {
          item = new Item(ItemId.ANCIENT_MEDALLION);
        } else if (roll >= 6 && roll <= 8) {
          item = new Item(ItemId.ANCIENT_STATUETTE);
        } else if (roll >= 9 && roll <= 12) {
          item = new Item(ItemId.MAGIC_SEED, 2 + PRandom.randomI(2));
        } else if (roll >= 13 && roll <= 15) {
          item = new Item(ItemId.ANCIENT_CRYSTAL);
        } else if (roll >= 16 && roll <= 20) {
          item = new Item(ItemId.ANCIENT_TOTEM);
        } else if (roll >= 21 && roll <= 26) {
          item = new Item(ItemId.ANCIENT_EMBLEM);
        } else if (roll >= 27 && roll <= 39) {
          item = new Item(ItemId.DRAGON_MED_HELM);
        }
      } else if (selectedChance < chanceB) {
        item = RandomItem.getItem(MEDIOCRE_DROP_TABLE);
      } else if (selectedChance < 3_000) {
        item = new Item(ItemId.COINS, PRandom.randomI(1_000, 10_000));
      }
      if (player.getEquipment().getNeckId() == ItemId.AMULET_OF_AVARICE) {
        item = new Item(item.getNotedId(), item);
      }
      if (item != null) {
        npc.getController().addMapItem(item, dropTile, player);
        if (logDrop) {
          player
              .getCombat()
              .logNPCItem(
                  npc.getCombatDef().getKillCountName(npc.getId()), item.getId(), item.getAmount());
          npc.getWorld()
              .sendRevenantCavesMessage(
                  "<col=005500>"
                      + player.getUsername()
                      + " received a drop: "
                      + (item.getAmount() > 1 ? item.getAmount() + " x " : "")
                      + item.getName());
        }
      }
      var etherCount = (1 + PRandom.randomE((int) Math.sqrt(clampedLevel))) * 2;
      if (player.getCharges().getEthereumAutoAbsorb()
          && (player.getEquipment().getHandId() == ItemId.BRACELET_OF_ETHEREUM
              || player.getEquipment().getHandId() == ItemId.BRACELET_OF_ETHEREUM_UNCHARGED)) {
        etherCount -=
            player
                .getCharges()
                .charge(
                    player.getEquipment().getHandItem(),
                    ItemId.BRACELET_OF_ETHEREUM,
                    etherCount,
                    new Item(ItemId.REVENANT_ETHER),
                    1);
      }
      if (etherCount > 0) {
        npc.getController()
            .addMapItem(new Item(ItemId.REVENANT_ETHER, etherCount), dropTile, player);
      }
    }
  }

  @Override
  public double damageInflictedHook(NpcCombatStyle combatStyle, Entity opponent, double damage) {
    if (!opponent.isPlayer()) {
      return damage;
    }
    var player = opponent.asPlayer();
    if (player.getCharges().degradeItems(false, ItemId.BRACELET_OF_ETHEREUM, false, 0)) {
      damage *= 0.25;
    }
    return damage;
  }

  @Override
  public boolean canBeAggressiveHook(Entity opponent) {
    return !(opponent instanceof Player)
        || ((Player) opponent).getEquipment().getHandId() != ItemId.BRACELET_OF_ETHEREUM;
  }

  public void healAttack() {
    if (npc.isLocked()) {
      return;
    }
    if (getHitpoints() >= getMaxHitpoints() / 2) {
      return;
    }
    if (isHitDelayed()) {
      return;
    }
    if (PRandom.randomE(4) != 0) {
      return;
    }
    changeHitpoints((int) (getMaxHitpoints() * 0.2));
    setHitDelay(getHitDelay() + 2);
    npc.setGraphic(1221);
  }
}
