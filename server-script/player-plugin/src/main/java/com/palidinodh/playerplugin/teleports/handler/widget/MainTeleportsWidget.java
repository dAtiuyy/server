package com.palidinodh.playerplugin.teleports.handler.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.playerplugin.teleports.MainTeleportCategoryType;
import com.palidinodh.playerplugin.teleports.MainTeleportType;
import com.palidinodh.playerplugin.teleports.MainTeleportsSubPlugin;
import com.palidinodh.playerplugin.teleports.TeleportsPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.TELEPORTS_1028)
class MainTeleportsWidget implements WidgetHandler {

  @Override
  public void onOpen(Player player) {
    var plugin = player.getPlugin(TeleportsPlugin.class).getMainTeleports();
    plugin.getSearch().clear();
    var category = MainTeleportCategoryType.CITIES;
    if (!plugin.getFavorites().isEmpty()) {
      category = MainTeleportCategoryType.FAVORITES;
    }
    var locations = category.getLocations(player);
    var location = locations.isEmpty() ? null : locations.get(0);
    plugin.setViewingCategory(category);
    plugin.setViewingLocation(location);
    plugin.sendCategories(true);
    plugin.sendLocations();
    plugin.sendDescription();
    plugin.sendHome();
    plugin.sendRecent();
    plugin.sendSettings();
  }

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    var plugin = player.getPlugin(TeleportsPlugin.class).getMainTeleports();
    var hasSearched = !plugin.getSearch().isEmpty();
    var maxChildId =
        MainTeleportsSubPlugin.WIDGET_CATEGORY_ENTRY_ID
            + MainTeleportsSubPlugin.WIDGET_CATEGORY_COUNT
                * MainTeleportsSubPlugin.WIDGET_CATEGORY_ENTRY_SIZE;
    if (childId >= MainTeleportsSubPlugin.WIDGET_CATEGORY_ENTRY_ID && childId <= maxChildId) {
      var categories = MainTeleportCategoryType.values();
      var entryId =
          (childId - MainTeleportsSubPlugin.WIDGET_CATEGORY_ENTRY_ID)
              / MainTeleportsSubPlugin.WIDGET_CATEGORY_ENTRY_SIZE;
      if (!hasSearched && entryId >= MainTeleportCategoryType.SEARCH.ordinal()) {
        entryId++;
      }
      if (plugin.getFavorites().isEmpty()
          && entryId >= MainTeleportCategoryType.FAVORITES.ordinal()) {
        entryId++;
      }
      if (entryId < 0 || entryId >= categories.length) {
        return;
      }
      var category = categories[entryId];
      if (category != MainTeleportCategoryType.SEARCH) {
        plugin.getSearch().clear();
      }
      var locations = category.getLocations(player);
      var location = locations.isEmpty() ? null : locations.get(0);
      plugin.setViewingCategory(category);
      plugin.setViewingLocation(location);
      plugin.sendCategories(hasSearched);
      plugin.sendLocations();
      plugin.sendDescription();
      return;
    }

    maxChildId =
        MainTeleportsSubPlugin.WIDGET_LOCATION_ENTRY_ID
            + MainTeleportsSubPlugin.WIDGET_LOCATION_COUNT
                * MainTeleportsSubPlugin.WIDGET_LOCATION_ENTRY_SIZE;
    if (childId >= MainTeleportsSubPlugin.WIDGET_LOCATION_ENTRY_ID && childId <= maxChildId) {
      var category = plugin.getViewingCategory();
      if (category == null) {
        return;
      }
      var locations = category.getLocations(player);
      if (locations == null || locations.isEmpty()) {
        return;
      }
      var entryId =
          (childId - MainTeleportsSubPlugin.WIDGET_LOCATION_ENTRY_ID)
              / MainTeleportsSubPlugin.WIDGET_LOCATION_ENTRY_SIZE;
      if (entryId < 0 || entryId >= locations.size()) {
        return;
      }
      var location = locations.get(entryId);
      plugin.setViewingLocation(location);
      plugin.sendDescription();
      switch (option) {
        case 0:
        case 1:
          {
            if (plugin.isQuickTeleportSetting() || option == 1) {
              plugin.selectTeleport(location);
            }
            break;
          }
        case 2:
          plugin.setFavorite(location);
          break;
      }
      return;
    }

    maxChildId =
        MainTeleportsSubPlugin.WIDGET_RECENT_ENTRY_ID
            + MainTeleportsSubPlugin.WIDGET_RECENT_COUNT
                * MainTeleportsSubPlugin.WIDGET_RECENT_ENTRY_SIZE;
    if (childId >= MainTeleportsSubPlugin.WIDGET_RECENT_ENTRY_ID && childId <= maxChildId) {
      var entryId =
          (childId - MainTeleportsSubPlugin.WIDGET_RECENT_ENTRY_ID)
              / MainTeleportsSubPlugin.WIDGET_RECENT_ENTRY_SIZE;
      var location = plugin.getRecent(entryId);
      plugin.setViewingLocation(location);
      plugin.sendDescription();
      switch (option) {
        case 0:
        case 1:
          {
            if (plugin.isQuickTeleportSetting() || option == 1) {
              plugin.selectTeleport(location);
            }
            break;
          }
        case 2:
          player.getGameEncoder().sendMessage("Home configuring TBD.");
          break;
      }
      return;
    }

    switch (childId) {
      case MainTeleportsSubPlugin.WIDGET_DEATHS_OFFICE_ID:
        plugin.selectTeleport(MainTeleportType.DEATHS_OFFICE);
        break;
      case MainTeleportsSubPlugin.WIDGET_SEARCH_ID:
        {
          player
              .getGameEncoder()
              .sendEnterString(
                  "Search Location, Monsters, or Items:",
                  ie -> {
                    var results = MainTeleportType.getSearchResults(ie);
                    var location = results.isEmpty() ? null : results.get(0);
                    plugin.setViewingCategory(MainTeleportCategoryType.SEARCH);
                    plugin.setViewingLocation(location);
                    plugin.getSearch().clear();
                    plugin.getSearch().addAll(results);
                    plugin.sendCategories(true);
                    plugin.sendLocations();
                    plugin.sendDescription();
                    player.sendDiscordNewAccountLog("Teleport Search: " + ie);
                  });
          break;
        }
      case MainTeleportsSubPlugin.WIDGET_DESCRIPTION_TELEPORT_OPTION_ID:
        {
          var location = plugin.getViewingLocation();
          switch (option) {
            case 0:
              plugin.selectTeleport(location);
              break;
            case 1:
              plugin.setFavorite(location);
              break;
          }
          break;
        }
      case MainTeleportsSubPlugin.WIDGET_DEFAULT_HOME_ID:
        plugin.selectTeleport(MainTeleportType.DEFAULT_HOME);
        break;
      case MainTeleportsSubPlugin.WIDGET_HOME_OPTION_ID:
        {
          var location = plugin.getHomeTeleport();
          plugin.setViewingLocation(location);
          plugin.sendDescription();
          switch (option) {
            case 0:
            case 1:
              {
                if (plugin.isQuickTeleportSetting() || option == 1) {
                  plugin.selectTeleport(location);
                }
                break;
              }
            case 2:
              plugin.setFavorite(location);
              break;
          }
          break;
        }
      case MainTeleportsSubPlugin.WIDGET_HOME_TELEPORT_SETTING_ENTRY_ID:
        plugin.setHomeTeleportSetting(!plugin.isHomeTeleportSetting());
        break;
      case MainTeleportsSubPlugin.WIDGET_QUICK_TELEPORT_SETTING_ENTRY_ID:
        plugin.setQuickTeleportSetting(!plugin.isQuickTeleportSetting());
        break;
      case MainTeleportsSubPlugin.WIDGET_WILD_WARNING_SETTING_ENTRY_ID:
        plugin.setWildWarningSetting(!plugin.isWildWarningSetting());
        break;
    }
  }
}
