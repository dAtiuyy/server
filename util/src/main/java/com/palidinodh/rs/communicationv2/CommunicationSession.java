package com.palidinodh.rs.communicationv2;

import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.nio.Session;
import com.palidinodh.nio.WriteEventHandler;
import com.palidinodh.rs.communicationv2.event.AuthEvent;
import lombok.Getter;
import lombok.Setter;

@Getter
public class CommunicationSession {

  private Session session;
  private Stream in = new Stream(32_768);
  private Stream out = new Stream(32_768);
  @Setter private AuthEvent authEvent;

  public CommunicationSession(Session session) {
    this.session = session;
  }

  public void write() {
    write(null);
  }

  public void write(WriteEventHandler handler) {
    if (out.getPosition() > 0 && session.isOpen()) {
      session.write(out.toByteArray(), handler);
    }
    out.clear();
  }

  public void close() {
    session.close();
  }

  public boolean isOpen() {
    return session.isOpen();
  }

  public boolean isAuthenticated() {
    return authEvent != null && authEvent.isAuthenticated();
  }

  public int getWorldId() {
    return authEvent.getId();
  }
}
