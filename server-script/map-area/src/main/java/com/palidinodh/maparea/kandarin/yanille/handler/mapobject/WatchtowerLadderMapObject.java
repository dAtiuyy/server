package com.palidinodh.maparea.kandarin.yanille.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ObjectId.LADDER_2833, ObjectId.LADDER_17122})
class WatchtowerLadderMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.LADDER_2833:
        player.getMovement().ladderUpTeleport(new Tile(2544, 3112, 1));
        break;
      case ObjectId.LADDER_17122:
        player.getMovement().ladderDownTeleport(new Tile(2544, 3112));
        break;
    }
  }
}
