package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class GiantSquirrelPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(ItemId.GIANT_SQUIRREL, NpcId.GIANT_SQUIRREL, NpcId.GIANT_SQUIRREL_7351));
    return builder;
  }
}
