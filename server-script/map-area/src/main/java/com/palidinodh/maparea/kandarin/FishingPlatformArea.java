package com.palidinodh.maparea.kandarin;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(11059)
public class FishingPlatformArea extends Area {}
