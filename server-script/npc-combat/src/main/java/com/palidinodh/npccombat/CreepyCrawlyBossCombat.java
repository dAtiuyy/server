package com.palidinodh.npccombat;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatMulti;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.graphic.Graphic;
import java.util.Arrays;
import java.util.List;

class CreepyCrawlyBossCombat extends NpcCombat {

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var xarpusCombat = NpcCombatDefinition.builder();
    xarpusCombat.id(NpcId.XARPUS_16038);
    xarpusCombat.hitpoints(
        NpcCombatHitpoints.builder().total(255).barType(HitpointsBarType.GREEN_RED_120).build());
    xarpusCombat.stats(
        NpcCombatStats.builder().rangedLevel(200).bonus(BonusType.ATTACK_RANGED, 50).build());
    xarpusCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    xarpusCombat.deathAnimation(8063);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(16));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(8059).attackSpeed(4);
    style.targetGraphic(new Graphic(1556));
    style.projectile(NpcCombatProjectile.id(1555));
    xarpusCombat.style(style.build());

    var maidenCombat = NpcCombatDefinition.builder();
    maidenCombat.id(NpcId.THE_MAIDEN_OF_SUGADINTI_16039);
    maidenCombat.hitpoints(
        NpcCombatHitpoints.builder().total(255).barType(HitpointsBarType.GREEN_RED_120).build());
    maidenCombat.stats(
        NpcCombatStats.builder().magicLevel(200).bonus(BonusType.ATTACK_MAGIC, 50).build());
    maidenCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    maidenCombat.focus(NpcCombatFocus.builder().disableFollowingOpponent(true).build());
    maidenCombat.deathAnimation(8093);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(16));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(8092).attackSpeed(4);
    style.targetGraphic(new Graphic(1579));
    style.projectile(NpcCombatProjectile.id(1578));
    maidenCombat.style(style.build());

    var sarachnisCombat = NpcCombatDefinition.builder();
    sarachnisCombat.id(NpcId.SARACHNIS_16040);
    sarachnisCombat.hitpoints(
        NpcCombatHitpoints.builder().total(255).barType(HitpointsBarType.GREEN_RED_120).build());
    sarachnisCombat.stats(
        NpcCombatStats.builder().attackLevel(200).bonus(BonusType.MELEE_ATTACK, 50).build());
    sarachnisCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    sarachnisCombat.deathAnimation(8318);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_STAB);
    style.damage(NpcCombatDamage.maximum(16));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(8339).attackSpeed(4);
    sarachnisCombat.style(style.build());

    var theNightmareCombat = NpcCombatDefinition.builder();
    theNightmareCombat.id(NpcId.THE_NIGHTMARE_16041);
    theNightmareCombat.hitpoints(
        NpcCombatHitpoints.builder().total(255).barType(HitpointsBarType.GREEN_RED_120).build());
    theNightmareCombat.stats(
        NpcCombatStats.builder().attackLevel(200).bonus(BonusType.MELEE_ATTACK, 50).build());
    theNightmareCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    theNightmareCombat.deathAnimation(8612);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(16));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(8594).attackSpeed(4);
    theNightmareCombat.style(style.build());

    var vasaNistirioCombat = NpcCombatDefinition.builder();
    vasaNistirioCombat.id(NpcId.VASA_NISTIRIO_16042);
    vasaNistirioCombat.hitpoints(
        NpcCombatHitpoints.builder().total(255).barType(HitpointsBarType.GREEN_RED_120).build());
    vasaNistirioCombat.stats(
        NpcCombatStats.builder().rangedLevel(200).bonus(BonusType.ATTACK_RANGED, 50).build());
    vasaNistirioCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    vasaNistirioCombat.deathAnimation(7415);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(16));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(7410).attackSpeed(4);
    style.targetGraphic(new Graphic(1330));
    style.projectile(NpcCombatProjectile.id(1329));
    vasaNistirioCombat.style(style.build());

    var ranisDrakanCombat = NpcCombatDefinition.builder();
    ranisDrakanCombat.id(NpcId.RANIS_DRAKAN_16043);
    ranisDrakanCombat.hitpoints(
        NpcCombatHitpoints.builder().total(255).barType(HitpointsBarType.GREEN_RED_120).build());
    ranisDrakanCombat.stats(
        NpcCombatStats.builder().attackLevel(200).bonus(BonusType.MELEE_ATTACK, 50).build());
    ranisDrakanCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    ranisDrakanCombat.deathAnimation(8033);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(16));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(8034).attackSpeed(4);
    ranisDrakanCombat.style(style.build());

    var porazdirCombat = NpcCombatDefinition.builder();
    porazdirCombat.id(NpcId.PORAZDIR_16044);
    porazdirCombat.hitpoints(
        NpcCombatHitpoints.builder().total(255).barType(HitpointsBarType.GREEN_RED_120).build());
    porazdirCombat.stats(
        NpcCombatStats.builder().magicLevel(200).bonus(BonusType.ATTACK_MAGIC, 50).build());
    porazdirCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    porazdirCombat.deathAnimation(7843);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(16));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(7841).attackSpeed(4);
    style.targetGraphic(new Graphic(78));
    porazdirCombat.style(style.build());

    var corruptedHunllefCombat = NpcCombatDefinition.builder();
    corruptedHunllefCombat.id(NpcId.CORRUPTED_HUNLLEF_16045);
    corruptedHunllefCombat.hitpoints(
        NpcCombatHitpoints.builder().total(255).barType(HitpointsBarType.GREEN_RED_120).build());
    corruptedHunllefCombat.stats(
        NpcCombatStats.builder().magicLevel(200).bonus(BonusType.ATTACK_MAGIC, 50).build());
    corruptedHunllefCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    corruptedHunllefCombat.deathAnimation(8421);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(16));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(8418).attackSpeed(4);
    style.targetGraphic(new Graphic(1718));
    corruptedHunllefCombat.style(style.build());

    var fragmentOfSerenCombat = NpcCombatDefinition.builder();
    fragmentOfSerenCombat.id(NpcId.FRAGMENT_OF_SEREN_16046);
    fragmentOfSerenCombat.hitpoints(
        NpcCombatHitpoints.builder().total(255).barType(HitpointsBarType.GREEN_RED_120).build());
    fragmentOfSerenCombat.stats(
        NpcCombatStats.builder().rangedLevel(200).bonus(BonusType.ATTACK_RANGED, 50).build());
    fragmentOfSerenCombat.aggression(NpcCombatAggression.builder().range(20).always(true).build());
    fragmentOfSerenCombat.deathAnimation(8384);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(16));
    style.multiCombat(NpcCombatMulti.WITHIN_ATTACK_RANGE);
    style.animation(8376).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(1700));
    fragmentOfSerenCombat.style(style.build());

    return Arrays.asList(
        xarpusCombat.build(),
        maidenCombat.build(),
        sarachnisCombat.build(),
        theNightmareCombat.build(),
        vasaNistirioCombat.build(),
        ranisDrakanCombat.build(),
        porazdirCombat.build(),
        corruptedHunllefCombat.build(),
        fragmentOfSerenCombat.build());
  }
}
