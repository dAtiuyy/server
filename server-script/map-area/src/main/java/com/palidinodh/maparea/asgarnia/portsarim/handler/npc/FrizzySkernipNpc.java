package com.palidinodh.maparea.asgarnia.portsarim.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.FRIZZY_SKERNIP)
class FrizzySkernipNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    if (option.getIndex() == 0) {
      player.getGameEncoder().sendMessage("This gardener will protect your patches for a fee.");
    } else {
      player.openOptionsDialogue(
          new DialogueOption(
              "Pay for patch protection!",
              (c, s) -> {
                player.getFarming().gardenerProtection(npc, option.getIndex() - 2);
              }),
          new DialogueOption(
              "Pay for digging up tree!",
              (c, s) -> {
                player.getFarming().gardenerRemoval(npc, option.getIndex() - 2);
              }));
    }
  }
}
