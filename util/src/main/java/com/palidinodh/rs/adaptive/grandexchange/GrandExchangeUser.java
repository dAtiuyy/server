package com.palidinodh.rs.adaptive.grandexchange;

import com.palidinodh.rs.adaptive.RsGameMode;
import com.palidinodh.util.PString;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GrandExchangeUser implements Serializable {

  public static final int HISTORY_RECENT = 1,
      HISTORY_RECENT_BUY = 2,
      HISTORY_RECENT_SELL = 3,
      HISTORY_RANDOM = 4,
      HISTORY_RANDOM_BUY = 5,
      HISTORY_RANDOM_SELL = 6;
  public static final int HISTORY_SIZE = 20;
  private static final long serialVersionUID = 12212016;
  private int userId;
  private String username;
  private int gameMode;
  private List<GrandExchangeItem> items = new ArrayList<>();

  public GrandExchangeUser(int userId) {
    this.userId = userId;
  }

  public GrandExchangeItem getItem(int slot) {
    return slot >= 0 && slot < items.size() ? items.get(slot) : null;
  }

  public void addItem(GrandExchangeItem item) {
    items.add(item);
  }

  public void abortItem(int slot) {
    if (slot < 0 || slot >= items.size()) {
      return;
    }
    items.get(slot).setAborted(true);
  }

  public void collect(int slot, int amount, int price) {
    if (slot < 0 || slot >= items.size()) {
      return;
    }
    items.get(slot).collected(amount, price);
    if (items.get(slot).canRemove()) {
      items.remove(slot);
    }
  }

  public GrandExchangeItem getRecentItem(int state) {
    GrandExchangeItem grandExchangeItem = null;
    for (GrandExchangeItem grandExchangeItem2 : items) {
      if (grandExchangeItem2 != null) {
        if (state == -1 || grandExchangeItem2.getState() == state) {
          if (grandExchangeItem == null
              || grandExchangeItem.getCreation() < grandExchangeItem2.getCreation()) {
            grandExchangeItem = grandExchangeItem2;
          }
        }
      }
    }
    return grandExchangeItem;
  }

  public GrandExchangeItem getItemFromId(int itemId) {
    for (GrandExchangeItem item : items) {
      if (item != null && item.isStateSelling() && item.getId() == itemId) {
        return item;
      }
    }
    return null;
  }

  public GrandExchangeItem getItemFromId(int itemId, int state) {
    for (GrandExchangeItem item : items) {
      if (item == null) {
        continue;
      }
      if (item.getId() != itemId) {
        continue;
      }
      if (item.getState() != state) {
        continue;
      }
      if (item.isAborted()) {
        continue;
      }
      if (item.getRemainingAmount() == 0) {
        continue;
      }
      return item;
    }
    return null;
  }

  public GrandExchangeItem getItemFromName(String itemName, int state) {
    for (GrandExchangeItem item : items) {
      if (item == null) {
        continue;
      }
      if (item.getName() == null || item.getName().isEmpty()) {
        continue;
      }
      if (!PString.cleanName(item.getName().replace(" ", "").toLowerCase()).contains(itemName)) {
        continue;
      }
      if (item.getState() != state) {
        continue;
      }
      if (item.isAborted()) {
        continue;
      }
      if (item.getRemainingAmount() == 0) {
        continue;
      }
      return item;
    }
    return null;
  }

  public int getBuyCount() {
    int count = 0;
    for (GrandExchangeItem item : items) {
      if (item != null
          && !item.isAborted()
          && item.getRemainingAmount() > 0
          && item.isStateBuying()) {
        count++;
      }
    }
    return count;
  }

  public int getSellCount() {
    int count = 0;
    for (GrandExchangeItem item : items) {
      if (item != null
          && !item.isAborted()
          && item.getRemainingAmount() > 0
          && item.isStateSelling()) {
        count++;
      }
    }
    return count;
  }

  public void expireItems() {
    for (GrandExchangeItem item : items) {
      if (item != null && item.isExpired()) {
        item.setAborted(true);
      }
    }
  }

  public int getUserId() {
    return userId;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public int getGameMode() {
    return gameMode;
  }

  public void setGameMode(int gameMode) {
    this.gameMode = gameMode;
  }

  public boolean isGameModeRegular() {
    return gameMode == RsGameMode.REGULAR.ordinal();
  }

  public boolean isGameModeDeadman() {
    return gameMode == RsGameMode.DEADMAN.ordinal();
  }

  public List<GrandExchangeItem> getItems() {
    return items;
  }

  public void setItems(List<GrandExchangeItem> items) {
    this.items = items;
  }
}
