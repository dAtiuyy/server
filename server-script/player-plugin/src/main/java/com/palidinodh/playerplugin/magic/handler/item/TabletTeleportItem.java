package com.palidinodh.playerplugin.magic.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.magic.SpellTeleport;
import com.palidinodh.rs.communication.log.PlayerLogType;
import com.palidinodh.rs.reference.ReferenceId;
import java.util.HashMap;
import java.util.Map;

@ReferenceId({
  ItemId.ANNAKARL_TELEPORT,
  ItemId.ARDOUGNE_TELEPORT,
  ItemId.CAMELOT_TELEPORT,
  ItemId.CARRALLANGAR_TELEPORT,
  ItemId.DAREEYAK_TELEPORT,
  ItemId.FALADOR_TELEPORT,
  ItemId.GHORROCK_TELEPORT,
  ItemId.HARMONY_ISLAND_TELEPORT,
  ItemId.KHARYRLL_TELEPORT,
  ItemId.LUMBRIDGE_TELEPORT,
  ItemId.PADDEWWA_TELEPORT,
  ItemId.RELLEKKA_TELEPORT,
  ItemId.TAVERLEY_TELEPORT,
  ItemId.TELEPORT_TO_HOUSE,
  ItemId.TROLLHEIM_TELEPORT,
  ItemId.VARROCK_TELEPORT
})
class TabletTeleportItem implements ItemHandler {

  private static final Map<Integer, Tile> TELEPORTS = new HashMap<>();

  static {
    TELEPORTS.put(ItemId.ANNAKARL_TELEPORT, new Tile(3290, 3886));
    TELEPORTS.put(ItemId.ARDOUGNE_TELEPORT, new Tile(2664, 3306));
    TELEPORTS.put(ItemId.CAMELOT_TELEPORT, new Tile(2725, 3485));
    TELEPORTS.put(ItemId.CARRALLANGAR_TELEPORT, new Tile(3175, 3669));
    TELEPORTS.put(ItemId.DAREEYAK_TELEPORT, new Tile(2968, 3695));
    TELEPORTS.put(ItemId.FALADOR_TELEPORT, new Tile(2965, 3379));
    TELEPORTS.put(ItemId.GHORROCK_TELEPORT, new Tile(2974, 3873));
    TELEPORTS.put(ItemId.HARMONY_ISLAND_TELEPORT, new Tile(3797, 2866));
    TELEPORTS.put(ItemId.KHARYRLL_TELEPORT, new Tile(3510, 3480));
    TELEPORTS.put(ItemId.LUMBRIDGE_TELEPORT, new Tile(3221, 3218));
    TELEPORTS.put(ItemId.PADDEWWA_TELEPORT, new Tile(3094, 3470));
    TELEPORTS.put(ItemId.RELLEKKA_TELEPORT, new Tile(2670, 3632));
    TELEPORTS.put(ItemId.TAVERLEY_TELEPORT, new Tile(2894, 3465));
    TELEPORTS.put(ItemId.TELEPORT_TO_HOUSE, null);
    TELEPORTS.put(ItemId.TROLLHEIM_TELEPORT, new Tile(2829, 3685));
    TELEPORTS.put(ItemId.VARROCK_TELEPORT, new Tile(3212, 3429));
  }

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var tile = TELEPORTS.get(item.getId());
    if (tile == null) {
      tile = player.getWidgetManager().getHomeTile();
      if (player.getArea().inPvpWorld()) {
        tile = new Tile(3093, 3510);
      }
    }
    var height = tile.getHeight();
    if (player.getClientHeight() == 0
        && Area.inWilderness(tile)
        && (player.inEdgeville() || player.getArea().inWilderness())) {
      height = player.getHeight();
    }
    tile = new Tile(tile.getX(), tile.getY(), height);
    if (!player.getController().canTeleport(tile, true)
        || !player.getController().canTeleport(true)) {
      return;
    }
    item.remove(1);
    SpellTeleport.teleport(player, SpellTeleport.TeleportStyle.TABLET, tile);
    player.log(PlayerLogType.TELEPORT, "teleported using " + item.getDef().getName());
    player.sendDiscordNewAccountLog("Teleport: " + item.getDef().getName());
  }
}
