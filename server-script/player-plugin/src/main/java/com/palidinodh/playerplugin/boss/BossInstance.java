package com.palidinodh.playerplugin.boss;

import com.palidinodh.cache.definition.osrs.NpcDefinition;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.MessageDialogue;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.sisterhoodsanctuary.SisterhoodSanctuaryPlugin;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;

@Getter
public class BossInstance {

  private static final Map<Integer, BossInstance> INSTANCES;

  static {
    var instances = new HashMap<Integer, BossInstance>();

    var instance = new BossInstance();
    instance.playerTile = new Tile(2974, 4384, 2);
    instance.spawns = new ArrayList<>();
    instance.spawns.add(new NpcSpawn(8, new Tile(2986, 4381, 2), NpcId.CORPOREAL_BEAST_785));
    instances.put(NpcId.CORPOREAL_BEAST_785, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(2900, 4449);
    instance.teleportType = TeleportType.LADDER_UP;
    instance.spawns.add(new NpcSpawn(4, new Tile(2920, 4441), NpcId.DAGANNOTH_REX_303));
    instance.spawns.add(new NpcSpawn(4, new Tile(2912, 4455), NpcId.DAGANNOTH_PRIME_303));
    instance.spawns.add(new NpcSpawn(4, new Tile(2906, 4441), NpcId.DAGANNOTH_SUPREME_303));
    instances.put(NpcId.DAGANNOTH_REX_303, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(2271, 4680);
    instance.spawns.add(new NpcSpawn(16, new Tile(2269, 4696), NpcId.KING_BLACK_DRAGON_276));
    instances.put(NpcId.KING_BLACK_DRAGON_276, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(1304, 1291);
    instance.taskOnly = true;
    instance.spawns.add(new NpcSpawn(new Tile(1302, 1314), NpcId.CERBERUS_318));
    instances.put(NpcId.CERBERUS_318, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(2376, 9452);
    instance.teleportType = TeleportType.LADDER_DOWN;
    instance.taskOnly = true;
    instance.spawns.add(new NpcSpawn(8, new Tile(2363, 9449), NpcId.THERMONUCLEAR_SMOKE_DEVIL_301));
    instance.spawns.add(new NpcSpawn(8, new Tile(2371, 9452), NpcId.SMOKE_DEVIL_160));
    instance.spawns.add(new NpcSpawn(8, new Tile(2357, 9454), NpcId.SMOKE_DEVIL_160));
    instance.spawns.add(new NpcSpawn(8, new Tile(2356, 9445), NpcId.SMOKE_DEVIL_160));
    instance.spawns.add(new NpcSpawn(8, new Tile(2363, 9443), NpcId.SMOKE_DEVIL_160));
    instance.spawns.add(new NpcSpawn(8, new Tile(2370, 9444), NpcId.SMOKE_DEVIL_160));
    instance.spawns.add(new NpcSpawn(8, new Tile(2366, 9455), NpcId.SMOKE_DEVIL_160));
    instances.put(NpcId.THERMONUCLEAR_SMOKE_DEVIL_301, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(1752, 5236);
    instance.teleportType = TeleportType.LADDER_DOWN;
    instance.startEvent =
        instance.startEventClanChat =
            p -> {
              if (!p.getInventory().hasItem(ItemId.FALADOR_SHIELD_3)
                  && p.getInventory().hasItem(ItemId.FALADOR_SHIELD_4)) {
                return;
              }
              var event =
                  new PEvent(10) {
                    @Override
                    public void execute() {
                      if (!p.isVisible()) {
                        stop();
                        return;
                      }
                      if (!p.inMoleLair()) {
                        stop();
                        p.getGameEncoder().sendHintIconReset();
                        return;
                      }
                      var mole = p.getController().getNpc(NpcId.GIANT_MOLE_230);
                      if (mole == null || !mole.isVisible()) {
                        p.getGameEncoder().sendHintIconReset();
                      } else if (p.withinVisibilityDistance(mole)) {
                        p.getGameEncoder().sendHintIconNpc(mole.getIndex());
                      } else {
                        p.getGameEncoder().sendHintIconTile(mole);
                      }
                    }
                  };
              p.getWorld().addEvent(event);
            };
    instance.spawns.add(new NpcSpawn(64, new Tile(1759, 5184), NpcId.GIANT_MOLE_230));
    instances.put(NpcId.GIANT_MOLE_230, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(2839, 5296, 2);
    instance.canStartEvent =
        instance.canStartEventClanChat =
            (p, c) -> {
              if (!p.getArea().is("GodWarsDungeon")) {
                return false;
              }
              if (p.getArea().script("has_armadyl_killcount") == Boolean.FALSE) {
                p.getGameEncoder().sendMessage("You need 40 killcount to enter.");
                return false;
              }
              return true;
            };
    instance.startEvent =
        instance.startEventClanChat =
            p -> {
              if (!p.getArea().is("GodWarsDungeon")) {
                return;
              }
              p.getArea().script("clear_armadyl_killcount");
            };
    instance.spawns.add(new NpcSpawn(6, new Tile(2831, 5302, 2), NpcId.KREEARRA_580));
    instance.spawns.add(new NpcSpawn(6, new Tile(2840, 5302, 2), NpcId.WINGMAN_SKREE_143));
    instance.spawns.add(new NpcSpawn(6, new Tile(2827, 5299, 2), NpcId.FLOCKLEADER_GEERIN_149));
    instance.spawns.add(new NpcSpawn(6, new Tile(2832, 5297, 2), NpcId.FLIGHT_KILISA_159));
    instances.put(NpcId.KREEARRA_580, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(2864, 5354, 2);
    instance.canStartEvent =
        instance.canStartEventClanChat =
            (p, c) -> {
              if (!p.getArea().is("GodWarsDungeon")) {
                return false;
              }
              if (p.getArea().script("has_bandos_killcount") == Boolean.FALSE) {
                p.getGameEncoder().sendMessage("You need 40 killcount to enter.");
                return false;
              }
              return true;
            };
    instance.startEvent =
        instance.startEventClanChat =
            p -> {
              if (!p.getArea().is("GodWarsDungeon")) {
                return;
              }
              p.getArea().script("clear_bandos_killcount");
            };
    instance.spawns.add(new NpcSpawn(6, new Tile(2872, 5358, 2), NpcId.GENERAL_GRAARDOR_624));
    instance.spawns.add(new NpcSpawn(6, new Tile(2866, 5358, 2), NpcId.SERGEANT_STRONGSTACK_141));
    instance.spawns.add(new NpcSpawn(6, new Tile(2873, 5353, 2), NpcId.SERGEANT_STEELWILL_142));
    instance.spawns.add(new NpcSpawn(6, new Tile(2868, 5362, 2), NpcId.SERGEANT_GRIMSPIKE_142));
    instances.put(NpcId.GENERAL_GRAARDOR_624, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(2925, 5331, 2);
    instance.canStartEvent =
        instance.canStartEventClanChat =
            (p, c) -> {
              if (!p.getArea().is("GodWarsDungeon")) {
                return false;
              }
              if (p.getArea().script("has_zamorak_killcount") == Boolean.FALSE) {
                p.getGameEncoder().sendMessage("You need 40 killcount to enter.");
                return false;
              }
              return true;
            };
    instance.startEvent =
        instance.startEventClanChat =
            p -> {
              if (!p.getArea().is("GodWarsDungeon")) {
                return;
              }
              p.getArea().script("clear_zamorak_killcount");
            };
    instance.spawns.add(new NpcSpawn(6, new Tile(2925, 5322, 2), NpcId.KRIL_TSUTSAROTH_650));
    instance.spawns.add(new NpcSpawn(6, new Tile(2932, 5328, 2), NpcId.TSTANON_KARLAK_145));
    instance.spawns.add(new NpcSpawn(6, new Tile(2919, 5327, 2), NpcId.ZAKLN_GRITCH_142));
    instance.spawns.add(new NpcSpawn(6, new Tile(2921, 5320, 2), NpcId.BALFRUG_KREEYATH_151));
    instances.put(NpcId.KRIL_TSUTSAROTH_650, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(2907, 5265);
    instance.canStartEvent =
        instance.canStartEventClanChat =
            (p, c) -> {
              if (!p.getArea().is("GodWarsDungeon")) {
                return false;
              }
              if (p.getArea().script("has_saradomin_killcount") == Boolean.FALSE) {
                p.getGameEncoder().sendMessage("You need 40 killcount to enter.");
                return false;
              }
              return true;
            };
    instance.startEvent =
        instance.startEventClanChat =
            p -> {
              if (!p.getArea().is("GodWarsDungeon")) {
                return;
              }
              p.getArea().script("clear_saradomin_killcount");
            };
    instance.spawns.add(new NpcSpawn(6, new Tile(2897, 5267), NpcId.COMMANDER_ZILYANA_596));
    instance.spawns.add(new NpcSpawn(6, new Tile(2903, 5261), NpcId.STARLIGHT_149));
    instance.spawns.add(new NpcSpawn(6, new Tile(2896, 5262), NpcId.GROWLER_139));
    instance.spawns.add(new NpcSpawn(6, new Tile(2902, 5272), NpcId.BREE_146));
    instances.put(NpcId.COMMANDER_ZILYANA_596, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(2983, 4820);
    instance.taskOnly = true;
    instance.spawns.add(new NpcSpawn(new Tile(2977, 4855), NpcId.ABYSSAL_SIRE_350));
    instances.put(NpcId.ABYSSAL_SIRE_350, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(2280, 10022);
    instance.teleportType = TeleportType.LADDER_DOWN;
    instance.taskOnly = true;
    instance.spawns.add(new NpcSpawn(new Tile(2278, 10035), NpcId.WHIRLPOOL));
    instances.put(NpcId.KRAKEN_291, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(3506, 9494);
    instance.teleportType = TeleportType.LADDER_DOWN;
    instance.spawns.add(new NpcSpawn(8, new Tile(3476, 9492), NpcId.KALPHITE_QUEEN_333));
    instance.spawns.add(new NpcSpawn(8, new Tile(3496, 9500), NpcId.KALPHITE_GUARDIAN_141_960));
    instance.spawns.add(new NpcSpawn(8, new Tile(3494, 9489), NpcId.KALPHITE_GUARDIAN_141_960));
    instance.spawns.add(new NpcSpawn(8, new Tile(3475, 9488), NpcId.KALPHITE_WORKER_28_956));
    instance.spawns.add(new NpcSpawn(8, new Tile(3483, 9486), NpcId.KALPHITE_WORKER_28_956));
    instance.spawns.add(new NpcSpawn(8, new Tile(3488, 9492), NpcId.KALPHITE_WORKER_28_956));
    instance.spawns.add(new NpcSpawn(8, new Tile(3495, 9496), NpcId.KALPHITE_WORKER_28_956));
    instance.spawns.add(new NpcSpawn(8, new Tile(3494, 9508), NpcId.KALPHITE_WORKER_28_956));
    instance.spawns.add(new NpcSpawn(8, new Tile(3485, 9513), NpcId.KALPHITE_WORKER_28_956));
    instance.spawns.add(new NpcSpawn(8, new Tile(3477, 9506), NpcId.KALPHITE_WORKER_28_956));
    instance.spawns.add(new NpcSpawn(8, new Tile(3471, 9498), NpcId.KALPHITE_WORKER_28_956));
    instances.put(NpcId.KALPHITE_QUEEN_333, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(2272, 4054);
    instance.spawns.add(new NpcSpawn(new Tile(2269, 4062), NpcId.VORKATH_8059));
    instances.put(NpcId.VORKATH_732, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(1356, 10258);
    instance.taskOnly = true;
    instance.spawns.add(new NpcSpawn(16, new Tile(1364, 10265), NpcId.ALCHEMICAL_HYDRA_426));
    instances.put(NpcId.ALCHEMICAL_HYDRA_426, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(1696, 4574);
    instance.taskOnly = true;
    instances.put(NpcId.DUSK_248, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(1366, 9367, 1);
    instance.spawns.add(
        new NpcSpawn(
            4,
            new Tile(1365, 9383, 1),
            NpcId.CRYSTALLINE_HUNLLEF_674,
            NpcId.CRYSTALLINE_HUNLLEF_674_9022,
            NpcId.CRYSTALLINE_HUNLLEF_674_9023));
    instances.put(NpcId.CRYSTALLINE_HUNLLEF_674, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = new Tile(1382, 9367, 1);
    instance.spawns.add(
        new NpcSpawn(
            4,
            new Tile(1381, 9383, 1),
            NpcId.CORRUPTED_HUNLLEF_894,
            NpcId.CORRUPTED_HUNLLEF_894_9036,
            NpcId.CORRUPTED_HUNLLEF_894_9037));
    instances.put(NpcId.CORRUPTED_HUNLLEF_894, instance);

    instance = new BossInstance();
    instance.spawns = new ArrayList<>();
    instance.playerTile = BossPlugin.VERZIK_ENTER_TILE;
    instance.spawns.add(BossPlugin.VERZIK_SPAWN);
    instances.put(NpcId.VERZIK_VITUR_1040, instance);

    instance = new BossInstance();
    instance.teleportType = TeleportType.NONE;
    instance.playerTile = new Tile(3870, 9948, 3);
    instance.alwaysUseController = true;
    instance.canStartEvent =
        (p, c) -> {
          if (SisterhoodSanctuaryPlugin.getIdleNightmare(p.getController()) != null) {
            p.openDialogue(
                new MessageDialogue(
                    "A group is already fighting the Nightmare. You'll have to wait until they are done."));
            return false;
          }
          return true;
        };
    instance.startEvent =
        p -> {
          if (p.getController().getNpc(NpcId.THE_NIGHTMARE_814_9432, NpcId.THE_NIGHTMARE_814_9433)
              == null) {
            p.getController().addNpc(SisterhoodSanctuaryPlugin.NIGHTMARE_SPAWN);
          }
          p.getPlugin(SisterhoodSanctuaryPlugin.class).enterTheNightmare();
        };
    instance.canStartEventClanChat =
        (p, c) -> {
          if (c != null && SisterhoodSanctuaryPlugin.getIdleNightmare(c) != null) {
            p.openDialogue(
                new MessageDialogue(
                    "This group is already fighting the Nightmare. You'll have to wait until they are done."));
            return false;
          }
          return true;
        };
    instance.startEventClanChat =
        p -> {
          if (p.getController().getNpc(NpcId.THE_NIGHTMARE_814_9432, NpcId.THE_NIGHTMARE_814_9433)
              == null) {
            p.getController().addNpc(SisterhoodSanctuaryPlugin.NIGHTMARE_SPAWN);
          }
          p.getPlugin(SisterhoodSanctuaryPlugin.class).enterTheNightmare();
        };
    instances.put(NpcId.THE_NIGHTMARE_814_9433, instance);

    for (var i : instances.values()) {
      if (i.spawns == null) {
        continue;
      }
      for (var spawn : i.spawns) {
        spawn.respawnable(true);
      }
    }
    INSTANCES = Collections.unmodifiableMap(instances);
  }

  private Tile playerTile;
  private TeleportType teleportType = TeleportType.DEFAULT;
  private boolean taskOnly;
  private boolean alwaysUseController;
  private CanStartEvent canStartEvent;
  private StartEvent startEvent;
  private CanStartEvent canStartEventClanChat;
  private StartEvent startEventClanChat;
  private List<NpcSpawn> spawns = new ArrayList<>();

  public static boolean canStart(Player player, int npcId, boolean clanChat) {
    var instance = INSTANCES.get(npcId);
    if (instance == null) {
      return false;
    }
    var freeInstance = isFreeInstance(npcId);
    if (!isTaskBypassable(instance)) {
      if (!player.getSkills().isAnySlayerTask(npcId)) {
        player.getGameEncoder().sendMessage("You need an appropriate Slayer task to do this.");
        return false;
      }
    }
    if (!freeInstance && !player.getInventory().hasItem(ItemId.BOSS_INSTANCE_SCROLL_32313)) {
      player.getGameEncoder().sendMessage("You need an instance creation item to do this.");
      return false;
    }
    if (clanChat && !player.getMessaging().canClanChatEvent()) {
      player
          .getGameEncoder()
          .sendMessage("Your Clan Chat privledges aren't high enough to do that.");
      return false;
    }
    var playerInstance =
        player
            .getWorld()
            .getPlayerBossInstance(
                player.getMessaging().getClanChatUsername(), player.getController());
    if (clanChat && playerInstance != null && playerInstance.is(BossInstanceController.class)) {
      player.getGameEncoder().sendMessage("There is already a boss instance for this Clan Chat.");
      return false;
    }
    var bossInstance =
        playerInstance != null && playerInstance.is(BossInstanceController.class)
            ? playerInstance.as(BossInstanceController.class)
            : null;
    if (clanChat) {
      return instance.canStartEventClanChat == null
          || instance.canStartEventClanChat.canStart(player, bossInstance);
    } else {
      return instance.canStartEvent == null
          || instance.canStartEvent.canStart(player, bossInstance);
    }
  }

  public static void start(Player player, int npcId, boolean clanChat) {
    var instance = INSTANCES.get(npcId);
    if (instance == null) {
      return;
    }
    if (!canStart(player, npcId, clanChat)) {
      return;
    }
    if (!isFreeInstance(npcId)
        && !player.getInventory().deleteItem(ItemId.BOSS_INSTANCE_SCROLL_32313).success()) {
      player.getGameEncoder().sendMessage("You need an instance creation item to do this.");
      return;
    }
    player.setController(new BossInstanceController());
    player.getController().startInstance();
    switch (instance.teleportType) {
      case DEFAULT:
        player.getMovement().teleport(instance.playerTile);
        break;
      case LADDER_UP:
        player.getMovement().ladderUpTeleport(instance.playerTile);
        break;
      case LADDER_DOWN:
        player.getMovement().ladderDownTeleport(instance.playerTile);
        break;
    }
    player
        .getController()
        .as(BossInstanceController.class)
        .setBossInstance(npcId, clanChat, instance.getSpawns());
    if (clanChat) {
      if (instance.startEventClanChat != null) {
        instance.startEventClanChat.start(player);
      }
    } else {
      if (instance.startEvent != null) {
        instance.startEvent.start(player);
      }
    }
    if (clanChat) {
      player
          .getWorld()
          .putPlayerBossInstance(
              player.getMessaging().getClanChatUsername(), player.getController());
    }
  }

  public static boolean canJoin(Player player, int npcId, boolean clanChat) {
    var instance = INSTANCES.get(npcId);
    if (instance == null) {
      return false;
    }
    if (!isTaskBypassable(instance)) {
      if (!player.getSkills().isAnySlayerTask(npcId)) {
        player.getGameEncoder().sendMessage("You need an appropriate Slayer task to do this.");
        return false;
      }
    }
    var clanChatUsername = player.getMessaging().getClanChatUsername();
    var playerInstance =
        player.getWorld().getPlayerBossInstance(clanChatUsername, player.getController());
    if (clanChat && (playerInstance == null || !playerInstance.is(BossInstanceController.class))) {
      player.getGameEncoder().sendMessage("Unable to locate a boss instance for this Clan Chat.");
      return false;
    }
    var bossInstance =
        playerInstance != null && playerInstance.is(BossInstanceController.class)
            ? playerInstance.as(BossInstanceController.class)
            : null;
    if (clanChat && npcId != bossInstance.getNpcId()) {
      player
          .getGameEncoder()
          .sendMessage(
              "The boss instance for this Clan Chat already exists for "
                  + NpcDefinition.getName(npcId)
                  + ".");
      return false;
    }
    if (clanChat) {
      return instance.canStartEventClanChat == null
          || instance.canStartEventClanChat.canStart(player, bossInstance);
    } else {
      return instance.canStartEvent == null
          || instance.canStartEvent.canStart(player, bossInstance);
    }
  }

  public static void join(Player player, int npcId, boolean clanChat) {
    var instance = INSTANCES.get(npcId);
    if (instance == null) {
      return;
    }
    if (!canJoin(player, npcId, clanChat)) {
      return;
    }
    var clanChatUsername = player.getMessaging().getClanChatUsername();
    var playerInstance =
        player.getWorld().getPlayerBossInstance(clanChatUsername, player.getController());
    if (instance.alwaysUseController || clanChat) {
      player.setController(new BossInstanceController());
    }
    if (clanChat) {
      player.getController().joinInstance(playerInstance);
    }
    switch (instance.teleportType) {
      case DEFAULT:
        player.getMovement().teleport(instance.playerTile);
        break;
      case LADDER_UP:
        player.getMovement().ladderUpTeleport(instance.playerTile);
        break;
      case LADDER_DOWN:
        player.getMovement().ladderDownTeleport(instance.playerTile);
        break;
    }
    if (clanChat) {
      player.getWorld().putPlayerBossInstance(clanChatUsername, player.getController());
    }
    if (clanChat) {
      if (instance.startEventClanChat != null) {
        instance.startEventClanChat.start(player);
      }
    } else {
      if (instance.startEvent != null) {
        instance.startEvent.start(player);
      }
    }
  }

  private static boolean isFreeInstance(int npcId) {
    if (npcId == NpcId.KRAKEN_291) {
      return true;
    }
    if (npcId == NpcId.VORKATH_732) {
      return true;
    }
    if (npcId == NpcId.ALCHEMICAL_HYDRA_426) {
      return true;
    }
    if (npcId == NpcId.CRYSTALLINE_HUNLLEF_674) {
      return true;
    }
    if (npcId == NpcId.CORRUPTED_HUNLLEF_894) {
      return true;
    }
    if (npcId == NpcId.VERZIK_VITUR_1040) {
      return true;
    }
    return npcId == NpcId.DUSK_248;
  }

  private static boolean isTaskBypassable(BossInstance instance) {
    if (!instance.taskOnly) {
      return true;
    }
    if (Settings.getInstance().isLocal()) {
      return true;
    }
    return Settings.getInstance().isBeta();
  }

  private enum TeleportType {
    DEFAULT,
    NONE,
    LADDER_UP,
    LADDER_DOWN
  }

  private interface CanStartEvent {

    boolean canStart(Player player, BossInstanceController controller);
  }

  private interface StartEvent {

    void start(Player player);
  }
}
