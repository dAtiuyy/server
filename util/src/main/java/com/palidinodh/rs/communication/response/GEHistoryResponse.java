package com.palidinodh.rs.communication.response;

import com.palidinodh.rs.adaptive.grandexchange.GrandExchangeHistoryItem;
import com.palidinodh.rs.communication.request.Request;
import java.util.List;
import lombok.Getter;

public class GEHistoryResponse extends Response {

  @Getter private List<GrandExchangeHistoryItem> items;

  public GEHistoryResponse(Request request, List<GrandExchangeHistoryItem> items) {
    super(request);
    this.items = items;
  }
}
