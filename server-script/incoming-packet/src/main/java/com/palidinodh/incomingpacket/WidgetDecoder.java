package com.palidinodh.incomingpacket;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.cache.store.util.Stream;
import com.palidinodh.io.Readers;
import com.palidinodh.osrscore.io.incomingpacket.InStreamKey;
import com.palidinodh.osrscore.io.incomingpacket.IncomingPacketDecoder;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Equipment;
import com.palidinodh.osrscore.model.entity.player.Herblore;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playermisc.DropItem;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PLogger;
import java.util.HashMap;
import java.util.Map;

class WidgetDecoder extends IncomingPacketDecoder {

  public static final Map<Integer, WidgetHandler> WIDGET_HANDLERS = new HashMap<>();

  static {
    load(false);
  }

  public static synchronized void load(boolean force) {
    if (!force && !WIDGET_HANDLERS.isEmpty()) {
      return;
    }
    try {
      WIDGET_HANDLERS.clear();
      var classes = Readers.getClasses(WidgetHandler.class, "com.palidinodh.incomingpacket.widget");
      for (var clazz : classes) {
        var handler = Readers.newInstance(clazz);
        var ids = WidgetHandler.getIds(handler);
        if (ids == null) {
          continue;
        }
        for (var id : ids) {
          if (WIDGET_HANDLERS.containsKey(id)) {
            throw new RuntimeException(clazz.getName() + " - " + id + ": widget id already used.");
          }
          WIDGET_HANDLERS.put(id, handler);
        }
      }
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  @Override
  public boolean execute(Player player, Stream stream) {
    var option = getInt(InStreamKey.PACKET_OPTION);
    var widgetHash = getInt(InStreamKey.WIDGET_HASH);
    var widgetId = WidgetHandler.getWidgetId(widgetHash);
    var widgetChildId = WidgetHandler.getWidgetChildId(widgetHash);
    var widgetSlot = getInt(InStreamKey.WIDGET_SLOT);
    var itemId = getInt(InStreamKey.ITEM_ID);
    player.clearIdleTime();
    var message =
        "[Widget("
            + option
            + ")] widgetId="
            + widgetId
            + "/"
            + WidgetId.valueOf(widgetId)
            + "; widgetChildId="
            + widgetChildId
            + "; widgetSlot="
            + widgetSlot
            + "; itemId="
            + itemId
            + "/"
            + ItemId.valueOf(itemId);
    if (Settings.getInstance().isLocal()) {
      PLogger.println(message);
    }
    if (player.getOptions().isDebug()) {
      player.getGameEncoder().sendMessage(message);
    }
    if (!player.getWidgetManager().hasWidget(widgetId)) {
      return false;
    }
    var isEquipment = widgetId == WidgetId.EQUIPMENT || widgetId == WidgetId.EQUIPMENT_BONUSES;
    Equipment.Slot equipmentSlot = null;
    if (isEquipment) {
      equipmentSlot = Equipment.Slot.getFromWidget(widgetId, widgetChildId);
      if (equipmentSlot != null) {
        widgetSlot = equipmentSlot.ordinal();
        itemId = player.getEquipment().getId(equipmentSlot);
      }
    }
    if (widgetId == WidgetId.INVENTORY || isEquipment) {
      if (player.isLocked()) {
        return false;
      }
      if (player.getMovement().isViewing()) {
        return false;
      }
      if (widgetId == WidgetId.INVENTORY && itemId == -1) {
        return false;
      }
      if (widgetId == WidgetId.INVENTORY && itemId != player.getInventory().getId(widgetSlot)) {
        return false;
      }
      if (isEquipment && equipmentSlot != null && itemId == -1) {
        return false;
      }
      if (isEquipment
          && equipmentSlot != null
          && itemId != player.getEquipment().getId(equipmentSlot)) {
        return false;
      }
    }
    var diaryWidgetSlot = widgetSlot;
    var diaryItemId = itemId;
    Diary.getDiaries(player)
        .forEach(
            d -> d.widget(player, option, widgetId, widgetChildId, diaryWidgetSlot, diaryItemId));
    if (player.getController().isItemStorageDisabled()
        && WidgetHandler.isItemStorageWidget(widgetId)) {
      player.getWidgetManager().removeInteractiveWidgets();
      return false;
    }
    if (player.getController().widgetHook(option, widgetId, widgetChildId, widgetSlot, itemId)) {
      return false;
    }
    var finalWidgetSlot = widgetSlot;
    var finalItemId = itemId;
    if (player
        .getPluginList()
        .containsIf(
            p -> p.widgetHook(option, widgetId, widgetChildId, finalWidgetSlot, finalItemId))) {
      return true;
    }
    if (player.getRandomEvent().widgetHook(option, widgetId, widgetChildId, widgetSlot, itemId)) {
      return false;
    }
    if (SkillContainer.widgetHooks(player, option, widgetId, widgetChildId, widgetSlot, itemId)) {
      return false;
    }
    if (widgetId == WidgetId.INVENTORY || equipmentSlot != null && isEquipment) {
      DefinitionOption definitionOption = null;
      Item item = null;
      if (widgetId == WidgetId.INVENTORY) {
        item = player.getInventory().getItem(widgetSlot);
        definitionOption = item.getDef().getOption(option);
        if (definitionOption == null) {
          return false;
        }
      } else if (isEquipment) {
        item = player.getEquipment().getItem(widgetSlot);
        definitionOption = item.getDef().getEquipmentOption(option);
        if (definitionOption == null) {
          return false;
        }
      }
      itemOption(player, isEquipment, definitionOption, item);
      return false;
    }
    var widgetHandler = WidgetHandler.getHandler(player, widgetId);
    if (widgetHandler == null) {
      widgetHandler = WIDGET_HANDLERS.get(widgetId);
    }
    if (widgetHandler != null) {
      if (!widgetHandler.isLockedUsable()) {
        if (player.isLocked()) {
          return true;
        }
        if (player.getMovement().isViewing()) {
          return false;
        }
        if (player.getDialogue() != null && player.getDialogue().isDisableActions()) {
          return false;
        }
      }
      widgetHandler.widgetOption(player, option, widgetId, widgetChildId, widgetSlot, itemId);
    }
    return false;
  }

  public void itemOption(
      Player player, boolean isEquipment, DefinitionOption definitionOption, Item item) {
    var worldEvent =
        player
            .getWorld()
            .getWorldEvents()
            .getIf(e -> e.getItemHandler(player, item.getId()) != null);
    var itemHandler = worldEvent == null ? null : worldEvent.getItemHandler(player, item.getId());
    if (itemHandler == null) {
      itemHandler = PlayerPlugin.getItemHandler(item.getId());
    }
    if (!isEquipment) {
      player.clearAllActions(false, false);
      if (item.getInfoDef().getEquipSlot() != null
          && (definitionOption.equals("wear")
              || definitionOption.equals("wield")
              || definitionOption.equals("equip")
              || definitionOption.equals("ride")
              || definitionOption.equals("hold"))) {
        player.getEquipment().equip(item.getId(), item.getSlot());
        return;
      }
      if (definitionOption.equals("drop") || definitionOption.equals("destroy")) {
        DropItem.drop(player, item.getSlot());
        return;
      }
      Diary.getDiaries(player).forEach(d -> d.itemOption(player, definitionOption, item));
      if (definitionOption.equals("eat") || definitionOption.equals("drink")) {
        if (player.getController().isFood(item.getId())) {
          player.getSkills().eatFood(item.getSlot());
          return;
        }
        if (player.getController().isDrink(item.getId())) {
          player.getSkills().drink(item.getSlot());
          return;
        }
      }
      if (definitionOption.equals("bury") && player.getPrayer().buryBones(item.getSlot()) >= 0) {
        return;
      }
      if (definitionOption.equals("scatter")
          && player.getPrayer().scatterAshes(item.getSlot()) >= 0) {
        return;
      }
      if (definitionOption.equals("clean")
          && Herblore.cleanHerb(player, item.getId(), item.getSlot())) {
        return;
      }
      if (definitionOption.equals("open") && item.getInfoDef().getExchangeSetIds() != null) {
        if (player.getInventory().getRemainingSlots()
            < item.getInfoDef().getExchangeSetIds().length) {
          player.getInventory().notEnoughSpace();
          return;
        }
        item.remove();
        for (var exchangeSetId : item.getInfoDef().getExchangeSetIds()) {
          player.getInventory().addItem(exchangeSetId);
        }
        return;
      }
    } else if (isEquipment) {
      if (definitionOption.equals("remove")) {
        player.getEquipment().unequip(item.getSlot());
        return;
      }
    }
    if (definitionOption.equals("check") && item.getAttachment() != null) {
      player.getCharges().checkCharges(item);
    }
    if (itemHandler != null && definitionOption != null) {
      itemHandler.itemOption(player, definitionOption, item);
    }
  }
}
