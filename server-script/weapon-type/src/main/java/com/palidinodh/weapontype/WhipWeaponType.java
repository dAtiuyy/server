package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.ABYSSAL_WHIP,
  ItemId.ABYSSAL_WHIP_4178,
  ItemId.ABYSSAL_TENTACLE,
  ItemId.VOLCANIC_ABYSSAL_WHIP,
  ItemId.FROZEN_ABYSSAL_WHIP,
  ItemId.ABYSSAL_WHIP_20405,
  ItemId.BLIGHTED_ABYSSAL_WHIP_32361,
  ItemId.ABYSSAL_TENTACLE_OR_60047
})
class WhipWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_20);
    type.renderAnimations(new int[] {808, 1660, 1660, 1660, 1660, 1660, 1661});
    type.equipSound(new Sound(2238));
    type.attackSpeed(4);
    type.defendAnimation(1659);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(1658).attackSound(new Sound(2720)).build());
    return type;
  }
}
