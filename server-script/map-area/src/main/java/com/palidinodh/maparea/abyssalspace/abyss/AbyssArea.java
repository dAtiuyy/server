package com.palidinodh.maparea.abyssalspace.abyss;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(12107)
public class AbyssArea extends Area {}
