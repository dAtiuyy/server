package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import java.util.Arrays;
import java.util.List;

class KrakenTentacleCombat extends NpcCombat {

  @Inject private Npc npc;
  private Npc kraken;
  private Npc[] tentacles = new Npc[4];

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.WHIRLPOOL_5534).id(NpcId.ENORMOUS_TENTACLE_112);
    combat.spawn(NpcCombatSpawn.builder().respawnDelay(6000).build());
    combat.hitpoints(NpcCombatHitpoints.total(120));
    combat.stats(
        NpcCombatStats.builder()
            .rangedLevel(150)
            .bonus(BonusType.DEFENCE_MAGIC, -15)
            .bonus(BonusType.DEFENCE_RANGED, 270)
            .build());
    combat.aggression(NpcCombatAggression.builder().always(true).forceable(false).build());
    combat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    combat.focus(
        NpcCombatFocus.builder().disableFollowingOpponent(true).bypassMapObjects(true).build());
    combat.deathAnimation(3620).blockAnimation(3619);

    var style = NpcCombatStyle.builder();
    style.type(
        NpcCombatStyleType.builder()
            .hitStyleType(HitStyleType.RANGED)
            .subHitStyleType(HitStyleType.MAGIC)
            .build());
    style.damage(NpcCombatDamage.maximum(2));
    style.animation(3618).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(162));
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void tickStartHook() {
    if (npc.isLocked()) {
      return;
    }
    if (!inRecentCombat() && !isAttacking() && npc.getId() != NpcId.WHIRLPOOL_5534) {
      npc.setId(NpcId.WHIRLPOOL_5534);
    } else if (inRecentCombat() || isAttacking()) {
      var attackingEntity = getAttackingEntity();
      emerge(attackingEntity instanceof Player ? attackingEntity.asPlayer() : null);
    }
  }

  @Override
  public boolean canBeAttackedHook(
      Entity opponent, boolean sendMessage, HitStyleType hitStyleType) {
    if (!opponent.isPlayer()) {
      return false;
    }
    var player = opponent.asPlayer();
    if (hitStyleType == HitStyleType.MELEE) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("Melee doesn't seem effective against these...");
      }
      return false;
    }
    if (isAttacking() && getLastAttackedByEntity() != null && player != getLastAttackedByEntity()) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("The Kraken is busy attacking someone else.");
      }
      return false;
    }
    if (kraken != null
        && kraken.getCombat().isAttacking()
        && kraken.getCombat().getLastAttackedByEntity() != null
        && player != kraken.getCombat().getLastAttackedByEntity()) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("The Kraken is busy attacking someone else.");
      }
      return false;
    }
    for (var tentacle : tentacles) {
      if (tentacle != null
          && tentacle.getCombat().isAttacking()
          && tentacle.getCombat().getLastAttackedByEntity() != null
          && player != tentacle.getCombat().getLastAttackedByEntity()) {
        if (sendMessage) {
          player.getGameEncoder().sendMessage("The Kraken is busy attacking someone else.");
        }
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean canBeAggressiveHook(Entity opponent) {
    return !isAttacking() || !isHitDelayed();
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("kraken")) {
      kraken = (Npc) args[0];
    } else if (name.startsWith("tentacle0")) {
      tentacles[0] = (Npc) args[0];
    } else if (name.startsWith("tentacle1")) {
      tentacles[1] = (Npc) args[0];
    } else if (name.startsWith("tentacle2")) {
      tentacles[2] = (Npc) args[0];
    } else if (name.startsWith("tentacle3")) {
      tentacles[3] = (Npc) args[0];
    }
    return null;
  }

  public void emerge(Player player) {
    if (npc.getId() == NpcId.ENORMOUS_TENTACLE_112) {
      return;
    }
    npc.setId(NpcId.ENORMOUS_TENTACLE_112);
    npc.setLock(1);
    npc.setAnimation(3860);
    setHitDelay(4);
    setAttackingEntity(player);
  }
}
