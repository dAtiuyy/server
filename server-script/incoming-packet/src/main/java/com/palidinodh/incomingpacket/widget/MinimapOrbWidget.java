package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(WidgetId.MINIMAP_ORBS)
class MinimapOrbWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    switch (childId) {
      case 1:
        if (option == 0) {
          player.getXPDrops().switchShow();
        } else if (option == 1) {
          player.getXPDrops().openSetup();
        }
        break;
      case 14:
        if (option == 0) {
          player.getPrayer().activateQuick();
        } else if (option == 1) {
          player.getPrayer().switchQuick();
        }
        break;
      case 22:
        player.getMovement().setRunning(!player.getMovement().isRunning());
        break;
      case 30:
        if (player.getCombat().getAttackingEntity() != null
            && player.getCombat().getAttackingEntity().isPlayer()) {
          break;
        }
        player.getCombat().activateSpecialAttack();
        break;
      case 43:
        if (option == 0) {
          player.getWidgetManager().changeWorldMapState();
        } else if (option == 1) {
          player.getWidgetManager().sendWorldMap(true);
        }
        break;
    }
  }
}
