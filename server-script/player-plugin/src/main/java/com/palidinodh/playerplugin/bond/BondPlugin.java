package com.palidinodh.playerplugin.bond;

import com.google.inject.Inject;
import com.palidinodh.cache.clientscript2.ScrollbarSizeCs2;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.Main;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.DonatorRankType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.PlayerPlugin;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.MysteryBox;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.setting.UserRank;
import com.palidinodh.util.PNumber;
import com.palidinodh.util.PString;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
public class BondPlugin implements PlayerPlugin {

  @Inject private transient Player player;
  private transient DonatorRankType donatorRank = DonatorRankType.NONE;

  @Setter private long pouch;
  @Setter private long totalPurchased;
  @Setter private long totalRedeemed;
  @Setter private boolean hideRankIcon;
  private Map<DonatorEffectType, Boolean> effects;
  private List<BondRelicType> relicsUnlocked = new ArrayList<>();

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("bond_plugin_donator_rank")) {
      return donatorRank;
    }
    if (name.equals("bond_plugin_is_relic_unlocked")) {
      return isRelicUnlocked((BondRelicType) args[0]) ? Boolean.TRUE : Boolean.FALSE;
    }
    if (name.equals("bond_plugin_is_donator_effect_enabled")) {
      return isDonatorEffectEnabled(DonatorEffectType.valueOf(((String) args[0]).toUpperCase()))
          ? Boolean.TRUE
          : Boolean.FALSE;
    }
    if (name.equals("bond_plugin_can_donator_pickup_item")) {
      return canDonatorPickupItem((int) args[0]) ? Boolean.TRUE : Boolean.FALSE;
    }
    if (name.equals("bond_plugin_pickup_type_is_pickup_item_id")) {
      return canDonatorNoteItem((int) args[0]) ? Boolean.TRUE : Boolean.FALSE;
    }
    if (name.equals("bond_plugin_hide_rank_icon")) {
      return hideRankIcon ? Boolean.TRUE : Boolean.FALSE;
    }
    if (name.equals("bond_plugin_total")) {
      return totalPurchased + totalRedeemed;
    }
    return null;
  }

  @Override
  public void login() {
    var info = Main.getBondInfo(player.getId());
    if (info != null && !info.given) {
      info.given = true;
      totalPurchased = info.totalPurchased;
      totalRedeemed = info.totalRedeemed;
      if (info.itemIds != null && info.itemIds.length > 0) {
        for (var itemId : info.itemIds) {
          player.getBank().add(new Item(itemId));
        }
      }
      player.getGameEncoder().sendMessage("Your previous bond information has been restored.");
    }
    sendPouchCounts();
  }

  @Override
  public int getCurrency(String identifier) {
    if (identifier.equals("bonds")) {
      return (int) Math.min(pouch, Item.MAX_AMOUNT);
    }
    return 0;
  }

  @Override
  public void changeCurrency(String identifier, int amount) {
    if (identifier.equals("bonds")) {
      pouch += amount;
      if (amount < 0) {
        totalRedeemed -= amount;
        sendPouchCounts();
      }
    } else if (identifier.equals("ordered bonds")) {
      pouch += amount;
      totalPurchased += amount;
    }
  }

  public void sendPouchCounts() {
    donatorRank = DonatorRankType.NONE;
    var widgetId = WidgetId.BOND_POUCH_1017;
    var inventoryCount = player.getInventory().getCount(ItemId.BOND_32318);
    var totalBonds = totalPurchased + totalRedeemed;
    var highestRankBonds = 0;
    for (var rank : DonatorRankType.values()) {
      if (totalBonds < rank.getBonds()) {
        continue;
      }
      player.addUsergroup(rank.getRank());
      if (highestRankBonds >= rank.getBonds()) {
        continue;
      }
      highestRankBonds = rank.getBonds();
      donatorRank = rank;
    }
    if (player.getWidgetManager().getInteractiveOverlay() != widgetId) {
      return;
    }
    player.getGameEncoder().sendWidgetText(widgetId, 34, PNumber.formatNumber(inventoryCount));
    player.getGameEncoder().sendWidgetText(widgetId, 39, PNumber.formatNumber(pouch));
    player.getGameEncoder().sendWidgetText(widgetId, 45, PNumber.formatNumber(totalPurchased));
    player.getGameEncoder().sendWidgetText(widgetId, 51, PNumber.formatNumber(totalRedeemed));
    player.getGameEncoder().sendWidgetText(widgetId, 57, PNumber.formatNumber(totalBonds));
    var rankName = donatorRank == DonatorRankType.NONE ? "None" : donatorRank.getFormattedName();
    if (hideRankIcon) {
      rankName += " (hidden)";
    }
    player.getGameEncoder().sendWidgetText(widgetId, 62, "Rank: " + rankName);
  }

  public void openInformation() {
    var widgetId = WidgetId.BOND_INFORMATION_1018;
    player.getWidgetManager().sendInteractiveOverlay(widgetId);
    var infos = BondInformationType.values();
    for (var i = 0; i < 20; i++) {
      if (i >= infos.length) {
        player.getGameEncoder().sendHideWidget(widgetId, 27 + i * 3, true);
        continue;
      }
      var info = infos[i];
      player.getGameEncoder().sendHideWidget(widgetId, 27 + i * 3, false);
      player.getGameEncoder().sendWidgetText(widgetId, 29 + i * 3, info.getName());
    }
    player
        .getGameEncoder()
        .sendClientScriptData(
            ScrollbarSizeCs2.builder()
                .root(widgetId)
                .container(26)
                .scrollbar(25)
                .height(BondInformationType.values().length * 20)
                .build());
    player.getGameEncoder().sendWidgetText(widgetId, 94, "");
    player
        .getGameEncoder()
        .sendClientScriptData(
            ScrollbarSizeCs2.builder().root(widgetId).container(93).scrollbar(92).build());
  }

  public void sendInformation(int index) {
    if (index < 0 || index >= BondInformationType.values().length) {
      return;
    }
    var widgetId = WidgetId.BOND_INFORMATION_1018;
    var info = BondInformationType.values()[index];
    var description =
        "<col=FFFFFF><u=FFFFFF>" + info.getName() + "</u></col><br>" + info.getDescription();
    player.getGameEncoder().sendWidgetText(widgetId, 94, description);
    var items = MysteryBox.getAllItems(player, info.getMysteryBoxItemId());
    player
        .getGameEncoder()
        .sendItems(widgetId, 95, 0, items == null ? null : items.toArray(new RandomItem[0]));
    var height = PString.getLineCount(description, 45) * 20;
    if (items != null) {
      height = (int) (200 + Math.ceil(items.size() / 6.0D) * 42);
    }
    player
        .getGameEncoder()
        .sendClientScriptData(
            ScrollbarSizeCs2.builder()
                .root(widgetId)
                .container(93)
                .scrollbar(92)
                .height(height)
                .build());
  }

  public boolean isRelicUnlocked(BondRelicType relic) {
    return relicsUnlocked.contains(relic);
  }

  public boolean isDonatorEffectEnabled(DonatorEffectType type) {
    if (!player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
      return false;
    }
    if (effects != null && effects.containsKey(type)) {
      return effects.get(type);
    }
    return type.isDefaultOn();
  }

  public boolean canDonatorPickupItem(int itemId) {
    if (!player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
      return false;
    }
    for (var type : DonatorEffectType.values()) {
      if (!isDonatorEffectEnabled(type)) {
        continue;
      }
      if (type.getPickupItemIds() == null) {
        continue;
      }
      if (!type.isPickupItemId(itemId)) {
        continue;
      }
      return PRandom.inRange(1, donatorRank.getActivationChance());
    }
    return false;
  }

  public boolean canDonatorNoteItem(int itemId) {
    if (!player.isUsergroup(UserRank.SAPPHIRE_MEMBER)) {
      return false;
    }
    for (var type : DonatorEffectType.values()) {
      if (!isDonatorEffectEnabled(type)) {
        continue;
      }
      if (type.getPickupType() == null) {
        continue;
      }
      if (!type.getPickupType().isPickupItemId(itemId)) {
        continue;
      }
      return true;
    }
    return false;
  }

  public void toggleDonatorEffect(DonatorEffectType type) {
    if (effects == null) {
      effects = new EnumMap<>(DonatorEffectType.class);
    }
    if (!effects.containsKey(type)) {
      effects.put(type, type.isDefaultOn());
    }
    var newValue = !effects.get(type);
    if (newValue == type.isDefaultOn()) {
      effects.remove(type);
    } else {
      effects.put(type, newValue);
    }
  }
}
