package com.palidinodh.npccombat;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.graphic.Graphic;
import java.util.Arrays;
import java.util.List;
import lombok.Setter;

class NexMinionCombat extends NpcCombat {

  @Setter private boolean blockDamage = true;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var fumusCombat = NpcCombatDefinition.builder();
    fumusCombat.id(NpcId.FUMUS_285);
    fumusCombat.hitpoints(NpcCombatHitpoints.total(500));
    fumusCombat.hitpoints(
        NpcCombatHitpoints.builder().total(500).barType(HitpointsBarType.GREEN_RED_50).build());
    fumusCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(200)
            .defenceLevel(200)
            .bonus(BonusType.ATTACK_MAGIC, 25)
            .bonus(BonusType.DEFENCE_STAB, 25)
            .bonus(BonusType.DEFENCE_SLASH, 100)
            .bonus(BonusType.DEFENCE_CRUSH, 100)
            .bonus(BonusType.DEFENCE_MAGIC, 150)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    fumusCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    fumusCombat.focus(NpcCombatFocus.builder().disableFollowingOpponent(true).build());
    fumusCombat.deathAnimation(836).blockAnimation(424);

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(29).splashOnMiss(true).build());
    style.animation(1979).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(385));
    fumusCombat.style(style.build());

    var umbraCombat = NpcCombatDefinition.builder();
    umbraCombat.id(NpcId.UMBRA_285);
    umbraCombat.hitpoints(
        NpcCombatHitpoints.builder().total(500).barType(HitpointsBarType.GREEN_RED_50).build());
    umbraCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(200)
            .defenceLevel(200)
            .bonus(BonusType.ATTACK_MAGIC, 25)
            .bonus(BonusType.MELEE_DEFENCE, 100)
            .bonus(BonusType.DEFENCE_MAGIC, 150)
            .bonus(BonusType.DEFENCE_RANGED, 25)
            .build());
    umbraCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    umbraCombat.focus(NpcCombatFocus.builder().disableFollowingOpponent(true).build());
    umbraCombat.deathAnimation(836).blockAnimation(424);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(29).splashOnMiss(true).build());
    style.animation(1979).attackSpeed(5);
    style.targetGraphic(new Graphic(383));
    umbraCombat.style(style.build());

    var cruorCombat = NpcCombatDefinition.builder();
    cruorCombat.id(NpcId.CRUOR_285);
    cruorCombat.hitpoints(
        NpcCombatHitpoints.builder().total(500).barType(HitpointsBarType.GREEN_RED_50).build());
    cruorCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(200)
            .defenceLevel(200)
            .bonus(BonusType.ATTACK_MAGIC, 25)
            .bonus(BonusType.DEFENCE_STAB, 100)
            .bonus(BonusType.DEFENCE_SLASH, 25)
            .bonus(BonusType.DEFENCE_CRUSH, 100)
            .bonus(BonusType.DEFENCE_MAGIC, 150)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    cruorCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    cruorCombat.focus(NpcCombatFocus.builder().disableFollowingOpponent(true).build());
    cruorCombat.deathAnimation(836).blockAnimation(424);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(29).splashOnMiss(true).build());
    style.animation(1979).attackSpeed(5);
    style.targetGraphic(new Graphic(377));
    cruorCombat.style(style.build());

    var glaciesCombat = NpcCombatDefinition.builder();
    glaciesCombat.id(NpcId.GLACIES_285);
    glaciesCombat.hitpoints(
        NpcCombatHitpoints.builder().total(500).barType(HitpointsBarType.GREEN_RED_50).build());
    glaciesCombat.stats(
        NpcCombatStats.builder()
            .magicLevel(200)
            .defenceLevel(200)
            .bonus(BonusType.ATTACK_MAGIC, 25)
            .bonus(BonusType.DEFENCE_STAB, 100)
            .bonus(BonusType.DEFENCE_SLASH, 100)
            .bonus(BonusType.DEFENCE_CRUSH, 25)
            .bonus(BonusType.DEFENCE_MAGIC, 150)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    glaciesCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    glaciesCombat.focus(NpcCombatFocus.builder().disableFollowingOpponent(true).build());
    glaciesCombat.deathAnimation(836).blockAnimation(424);

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.builder().maximum(29).splashOnMiss(true).build());
    style.animation(1979).attackSpeed(5);
    style.projectile(NpcCombatProjectile.id(368));
    glaciesCombat.style(style.build());

    return Arrays.asList(
        fumusCombat.build(), umbraCombat.build(), cruorCombat.build(), glaciesCombat.build());
  }

  @Override
  public void restoreHook() {
    blockDamage = true;
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (blockDamage) {
      damage = 0;
    }
    return damage;
  }
}
