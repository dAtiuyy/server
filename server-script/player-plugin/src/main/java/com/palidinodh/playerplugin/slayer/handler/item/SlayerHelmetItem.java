package com.palidinodh.playerplugin.slayer.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.slayer.ColoredSlayerHelmet;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.SLAYER_HELMET,
  ItemId.SLAYER_HELMET_I,
  ItemId.BLACK_SLAYER_HELMET,
  ItemId.BLACK_SLAYER_HELMET_I,
  ItemId.GREEN_SLAYER_HELMET,
  ItemId.GREEN_SLAYER_HELMET_I,
  ItemId.RED_SLAYER_HELMET,
  ItemId.RED_SLAYER_HELMET_I,
  ItemId.PURPLE_SLAYER_HELMET,
  ItemId.PURPLE_SLAYER_HELMET_I,
  ItemId.TURQUOISE_SLAYER_HELMET,
  ItemId.TURQUOISE_SLAYER_HELMET_I,
  ItemId.HYDRA_SLAYER_HELMET,
  ItemId.HYDRA_SLAYER_HELMET_I,
  ItemId.TWISTED_SLAYER_HELMET,
  ItemId.TWISTED_SLAYER_HELMET_I,
  ItemId.TZTOK_SLAYER_HELMET,
  ItemId.TZTOK_SLAYER_HELMET_I,
  ItemId.TZKAL_SLAYER_HELMET,
  ItemId.TZKAL_SLAYER_HELMET_I,
  ItemId.VAMPYRIC_SLAYER_HELMET,
  ItemId.VAMPYRIC_SLAYER_HELMET_I
})
class SlayerHelmetItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    if (option.equals("check")) {
      player.openDialogue(
          new OptionsDialogue(
              "Which task would you like to check?",
              new DialogueOption("Normal task!", (c, s) -> plugin.sendTask(plugin.getTask())),
              new DialogueOption(
                  "Wilderness task!", (c, s) -> plugin.sendTask(plugin.getWildernessTask()))));
    }
  }

  @Override
  public boolean itemOnItem(Player player, Item useItem, Item onItem) {
    var plugin = player.getPlugin(SlayerPlugin.class);
    for (var slayerHelmet : ColoredSlayerHelmet.values()) {
      if (slayerHelmet.getFromHelmetId() == -1) {
        continue;
      }
      if (slayerHelmet.getFromAttachmentId() == -1) {
        continue;
      }
      if (!ItemHandler.used(
          useItem, onItem, slayerHelmet.getFromHelmetId(), slayerHelmet.getFromAttachmentId())) {
        continue;
      }
      if (slayerHelmet.getUnlock() != null
          && !plugin.getRewards().isUnlocked(slayerHelmet.getUnlock())) {
        player.getGameEncoder().sendMessage("You need to unlock this feature first.");
        return true;
      }
      useItem.remove();
      onItem.replace(new Item(slayerHelmet.getToHelmetId()));
      return true;
    }
    return false;
  }
}
