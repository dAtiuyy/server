package com.palidinodh.playerplugin.grandexchange.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.RANDOM_BARROWS_ITEM_32395)
class RandomBarrowsItem implements ItemHandler {

  private static final int[] EQUIPMENT = {
    ItemId.AHRIMS_HOOD,
    ItemId.AHRIMS_STAFF,
    ItemId.AHRIMS_ROBETOP,
    ItemId.AHRIMS_ROBESKIRT,
    ItemId.DHAROKS_HELM,
    ItemId.DHAROKS_GREATAXE,
    ItemId.DHAROKS_PLATEBODY,
    ItemId.DHAROKS_PLATELEGS,
    ItemId.GUTHANS_HELM,
    ItemId.GUTHANS_WARSPEAR,
    ItemId.GUTHANS_PLATEBODY,
    ItemId.GUTHANS_CHAINSKIRT,
    ItemId.KARILS_COIF,
    ItemId.KARILS_CROSSBOW,
    ItemId.KARILS_LEATHERTOP,
    ItemId.KARILS_LEATHERSKIRT,
    ItemId.TORAGS_HELM,
    ItemId.TORAGS_HAMMERS,
    ItemId.TORAGS_PLATEBODY,
    ItemId.TORAGS_PLATELEGS,
    ItemId.VERACS_HELM,
    ItemId.VERACS_FLAIL,
    ItemId.VERACS_BRASSARD,
    ItemId.VERACS_PLATESKIRT
  };

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    item.replace(new Item(PRandom.arrayRandom(EQUIPMENT)));
  }
}
