package com.palidinodh.incomingpacket.misc.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.OptionsDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({ItemId.ABYSSAL_TENTACLE, ItemId.ABYSSAL_TENTACLE_OR_60047})
class AbyssalTentacleItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (item.getId()) {
      case ItemId.ABYSSAL_TENTACLE:
        {
          switch (option.getText()) {
            case "dissolve":
              player.openDialogue(
                  new OptionsDialogue(
                      "Are you sure you want to dissolve this item?",
                      new DialogueOption(
                          "Yes, dissolve this item and lose the abyssal whip.",
                          (c, s) -> {
                            item.replace(new Item(ItemId.KRAKEN_TENTACLE));
                          }),
                      new DialogueOption("Cancel.")));
              break;
          }
          break;
        }
      case ItemId.ABYSSAL_TENTACLE_OR_60047:
        {
          switch (option.getText()) {
            case "dissolve":
              player.openDialogue(
                  new OptionsDialogue(
                      "Are you sure you want to dissolve this item?",
                      new DialogueOption(
                          "Yes, dissolve this item and lose the abyssal whip.",
                          (c, s) -> {
                            item.replace(new Item(ItemId.KRAKEN_TENTACLE_OR_60048));
                          }),
                      new DialogueOption("Cancel.")));
              break;
          }
          break;
        }
    }
  }
}
