package com.palidinodh.playerplugin.clanwars;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.player.Loadout;
import com.palidinodh.osrscore.model.entity.player.Magic;
import com.palidinodh.osrscore.model.entity.player.Messaging;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Prayer;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.consumable.Consumable;
import com.palidinodh.osrscore.model.entity.player.controller.PController;
import com.palidinodh.osrscore.model.entity.player.magic.SpellbookType;
import com.palidinodh.osrscore.model.item.ItemDef;
import com.palidinodh.osrscore.model.map.MapItem;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.worldevent.pvptournament.PvpTournamentEvent;
import java.util.List;

public class ClanWarsPC extends PController {

  public static final long serialVersionUID = 12022016L;

  @Inject private Player player;
  private transient PvpTournamentEvent tournament;
  private transient ClanWarsPlugin plugin;

  private Tile exitTile;
  private int lastUpdate = 4;
  private String clanChatUsername;
  private int tournamentInterfaceDelay;
  private SpellbookType originalSpellbook;

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("clan_wars")) {
      return true;
    } else if (name.equals("clan_wars_battle")) {
      return plugin.getState() == ClanWarsPlayerState.BATTLE;
    }
    return null;
  }

  @Override
  public void startHook() {
    plugin = player.getPlugin(ClanWarsPlugin.class);
    setTeleportsDisabled(true);
    exitTile = new Tile(new Tile(player));
    player.getGameEncoder().sendPlayerOption("null", 1, false);
    player.getGameEncoder().sendHintIconReset();
    originalSpellbook = player.getMagic().getSpellbook();
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      tournament = player.getWorld().getWorldEvent(PvpTournamentEvent.class);
      player.getPrayer().setAllowAllPrayers(true);
      setItemStorageDisabled(true);
      if (tournament.getMode().getRunes() != null) {
        tournament
            .getMode()
            .getRunes()
            .forEach(
                i -> {
                  player.getInventory().addItem(i, Magic.MAX_RUNE_POUCH_AMOUNT);
                  addRunePouchRune(0, Magic.MAX_RUNE_POUCH_AMOUNT);
                });
      }
    } else {
      player.getGameEncoder().sendPlayerOption("Attack", 2, false);
      player.getWidgetManager().sendOverlay(88);
      plugin.sendBattleVarbits(0, 0, null);
      player
          .getAppearance()
          .setHiddenTeamId(plugin.isTop() ? ItemId.CLAN_WARS_CAPE : ItemId.CLAN_WARS_CAPE_12675);
    }
    clanChatUsername = player.getMessaging().getClanChatUsername();
  }

  @Override
  public void stopHook() {
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      player.getGameEncoder().sendHideWidget(WidgetId.LMS_LOBBY_OVERLAY, 0, false);
      player.getInventory().clear();
      player.getEquipment().clear();
      player.getEquipment().weaponUpdate(true);
      tournament.removePlayer(player);
    } else {
      player.getGameEncoder().sendPlayerOption("Challenge", 1, false);
      player.getAppearance().setHiddenTeamId(-1);
      player.rejuvenate();
    }
    player.getGameEncoder().sendPlayerOption("null", 2, false);
    player.getWidgetManager().removeOverlay();
    player.getWidgetManager().removeFullOverlay();
    player.getWidgetManager().removeInteractiveWidgets();
    player.getMovement().teleport(exitTile);
    player.restore();
    ClanWarsStages.openCompletedState(player, plugin);
    player.getCombat().setUsingWildernessInterface(false);
    player.getSkills().setCombatLevel();
    player.getGameEncoder().sendWorldMode(player.getWorld().getMode());
    player.getPrayer().setAllowAllPrayers(false);
    player.getMagic().setSpellbook(originalSpellbook);
  }

  @Override
  public void tickHook() {
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT
        && !player.isLocked()
        && !player.getMovement().isTeleportStateStarted()
        && !player.getMovement().isTeleportStateFinished()) {
      if (!player.inClanWarsTournamentLobby() && !player.inClanWarsBattle()) {
        stop();
        return;
      }
      if (plugin.getOpponent() != null && !plugin.getOpponent().inClanWarsBattle()) {
        plugin.setOpponent(null);
      }
    }
    setMultiCombatFlag(plugin.getState() != ClanWarsPlayerState.TOURNAMENT);
    if (lastUpdate > 0) {
      lastUpdate--;
      if (lastUpdate == 0) {
        lastUpdate = 8;
        checkStatus();
      }
    }
    if (player != null
        && player.getMovement().isViewing()
        && player.getMovement().getViewing() != null
        && player.getMovement().getViewing().getX() != 0
        && !player.getMovement().isTeleportStateStarted()
        && !player.getMovement().isTeleportStateFinished()
        && !player.getWidgetManager().hasWidget(WidgetId.CLAN_WARS_ORBS)
        && !player.getWidgetManager().hasWidget(WidgetId.TOURNAMENT_VIEWER)) {
      player.getMovement().stopViewing();
    }
    if (tournamentInterfaceDelay > 0) {
      tournamentInterfaceDelay--;
      if (player != null && tournamentInterfaceDelay == 0) {
        player.getGameEncoder().setVarp(1434, 0);
        player.getGameEncoder().sendWorldMode(player.getWorld().getMode());
      }
    }
  }

  @Override
  public boolean canMagicBindHook() {
    return true /* && plugin.ruleSelected(ClanWars.IGNORE_FREEZING, ClanWars.DISABLED) */;
  }

  @Override
  public boolean allowMultiTargetAttacksHook() {
    return plugin.ruleSelected(ClanWarsRule.SINGLE_SPELLS, ClanWarsRuleOption.DISABLED);
  }

  @Override
  public MapItem addMapItemHook(MapItem mapItem) {
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      mapItem.setNeverAppear();
      if (!player.inClanWarsBattle()) {
        mapItem = null;
      }
    }
    return mapItem;
  }

  @Override
  public void applyDead() {
    var isTournament = plugin.getState() == ClanWarsPlayerState.TOURNAMENT;
    var plugin = player.getPlugin(ClanWarsPlugin.class);
    var opponent = plugin.getOpponent();
    if (isTournament
        && opponent != null
        && opponent.getCombat().isDead()
        && player.getCombat().getRespawnDelay() >= opponent.getCombat().getRespawnDelay()) {
      player.restore();
      return;
    }
    player.getCombat().clearHitEvents();
    if (player.getCombat().getRespawnDelay() == PCombat.PLAYER_RESPAWN_DELAY - 2) {
      player.setAnimation(DEATH_ANIMATION);
    } else if (player.getCombat().getRespawnDelay() == 0) {
      for (var player2 : getPlayers()) {
        if (player2.getPlugin(ClanWarsPlugin.class).getState() != ClanWarsPlayerState.BATTLE
            || plugin.isTop() == player2.getPlugin(ClanWarsPlugin.class).isTop()) {
          continue;
        }
        player2.getPlugin(ClanWarsPlugin.class).incrimentTotalKills();
      }
      if (isTournament) {
        player.getInventory().clear();
        player.getEquipment().clear();
        tournament.removePlayer(player);
        tournament.checkPrizes(player, false);
      }
      if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.LAST_TEAM_STANDING)
          && !isTournament) {
        player.restore();
        plugin.setState(ClanWarsPlayerState.VIEW);
        plugin.teleportViewing();
      } else {
        stop();
      }
    }
  }

  @Override
  public boolean canEatHook(int itemId) {
    if (plugin.ruleSelected(ClanWarsRule.FOOD, ClanWarsRuleOption.DISABLED)) {
      player.getGameEncoder().sendMessage("You can't eat food in this war.");
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.IGNORE_FREEZING, ClanWarsRuleOption.ALLOWED)
        && ItemDef.isMembers(itemId)) {
      player.getGameEncoder().sendMessage("You can't eat this food in this war.");
      return false;
    }
    return true;
  }

  @Override
  public boolean canDrinkHook(int itemId) {
    if (plugin.ruleSelected(ClanWarsRule.DRINKS, ClanWarsRuleOption.DISABLED)) {
      player.getGameEncoder().sendMessage("You can't drink potions in this war.");
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.FOOD, ClanWarsRuleOption.DISABLED)
        && Consumable.isDrink(itemId)
        && Consumable.getDrink(itemId).heals()) {
      player.getGameEncoder().sendMessage("You can't eat food in this war.");
      return false;
    }
    if (plugin.ruleSelected(ClanWarsRule.IGNORE_FREEZING, ClanWarsRuleOption.ALLOWED)
        && ItemDef.isMembers(itemId)) {
      player.getGameEncoder().sendMessage("You can't drink this potion in this war.");
      return false;
    }
    return true;
  }

  @Override
  public int getLevelForXP(int index) {
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      if (index < tournament.getMode().getSkillLevels().length) {
        return tournament.getMode().getSkillLevels()[index];
      } else {
        return 99;
      }
    }
    return super.getLevelForXP(index);
  }

  @Override
  public int getXP(int index) {
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      if (index < tournament.getMode().getSkillLevels().length) {
        return Skills.XP_TABLE[tournament.getMode().getSkillLevels()[index]];
      } else {
        return Skills.XP_TABLE[99];
      }
    }
    return super.getXP(index);
  }

  @Override
  public boolean canGainXP() {
    return false;
  }

  @Override
  public int getExpMultiplier(int skillId) {
    return plugin.getState() == ClanWarsPlayerState.TOURNAMENT ? 1 : -1;
  }

  @Override
  public boolean canTradeHook(Player player2) {
    player.getGameEncoder().sendMessage("You can't trade during a war.");
    return false;
  }

  @Override
  public boolean canBankHook() {
    return false;
  }

  @Override
  public boolean canEquipHook(int itemId, int slot) {
    return !plugin.ruleSelected(ClanWarsRule.IGNORE_FREEZING, ClanWarsRuleOption.ALLOWED)
        || !ItemDef.isMembers(itemId);
  }

  @Override
  public boolean canActivatePrayerHook(int childId) {
    if (plugin.ruleSelected(ClanWarsRule.PRAYER, ClanWarsRuleOption.DISABLED)) {
      return false;
    } else if (plugin.ruleSelected(ClanWarsRule.PRAYER, ClanWarsRuleOption.NO_OVERHEADS)) {
      if (Prayer.getPrayerDef(childId) != null
          && Prayer.getPrayerDef(childId).getHeadiconId() != -1) {
        return false;
      }
    }
    if (plugin.ruleSelected(ClanWarsRule.IGNORE_FREEZING, ClanWarsRuleOption.ALLOWED)
        && Prayer.getPrayerDef(childId) != null
        && Prayer.getPrayerDef(childId).getIdentifier() != null) {
      String identifier = Prayer.getPrayerDef(childId).getIdentifier();
      return !identifier.equals("retribution")
          && !identifier.equals("redemption")
          && !identifier.equals("smite")
          && !identifier.equals("preserve")
          && !identifier.equals("chivalry")
          && !identifier.equals("piety")
          && !identifier.equals("rigour")
          && !identifier.equals("augury");
    }
    return true;
  }

  @Override
  public boolean canActivateSpecialAttackHook() {
    if (plugin.ruleSelected(ClanWarsRule.SPECIAL_ATTACKS, ClanWarsRuleOption.DISABLED)) {
      player.getGameEncoder().sendMessage("You can't use special attacks in this war.");
      return false;
    }
    if (plugin.ruleSelected(
        ClanWarsRule.SPECIAL_ATTACKS, ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD)) {
      if (player.getEquipment().getWeaponId() == 11791
          || player.getEquipment().getWeaponId() == 12902
          || player.getEquipment().getWeaponId() == 12904
          || player.getEquipment().getWeaponId() == 22296
          || player.getEquipment().getWeaponId() == 24144) {
        player.getGameEncoder().sendMessage("You can't use this special attack in this war.");
        return false;
      }
    }
    return true;
  }

  @Override
  public boolean spawnLoadouts() {
    return plugin.getState() == ClanWarsPlayerState.TOURNAMENT;
  }

  @Override
  public boolean inLoadoutZoneHook() {
    return plugin.getState() == ClanWarsPlayerState.TOURNAMENT
        && player.inClanWarsTournamentLobby();
  }

  @Override
  public List<Loadout.Entry> getLoadoutEntriesHook() {
    return plugin.getState() == ClanWarsPlayerState.TOURNAMENT
        ? tournament.getMode().getLoadouts()
        : null;
  }

  @Override
  public boolean canAttackPlayer(Player opponent, boolean sendMessage, HitStyleType hitStyleType) {
    boolean tridentAttack =
        hitStyleType == HitStyleType.MAGIC
            && player.getMagic().getActiveSpell() != null
            && (player.getMagic().getActiveSpell().getName().equals("trident of the seas")
                || player.getMagic().getActiveSpell().getName().equals("trident of the swamp")
                || player.getMagic().getActiveSpell().getName().equals("sanguinesti staff"));
    if (plugin.ruleSelected(ClanWarsRule.MELEE, ClanWarsRuleOption.DISABLED)
        && hitStyleType == HitStyleType.MELEE) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't use melee in this war.");
      }
      return false;
    } else if (plugin.ruleSelected(ClanWarsRule.RANGING, ClanWarsRuleOption.DISABLED)
        && hitStyleType == HitStyleType.RANGED) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't use ranged in this war.");
      }
      return false;
    } else if (plugin.ruleSelected(ClanWarsRule.MAGIC, ClanWarsRuleOption.DISABLED)
        && hitStyleType == HitStyleType.MAGIC) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't use magic in this war.");
      }
      return false;
    } else if (plugin.ruleSelected(ClanWarsRule.MAGIC, ClanWarsRuleOption.STANDARD_SPELLS)
        && hitStyleType == HitStyleType.MAGIC
        && (player.getMagic().getSpellbook() != SpellbookType.STANDARD || tridentAttack)) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't use this spellbook in this war.");
      }
      return false;
    } else if (plugin.ruleSelected(ClanWarsRule.MAGIC, ClanWarsRuleOption.BINDING_ONLY)
        && hitStyleType == HitStyleType.MAGIC
        && player.getMagic().getActiveSpell() != null
        && !player.getMagic().getActiveSpell().getName().equals("bind")
        && !player.getMagic().getActiveSpell().getName().equals("snare")
        && !player.getMagic().getActiveSpell().getName().equals("entangle")) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't use this spell in this war.");
      }
      return false;
    } else if (plugin.ruleSelected(ClanWarsRule.ALLOW_TRIDENT_IN_PVP, ClanWarsRuleOption.DISABLED)
        && tridentAttack) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't use this spell in this war.");
      }
      return false;
    } else if (plugin.getCountdown() > 0
        || plugin.getState() != ClanWarsPlayerState.BATTLE
            && plugin.getState() != ClanWarsPlayerState.TOURNAMENT) {
      return false;
    } else if (opponent != null
        && opponent.getPlugin(ClanWarsPlugin.class).getState() != ClanWarsPlayerState.BATTLE
        && opponent.getPlugin(ClanWarsPlugin.class).getState() != ClanWarsPlayerState.TOURNAMENT) {
      if (sendMessage) {
        player.getGameEncoder().sendMessage("You can't attack this player.");
      }
      return false;
    } else if (plugin.ruleSelected(ClanWarsRule.IGNORE_FREEZING, ClanWarsRuleOption.ALLOWED)
        && hitStyleType == HitStyleType.MAGIC
        && player.getMagic().getActiveSpell() != null) {
      if (player.getMagic().getSpellbook() != SpellbookType.STANDARD) {
        if (sendMessage) {
          player.getGameEncoder().sendMessage("You can't use this spellbook in this war.");
        }
        return false;
      } else if (player.getMagic().getActiveSpell().getChildId() > 39
          || player.getMagic().getActiveSpell().getChildId() == 30) {
        if (sendMessage) {
          player.getGameEncoder().sendMessage("You can't use this spell in this war.");
        }
        return false;
      }
    }
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      if (!player.inClanWarsBattle()) {
        return false;
      }
      if (plugin.getOpponent() != opponent) {
        if (sendMessage) {
          player.getGameEncoder().sendMessage("This player isn't your opponent.");
        }
        return false;
      }
      return true;
    }
    return opponent != null && plugin.isTop() != opponent.getPlugin(ClanWarsPlugin.class).isTop();
  }

  @Override
  public boolean widgetHook(int option, int widgetId, int childId, int slot, int itemId) {
    if (player.isLocked() || !player.getMovement().isTeleportStateNone()) {
      return true;
    }
    if (widgetId == WidgetId.BANK
        || widgetId == WidgetId.BANK_INVENTORY
        || widgetId == WidgetId.DEPOSIT_BOX
        || widgetId == WidgetId.LOOTING_BAG
        || widgetId == WidgetId.TRADE
        || widgetId == WidgetId.TRADE_INVENTORY
        || widgetId == WidgetId.DUEL_STAKE) {
      stop();
      return true;
    } else if (widgetId == WidgetId.RUNE_POUCH
        || widgetId == WidgetId.INVENTORY && itemId == ItemId.RUNE_POUCH) {
      player.getGameEncoder().sendMessage("You can't change your pouch right now.");
      return true;
    } else if (widgetId == Messaging.INTERFACE_CLAN_ID) {
      player.getGameEncoder().sendMessage("You can't change Clan Chat settings right now.");
      return true;
    } else if (widgetId == WidgetId.BOND_POUCH_704) {
      player.getGameEncoder().sendMessage("You can't use bonds right now.");
      return true;
    } else if (plugin.ruleSelected(ClanWarsRule.MAGIC, ClanWarsRuleOption.DISABLED)
        && (widgetId == Magic.INTERFACE_ID || widgetId == Magic.INTERFACE_SPELL_SELECT_ID)) {
      player.getGameEncoder().sendMessage("You can't use magic in this war.");
      return true;
    } else if (plugin.ruleSelected(ClanWarsRule.MAGIC, ClanWarsRuleOption.BINDING_ONLY)
        && (widgetId == Magic.INTERFACE_ID || widgetId == Magic.INTERFACE_SPELL_SELECT_ID)) {
      player.getGameEncoder().sendMessage("You can't use this spellbook in this war.");
      return true;
    } else if (plugin.ruleSelected(ClanWarsRule.MAGIC, ClanWarsRuleOption.STANDARD_SPELLS)
        && (widgetId == Magic.INTERFACE_ID || widgetId == Magic.INTERFACE_SPELL_SELECT_ID)
        && player.getMagic().getSpellbook() != SpellbookType.STANDARD) {
      player.getGameEncoder().sendMessage("You can't use this spellbook in this war.");
      return true;
    } else if (widgetId == WidgetId.CLAN_WARS_ORBS) {
      switch (childId) {
        case 3:
          player.getMovement().stopViewing();
          player.getWidgetManager().removeInteractiveWidgets();
          break;
        case 7:
          plugin.teleportViewing(0);
          break;
        case 8:
          plugin.teleportViewing(1);
          break;
        case 4:
          plugin.teleportViewing(2);
          break;
        case 5:
          plugin.teleportViewing(3);
          break;
        case 6:
          plugin.teleportViewing(4);
          break;
      }
      return true;
    }
    return false;
  }

  @Override
  public boolean mapObjectOptionHook(int option, MapObject mapObject) {
    if (player.isLocked() || !player.getMovement().isTeleportStateNone()) {
      return true;
    }
    switch (mapObject.getId()) {
      case 32446:
        if (plugin.getState() != ClanWarsPlayerState.TOURNAMENT
            || tournament.getMode().getLoadouts() == null) {
          break;
        }
        player.getLoadout().openSelection();
        return true;
    }
    return false;
  }

  public void checkStatus() {
    if (plugin.getState() == ClanWarsPlayerState.TOURNAMENT) {
      return;
    }
    if (clanChatUsername == null || !player.getMessaging().matchesClanChat(clanChatUsername)) {
      stop();
      return;
    }
    int myTeamCount = 0;
    int otherTeamCount = 0;
    List<Player> players = getPlayers();
    Player opponent = null;
    for (int i = 0; i < players.size(); i++) {
      Player p2 = players.get(i);
      if (p2.getPlugin(ClanWarsPlugin.class).getState() != ClanWarsPlayerState.BATTLE) {
        continue;
      }
      if (plugin.isTop() == p2.getPlugin(ClanWarsPlugin.class).isTop()) {
        myTeamCount++;
      } else {
        otherTeamCount++;
        opponent = p2;
      }
    }
    boolean meetsGameEnd = false;
    if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_25)
        && plugin.getTotalKills() >= 25) {
      meetsGameEnd = true;
    } else if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_50)
        && plugin.getTotalKills() >= 50) {
      meetsGameEnd = true;
    } else if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_100)
        && plugin.getTotalKills() >= 100) {
      meetsGameEnd = true;
    } else if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_200)
        && plugin.getTotalKills() >= 200) {
      meetsGameEnd = true;
    } else if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_500)
        && plugin.getTotalKills() >= 500) {
      meetsGameEnd = true;
    } else if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_1000)
        && plugin.getTotalKills() >= 1000) {
      meetsGameEnd = true;
    } else if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_5000)
        && plugin.getTotalKills() >= 5000) {
      meetsGameEnd = true;
    } else if (plugin.ruleSelected(ClanWarsRule.GAME_END, ClanWarsRuleOption.KILLS_10000)
        && plugin.getTotalKills() >= 10000) {
      meetsGameEnd = true;
    }
    plugin.sendBattleVarbits(myTeamCount, otherTeamCount, opponent);
    boolean ignore5 = plugin.ruleSelected(ClanWarsRule.STRAGGLERS, ClanWarsRuleOption.IGNORE_5);
    if (!meetsGameEnd && (ignore5 ? otherTeamCount > 5 : otherTeamCount > 0)) {
      return;
    }
    Player myPlayer = player;
    String lostClanName = null;
    for (int i = 0; i < players.size(); i++) {
      Player p2 = players.get(i);
      if (plugin.isTop() == p2.getPlugin(ClanWarsPlugin.class).isTop()) {
        p2.getPlugin(ClanWarsPlugin.class).setCompleted(CompletedState.WIN);
      } else {
        p2.getPlugin(ClanWarsPlugin.class).setCompleted(CompletedState.LOSE);
        lostClanName = p2.getMessaging().getClanChatName();
      }
      p2.getController().stop();
    }
    if (Settings.getInstance().isBeta()) {
      if (lostClanName != null) {
        myPlayer
            .getWorld()
            .sendNews(
                myPlayer.getMessaging().getClanChatName()
                    + " has defeated "
                    + lostClanName
                    + " in a clan war!");
      } else {
        myPlayer
            .getWorld()
            .sendNews(myPlayer.getMessaging().getClanChatName() + " has won a clan war!");
      }
    }
  }
}
