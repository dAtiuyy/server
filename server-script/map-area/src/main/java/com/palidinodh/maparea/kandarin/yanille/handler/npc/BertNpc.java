package com.palidinodh.maparea.kandarin.yanille.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.achievementdiary.Diary;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.osrscore.model.entity.player.dialogue.chat.NormalChatDialogue;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(NpcId.BERT)
public class BertNpc implements NpcHandler {

  private static void talkToDialogue(Player player, Npc npc) {
    player.openDialogue(
        new NormalChatDialogue(
            npc.getId(),
            "Would you like a bucket of sand?",
            (c, s) -> {
              optionDialogue(player, npc);
            }));
  }

  private static void optionDialogue(Player player, Npc npc) {
    player.openOptionsDialogue(
        new DialogueOption(
            "Yes.",
            (c, s) -> {
              var bucketOfSandItem = new Item(ItemId.BUCKET_OF_SAND);
              player.getInventory().addItem(bucketOfSandItem);
              Diary.getDiaries(player)
                  .forEach(d -> d.makeItem(player, -1, bucketOfSandItem, npc, null));
            }),
        new DialogueOption("No."));
  }

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    talkToDialogue(player, npc);
  }
}
