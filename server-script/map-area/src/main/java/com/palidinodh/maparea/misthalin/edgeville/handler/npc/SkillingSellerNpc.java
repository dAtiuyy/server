package com.palidinodh.maparea.misthalin.edgeville.handler.npc;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.io.incomingpacket.NpcHandler;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.dialogue.DialogueOption;
import com.palidinodh.playerplugin.fishing.FishingPlugin;
import com.palidinodh.playerplugin.woodcutting.WoodcuttingPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.util.PNumber;

@ReferenceId(NpcId.SKILLING_SELLER)
class SkillingSellerNpc implements NpcHandler {

  @Override
  public void npcOption(Player player, DefinitionOption option, Npc npc) {
    if (option.getIndex() == 0) {
      player.openOptionsDialogue(
          new DialogueOption(
              "View shop",
              (c, s) -> {
                player.openShop("skilling");
              }),
          new DialogueOption(
              "Exchange special skilling items",
              (c, s) -> {
                var totalCoins = 0L;
                var unusualFishCount = player.getInventory().getCount(ItemId.UNUSUAL_FISH);
                if (unusualFishCount > 0) {
                  var value = FishingPlugin.UNUSUAL_FISH_VALUE;
                  if (player.getGameMode().isIronType()) {
                    value /= 4;
                  }
                  player.getInventory().deleteItem(ItemId.UNUSUAL_FISH, unusualFishCount);
                  player.getInventory().addOrDropItem(ItemId.COINS, value * unusualFishCount);
                  totalCoins += (long) value * unusualFishCount;
                  if (PRandom.randomE(156 / unusualFishCount) == 0) {
                    player.getInventory().addOrDropItem(ItemId.GOLDEN_TENCH);
                    player
                        .getGameEncoder()
                        .sendMessage(
                            "<col=ff0000>The cormorant has brought you a very strange tench.</col>");
                  }
                }
                var coloredEggCount =
                    player.getInventory().getCount(ItemId.BIRDS_EGG)
                        + player.getInventory().getCount(ItemId.BIRDS_EGG_5077)
                        + player.getInventory().getCount(ItemId.BIRDS_EGG_5078);
                if (coloredEggCount > 0) {
                  totalCoins += player.getPlugin(WoodcuttingPlugin.class).checkShrine();
                }
                var unidentifiedMineralCount =
                    player.getInventory().getCount(ItemId.UNIDENTIFIED_MINERALS);
                if (unidentifiedMineralCount > 0) {
                  var value = 100_000;
                  if (player.getGameMode().isIronType()) {
                    value /= 4;
                  }
                  player
                      .getInventory()
                      .deleteItem(ItemId.UNIDENTIFIED_MINERALS, unidentifiedMineralCount);
                  player
                      .getInventory()
                      .addOrDropItem(ItemId.COINS, value * unidentifiedMineralCount);
                  totalCoins += (long) value * unidentifiedMineralCount;
                }
                if (unusualFishCount == 0
                    && coloredEggCount == 0
                    && unidentifiedMineralCount == 0) {
                  player.getGameEncoder().sendMessage("You have no special items to exchange.");
                }
                if (totalCoins > 0) {
                  player.sendDiscordNewAccountLog(
                      "Coins: " + PNumber.abbreviateNumber(totalCoins) + " from Skilling Seller");
                }
              }),
          new DialogueOption("Nevermind"));
    } else if (option.getIndex() == 2) {
      player.openShop("skilling");
    }
  }
}
