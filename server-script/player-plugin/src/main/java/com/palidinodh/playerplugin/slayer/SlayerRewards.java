package com.palidinodh.playerplugin.slayer;

import com.palidinodh.cache.id.VarbitId;
import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.slayer.SlayerUnlock;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.util.PNumber;
import java.util.ArrayList;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SlayerRewards {

  private static final int[] BLOCKED_TASK_VARBITS = {
    VarbitId.SLAYER_BLOCKED_TASK_1,
    VarbitId.SLAYER_BLOCKED_TASK_2,
    VarbitId.SLAYER_BLOCKED_TASK_3,
    VarbitId.SLAYER_BLOCKED_TASK_4,
    VarbitId.SLAYER_BLOCKED_TASK_5,
    VarbitId.SLAYER_BLOCKED_TASK_6
  };

  private transient Player player;
  private transient SlayerPlugin plugin;

  public void open() {
    player.getWidgetManager().sendInteractiveOverlay(WidgetId.SLAYER_REWARDS);
    player.getGameEncoder().sendWidgetSettings(WidgetId.SLAYER_REWARDS, 8, 0, 100, 2);
    player.getGameEncoder().sendWidgetSettings(WidgetId.SLAYER_REWARDS, 12, 0, 10, 2);
    player.getGameEncoder().sendWidgetSettings(WidgetId.SLAYER_REWARDS, 23, 0, 20, 1086);
    sendVarbits();
  }

  public boolean isUnlocked(SlayerUnlock unlock) {
    return plugin.getUnlocks() != null && plugin.getUnlocks().contains(unlock);
  }

  public void unlock(SlayerUnlock unlock) {
    if (isUnlocked(unlock)) {
      lock(unlock);
      return;
    }
    var cost = unlock.getPrice();
    if (plugin.getPoints() < cost) {
      player.getGameEncoder().sendMessage("You need " + cost + " points to unlock this.");
      return;
    }
    if (plugin.getUnlocks() == null) {
      plugin.setUnlocks(new ArrayList<>());
    }
    if (!plugin.getUnlocks().contains(unlock)) {
      plugin.getUnlocks().add(unlock);
    }
    plugin.setPoints(plugin.getPoints() - cost);
    sendVarbits();
  }

  public void lock(SlayerUnlock unlock) {
    if (plugin.getUnlocks() == null || !plugin.getUnlocks().contains(unlock)) {
      return;
    }
    plugin.getUnlocks().remove(unlock);
    if (unlock.getVarbit() != -1) {
      player.getGameEncoder().setVarbit(unlock.getVarbit(), 0);
    }
    sendVarbits();
  }

  public void cancelTask() {
    if (plugin.getTask().isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    var cost = 30;
    var bondPlugin = player.getPlugin(BondPlugin.class);
    if (bondPlugin.getDonatorRank().getMultiplier() > 0) {
      cost /= bondPlugin.getDonatorRank().getMultiplier();
    }
    cost = Math.max(0, cost);
    if (plugin.getPoints() < cost) {
      player.getGameEncoder().sendMessage("You need " + cost + " points to cancel a task.");
      return;
    }
    player.getGameEncoder().sendMessage("Your task has been cancelled.");
    plugin.getTask().cancel();
    plugin.setPoints(plugin.getPoints() - cost);
    sendVarbits();
  }

  public void cancelWildernessTask() {
    if (plugin.getWildernessTask().isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    var cost = 30;
    var bondPlugin = player.getPlugin(BondPlugin.class);
    if (bondPlugin.getDonatorRank().getMultiplier() > 0) {
      cost /= bondPlugin.getDonatorRank().getMultiplier();
    }
    cost = Math.max(0, cost);
    if (plugin.getPoints() < cost) {
      player.getGameEncoder().sendMessage("You need " + cost + " points to cancel a task.");
      return;
    }
    player.getGameEncoder().sendMessage("Your wilderness task has been cancelled.");
    plugin.getWildernessTask().cancel();
    plugin.setPoints(plugin.getPoints() - cost);
  }

  public void blockTask() {
    if (plugin.getTask().isComplete()) {
      player.getGameEncoder().sendMessage("You need something new to hunt.");
      return;
    }
    if (plugin.getTask().getIdentifier() == null) {
      player.getGameEncoder().sendMessage("This task can't be blocked.");
      return;
    }
    if (plugin.getBlockedTasks() != null
        && plugin.getBlockedTasks().contains(plugin.getTask().getIdentifier())) {
      player.getGameEncoder().sendMessage("This task is already blocked.");
      return;
    }
    if (plugin.getPoints() < 100) {
      player.getGameEncoder().sendMessage("You need 100 points to block a task.");
      return;
    }
    if (plugin.getBlockedTasks() != null && plugin.getBlockedTasks().size() >= 6) {
      player.getGameEncoder().sendMessage("You can't block any more tasks.");
      return;
    }
    if (plugin.getBlockedTasks() == null) {
      plugin.setBlockedTasks(new ArrayList<>());
    }
    plugin.getBlockedTasks().add(plugin.getTask().getIdentifier());
    plugin.setPoints(plugin.getPoints() - 100);
    plugin.getTask().cancel();
    sendVarbits();
  }

  public void unblockTask(int option) {
    if (plugin.getBlockedTasks() == null || option >= plugin.getBlockedTasks().size()) {
      return;
    }
    plugin.getBlockedTasks().remove(option);
    if (plugin.getBlockedTasks().isEmpty()) {
      plugin.setBlockedTasks(null);
    }
    sendVarbits();
  }

  public void buy(Item item, int cost) {
    if (plugin.getPoints() < cost) {
      player
          .getGameEncoder()
          .sendMessage("You need " + PNumber.formatNumber(cost) + " points to buy this.");
      return;
    }
    if (!player.getInventory().canAddItem(item)) {
      player.getInventory().notEnoughSpace();
      return;
    }
    player.getInventory().addItem(item);
    plugin.setPoints(plugin.getPoints() - cost);
    sendVarbits();
  }

  private void sendVarbits() {
    plugin.sendVarps();
    player.getGameEncoder().setVarbit(VarbitId.SLAYER_POINTS, plugin.getPoints());
    if (plugin.getUnlocks() != null) {
      for (SlayerUnlock unlock : plugin.getUnlocks()) {
        if (unlock.getVarbit() == -1) {
          continue;
        }
        player.getGameEncoder().setVarbit(unlock.getVarbit(), 1);
      }
    }
    if (plugin.getBlockedTasks() != null) {
      for (int i = 0; i < plugin.getBlockedTasks().size(); i++) {
        player
            .getGameEncoder()
            .setVarbit(BLOCKED_TASK_VARBITS[i], plugin.getBlockedTasks().get(i).ordinal());
      }
    }
  }
}
