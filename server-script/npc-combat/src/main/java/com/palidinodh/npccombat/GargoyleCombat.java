package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.BonusType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.combat.HitpointsBarType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatAggression;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatKillCount;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.npc.combat.style.special.NpcCombatTargetTile;
import com.palidinodh.osrscore.model.entity.player.PCombat;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.slayer.SlayerPlugin;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class GargoyleCombat extends NpcCombat {

  private static final NpcCombatStyle SPECIAL_ATTACK =
      NpcCombatStyle.builder()
          .type(
              NpcCombatStyleType.builder()
                  .hitStyleType(HitStyleType.MAGIC)
                  .subHitStyleType(HitStyleType.TYPELESS)
                  .build())
          .damage(NpcCombatDamage.maximum(38))
          .animation(7815)
          .attackSpeed(4)
          .projectile(NpcCombatProjectile.builder().id(1453).speedMinimumDistance(10).build())
          .effect(NpcCombatEffect.builder().includeMiss(true).bind(6).build())
          .specialAttack(NpcCombatTargetTile.builder().build())
          .build();

  @Inject private Npc npc;

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop = NpcCombatDrop.builder().gemDropTableDenominator(25).clue(ClueScrollType.HARD, 128);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(512).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MYSTIC_ROBE_TOP_DARK)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(256).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRANITE_MAUL)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(150).slayerTask(true).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BRITTLE_KEY)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_BOOTS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_PLATELEGS)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_FULL_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_RUNE, 30)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_BAR_NOTED, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_ORE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_PLATELEGS)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_RUNE, 75, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 400, 1000)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 10000)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GOLD_ORE_NOTED, 10, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_BAR_NOTED, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GOLD_BAR_NOTED, 10, 15)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.GARGOYLE_111);
    combat.hitpoints(NpcCombatHitpoints.total(105));
    combat.stats(
        NpcCombatStats.builder()
            .attackLevel(75)
            .defenceLevel(107)
            .bonus(BonusType.DEFENCE_STAB, 20)
            .bonus(BonusType.DEFENCE_SLASH, 20)
            .bonus(BonusType.DEFENCE_MAGIC, 20)
            .bonus(BonusType.DEFENCE_RANGED, 20)
            .build());
    combat.slayer(
        NpcCombatSlayer.builder().level(75).superiorId(NpcId.MARBLE_GARGOYLE_349).build());
    combat.aggression(NpcCombatAggression.PLAYERS);
    combat.blockAnimation(1517);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.damage(NpcCombatDamage.maximum(11));
    style.animation(1519).attackSpeed(4);
    combat.style(style.build());

    var cursedCombat = NpcCombatDefinition.builder();
    cursedCombat.id(NpcId.CURSED_GARGOYLE_349_16007);
    cursedCombat.hitpoints(NpcCombatHitpoints.builder().total(158).build());
    cursedCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(112)
            .defenceLevel(112)
            .bonus(BonusType.DEFENCE_STAB, 30)
            .bonus(BonusType.DEFENCE_SLASH, 30)
            .bonus(BonusType.DEFENCE_MAGIC, 30)
            .bonus(BonusType.DEFENCE_RANGED, 30)
            .build());
    cursedCombat.slayer(NpcCombatSlayer.builder().level(75).build());
    cursedCombat.deathAnimation(7812).blockAnimation(7814);
    cursedCombat.drop(drop.rolls(2).build());

    style = NpcCombatStyle.builder();
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(7811).attackSpeed(4);
    cursedCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(7811).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(294));
    cursedCombat.style(style.build());

    var superiorCombat = NpcCombatDefinition.builder();
    superiorCombat.id(NpcId.MARBLE_GARGOYLE_349);
    superiorCombat.hitpoints(
        NpcCombatHitpoints.builder().total(270).barType(HitpointsBarType.GREEN_RED_60).build());
    superiorCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(230)
            .defenceLevel(190)
            .bonus(BonusType.DEFENCE_STAB, 50)
            .bonus(BonusType.DEFENCE_SLASH, 50)
            .bonus(BonusType.DEFENCE_MAGIC, 50)
            .bonus(BonusType.DEFENCE_RANGED, 50)
            .build());
    superiorCombat.slayer(NpcCombatSlayer.builder().level(75).experience(3044).build());
    superiorCombat.aggression(NpcCombatAggression.PLAYERS);
    superiorCombat.killCount(
        NpcCombatKillCount.builder().asName("Superior Slayer Creature").build());
    superiorCombat.deathAnimation(7812).blockAnimation(7814);
    superiorCombat.drop(drop.rolls(3).build());

    style = NpcCombatStyle.builder();
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(7811).attackSpeed(4);
    superiorCombat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.RANGED);
    style.damage(NpcCombatDamage.maximum(30));
    style.animation(7811).attackSpeed(4);
    style.projectile(NpcCombatProjectile.id(294));
    superiorCombat.style(style.build());

    return Arrays.asList(combat.build(), cursedCombat.build(), superiorCombat.build());
  }

  @Override
  public NpcCombatStyle attackTickCombatStyleHook(NpcCombatStyle combatStyle, Entity opponent) {
    return (npc.getId() == NpcId.MARBLE_GARGOYLE_349
                || npc.getId() == NpcId.CURSED_GARGOYLE_349_16007)
            && PRandom.randomE(8) == 0
        ? SPECIAL_ATTACK
        : combatStyle;
  }

  @Override
  public void attackedActionsOpponentHook(Entity opponent) {
    if (!opponent.isPlayer() || npc.getId() != NpcId.CURSED_GARGOYLE_349_16007) {
      return;
    }
    opponent.asPlayer().getCombat().setPKSkullDelay(PCombat.SKULL_DELAY);
  }

  @Override
  public void deathDropItemsHook(Player player, int additionalPlayerLoopCount, Tile dropTile) {
    if (npc.getId() != NpcId.CURSED_GARGOYLE_349_16007) {
      return;
    }
    if (!player.getPlugin(SlayerPlugin.class).isWildernessTask(npc)) {
      return;
    }
    var table =
        NpcCombatDefinition.getDefinition(NpcId.DUSK_328_7889)
            .getDrop()
            .getTable(npc, player, 4, 0, true);
    if (table == null || table.getProbabilityDenominator() < 32) {
      return;
    }
    table.dropItems(npc, player, dropTile);
  }

  @Override
  public NpcCombatDropTable deathDropItemsTableHook(
      Player player, int dropRateDivider, int roll, NpcCombatDropTable table) {
    if (npc.getId() == NpcId.CURSED_GARGOYLE_349_16007) {
      if (!player.getPlugin(SlayerPlugin.class).isWildernessTask(npc)) {
        player.getGameEncoder().sendMessage("Without an assigned task, the loot turns to dust...");
        return null;
      }
      if (roll != 0) {
        return table;
      }
      if (table.getProbabilityDenominator() != NpcCombatDropTable.COMMON) {
        return table;
      }
      return NpcCombatDefinition.getDefinition(NpcId.DUSK_328_7889)
          .getDrop()
          .getTable(NpcCombatDropTable.COMMON);
    }
    return table;
  }
}
