package com.palidinodh.worldevent.pvptournament.mode;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.player.Loadout;
import com.palidinodh.osrscore.model.entity.player.magic.SpellbookType;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.playerplugin.clanwars.ClanWarsRule;
import com.palidinodh.playerplugin.clanwars.ClanWarsRuleOption;
import com.palidinodh.worldevent.pvptournament.ModeMap;
import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
class PureMode implements Mode {

  private List<SubMode> subModes = new ArrayList<>();

  PureMode() {
    var mode =
        SubMode.builder()
            .name("Pure")
            .map(ModeMap.FIVE_BY_FIVE_ROOMS)
            .spellbook(SpellbookType.STANDARD)
            .brewCap(3);
    mode.rules(
        Mode.buildRules(
            ClanWarsRule.PRAYER,
            ClanWarsRuleOption.NO_OVERHEADS,
            ClanWarsRule.SPECIAL_ATTACKS,
            ClanWarsRuleOption.NO_STAFF_OF_THE_DEAD));
    mode.attackLevel(99)
        .strengthLevel(99)
        .rangedLevel(99)
        .magicLevel(99)
        .defenceLevel(1)
        .hitpointsLevel(99)
        .prayerLevel(99);
    mode.rune(ItemId.AIR_RUNE).rune(ItemId.FIRE_RUNE).rune(ItemId.WRATH_RUNE);
    mode.loadout(
        new Loadout.Entry(
            "Melee Load-out #1",
            new Item[] {
              new Item(ItemId.BEARHEAD),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.AMULET_OF_TORTURE),
              new Item(ItemId.ABYSSAL_TENTACLE),
              new Item(ItemId.IRON_PLATEBODY),
              new Item(ItemId.BOOK_OF_WAR),
              null,
              new Item(ItemId.IRON_PLATELEGS),
              null,
              new Item(ItemId.REGEN_BRACELET),
              new Item(ItemId.CLIMBING_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              new Item(ItemId.WAR_BLESSING)
            },
            new Item[] {
              new Item(ItemId.DRAGON_CLAWS),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "Melee Load-out #2",
            new Item[] {
              new Item(ItemId.BEARHEAD),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.AMULET_OF_TORTURE),
              new Item(ItemId.ABYSSAL_TENTACLE),
              new Item(ItemId.IRON_PLATEBODY),
              new Item(ItemId.BOOK_OF_WAR),
              null,
              new Item(ItemId.IRON_PLATELEGS),
              null,
              new Item(ItemId.REGEN_BRACELET),
              new Item(ItemId.CLIMBING_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              new Item(ItemId.WAR_BLESSING)
            },
            new Item[] {
              new Item(ItemId.ARMADYL_GODSWORD),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "Melee Load-out #3",
            new Item[] {
              new Item(ItemId.BEARHEAD),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.AMULET_OF_TORTURE),
              new Item(ItemId.GHRAZI_RAPIER),
              new Item(ItemId.IRON_PLATEBODY),
              new Item(ItemId.BOOK_OF_WAR),
              null,
              new Item(ItemId.IRON_PLATELEGS),
              null,
              new Item(ItemId.REGEN_BRACELET),
              new Item(ItemId.CLIMBING_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              new Item(ItemId.WAR_BLESSING)
            },
            new Item[] {
              new Item(ItemId.DRAGON_CLAWS),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "Melee Load-out #4",
            new Item[] {
              new Item(ItemId.BEARHEAD),
              new Item(ItemId.INFERNAL_CAPE),
              new Item(ItemId.AMULET_OF_TORTURE),
              new Item(ItemId.GHRAZI_RAPIER),
              new Item(ItemId.IRON_PLATEBODY),
              new Item(ItemId.BOOK_OF_WAR),
              null,
              new Item(ItemId.IRON_PLATELEGS),
              null,
              new Item(ItemId.REGEN_BRACELET),
              new Item(ItemId.CLIMBING_BOOTS),
              null,
              new Item(ItemId.BERSERKER_RING_I),
              new Item(ItemId.WAR_BLESSING)
            },
            new Item[] {
              new Item(ItemId.ARMADYL_GODSWORD),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SUPER_COMBAT_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "Ranged Load-out #1",
            new Item[] {
              new Item(ItemId.ROBIN_HOOD_HAT),
              new Item(ItemId.AVAS_ASSEMBLER),
              new Item(ItemId.NECKLACE_OF_ANGUISH),
              new Item(ItemId.DRAGON_KNIFE_P_PLUS_PLUS, 8000),
              new Item(ItemId.RANGERS_TUNIC),
              new Item(ItemId.BOOK_OF_LAW),
              null,
              new Item(ItemId.SARADOMIN_CHAPS),
              null,
              new Item(ItemId.RANGER_GLOVES),
              new Item(ItemId.RANGER_BOOTS),
              null,
              new Item(ItemId.ARCHERS_RING_I),
              new Item(ItemId.DRAGON_ARROW_P_PLUS_PLUS, 8000)
            },
            new Item[] {
              new Item(ItemId.DARK_BOW),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.BASTION_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "Ranged Load-out #2",
            new Item[] {
              new Item(ItemId.ROBIN_HOOD_HAT),
              new Item(ItemId.AVAS_ASSEMBLER),
              new Item(ItemId.NECKLACE_OF_ANGUISH),
              new Item(ItemId._3RD_AGE_BOW),
              new Item(ItemId.RANGERS_TUNIC),
              null,
              null,
              new Item(ItemId.SARADOMIN_CHAPS),
              null,
              new Item(ItemId.RANGER_GLOVES),
              new Item(ItemId.RANGER_BOOTS),
              null,
              new Item(ItemId.ARCHERS_RING_I),
              new Item(ItemId.DRAGON_ARROW_P_PLUS_PLUS, 8000)
            },
            new Item[] {
              new Item(ItemId.DARK_BOW),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.BASTION_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "Ranged Load-out #3",
            new Item[] {
              new Item(ItemId.ROBIN_HOOD_HAT),
              new Item(ItemId.AVAS_ASSEMBLER),
              new Item(ItemId.NECKLACE_OF_ANGUISH),
              new Item(ItemId.ARMADYL_CROSSBOW),
              new Item(ItemId.RANGERS_TUNIC),
              new Item(ItemId.BOOK_OF_LAW),
              null,
              new Item(ItemId.SARADOMIN_CHAPS),
              null,
              new Item(ItemId.RANGER_GLOVES),
              new Item(ItemId.RANGER_BOOTS),
              null,
              new Item(ItemId.ARCHERS_RING_I),
              new Item(ItemId.DRAGONSTONE_BOLTS_E, 8000)
            },
            new Item[] {
              new Item(ItemId.DARK_BOW),
              new Item(ItemId.DRAGON_ARROW_P_PLUS_PLUS, 8000),
              new Item(ItemId.BASTION_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN)
            },
            -1));
    mode.loadout(
        new Loadout.Entry(
            "Magic Load-out",
            new Item[] {
              new Item(ItemId.ELDER_CHAOS_HOOD),
              new Item(ItemId.IMBUED_ZAMORAK_CAPE),
              new Item(ItemId.OCCULT_NECKLACE),
              new Item(ItemId.TOXIC_STAFF_OF_THE_DEAD),
              new Item(ItemId.ELDER_CHAOS_TOP),
              new Item(ItemId.TOME_OF_FIRE),
              null,
              new Item(ItemId.ELDER_CHAOS_ROBE),
              null,
              new Item(ItemId.TORMENTED_BRACELET),
              new Item(ItemId.WIZARD_BOOTS),
              null,
              new Item(ItemId.SEERS_RING_I),
              new Item(ItemId.ANCIENT_BLESSING)
            },
            new Item[] {
              new Item(ItemId.SARADOMIN_SWORD),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.BATTLEMAGE_POTION_4),
              new Item(ItemId.SARADOMIN_BREW_4),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.SANFEW_SERUM_4),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.COOKED_KARAMBWAN),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.ANGLERFISH),
              new Item(ItemId.RUNE_POUCH)
            },
            -1));
    mode.loadout(new Loadout.Entry("+ Dragon Thrownaxe", new Item(ItemId.DRAGON_THROWNAXE, 8000)));
    mode.loadout(new Loadout.Entry("+ Granite Maul", new Item(ItemId.GRANITE_MAUL)));
    subModes.add(mode.build());
  }
}
