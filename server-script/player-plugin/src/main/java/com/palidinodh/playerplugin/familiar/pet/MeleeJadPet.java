package com.palidinodh.playerplugin.familiar.pet;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.playerplugin.familiar.Pet;

class MeleeJadPet implements Pet.BuildType {

  @Override
  public Pet.PetBuilder builder() {
    var builder = Pet.builder();
    builder.entry(
        new Pet.Entry(
            ItemId.TINY_MELEE_JAD_60044, NpcId.TINY_MELEE_JAD_16070, NpcId.TINY_MELEE_JAD_16069));
    return builder;
  }
}
