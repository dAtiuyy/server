package com.palidinodh.command.beta;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler.Beta;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic.Projectile;
import com.palidinodh.osrscore.model.graphic.Graphic.ProjectileSpeed;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.rs.reference.ReferenceName;
import com.palidinodh.util.PEvent;

@ReferenceName({"gfx", "gfxloop"})
class GraphicCommand implements CommandHandler, Beta {

  @Override
  public String getExample(String name) {
    switch (name) {
      case "gfx":
        return "id (height)";
      case "gfxloop":
        return "start_id";
    }
    return "";
  }

  @Override
  public void execute(Player player, String name, String message) {
    var messages = CommandHandler.splitInt(message);
    var height = messages.length > 1 ? messages[1] : 0;
    switch (name) {
      case "gfx":
        player.setGraphic(messages[0], height);
        player
            .getController()
            .sendMapProjectile(
                Projectile.builder()
                    .id(messages[0])
                    .startTile(player)
                    .endTile(new Tile(player).moveY(-2))
                    .speed(new ProjectileSpeed(0, 0, 80))
                    .startHeight(200)
                    .endHeight(0)
                    .build());
        break;
      case "gfxloop":
        {
          var fromId = messages[0];
          var event =
              new PEvent(1) {
                private int id = fromId;
                private boolean reset;

                @Override
                public void execute() {
                  if (reset) {
                    reset = false;
                    setTick(0);
                  } else {
                    reset = true;
                    player.getGameEncoder().sendMessage("Graphic: " + id);
                    player.setGraphic(id, height);
                    player
                        .getController()
                        .sendMapProjectile(
                            Projectile.builder()
                                .id(id++)
                                .startTile(player)
                                .endTile(new Tile(player).moveY(-2))
                                .speed(new ProjectileSpeed(0, 0, 80))
                                .startHeight(200)
                                .endHeight(0)
                                .build());
                    setTick(1);
                  }
                }
              };
          player.setAction(event);
          break;
        }
    }
  }
}
