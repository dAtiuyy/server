package com.palidinodh.rs.communicationv2;

import com.palidinodh.io.DiscordBot;
import com.palidinodh.io.FileManager;
import com.palidinodh.io.JsonIO;
import com.palidinodh.io.Readers;
import com.palidinodh.nio.NioServer;
import com.palidinodh.nio.Session;
import com.palidinodh.nio.SessionHandler;
import com.palidinodh.rs.adaptive.Clan;
import com.palidinodh.rs.adaptive.RsPlayer;
import com.palidinodh.rs.communicationv2.event.AuthEvent;
import com.palidinodh.rs.setting.SecureSettings;
import com.palidinodh.rs.setting.Settings;
import com.palidinodh.util.PArrayList;
import com.palidinodh.util.PLogger;
import com.palidinodh.util.PTime;
import com.palidinodh.util.PUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import lombok.Getter;

@Getter
public class MainCommunicationServer implements Runnable, SessionHandler {

  @Getter private static MainCommunicationServer instance;

  private int head;
  private int tail;
  private boolean running;
  private NioServer listener;
  private Map<Integer, CommunicationSession> worlds = new ConcurrentHashMap<>();
  private Map<String, RsPlayer> playersByUsername = new ConcurrentHashMap<>();
  private Map<Integer, RsPlayer> playersById = new ConcurrentHashMap<>();
  private ConcurrentLinkedQueue<CommunicationEvent> events = new ConcurrentLinkedQueue<>();
  private ConcurrentLinkedQueue<CommunicationNotification> notifications =
      new ConcurrentLinkedQueue<>();
  private List<Clan> clanUpdates = new ArrayList<>();

  private MainCommunicationServer(String ip, int port) {
    try {
      listener = new NioServer();
      listener.setSessionHandler(this);
      listener.setMaxConnectionsPerAddress(2);
      listener.setSessionIdleTimeout(10 * 60);
      listener.start(ip, port);
      running = true;
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  public static void init(SecureSettings settings) {
    instance = new MainCommunicationServer("0.0.0.0", settings.getCommunicationPort());
    new Thread(instance).start();
  }

  public static void main(String[] args) {
    try {
      var properties = new HashMap<String, String>();
      for (var arg : args) {
        var argSet = arg.split("=", 2);
        properties.put(argSet[0], argSet[1]);
      }
      Settings.setInstance(JsonIO.read(new File(properties.get("settings")), Settings.class));
      Settings.setSecure(
          JsonIO.read(
              new File(properties.get("settings").replace(".json", "_secure.json")),
              SecureSettings.class));
      DiscordBot.init(Settings.getSecure().getDiscordToken(), -1);
      init(Settings.getSecure());
    } catch (Exception e) {
      PLogger.error(e);
    }
  }

  @Override
  public void run() {
    var activeWorlds = new PArrayList<CommunicationSession>();
    var activeEvents = new PArrayList<CommunicationEvent>();
    var activeNotifications = new PArrayList<CommunicationNotification>();
    while (running) {
      while (!Settings.getInstance().isLocal() && FileManager.getSqlConnection() == null) {
        try {
          Thread.sleep(1_000);
        } catch (InterruptedException ie) {
        }
      }
      PUtil.gc();
      PTime.update();
      activeEvents.forEach(CommunicationEvent::mainComplete);
      activeNotifications.forEach(n -> activeWorlds.forEach(n::write));
      synchronized (this) {
        activeWorlds.clear();
        activeEvents.clear();
        activeNotifications.clear();
        try {
          var hasPending = head != tail || !events.isEmpty();
          wait(hasPending ? 200 : 60_000);
          activeWorlds.addAll(worlds.values());
          activeEvents.addAll(events);
          events.clear();
          head = tail;
        } catch (InterruptedException ie) {
          break;
        }
      }
    }
    listener.stop();
  }

  @Override
  public void accept(Session session) {
    var communicationSession = new CommunicationSession(session);
    session.setAttachment(communicationSession);
  }

  @Override
  public void read(Session session, byte[] bytes) {
    var communicationSession = (CommunicationSession) session.getAttachment();
    var in = communicationSession.getIn();
    in.appendBytes(bytes);
    if (in.available() < 10) {
      return;
    }
    var readEvents = new ArrayList<CommunicationEvent>();
    try {
      while (in.available() >= 10) {
        in.mark();
        var length = in.readInt();
        if (length > in.available()) {
          in.reset();
          break;
        }
        var className = in.readString();
        var json = new String(Readers.gzDecompress(in.readBytes(in.readInt())));
        var eventObject = JsonIO.read(json, Class.forName(className));
        if (!(eventObject instanceof CommunicationEvent)) {
          session.close();
          break;
        }
        var event = (CommunicationEvent) eventObject;
        event.loadMain(this, communicationSession);
        if (event instanceof AuthEvent) {
          communicationSession.setAuthEvent((AuthEvent) event);
          event.mainAction();
          break;
        }
        if (!communicationSession.isAuthenticated()) {
          session.close();
          break;
        }
        readEvents.add(event);
      }
    } catch (Exception e) {
      PLogger.error(e);
      in.clear();
    }
    if (in.available() == 0) {
      in.clear();
    }
    synchronized (this) {
      if (readEvents != null) {
        events.addAll(readEvents);
      }
      tail++;
      notify();
    }
  }

  public void addNotification(CommunicationNotification notification) {
    notification.load(this);
    synchronized (this) {
      notifications.add(notification);
      tail++;
      notify();
    }
  }

  public Clan getClan(RsPlayer player) {
    return null;
  }
}
