package com.palidinodh.skill.woodcutting;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.player.BondRelicType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.entity.player.Skills;
import com.palidinodh.osrscore.model.entity.player.skill.SkillContainer;
import com.palidinodh.osrscore.model.entity.player.skill.SkillEntry;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.playerplugin.bond.BondPlugin;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PEvent;
import java.util.List;

public class Woodcutting extends SkillContainer {

  private static final WoodcuttingHatchet[] HATCHETS = {
    new WoodcuttingHatchet(ItemId.BRONZE_AXE, 1, 879),
    new WoodcuttingHatchet(ItemId.IRON_AXE, 1, 877),
    new WoodcuttingHatchet(ItemId.STEEL_AXE, 6, 875),
    new WoodcuttingHatchet(ItemId.MITHRIL_AXE, 21, 871),
    new WoodcuttingHatchet(ItemId.ADAMANT_AXE, 31, 869),
    new WoodcuttingHatchet(ItemId.RUNE_AXE, 41, 867),
    new WoodcuttingHatchet(ItemId.GILDED_AXE, 41, 8303),
    new WoodcuttingHatchet(ItemId.DRAGON_AXE, 61, 2846),
    new WoodcuttingHatchet(ItemId._3RD_AGE_AXE, 61, 7264),
    new WoodcuttingHatchet(ItemId.INFERNAL_AXE, 61, 2117),
    new WoodcuttingHatchet(ItemId.INFERNAL_AXE_UNCHARGED, 61, 2117)
  };
  private static final Item[] COLORED_EGG_NESTS = {
    new Item(ItemId.BIRD_NEST), new Item(ItemId.BIRD_NEST_5071), new Item(ItemId.BIRD_NEST_5072)
  };

  private static boolean inWoodcuttingGuild(Tile tile) {
    return tile.within(1607, 3487, 1657, 3518) || tile.within(1563, 3472, 1600, 3503);
  }

  private static WoodcuttingHatchet getHatchet(Player player) {
    for (var i = HATCHETS.length - 1; i >= 0; i--) {
      var hatchet = HATCHETS[i];
      if (!player.carryingItem(hatchet.getItemId())) {
        continue;
      }
      if (player.getSkills().getLevel(Skills.WOODCUTTING) < hatchet.getLevel()) {
        continue;
      }
      return hatchet;
    }
    return null;
  }

  @Override
  public int getSkillId() {
    return Skills.WOODCUTTING;
  }

  @Override
  public List<SkillEntry> getEntries() {
    return WoodcuttingEntries.getEntries();
  }

  @Override
  public Object script(String name, Object... args) {
    if (name.equals("farming_resources")) {
      return 8;
    } else if (name.equals("animation")) {
      var hatchet = getHatchet((Player) args[0]);
      return hatchet != null ? hatchet.getAnimation() : -1;
    }
    return null;
  }

  @Override
  public int getEventTick(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    return 3;
  }

  @Override
  public void eventStarted(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    player.getGameEncoder().sendMessage("You swing your axe at the tree.");
  }

  @Override
  public void eventStopped(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    player.setAnimation(-1);
  }

  @Override
  public void actionSuccess(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    setTemporaryMapObject(player, mapObject, entry);
    var nestChance = 128;
    if (player.getEquipment().wearingAccomplishmentCape(getSkillId())) {
      nestChance /= 1.1;
    }
    if (PRandom.randomE(nestChance - entry.getLevel()) == 0) {
      player.getController().addMapItem(PRandom.arrayRandom(COLORED_EGG_NESTS), player, player);
      player.getGameEncoder().sendMessage("<col=ff0000>A bird's nest falls out of the tree.</col>");
    }
  }

  @Override
  public void clueRolled(Player player, Npc npc, MapObject mapObject, SkillEntry entry) {
    var items =
        RandomItem.buildList(
            new RandomItem(ClueScrollType.EASY.getNestId()).weight(4),
            new RandomItem(ClueScrollType.MEDIUM.getNestId()).weight(3),
            new RandomItem(ClueScrollType.HARD.getNestId()).weight(2),
            new RandomItem(ClueScrollType.ELITE.getNestId()).weight(1));
    var clueItem = RandomItem.getItem(items);
    player.getInventory().addOrDropItem(clueItem);
  }

  @Override
  public Item createHook(
      Player player, PEvent event, Item item, Npc npc, MapObject mapObject, SkillEntry entry) {
    var fireContainer = SkillContainer.getBySkillId(Skills.FIREMAKING);
    var fireEntry = fireContainer.findEntryFromConsume(item.getId());
    if (getHatchet(player).getItemId() == ItemId.INFERNAL_AXE
        && fireEntry != null
        && PRandom.randomE(3) == 0) {
      var fireXp =
          fireContainer.experienceHook(
              player, event, fireEntry.getExperience(), npc, mapObject, fireEntry);
      player.setGraphic(86, 100);
      player.getSkills().addXp(Skills.FIREMAKING, fireXp / 2);
      player.getCharges().degradeItem(ItemId.INFERNAL_AXE);
      fireContainer.rollPet(player, fireEntry);
      if (PRandom.randomE(Math.max(100 - fireEntry.getLevel(), 20)) == 0) {
        player.getInventory().addOrDropItem(ItemId.SUPPLY_CRATE);
      }
      return null;
    }
    player.getGameEncoder().sendMessage("You get some " + item.getInfoDef().getLCName() + ".");
    if (player.getPlugin(BondPlugin.class).isRelicUnlocked(BondRelicType.BIGGER_HARVEST_WOODCUTTING)
        && PRandom.randomE(2) == 0) {
      item = new Item(item.getId(), item.getAmount() * 2);
    }
    return item;
  }

  @Override
  public int experienceHook(
      Player player, PEvent event, int experience, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (player.getEquipment().wearingLumberjackOutfit()) {
      experience *= 1.1;
    }
    return experience;
  }

  @Override
  public int animationHook(
      Player player, int animation, Npc npc, MapObject mapObject, SkillEntry entry) {
    return getHatchet(player).getAnimation();
  }

  @Override
  public boolean canDoActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (getHatchet(player) == null) {
      player.getGameEncoder().sendMessage("You need an axe to do this.");
      return false;
    }
    return true;
  }

  @Override
  public boolean skipActionHook(
      Player player, PEvent event, Npc npc, MapObject mapObject, SkillEntry entry) {
    var power =
        (player.getSkills().getLevel(getSkillId()) / 2) + (getHatchet(player).getLevel() / 2) + 8;
    var failure = entry.getLevel() + 2;
    if (player.inWildernessResourceArea()) {
      failure /= 2;
    }
    if (inWoodcuttingGuild(player)) {
      power += 7;
    }
    var chance = 0;
    if (power > failure) {
      chance = (int) ((1 - (failure + 2) / ((power + 1) * 2.0)) * 100);
    } else {
      chance = (int) ((power / ((failure + 1) * 2.0)) * 100);
    }
    chance *= 1 + getMembershipBoost(player);
    if (player.getEquipment().wearingLumberjackOutfit()) {
      chance += 10;
    }
    chance = Math.min(chance, 100);
    return PRandom.randomE(100) > chance;
  }

  @Override
  public int clueChanceHook(
      Player player, int chance, Npc npc, MapObject mapObject, SkillEntry entry) {
    if (player.getEquipment().wearingLumberjackOutfit()) {
      return (int) (chance / 1.1);
    }
    return chance;
  }
}
