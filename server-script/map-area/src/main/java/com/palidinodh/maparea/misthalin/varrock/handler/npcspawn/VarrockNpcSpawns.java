package com.palidinodh.maparea.misthalin.varrock.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class VarrockNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3229, 3454), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(2, new Tile(3226, 3457), NpcId.TREZNOR));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3187, 3446), NpcId.BANKER_8666));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3187, 3444), NpcId.BANKER_2897));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3187, 3442), NpcId.BANKER_2898));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3187, 3440), NpcId.BANKER_2897));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3187, 3438), NpcId.BANKER_2898));
    spawns.add(new NpcSpawn(Tile.Direction.WEST, new Tile(3187, 3436), NpcId.BANKER_2897));
    spawns.add(new NpcSpawn(4, new Tile(3188, 3423), NpcId.MASTER_SMITHING_TUTOR));
    spawns.add(new NpcSpawn(4, new Tile(3203, 3435), NpcId.ZAFF));
    spawns.add(new NpcSpawn(2, new Tile(3203, 3424), NpcId.GYPSY_ARIS));
    spawns.add(new NpcSpawn(4, new Tile(3212, 3427), NpcId.ROMEO));
    spawns.add(new NpcSpawn(4, new Tile(3217, 3435), NpcId.BARAEK));
    spawns.add(new NpcSpawn(4, new Tile(3220, 3432), NpcId.BENNY));
    spawns.add(new NpcSpawn(4, new Tile(3218, 3415), NpcId.SHOP_KEEPER_508));
    spawns.add(new NpcSpawn(4, new Tile(3216, 3414), NpcId.SHOP_ASSISTANT_509));
    spawns.add(new NpcSpawn(4, new Tile(3180, 3401), NpcId.GUARD_21_3010));
    spawns.add(new NpcSpawn(4, new Tile(3194, 3395), NpcId.MAN_2_3108));
    spawns.add(new NpcSpawn(4, new Tile(3196, 3404), NpcId.APOTHECARY));
    spawns.add(new NpcSpawn(4, new Tile(3206, 3415), NpcId.THESSALIA));
    spawns.add(new NpcSpawn(new Tile(3204, 3419), NpcId.IFFIE));
    spawns.add(new NpcSpawn(4, new Tile(3206, 3397), NpcId.SHOP_KEEPER_537));
    spawns.add(new NpcSpawn(4, new Tile(3206, 3401), NpcId.SHOP_ASSISTANT_538));
    spawns.add(new NpcSpawn(4, new Tile(3209, 3392), NpcId.CHARLIE_THE_TRAMP));
    spawns.add(new NpcSpawn(4, new Tile(3226, 3399), NpcId.BARTENDER_1312));
    spawns.add(new NpcSpawn(4, new Tile(3223, 3401), NpcId.BARBARIAN_8));
    spawns.add(new NpcSpawn(4, new Tile(3224, 3395), NpcId.DR_HARLOW));
    spawns.add(new NpcSpawn(4, new Tile(3219, 3395), NpcId.JONNY_THE_BEARD_2));
    spawns.add(new NpcSpawn(4, new Tile(3219, 3399), NpcId.WOMAN_2));
    spawns.add(new NpcSpawn(4, new Tile(3242, 3389), NpcId.TRAMP));
    spawns.add(new NpcSpawn(4, new Tile(3234, 3390), NpcId.THIEF_16_5217));
    spawns.add(new NpcSpawn(4, new Tile(3253, 3401), NpcId.AUBURY));
    spawns.add(new NpcSpawn(4, new Tile(3237, 3407), NpcId.MAN_2_3106));
    spawns.add(new NpcSpawn(4, new Tile(3237, 3404), NpcId.MAN_2_3108));
    spawns.add(new NpcSpawn(4, new Tile(3261, 3398), NpcId.MAN_2_3107));
    spawns.add(new NpcSpawn(new Tile(3271, 3411), NpcId.TEA_SELLER));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3256, 3418), NpcId.BANKER_2898));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3255, 3418), NpcId.BANKER_2898));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3254, 3418), NpcId.BANKER_2898));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3253, 3418), NpcId.BANKER_2897));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3252, 3418), NpcId.BANKER_2897));
    spawns.add(new NpcSpawn(Tile.Direction.NORTH, new Tile(3251, 3418), NpcId.BANKER_2897));
    spawns.add(new NpcSpawn(8, new Tile(3253, 3425), NpcId.CLEANER));
    spawns.add(new NpcSpawn(4, new Tile(3254, 3428), NpcId.TOWN_CRIER));
    spawns.add(new NpcSpawn(4, new Tile(3270, 3427), NpcId.GUARD_21_3010));
    spawns.add(new NpcSpawn(4, new Tile(3274, 3430), NpcId.GUARD_21_3010));
    spawns.add(new NpcSpawn(4, new Tile(3276, 3426), NpcId.GUARD_21_3010));
    spawns.add(new NpcSpawn(4, new Tile(3272, 3431), NpcId.GUARD_21_3011));
    spawns.add(new NpcSpawn(new Tile(3263, 3441), NpcId.MUSEUM_GUARD_1912));
    spawns.add(new NpcSpawn(4, new Tile(3232, 3424), NpcId.LOWE));
    spawns.add(new NpcSpawn(4, new Tile(3229, 3437), NpcId.HORVIK));
    spawns.add(new NpcSpawn(4, new Tile(3151, 3412), NpcId.GERTRUDE));
    spawns.add(new NpcSpawn(4, new Tile(3160, 3435), NpcId.DRAUL_LEPTOC));
    spawns.add(new NpcSpawn(4, new Tile(3152, 3430), NpcId.MAN_2));
    spawns.add(new NpcSpawn(4, new Tile(3173, 3427), NpcId.GUARD_21_3010));
    spawns.add(new NpcSpawn(4, new Tile(3177, 3430), NpcId.GUARD_21_3010));
    spawns.add(new NpcSpawn(4, new Tile(3179, 3425), NpcId.GUARD_21_3010));

    return spawns;
  }
}
