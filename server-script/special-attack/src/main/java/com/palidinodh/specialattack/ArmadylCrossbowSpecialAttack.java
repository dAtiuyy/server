package com.palidinodh.specialattack;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.SpecialAttack;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.ARMADYL_CROSSBOW,
  ItemId.ARMADYL_CROSSBOW_23611,
  ItemId.ARMADYL_CROSSBOW_OR_60049
})
class ArmadylCrossbowSpecialAttack extends SpecialAttack {

  ArmadylCrossbowSpecialAttack() {
    var entry = Entry.builder();
    entry.drain(50);
    entry.animation(4230);
    entry.projectileId(301);
    entry.castSound(new Sound(3892));
    entry.accuracyModifier(2);
    addEntry(entry);
  }
}
