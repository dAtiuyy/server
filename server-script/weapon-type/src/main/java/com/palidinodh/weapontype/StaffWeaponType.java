package com.palidinodh.weapontype;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.Sound;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponAttackSet;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponConfigType;
import com.palidinodh.osrscore.model.entity.player.combat.weapon.WeaponType;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ItemId.STAFF,
  ItemId.STAFF_OF_AIR,
  ItemId.STAFF_OF_WATER,
  ItemId.STAFF_OF_EARTH,
  ItemId.STAFF_OF_FIRE,
  ItemId.MAGIC_STAFF,
  ItemId.BATTLESTAFF,
  ItemId.FIRE_BATTLESTAFF,
  ItemId.WATER_BATTLESTAFF,
  ItemId.AIR_BATTLESTAFF,
  ItemId.EARTH_BATTLESTAFF,
  ItemId.MYSTIC_FIRE_STAFF,
  ItemId.MYSTIC_WATER_STAFF,
  ItemId.MYSTIC_AIR_STAFF,
  ItemId.MYSTIC_EARTH_STAFF,
  ItemId.IBANS_STAFF,
  ItemId.LAVA_BATTLESTAFF,
  ItemId.MYSTIC_LAVA_STAFF,
  ItemId.MUD_BATTLESTAFF,
  ItemId.MYSTIC_MUD_STAFF,
  ItemId.WHITE_MAGIC_STAFF,
  ItemId.LUNAR_STAFF,
  ItemId.SARADOMIN_CROZIER,
  ItemId.GUTHIX_CROZIER,
  ItemId.ZAMORAK_CROZIER,
  ItemId.STEAM_BATTLESTAFF,
  ItemId.MYSTIC_STEAM_STAFF,
  ItemId.SMOKE_BATTLESTAFF,
  ItemId.MYSTIC_SMOKE_STAFF,
  ItemId.ANCIENT_CROZIER,
  ItemId.ARMADYL_CROZIER,
  ItemId.BANDOS_CROZIER,
  ItemId.IBANS_STAFF_U,
  ItemId.STEAM_BATTLESTAFF_12795,
  ItemId.MYSTIC_STEAM_STAFF_12796,
  ItemId.MIST_BATTLESTAFF,
  ItemId.MYSTIC_MIST_STAFF,
  ItemId.DUST_BATTLESTAFF,
  ItemId.MYSTIC_DUST_STAFF,
  ItemId.LAVA_BATTLESTAFF_21198,
  ItemId.MYSTIC_LAVA_STAFF_21200,
  ItemId.BRYOPHYTAS_STAFF_UNCHARGED,
  ItemId.BRYOPHYTAS_STAFF,
  ItemId.ZURIELS_STAFF,
  ItemId._3RD_AGE_DRUIDIC_STAFF,
  ItemId.STAFF_OF_BOB_THE_CAT,
  ItemId.ZURIELS_STAFF_23617,
  ItemId.CORRUPTED_SCEPTRE,
  ItemId.CRYSTAL_SCEPTRE,
  ItemId.NIGHTMARE_STAFF,
  ItemId.HARMONISED_NIGHTMARE_STAFF,
  ItemId.VOLATILE_NIGHTMARE_STAFF,
  ItemId.ELDRITCH_NIGHTMARE_STAFF,
  ItemId.ZURIELS_STAFF_CHARGED_32257
})
class Staff5TickWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_18);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.attackSpeed(5);
    type.defendAnimation(420);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(393).attackSound(new Sound(2555)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.ROD_OF_IVANDIS_10,
  ItemId.ROD_OF_IVANDIS_9,
  ItemId.ROD_OF_IVANDIS_8,
  ItemId.ROD_OF_IVANDIS_7,
  ItemId.ROD_OF_IVANDIS_6,
  ItemId.ROD_OF_IVANDIS_5,
  ItemId.ROD_OF_IVANDIS_4,
  ItemId.ROD_OF_IVANDIS_3,
  ItemId.ROD_OF_IVANDIS_2,
  ItemId.ROD_OF_IVANDIS_1,
  ItemId.IVANDIS_FLAIL,
  ItemId.BLISTERWOOD_FLAIL
})
class FlailWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_18);
    type.renderAnimations(new int[] {8009, 8015, 8011, 8012, 8013, 8014, 8016});
    type.equipSound(new Sound(2238));
    type.attackSpeed(5);
    type.defendAnimation(8017);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(8010).attackSound(new Sound(2555)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.SLAYERS_STAFF,
  ItemId.ANCIENT_STAFF,
  ItemId.CURSED_GOBLIN_STAFF,
  ItemId.ANCIENT_STAFF_20431,
  ItemId.SLAYERS_STAFF_E,
  ItemId.THAMMARONS_SCEPTRE_U,
  ItemId.THAMMARONS_SCEPTRE,
  ItemId.BLIGHTED_ANCIENT_STAFF_32372
})
class Staff4TickWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_18);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.attackSpeed(4);
    type.defendAnimation(420);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(393).attackSound(new Sound(2555)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.BEGINNER_WAND,
  ItemId.APPRENTICE_WAND,
  ItemId.TEACHER_WAND,
  ItemId.MASTER_WAND,
  ItemId._3RD_AGE_WAND,
  ItemId.BEGINNER_WAND_20553,
  ItemId.APPRENTICE_WAND_20556,
  ItemId.MASTER_WAND_20560,
  ItemId.KODAI_WAND,
  ItemId.KODAI_WAND_23626,
  ItemId.MASTER_WAND_OR_60050
})
class WandWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_18);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.attackSpeed(4);
    type.defendAnimation(424);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(393).attackSound(new Sound(2563)).build());
    return type;
  }
}

@ReferenceId({ItemId.VOID_KNIGHT_MACE, ItemId.VOID_KNIGHT_MACE_L})
class VoidKnightMaceWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_18);
    type.equipSound(new Sound(2238));
    type.attackSpeed(5);
    type.defendAnimation(420);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(393).attackSound(new Sound(2555)).build());
    return type;
  }
}

@ReferenceId({ItemId.SARADOMIN_STAFF, ItemId.GUTHIX_STAFF, ItemId.ZAMORAK_STAFF})
class GodStaffWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_18);
    type.equipSound(new Sound(2238));
    type.attackSpeed(5);
    type.defendAnimation(420);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(393).attackSound(new Sound(2555)).build());
    return type;
  }
}

@ReferenceId({
  ItemId.AHRIMS_STAFF,
  ItemId.AHRIMS_STAFF_100,
  ItemId.AHRIMS_STAFF_75,
  ItemId.AHRIMS_STAFF_50,
  ItemId.AHRIMS_STAFF_25,
  ItemId.AHRIMS_STAFF_23653
})
class AhrimsStaffWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_18);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.attackSpeed(6);
    type.defendAnimation(420);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(393).attackSound(new Sound(2555)).build());
    return type;
  }
}

@ReferenceId(ItemId.TOKTZ_MEJ_TAL)
class ToktzMejTalWeaponType implements WeaponType.BuildType {

  @Override
  public WeaponType.WeaponTypeBuilder builder() {
    var type = WeaponType.builder();
    type.config(WeaponConfigType.TYPE_18);
    type.renderAnimations(new int[] {813, 1209, 1205, 1206, 1207, 1208, 1210});
    type.equipSound(new Sound(2238));
    type.twoHanded(true);
    type.attackSpeed(6);
    type.defendAnimation(420);
    type.attackSet(
        WeaponAttackSet.builder().attackAnimation(393).attackSound(new Sound(2555)).build());
    return type;
  }
}
