package com.palidinodh.maparea.feldiphills.corsaircove;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({10028, 10284})
public class CorsairCoveArea extends Area {}
