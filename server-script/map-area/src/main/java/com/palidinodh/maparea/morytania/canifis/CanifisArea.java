package com.palidinodh.maparea.morytania.canifis;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(13878)
public class CanifisArea extends Area {}
