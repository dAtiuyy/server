package com.palidinodh.maparea.asgarnia.rimmington.handler.npcspawn;

import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawn;
import com.palidinodh.osrscore.model.entity.npc.spawn.NpcSpawnHandler;
import com.palidinodh.osrscore.model.tile.Tile;
import java.util.ArrayList;
import java.util.List;

class RimmingtonNpcSpawns implements NpcSpawnHandler {

  @Override
  public List<NpcSpawn> getSpawns() {
    var spawns = new ArrayList<NpcSpawn>();

    spawns.add(new NpcSpawn(4, new Tile(2968, 3205), NpcId.HETTY));
    spawns.add(new NpcSpawn(4, new Tile(2955, 3203), NpcId.BRIAN_1309));
    spawns.add(new NpcSpawn(2, new Tile(2949, 3205), NpcId.ROMMIK));
    spawns.add(new NpcSpawn(2, new Tile(2949, 3212), NpcId.PHIALS));
    spawns.add(new NpcSpawn(new Tile(2947, 3217), NpcId.SHOP_KEEPER_516));
    spawns.add(new NpcSpawn(Tile.Direction.EAST, new Tile(2948, 3217), NpcId.SHOP_ASSISTANT));
    spawns.add(new NpcSpawn(8, new Tile(2947, 3221), NpcId.IMP_7));
    spawns.add(new NpcSpawn(new Tile(2940, 3225), NpcId.TOOL_LEPRECHAUN));
    spawns.add(new NpcSpawn(2, new Tile(2943, 3224), NpcId.TARIA));
    spawns.add(new NpcSpawn(8, new Tile(2937, 3234), NpcId.IMP_7));
    spawns.add(new NpcSpawn(new Tile(2929, 3222), NpcId.CHANCY));
    spawns.add(new NpcSpawn(2, new Tile(2931, 3220), NpcId.HOPS));
    spawns.add(new NpcSpawn(2, new Tile(2928, 3218), NpcId.DA_VINCI));
    spawns.add(new NpcSpawn(4, new Tile(2932, 3209), NpcId.CHEMIST));

    return spawns;
  }
}
