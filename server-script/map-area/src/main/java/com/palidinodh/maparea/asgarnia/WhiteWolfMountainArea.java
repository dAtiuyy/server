package com.palidinodh.maparea.asgarnia;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId(11318)
@ReferenceIdSet(
    primary = 11062,
    secondary = {101, 102, 103, 117, 118, 119})
@ReferenceIdSet(
    primary = 11317,
    secondary = {71, 86, 87, 100, 101, 102, 103, 112, 113, 114, 115, 116, 117, 118, 119})
public class WhiteWolfMountainArea extends Area {}
