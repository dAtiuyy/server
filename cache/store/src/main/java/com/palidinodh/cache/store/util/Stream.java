package com.palidinodh.cache.store.util;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("resource")
public class Stream extends OutputStream {

  public static final int MAX_STRING_LENGTH = 16384;
  public static final char[] SPECIAL_CHARACTERS = {
    '\u20ac', '\u0000', '\u201a', '\u0192', '\u201e', '\u2026', '\u2020', '\u2021', '\u02c6',
    '\u2030', '\u0160', '\u2039', '\u0152', '\u0000', '\u017d', '\u0000', '\u0000', '\u2018',
    '\u2019', '\u201c', '\u201d', '\u2022', '\u2013', '\u2014', '\u02dc', '\u2122', '\u0161',
    '\u203a', '\u0153', '\u0000', '\u017e', '\u0178'
  };
  private static final int[] BIT_MASK = new int[32];

  static {
    for (int i = 0; i < 32; i++) {
      BIT_MASK[i] = (1 << i) - 1;
    }
  }

  private int position;
  private int length;
  private byte[] buffer;
  private int bitPosition;
  private int opcodeStart;
  private int mark;

  public Stream() {
    this(1024);
  }

  public Stream(int initialCapacity) {
    buffer = new byte[initialCapacity];
  }

  public Stream(byte[] buffer) {
    this.buffer = buffer;
    length = buffer.length;
  }

  @Override
  public void write(int b) throws IOException {
    checkCapacity(position + 1);
    buffer[position++] = (byte) b;
  }

  public int getBufferLength() {
    return buffer.length;
  }

  public int getMark() {
    return mark;
  }

  public int getPosition() {
    return position;
  }

  public void setPosition(int position) {
    if (position < mark) {
      mark = 0;
    }
    this.position = position;
  }

  public int getBitPosition() {
    return bitPosition;
  }

  public int getLength() {
    return length;
  }

  public void setLength(int length) {
    if (length < mark) {
      mark = 0;
    }
    this.length = length;
  }

  public byte[] getArray() {
    return buffer;
  }

  public int available() {
    return position < length && position < buffer.length ? length - position : 0;
  }

  public byte[] toByteArray() {
    return Arrays.copyOf(buffer, position);
  }

  public byte[] getRemaining() {
    byte[] bytes = new byte[length - position];
    System.arraycopy(buffer, position, bytes, 0, bytes.length);
    return bytes;
  }

  public void clear() {
    position = length = mark = 0;
  }

  public void mark() {
    mark = position;
  }

  public void reset() {
    position = mark;
  }

  public void skip(int l) {
    position += l;
  }

  public void shift(int p) {
    if (p < 0 || p >= buffer.length) {
      return;
    }
    byte[] newBuffer = new byte[buffer.length];
    length = buffer.length - p;
    System.arraycopy(buffer, p, newBuffer, 0, length);
    buffer = newBuffer;
    position = 0;
    mark = 0;
  }

  public void checkCapacity(int l) {
    if (l < buffer.length) {
      return;
    }
    buffer = Arrays.copyOf(buffer, l * 2);
  }

  public void appendBytes(byte[] b) {
    appendBytes(b, 0, b.length);
  }

  public void appendBytes(byte[] b, int p, int count) {
    checkCapacity(length + count);
    System.arraycopy(b, p, buffer, length, count);
    length += count;
  }

  public void appendBytes(int index, byte[] b) {
    appendBytes(index, b, 0, b.length);
  }

  public void appendBytes(int index, byte[] b, int p, int count) {
    if (index < 0 || index > length) {
      return;
    }
    checkCapacity(length + count);
    int numMoved = length - index;
    if (numMoved > 0) {
      System.arraycopy(buffer, index, buffer, index + count, numMoved);
    }
    System.arraycopy(b, p, buffer, index, count);
    length += count;
  }

  public byte getByte(int p) {
    return p >= 0 && p < length ? buffer[p] : 0;
  }

  public void setByte(int p, int i) {
    checkCapacity(p);
    buffer[p] = (byte) i;
  }

  public int getBitsAvailable(int l) {
    return l * 8 - position;
  }

  public void writeByte(int i) {
    setByte(position++, i);
  }

  public void writeByteA(int i) {
    writeByte(i + 128);
  }

  public void writeByteS(int i) {
    writeByte(128 - i);
  }

  public void writeByteC(int i) {
    writeByte(-i);
  }

  public void writeBoolean(boolean b) {
    writeByte(b ? 1 : 0);
  }

  public void writeBytes(byte[] b) {
    writeBytes(b, 0, b.length);
  }

  public void writeBytes(byte[] b, int p, int count) {
    checkCapacity(position + count);
    System.arraycopy(b, p, buffer, position, count);
    position += count;
  }

  public void writeBytesReversed(byte[] b) {
    writeBytesReversed(b, 0, b.length);
  }

  public void writeBytesReversed(byte[] b, int p, int count) {
    for (int i = count + p - 1; i >= p; i--) {
      writeByte(b[i]);
    }
  }

  public void writeBytesReversedA(byte[] b) {
    writeBytesReversedA(b, 0, b.length);
  }

  public void writeBytesReversedA(byte[] b, int p, int count) {
    for (int i = count + p - 1; i >= p; i--) {
      writeByte(b[i] + 128);
    }
  }

  public void writeBytesA(byte[] b) {
    writeBytesA(b, 0, b.length);
  }

  public void writeBytesA(byte[] b, int p, int count) {
    for (int i = p; i < count + p; i++) {
      writeByteA(b[i]);
    }
  }

  public void writeShort(int i) {
    writeByte(i >> 8);
    writeByte(i);
  }

  public void writeLEShort(int i) {
    writeByte(i);
    writeByte(i >> 8);
  }

  public void writeShortA(int i) {
    writeByte(i >> 8);
    writeByte(i + 128);
  }

  public void writeLEShortA(int i) {
    writeByte(i + 128);
    writeByte(i >> 8);
  }

  public void writeSmart(int i) {
    if (i >= 0 && i < 128) {
      writeByte(i);
    } else if (i >= 0 && i < 32768) {
      writeShort(i + 32768);
    } else {
      throw new IllegalArgumentException("invalid parameter: " + i);
    }
  }

  public void writeBigSmart(int i) {
    if (i >= Short.MAX_VALUE) {
      writeInt(i - Integer.MAX_VALUE - 1);
    } else {
      writeShort(i >= 0 ? i : 32767);
    }
  }

  public void writeHugeSmart(int i) {
    if (i < 32767) {
      writeSmart(i);
    } else {
      int divisions = i / 32767;
      for (int i2 = 0; i2 < divisions; i2++) {
        writeSmart(32767);
      }
      writeSmart(i - 32767 * divisions);
    }
  }

  public void writeTriByte(int i) {
    writeByte(i >> 16);
    writeByte(i >> 8);
    writeByte(i);
  }

  public void writeLETriByte(int i) {
    writeByte(i);
    writeByte(i >> 8);
    writeByte(i >> 16);
  }

  public void writeTriByte1(int i) {
    writeByte(i >> 8);
    writeByte(i >> 16);
    writeByte(i);
  }

  public void writeInt(int i) {
    writeByte(i >> 24);
    writeByte(i >> 16);
    writeByte(i >> 8);
    writeByte(i);
  }

  public void writeLEInt(int i) {
    writeByte(i);
    writeByte(i >> 8);
    writeByte(i >> 16);
    writeByte(i >> 24);
  }

  public void writeInt1(int i) {
    writeByte(i >> 8);
    writeByte(i);
    writeByte(i >> 24);
    writeByte(i >> 16);
  }

  public void writeInt2(int i) {
    writeByte(i >> 16);
    writeByte(i >> 24);
    writeByte(i);
    writeByte(i >> 8);
  }

  public void writePentaByte(int i) {
    writeByte(i >> 32);
    writeByte(i >> 24);
    writeByte(i >> 16);
    writeByte(i >> 8);
    writeByte(i);
  }

  public void writeHexaByte(int i) {
    writeByte(i >> 40);
    writeByte(i >> 32);
    writeByte(i >> 24);
    writeByte(i >> 16);
    writeByte(i >> 8);
    writeByte(i);
  }

  public void writeDouble(Double d) {
    writeLong(Double.doubleToLongBits(d));
  }

  public void writeLong(long l) {
    writeByte((int) (l >> 56));
    writeByte((int) (l >> 48));
    writeByte((int) (l >> 40));
    writeByte((int) (l >> 32));
    writeByte((int) (l >> 24));
    writeByte((int) (l >> 16));
    writeByte((int) (l >> 8));
    writeByte((int) l);
  }

  public void writeString(String s) {
    if (s.length() > MAX_STRING_LENGTH) {
      s = s.substring(0, MAX_STRING_LENGTH);
    }
    checkCapacity(position + s.length() + 1);
    System.arraycopy(s.getBytes(), 0, buffer, position, s.length());
    position += s.length();
    writeByte(0);
  }

  public void WriteJString(String s) {
    writeByte(0);
    writeString(s);
  }

  public void writeOpcode(int id) {
    writeEncryptedByte(id);
  }

  public void writeOpcodeVarByte(int id) {
    writeOpcode(id);
    writeByte(0);
    opcodeStart = position - 1;
  }

  public void writeOpcodeVarShort(int id) {
    writeOpcode(id);
    writeShort(0);
    opcodeStart = position - 2;
  }

  public void writeOpcodeVarInt(int id) {
    writeOpcode(id);
    writeInt(0);
    opcodeStart = position - 4;
  }

  public void endOpcodeVarByte() {
    int size = position - (opcodeStart + 1);
    if (size > 255) {
      throw new IndexOutOfBoundsException("size > 255");
    }
    setByte(opcodeStart, size);
  }

  public void endOpcodeVarShort() {
    int size = position - (opcodeStart + 2);
    if (size > 65535) {
      throw new IndexOutOfBoundsException("size > 65535");
    }
    setByte(opcodeStart++, size >> 8);
    setByte(opcodeStart, size);
  }

  public void endOpcodeVarInt() {
    int size = position - (opcodeStart + 4);
    if (size < 0) {
      throw new IndexOutOfBoundsException("size < 0");
    }
    setByte(opcodeStart++, size >> 24);
    setByte(opcodeStart++, size >> 16);
    setByte(opcodeStart++, size >> 8);
    setByte(opcodeStart, size);
  }

  public void writeEncryptedByte(int id) {
    writeByte(id);
  }

  public void writeEncryptedBytes(byte[] b) {
    writeEncryptedBytes(b, 0, b.length);
  }

  public void writeEncryptedBytes(byte[] b, int p, int count) {
    checkCapacity(position + count);
    for (int i = p; i < count; i++) {
      writeEncryptedByte(b[i]);
    }
  }

  public void startBitAccess() {
    bitPosition = position * 8;
  }

  public void endBitAccess() {
    position = (bitPosition + 7) / 8;
  }

  public void writeBits(int numBits, int value) {
    int bytePos = bitPosition >> 3;
    int bitposition = 8 - (bitPosition & 7);
    bitPosition += numBits;
    for (; numBits > bitposition; bitposition = 8) {
      checkCapacity(bytePos);
      buffer[bytePos] &= ~BIT_MASK[bitposition];
      buffer[bytePos++] |= value >> numBits - bitposition & BIT_MASK[bitposition];
      numBits -= bitposition;
    }
    checkCapacity(bytePos);
    if (numBits == bitposition) {
      buffer[bytePos] &= ~BIT_MASK[bitposition];
      buffer[bytePos] |= value & BIT_MASK[bitposition];
    } else {
      buffer[bytePos] &= ~(BIT_MASK[numBits] << bitposition - numBits);
      buffer[bytePos] |= (value & BIT_MASK[numBits]) << bitposition - numBits;
    }
  }

  public void writeScript(Map<Integer, Object> script) {
    writeByte(script.size());
    for (Map.Entry<Integer, Object> entry : script.entrySet()) {
      writeByte(entry.getValue() instanceof String ? 1 : 0);
      writeTriByte(entry.getKey());
      if (entry.getValue() instanceof String) {
        writeString((String) entry.getValue());
      } else {
        int value = 0;
        if (entry.getValue() instanceof Integer) {
          value = (int) entry.getValue();
        } else if (entry.getValue() instanceof Double) {
          value = (int) (double) entry.getValue();
        }
        writeInt(value);
      }
    }
  }

  public byte[] readBytes(int count) {
    byte[] b = new byte[count];
    readBytes(b);
    return b;
  }

  public void readBytes(byte[] b) {
    readBytes(b, 0, b.length);
  }

  public void readBytes(byte[] b, int p, int count) {
    System.arraycopy(buffer, position, b, p, count);
    position += count;
  }

  public void readBytesA(byte[] b, int p, int count) {
    for (int i = p; i < count + p; i++) {
      b[i] = readByteA();
    }
  }

  public void readBytesReversed(byte[] b, int p, int count) {
    for (int i = count + p - 1; i >= p; i--) {
      b[i] = readByte();
    }
  }

  public void readBytesReversedA(byte[] b, int p, int count) {
    for (int i = count + p - 1; i >= p; i--) {
      b[i] = readByteA();
    }
  }

  public byte readByte() {
    return getByte(position++);
  }

  public int readUnsignedByte() {
    return readByte() & 255;
  }

  public byte readByteA() {
    return (byte) (readByte() - 128);
  }

  public int readUnsignedByteA() {
    return readUnsignedByte() - 128 & 255;
  }

  public byte readByteC() {
    return (byte) -readByte();
  }

  public int readUnsignedByteC() {
    return -readUnsignedByte() & 255;
  }

  public byte readByteS() {
    return (byte) (128 - readByte());
  }

  public int readUnsignedByteS() {
    return 128 - readUnsignedByte() & 255;
  }

  public boolean readBoolean() {
    return (readUnsignedByte() & 1) == 1;
  }

  public short readShort() {
    int i = (readUnsignedByte() << 8) + readUnsignedByte();
    if (i > 32767) {
      i -= 65536;
    }
    return (short) i;
  }

  public int readUnsignedShort() {
    return (readUnsignedByte() << 8) + readUnsignedByte();
  }

  public short readLEShort() {
    int i = readUnsignedByte() + (readUnsignedByte() << 8);
    if (i > 32767) {
      i -= 65536;
    }
    return (short) i;
  }

  public int readUnsignedLEShort() {
    return readUnsignedByte() + (readUnsignedByte() << 8);
  }

  public short readShortA() {
    int i = (readUnsignedByte() << 8) + (readByte() - 128 & 255);
    if (i > 32767) {
      i -= 65536;
    }
    return (short) i;
  }

  public int readUnsignedShortA() {
    return (readUnsignedByte() << 8) + (readByte() - 128 & 255);
  }

  public short readLEShortA() {
    int i = (readByte() - 128 & 255) + (readUnsignedByte() << 8);
    if (i > 32767) {
      i -= 65536;
    }
    return (short) i;
  }

  public int readUnsignedLEShortA() {
    return (readByte() - 128 & 255) + (readUnsignedByte() << 8);
  }

  public int readSmart() {
    int i = available() > 0 ? buffer[position] & 255 : 0;
    return i < 128 ? readUnsignedByte() - 64 : readUnsignedShort() - 49152;
  }

  public int readUnsignedSmart() {
    int i = available() > 0 ? buffer[position] & 255 : 0;
    return i < 128 ? readUnsignedByte() : readUnsignedShort() - 32768;
  }

  public int readBigSmart() {
    int i = available() > 0 ? buffer[position] : 0;
    if (i < 0) {
      return readInt() & Integer.MAX_VALUE;
    }
    i = readUnsignedShort();
    return i == 32767 ? -1 : i;
  }

  public int readUnsignedBigSmart() {
    return (available() > 0 ? buffer[position] : 0) < 0
        ? readInt() & Integer.MAX_VALUE
        : readUnsignedShort();
  }

  public int readHugeSmart() {
    int baseVal = 0;
    int lastVal;
    while ((lastVal = readUnsignedSmart()) == 32767) {
      baseVal += 32767;
    }
    return baseVal + lastVal;
  }

  public int readTriByte() {
    return (readUnsignedByte() << 16) + (readUnsignedByte() << 8) + readUnsignedByte();
  }

  public int readLETriByte() {
    return readUnsignedByte() + (readUnsignedByte() << 8) + (readUnsignedByte() << 16);
  }

  public int readTriByte1() {
    return (readUnsignedByte() << 8) + (readUnsignedByte() << 16) + readUnsignedByte();
  }

  public int readInt() {
    return (readUnsignedByte() << 24)
        + (readUnsignedByte() << 16)
        + (readUnsignedByte() << 8)
        + readUnsignedByte();
  }

  public int readLEInt() {
    return readUnsignedByte()
        + (readUnsignedByte() << 8)
        + (readUnsignedByte() << 16)
        + (readUnsignedByte() << 24);
  }

  public int readInt1() {
    return (readUnsignedByte() << 8)
        + readUnsignedByte()
        + (readUnsignedByte() << 24)
        + (readUnsignedByte() << 16);
  }

  public int readInt2() {
    return (readUnsignedByte() << 16)
        + (readUnsignedByte() << 24)
        + readUnsignedByte()
        + (readUnsignedByte() << 8);
  }

  public int readPentaByte() {
    return (readUnsignedByte() << 32)
        + (readUnsignedByte() << 24)
        + (readUnsignedByte() << 16)
        + (readUnsignedByte() << 8)
        + readUnsignedByte();
  }

  public int readHexaByte() {
    return (readUnsignedByte() << 40)
        + (readUnsignedByte() << 32)
        + (readUnsignedByte() << 24)
        + (readUnsignedByte() << 16)
        + (readUnsignedByte() << 8)
        + readUnsignedByte();
  }

  public long readLong() {
    long l = readInt() & 0xffffffffL;
    long l1 = readInt() & 0xffffffffL;
    return (l << 32) + l1;
  }

  public String readString() {
    int aChar;
    StringBuilder stringBuilder = new StringBuilder();
    while ((aChar = readByte()) != 0 && stringBuilder.length() < MAX_STRING_LENGTH) {
      stringBuilder.append((char) aChar);
    }
    return stringBuilder.toString();
  }

  public String readJString() {
    readByte();
    int aChar;
    StringBuilder stringBuilder = new StringBuilder();
    while ((aChar = readByte()) != 0 && stringBuilder.length() < MAX_STRING_LENGTH) {
      stringBuilder.append((char) aChar);
    }
    return stringBuilder.toString();
  }

  public String readSpecialString() {
    int start = position;
    int l = 0;
    while (readByte() != 0 && l < MAX_STRING_LENGTH) {
      l++;
    }
    if (l == 0) {
      return "";
    }
    char[] chars = new char[l];
    int charPos = 0;
    for (int i = 0; i < l; i++) {
      int charByte = buffer[start + i] & 255;
      if (charByte != 0) {
        if (charByte >= 128 && charByte < 160) {
          char specialChar = SPECIAL_CHARACTERS[charByte - 128];
          if (specialChar == 0) {
            specialChar = 63;
          }
          charByte = specialChar;
        }
        chars[charPos++] = (char) charByte;
      }
    }
    return new String(chars, 0, charPos);
  }

  public int readOpcode() {
    return readUnsignedByte();
  }

  public int readBits(int size) {
    int bytePos = position >> 3;
    int bitposition = 8 - (position & 7);
    int value = 0;
    for (position += size; size > bitposition; bitposition = 8) {
      value += (getByte(bytePos++) & BIT_MASK[bitposition]) << size - bitposition;
      size -= bitposition;
    }
    if (bitposition == size) {
      value += getByte(bytePos) & BIT_MASK[bitposition];
    } else {
      value += getByte(bytePos) >> bitposition - size & BIT_MASK[size];
    }
    return value;
  }

  public byte[] decodeRsa(BigInteger exponent, BigInteger modulus) {
    byte[] data = new byte[readShort()];
    readBytes(data);
    if (exponent == null || modulus == null) {
      return new BigInteger(data).toByteArray();
    }
    return new BigInteger(data).modPow(exponent, modulus).toByteArray();
  }

  public void decodeXtea(int[] keys, int start, int end) {
    int l = position;
    position = start;
    int i1 = (end - start) / 8;
    for (int j1 = 0; j1 < i1; j1++) {
      int k1 = readInt();
      int l1 = readInt();
      int sum = 0xc6ef3720;
      int delta = 0x9e3779b9;
      for (int k2 = 32; k2-- > 0; ) {
        l1 -= keys[(sum & 0x1c84) >>> 11] + sum ^ (k1 >>> 5 ^ k1 << 4) + k1;
        sum -= delta;
        k1 -= (l1 >>> 5 ^ l1 << 4) + l1 ^ keys[sum & 3] + sum;
      }
      position -= 8;
      writeInt(k1);
      writeInt(l1);
    }
    position = l;
  }

  public void encodeXtea(int[] keys, int start, int end) {
    int o = position;
    int j = (end - start) / 8;
    position = start;
    for (int k = 0; k < j; k++) {
      int l = readInt();
      int i1 = readInt();
      int sum = 0;
      int delta = 0x9e3779b9;
      for (int l1 = 32; l1-- > 0; ) {
        l += sum + keys[3 & sum] ^ i1 + (i1 >>> 5 ^ i1 << 4);
        sum += delta;
        i1 += l + (l >>> 5 ^ l << 4) ^ keys[(0x1eec & sum) >>> 11] + sum;
      }
      position -= 8;
      writeInt(l);
      writeInt(i1);
    }
    position = o;
  }

  public Map<Integer, Object> readScript() {
    int size = readUnsignedByte();
    Map<Integer, Object> script = new HashMap<>();
    for (int i = 0; i < size; i++) {
      boolean b = readUnsignedByte() == 1;
      int key = readTriByte();
      Object value = b ? readString() : readInt();
      script.put(key, value);
    }
    return script;
  }
}
