package com.palidinodh.command.beta;

import com.palidinodh.osrscore.io.incomingpacket.CommandHandler;
import com.palidinodh.osrscore.io.incomingpacket.CommandHandler.Beta;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceName;

@ReferenceName("debug")
class DebugCommand implements CommandHandler, Beta {

  @Override
  public void execute(Player player, String name, String message) {
    player.getOptions().setDebug(!player.getOptions().isDebug());
    player.getGameEncoder().sendMessage("Debug: " + player.getOptions().isDebug());
  }
}
