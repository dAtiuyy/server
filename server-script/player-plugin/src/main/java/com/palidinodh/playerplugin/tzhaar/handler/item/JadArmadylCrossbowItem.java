package com.palidinodh.playerplugin.tzhaar.handler.item;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.io.incomingpacket.ItemHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ItemId.ARMADYL_CROSSBOW_OR_60049)
class JadArmadylCrossbowItem implements ItemHandler {

  @Override
  public void itemOption(Player player, DefinitionOption option, Item item) {
    switch (option.getText()) {
      case "revert":
        {
          item.remove();
          player.getInventory().addItem(ItemId.ARMADYL_CROSSBOW);
          player.getInventory().addItem(ItemId.ACB_ORNAMENT_KIT_60060);
          break;
        }
    }
  }
}
