package com.palidinodh.maparea.asgarnia.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.playerplugin.teleports.TeleportsPlugin;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId(ObjectId.ROCK_19141)
class HotAirBalloonMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    player.getPlugin(TeleportsPlugin.class).sendHotAirBalloonTransport(mapObject);
  }
}
