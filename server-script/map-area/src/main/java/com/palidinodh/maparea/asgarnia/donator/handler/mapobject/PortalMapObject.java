package com.palidinodh.maparea.asgarnia.donator.handler.mapobject;

import com.palidinodh.cache.definition.util.DefinitionOption;
import com.palidinodh.cache.id.ObjectId;
import com.palidinodh.osrscore.io.incomingpacket.MapObjectHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.map.MapObject;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({
  ObjectId.PORTAL_OF_CHAMPIONS,
  ObjectId.PORTAL_OF_LEGENDS,
  ObjectId.PORTAL_OF_HEROES,
  ObjectId.PORTAL_7315
})
class PortalMapObject implements MapObjectHandler {

  @Override
  public void mapObjectOption(Player player, DefinitionOption option, MapObject mapObject) {
    switch (mapObject.getId()) {
      case ObjectId.PORTAL_OF_CHAMPIONS:
      case ObjectId.PORTAL_7315:
      case ObjectId.PORTAL_OF_HEROES:
      case ObjectId.PORTAL_OF_LEGENDS:
        player
            .getGameEncoder()
            .sendMessage("Where should this teleport to? ID: " + mapObject.getId());
        break;
    }
  }
}
