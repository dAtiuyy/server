package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitMarkType;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatFocus;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatImmunity;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSpawn;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.random.PRandom;
import com.palidinodh.util.PNumber;
import java.util.Arrays;
import java.util.List;

class VerzikNylocasCombat extends NpcCombat {

  @Inject private Npc npc;
  private Npc verzikVitur;

  private static boolean instantKill(Player player) {
    if (player.getEquipment().getHeadId() == ItemId.SERPENTINE_HELM) {
      return true;
    }
    if (player.getEquipment().getHeadId() == ItemId.TANZANITE_HELM) {
      return true;
    }
    if (player.getEquipment().getHeadId() == ItemId.MAGMA_HELM) {
      return true;
    }
    var weaponDef = player.getEquipment().getWeaponDef();
    if (weaponDef.getId() == ItemId.TOXIC_BLOWPIPE) {
      return true;
    }
    if (weaponDef.getId() == ItemId.TOXIC_STAFF_OF_THE_DEAD) {
      return true;
    }
    if (weaponDef.getId() == ItemId.TRIDENT_OF_THE_SWAMP) {
      return true;
    }
    if (weaponDef.getId() == ItemId.TRIDENT_OF_THE_SWAMP_E) {
      return true;
    }
    return weaponDef.getName().contains("(p");
  }

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var ischyrosCombat = NpcCombatDefinition.builder();
    ischyrosCombat.id(NpcId.NYLOCAS_ISCHYROS_162_8381);
    ischyrosCombat.hitpoints(NpcCombatHitpoints.total(200));
    ischyrosCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(100)
            .magicLevel(100)
            .rangedLevel(100)
            .defenceLevel(100)
            .build());
    ischyrosCombat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    ischyrosCombat.deathAnimation(8005);

    var toxobolosCombat = NpcCombatDefinition.builder();
    toxobolosCombat.id(NpcId.NYLOCAS_TOXOBOLOS_162_8382);
    toxobolosCombat.hitpoints(NpcCombatHitpoints.total(200));
    toxobolosCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(100)
            .magicLevel(100)
            .rangedLevel(100)
            .defenceLevel(100)
            .build());
    toxobolosCombat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    toxobolosCombat.deathAnimation(7998);

    var hagiosCombat = NpcCombatDefinition.builder();
    hagiosCombat.id(NpcId.NYLOCAS_HAGIOS_162_8383);
    hagiosCombat.hitpoints(NpcCombatHitpoints.total(200));
    hagiosCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(100)
            .magicLevel(100)
            .rangedLevel(100)
            .defenceLevel(100)
            .build());
    hagiosCombat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    hagiosCombat.deathAnimation(7991);

    var matomenosCombat = NpcCombatDefinition.builder();
    matomenosCombat.id(NpcId.NYLOCAS_MATOMENOS_115_8385);
    matomenosCombat.spawn(NpcCombatSpawn.builder().animation(8098).build());
    matomenosCombat.hitpoints(NpcCombatHitpoints.total(200));
    matomenosCombat.stats(
        NpcCombatStats.builder()
            .attackLevel(100)
            .magicLevel(100)
            .rangedLevel(100)
            .defenceLevel(100)
            .build());
    matomenosCombat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    matomenosCombat.deathAnimation(8097);

    var athanatosCombat = NpcCombatDefinition.builder();
    athanatosCombat.id(NpcId.NYLOCAS_ATHANATOS_350);
    athanatosCombat.spawn(NpcCombatSpawn.builder().animation(8079).build());
    athanatosCombat.hitpoints(NpcCombatHitpoints.total(180));
    athanatosCombat.stats(NpcCombatStats.builder().magicLevel(50).defenceLevel(50).build());
    athanatosCombat.immunity(NpcCombatImmunity.builder().poison(true).venom(true).build());
    athanatosCombat.focus(NpcCombatFocus.builder().retaliationDisabled(true).build());
    athanatosCombat.deathAnimation(8078);

    return Arrays.asList(
        ischyrosCombat.build(),
        toxobolosCombat.build(),
        hagiosCombat.build(),
        matomenosCombat.build(),
        athanatosCombat.build());
  }

  @Override
  public void spawnHook() {
    npc.setLargeVisibility();
    npc.getController().setMagicBind(4);
    verzikVitur = npc.getController().getNpc(NpcId.VERZIK_VITUR_1265, NpcId.VERZIK_VITUR_1520);
    if (npc.getId() == NpcId.NYLOCAS_ATHANATOS_350) {
      npc.setFaceEntity(verzikVitur);
    }
    var playerCount = getAttackablePlayers().size();
    var multiplier = PNumber.addDoubles(0.375, playerCount * 0.125);
    setMaxHitpoints((int) (getMaxHitpoints() * multiplier));
    setHitpoints(getMaxHitpoints());
  }

  @Override
  public void despawnHook() {
    verzikVitur = null;
  }

  @Override
  public void tickStartHook() {
    if (npc.getId() == NpcId.NYLOCAS_ISCHYROS_162_8381
        || npc.getId() == NpcId.NYLOCAS_TOXOBOLOS_162_8382
        || npc.getId() == NpcId.NYLOCAS_HAGIOS_162_8383) {
      exploderTick();
    } else if (npc.getId() == NpcId.NYLOCAS_MATOMENOS_115_8385) {
      matomenosTick();
    } else if (npc.getId() == NpcId.NYLOCAS_ATHANATOS_350) {
      athanatosTick();
    }
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (npc.getId() == NpcId.NYLOCAS_ATHANATOS_350) {
      athanatosDamaged(opponent);
    }
    return damage;
  }

  private void exploderTick() {
    if (npc.isLocked()) {
      return;
    }
    var following = npc.getMovement().getFollowing();
    if (!npc.withinDistance(following, 1)) {
      return;
    }
    npc.getMovement().setFollowing(null);
    npc.getMovement().clear();
    addSingleEvent(
        1,
        t -> {
          startDeath();
          addSingleEvent(
              1,
              t2 -> {
                if (following.isLocked()) {
                  return;
                }
                if (!npc.withinDistance(following, 2)) {
                  return;
                }
                following.getCombat().addHitEvent(new HitEvent(new Hit(PRandom.randomI(63))));
              });
        });
  }

  private void matomenosTick() {
    if (npc.isLocked()) {
      return;
    }
    if (verzikVitur == null || verzikVitur.isLocked()) {
      return;
    }
    if (npc.getTotalTicks() == 40) {
      var remainingHitpoints = getHitpoints();
      startDeath();
      var projectile =
          Graphic.Projectile.builder()
              .id(1587)
              .startTile(npc)
              .entity(verzikVitur)
              .speed(getProjectileSpeed(verzikVitur))
              .build();
      verzikVitur
          .getCombat()
          .addHitEvent(
              new HitEvent(
                  projectile.getEventDelay(),
                  new Hit(remainingHitpoints, HitMarkType.HEAL, HitStyleType.HEAL)));
    }
  }

  private void athanatosTick() {
    if (npc.isLocked() || isHitDelayed()) {
      return;
    }
    if (verzikVitur == null || verzikVitur.isLocked()) {
      return;
    }
    setHitDelay(4);
    var projectile =
        Graphic.Projectile.builder()
            .id(1587)
            .startTile(npc)
            .entity(verzikVitur)
            .speed(getProjectileSpeed(verzikVitur))
            .build();
    verzikVitur
        .getCombat()
        .addHitEvent(
            new HitEvent(
                projectile.getEventDelay(),
                new Hit(PRandom.randomI(11), HitMarkType.HEAL, HitStyleType.HEAL)));
    sendMapProjectile(projectile);
  }

  private void athanatosDamaged(Entity opponent) {
    if (npc.isLocked() || !opponent.isPlayer()) {
      return;
    }
    if (verzikVitur == null || verzikVitur.isLocked()) {
      return;
    }
    var player = opponent.asPlayer();
    if (!instantKill(player)) {
      return;
    }
    startDeath();
    npc.setGraphic(1590);
    var projectile =
        Graphic.Projectile.builder()
            .id(1588)
            .startTile(npc)
            .entity(verzikVitur)
            .speed(getProjectileSpeed(verzikVitur))
            .build();
    verzikVitur
        .getCombat()
        .addHitEvent(
            new HitEvent(
                projectile.getEventDelay(), new Hit(PRandom.randomI(70, 75), HitMarkType.GREEN)));
  }
}
