package com.palidinodh.maparea.kharidiandesert;

import com.palidinodh.osrscore.model.map.area.Area;
import com.palidinodh.rs.reference.ReferenceId;
import com.palidinodh.rs.reference.ReferenceIdSet;

@ReferenceId(13362)
@ReferenceIdSet(
    primary = 13106,
    secondary = {99, 100, 101, 112, 113, 114, 115, 116, 117, 118, 119})
@ReferenceIdSet(primary = 13107, secondary = 112)
@ReferenceIdSet(
    primary = 13363,
    secondary = {0, 16, 32, 33, 48, 49, 64, 65, 80, 81, 96, 97, 112, 113})
@ReferenceIdSet(
    primary = 13618,
    secondary = {0, 1, 2, 3, 4, 5, 6, 7})
@ReferenceIdSet(primary = 13619, secondary = 0)
public class DuelArenaArea extends Area {}
