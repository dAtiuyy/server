package com.palidinodh.cache.store.fs;

import com.palidinodh.cache.store.fs.jagex.CompressionType;
import com.palidinodh.cache.store.util.BZip2;
import com.palidinodh.cache.store.util.Crc32;
import com.palidinodh.cache.store.util.GZip;
import com.palidinodh.cache.store.util.IndexType;
import com.palidinodh.cache.store.util.Ints;
import com.palidinodh.cache.store.util.Stream;
import java.io.IOException;
import java.util.stream.IntStream;

public class Container {

  public byte[] data;
  public int compression;
  public int revision;
  public int crc;

  public Container(int compression, int revision) {
    this.compression = compression;
    this.revision = revision;
  }

  public static Container decompress(int index, int archive, byte[] b, int[] keys)
      throws IOException {
    var stream = new Stream(b);
    var compression = stream.readUnsignedByte();
    var compressedLength = stream.readInt();
    if (compressedLength < 0 || compressedLength > 64000000) {
      throw new RuntimeException("[" + index + "-" + archive + "] Invalid data: ");
    }
    Crc32.reset();
    Crc32.update(b, 0, 5);
    byte[] data;
    var revision = -1;
    switch (compression) {
      case CompressionType.NONE:
        data = noCompression(stream, keys, compressedLength);
        break;
      case CompressionType.BZ2:
        {
          var encryptedData = new byte[compressedLength + 4];
          stream.readBytes(encryptedData);
          Crc32.update(encryptedData);
          var decryptedData = decrypt(encryptedData, encryptedData.length, keys);
          var decrytpedStream = new Stream(decryptedData);
          var decompressedLength = decrytpedStream.readInt();
          data = BZip2.decompress(decrytpedStream.getRemaining(), compressedLength);
          if (data == null) {
            return null;
          }
          if (IndexType.get(index) != IndexType.MAP && data.length != decompressedLength) {
            throw new RuntimeException(
                "["
                    + index
                    + "-"
                    + archive
                    + "] Decompressed length unexpected: "
                    + data.length
                    + "/"
                    + decompressedLength);
          }
          break;
        }
      case CompressionType.GZ:
        {
          var encryptedData = new byte[compressedLength + 4];
          stream.readBytes(encryptedData);
          Crc32.update(encryptedData);
          var decryptedData = decrypt(encryptedData, encryptedData.length, keys);
          var decrytpedStream = new Stream(decryptedData);
          var decompressedLength = decrytpedStream.readInt();
          try {
            data = GZip.decompress(decrytpedStream.getRemaining(), compressedLength);
          } catch (IOException ioe) {
            data = decryptedData;
          }
          if (data == null) {
            return null;
          }
          if (IndexType.get(index) != IndexType.MAP && data.length != decompressedLength) {
            throw new RuntimeException(
                "["
                    + index
                    + "-"
                    + archive
                    + "] Decompressed length unexpected: "
                    + data.length
                    + "/"
                    + decompressedLength);
          }
          break;
        }
      default:
        throw new RuntimeException(
            "[" + index + "-" + archive + "] Unknown decompression type: " + compression);
    }
    if (stream.available() >= 2) {
      revision = stream.readUnsignedShort();
      if (revision == -1) {
        throw new RuntimeException("[" + index + "-" + archive + "] Revision is -1");
      }
    }
    var container = new Container(compression, revision);
    container.data = data;
    container.crc = Crc32.getRoughHash();
    return container;
  }

  private static byte[] noCompression(Stream stream, int[] keys, int compressedLength) {
    var encryptedData = new byte[compressedLength];
    stream.readBytes(encryptedData, 0, compressedLength);
    Crc32.update(encryptedData, 0, compressedLength);
    return decrypt(encryptedData, encryptedData.length, keys);
  }

  private static byte[] decrypt(byte[] data, int length, int[] keys) {
    if (keys == null) {
      return data;
    }
    if (IntStream.of(0, 1, 2, 3).allMatch(i -> keys[i] == 0)) {
      return data;
    }
    var stream = new Stream(data);
    stream.decodeXtea(keys, 0, length);
    return stream.getRemaining();
  }

  private static byte[] encrypt(byte[] data, int length, int[] keys) {
    if (keys == null) {
      return data;
    }
    var stream = new Stream(data);
    stream.encodeXtea(keys, 0, length);
    return stream.getRemaining();
  }

  private static byte[] concat(byte[]... arrays) {
    var length = 0;
    for (var array : arrays) {
      length += array.length;
    }
    var result = new byte[length];
    var pos = 0;
    for (var array : arrays) {
      System.arraycopy(array, 0, result, pos, array.length);
      pos += array.length;
    }
    return result;
  }

  public void compress(byte[] data, int[] keys) throws IOException {
    var stream = new Stream();
    byte[] compressedData;
    int length;
    switch (compression) {
      case CompressionType.NONE:
        compressedData = data;
        length = compressedData.length;
        break;
      case CompressionType.BZ2:
        compressedData = concat(Ints.toByteArray(data.length), BZip2.compress(data));
        length = compressedData.length - 4;
        break;
      case CompressionType.GZ:
        compressedData = concat(Ints.toByteArray(data.length), GZip.compress(data));
        length = compressedData.length - 4;
        break;
      default:
        throw new RuntimeException("Unknown compression type: " + compression);
    }
    compressedData = encrypt(compressedData, compressedData.length, keys);
    stream.writeByte(compression);
    stream.writeInt(length);
    stream.writeBytes(compressedData);
    if (revision != -1) {
      stream.writeShort(revision);
    }
    this.data = stream.toByteArray();
  }
}
