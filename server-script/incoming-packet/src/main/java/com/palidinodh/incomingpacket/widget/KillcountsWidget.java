package com.palidinodh.incomingpacket.widget;

import com.palidinodh.cache.id.WidgetId;
import com.palidinodh.osrscore.io.incomingpacket.WidgetHandler;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.rs.reference.ReferenceId;

@ReferenceId({WidgetId.KILL_LOG, WidgetId.NPC_DROP_RATES_700})
class KillcountsWidget implements WidgetHandler {

  @Override
  public void widgetOption(
      Player player, int option, int widgetId, int childId, int slot, int itemId) {
    if (widgetId == WidgetId.NPC_DROP_RATES_700) {
      switch (childId) {
        case 425:
          player.getCombat().openNPCRareLootList();
          break;
      }
    }
  }
}
