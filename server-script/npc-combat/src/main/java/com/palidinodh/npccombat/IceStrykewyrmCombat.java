package com.palidinodh.npccombat;

import com.google.inject.Inject;
import com.palidinodh.cache.id.ItemId;
import com.palidinodh.cache.id.NpcId;
import com.palidinodh.osrscore.model.combat.Hit;
import com.palidinodh.osrscore.model.combat.HitEvent;
import com.palidinodh.osrscore.model.combat.HitStyleType;
import com.palidinodh.osrscore.model.entity.Entity;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombat;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDefinition;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatHitpoints;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatSlayer;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatStats;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatDamage;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatEffect;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatProjectile;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyle;
import com.palidinodh.osrscore.model.entity.npc.combat.style.NpcCombatStyleType;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.graphic.Graphic;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.item.clue.ClueScrollType;
import com.palidinodh.random.PRandom;
import java.util.Arrays;
import java.util.List;

class IceStrykewyrmCombat extends NpcCombat {

  @Inject private Npc npc;

  private static boolean hasFireCape(Player player) {
    var capeId = player.getEquipment().getCapeId();
    if (capeId == ItemId.FIRE_MAX_CAPE) {
      return true;
    }
    if (capeId == ItemId.INFERNAL_CAPE) {
      return true;
    }
    if (capeId == ItemId.INFERNAL_MAX_CAPE) {
      return true;
    }
    return capeId == ItemId.FIRE_CAPE;
  }

  @Override
  public List<NpcCombatDefinition> getCombatDefinitions() {
    var drop =
        NpcCombatDrop.builder()
            .rareDropTableDenominator(42)
            .gemDropTableDenominator(42)
            .clue(ClueScrollType.ELITE, 512)
            .clue(ClueScrollType.HARD, 128);
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(512).log(true);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STAFF_OF_LIGHT)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.RARE);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.AIR_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BODY_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COSMIC_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.EARTH_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.FIRE_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MIND_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_ARROW, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_ARROW, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_JAVELIN, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DWARF_WEED_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAPLE_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_SQ_SHIELD)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.UNCOMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BLOOD_RUNE, 15)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE, 20)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATER_TALISMAN_NOTED, 1, 3)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_CADANTINE_NOTED, 1, 4)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_DWARF_WEED_NOTED, 1, 4)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_LANTADYME_NOTED, 1, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LIMPWURT_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BAR_NOTED)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(NpcCombatDropTable.COMMON);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MITHRIL_BATTLEAXE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_AVANTOE_NOTED, 1, 4)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_GUAM_LEAF_NOTED, 1, 4)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_HARRALANDER_NOTED, 1, 4)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_IRIT_LEAF_NOTED, 1, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_KWUARM_NOTED, 1, 4)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_MARRENTILL_NOTED, 1, 4)));
    dropTable.drop(
        NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_RANARR_WEED_NOTED, 1, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.GRIMY_TARROMIN_NOTED, 1, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.BELLADONNA_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CACTUS_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.JANGERBERRY_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.KWUARM_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.POISON_IVY_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RANARR_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STRAWBERRY_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TARROMIN_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOADFLAX_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WATERMELON_SEED, 1, 4)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WHITEBERRY_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.WILDBLOOD_SEED)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 200, 8138)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CRUSHED_NEST_NOTED, 5, 9)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.PURE_ESSENCE_NOTED, 200)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHARK, 1, 6)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.MAGIC_LOGS_NOTED, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SUPER_DEFENCE_3)));
    drop.table(dropTable.build());

    var combat = NpcCombatDefinition.builder();
    combat.id(NpcId.ICE_STRYKEWYRM_210_16063).id(NpcId.MOUND_16064);
    combat.hitpoints(NpcCombatHitpoints.total(300));
    combat.stats(
        NpcCombatStats.builder().attackLevel(152).magicLevel(152).defenceLevel(152).build());
    combat.slayer(NpcCombatSlayer.builder().level(93).taskOnly(true).build());
    combat.deathAnimation(7599);
    combat.drop(drop.build());

    var style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MELEE_SLASH);
    style.damage(NpcCombatDamage.maximum(17));
    style.animation(7597).attackSpeed(4);
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.builder().hitStyleType(HitStyleType.MAGIC).weight(10).build());
    style.damage(NpcCombatDamage.maximum(20));
    style.animation(7598).attackSpeed(4);
    style.targetGraphic(new Graphic(1257));
    style.projectile(NpcCombatProjectile.id(1256));
    combat.style(style.build());

    style = NpcCombatStyle.builder();
    style.type(NpcCombatStyleType.MAGIC);
    style.damage(NpcCombatDamage.maximum(20));
    style.animation(7598).attackSpeed(4);
    style.targetGraphic(new Graphic(369));
    style.projectile(NpcCombatProjectile.id(1256));
    style.effect(NpcCombatEffect.builder().bind(5).build());
    combat.style(style.build());

    return Arrays.asList(combat.build());
  }

  @Override
  public void tickStartHook() {
    checkId();
    digAttack();
  }

  @Override
  public double damageInflictedHook(NpcCombatStyle combatStyle, Entity opponent, double damage) {
    if (!opponent.isPlayer() || damage == 0) {
      return damage;
    }
    if (hasFireCape(opponent.asPlayer())) {
      return damage;
    }
    return damage + 4;
  }

  @Override
  public double damageReceivedHook(
      Entity opponent, double damage, HitStyleType hitStyleType, HitStyleType defenceType) {
    if (!opponent.isPlayer() || damage == 0) {
      return damage;
    }
    var player = opponent.asPlayer();
    var spell = player.getMagic().getActiveSpell();
    if (hasFireCape(player) && spell != null) {
      switch (spell.getSpellbook()) {
        case FIRE_STRIKE:
        case FIRE_BOLT:
        case FIRE_BLAST:
        case FIRE_WAVE:
        case FIRE_SURGE:
          damage *= 2;
          break;
      }
      damage += 4;
    }
    return damage;
  }

  private void checkId() {
    if (npc.isLocked()) {
      return;
    }
    if (!inRecentCombat() && !isAttacking() && npc.getId() != NpcId.MOUND_16064) {
      npc.setId(NpcId.MOUND_16064);
      return;
    }
    if ((inRecentCombat() || isAttacking()) && npc.getId() != NpcId.ICE_STRYKEWYRM_210_16063) {
      npc.setId(NpcId.ICE_STRYKEWYRM_210_16063);
      npc.setAnimation(7601);
      setHitDelay(4);
    }
  }

  private void digAttack() {
    if (npc.getId() == NpcId.MOUND_16064) {
      return;
    }
    if (npc.isLocked()) {
      return;
    }
    if (!isAttacking()) {
      return;
    }
    if (getHitDelay() > 0) {
      return;
    }
    if (PRandom.randomE(10) != 0) {
      return;
    }
    var opponent = getAttackingEntity();
    if (!npc.getController().routeAllow(opponent)) {
      return;
    }
    npc.setLock(8);
    npc.getMovement().animatedTeleport(opponent, 7600, 7601, null, null, 4);
    addSingleEvent(
        6,
        e -> {
          if (npc.withinDistance(opponent, 0)) {
            opponent.getCombat().addHitEvent(new HitEvent(new Hit(30)));
          }
          setAttackingEntity(opponent);
        });
  }
}
