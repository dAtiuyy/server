package com.palidinodh.npccombat.hook;

import com.palidinodh.cache.id.ItemId;
import com.palidinodh.osrscore.model.entity.npc.Npc;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDrop;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTable;
import com.palidinodh.osrscore.model.entity.npc.combat.NpcCombatDropTableDrop;
import com.palidinodh.osrscore.model.entity.player.Player;
import com.palidinodh.osrscore.model.item.Item;
import com.palidinodh.osrscore.model.item.RandomItem;
import com.palidinodh.osrscore.model.tile.Tile;
import com.palidinodh.random.PRandom;

public class RareDropTable {

  public static final NpcCombatDrop RARE_DROP;
  public static final NpcCombatDrop GEM_RARE_DROP;
  public static final NpcCombatDrop ROW_GEM_RARE_DROP;
  public static final NpcCombatDrop MEGA_RARE_DROP;
  public static final NpcCombatDrop ROW_MEGA_RARE_DROP;

  static {
    var drop = NpcCombatDrop.builder();
    var dropTable = NpcCombatDropTable.builder().probabilityDenominator(128);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_MED_HELM)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_KITESHIELD)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(64);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.ADAMANT_JAVELIN, 5)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DEATH_RUNE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LAW_RUNE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_ARROW, 42)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.STEEL_ARROW, 150)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_SQ_SHIELD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGONSTONE)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SILVER_ORE_NOTED, 100)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(42);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NATURE_RUNE, 67)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_2H_SWORD)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_BATTLEAXE)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(25);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNITE_BAR)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(6);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.COINS, 3000)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LOOP_HALF_OF_KEY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOOTH_HALF_OF_KEY)));
    drop.table(dropTable.build());
    RARE_DROP = drop.build();

    drop = NpcCombatDrop.builder();
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(128);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LOOP_HALF_OF_KEY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOOTH_HALF_OF_KEY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_JAVELIN, 5)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(64);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DIAMOND)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(42);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NATURE_TALISMAN)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(16);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_RUBY)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(8);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_EMERALD)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(4);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_SAPPHIRE)));
    drop.table(dropTable.build());
    GEM_RARE_DROP = drop.build();

    drop = NpcCombatDrop.builder();
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(65);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.LOOP_HALF_OF_KEY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.TOOTH_HALF_OF_KEY)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_JAVELIN, 5)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(32);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_DIAMOND)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(21);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.CHAOS_TALISMAN)));
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.NATURE_TALISMAN)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(8);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_RUBY)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(4);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_EMERALD)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(2);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.UNCUT_SAPPHIRE)));
    drop.table(dropTable.build());
    ROW_GEM_RARE_DROP = drop.build();

    drop = NpcCombatDrop.builder();
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(42);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_SPEAR)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(32);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHIELD_LEFT_HALF)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(16);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_SPEAR)));
    drop.table(dropTable.build());
    MEGA_RARE_DROP = drop.build();

    drop = NpcCombatDrop.builder();
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(5);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.DRAGON_SPEAR)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(3);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.SHIELD_LEFT_HALF)));
    drop.table(dropTable.build());
    dropTable = NpcCombatDropTable.builder().probabilityDenominator(2);
    dropTable.drop(NpcCombatDropTableDrop.items(new RandomItem(ItemId.RUNE_SPEAR)));
    drop.table(dropTable.build());
    ROW_MEGA_RARE_DROP = drop.build();
  }

  public static void rareDropTable(Npc npc, Player player, Tile dropTile) {
    var row = NpcCombatDropTableDrop.hasRow(player);
    var megaDenominator = player.getCombat().getDropRateDenominator(8);
    if (PRandom.inRange(1, megaDenominator)) {
      if (row) {
        ROW_MEGA_RARE_DROP.getItems(player).forEach(i -> dropItem(player, dropTile, i));
      } else {
        MEGA_RARE_DROP.getItems(player).forEach(i -> dropItem(player, dropTile, i));
      }
      return;
    }
    var gemDenominator = player.getCombat().getDropRateDenominator(6);
    if (PRandom.inRange(1, gemDenominator)) {
      gemDropTable(npc, player, dropTile);
      return;
    }
    RARE_DROP.getItems(player).forEach(i -> dropItem(player, dropTile, i));
  }

  public static void gemDropTable(Npc npc, Player player, Tile dropTile) {
    var row = NpcCombatDropTableDrop.hasRow(player);
    var megaDenominator = player.getCombat().getDropRateDenominator(row ? 65 : 128);
    if (PRandom.inRange(1, megaDenominator)) {
      if (row) {
        ROW_MEGA_RARE_DROP.getItems(player).forEach(i -> dropItem(player, dropTile, i));
      } else {
        MEGA_RARE_DROP.getItems(player).forEach(i -> dropItem(player, dropTile, i));
      }
      return;
    }
    if (row) {
      ROW_GEM_RARE_DROP.getItems(player).forEach(i -> dropItem(player, dropTile, i));
    } else {
      GEM_RARE_DROP.getItems(player).forEach(i -> dropItem(player, dropTile, i));
    }
  }

  private static void dropItem(Player player, Tile dropTile, Item item) {
    if (player.canDonatorNoteItem(item.getId()) && item.getDef().getNotedId() != -1) {
      item = new Item(item.getDef().getNotedId(), item.getAmount());
    }
    if (player.canDonatorPickupItem(item.getId()) && player.getInventory().canAddItem(item)) {
      player.getInventory().addItem(item);
    } else {
      player.getController().addMapItem(item, dropTile, player);
    }
  }
}
